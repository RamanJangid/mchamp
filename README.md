
Step 1. Add the JitPack repository to your build file

    Add it in your root build.gradle at the end of repositories
	allprojects {
		repositories {
			maven { url 'https://jitpack.io' }
			credentials { username authToken }
		}
	}
  
Step 2. Add the dependency

    dependencies {
	        implementation 'com.github.rginfotechtest:fantasy:3.3'
	}
	
Step 3. Add the token to your gradle.properties file

   authToken=jp_5d2pc70h2o8sd70nfkdeveneph
   
Step 4. Add this line where u want to open fantasy app

    startActivity(new Intent(this, HomeActivity.java))
  


------ Wallet Click Listener Integration -------


Step 1. Please implement the "OnWalletClickListener" where u want get wallet click listener

Step 2. After implement please add the below line in your file

     MyApplication.walletClickListener=this;
     
Step 3. After above steps please write your code to in this function whatever you want     

    @Override
    public void onFantasyWalletClick() {
        //Put here your code
    }
     
Date :-  03/05/2021 12:41 PM     
     
------- Library version ---------
Step1. Please update library version 3.3 -> 3.5


------- Set Credentials-------------------------

Step 1. Please pass the credentials with home activity intent like below.

 startActivity(new Intent(this, HomeActivity.java).putExtra("userId","example").putExtra("deviceId","example"))
  