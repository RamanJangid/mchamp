package com.rg.mchampgold.common.utils;

import android.util.Log;


import com.rg.mchampgold.BuildConfig;

import okhttp3.logging.HttpLoggingInterceptor;

public class Constants {

    public static final String SHARED_PREFERENCES_IS_LOGGED_IN = "is_logged_in";
    public static final String SHARED_PREFERENCE_USER_ID = "user_id";
    public static final String SHARED_PREFERENCE_M_USER_ID = "m_user_id";
    public static final String SHARED_PREFERENCE_DEVICE_ID = "device_id";
    public static final String SHARED_PREFERENCE_USER_TOKEN = "user_token";
    public static final String SHARED_PREFERENCE_USER_NAME = "user_name";
    public static final String SHARED_PREFERENCE_USER_EMAIL = "user_email";
    public static final String SHARED_PREFERENCE_USER_MOBILE = "user_mobile";

    public static final String SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS = "user_mobile_verify_status";
    public static final String SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS = "user_email_verify_status";
    public static final String SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS = "user_pan_verify_status";
    public static final String SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS = "user_bank_verify_status";

    public static final String SHARED_PREFERENCE_USER_FCM_TOKEN = "user_fcm_token";
    public static final String SHARED_PREFERENCE_USER_REFER_CODE = "user_refer_code";
    public static final String KEY_FANTASY_TYPE_STATUS = "fantasy_type_status";
    public static final String SPORT_KEY = "sport_key";

    public static final String SHARED_PREFERENCE_USER_TEAM_NAME = "user_team_name";
    public static final String SHARED_PREFERENCE_USER_STATE = "user_state";
    public static final String SHARED_PREFERENCE_USER_PIC = "user_pic_url";
    public static final String SHARED_PREFERENCES_ACCESS_TOKEN = "access_token";
    public static final String SHARED_PREFERENCES_CLIENT_ACCESS_TOKEN = "client_access_token";
    public static final String SHARED_SPORTS_LIST= "SHARED_SPORTS_LIST";

    public static final String IS_MULTI_SPORTS_ENABLE = "IS_MULTI_SPORTS_ENABLE";
    public static final String MULTI_SPORTS = "MULTI_SPORTS";
    public static final String AUTHTOKEN = "AUTHTOKEN";
    public static final String VERIFIED = "Verified";
    public static final String UNVERIFIED = "Unverified";
    public static final String PAYTM = "paytm";
    public static final String NO_TEAM_CREATED = "You have not created any team yet !!";
    public static final String SOMETHING_WENT_WRONG = "Oops! Something went Wrong";
    public static final long OTP_SEND_TIME = 60 * 1000;
    public static final long SPLASH_TIMEOUT = 3000;
    public static final String APP_NAME = "mChamp Gold";
    public static final String RAZORPAY = "razorPay";
    public static final int MIN_BANK_WITHDRAW_AMOUNT = 200;
    public static final int MIN_PAYTM_WITHDRAW_AMOUNT = 200;

    public static final int MAX_PAYTM_WITHDRAW_AMOUNT = 10000;
    public static final long MAX_BANK_WITHDRAW_AMOUNT = 200000;
    public static final double DEFAULT_PERCENT = 1.15;
    public static final String USER_TEAM_COUNT = "user_team_count";

    private static final String API_VERSION = "1.1";
    public static final String ACCEPT_HEADER = "application/vnd.md.api.v" + API_VERSION + "+json";
    public static final HttpLoggingInterceptor.Level HTTPLogLevel = BuildConfig.DEBUG?HttpLoggingInterceptor.Level.BODY:HttpLoggingInterceptor.Level.NONE;
    public static final String KEY_WINING_PERCENT = "key_winning_percent";


    public static final int KEY_LIVE_MATCH = 1;
    public static final int KEY_UPCOMING_MATCH = 2;
    public static final int KEY_FINISHED_MATCH = 3;

    //PAN STATUS
    public static final int KEY_PAN_VERIFIED = 1;
    public static final int KEY_PAN_UNVERIFIED = 0;
    public static final int KEY_PAN_NOT_REQUESTED = -1;
    public static final int KEY_PAN_REJECTED = 2;

    //BANK STATUS
    public static final int KEY_BANK_VERIFIED = 1;
    public static final int KEY_BANK_UNVERIFIED = 0;
    public static final int KEY_BANK_NOT_REQUESTED = -1;
    public static final int KEY_BANK_REJECTED = 2;

    //EMAIL STATUS
    public static final int KEY_EMAIL_VERIFIED = 1;
    public static final int KEY_EMAIL_UNVERIFIED = 0;

    //MOBILE_STATUS
    public static final int KEY_MOBILE_VERIFIED = 1;
    public static final int KEY_MOBILE_UNVERIFIED = 0;


    //Player Role
    public static final String KEY_PLAYER_ROLE_BAT = "batsman";
    public static final String KEY_PLAYER_ROLE_ALL_R = "allrounder";
    public static final String KEY_PLAYER_ROLE_BOL = "bowler";
    public static final String KEY_PLAYER_ROLE_KEEP = "keeper";

    //That use for ic_switch_team data
    public static final String KEY_MATCH_KEY = "key_match_key";
    public static final String KEY_WINING_AMOUNT = "key_wining_amount";
    public static final String KEY_TEAM_VS = "key_team_vs";
    public static final String KEY_TEAM_FIRST_URL = "key_team_first_url";
    public static final String KEY_TEAM_SECOND_URL = "key_team_second_url";
    public static final String KEY_TEAM_ID = "key_team_id";
    public static final String KEY_TEAM_ID2 = "key_team_id2";
    public static final String KEY_CONTEST_KEY = "key_contest_key";
    public static final String KEY_IS_FOR_JOIN_CONTEST = "key_is_for_join_contest";
    public static final String KEY_CONTEST_DATA = "key_contest_data";
    public static final String KEY_USER_BALANCE = "key_user_balance";
    public static final String KEY_USER_BONUS_BALANCE = "key_user_bonus_balance";
    public static final String KEY_USER_WINING_AMOUNT = "key_user_wining_amount";
    public static final String CONTEST_ID = "CONTEST_ID";
    public static final String IS_POPUP_IMAGE_SHOWING = "IS_POPUP_IMAGE_SHOWING";


    public static final String KEY_STATUS_HEADER_TEXT = "key_status_header_text";
    public static final String KEY_STATUS_IS_TIMER_HEADER = "key_status_is_timer_text";
    public static final String KEY_IS_FOR_SWITCH_TEAM = "key_is_for_switch_team";

    public static final String KEY_TEAM_COUNT = "key_team_count";
    public static final String KEY_SERIES_ID = "key_series_id";
    public static final String KEY_IS_LEADERBOARD = "key_is_leaderboard";


    public static final String KEY_STATUS_IS_FOR_CONTEST_DETAILS = "key_is_for_contest_details";
    public static String SKIP_CREATETEAM_INSTRUCTION = "SKIP_CREATETEAM_INSTRUCTION";
    public static String SKIP_CREATECVC_INSTRUCTION = "SKIP_CREATECVC_INSTRUCTION";


    public static final String KEY_TEAM_LIST_WK = "key_team_list_wk";
    public static final String KEY_TEAM_LIST_BAT = "key_team_list_bat";
    public static final String KEY_TEAM_LIST_AR = "key_team_list_ar";
    public static final String KEY_TEAM_LIST_BOWL = "key_team_list_bowl";
    public static final String KEY_TEAM_LIST_C = "KEY_TEAM_LIST_C";
    public static final String KEY_TEAM_NAME = "key_team_name";

    public static final String KEY_ALL_CONTEST = "key_all_contest";
    public static final String ERROR_MSG = "We are facing problem...We will be right back!";

    //TAGS
    public static final String TAG_FOOTBALL = "FOOTBALL";
    public static final String TAG_BASKETBALL = "BASKETBALL";

    //CRICKET TAG
    public static final String WK = "WK";
    public static final String BAT = "BAT";
    public static final String AR = "AR";
    public static final String BOWL = "BOWL";
    //Player Role for Football
    public static final String KEY_PLAYER_ROLE_DEF = "Defender";
    public static final String KEY_PLAYER_ROLE_ST = "Forward";
    public static final String KEY_PLAYER_ROLE_GK = "Goalkeeper";
    public static final String KEY_PLAYER_ROLE_MID = "Midfielder";
    //FOOTBALL TAG
    public static final String GK = "GK";
    public static final String DEF = "DEF";
    public static final String MID = "MID";
    public static final String ST = "ST";
    //Player Role for BasketBall(NBA)
    public static final String KEY_PLAYER_ROLE_PG = "Point guard";//1-4
    public static final String KEY_PLAYER_ROLE_SG = "Shooting guard";//1-4
    public static final String KEY_PLAYER_ROLE_SF = "Small forward";//1-4
    public static final String KEY_PLAYER_ROLE_PF = "Power forward"; //1-4
    public static final String KEY_PLAYER_ROLE_C = "Center"; //1-4

    public static final String PG = "PG";
    public static final String SG = "SG";
    public static final String SF = "SF";
    public static final String PF = "PF";
    public static final String C = "C";
    public static boolean isAppInDebugMode() {
        if (BuildConfig.DEBUG) {
            Log.v("BuildConfig ", "Debug");
            return true;
        }
        return false;
    }

    // Playing Status
    public static final String PLAYING = "playing";
    public static final String NOT_PLAYING = "not_playing";
    public static final String BOTH = "both";
}
