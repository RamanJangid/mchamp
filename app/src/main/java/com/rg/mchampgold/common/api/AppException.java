package com.rg.mchampgold.common.api;

public class AppException extends Exception {
    public AppException(Throwable t) {
        super(t);
    }
}
