package com.rg.mchampgold.common.di.qualifiers;

import javax.inject.Qualifier;


@Qualifier
public @interface RestServiceOkHttpClient {
}
