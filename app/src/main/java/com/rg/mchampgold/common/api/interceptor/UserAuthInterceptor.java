package com.rg.mchampgold.common.api.interceptor;

import android.util.Log;


import com.rg.mchampgold.app.di.module.AppModule;
import com.rg.mchampgold.common.api.RestHelper;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.common.utils.TinyDB;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class UserAuthInterceptor implements Interceptor {

    AppModule appModule;
    TinyDB tinyDB;

    public UserAuthInterceptor(AppModule appModule) {
        // Better to have these as constructor arguments
        this.appModule = appModule;
        this.tinyDB = appModule.provideTinyDB();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String TAG = "UserAuthInterceptor";
        Log.e(TAG, "access token in db" + this.tinyDB.getString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN));

        // Customize the request
        Request request = original.newBuilder()
                .header("Authorization", "Bearer " + this.tinyDB.getString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN))
                //.header("registeredDeviceId", appModule.provideDeviceId())
                .build();

        Response response = chain.proceed(request);
        if (response.code() == 401) {
            RestHelper restHelper = appModule.provideMyRestHelper(appModule);
            // Log.e(TAG, " RestHelper " + restHelper.toString() + " tinyDb " + this.tinyDB.toString() + "  this " + this.toString() + " appmodule " + appModule.toString());
            synchronized (this) {
                String token = "Bearer " + this.tinyDB.getString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN);
                String requestToken = request.header("Authorization").trim();
                Log.e(TAG, "In Sync block refresh token " + requestToken + " - token " + token);
                if (!requestToken.equalsIgnoreCase("Bearer") && !requestToken.equalsIgnoreCase(token)) {
                    request = request.newBuilder()
                       //     .header("Authorization", token)
                            .build();
                    Log.e(TAG, "Not sending request " + requestToken + " - token " + token);
                }/* else if (restHelper.getToken(true)) {
                    Log.e(TAG, "Sent request " + requestToken + " - token " + this.tinyDB.getString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN));
                    // Customize the request
                    request = request.newBuilder()
                            .header("Authorization", "Bearer " + this.tinyDB.getString(Constants.SHARED_PREFERENCES_ACCESS_TOKEN))
                            .build();
                    Log.d(TAG, "response true came for getToken");
                } else {
                    Log.e(TAG, "UNEXPECTED ERROR - Can not refresh the client token");
                }*/
            }
            response = chain.proceed(request);
        }
        // Customize or return the response
        return response;
    }

    int randomWithRange(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }
}
