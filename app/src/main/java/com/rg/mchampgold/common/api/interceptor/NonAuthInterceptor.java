package com.rg.mchampgold.common.api.interceptor;

import android.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class NonAuthInterceptor implements Interceptor {

    private String TAG = "NonAuthInterceptor";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String header = "automator_app" + ":" + "automator_app_access";
        String authHeader = "";
        try {
            String base64 = Base64.encodeToString(header.getBytes("UTF-8"), Base64.DEFAULT);
            authHeader = "Basic ".concat(base64);
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();

        }
        // Customize the request
        Request request = original.newBuilder()
                //TODO: Move this to .properties
                .header("Authorization", "Basic YXV0b21hdG9yX2FwcDphdXRvbWF0b3JfYXBwX2FjY2Vzcw==")
                .method(original.method(), original.body())
                .build();

        Response response = chain.proceed(request);

        // Customize or return the response
        return response;
    }
}