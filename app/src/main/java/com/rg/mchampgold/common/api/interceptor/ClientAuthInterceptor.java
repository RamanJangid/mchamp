package com.rg.mchampgold.common.api.interceptor;



import com.rg.mchampgold.app.di.module.AppModule;
import com.rg.mchampgold.common.api.RestHelper;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.common.utils.SmartLog;
import com.rg.mchampgold.common.utils.TinyDB;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ClientAuthInterceptor implements Interceptor {

    private String TAG = "ClientAuthInterceptor";
    AppModule appModule;
    TinyDB tinyDB;

    public ClientAuthInterceptor(AppModule appModule) {
        this.appModule = appModule;
        this.tinyDB = appModule.provideTinyDB();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        SmartLog.e(TAG, "access token in db" + this.tinyDB.getString(Constants.SHARED_PREFERENCES_CLIENT_ACCESS_TOKEN));
        // Customize the request
        Request request = original.newBuilder()
                //TODO: Move this to .properties
               // .header("Authorization", "Bearer " + this.tinyDB.getString(Constants.SHARED_PREFERENCES_CLIENT_ACCESS_TOKEN))
                .method(original.method(), original.body())
                .build();

        Response response = chain.proceed(request);
        if (response.code() == 401) {
            RestHelper restHelper = appModule.provideMyRestHelper(appModule);
            /*if (restHelper.getToken(false)) {*/
                // Customize the request
                request = request.newBuilder()
                        //TODO: Move this to .properties
                      //  .header("Authorization", "Bearer " + this.tinyDB.getString(Constants.SHARED_PREFERENCES_CLIENT_ACCESS_TOKEN))
                        .method(original.method(), original.body())
                        .build();

                response = chain.proceed(request);
                SmartLog.e(TAG, "response true came for gettoken");
          /*  } else {
                SmartLog.e(TAG, "response false came for gettoken");
            }*/
        }
        // Customize or return the response
        return response;
    }
}
