package com.rg.mchampgold.common.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.rg.mchampgold.R;

import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

public class Utils {

    public static void  setFullScreen(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    public int getColor(Context context, int id) {

        final int version = Build.VERSION.SDK_INT;
        if (version >= 23) {

            return ContextCompat.getColor(context, id);

        } else {

            return context.getResources().getColor(id);

        }

    }

    public static ProgressDialog getProgressDialog(AppCompatActivity appCompatActivity) {
//        ProgressDialog progressDialog =ProgressDialog.show(appCompatActivity, "Retrieving List", "Loading...", true);;
//        progressDialog.setContentView(R.layout.layout_loading);
        ProgressDialog progressDialog =new ProgressDialog(appCompatActivity);;
        progressDialog.setMessage("Please wait...");
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    /**
     * Check Network Connectivity
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            boolean connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
            return connected;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * hide soft input keyboard
     */
    public static void hideSoftKeyboard(Activity activity) {

        try {
            if (activity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * hide soft input keyboard
     */
    public void showSoftKeyboard(Activity activity) {

        try {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * @param mActivity
     * @param messageToShow
     */
    public void showMessageSnackBar(Activity mActivity, String messageToShow) {

        Toast.makeText(mActivity, messageToShow, Toast.LENGTH_SHORT).show();

    }

    public static boolean isValidLoginPassword(EditText edt_password, AppCompatActivity appCompatActivity) {
        boolean validation = true;
        String password = edt_password.getText().toString();
        if (password.trim().isEmpty()) {
            validation = false;

        }
        return validation;
    }

//

    /**
     * Setting the layout for arabic and english
     */
    //AppUtils.setDeviceLanguage(this);
    public static void setDeviceLanguage(Activity currentActivity) {

        /**
         * Detecting current selected language of device
         */
        String currentDeviceLanguage = Locale.getDefault().getDisplayLanguage();


        if (currentDeviceLanguage.equals("فارسی") || currentDeviceLanguage.equals("العربية")) {
            setLocale(currentActivity.getApplicationContext(), "ar");
        }
    }

    /**
     * this method identifies for selection of layout for specific language like Arabic
     *
     * @param context
     * @param lang
     */
    public static void setLocale(final Context context, final String lang) {
        final Locale loc = new Locale(lang);
        Locale.setDefault(loc);
        final Configuration cfg = new Configuration();
        cfg.locale = loc;
        context.getResources().updateConfiguration(cfg, null);
    }


    /**
     * Generate Hash Key
     *
     * @param appCompatActivity
     */

    public static void mGenerateHashKey(AppCompatActivity appCompatActivity) {
        try {
            PackageInfo info = appCompatActivity.getPackageManager().getPackageInfo("com.superstrike", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static String mFilePath(String imageName) {
        return imageName.substring(imageName.lastIndexOf("/") + 1);
    }

    public static String mGetDate(Long timeStamp) {
        String dateFoemat = "";
        dateFoemat = new SimpleDateFormat("dd/MM/yyyy").format(new Date(timeStamp));

        return dateFoemat;
    }

    /**
     * valid email
     *
     * @param edtTxtEmailAddress
     * @return
     */
    public static boolean isValidEmailAddress(EditText edtTxtEmailAddress, AppCompatActivity appCompatActivity) {

        boolean validation = true;
        Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(EMAIL_PATTERN);
        String login_email_address = edtTxtEmailAddress.getText().toString();

        if (login_email_address.trim().isEmpty()) {

            validation = false;

        } else if (!EMAIL_ADDRESS_PATTERN.matcher(login_email_address).matches()) {

            validation = false;
        }

        return validation;

    }


    public static void mShowDialog(AppCompatActivity appCompatActivity, String msg) {

        AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity);
        builder.setIcon(R.mipmap.ic_launcher);

        builder.setTitle(appCompatActivity.getResources().getString(R.string.app_name));
        builder.setMessage(msg);


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

            }
        });
        AlertDialog alertDialog = builder.create();
        int textColorId = appCompatActivity.getResources().getIdentifier("alertMessage", "id", "android");
        TextView textColor = (TextView) alertDialog.findViewById(textColorId);
        if (textColor != null) {
            textColor.setTextColor(Color.BLACK);
        }
        alertDialog.show();
    }
    public static void mShowDialogd(final AppCompatActivity appCompatActivity, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setCancelable(false);

        builder.setTitle(appCompatActivity.getResources().getString(R.string.app_name));
        builder.setMessage(msg);


        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                appCompatActivity.finish();

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        int textColorId = appCompatActivity.getResources().getIdentifier("alertMessage", "id", "android");
        TextView textColor = (TextView) alertDialog.findViewById(textColorId);
        if (textColor != null) {
            textColor.setTextColor(Color.BLACK);
        }
        alertDialog.show();
    }
    public static void showError2(View parentLayout, String msg)
    {
        Snackbar snack = Snackbar.make(parentLayout, msg, Snackbar.LENGTH_LONG);
        View view = snack.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        params.setMargins(0,180,0,0);
        view.setLayoutParams(params);
        snack.show();
    }


    public static void showError1(View parentLayout, String msg)
    {
        Snackbar snack = Snackbar.make(parentLayout, msg, Snackbar.LENGTH_LONG);
        View view = snack.getView();

        CoordinatorLayout.LayoutParams params2 =(CoordinatorLayout.LayoutParams)view.getLayoutParams();
        params2.gravity = Gravity.TOP;
        params2.setMargins(0,180,0,0);
        view.setLayoutParams(params2);
        snack.show();
    }
    public static String mDate(String stringDate)
    {
        String mDate = "";
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(stringDate);
            mDate = new SimpleDateFormat("dd MMM hh:mm a").format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDate;
    }
    //2018-03-20 15:45:28
    public static Long EventDateMilisecond(String eventDate) throws ParseException {
        Date eventDatee = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(eventDate);
        long ctime = eventDatee.getTime() - System.currentTimeMillis();
        long seconds = ctime / 1000 % 60;
        long minutes = (ctime / (1000 * 60)) % 60;
        long diffHours = ctime / (60 * 60 * 1000);

        return ctime;

    }

    public static float dp2px(Resources resources, float dp) {
        final float scale = resources.getDisplayMetrics().density;
        return  dp * scale + 0.5f;
    }

    public static float sp2px(Resources resources, float sp){
        final float scale = resources.getDisplayMetrics().scaledDensity;
        return sp * scale;
    }


}
