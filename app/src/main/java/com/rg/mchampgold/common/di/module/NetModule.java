package com.rg.mchampgold.common.di.module;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.ClientRestService;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.api.service.UserRestService;
import com.rg.mchampgold.app.di.module.AppModule;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.api.LiveDataCallAdapterFactory;
import com.rg.mchampgold.common.api.interceptor.ClientAuthInterceptor;
import com.rg.mchampgold.common.api.interceptor.NonAuthInterceptor;
import com.rg.mchampgold.common.api.interceptor.UserAuthInterceptor;
import com.rg.mchampgold.common.di.qualifiers.ClientRestServiceAuthOkHttpClient;
import com.rg.mchampgold.common.di.qualifiers.ClientRestServiceOkHttpClient;
import com.rg.mchampgold.common.di.qualifiers.RestServiceOkHttpClient;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.common.utils.DateSerializer;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    final private String baseUrl;

    public NetModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    public Cache provideHttpCache(MyApplication application) {
        long cacheSize = 10 * 1024 * 1024L;
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .setLenient()
                .registerTypeAdapter(Date.class, new DateSerializer());
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        logInterceptor.setLevel(Constants.HTTPLogLevel);
        return logInterceptor;
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkhttpClient(Cache cache, HttpLoggingInterceptor loggingInterceptor) {

        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        logInterceptor.setLevel(Constants.HTTPLogLevel);
        OkHttpClient.Builder client = (new OkHttpClient.Builder().addInterceptor(loggingInterceptor).addNetworkInterceptor(chain -> {
            Request originalRequest = chain.request();
            CacheControl control = originalRequest.cacheControl();
            int maxAge = control.maxAgeSeconds();
            Request request = originalRequest.newBuilder()
                       .header("accept","application/json")
                       .header("Authorization", MyApplication.tinyDB.getString(Constants.AUTHTOKEN))
                    //.header("X-app-os", "android")
//                    .header("X-app-version", CommonUtils.getAppVersion(context))
                    .build();
            Response response = chain.proceed(request);

            if (maxAge > 0) {
                String cacheHeaderValue = "private, max-age=" + control.maxAgeSeconds();
                return response.newBuilder()
                        .removeHeader("Pragma")
                        .removeHeader("Cache-Control")
                        .header("Cache-Control", cacheHeaderValue)
                        .build();
            }
            return response;
        }));
        client.readTimeout(60, TimeUnit.SECONDS);
        client.writeTimeout(60, TimeUnit.SECONDS);
        client.connectTimeout(60, TimeUnit.SECONDS);

        client.cache(cache);

        if (Constants.isAppInDebugMode()) {
            //client.addNetworkInterceptor(BuildConfig.STETHO.networkInterceptor());
        }

        return client.build();
    }

    @Provides
    @Singleton
    @RestServiceOkHttpClient
    public OkHttpClient provideRestServiceOkHttpClient(OkHttpClient okHttpClient, AppModule appModule) {
        return okHttpClient.newBuilder().addInterceptor(new UserAuthInterceptor(appModule)).build();
    }

    @Provides
    @Singleton
    @ClientRestServiceAuthOkHttpClient
    public OkHttpClient provideClientRestServiceAuthOkHttpClient(OkHttpClient okHttpClient) {
        return okHttpClient.newBuilder().addInterceptor(new NonAuthInterceptor()).build();
    }

    @Provides
    @Singleton
    @ClientRestServiceOkHttpClient
    public OkHttpClient provideClientRestServiceOkHttpClient(OkHttpClient okHttpClient, AppModule appModule) {
        return okHttpClient.newBuilder().addInterceptor(new ClientAuthInterceptor(appModule)).build();
    }

    @Provides
    public OAuthRestService provideOAuthRestService(@ClientRestServiceAuthOkHttpClient OkHttpClient okHttpClient, Context context) { //dependencies!
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .addCallAdapterFactory(new CustomCallAdapter.ErrorHandlingCallAdapterFactory
                        (new CustomCallAdapter.MainThreadExecutor(), context))
                .build();
        return retrofit.create(OAuthRestService.class);
    }

    @Provides
    public ClientRestService provideClientRestService(@ClientRestServiceOkHttpClient OkHttpClient okHttpClient, Context context) { //dependencies!
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .addCallAdapterFactory(new CustomCallAdapter.ErrorHandlingCallAdapterFactory
                        (new CustomCallAdapter.MainThreadExecutor(), context))
                .build();
        return retrofit.create(ClientRestService.class);
    }

    @Provides
    @Singleton
    public UserRestService provideRestService(@RestServiceOkHttpClient OkHttpClient okHttpClient, Gson gson) { //dependencies!
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build();
        return retrofit.create(UserRestService.class);
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                //.addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .baseUrl(baseUrl)
                .build();
    }

}
