package com.rg.mchampgold.common.api;


import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.api.service.UserRestService;
import com.rg.mchampgold.app.di.module.AppModule;

import javax.inject.Inject;


public abstract class RestHelper {

    public static final String TAG = "RestHelper";

    AppModule appModule;

    @Inject
    UserRestService userRestService;

    @Inject
    OAuthRestService oAuthRestService;

    public RestHelper(AppModule appModule) {
        this.appModule = appModule;
        appModule.provideMyApplication().getComponent().inject(this);
    }

    public abstract void logoutUser();

    public abstract void onErrorWhileRefreshingUserToken();
}
