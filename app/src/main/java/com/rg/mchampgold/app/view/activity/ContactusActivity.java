package com.rg.mchampgold.app.view.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.databinding.ActivityContactusBinding;

import java.util.List;


public class ContactusActivity extends AppCompatActivity {

    ActivityContactusBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contactus);
        initialize();

          String emails[] = {getString(R.string.support_email)};


        binding.emailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, emails);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.setType("text/plain");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                final PackageManager pm = getPackageManager();
                final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
                ResolveInfo best = null;
                for (final ResolveInfo info : matches)
                    if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail"))
                        best = info;
                if (best != null)
                    emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                startActivity(emailIntent);
            }
        });

        binding.facebookBtn.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MyApplication.fb_url));
            startActivity(browserIntent);
        });

        binding.twitterBtn.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MyApplication.twitter_url));
            startActivity(browserIntent);
        });

        binding.instaBtn.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MyApplication.instagram_url));
            startActivity(browserIntent);
        });

        binding.telegramBtn.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(MyApplication.telegram_url));
            startActivity(browserIntent);
        });

        binding.callBtn.setOnClickListener(view -> {
            String phone = getString(R.string.support_contact);
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
            startActivity(intent);
        });

    }

    void initialize() {
        setSupportActionBar(binding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.contact_us));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
