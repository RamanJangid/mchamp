package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.SelectedPlayer;
import com.rg.mchampgold.app.view.activity.CreateTeamActivity;
import com.rg.mchampgold.app.view.interfaces.PlayerItemClickListener;
import com.rg.mchampgold.databinding.RecyclerItemPlayerBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayerItemAdapter extends RecyclerView.Adapter<PlayerItemAdapter.ViewHolder> {

    private List<Player> mainPlayerList;
    private List<Player> playerTypeList;
    public static PlayerItemClickListener listener;
    Context context;
    private int type;

    private static int WK = 1;
    private static int BAT = 2;
    private static int AR = 3;
    private static int BOWLER = 4;

    class ViewHolder extends RecyclerView.ViewHolder {

        final RecyclerItemPlayerBinding binding;

        ViewHolder(RecyclerItemPlayerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public PlayerItemAdapter(Context context, ArrayList<Player> mainPlayerList, List<Player> playerTypeList, PlayerItemClickListener listener, int type) {
        this.mainPlayerList = mainPlayerList;
        this.playerTypeList = playerTypeList;
        this.listener = listener;
        this.context = context;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemPlayerBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_player,
                        parent, false);
        return new ViewHolder(binding);
    }


    public void sortWithPoints(boolean flag) {
        if (flag) {
            Collections.sort(playerTypeList, (contest, t1) -> Double.valueOf(contest.getSeriesPoints()).compareTo(t1.getSeriesPoints()));
        } else {
            Collections.sort(playerTypeList, (contest, t1) -> Double.valueOf(t1.getSeriesPoints()).compareTo(contest.getSeriesPoints()));
        }
        notifyDataSetChanged();
    }


    public void sortWithCredit(boolean flag) {
        if (flag) {
            Collections.sort(playerTypeList, (contest, t1) -> Double.valueOf(contest.getCredit()).compareTo(t1.getCredit()));
        } else {
            Collections.sort(playerTypeList, (contest, t1) -> Double.valueOf(t1.getCredit()).compareTo(contest.getCredit()));
        }
        notifyDataSetChanged();
    }

    public void sortByPlayer(boolean flag) {
        if (flag) {
            Collections.sort(playerTypeList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
        } else {
            Collections.sort(playerTypeList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
        }
        notifyDataSetChanged();
    }


    public void sortWithSelectedBy(boolean flag) {
        if (flag) {
            Collections.sort(playerTypeList, (contest, t1) -> Double.valueOf(contest.getSelectedBy()).compareTo(Double.valueOf(t1.getSelectedBy())));
        } else {
            Collections.sort(playerTypeList, (contest, t1) -> Double.valueOf(t1.getSelectedBy()).compareTo(Double.valueOf(contest.getSelectedBy())));
        }
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setPlayer(playerTypeList.get(position));
        AppUtils.loadPlayerImage(holder.binding.ivPlayer, playerTypeList.get(position).getImage());

        /*if (((CreateTeamActivity) context).playerStatus.equals(Constants.PLAYING)) {
            if (playerTypeList.get(position).getIs_playing() == 1) {
                holder.binding.llBackground.setVisibility(View.VISIBLE);
            } else {
                holder.binding.llBackground.setVisibility(View.GONE);

            }
        } else if (((CreateTeamActivity) context).playerStatus.equals(Constants.NOT_PLAYING)) {
            if (playerTypeList.get(position).getIs_playing() != 1) {
                holder.binding.llBackground.setVisibility(View.VISIBLE);
            } else {
                holder.binding.llBackground.setVisibility(View.GONE);
            }
        } else {
            holder.binding.llBackground.setVisibility(View.VISIBLE);
        }*/

        holder.itemView.setOnClickListener(v -> {
            MyApplication.playerItemAdapter = this;
            if (playerTypeList.get(position).isSelected())
                listener.onPlayerClick(false, position, type);
            else
                listener.onPlayerClick(true, position, type);
        });


        /*
        holder.binding.toggleButton.setOnClickListener(view -> {
            if(playerTypeList.get(position).isSelected())
                listener.onPlayerClick(false,position,type);
            else
                listener.onPlayerClick(true,position,type);
        });

       */

      /*  holder.itemView.setOnClickListener(v -> {
            if (playerTypeList.get(position).isSelected())
                listener.onPlayerClick(false, position, type);
            else
                listener.onPlayerClick(true, position, type);
        });*/

        //   holder.binding.tvSel.setText("Sel by "+ String.format("%.1f",Double.parseDouble(playerTypeList.get(position).getSelectedBy()))+"%");


        if (playerTypeList.get(position).isSelected()) {
            holder.binding.ivSelected.setImageResource(R.drawable.ic_remove_player);
            holder.binding.llBackground.setBackgroundColor(ContextCompat.getColor(context, R.color.team_selected_color2));


            /*holder.binding.tvName.setTextColor(Color.parseColor("#ffffff"));
            holder.binding.tvPoints.setTextColor(Color.parseColor("#ffffff"));
            holder.binding.tvCredits.setTextColor(Color.parseColor("#ffffff"));
            holder.binding.tvPlayerName.setTextColor(Color.parseColor("#ffffff"));
            holder.binding.tvSel.setTextColor(Color.parseColor("#ffffff"));*/
        } else {
            holder.binding.ivSelected.setImageResource(R.drawable.ic_add_player);
            holder.binding.llBackground.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));

            /*holder.binding.tvName.setTextColor(context.getResources().getColor(R.color.match_title_color));
            holder.binding.tvPoints.setTextColor(context.getResources().getColor(R.color.match_title_color));
            holder.binding.tvCredits.setTextColor(context.getResources().getColor(R.color.match_title_color));
            holder.binding.tvPlayerName.setTextColor(context.getResources().getColor(R.color.match_title_color));
            holder.binding.tvSel.setTextColor(context.getResources().getColor(R.color.match_title_color));*/

        }

        if (playerTypeList.get(position).getIs_playing_show() == 1) {
            holder.binding.isPNpLayout.setVisibility(View.VISIBLE);
            if (playerTypeList.get(position).getIs_playing() == 1) {
                holder.binding.isPlayingView.setVisibility(View.VISIBLE);
                holder.binding.isNotPlayingView.setVisibility(View.GONE);
                holder.binding.tvPlayingNotPlaying.setText("Playing");
                holder.binding.tvPlayingNotPlaying.setTextColor(context.getResources().getColor(R.color.color_green));
            } else {
                holder.binding.isPlayingView.setVisibility(View.GONE);
                holder.binding.isNotPlayingView.setVisibility(View.VISIBLE);
                holder.binding.tvPlayingNotPlaying.setText("Not Playing");
                holder.binding.tvPlayingNotPlaying.setTextColor(context.getResources().getColor(R.color.color_red));
            }
        } else {
            holder.binding.isPNpLayout.setVisibility(View.GONE);
        }

        if (playerTypeList.get(position).getLast_match() == 1) {
            holder.binding.isLastLayout.setVisibility(View.VISIBLE);
            holder.binding.lastmatchTxt.setText(playerTypeList.get(position).getLast_match_text());
            holder.binding.lastmatchTxt.setTextColor(Color.parseColor("#c29f5d"));
        } else {
            holder.binding.isLastLayout.setVisibility(View.GONE);
        }

        holder.binding.ivPlayerInfo.setOnClickListener(v -> holder.binding.ivPlayer.performClick());
        holder.binding.ivPlayer.setOnClickListener(view -> {
            MyApplication.playerItemAdapter = this;
            if (context instanceof CreateTeamActivity)
                ((CreateTeamActivity) context).openPlayerInfoActivity(playerTypeList.get(position).getId() + "",
                        playerTypeList.get(position).getName(), playerTypeList.get(position).getTeam(), playerTypeList.get(position).getImage(),
                        playerTypeList.get(position).isSelected(), position, type);
        });

        //    holder.binding.playerInfo.setOnClickListener(view -> holder.binding.ivPlayer.performClick());


        SelectedPlayer selectedPlayer = ((CreateTeamActivity) context).selectedPlayer;
        if (playerTypeList.get(position).getTeam().equals("team1")) {

            holder.binding.tvName.setBackgroundResource(R.drawable.round_black_box);
            holder.binding.tvName.setTextColor(context.getResources().getColor(R.color.white));
            if (selectedPlayer.getLocalTeamplayerCount() == ((CreateTeamActivity) context).maxTeamPlayerCount) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else {
                checkList(holder, position, selectedPlayer);
            }

        } else if (playerTypeList.get(position).getTeam().equals("team2")) {

            holder.binding.tvName.setBackgroundResource(R.drawable.black_border);
            holder.binding.tvName.setTextColor(context.getResources().getColor(R.color.black));
            if (selectedPlayer.getVisitorTeamPlayerCount() == ((CreateTeamActivity) context).maxTeamPlayerCount) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else {
                checkList(holder, position, selectedPlayer);
            }
        }


       /* holder.itemView.setOnClickListener(v -> {
            MyApplication.playerItemAdapter=this;
            if (playerTypeList.get(position).isSelected())
                listener.onPlayerClick(false, position, type);
            else
                listener.onPlayerClick(true, position, type);
        });*/

        holder.binding.selectedBy.setText("Sel by " + playerTypeList.get(position).getSelectedBy() + "%");
        holder.binding.executePendingBindings();
    }


    @Override
    public int getItemCount() {
        return playerTypeList.size();
    }

    void checkList(ViewHolder holder, int position, SelectedPlayer selectedPlayer) {
        if (type == WK) {
            if (selectedPlayer.getWk_selected() == selectedPlayer.getWk_max_count()) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (selectedPlayer.getWk_selected() >= selectedPlayer.getWk_min_count() && selectedPlayer.getExtra_player() == 0) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (((CreateTeamActivity) context).exeedCredit) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(1.0f);
                else if (100 - selectedPlayer.getTotal_credit() >= selectedPlayer.getTotal_credit() + playerTypeList.get(position).getCredit())
                    holder.binding.llBackground.setAlpha(1.0f);
                else
                    holder.binding.llBackground.setAlpha(0.3f);
            } else if (playerTypeList.get(position).getCredit() > (100 - selectedPlayer.getTotal_credit())) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else {
                holder.binding.llBackground.setAlpha(1.0f);
            }
        } else if (type == AR) {
            if (selectedPlayer.getAr_selected() == selectedPlayer.getAr_maxcount()) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (selectedPlayer.getAr_selected() >= selectedPlayer.getAr_mincount() && selectedPlayer.getExtra_player() == 0) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (((CreateTeamActivity) context).exeedCredit) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(1.0f);
                else if (100 - selectedPlayer.getTotal_credit() >= selectedPlayer.getTotal_credit() + playerTypeList.get(position).getCredit())
                    holder.binding.llBackground.setAlpha(1.0f);
                else
                    holder.binding.llBackground.setAlpha(0.3f);
            } else if (playerTypeList.get(position).getCredit() > (100 - selectedPlayer.getTotal_credit())) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else {
                holder.binding.llBackground.setAlpha(1.0f);
            }
        } else if (type == BAT) {
            if (selectedPlayer.getBat_selected() == selectedPlayer.getBat_maxcount()) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (selectedPlayer.getBat_selected() >= selectedPlayer.getBat_mincount() && selectedPlayer.getExtra_player() == 0) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (/*(mContext as ChooseTeamActivity).*/((CreateTeamActivity) context).exeedCredit) {
                if (playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(1.0f);
                else if (100 - selectedPlayer.getTotal_credit() >= selectedPlayer.getTotal_credit() + playerTypeList.get(position).getCredit())
                    holder.binding.llBackground.setAlpha(1.0f);
                else
                    holder.binding.llBackground.setAlpha(0.3f);
            } else if (playerTypeList.get(position).getCredit() > (100 - selectedPlayer.getTotal_credit())) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else {
                holder.binding.llBackground.setAlpha(1.0f);
            }
        } else if (type == BOWLER) {
            if (selectedPlayer.getBowl_selected() == selectedPlayer.getBowl_maxcount()) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (selectedPlayer.getBowl_selected() >= selectedPlayer.getBowl_mincount() && selectedPlayer.getExtra_player() == 0) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else if (/*(mContext as ChooseTeamActivity).*/((CreateTeamActivity) context).exeedCredit) {
                if (playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(1.0f);
                else if (100 - selectedPlayer.getTotal_credit() >= selectedPlayer.getTotal_credit() + playerTypeList.get(position).getCredit())
                    holder.binding.llBackground.setAlpha(1.0f);
                else
                    holder.binding.llBackground.setAlpha(0.3f);
            } else if (playerTypeList.get(position).getCredit() > (100 - selectedPlayer.getTotal_credit())) {
                if (!playerTypeList.get(position).isSelected())
                    holder.binding.llBackground.setAlpha(0.3f);
                else
                    holder.binding.llBackground.setAlpha(1.0f);
            } else {
                holder.binding.llBackground.setAlpha(1.0f);
            }
        }
    }

    public void updateData(ArrayList<Player> playerTypeList, int type) {
        this.playerTypeList = playerTypeList;
        this.type = type;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}