package com.rg.mchampgold.app.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.WinnerScoreCardItem;

import java.util.ArrayList;
public class PriceItemAdapter extends BaseAdapter {

    Context context;
    ArrayList<WinnerScoreCardItem> list;

    public PriceItemAdapter(Context context,ArrayList<WinnerScoreCardItem> list){
        this.context=context;
        this.list= list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v;
        TextView rank,price;

        LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v= inflater.inflate(R.layout.recycler_item_price_card,null);

        rank= v.findViewById(R.id.rank);
        price= v.findViewById(R.id.price);
        rank.setText("Rank "+list.get(i).getStartPosition());
        price.setText("₹ "+list.get(i).getPrice());

        return v;
    }

}