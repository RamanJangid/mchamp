package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class TeamPointResponseItem {

	@SerializedName("bowler")
	private List<Player> bowler;

	@SerializedName("keeper")
	private List<Player> keeper;

	@SerializedName("allrounder")
	private List<Player> allrounder;

	@SerializedName("batsman")
	private List<Player> batsman;

	@SerializedName("teamname")
	private String teamname;

	@SerializedName("teamnumber")
	private int teamnumber;

	@SerializedName("points")
	private double points;

	@SerializedName("status")
	private int status;


	@SerializedName("Forward")
	private List<Player> forwardList;


	@SerializedName("Goalkeeper")
	private List<Player> goalKeeperList;


	@SerializedName("Midfielder")
	private List<Player> midfielderList;

	@SerializedName("Defender")
	private List<Player> defenderList;


	@SerializedName("Point guard")
	private List<Player> pgList;

	@SerializedName("Shooting guard")
	private List<Player> sgList;

	@SerializedName("Small forward")
	private List<Player> smallForwardList;

	@SerializedName("Power forward")
	private List<Player> powerForwardList;

	@SerializedName("Center")
	private List<Player> centreList;






	public void setTeamname(String teamname){
		this.teamname = teamname;
	}

	public String getTeamname(){
		return teamname;
	}

	public void setTeamnumber(int teamnumber){
		this.teamnumber = teamnumber;
	}

	public int getTeamnumber(){
		return teamnumber;
	}

	public void setPoints(double points){
		this.points = points;
	}

	public double getPoints(){
		return points;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}


	public List<Player> getBowler() {
		return bowler == null?new ArrayList<>():bowler;
	}

	public void setBowler(List<Player> bowler) {
		this.bowler = bowler;
	}

	public List<Player> getKeeper() {
		return keeper == null?new ArrayList<>():keeper;
	}

	public void setKeeper(List<Player> keeper) {
		this.keeper = keeper;
	}

	public List<Player> getAllrounder() {
		return allrounder == null?new ArrayList<>():allrounder;

	}

	public void setAllrounder(List<Player> allrounder) {
		this.allrounder = allrounder;
	}

	public List<Player> getBatsman() {
		return batsman == null?new ArrayList<>():batsman;
	}

	public void setBatsman(List<Player> batsman) {
		this.batsman = batsman;
	}

	public List<Player> getForwardList() {
		return forwardList == null?new ArrayList<>():forwardList;
	}

	public void setForwardList(List<Player> forwardList) {
		this.forwardList = forwardList;
	}

	public List<Player> getGoalKeeperList() {
		return goalKeeperList == null?new ArrayList<>():goalKeeperList;
	}

	public void setGoalKeeperList(List<Player> goalKeeperList) {
		this.goalKeeperList = goalKeeperList;
	}

	public List<Player> getMidfielderList() {
		return midfielderList == null?new ArrayList<>():midfielderList;
	}

	public void setMidfielderList(List<Player> midfielderList) {
		this.midfielderList = midfielderList;
	}

	public List<Player> getDefenderList() {
		return defenderList == null?new ArrayList<>():defenderList;
	}

	public void setDefenderList(List<Player> defenderList) {
		this.defenderList = defenderList;
	}


	public List<Player> getPgList() {
		return pgList == null?new ArrayList<>():pgList;
	}

	public void setPgList(List<Player> pgList) {
		this.pgList = pgList;
	}

	public List<Player> getSgList() {
		return sgList == null?new ArrayList<>():sgList;
	}

	public void setSgList(List<Player> sgList) {
		this.sgList = sgList;
	}

	public List<Player> getSmallForwardList() {
		return smallForwardList == null?new ArrayList<>():smallForwardList;
	}

	public void setSmallForwardList(List<Player> smallForwardList) {
		this.smallForwardList = smallForwardList;
	}

	public List<Player> getPowerForwardList() {
		return powerForwardList == null?new ArrayList<>():powerForwardList;
	}

	public void setPowerForwardList(List<Player> powerForwardList) {
		this.powerForwardList = powerForwardList;
	}

	public List<Player> getCentreList() {
		return centreList == null?new ArrayList<>():centreList;
	}

	public void setCentreList(List<Player> centreList) {
		this.centreList = centreList;
	}
}