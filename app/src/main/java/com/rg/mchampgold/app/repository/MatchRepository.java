package com.rg.mchampgold.app.repository;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.request.CreateTeamRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.api.service.UserRestService;
import com.rg.mchampgold.app.dataModel.BalanceResponse;
import com.rg.mchampgold.app.dataModel.ContestDetailResponse;
import com.rg.mchampgold.app.dataModel.ContestResponse;
import com.rg.mchampgold.app.dataModel.CreateTeamResponse;
import com.rg.mchampgold.app.dataModel.JoinContestResponse;
import com.rg.mchampgold.app.dataModel.JoinedContestResponse;
import com.rg.mchampgold.app.dataModel.MatchListResponse;
import com.rg.mchampgold.app.dataModel.MyTeamResponse;
import com.rg.mchampgold.app.dataModel.PlayerListResponse;
import com.rg.mchampgold.app.dataModel.PlayerPointsResponse;
import com.rg.mchampgold.app.dataModel.RefreshScoreResponse;
import com.rg.mchampgold.common.api.ApiResponse;
import com.rg.mchampgold.common.api.NetworkBoundResource;
import com.rg.mchampgold.common.api.Resource;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MatchRepository {
    final private UserRestService userRestService;

    private final MutableLiveData<MatchListResponse> matchListResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<MatchListResponse> myMatchListResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ContestResponse> contestResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<ContestDetailResponse> contestDetailsResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<RefreshScoreResponse> refreshScoreResponseMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<PlayerPointsResponse> playerPointsResponseMutableLiveData = new MutableLiveData<>();


    @Inject
    public MatchRepository(UserRestService api) {
        this.userRestService = api;
    }

    //Get MatchList
    public LiveData<Resource<MatchListResponse>> getSearchItems(BaseRequest baseRequest) {
        LiveData<Resource<MatchListResponse>> liveDataMatchList = new NetworkBoundResource<MatchListResponse, MatchListResponse>() {

            @Override
            protected void saveCallResult(@NonNull MatchListResponse item) {
                matchListResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable MatchListResponse item) {
                if (matchListResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<MatchListResponse> loadFromDb() {
                matchListResponseMutableLiveData.setValue(matchListResponseMutableLiveData.getValue());
                return matchListResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<MatchListResponse>> createCall() {
                return userRestService.getMatchList(baseRequest);
            }
        }.getAsLiveData();

        return liveDataMatchList;
    }

    //GetContest
    public LiveData<Resource<ContestResponse>> getContestList(ContestRequest contestRequest) {
        LiveData<Resource<ContestResponse>> liveDataContest = new NetworkBoundResource<ContestResponse, ContestResponse>() {

            @Override
            protected void saveCallResult(@NonNull ContestResponse item) {
                contestResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable ContestResponse item) {
                if (contestResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<ContestResponse> loadFromDb() {
                contestResponseMutableLiveData.setValue(contestResponseMutableLiveData.getValue());
                return contestResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<ContestResponse>> createCall() {
                return userRestService.getContest(contestRequest);
            }
        }.getAsLiveData();

        return liveDataContest;
    }



    //GetMyTeams
    private final MutableLiveData<MyTeamResponse> myTeamResponseMutableLiveData = new MutableLiveData<>();
    public LiveData<Resource<MyTeamResponse>> getTeams(MyTeamRequest teamRequest) {
        LiveData<Resource<MyTeamResponse>> liveDataMyTeam = new NetworkBoundResource<MyTeamResponse, MyTeamResponse>() {

            @Override
            protected void saveCallResult(@NonNull MyTeamResponse item) {
                myTeamResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable MyTeamResponse item) {
                if (myTeamResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<MyTeamResponse> loadFromDb() {
                myTeamResponseMutableLiveData.setValue(myTeamResponseMutableLiveData.getValue());
                return myTeamResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<MyTeamResponse>> createCall() {
                return userRestService.getMyTeams(teamRequest);
            }
        }.getAsLiveData();

        return liveDataMyTeam;
    }



    //GetPlayerList
    private final MutableLiveData<PlayerListResponse> playerListResponseMutableLiveData = new MutableLiveData<>();
    public LiveData<Resource<PlayerListResponse>> getPlayerList(MyTeamRequest teamRequest) {
        LiveData<Resource<PlayerListResponse>> liveDataPlayerList = new NetworkBoundResource<PlayerListResponse, PlayerListResponse>() {

            @Override
            protected void saveCallResult(@NonNull PlayerListResponse item) {
                playerListResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable PlayerListResponse item) {
                if (playerListResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<PlayerListResponse> loadFromDb() {
                playerListResponseMutableLiveData.setValue(playerListResponseMutableLiveData.getValue());
                return playerListResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<PlayerListResponse>> createCall() {
                return userRestService.getPlayerList(teamRequest);
            }
        }.getAsLiveData();

        return liveDataPlayerList;
    }




    private final MutableLiveData<CreateTeamResponse> createTeamResponseMutableLiveData = new MutableLiveData<>();
    public LiveData<Resource<CreateTeamResponse>> createTeam(CreateTeamRequest createTeamRequest) {
        LiveData<Resource<CreateTeamResponse>> liveDataCreateTeam = new NetworkBoundResource<CreateTeamResponse, CreateTeamResponse>() {

            @Override
            protected void saveCallResult(@NonNull CreateTeamResponse item) {
                createTeamResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable CreateTeamResponse item) {
                if (createTeamResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<CreateTeamResponse> loadFromDb() {
                createTeamResponseMutableLiveData.setValue(createTeamResponseMutableLiveData.getValue());
                return createTeamResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<CreateTeamResponse>> createCall() {
                return userRestService.createTeam(createTeamRequest);
            }
        }.getAsLiveData();

        return liveDataCreateTeam;
    }


    public LiveData<Resource<MatchListResponse>> getMyMatchesList(BaseRequest baseRequest) {
        LiveData<Resource<MatchListResponse>> liveDataMyMatchesList = new NetworkBoundResource<MatchListResponse, MatchListResponse>() {

            @Override
            protected void saveCallResult(@NonNull MatchListResponse item) {
                myMatchListResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable MatchListResponse item) {
                if (myMatchListResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<MatchListResponse> loadFromDb() {
                myMatchListResponseMutableLiveData.setValue(myMatchListResponseMutableLiveData.getValue());
                return myMatchListResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<MatchListResponse>> createCall() {
                return userRestService.getMyMatchList(baseRequest);
            }
        }.getAsLiveData();

        return liveDataMyMatchesList;
    }


    public LiveData<Resource<ContestDetailResponse>> getContestDetails(ContestRequest contestRequest) {
        LiveData<Resource<ContestDetailResponse>> liveDataContestDetails = new NetworkBoundResource<ContestDetailResponse, ContestDetailResponse>() {

            @Override
            protected void saveCallResult(@NonNull ContestDetailResponse item) {
                contestDetailsResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable ContestDetailResponse item) {
                if (contestDetailsResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<ContestDetailResponse> loadFromDb() {
                contestDetailsResponseMutableLiveData.setValue(contestDetailsResponseMutableLiveData.getValue());
                return contestDetailsResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<ContestDetailResponse>> createCall() {
                if(contestRequest.isShowLeaderBoard())
                    return userRestService.getLeaderBoard(contestRequest);
                else
                    return userRestService.getContestDetails(contestRequest);
            }
        }.getAsLiveData();

        return liveDataContestDetails;
    }


    private final MutableLiveData<BalanceResponse> balanceResponseMutableLiveData = new MutableLiveData<>();
    public LiveData<Resource<BalanceResponse>> getUsableBalance(JoinContestRequest joinContestRequest) {
        LiveData<Resource<BalanceResponse>> liveDataBalanceResponse = new NetworkBoundResource<BalanceResponse, BalanceResponse>() {

            @Override
            protected void saveCallResult(@NonNull BalanceResponse item) {
                balanceResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable BalanceResponse item) {
                if (contestDetailsResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<BalanceResponse> loadFromDb() {
                balanceResponseMutableLiveData.setValue(balanceResponseMutableLiveData.getValue());
                return balanceResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<BalanceResponse>> createCall() {
                return userRestService.getUsableBalance(joinContestRequest);
            }
        }.getAsLiveData();

        return liveDataBalanceResponse;
    }


    private final MutableLiveData<JoinContestResponse> joinContestResponseMutableLiveData = new MutableLiveData<>();
    public LiveData<Resource<JoinContestResponse>> joinContest(JoinContestRequest joinContestRequest) {
        LiveData<Resource<JoinContestResponse>> liveDataJoinContest = new NetworkBoundResource<JoinContestResponse, JoinContestResponse>() {

            @Override
            protected void saveCallResult(@NonNull JoinContestResponse item) {
                joinContestResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable JoinContestResponse item) {
                if (joinContestResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<JoinContestResponse> loadFromDb() {
                joinContestResponseMutableLiveData.setValue(joinContestResponseMutableLiveData.getValue());
                return joinContestResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<JoinContestResponse>> createCall() {
                return userRestService.joinContest(joinContestRequest);
            }
        }.getAsLiveData();

        return liveDataJoinContest;
    }


    private final MutableLiveData<JoinedContestResponse> joinedContestResponseMutableLiveData = new MutableLiveData<>();
    public LiveData<Resource<JoinedContestResponse>> joinedContestList(JoinContestRequest joinContestRequest) {
        LiveData<Resource<JoinedContestResponse>> liveDataJoinContest = new NetworkBoundResource<JoinedContestResponse, JoinedContestResponse>() {

            @Override
            protected void saveCallResult(@NonNull JoinedContestResponse item) {
                joinedContestResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable JoinedContestResponse item) {
                if (joinedContestResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<JoinedContestResponse> loadFromDb() {
                joinedContestResponseMutableLiveData.setValue(joinedContestResponseMutableLiveData.getValue());
                return joinedContestResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<JoinedContestResponse>> createCall() {
                return userRestService.joinedContestList(joinContestRequest);
            }
        }.getAsLiveData();

        return liveDataJoinContest;
    }




    //GetContest
    public LiveData<Resource<RefreshScoreResponse>> getRefreshScore(ContestRequest contestRequest) {
        LiveData<Resource<RefreshScoreResponse>> liveDataContest = new NetworkBoundResource<RefreshScoreResponse, RefreshScoreResponse>() {

            @Override
            protected void saveCallResult(@NonNull RefreshScoreResponse item) {
                refreshScoreResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable RefreshScoreResponse item) {
                if (refreshScoreResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<RefreshScoreResponse> loadFromDb() {
                refreshScoreResponseMutableLiveData.setValue(refreshScoreResponseMutableLiveData.getValue());
                return refreshScoreResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<RefreshScoreResponse>> createCall() {
                return userRestService.refreshScore(contestRequest);
            }
        }.getAsLiveData();

        return liveDataContest;
    }


    //Player Points
    public LiveData<Resource<PlayerPointsResponse>> getPlayerPoints(ContestRequest contestRequest) {
        LiveData<Resource<PlayerPointsResponse>> liveDataPlayerPoints = new NetworkBoundResource<PlayerPointsResponse, PlayerPointsResponse>() {

            @Override
            protected void saveCallResult(@NonNull PlayerPointsResponse item) {
                playerPointsResponseMutableLiveData.postValue(item);
            }

            @Override
            protected boolean shouldFetch(@Nullable PlayerPointsResponse item) {
                if (playerPointsResponseMutableLiveData.getValue() != null) {
                    return true;
                }
                return true;
            }

            @NonNull
            @Override
            protected LiveData<PlayerPointsResponse> loadFromDb() {
                playerPointsResponseMutableLiveData.setValue(playerPointsResponseMutableLiveData.getValue());
                return playerPointsResponseMutableLiveData;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<PlayerPointsResponse>> createCall() {
                return userRestService.getPlayerPoints(contestRequest);
            }
        }.getAsLiveData();

        return liveDataPlayerPoints;
    }

}





