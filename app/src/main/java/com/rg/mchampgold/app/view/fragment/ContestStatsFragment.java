package com.rg.mchampgold.app.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.dataModel.MultiSportsPlayerPointItem;
import com.rg.mchampgold.app.view.activity.UpComingContestDetailActivity;
import com.rg.mchampgold.app.view.adapter.ContestStatsAdapter;
import com.rg.mchampgold.app.viewModel.ContestViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentContestStatsBinding;

import java.util.ArrayList;

public class ContestStatsFragment extends Fragment {

    private FragmentContestStatsBinding fragmentContestStatsBinding;
    private ContestViewModel contestViewModel;
    private ContestStatsAdapter contestStatsAdapter;
    private ArrayList<MultiSportsPlayerPointItem> list = new ArrayList<>();
    private boolean isSelectedBySort = false;
    private boolean isPointsSort = false;
    private boolean isCaptainBy = false;
    private String matchKey = "", sportKey = "", contestId = "";

    public static Fragment newInstance(String matchKey, String contestId, String sportKey) {
        ContestStatsFragment contestStatsFragment = new ContestStatsFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KEY_MATCH_KEY, matchKey);
        args.putString(Constants.CONTEST_ID, contestId);
        args.putString("sportKey",sportKey);
        contestStatsFragment.setArguments(args);
        return contestStatsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contestViewModel = ContestViewModel.create(this);
        MyApplication.getAppComponent().inject(contestViewModel);
        MyApplication.getAppComponent().inject(this);

        if (getArguments() != null) {
            contestId = getArguments().getString(Constants.CONTEST_ID);
            matchKey = getArguments().getString(Constants.KEY_MATCH_KEY);
            sportKey = getArguments().getString("sportKey");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentContestStatsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_contest_stats, container, false);
        intialize();
        return fragmentContestStatsBinding.getRoot();
    }

    private void intialize() {

        fragmentContestStatsBinding.rlPointsSort.setOnClickListener(v -> {
            if (!isPointsSort) {
                fragmentContestStatsBinding.ivPointsSort.setImageResource(R.drawable.ic_down_sort);
                isPointsSort = true;
                contestStatsAdapter.sortWithPoints(true);
            } else {
                fragmentContestStatsBinding.ivPointsSort.setImageResource(R.drawable.ic_up_sort);
                isPointsSort = false;
                contestStatsAdapter.sortWithPoints(false);
            }

            fragmentContestStatsBinding.ivPointsSort.setVisibility(View.VISIBLE);
            fragmentContestStatsBinding.ivSelSort.setVisibility(View.INVISIBLE);
            fragmentContestStatsBinding.ivCSort.setVisibility(View.INVISIBLE);
        });

        fragmentContestStatsBinding.rlSelSort.setOnClickListener(v -> {
            if (!isSelectedBySort) {
                fragmentContestStatsBinding.ivSelSort.setImageResource(R.drawable.ic_down_sort);
                isSelectedBySort = true;
                contestStatsAdapter.sortWithSelectedBy(true);
            } else {
                fragmentContestStatsBinding.ivSelSort.setImageResource(R.drawable.ic_up_sort);
                isSelectedBySort = false;
                contestStatsAdapter.sortWithSelectedBy(false);
            }

            fragmentContestStatsBinding.ivPointsSort.setVisibility(View.INVISIBLE);
            fragmentContestStatsBinding.ivSelSort.setVisibility(View.VISIBLE);
            fragmentContestStatsBinding.ivCSort.setVisibility(View.INVISIBLE);
        });

        fragmentContestStatsBinding.rlCSort.setOnClickListener(v -> {
            if (!isCaptainBy) {
                fragmentContestStatsBinding.ivCSort.setImageResource(R.drawable.ic_down_sort);
                isCaptainBy = true;
                contestStatsAdapter.sortWithCaptaindBy(true);
            } else {
                fragmentContestStatsBinding.ivCSort.setImageResource(R.drawable.ic_up_sort);
                isCaptainBy = false;
                contestStatsAdapter.sortWithCaptaindBy(false);
            }

            fragmentContestStatsBinding.ivPointsSort.setVisibility(View.INVISIBLE);
            fragmentContestStatsBinding.ivSelSort.setVisibility(View.INVISIBLE);
            fragmentContestStatsBinding.ivCSort.setVisibility(View.VISIBLE);
        });

        setUpRecyclerView();
        getContestStats();
    }

    private void getContestStats() {
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        contestViewModel.loadPlayerPointRequest(request);
        contestViewModel.getPlayerPoints().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    fragmentContestStatsBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    fragmentContestStatsBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    fragmentContestStatsBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1) {
                        list = arrayListResource.getData().getResult();
                        contestStatsAdapter.updateData(list);
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }

    private void setUpRecyclerView() {
        contestStatsAdapter = new ContestStatsAdapter((UpComingContestDetailActivity) getActivity(), list);
        fragmentContestStatsBinding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmentContestStatsBinding.recyclerView.setHasFixedSize(true);
        fragmentContestStatsBinding.recyclerView.setAdapter(contestStatsAdapter);
    }
}
