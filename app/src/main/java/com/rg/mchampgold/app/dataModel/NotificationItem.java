package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Generated("com.robohorse.robopojogenerator")
public class NotificationItem{

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("title")
	private String title;

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	@Override
 	public String toString(){
		return 
			"NotificationItem{" + 
			"created_at = '" + createdAt + '\'' + 
			",title = '" + title + '\'' + 
			"}";
		}


	public String dayWithMonth() {
		String string = createdAt;
		Date date1 = null;
		try {

			date1=new SimpleDateFormat("yyyy-MM-dd").parse(string);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd MMMM");

			return simpleDateFormat.format(date1);
		} catch (ParseException e) {
			return "";
		}
	}
}