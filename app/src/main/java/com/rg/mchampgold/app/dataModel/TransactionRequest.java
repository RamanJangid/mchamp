package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class TransactionRequest{

	@SerializedName("user_id")
	private String userId;

	@SerializedName("page")
	private int page;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@Override
 	public String toString(){
		return 
			"TransactionRequest{" + 
			"user_id = '" + userId + '\'' + 
			",page = '" + page + '\'' + 
			"}";
		}
}