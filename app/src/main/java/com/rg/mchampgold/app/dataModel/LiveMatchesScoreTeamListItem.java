package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class LiveMatchesScoreTeamListItem {
    @SerializedName("innings")
    private ArrayList<LiveMatchesScoreInningListItem> innings;

    public ArrayList<LiveMatchesScoreInningListItem> getInnings() {
        return innings;
    }

    public void setInnings(ArrayList<LiveMatchesScoreInningListItem> innings) {
        this.innings = innings;
    }
}
