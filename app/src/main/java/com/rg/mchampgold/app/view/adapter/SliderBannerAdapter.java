package com.rg.mchampgold.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.PagerAdapter;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.BannerListItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.AddBalanceActivity;
import com.rg.mchampgold.app.view.activity.HomeActivity;
import com.rg.mchampgold.app.view.activity.InviteFriendActivity;
import com.rg.mchampgold.databinding.LayoutSliderImageBinding;


import java.util.List;

public class SliderBannerAdapter extends PagerAdapter {

    private List<BannerListItem> urls;
    private Context context;
    private LayoutInflater inflater;

    public SliderBannerAdapter(Context context, List<BannerListItem> urls) {
        this.urls = urls;
        this.context = context;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutSliderImageBinding layoutSliderImageBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_slider_image, container, false);
        AppUtils.loadImageBanner(layoutSliderImageBinding.ivImage, urls.get(position).getImage());

        layoutSliderImageBinding.ivImage.setOnClickListener(view -> {
            if (!urls.get(position).getLink().equalsIgnoreCase("")) {
                openUrl(urls.get(position).getLink());
            } else {
                if (urls.get(position).getType().equalsIgnoreCase("addcash")) {
                    if (MyApplication.walletClickListener != null) {
                        MyApplication.walletClickListener.onFantasyWalletClick();
                    }
                } else if (urls.get(position).getType().equalsIgnoreCase("refer")) {
                    context.startActivity(new Intent(context, InviteFriendActivity.class));
                } else if (MyApplication.refer_earn.equalsIgnoreCase("0")) {
                    context.startActivity(new Intent(context, InviteFriendActivity.class));
                }
            }
        });

        container.addView(layoutSliderImageBinding.getRoot());
        return layoutSliderImageBinding.getRoot();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    private int getWidthInPix() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((HomeActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        return width;
    }


 /*   public getHeightinPix() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
    }*/

    private void openUrl(String url) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    public static int dpToPx(float dp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

}


