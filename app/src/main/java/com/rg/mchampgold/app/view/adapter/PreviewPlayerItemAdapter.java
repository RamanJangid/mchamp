package com.rg.mchampgold.app.view.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.MultiSportsPlayerPointItem;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.PlayerPointsResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.BreakupPlayerPointsActivity;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.RecyclerPreviewPlayerItemBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

public class PreviewPlayerItemAdapter extends RecyclerView.Adapter<PreviewPlayerItemAdapter.ViewHolder> {
    @Inject
    OAuthRestService oAuthRestService;
    private List<Player> playerList;
    boolean isFromPreviewPoint;
    String matchKey;
    ArrayAdapter<String> adapter ;
    Context context;

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerPreviewPlayerItemBinding binding;
        ViewHolder(RecyclerPreviewPlayerItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding; }
    }

    public PreviewPlayerItemAdapter(Context context,boolean isFromPreviewPoint ,List<Player> playerList,String matchKey) {
        MyApplication.getAppComponent().inject(this);
        this.playerList = playerList;
        this.matchKey = matchKey;
        this.context = context;
        this.isFromPreviewPoint = isFromPreviewPoint;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerPreviewPlayerItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_preview_player_item,
                        parent, false);
        /*float width = parent.getWidth()/playerList.size();*/
        binding.getRoot().getLayoutParams().width = (parent.getMeasuredWidth()/playerList.size())-12;
        return new ViewHolder(binding);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setPlayer(playerList.get(position));
        Player player = playerList.get(position);

        AppUtils.loadPreviewImage(holder.binding.playertypeImage,player.getImage());

        holder.binding.playerNametxt.setText(player.getShortName());

        DecimalFormat decimalFormat = new DecimalFormat("###.#");
        if(isFromPreviewPoint) {
            holder.binding.playerPointtxt.setText(decimalFormat.format(player.getTotalpoints())+" Pt.");
        }
        else {
            holder.binding.playerPointtxt.setText(decimalFormat.format(player.getCredit())+" Cr.");
        }

        if (player.isCaptain()) {
            holder.binding.cVcTxt.setText("C");
            holder.binding.cVcTxt.setVisibility(View.VISIBLE);
            holder.binding.cVcTxt.setBackgroundResource(R.drawable.cap_selected);
            holder.binding.cVcTxt.setTextColor(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));
        } else if (player.isVcCaptain()) {
            holder.binding.cVcTxt.setText("VC");
            holder.binding.cVcTxt.setVisibility(View.VISIBLE);
            holder.binding.cVcTxt.setBackgroundResource(R.drawable.vcap_selected);
            holder.binding.cVcTxt.setTextColor(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));
        }



        if (player.getCaptain() == 1) {
            holder.binding.cVcTxt.setText("C");
            holder.binding.cVcTxt.setVisibility(View.VISIBLE);
            holder.binding.cVcTxt.setBackgroundResource(R.drawable.cap_selected);
            holder.binding.cVcTxt.setTextColor(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));
        } else if (player.getVicecaptain() == 1) {
            holder.binding.cVcTxt.setText("VC");
            holder.binding.cVcTxt.setVisibility(View.VISIBLE);
            holder.binding.cVcTxt.setBackgroundResource(R.drawable.vcap_selected);
            holder.binding.cVcTxt.setTextColor(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));
        }


        if (!"team1".equalsIgnoreCase(player.getTeam())) {
            holder.binding.playerNametxt.setBackgroundResource(R.drawable.team2);
            holder.binding.playerNametxt.setTextColor(Color.parseColor("#FFFFFF"));
            // holder.binding.playertypeImage.setImageResource(R.drawable.player_team_two);

        } else {
            holder.binding.playerNametxt.setBackgroundResource(R.drawable.team1);
            holder.binding.playerNametxt.setTextColor(Color.parseColor("#000000"));
            //   holder.binding.playertypeImage.setImageResource(R.drawable.player_team_one);
        }
        if (playerList.get(position).getIs_playing_show() == 1) {
            holder.binding.notLead.setVisibility(View.VISIBLE);
            if (playerList.get(position).getIs_playing() == 1) {
                holder.binding.isPlayingView.setVisibility(View.VISIBLE);
                holder.binding.isNotPlayingView.setVisibility(View.GONE);
            } else {
                holder.binding.isPlayingView.setVisibility(View.GONE);
                holder.binding.isNotPlayingView.setVisibility(View.VISIBLE);
            }
        } else {
            holder.binding.notLead.setVisibility(View.GONE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPlayerPoints(String.valueOf(playerList.get(position).getId()));
            }
        });
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return playerList.size();
    }


    public void updateData(ArrayList<Player> list,String role) {
        playerList = list;
        notifyDataSetChanged();
    }

    private void showPlayerPoints(String playerId) {
        MyApplication.showLoader(context);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setMatchkey(matchKey);
        baseRequest.setSport_key(MyApplication.tinyDB.getSportKey(Constants.SPORT_KEY));
        baseRequest.setPlayer_id(playerId);
        CustomCallAdapter.CustomCall<PlayerPointsResponse> bankDetailResponseCustomCall = oAuthRestService.showPlayerPoints(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<PlayerPointsResponse>() {
            @Override
            public void success(Response<PlayerPointsResponse> response) {
                MyApplication.hideLoader();
                if (response.isSuccessful() && response.body() != null) {
                    List<MultiSportsPlayerPointItem> playerPointItems=response.body().getResult();
                    if (playerPointItems!=null) {
                        if (playerPointItems.size()>0){
                            Intent intent = new Intent(context, BreakupPlayerPointsActivity.class);
                            intent.putExtra("playerPointItem", playerPointItems.get(0).getBreakup_points());
                            intent.putExtra("playerName", playerPointItems.get(0).getPlayer_name());
                            intent.putExtra("selectedBy", playerPointItems.get(0).showSelectedBy());
                            intent.putExtra("point", playerPointItems.get(0).getPoints());
                            intent.putExtra("isSelected", playerPointItems.get(0).getIsSelected());
                            intent.putExtra("pImage", playerPointItems.get(0).getImage());
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }else {
                            AppUtils.showErrorr(((AppCompatActivity)context),response.body().getMessage());
                        }
                    }else {
                        AppUtils.showErrorr(((AppCompatActivity)context),response.body().getMessage());
                    }
                }


            }

            @Override
            public void failure(ApiException e) {
//                fragmentBankVerificationBinding.setRefreshing(false);
                MyApplication.hideLoader();
                e.printStackTrace();
            }
        });
    }


}