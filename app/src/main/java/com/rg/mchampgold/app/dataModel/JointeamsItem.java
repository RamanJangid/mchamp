package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class JointeamsItem{

	@SerializedName("teamid")
	private int teamid;

	@SerializedName("user_id")
	private int userid;

	@SerializedName("is_show")
	private boolean isShow;

	@SerializedName("teamname")
	private String teamname;

	@SerializedName("teamnumber")
	private int teamnumber;

	@SerializedName("points")
	private int points;

	public void setTeamid(int teamid){
		this.teamid = teamid;
	}

	public int getTeamid(){
		return teamid;
	}

	public void setUserid(int userid){
		this.userid = userid;
	}

	public int getUserid(){
		return userid;
	}

	public void setIsShow(boolean isShow){
		this.isShow = isShow;
	}

	public boolean isIsShow(){
		return isShow;
	}

	public void setTeamname(String teamname){
		this.teamname = teamname;
	}

	public String getTeamname(){
		return teamname;
	}

	public void setTeamnumber(int teamnumber){
		this.teamnumber = teamnumber;
	}

	public int getTeamnumber(){
		return teamnumber;
	}

	public void setPoints(int points){
		this.points = points;
	}

	public int getPoints(){
		return points;
	}

	@Override
 	public String toString(){
		return 
			"JointeamsItem{" + 
			"teamid = '" + teamid + '\'' + 
			",userid = '" + userid + '\'' + 
			",is_show = '" + isShow + '\'' + 
			",teamname = '" + teamname + '\'' + 
			",teamnumber = '" + teamnumber + '\'' + 
			",points = '" + points + '\'' + 
			"}";
		}
}