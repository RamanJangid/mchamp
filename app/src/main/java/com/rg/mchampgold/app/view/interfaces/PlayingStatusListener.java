package com.rg.mchampgold.app.view.interfaces;

public interface PlayingStatusListener {
    void isPlayingSelected(boolean isPlaying);
}
