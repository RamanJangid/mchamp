package com.rg.mchampgold.app.view.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.ReferLIstItem;
import com.rg.mchampgold.databinding.RecyclerItemInvitedFriendBinding;

import java.util.ArrayList;
import java.util.List;

public class ReferFriendItemAdapter extends RecyclerView.Adapter<ReferFriendItemAdapter.ViewHolder> {

    private List<ReferLIstItem> referLIstItemList;


    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemInvitedFriendBinding binding;

        ViewHolder(RecyclerItemInvitedFriendBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public ReferFriendItemAdapter(ArrayList<ReferLIstItem> referLIstItemList) {
        this.referLIstItemList = referLIstItemList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemInvitedFriendBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_invited_friend,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setReferListItem(referLIstItemList.get(position));
        if (referLIstItemList.get(position).getName().equals(""))
            holder.binding.tvReferName.setText(referLIstItemList.get(position).getEmail());
        else
            holder.binding.tvReferName.setText(referLIstItemList.get(position).getName());

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return referLIstItemList.size();
    }


}