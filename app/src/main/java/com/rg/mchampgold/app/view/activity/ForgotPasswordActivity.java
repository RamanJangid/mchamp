package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.NormalResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.common.utils.NetworkUtils;
import com.rg.mchampgold.databinding.ActivityForgotPasswordBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class ForgotPasswordActivity extends AppCompatActivity {

    ActivityForgotPasswordBinding mBinding;
    @Inject
    OAuthRestService oAuthRestService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        MyApplication.getAppComponent().inject(ForgotPasswordActivity.this);

        AppUtils.disableCopyPaste(mBinding.etEmail);

        setSupportActionBar(mBinding.mytoolbar);

        if (getSupportActionBar() != null) {
            // getSupportActionBar().setTitle("Forgot Password");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mBinding.submit.setOnClickListener(view -> {

            if (mBinding.etEmail.getText().toString().trim().isEmpty()) {
                AppUtils.showErrorr(ForgotPasswordActivity.this, "Please enter correct Email address.");
            } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mBinding.etEmail.getText().toString().trim()).matches()) {
                AppUtils.showErrorr(ForgotPasswordActivity.this, "Entered Email address is not correct.");
            } else {
                if (NetworkUtils.isNetworkAvailable()) {
                    forgotPassword(mBinding.etEmail.getText().toString().trim());
                } else {
                    AppUtils.showErrorr(this, getString(R.string.internet_off));
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void forgotPassword(String email) {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setEmail(email);
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<NormalResponse> bankDetailResponseCustomCall = oAuthRestService.forgotPassword(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<NormalResponse>() {
            @Override
            public void success(Response<NormalResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    NormalResponse normalResponse = response.body();
                    Toast.makeText(ForgotPasswordActivity.this, normalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    if (normalResponse.getStatus() == 1) {
                        finish();
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }
}
