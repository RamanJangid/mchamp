package com.rg.mchampgold.app.view.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.BankDetailItem;
import com.rg.mchampgold.app.dataModel.BankDetailResponse;
import com.rg.mchampgold.app.dataModel.BankVerifyRequest;
import com.rg.mchampgold.app.dataModel.BankVerifyResponse;
import com.rg.mchampgold.app.dataModel.ImageUploadResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.ProgressRequestBody;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentBankVerificationBinding;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class BankVerificationFragment extends Fragment implements ProgressRequestBody.UploadCallbacks {


    @Inject
    OAuthRestService oAuthRestService;
    Context context;
    FragmentBankVerificationBinding fragmentBankVerificationBinding;
    String state;
    String fileName = "";
    String Simage = "";
    private String fileImage = "";

    private static int GALLERY_REQUEST_CODE = 100;
    private static int CAMERA_REQUEST_CODE = 101;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = getActivity();
        fragmentBankVerificationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_bank_verification, container, false);

        AppUtils.disableCopyPaste(fragmentBankVerificationBinding.bankName);
        AppUtils.disableCopyPaste(fragmentBankVerificationBinding.etAccountholderName);
        AppUtils.disableCopyPaste(fragmentBankVerificationBinding.etAccountNumber);
        AppUtils.disableCopyPaste(fragmentBankVerificationBinding.cnfAccountNumber);
        AppUtils.disableCopyPaste(fragmentBankVerificationBinding.ifscCode);

        fragmentBankVerificationBinding.btnUpload.setOnClickListener(view -> {
            if (getActivity().checkCallingOrSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    getActivity().checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                showImageSelectionDialog();
            else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 22);
            }
        });
        fragmentBankVerificationBinding.btnSubmit.setOnClickListener(view -> validate());

        getBankDetails();

        return fragmentBankVerificationBinding.getRoot();
    }

    private void validate() {
        if (fragmentBankVerificationBinding.etAccountholderName.getText().toString().length() < 1)
            AppUtils.showErrorr((AppCompatActivity) context, "Please enter valid account holder name.");
        else if (fragmentBankVerificationBinding.etAccountNumber.getText().toString().length() < 1)
            AppUtils.showErrorr((AppCompatActivity) context, "Please enter valid account number.");
        else if (fragmentBankVerificationBinding.cnfAccountNumber.getText().toString().length() < 1)
            AppUtils.showErrorr((AppCompatActivity) context, "Please enter valid confirm account number.");
        else if (!fragmentBankVerificationBinding.etAccountNumber.getText().toString().equals(fragmentBankVerificationBinding.cnfAccountNumber.getText().toString()))
            AppUtils.showErrorr((AppCompatActivity) context, "Your account number and verify account number not matched.");
        else if (fragmentBankVerificationBinding.ifscCode.getText().toString().length() != 11)
            AppUtils.showErrorr((AppCompatActivity) context, "Please enter valid IFSC Code.");
        else if (fragmentBankVerificationBinding.bankName.getText().toString().length() < 1)
            AppUtils.showErrorr((AppCompatActivity) context, "Please enter valid bank name.");
        else {
            verifyBank();
        }
    }


    private void showImageSelectionDialog() {

        LayoutInflater inflater1 = getLayoutInflater();
        View alertLayout = inflater1.inflate(R.layout.layout_pic_upload, null);

        final TextView tvGallery = alertLayout.findViewById(R.id.tv_gallery);
        final TextView tvCamera = alertLayout.findViewById(R.id.tv_camera);
        final TextView tvCancel = alertLayout.findViewById(R.id.tv_cancel);

        /*AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(alertLayout);
        AlertDialog alert = builder.create();*/

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(alertLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        tvGallery.setOnClickListener(view1 -> {
            dialog.dismiss();
            openGallery();
        });


        tvCamera.setOnClickListener(view12 -> {
            dialog.dismiss();
            openCamera();
        });

        tvCancel.setOnClickListener(view13 -> dialog.dismiss());
        dialog.show();


    }

    private void getBankDetails() {
        fragmentBankVerificationBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<BankDetailResponse> bankDetailResponseCustomCall = oAuthRestService.getBankDetail(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<BankDetailResponse>() {
            @Override
            public void success(Response<BankDetailResponse> response) {
                fragmentBankVerificationBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    BankDetailResponse bankDetailResponse = response.body();
                    if (bankDetailResponse.getStatus() == 1 && bankDetailResponse.getResult().size() > 0) {
                        BankDetailItem bankDetailItem = bankDetailResponse.getResult().get(0);
                        if (bankDetailItem.getStatus() == 0) {
                            fragmentBankVerificationBinding.ivEmailVerified.setImageResource(R.drawable.ic_verification_bank);
                            verifiedLayout("Your Bank details are sent for verification", "Under Review");
                        } else if (bankDetailItem.getStatus() == 1) {
                            verifiedLayout("Your Bank Details are Verified", bankDetailItem.getAccno());
                            fragmentBankVerificationBinding.ivEmailVerified.setImageResource(R.drawable.ic_email_verified);
                        } else {
                            notVerifiedLayout();
                        }
                    } else {
                        Toast.makeText(context, bankDetailResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void failure(ApiException e) {
                fragmentBankVerificationBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    private void openGallery() {
        /*Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);*/

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), GALLERY_REQUEST_CODE);
    }

    private void openCamera() {
   /*     Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI = FileProvider.getUriForFile(this,
                "com.example.android.fileprovider",
                photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        startActivityForResult(intent, CAMERA_REQUEST_CODE);*/

        dispatchTakePictureIntent();

    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getActivity().getPackageName() + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }


    private void verifyBank() {
        fragmentBankVerificationBinding.setRefreshing(true);
        BankVerifyRequest bankVerifyRequest = new BankVerifyRequest();
        bankVerifyRequest.setImage(fileName);
        bankVerifyRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        bankVerifyRequest.setIfsc(fragmentBankVerificationBinding.ifscCode.getText().toString().trim());
        bankVerifyRequest.setBankname(fragmentBankVerificationBinding.bankName.getText().toString().trim());
        bankVerifyRequest.setState(state);
        bankVerifyRequest.setAcHolderName(fragmentBankVerificationBinding.etAccountholderName.getText().toString().trim());
        bankVerifyRequest.setAccno(fragmentBankVerificationBinding.etAccountNumber.getText().toString().trim());

        CustomCallAdapter.CustomCall<BankVerifyResponse> bankDetailResponseCustomCall = oAuthRestService.bankVerify(bankVerifyRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<BankVerifyResponse>() {
            @Override
            public void success(Response<BankVerifyResponse> response) {
                BankVerifyResponse bankVerifyResponse = response.body();
                if (bankVerifyResponse.getStatus() == 1) {
                    if (bankVerifyResponse.getResult().getStatus() == 1) {
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 0);
                        AppUtils.showSuccess((AppCompatActivity) context, "Your Bank details are sent for verification.");
                        verifiedLayout("Your Bank details are sent for verification", "Under Review");
                    } else {
                        AppUtils.showErrorr((AppCompatActivity) context, bankVerifyResponse.getResult().getMsg());
                    }
                } else {
                    AppUtils.showErrorr((AppCompatActivity) context, bankVerifyResponse.getMessage());
                }
                fragmentBankVerificationBinding.setRefreshing(false);
            }

            @Override
            public void failure(ApiException e) {
                fragmentBankVerificationBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean flag = false;
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri image = data.getData();
                flag = true;
                fileImage = image == null ? "" : AppUtils.getCompressImagePath(image, getActivity());

            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                flag = true;
                fileImage = currentPhotoPath;
            }
        }
        if (fileImage != null) {
            if (!fileImage.equalsIgnoreCase("")) {
                if (flag) {
                    upLoadBankImage();
                    return;
                }
            }
        }
        AppUtils.showErrorr((AppCompatActivity) getActivity(), "Can't load this file");
    }

    ProgressDialog pDialog = null;

    private void showProgress() {
        pDialog = new ProgressDialog(getActivity(), R.style.RoundedCornersDialog);
        pDialog.setMessage("Uploading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.show();
    }


    private void upLoadBankImage() {

    /*    fragmentBankVerificationBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setFile(fileImage);
    */


//        fragmentBankVerificationBinding.setRefreshing(true);
        showProgress();
        String userId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);

        RequestBody requestBodyUserId = RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        File file = new File(fileImage);
        long length = (file.length() / (1024 * 1024));
        if (length > 1) {
            file = AppUtils.saveBitmapToFile(file);
        }
        ProgressRequestBody progressRequestBody = new ProgressRequestBody(file, "multipart/form-data", this);
//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part uploadPic = MultipartBody.Part.createFormData("file", file.getName(), progressRequestBody);


        CustomCallAdapter.CustomCall<ImageUploadResponse> imageUploadResponseCustomCall = oAuthRestService.uploadBankImage(requestBodyUserId, uploadPic);
        imageUploadResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<ImageUploadResponse>() {
            @Override
            public void success(Response<ImageUploadResponse> response) {
//                fragmentBankVerificationBinding.setRefreshing(false);
                if (pDialog != null) pDialog.dismiss();

                ImageUploadResponse imageUploadResponse = response.body();
                if (imageUploadResponse.getStatus() == 1) {
                    if (imageUploadResponse.getStatus() == 1)
                        if (imageUploadResponse.getResult().get(0).getStatus() == 1) {
                            fragmentBankVerificationBinding.imageProgress.setVisibility(View.VISIBLE);
                            fileName = imageUploadResponse.getResult().get(0).getImage();
                            AppUtils.loadPanBankImage(fragmentBankVerificationBinding.imageProgress, fragmentBankVerificationBinding.ivPassbook, fileName, false);
                            AppUtils.showSuccess((AppCompatActivity) context, "Image uploaded.");
                        } else {
                            AppUtils.showErrorr((AppCompatActivity) context, "Error in image uploading.");
                        }
                } else {
                    AppUtils.showErrorr((AppCompatActivity) context, imageUploadResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                if (pDialog != null) pDialog.dismiss();
//                fragmentBankVerificationBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {
        pDialog.setProgress(percentage);
    }

    @Override
    public void onError() {
        pDialog.dismiss();
    }

    @Override
    public void onFinish() {
        pDialog.setProgress(100);
    }

    @Override
    public void onResume() {
        super.onResume();
       /* if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,0)==0) {
            fragmentBankVerificationBinding.bankText.setText("Your Bank details are sent for verification.");
        } else if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,0) == -1) {

        }*/
    }

    private void verifiedLayout(String title, String status) {
        fragmentBankVerificationBinding.verifiedLayout.setVisibility(View.VISIBLE);
        fragmentBankVerificationBinding.bankNotVerified.setVisibility(View.GONE);
        fragmentBankVerificationBinding.tvBankTitle.setText(title);
        fragmentBankVerificationBinding.tvPanNumber.setText(Html.fromHtml("<u>" + status + "</u>"));
        fragmentBankVerificationBinding.bankWithdrawWinningCardView.setVisibility(View.GONE);
    }

    private void notVerifiedLayout() {
        fragmentBankVerificationBinding.verifiedLayout.setVisibility(View.GONE);
        fragmentBankVerificationBinding.bankNotVerified.setVisibility(View.VISIBLE);
//        if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0) == 1) {
//
//            fragmentBankVerificationBinding.verifiedLayout.setVisibility(View.GONE);
//            fragmentBankVerificationBinding.bankNotVerified.setVisibility(View.VISIBLE);
//            fragmentBankVerificationBinding.bankWithdrawWinningCardView.setVisibility(View.GONE);
//        } else {
//            fragmentBankVerificationBinding.bankWithdrawWinningCardView.setVisibility(View.VISIBLE);
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 22) {
            if (grantResults.length != 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showImageSelectionDialog();
                }
            }
        }
    }
}
