package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;
import com.rg.mchampgold.common.utils.Constants;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Player implements Serializable {

    @SerializedName("role")
    private String role;

    @SerializedName("playerkey")
    private String playerkey;

    @SerializedName("name")
    private String name = "";

    @SerializedName("isSelected")
    private boolean isSelected;

    @SerializedName("teamcolor")
    private String teamcolor;

    @SerializedName("id")
    private int id;

    @SerializedName("team")
    private String team;

    @SerializedName("credit")
    private double credit;

    @SerializedName("points")
    private double totalpoints;

    @SerializedName("playing_11")
    private int playing11;

    @SerializedName("selected_by")
    private String selectedBy;

    @SerializedName("captain_selected_by")
    private String captain_selected_by;


    @SerializedName("vice_captain_selected_by")
    private String vice_captain_selected_by;

    public String getVice_captain_selected_by() {
        return vice_captain_selected_by;
    }

    public void setVice_captain_selected_by(String vice_captain_selected_by) {
        this.vice_captain_selected_by = vice_captain_selected_by;
    }

    public String getCaptain_selected_by() {
        return captain_selected_by;
    }

    public void setCaptain_selected_by(String captain_selected_by) {
        this.captain_selected_by = captain_selected_by;
    }

    @SerializedName("teamcode")
    private String teamname;

    @SerializedName("image")
    private String image;

    @SerializedName("captain")
    private int captain;

    boolean isHeader;

    @SerializedName("vicecaptain")
    private int vicecaptain;

    boolean isCaptain;
    boolean isVcCaptain;

    private String captaionText;
    private String vCaptainText;

    @SerializedName("is_playing_show")
    private int is_playing_show;

    @SerializedName("is_playing")
    private int is_playing;

    @SerializedName("last_match")
    private int last_match;

    @SerializedName("last_match_text")
    private String last_match_text;


    @SerializedName("series_points")
    private double seriesPoints;


    public void setRole(String role) {
        this.role = role;
    }

    public String getRole() {
        return role == null ? "" : role;
    }

    public void setPlayerkey(String playerkey) {
        this.playerkey = playerkey;
    }

    public String getPlayerkey() {
        return playerkey == null ? "" : playerkey;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name == null ? "" : name;
    }

   /* public String getShortName() {
        StringBuilder s1 = new StringBuilder();
        if (!name.equals("")) {
            String args[] = name.split(" ");

            if (args!=null) {
                if (args.length > 1) {
                    for (int i = 0; i < args.length; i++) {
                        if (i == 2)
                            break;
                        if (i == 0) {
                            if (args.length > 0)
                                s1.append(args[i].charAt(i));
                        } else {
                            s1.append(" " + args[i]);
                        }
                    }
                    return s1.toString();
                } else
                    return name;
            }
        }

            return "";

    }*/

    public double getSeriesPoints() {
        return seriesPoints;
    }

    public void setSeriesPoints(double seriesPoints) {
        this.seriesPoints = seriesPoints;
    }

    public String getShortName() {

        if (!name.equals("") && name != null) {
            name = name.trim();
            if (name.trim().length() > 0) {
                String names[] = name.split(" ");
                if (names.length > 1)
                    return names[0].charAt(0) + " " + names[1];
                else
                    return names[0];
            } else
                return name;
        }

        return "";
    }


    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public boolean isIsSelected() {
        return isSelected;
    }

    public void setTeamcolor(String teamcolor) {
        this.teamcolor = teamcolor;
    }

    public String getTeamcolor() {
        return teamcolor == null ? "" : teamcolor;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getTeam() {
        return team == null ? "" : team;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public void setTotalpoints(double totalpoints) {
        this.totalpoints = totalpoints;
    }

    public double getTotalpoints() {
        return totalpoints;
    }

    public void setPlaying11(int playing11) {
        this.playing11 = playing11;
    }

    public int getPlaying11() {
        return playing11;
    }


    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getTeamname() {
        return teamname == null ? "" : teamname;

    }

    public String getTeamnameWithRole() {
        String roleTag = "";
        if (role.equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_KEEP)) {
            roleTag = "WK";
        } else if (role.equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BAT)) {
            roleTag = "BAT";
        } else if (role.equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ALL_R)) {
            roleTag = "ALL";
        } else if (role.equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BOL)) {
            roleTag = "BOWL";
        }

        return teamname == null ? "" : teamname + "-" + roleTag;
//        return teamname == null ? "" : teamname;
    }

    public String getSelectedBy() {
        return selectedBy;
    }

    public void setSelectedBy(String selectedBy) {
        this.selectedBy = selectedBy;
    }

    public String showPlayerPoint() {
        return "" + String.valueOf(seriesPoints);
    }

    public String showPlayerCredit() {
        return "" + String.valueOf(credit);
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }


    public boolean isCaptain() {
        return isCaptain;
    }

    public void setCaptain(boolean captain) {
        isCaptain = captain;
    }

    public boolean isVcCaptain() {
        return isVcCaptain;
    }

    public void setVcCaptain(boolean vcCaptain) {
        isVcCaptain = vcCaptain;
    }


    public int getCaptain() {
        return captain;
    }

    public void setCaptain(int captain) {
        this.captain = captain;
    }

    public int getVicecaptain() {
        return vicecaptain;
    }

    public void setVicecaptain(int vicecaptain) {
        this.vicecaptain = vicecaptain;
    }


    public String getCaptaionText() {
	/*	if(isCaptain)
		   return "2x";
		else*/
        return "C";
    }

    public String getvCaptainText() {
	/*	if(isVcCaptain)
			return "1.5x";

		else*/
        return "VC";
    }

    public int getIs_playing_show() {
        return is_playing_show;
    }

    public void setIs_playing_show(int is_playing_show) {
        this.is_playing_show = is_playing_show;
    }

    public int getIs_playing() {
        return is_playing;
    }

    public void setIs_playing(int is_playing) {
        this.is_playing = is_playing;
    }

    public int getLast_match() {
        return last_match;
    }

    public void setLast_match(int last_match) {
        this.last_match = last_match;
    }

    public String getLast_match_text() {
        return last_match_text;
    }

    public void setLast_match_text(String last_match_text) {
        this.last_match_text = last_match_text;
    }
}