package com.rg.mchampgold.app.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.JoinedContesttItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.JoinedContestItemAdapter;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.app.viewModel.ContestDetailsViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityJoinedContestBinding;

import java.util.ArrayList;

public class JoinedContestActivity extends AppCompatActivity implements OnContestItemClickListener {

    ActivityJoinedContestBinding mBinding;
    JoinedContestItemAdapter mAdapter;
    ContestDetailsViewModel contestDetailsViewModel;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    Context context;
    String headerText;
    String date;
    String sportKey = "";
    int teamCount;


    ArrayList<JoinedContesttItem> list = new ArrayList<>();
    private int seriesId;
    private int isLeaderboard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        contestDetailsViewModel = ContestDetailsViewModel.create(JoinedContestActivity.this);
        MyApplication.getAppComponent().inject(contestDetailsViewModel);
        context = JoinedContestActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_joined_contest);
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.joined_contest));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            date = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
            seriesId = getIntent().getExtras().getInt(Constants.KEY_SERIES_ID, 0);
            isLeaderboard = getIntent().getExtras().getInt(Constants.KEY_IS_LEADERBOARD, 0);
        }

        if (headerText.equalsIgnoreCase("Winner Declared")) {
            mBinding.matchHeaderInfo.tvMatchTimer.setText("Winner Declared");
            mBinding.matchHeaderInfo.tvMatchTimer.setTextColor(getResources().getColor(R.color.color_match_declared));
        } else if (headerText.equalsIgnoreCase("In Progress")) {
            mBinding.matchHeaderInfo.tvMatchTimer.setText("In Progress");
            mBinding.matchHeaderInfo.tvMatchTimer.setTextColor(getResources().getColor(R.color.color_match_progress));
        } else {
            mBinding.matchHeaderInfo.tvMatchTimer.setText(headerText);
            mBinding.matchHeaderInfo.tvMatchTimer.setTextColor(getResources().getColor(R.color.pink));

        }


        String teams[] = teamVsName.split(" ");
        mBinding.matchHeaderInfo.tvTeam1.setText(teams[0]);
        mBinding.matchHeaderInfo.tvTeam2.setText(teams[2]);

        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam2, teamSecondUrl);

        showTimer();
        setupRecyclerView();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            finish();
            return true;
        } else if (itemId == R.id.navigation_award) {
//            Intent intent = new Intent(MyApplication.appContext, LeaderboardActivity.class);
//            intent.putExtra(Constants.KEY_SERIES_ID, seriesId);
//            intent.putExtra(Constants.KEY_IS_LEADERBOARD, isLeaderboard);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            MyApplication.appContext.startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_leaderboard, menu);
//
//        ImageView imageView = (ImageView) menu.findItem(R.id.navigation_award).getActionView().findViewById(R.id.navigation_award);
//
//        imageView.setImageResource(R.drawable.ic_rank_yellow);
//
//        MenuItem leaderboardAwardItem = menu.findItem(R.id.navigation_award);
//
//        leaderboardAwardItem.getActionView().setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                JoinedContestActivity.this.onOptionsItemSelected(leaderboardAwardItem);
//            }
//        });

        return true;
    }

    private void setupRecyclerView() {
        mAdapter = new JoinedContestItemAdapter(context, list, this, matchKey);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
        getData();
    }


    private void getData() {
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        contestDetailsViewModel.loadJoinedContestRequest(request);
        contestDetailsViewModel.getJoinedContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getJoinedContestItem().getContest().size() > 0) {
                        teamCount = arrayListResource.getData().getJoinedContestItem().getUserTeams();
                        list = arrayListResource.getData().getJoinedContestItem().getContest();
                        mAdapter.updateData(list);
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }

    @Override
    public void onContestClick(Contest contest, boolean isForDetail) {
        Intent intent;
        if (isForDetail) {
            intent = new Intent(JoinedContestActivity.this, UpComingContestDetailActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS, true);
            intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
            intent.putExtra(Constants.SPORT_KEY, sportKey);
            intent.putExtra(Constants.KEY_SERIES_ID, seriesId);
            intent.putExtra(Constants.KEY_IS_LEADERBOARD, isLeaderboard);
        } else {
            intent = new Intent(JoinedContestActivity.this, MyTeamsActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.SPORT_KEY, sportKey);
        }
        startActivity(intent);
    }


    private void showTimer() {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                    mBinding.matchHeaderInfo.tvMatchTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.matchHeaderInfo.tvMatchTimer.setText("00h 00m 00s");
                }
            };
            countDownTimer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
