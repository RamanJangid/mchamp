package com.rg.mchampgold.app.payTm;

import android.app.AlertDialog;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
//import com.paytm.pgsdk.TransactionManager;
import com.google.firebase.analytics.FirebaseAnalytics;
//import com.paytm.pgsdk.TransactionManager;
//import com.paytm.pgsdk.PaytmOrder;
//import com.paytm.pgsdk.PaytmPGService;
//import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.AddPaymentRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.AddPaymentValueResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityMerchantBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;


public class PaytmMerchantActivity extends AppCompatActivity {

    ActivityMerchantBinding merchantBinding;
    int requestCode = 2531;
//    PaytmPGService Service;
    String txnToken, CHECKSUMHASH1, orderid, customerid, price, MID = MyApplication.paytm_mid,
            Industry_TYpe = "Retail", Channel_ID = "WAP", WEBSITE = "DEFAULT";
    int promoId;
            /*String CHECKSUMHASH1, orderid, customerid, price, MID = MyApplication.paytm_mid,
            Industry_TYpe = "Retail", Channel_ID = "WAP", WEBSITE = "WEBSTAGING";
*/

    RequestQueue requestQueue;

    @Inject
    OAuthRestService oAuthRestService;

    FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        merchantBinding = DataBindingUtil.setContentView(this, R.layout.activity_merchant);
        MyApplication.getAppComponent().inject(PaytmMerchantActivity.this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        if (getIntent() != null && getIntent().getExtras() != null) {
            price = getIntent().getExtras().getString("addedValue");
            promoId = getIntent().getExtras().getInt("promoId");
        }

        requestQueue = Volley.newRequestQueue(PaytmMerchantActivity.this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        onStartTransaction();
    }

    //This is to refresh the order id: Only for the Sample App's purpose.
    @Override
    protected void onStart() {
        super.onStart();
    }


    public void onStartTransaction() {
        txnToken = "";
        CHECKSUMHASH1 = "";
        orderid = "";
        customerid = "";
        orderid = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID) + "MCHAMP_GOLD" + System.currentTimeMillis();
        customerid = "MCHAMP_GOLD" + MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);


        CallVolley(MyApplication.paytm_checksum_url);
    }

    public void CallVolley(String a) {
        merchantBinding.setRefreshing(true);
        try {
            StringRequest strRequest = new StringRequest(Request.Method.POST, a,
                    response -> {
                        merchantBinding.setRefreshing(false);
                        try {
                            Log.i("CheckVer is", response);
                            JSONObject jsonObject = new JSONObject(response);
//                            CHECKSUMHASH1 = jsonObject.has("CHECKSUMHASH") ? jsonObject.getString("CHECKSUMHASH") : "";
                            txnToken = jsonObject.has("token") ? jsonObject.getString("token") : "";
//                            CallPaytmIntegration();
                            callAllInOnePaytm();
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    },
                    error -> {
                        merchantBinding.setRefreshing(false);
                        showToast(error.toString());
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    //paytm live BBF
                    HashMap<String, String> paramMap = new HashMap<>();
                    paramMap.put("MID", MID);
                    paramMap.put("ORDER_ID", orderid);
                    paramMap.put("CUST_ID", customerid);
                    paramMap.put("INDUSTRY_TYPE_ID", Industry_TYpe);
                    paramMap.put("CHANNEL_ID", Channel_ID);
                    paramMap.put("TXN_AMOUNT", price);
                    paramMap.put("WEBSITE", WEBSITE);
                    paramMap.put("EMAIL", MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL));
                    paramMap.put("MOBILE_NO", MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_MOBILE));
                    //paramMap.put("MOBILE_NO", PHONE_NO);
                    paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderid);//Provided by Paytm live
//                     paramMap.put("CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + orderid);//Provided by Paytm Staging url

                    return paramMap;
                }
            };
            requestQueue.add(strRequest);
        } catch (Exception e) {
            showToast("---" + e);
        }

    }

    public void callAllInOnePaytm() {
//        String callbackurl = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderid;
////        String callbackurl="https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + orderid;
//        PaytmOrder paytmOrder = new PaytmOrder(orderid, MID, txnToken, price, callbackurl);
//        TransactionManager transactionManager = new TransactionManager(paytmOrder, new PaytmPaymentTransactionCallback() {
//            @Override
//            public void onTransactionResponse(Bundle inResponse) {
//                String status = (String) inResponse.get("STATUS");
//                if (status.equals("TXN_SUCCESS")) {
//                    ORDERID = (String) inResponse.get("ORDERID");
//                    addBalance();
//                } else {
//                    String reason = (String) inResponse.get("RESPMSG");
//                    showToast(reason);
//                    finishThisActivity();
//                }
//            }
//
//            @Override
//            public void networkNotAvailable() {
//                finishThisActivity();
//            }
//
//            @Override
//            public void onErrorProceed(String s) {
//                finishThisActivity();
//            }
//
//            @Override
//            public void clientAuthenticationFailed(String s) {
//                finishThisActivity();
//            }
//
//            @Override
//            public void someUIErrorOccurred(String s) {
//                finishThisActivity();
//            }
//
//            @Override
//            public void onErrorLoadingWebPage(int i, String s, String s1) {
//                finishThisActivity();
//            }
//
//            @Override
//            public void onBackPressedCancelTransaction() {
//                showToast("User has not completed transcation");
//                finishThisActivity();
//            }
//
//            @Override
//            public void onTransactionCancel(String s, Bundle bundle) {
//                finishThisActivity();
//            }
//        });
////        transactionManager.setSubscriptioFlow(true);
////        transactionManager.setShowPaymentUrl("https://securegw-stage.paytm.in/theia/api/v1/showPaymentPage");
//        transactionManager.setShowPaymentUrl("https://securegw.paytm.in/theia/api/v1/showPaymentPage");
//        transactionManager.startTransaction(this, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PaytmMerchantActivity.this.requestCode && data != null) {
//            String inResponse=data.getStringExtra("response");
            try {

                if (data.hasExtra("response")) {

                    if (TextUtils.isEmpty(data.getStringExtra("response"))) {
                        if (data.hasExtra("nativeSdkForMerchantMessage")) {
                            if (!TextUtils.isEmpty(data.getStringExtra("nativeSdkForMerchantMessage"))) {
                                if (data.getStringExtra("nativeSdkForMerchantMessage").equals("onBackPressedCancelTransaction")) {
                                    showToast("User has not completed transcation");
                                    finishThisActivity();
                                }
                            }
                        }
                    }

                    JSONObject jsonObject = new JSONObject(data.getStringExtra("response"));
                    String status = String.valueOf(jsonObject.get("STATUS"));
                    if (status.equals("TXN_SUCCESS")) {
                        ORDERID = String.valueOf(jsonObject.get("ORDERID"));
                        addBalance();
                    } else {
                        String reason = String.valueOf(jsonObject.get("RESPMSG"));
                        showToast(reason);
                        finishThisActivity();
                    }
                } else {
                    finishThisActivity();
                }
            } catch (Exception e) {
                e.printStackTrace();
                finishThisActivity();
            }

//            Toast.makeText(this, data.getStringExtra("nativeSdkForMerchantMessage") + data.getStringExtra("response"), Toast.LENGTH_SHORT).show();
        }
    }

    String ORDERID = "";

    public void CallPaytmIntegration() {
//        Service = PaytmPGService.getProductionService();
//        HashMap<String, String> paramMap = new HashMap<String, String>();
//        paramMap.put("MID", MID);
//        paramMap.put("ORDER_ID", orderid);
//        paramMap.put("CUST_ID", customerid);
//        paramMap.put("INDUSTRY_TYPE_ID", Industry_TYpe);
//        paramMap.put("CHANNEL_ID", Channel_ID);
//        paramMap.put("TXN_AMOUNT", price);
//        paramMap.put("WEBSITE", WEBSITE);
//        paramMap.put("EMAIL", MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL));
//        paramMap.put("MOBILE_NO", MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_MOBILE));
//
//        //  paramMap.put("MOBILE_NO", PHONE_NO);
//        paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderid);//Provided by Paytm
//        //  paramMap.put("CALLBACK_URL", "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + orderid);//Provided by Paytm
//        paramMap.put("CHECKSUMHASH", CHECKSUMHASH1);
//
//        PaytmOrder Order = new PaytmOrder(paramMap);
//        Service.initialize(Order, null);
//        Service.startPaymentTransaction(this, true, true, new PaytmPaymentTransactionCallback() {
//
//            @Override
//            public void someUIErrorOccurred(String inErrorMessage) {
//                Log.d("LOG 1", "UI Error Occur.");
//                showToast("UI Error Occur.");
//                finish();
//            }
//
//            @Override
//            public void onTransactionResponse(Bundle inResponse) {
//                Log.d("LOG 2", "Payment Transaction : " + inResponse);
//                //   Log.e("RESPONSE", inResponse.toString());
//                String status = (String) inResponse.get("STATUS");
//
//                if (status.equals("TXN_SUCCESS")) {
//                    ORDERID = (String) inResponse.get("ORDERID");
//                    addBalance();
//                } else {
//                    String reason = (String) inResponse.get("RESPMSG");
//                    showToast(reason);
//                    finishThisActivity();
//                }
//            }
//
//            @Override
//            public void networkNotAvailable() {
//                showToast("UI Error Occur.");
//                Log.d("LOG 3", "UI Error Occur.");
//                // AppUtils.showErrorr(PaytmMerchantActivity.this, "UI Error occured!");
//                finishThisActivity();
//            }
//
//            @Override
//            public void clientAuthenticationFailed(String inErrorMessage) {
//                Log.d("LOG 4", "UI Error Occur.");
//                showToast("server side error");
//                finishThisActivity();
//            }
//
//            @Override
//            public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
//                Log.d("LOG 5", "web page error");
//                finishThisActivity();
//            }
//
//            @Override
//            public void onBackPressedCancelTransaction() {
//                Log.d("LOG 6", "back pressed");
//                finishThisActivity();
//            }
//
//            @Override
//            public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
//                Log.d("LOG 7", "Payment Transaction Failed " + inErrorMessage);
//                showToast("Payment transaction failed");
//                finishThisActivity();
//            }
//
//        });
    }


    @Override
    public void onBackPressed() {
        AlertDialog d = new AlertDialog.Builder(PaytmMerchantActivity.this).create();
        d.setMessage("Are you sure to cancel the payment");
        d.setTitle("Cancel Payment");
        d.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", (dialogInterface, i) -> finish());
        d.setButton(AlertDialog.BUTTON_NEGATIVE, "No", (dialogInterface, i) -> {

        });
        d.show();
    }

    public void showToast(String message) {
        Toast.makeText(PaytmMerchantActivity.this, message, Toast.LENGTH_SHORT).show();

    }

    private void addBalance() {
        merchantBinding.setRefreshing(true);
        AddPaymentRequest addPaymentRequest = new AddPaymentRequest();
        addPaymentRequest.setAmount(price);
        addPaymentRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        addPaymentRequest.setOrder_id(orderid);
        addPaymentRequest.setPaymentby(Constants.PAYTM);
        addPaymentRequest.setPayment_id(ORDERID);
        addPaymentRequest.setPromo_id(String.valueOf(promoId));
        CustomCallAdapter.CustomCall<AddPaymentValueResponse> orderIdResponse = oAuthRestService.addPayment(addPaymentRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<AddPaymentValueResponse>() {
            @Override
            public void success(retrofit2.Response<AddPaymentValueResponse> response) {
                merchantBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    AddPaymentValueResponse addPaymentValueResponse = response.body();
                    if (addPaymentValueResponse.getStatus() == 1) {
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, addPaymentValueResponse.getResult().getAmount() + "");
                        if (addPaymentValueResponse.getResult().getMsg().equalsIgnoreCase("Payment done.")) {
                            showToast("Balance added successfully");
                        }
                    } else {
                        Toast.makeText(PaytmMerchantActivity.this, addPaymentValueResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PaytmMerchantActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }

                finishThisActivity();
            }

            @Override
            public void failure(ApiException e) {
                merchantBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void finishThisActivity() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}


