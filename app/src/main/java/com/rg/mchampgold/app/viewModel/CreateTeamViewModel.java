package com.rg.mchampgold.app.viewModel;


import androidx.arch.core.util.Function;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.rg.mchampgold.app.api.request.CreateTeamRequest;
import com.rg.mchampgold.app.dataModel.CreateTeamResponse;
import com.rg.mchampgold.app.repository.MatchRepository;
import com.rg.mchampgold.common.api.Resource;

import javax.inject.Inject;


public class CreateTeamViewModel extends ViewModel {

    private MatchRepository mRepository;
    
    private final MutableLiveData<CreateTeamRequest> teamRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<CreateTeamResponse>> createTeamLiveData = Transformations.switchMap(teamRequestMutableLiveData, new Function<CreateTeamRequest, LiveData<Resource<CreateTeamResponse>>>() {
        @Override
        public LiveData<Resource<CreateTeamResponse>> apply(final CreateTeamRequest input) {
            LiveData<Resource<CreateTeamResponse>> resourceLiveData = mRepository.createTeam(input);
            final MediatorLiveData<Resource<CreateTeamResponse>> mediator = new MediatorLiveData<Resource<CreateTeamResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<CreateTeamResponse>>() {
                @Override
                public void onChanged(Resource<CreateTeamResponse> arrayListResource) {
                    CreateTeamResponse resp = arrayListResource.getData();
                    Resource<CreateTeamResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    /*
    Get the view model instance.
     */
    public static CreateTeamViewModel create(FragmentActivity activity) {
        CreateTeamViewModel viewModel = ViewModelProviders.of(activity).get(CreateTeamViewModel.class);
        return viewModel;
    }


    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<CreateTeamResponse>> createTeam() {
        return createTeamLiveData;
    }

    public void loadCreateTeamRequest(CreateTeamRequest createTeamRequest) {
        teamRequestMutableLiveData.setValue(createTeamRequest);
    }


    @Inject
    public void setRepository(MatchRepository repository) {
        this.mRepository = repository;
    }

    

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}