package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class MyTransactionHistoryResponse{

	@SerializedName("result")
	private TransactionHistoryItem transactionHistoryItem;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;


	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MyTransactionHistoryResponse{" + 
			",message = '" + message + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}

    public TransactionHistoryItem getTransactionHistoryItem() {
        return transactionHistoryItem;
    }

    public void setTransactionHistoryItem(TransactionHistoryItem transactionHistoryItem) {
        this.transactionHistoryItem = transactionHistoryItem;
    }
}