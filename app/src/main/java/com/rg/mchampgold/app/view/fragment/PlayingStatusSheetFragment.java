package com.rg.mchampgold.app.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.view.activity.CreateTeamActivity;
import com.rg.mchampgold.app.view.basketball.BasketBallCreateTeamActivity;
import com.rg.mchampgold.app.view.football.FootballCreateTeamActivity;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentPlayingStatusBinding;

public class PlayingStatusSheetFragment extends BottomSheetDialogFragment {

    private FragmentPlayingStatusBinding binding;
    private Context context;
    private String playerStatus;

    public PlayingStatusSheetFragment(Context context) {
        this.context = context;
        if (context instanceof CreateTeamActivity) {
            this.playerStatus = ((CreateTeamActivity) context).playerStatus;
        } else if (context instanceof FootballCreateTeamActivity) {
            this.playerStatus = ((FootballCreateTeamActivity) context).playerStatus;
        } else if (context instanceof BasketBallCreateTeamActivity) {
            this.playerStatus = ((BasketBallCreateTeamActivity) context).playerStatus;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_playing_status, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (playerStatus.equals(Constants.PLAYING)) {
            binding.icPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_colored_radio_button));
            binding.icNotPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_un_colored_radio_button));
        } else if (playerStatus.equals(Constants.NOT_PLAYING)){
            binding.icNotPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_colored_radio_button));
            binding.icPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_un_colored_radio_button));
        }

        binding.llPlayingStatus.setOnClickListener(view1 -> {
            dismiss();
        });

        binding.llPlaying.setOnClickListener(view2 -> {
            binding.icPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_colored_radio_button));
            binding.icNotPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_un_colored_radio_button));
            if (context instanceof CreateTeamActivity) {
                ((CreateTeamActivity) context).changePlayerStatus(Constants.PLAYING);
            } else if (context instanceof FootballCreateTeamActivity) {
                ((FootballCreateTeamActivity) context).changePlayerStatus(Constants.PLAYING);
            } else if (context instanceof BasketBallCreateTeamActivity) {
                ((BasketBallCreateTeamActivity) context).changePlayerStatus(Constants.PLAYING);
            }
            dismiss();
        });

        binding.llNotPlaying.setOnClickListener(view3 -> {
            binding.icNotPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_colored_radio_button));
            binding.icPlayingButton.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_un_colored_radio_button));
            if (context instanceof CreateTeamActivity) {
                ((CreateTeamActivity) context).changePlayerStatus(Constants.NOT_PLAYING);
            } else if (context instanceof FootballCreateTeamActivity) {
                ((FootballCreateTeamActivity) context).changePlayerStatus(Constants.NOT_PLAYING);
            } else if (context instanceof BasketBallCreateTeamActivity) {
                ((BasketBallCreateTeamActivity) context).changePlayerStatus(Constants.NOT_PLAYING);
            }
            dismiss();
        });
    }
}
