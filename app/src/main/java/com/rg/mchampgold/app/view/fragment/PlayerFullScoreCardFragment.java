package com.rg.mchampgold.app.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.LiveMatchesScoreInningListItem;
import com.rg.mchampgold.app.view.adapter.LiveScoreCardAdapter;
import com.rg.mchampgold.databinding.PlayerFullScoreCardLayoutBinding;

public class PlayerFullScoreCardFragment extends Fragment {

    PlayerFullScoreCardLayoutBinding mBinding;
    LiveMatchesScoreInningListItem data;

    public PlayerFullScoreCardFragment(LiveMatchesScoreInningListItem data){
        this.data=data;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.player_full_score_card_layout, container, false);
        mBinding.playerExtra.setText(String.valueOf(data.getExtra_run()));
        mBinding.playerTotal.setText(data.getScores_full()+"("+data.getOvers()+")");
        RecyclerView.LayoutManager pLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.recyclerView.setLayoutManager(pLayoutManager);

        RecyclerView.LayoutManager bLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.bowlerRecyclerView.setLayoutManager(bLayoutManager);
        mBinding.recyclerView.setAdapter(new LiveScoreCardAdapter(getActivity(),data.getPlayers(),null));
        mBinding.bowlerRecyclerView.setAdapter(new LiveScoreCardAdapter(getActivity(),null,data.getBowlers()));
        return  mBinding.getRoot();
    }
}
