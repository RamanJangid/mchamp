package com.rg.mchampgold.app.view.basketball;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityBasketballTeamPreviewBinding;

import java.util.ArrayList;


public class BasketBallTeamPreviewActivity extends AppCompatActivity {

    ActivityBasketballTeamPreviewBinding activityTeamPreviewBinding;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    String teamName="";
    String headerText;
    String sportKey;
    boolean isShowTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(BasketBallTeamPreviewActivity.this);
        activityTeamPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_basketball_team_preview);
        initialize();

        LinearLayoutManager horizontalLayoutManagaerr = new LinearLayoutManager(BasketBallTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.pgRecyclerView.setLayoutManager(horizontalLayoutManagaerr);

        LinearLayoutManager horizontalLayoutManagaer =
                new LinearLayoutManager(BasketBallTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.sgRecyclerView.setLayoutManager(horizontalLayoutManagaer);

        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(BasketBallTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.sfRecyclerView.setLayoutManager(horizontalLayoutManagaer1);

        LinearLayoutManager horizontalLayoutManagaer2 = new LinearLayoutManager(BasketBallTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.pfRecyclerView.setLayoutManager(horizontalLayoutManagaer2);

        LinearLayoutManager horizontalLayoutManagaer3 = new LinearLayoutManager(BasketBallTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.centreRecyclerView.setLayoutManager(horizontalLayoutManagaer3);
        if (MyApplication.fromMyTeams) {
            MyApplication.fromMyTeams=false;
//            activityTeamPreviewBinding.icClose.setVisibility(View.GONE);
            activityTeamPreviewBinding.edtText.setVisibility(View.VISIBLE);
        }else {
//            activityTeamPreviewBinding.icClose.setVisibility(View.VISIBLE);
            activityTeamPreviewBinding.edtText.setVisibility(View.GONE);
        }

        activityTeamPreviewBinding.edtText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editOrClone();
            }
        });

    }

    public void editOrClone() {
        Intent intent = new Intent(BasketBallTeamPreviewActivity.this, BasketBallCreateTeamActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_TEAM_ID, MyApplication.teamId);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY, sportKey);
        intent.putExtra("isFromEditOrClone", true);
        intent.putExtra("selectedList", MyApplication.teamList);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;
        } else if (itemId == R.id.navigation_wallet) {
            openWalletActivity();
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void initialize() {
       /* setSupportActionBar(activityTeamPreviewBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.team_preview));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
*/

        ArrayList<Player> listPG = new ArrayList<>();
        ArrayList<Player> listSG = new ArrayList<>();
        ArrayList<Player> listSF = new ArrayList<>();
        ArrayList<Player> listPF = new ArrayList<>();
        ArrayList<Player> listC = new ArrayList<>();

        if(getIntent()!=null && getIntent().getExtras()!=null) {
            matchKey=  getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName=  getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl=  getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl=  getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            teamName=  getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT,"");
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER,false);
            sportKey  = getIntent().getExtras().getString(Constants.SPORT_KEY);
            activityTeamPreviewBinding.team.setText(teamName);
            listPG         =   (ArrayList<Player>)getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_WK);
            listSG         =  (ArrayList<Player>)getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_BAT);
            listSF         =  (ArrayList<Player>)getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_AR);
            listPF         =  (ArrayList<Player>)getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_BOWL);
            listC         =  (ArrayList<Player>)getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_C);
            Double credits=0.0;
            for (int i=0; i<listPG.size(); i++){
                credits=credits+listPG.get(i).getCredit();
            }
            for (int i=0; i<listSG.size(); i++){
                credits=credits+listSG.get(i).getCredit();
            }
            for (int i=0; i<listSF.size(); i++){
                credits=credits+listSF.get(i).getCredit();
            }
            for (int i=0; i<listPF.size(); i++){
                credits=credits+listPF.get(i).getCredit();
            }
            for (int i=0; i<listC.size(); i++){
                credits=credits+listC.get(i).getCredit();
            }
            activityTeamPreviewBinding.totalCredits.setText(String.valueOf(credits)+"/100");
        }
      /*  activityTeamPreviewBinding.matchHeaderInfo.tvTeamVs.setText(teamVsName);
        activityTeamPreviewBinding.matchHeaderInfo.ivTeamFirst.setImageURI(teamFirstUrl);
        activityTeamPreviewBinding.matchHeaderInfo.ivTeamSecond.setImageURI(teamSecondUrl);

        if(isShowTimer) {
            showTimer();
        }
        else {
            if(headerText.equalsIgnoreCase("Winner Declared")) {
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText("Winner Declared");
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#f70073"));
            }
            else if(headerText.equalsIgnoreCase("In Progress")) {
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText("In Progress");
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#16ae28"));
            }
        }*/
        if(teamName!=null)
        activityTeamPreviewBinding.teamName.setText(teamName);

        activityTeamPreviewBinding.pgRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.pgRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false,listPG,matchKey));

        activityTeamPreviewBinding.sgRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.sgRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false,listSG,matchKey));

        activityTeamPreviewBinding.sfRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.sfRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false,listSF,matchKey));

        activityTeamPreviewBinding.pfRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.pfRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false,listPF,matchKey));

        activityTeamPreviewBinding.centreRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.centreRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false,listC,matchKey));

        activityTeamPreviewBinding.icClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private void openNotificationActivity() {
        startActivity(new Intent(BasketBallTeamPreviewActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(BasketBallTeamPreviewActivity.this, MyWalletActivity.class));

    }


   /* private void showTimer()
    {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000)) % 24;
                    if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                        activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d " + twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    } else {
                        activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText(twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    }

                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText("00h 00m 00s");
                }
            };
            countDownTimer.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
