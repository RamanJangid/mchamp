package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

@Generated("com.robohorse.robopojogenerator")
public class GetNotificationResponse{

	@SerializedName("result")
	private ArrayList<NotificationItem> result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public ArrayList<NotificationItem> getResult() {
		return result;
	}

	public void setResult(ArrayList<NotificationItem> result) {
		this.result = result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetNotificationResponse{" + 
			",message = '" + message + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}
}