package com.rg.mchampgold.app.utils;

public interface IncomingSMSListener {


    void onOTPReceived(String otp);

    void onOTPTimeOut();

}
