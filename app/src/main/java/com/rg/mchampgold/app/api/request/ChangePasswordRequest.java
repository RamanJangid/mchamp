package com.rg.mchampgold.app.api.request;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ChangePasswordRequest{

	@SerializedName("user_id")
	private String userId;

	@SerializedName("oldpassword")
	private String oldpassword;

	@SerializedName("newpassword")
	private String newpassword;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setOldpassword(String oldpassword){
		this.oldpassword = oldpassword;
	}

	public String getOldpassword(){
		return oldpassword;
	}

	public void setNewpassword(String newpassword){
		this.newpassword = newpassword;
	}

	public String getNewpassword(){
		return newpassword;
	}

	@Override
 	public String toString(){
		return 
			"ChangePasswordRequest{" + 
			"user_id = '" + userId + '\'' + 
			",oldpassword = '" + oldpassword + '\'' + 
			",newpassword = '" + newpassword + '\'' + 
			"}";
		}
}