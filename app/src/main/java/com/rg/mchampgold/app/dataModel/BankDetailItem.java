package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class BankDetailItem {

	@SerializedName("image")
	private String image;

	@SerializedName("imagetype")
	private String imagetype;

	@SerializedName("accno")
	private String accno;

	@SerializedName("bankbranch")
	private String bankbranch;

	@SerializedName("bankname")
	private String bankname;

	@SerializedName("state")
	private String state;

	@SerializedName("ifsc")
	private String ifsc;

	@SerializedName("status")
	private int status;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setImagetype(String imagetype){
		this.imagetype = imagetype;
	}

	public String getImagetype(){
		return imagetype;
	}

	public void setAccno(String accno){
		this.accno = accno;
	}

	public String getAccno(){
		return accno;
	}

	public void setBankbranch(String bankbranch){
		this.bankbranch = bankbranch;
	}

	public String getBankbranch(){
		return bankbranch;
	}

	public void setBankname(String bankname){
		this.bankname = bankname;
	}

	public String getBankname(){
		return bankname;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setIfsc(String ifsc){
		this.ifsc = ifsc;
	}

	public String getIfsc(){
		return ifsc;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"BankDetailItem{" +
			"image = '" + image + '\'' + 
			",imagetype = '" + imagetype + '\'' + 
			",accno = '" + accno + '\'' + 
			",bankbranch = '" + bankbranch + '\'' + 
			",bankname = '" + bankname + '\'' + 
			",state = '" + state + '\'' + 
			",ifsc = '" + ifsc + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}