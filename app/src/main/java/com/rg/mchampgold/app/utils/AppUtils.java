package com.rg.mchampgold.app.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.andrognito.flashbar.Flashbar;
import com.andrognito.flashbar.anim.FlashAnim;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.WinnerScoreCardItem;
import com.rg.mchampgold.app.view.CircleTransform;
import com.rg.mchampgold.app.view.ExpandableHeightListView;
import com.rg.mchampgold.app.view.activity.LeaderboardActivity;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.WebActivity;
import com.rg.mchampgold.common.utils.Constants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kotlin.jvm.internal.Intrinsics;


public class AppUtils {


    public static void setLocale(final Context context, final String lang) {
        final Locale loc = new Locale(lang);
        Locale.setDefault(loc);
        final Configuration cfg = new Configuration();
        cfg.locale = loc;
        context.getResources().updateConfiguration(cfg, null);
    }

    public static void showErrorr(AppCompatActivity appCompatActivity, String mesg) {
        Flashbar flashbar = new Flashbar.Builder(appCompatActivity)
                .gravity(Flashbar.Gravity.TOP)
                // .title(appCompatActivity.getResources().getString(R.string.app_name))
                .message(mesg != null ? mesg : "Data not found")
                .messageSizeInSp(14f)
                .messageTypeface(Typeface.createFromAsset(appCompatActivity.getAssets(), "font/roboto_regular.ttf"))
                .messageColorRes(R.color.colorWhite)
                .backgroundDrawable(R.drawable.bg_gradient_create_team_warning)
                .showIcon()
                .icon(R.drawable.close)
                .duration(2000)
                .dismissOnTapOutside()
                .build();

        flashbar.show();
    }

    public static void showWithdrawError(AppCompatActivity appCompatActivity, String mesg) {
        Flashbar flashbar = new Flashbar.Builder(appCompatActivity)
                .gravity(Flashbar.Gravity.TOP)
                // .title(appCompatActivity.getResources().getString(R.string.app_name))
                .message(mesg != null ? mesg : "Data not found")
                .backgroundDrawable(R.drawable.bg_gradient_create_team_warning)
                .showIcon()
                .duration(2000)
                .dismissOnTapOutside()
                .messageSizeInSp(14f)
                .messageTypeface(Typeface.createFromAsset(appCompatActivity.getAssets(), "font/roboto_regular.ttf"))
                .messageColorRes(R.color.colorWhite)
                .icon(R.drawable.close)
                .build();

        flashbar.show();
    }


    public static void openWebViewActivity(String titile, String type) {
        Intent intent = new Intent(MyApplication.appContext, WebActivity.class);
        intent.putExtra("title", titile);
        intent.putExtra("type", type);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MyApplication.appContext.startActivity(intent);
    }


    public static void showSuccess(AppCompatActivity appCompatActivity, String mesg) {
        @SuppressLint("ResourceAsColor") final Flashbar flashbar = new Flashbar.Builder(appCompatActivity)
                .gravity(Flashbar.Gravity.TOP)
                .title(appCompatActivity.getResources().getString(R.string.app_name))
                .message(mesg)
                .messageSizeInSp(14f)
                .messageTypeface(Typeface.createFromAsset(appCompatActivity.getAssets(), "font/roboto_regular.ttf"))
                .messageColorRes(R.color.colorWhite)
                .backgroundDrawable(R.drawable.bg_gradient_success)
                .showIcon()
                .dismissOnTapOutside()
                .titleColorRes(R.color.white)
                .icon(R.drawable.ic_launcher_placeholder)
                .iconColorFilter(Color.parseColor("#f1d457"))
                .duration(2000)
                .iconAnimation(FlashAnim.with(appCompatActivity).animateIcon().pulse()
                        .alpha()
                        .duration(750)
                        .accelerate())
                .build();

        flashbar.show();
    }

    public static void showWithdrawSuccess(AppCompatActivity appCompatActivity, String mesg) {
        final Flashbar flashbar = new Flashbar.Builder(appCompatActivity)
                .gravity(Flashbar.Gravity.TOP)
                .title(appCompatActivity.getResources().getString(R.string.app_name))
                .message(mesg)
                .messageSizeInSp(14f)
                .messageTypeface(Typeface.createFromAsset(appCompatActivity.getAssets(), "font/roboto_regular.ttf"))
                .messageColorRes(R.color.colorWhite)
                .backgroundDrawable(R.drawable.bg_gradient_success)
                .showIcon()
                .dismissOnTapOutside()
                .duration(2000)
                .titleColorRes(R.color.white)
                .icon(R.mipmap.ic_launcher)
                .iconAnimation(FlashAnim.with(appCompatActivity).animateIcon().pulse()
                        .alpha()
                        .duration(2000)
                        .accelerate())
                .build();

        flashbar.show();
    }


    public static String getShortName(String name) {

        if (name != null && !name.equals("")) {
            name = name.trim();
            if (name.trim().length() > 0) {
                String names[] = name.split(" ");
                if (names.length > 1)
                    return names[0].charAt(0) + " " + names[1];
                else
                    return names[0];
            } else
                return name;
        }

        return "";
    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


  /*  public static String getDeviceId(Context pContext) {
        String toSent = "";
        toSent = Settings.Secure.getString(pContext.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (toSent == null) {
            TelephonyManager mTelephonyManager = (TelephonyManager) pContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (mTelephonyManager != null) {
                toSent = mTelephonyManager.getDeviceId();
            }
            if (toSent == null) {
                toSent = "";
            }
        }

       return toSent;
    }*/


    //2018-03-20 15:45:28
    public static Long EventDateMilisecond(String eventDate) throws ParseException {
        Date eventDatee = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(eventDate);
        long ctime = eventDatee.getTime() - System.currentTimeMillis();
        long seconds = ctime / 1000 % 60;
        long minutes = (ctime / (1000 * 60)) % 60;
        long diffHours = ctime / (60 * 60 * 1000);

        return ctime;

    }

    private int dpToPx(int dp, AppCompatActivity activity) {
        float density = activity.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    public static void loadPopupImage(ImageView imageView, String url) {


        //  Log.e("url",url);
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(R.drawable.ic_banner_placeholder)
                    .error(R.drawable.ic_banner_placeholder)
                    .into(imageView);
        }
    }

    public static void loadImageCircle(ImageView imageView, String url) {

        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url).transform(new CircleTransform())

                    .placeholder(R.drawable.iv_no_profile_pic)
                    .error(R.drawable.iv_no_profile_pic)
                    .into(imageView);
        }
    }


    public static void loadImageWithoutPlaceholder(ImageView imageView, String url) {
        if (url != null && !url.equals("")) {
            Picasso.get().load(url.replace(" ", "%20")).into(imageView);
        }
    }

    public static void loadImageBanner(ImageView imageView, String url) {
        if (url != null && !url.equals("")) {
            Picasso.get().load(url.replaceAll(" ", "%20")).placeholder(R.drawable.banner_placeholder).error(R.drawable.banner_placeholder).into(imageView);
        }
    }

    public static void loadImageCategory(ImageView imageView, String url) {
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(R.mipmap.hotcontest)

                    .error(R.mipmap.hotcontest)
                    // .transform(new CircleTransform())
                    .into(imageView);
        }
    }


    public static void loadImageMatch(ImageView imageView, String url) {
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(R.drawable.ic_launcher_placeholder)
                    .error(R.drawable.ic_launcher_placeholder)
                    // .transform(new CircleTransform())
                    .into(imageView);
        }
    }


    public static void loadImage(ImageView imageView, String url) {


        //  Log.e("url",url);
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(R.drawable.iv_no_profile_pic)
                    .error(R.drawable.iv_no_profile_pic)
                    .into(imageView);
        } else {
            Picasso.get()
                    .load(R.drawable.iv_no_profile_pic)
                    .placeholder(R.drawable.iv_no_profile_pic)
                    .error(R.drawable.iv_no_profile_pic)
                    .into(imageView);
        }
    }

    public static void loadPreviewImage(ImageView imageView, String url) {


        //  Log.e("url",url);
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(R.drawable.img_preview_placeholder)
                    .error(R.drawable.img_preview_placeholder)
                    .into(imageView);
        } else {
            Picasso.get()
                    .load(R.drawable.img_preview_placeholder)
                    .placeholder(R.drawable.img_preview_placeholder)
                    .error(R.drawable.img_preview_placeholder)
                    .into(imageView);
        }
    }


    public static void loadPlayerImage(ImageView imageView, String url) {


        //   Log.e("url",url);
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(R.drawable.iv_no_profile_pic)
                    .transform(new CircleTransform())
                    //.transform(new RoundedCornerTransformation(2,0))
                    .error(R.drawable.iv_no_profile_pic)
                    .into(imageView);
        }
    }


    public static void loadImageleaderboard(ImageView imageView, String url) {
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(R.drawable.iv_no_profile_pic)
                    .error(R.drawable.iv_no_profile_pic)
                    .transform(new CircleTransform())
                    .into(imageView);
        }else {
            imageView.setImageResource(R.drawable.iv_no_profile_pic);
        }
    }


    public static void setStatusBarColor(AppCompatActivity activity, int color) {
/*        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
//            Drawable background = activity.getResources().getDrawable(R.drawable.status_bar_gradient_btn);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }*/
    }


    public static void setStatusBar(AppCompatActivity activity, int color) {

        Window window = activity.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(color);
        }

    }

    public static void showWinningPopup(Context context, ArrayList<WinnerScoreCardItem> priceCardlist, String s) {
        ExpandableHeightListView priceCard;
        TextView totalWinnersAmount;
        BottomSheetDialog dialog = new BottomSheetDialog(context);
        dialog.setContentView(R.layout.price_card_dialog);
        priceCard = dialog.findViewById(R.id.priceCard);
        totalWinnersAmount = dialog.findViewById(R.id.totalWinnersAmount);
        totalWinnersAmount.setText("₹ " + s);
        priceCard.setExpanded(true);
        priceCard.setAdapter(new PriceItemAdapter(context, priceCardlist));
        dialog.show();
    }

    public static void showFinishMatchState(Context context) {

        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.show_finish_match_status);
        Button close = dialog.findViewById(R.id.btnClose);
        close.setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    public static void showContestInformation(Context context, int teamLimit, boolean isShowCTag, boolean isShowMTag, boolean isShowWDTag, boolean isShowBTag, String bonusPercent) {

        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.contest_information_layout);
        LinearLayout cTag = dialog.findViewById(R.id.cTag);
        LinearLayout mTag = dialog.findViewById(R.id.mTag);
        LinearLayout wdTag = dialog.findViewById(R.id.wdTag);
        LinearLayout bTag = dialog.findViewById(R.id.bTag);
        View cTagView = dialog.findViewById(R.id.cTagView);
        View mTagView = dialog.findViewById(R.id.mTagView);
        View wdTagView = dialog.findViewById(R.id.wdTagView);
        Button close = dialog.findViewById(R.id.btnClose);
        TextView tagM_text = dialog.findViewById(R.id.tagM_text);
        TextView tagB_text = dialog.findViewById(R.id.tagb_text);
        TextView mContent = dialog.findViewById(R.id.mContent);
        TextView bContent = dialog.findViewById(R.id.bContent);
        tagM_text.setText(String.valueOf(teamLimit));
        tagB_text.setText(bonusPercent);
        mContent.setText("Maximum " + teamLimit + " Teams per user");
        bContent.setText("In this contest you can use " + bonusPercent + " Cash Bonus");

        if (isShowMTag) {
            mTag.setVisibility(View.VISIBLE);
            mTagView.setVisibility(View.VISIBLE);
        } else {
            mTag.setVisibility(View.GONE);
            mTagView.setVisibility(View.GONE);
        }
        if (isShowCTag) {
            cTag.setVisibility(View.VISIBLE);
            cTagView.setVisibility(View.VISIBLE);
        } else {
            cTag.setVisibility(View.GONE);
            cTagView.setVisibility(View.GONE);
        }
        if (isShowWDTag) {
            wdTag.setVisibility(View.VISIBLE);
            cTagView.setVisibility(View.VISIBLE);
        } else {
            wdTag.setVisibility(View.GONE);
            cTagView.setVisibility(View.GONE);
        }

        if (isShowBTag) {
            bTag.setVisibility(View.VISIBLE);
            wdTagView.setVisibility(View.VISIBLE);
        } else {
            bTag.setVisibility(View.GONE);
            wdTagView.setVisibility(View.GONE);
        }
        close.setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    public static void showFlexibleDialog(Context context) {

        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_flexible);
        Button close = dialog.findViewById(R.id.btnClose);

        close.setOnClickListener(v -> dialog.dismiss());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    public static void setWalletBalance(Menu menu, int color, boolean isFromHomeFragment, int isLeaderboard, int seriesId) {

        TextView textView = menu.findItem(R.id.navigation_wallet).getActionView().findViewById(R.id.navigation_wallet);
        if (color == MyApplication.appContext.getResources().getColor(R.color.pink)) {
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_wallet_inner, 0);
            if (menu.findItem(R.id.navigation_notification) != null)
                menu.findItem(R.id.navigation_notification).setIcon(R.drawable.notification_bell);
        } else {

        }
        if (!MyApplication.tinyDB.getString(Constants.KEY_USER_BALANCE).equalsIgnoreCase("") && !MyApplication.tinyDB.getString(Constants.KEY_USER_BONUS_BALANCE).equalsIgnoreCase("") && !MyApplication.tinyDB.getString(Constants.KEY_USER_WINING_AMOUNT).equalsIgnoreCase("")) {
//            textView.setText("₹" + new DecimalFormat("##.##").format((Float.parseFloat(MyApplication.tinyDB.getString(Constants.KEY_USER_BALANCE)) + Float.parseFloat(MyApplication.tinyDB.getString(Constants.KEY_USER_BONUS_BALANCE)) + Float.parseFloat(MyApplication.tinyDB.getString(Constants.KEY_USER_WINING_AMOUNT)))));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                for (Drawable drawable : textView.getCompoundDrawables()) {
                    if (drawable != null) {
                        drawable.setTint(color);
                    }
                }
            }
            textView.setTextColor(color);
            menu.findItem(R.id.navigation_wallet).getActionView().setOnClickListener(v -> {
//                Intent intent = new Intent(MyApplication.appContext, MyWalletActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                MyApplication.appContext.startActivity(intent);
                if (MyApplication.walletClickListener!=null){
                    MyApplication.walletClickListener.onFantasyWalletClick();
                }
            });

        }

//        ImageView imageView = (ImageView) menu.findItem(R.id.navigation_award).getActionView().findViewById(R.id.navigation_award);

//        if (isFromHomeFragment) {
//            imageView.setImageResource(R.drawable.ic_rank_leaderbord);
//        } else {
//            imageView.setImageResource(R.drawable.ic_rank_yellow);
//        }

//        imageView.setOnClickListener(v -> {
//            Intent intent = new Intent(MyApplication.appContext, LeaderboardActivity.class);
//            intent.putExtra(Constants.KEY_SERIES_ID, seriesId);
//            intent.putExtra(Constants.KEY_IS_LEADERBOARD, isLeaderboard);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            MyApplication.appContext.startActivity(intent);
//        });
    }

    public static void loadPanBankImage(ProgressBar progressBar, ImageView imageView, String url, boolean isPan) {

        //  Log.e("url",url);
        if (url != null && !url.equals("")) {
            Picasso.get()
                    .load(url.replace(" ", "%20"))
                    .placeholder(isPan ? R.drawable.pan_place_holder : R.drawable.bank_place_holder)
                    .error(isPan ? R.drawable.pan_place_holder : R.drawable.bank_place_holder)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches() && password.length() >= 8;
    }

    public static void disableCopyPaste(EditText editText) {
        editText.setLongClickable(false);
        editText.setTextIsSelectable(false);
        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });
    }

    private static final int IMAGE_MAX_SIZE = 816;

    public static int getIMAGE_MAX_SIZE() {
        return IMAGE_MAX_SIZE;
    }

    @Nullable
    public static String getCompressImagePath(@NotNull Uri uri, @NotNull Context mContext) {
        Intrinsics.checkParameterIsNotNull(uri, "uri");
        Intrinsics.checkParameterIsNotNull(mContext, "mContext");
        InputStream in = (InputStream) null;
        Bitmap returnedBitmap = (Bitmap) null;
        ContentResolver mContentResolver = null;
        String var5 = "";

        try {
            ContentResolver var10000 = mContext.getContentResolver();
            Intrinsics.checkExpressionValueIsNotNull(var10000, "mContext.contentResolver");
            mContentResolver = var10000;
            in = mContentResolver.openInputStream(uri);
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, (Rect) null, o);
            if (in == null) {
                Intrinsics.throwNpe();
            }

            in.close();
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2.0D, (double) ((int) Math.round(Math.log((double) IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5D))));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = mContentResolver.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(in, (Rect) null, o2);
            if (in == null) {
                Intrinsics.throwNpe();
            }

            in.close();
            ExifInterface ei = new ExifInterface(PathUtils.getPath(mContext, uri));
            int orientation = ei.getAttributeInt("Orientation", 1);
            switch (orientation) {
                case 3:
                    returnedBitmap = rotateImage(bitmap, 180.0F);
                    if (bitmap == null) {
                        Intrinsics.throwNpe();
                    }

                    bitmap.recycle();
                    bitmap = (Bitmap) null;
                    break;
                case 4:
                case 5:
                case 7:
                default:
                    returnedBitmap = bitmap;
                    break;
                case 6:
                    returnedBitmap = rotateImage(bitmap, 90.0F);
                    if (bitmap == null) {
                        Intrinsics.throwNpe();
                    }

                    bitmap.recycle();
                    bitmap = (Bitmap) null;
                    break;
                case 8:
                    returnedBitmap = rotateImage(bitmap, 270.0F);
                    if (bitmap == null) {
                        Intrinsics.throwNpe();
                    }

                    bitmap.recycle();
                    bitmap = (Bitmap) null;
            }

            String var23 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
            Intrinsics.checkExpressionValueIsNotNull(var23, "Environment.getExternalS…ageDirectory().toString()");
            String root = var23;
            File myDir = new File(root + "/Android/data/com.os.pikpak.activity/cache/image");
            myDir.mkdirs();
            long tsLong = System.currentTimeMillis();
            String timeStamp = String.valueOf(tsLong);
            String fname = timeStamp + "_" + ".jpg";
            File file = new File(myDir, fname);
            Log.i("file ", "" + file);
            if (file.exists()) {
                file.delete();
            }

            try {
                FileOutputStream out = new FileOutputStream(file);
                if (returnedBitmap == null) {
                    Intrinsics.throwNpe();
                }

                returnedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, (OutputStream) out);
                out.flush();
                out.close();
            } catch (Exception var20) {
                var20.printStackTrace();
            }

            return myDir.toString() + "/" + fname;
        } catch (FileNotFoundException var21) {
        } catch (IOException var22) {
        }

        return null;
    }

    private static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        if (source == null) {
            Intrinsics.throwNpe();
        }

        Bitmap var10000 = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
        Intrinsics.checkExpressionValueIsNotNull(var10000, "Bitmap.createBitmap(sour…rce.height, matrix, true)");
        return var10000;
    }

    public static File saveBitmapToFile(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
//            file.createNewFile();
            File file_out = saveFileAfterCompress();
            FileOutputStream outputStream = new FileOutputStream(file_out);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file_out;
        } catch (Exception e) {
            return null;
        }
    }

    public static File saveFileAfterCompress() {
        String timeStamp = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss")
                .format(new Date());
        String filename = "image_" + timeStamp;
        String fileNameExtension = ".jpg";
        File imageStorageFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()
                + File.separator
                + "mChamp/images");
        if (!imageStorageFolder.exists()) {
            imageStorageFolder.mkdirs();
        }
        return new File(imageStorageFolder, filename + fileNameExtension);
    }

    public static void setFantasyCredentials(String userId,String deviceId){
        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_M_USER_ID,userId);
        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_DEVICE_ID,deviceId);
    }
}
