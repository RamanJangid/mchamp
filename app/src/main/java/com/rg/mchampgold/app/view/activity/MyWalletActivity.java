package com.rg.mchampgold.app.view.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.dataModel.UserImageUploadResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.ProgressRequestBody;
import com.rg.mchampgold.app.view.addCash.BalanceFragment;
import com.rg.mchampgold.app.view.addCash.PlayingHistoryFragment;
import com.rg.mchampgold.app.view.addCash.TransactionsFragment;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityMyWalletBinding;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class MyWalletActivity extends AppCompatActivity implements ProgressRequestBody.UploadCallbacks {

    ActivityMyWalletBinding binding;

    @Inject
    OAuthRestService oAuthRestService;

    private MyBalanceResultItem myBalanceResultItem;
    Context context;
    TabAdapter mAdapter;


    String fileName = "";
    String Simage = "";
    private String fileImage = "";

    private static int GALLERY_REQUEST_CODE = 100;
    private static int CAMERA_REQUEST_CODE = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_wallet);
        MyApplication.getAppComponent().inject(MyWalletActivity.this);

        context = MyWalletActivity.this;

        AppUtils.loadImageCircle(binding.ivPlayer, MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_PIC));

        binding.btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, PersonalDetailsActivity.class));
            }
        });

        binding.btnVerifyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, VerifyAccountActivity.class));
            }
        });

        binding.tvUserName.setText(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_NAME));
        binding.tvUserEmail.setText(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL));

        setSupportActionBar(binding.mytoolbar);
        if (getSupportActionBar() != null) {
            //  getSupportActionBar().setTitle(getString(R.string.wallets));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mAdapter = new TabAdapter(getSupportFragmentManager());
        mAdapter.addFragment(new BalanceFragment(), "Balance");
        mAdapter.addFragment(new PlayingHistoryFragment(), "Playing History");
        mAdapter.addFragment(new TransactionsFragment(), "Transactions");
        binding.viewPager.setAdapter(mAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);


        binding.ivPlayer.setOnClickListener(view -> {
            if (checkCallingOrSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                showImageSelectionDialog();
            else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 22);
                }
            }
        });
    }

    public class TabAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void showImageSelectionDialog() {

        LayoutInflater inflater1 = getLayoutInflater();
        View alertLayout = inflater1.inflate(R.layout.layout_pic_upload, null);

        final TextView tvGallery = alertLayout.findViewById(R.id.tv_gallery);
        final TextView tvCamera = alertLayout.findViewById(R.id.tv_camera);
        final TextView tvCancel = alertLayout.findViewById(R.id.tv_cancel);

        /*AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);
        AlertDialog alert = builder.create();*/

        Dialog dialog = new Dialog(MyWalletActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(alertLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        tvGallery.setOnClickListener(view1 -> {
            dialog.dismiss();
            openGallery();
        });


        tvCamera.setOnClickListener(view12 -> {
            dialog.dismiss();
            openCamera();
        });

        tvCancel.setOnClickListener(view13 -> dialog.dismiss());
        dialog.show();


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 22) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageSelectionDialog();
            }
//            else {
//                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 22);
//            }
        }
    }


    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), GALLERY_REQUEST_CODE);
    }

    private void openCamera() {

        dispatchTakePictureIntent();

    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        getPackageName() + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean flag = false;
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri image = data.getData();
                flag = true;
                fileImage = image == null ? "" : AppUtils.getCompressImagePath(image, MyWalletActivity.this);

            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                flag = true;
                fileImage = currentPhotoPath;
            }
        }
        if (fileImage != null) {
            if (!fileImage.equalsIgnoreCase("")) {
                if (flag) {
                    uploadUserImage();
                    return;
                }
            }
        }
        AppUtils.showErrorr((AppCompatActivity) MyWalletActivity.this, "Can't load this file");
    }

    ProgressDialog pDialog = null;

    private void showProgress() {
        pDialog = new ProgressDialog(MyWalletActivity.this);
        pDialog.setMessage("Uploading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.show();
    }


    private void uploadUserImage() {
//        binding.setRefreshing(true);
        showProgress();
        String userId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);
        RequestBody requestBodyUserId = RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        File file = new File(fileImage);
        long length = (file.length() / (1024 * 1024));
        if (length > 1) {
            file = AppUtils.saveBitmapToFile(file);
            if (file == null) {
                return;
            }
        }
        ProgressRequestBody progressRequestBody = new ProgressRequestBody(file, "multipart/form-data", this);
//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part uploadPic = MultipartBody.Part.createFormData("file", file.getName(), progressRequestBody);
        CustomCallAdapter.CustomCall<UserImageUploadResponse> imageUploadResponseCustomCall = oAuthRestService.uploadUserImage(requestBodyUserId, uploadPic);
        imageUploadResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<UserImageUploadResponse>() {
            @Override
            public void success(Response<UserImageUploadResponse> response) {
//                binding.setRefreshing(false);
                if (pDialog != null) pDialog.dismiss();

                UserImageUploadResponse imageUploadResponse = response.body();
                if (imageUploadResponse.getStatus() == 1) {
                    if (imageUploadResponse.getResult().get(0).getStatus() == 1) {
                        fileName = imageUploadResponse.getResult().get(0).getImage();

                        //    Log.e("filename", fileName);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_PIC, fileName);
                        AppUtils.loadImageCircle(binding.ivPlayer, MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_PIC));
                        AppUtils.showSuccess(MyWalletActivity.this, "Image uploaded.");
                    } else {
                        AppUtils.showErrorr(MyWalletActivity.this, "Error in image uploading.");
                    }
                } else {
                    AppUtils.showErrorr(MyWalletActivity.this, imageUploadResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                if (pDialog != null)
                    pDialog.dismiss();

//                binding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {
        pDialog.setProgress(percentage);
    }

    @Override
    public void onError() {
        pDialog.dismiss();
    }

    @Override
    public void onFinish() {
        pDialog.setProgress(100);
    }

}
