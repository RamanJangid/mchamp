package com.rg.mchampgold.app.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Team;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.adapter.TeamItemAdapter;
import com.rg.mchampgold.app.viewModel.TeamViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentMyTeamBinding;

import java.util.ArrayList;

import javax.inject.Inject;


public class MyTeamFragment extends Fragment {

    FragmentMyTeamBinding mBinding;
    TeamItemAdapter mAdapter;
    private TeamViewModel teamViewModel;
    String matchKey;
    String sportKey;
    ArrayList<Team> list = new ArrayList<>();
    int teamCount;
    Context context;
    boolean isForJoinContest;
    public int joinedContestCount = 0;
    @Inject
    OAuthRestService oAuthRestService;


    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_team, container, false);
        setupRecyclerView();
        /* UpComingContestActivity.fab_create_team.setVisibility(View.VISIBLE);*/
        mBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            getData();
            mBinding.swipeRefreshLayout.setRefreshing(false);
        });
        return mBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        teamViewModel = TeamViewModel.create(this);
        MyApplication.getAppComponent().inject(teamViewModel);
        if (getArguments() != null) {
            matchKey = getArguments().getString(Constants.KEY_MATCH_KEY);
            sportKey = getArguments().getString(Constants.SPORT_KEY);
        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
    }


    private void setupRecyclerView() {
        mAdapter = new TeamItemAdapter(list, getActivity(), isForJoinContest, sportKey, 0, true, 0);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
        mBinding.noTeamCreateButton.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).creteTeam());
        mBinding.btnCreateTeam.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).creteTeam());
        mBinding.fabCreateTeam.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).creteTeam());
    }


    private void getData() {
        MyTeamRequest request = new MyTeamRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        request.setChallengeId(0 + "");
        teamViewModel.loadMyTeamRequest(request);
        teamViewModel.getContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1) {
                        if (arrayListResource.getData().getTeamITem().getTeams().size() > 0) {
                            mBinding.rlNoTeam.setVisibility(View.GONE);
                            mBinding.rlMainLayout.setVisibility(View.VISIBLE);
                            list = arrayListResource.getData().getTeamITem().getTeams();
                            teamCount = arrayListResource.getData().getTeamITem().getUserTeams();
                            joinedContestCount = arrayListResource.getData().getTeamITem().getJoined_leagues();

                            mAdapter.updateData(list, 0);
                            if (getActivity() != null && getActivity() instanceof UpComingContestActivity)
                                ((UpComingContestActivity) getActivity()).setTabTitle(teamCount, joinedContestCount);
                        } else {
                            mBinding.rlNoTeam.setVisibility(View.VISIBLE);
                            mBinding.rlMainLayout.setVisibility(View.GONE);
                        }


                        setTeamCreateButtonName();
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }


    private void setTeamCreateButtonName() {


        if (teamCount > 11) {
            mBinding.btnCreateTeam.setVisibility(View.GONE);
        } else {
            if (teamCount == 0) {
                mBinding.btnCreateTeam.setVisibility(View.GONE);
                mBinding.noTeamCreateButton.setVisibility(View.VISIBLE);
            } else {
                mBinding.btnCreateTeam.setVisibility(View.VISIBLE);
                mBinding.noTeamCreateButton.setVisibility(View.GONE);
            }
        }

        mBinding.btnCreateTeam.setText("Create Team " + (teamCount + 1));


       /* if(teamCount>=6)
            mBinding.fabCreateTeam.setVisibility(View.GONE);
        else
            mBinding.fabCreateTeam.setVisibility(View.VISIBLE);*/
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisible()) {
            if (isVisibleToUser) {
                //  getData();
            }
        }
    }


}