package com.rg.mchampgold.app.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.SeriesDataModel;
import com.rg.mchampgold.app.dataModel.SeriesLeaderbardData;
import com.rg.mchampgold.app.dataModel.SeriesLeaderbardResponse;
import com.rg.mchampgold.app.dataModel.SeriesLeaderboardDataModel;
import com.rg.mchampgold.app.dataModel.SeriesResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.LeaderboardListAdapter;
import com.rg.mchampgold.app.view.adapter.SpinnerAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.LeaderboardLayoutBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class LeaderboardActivity extends AppCompatActivity {

    @Inject
    OAuthRestService oAuthRestService;
    ArrayList<SeriesDataModel> seriesList = new ArrayList<>();
    ArrayList<SeriesLeaderboardDataModel> leaderboardList = new ArrayList<>();
    ArrayList<SeriesLeaderboardDataModel> leaderboardRankList = new ArrayList<>();
    //    ArrayList<WinningBreakupDataModel> breakupList=new ArrayList<>();
    int selectedSeriesPos = 0;
    String selectedSeriesId;
    LeaderboardLayoutBinding mBinding;
    String bannerImageUrl;
    String infoUrl;
    private int seriesId;
    private int isLeaderboard;
    private boolean isFirstTime = true;
    private boolean isDummyData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(LeaderboardActivity.this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.leaderboard_layout);

        if (getIntent() != null && getIntent().getExtras() != null) {
            seriesId = getIntent().getExtras().getInt(Constants.KEY_SERIES_ID, 0);
            isLeaderboard = getIntent().getExtras().getInt(Constants.KEY_IS_LEADERBOARD, 0);
        }

        mBinding.frameLayout.setOnClickListener(v -> mBinding.spinner.performClick());


        mBinding.rank1layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDummyData) {
                    Intent intent = new Intent(getApplicationContext(), LeaderboardDetailsActivity.class);
                    intent.putExtra("series_id", leaderboardRankList.get(0).getSeries_id());
                    intent.putExtra("user_id", leaderboardRankList.get(0).getUser_id());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
        mBinding.rank2layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDummyData) {
                    Intent intent = new Intent(getApplicationContext(), LeaderboardDetailsActivity.class);
                    intent.putExtra("series_id", leaderboardRankList.get(1).getSeries_id());
                    intent.putExtra("user_id", leaderboardRankList.get(1).getUser_id());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
        mBinding.rank3layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDummyData) {
                    Intent intent = new Intent(getApplicationContext(), LeaderboardDetailsActivity.class);
                    intent.putExtra("series_id", leaderboardRankList.get(2).getSeries_id());
                    intent.putExtra("user_id", leaderboardRankList.get(2).getUser_id());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        });
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBinding.ivLeaderboardBanner.setOnClickListener(v -> AppUtils.openWebViewActivity("Leaderboard T&C", infoUrl));
        mBinding.tvMore.setOnClickListener(v -> AppUtils.openWebViewActivity("Leaderboard T&C", infoUrl));

        getSeries();
    }

    public void getSeries() {

        mBinding.setRefreshing(true);
        CustomCallAdapter.CustomCall<SeriesResponse> bankDetailResponseCustomCall = oAuthRestService.getSeries();
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<SeriesResponse>() {
            @Override
            public void success(Response<SeriesResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        seriesList = response.body().getResult();
                        if (seriesList != null) {
                            if (seriesList.size() > 0) {
                                mBinding.scrollView.setVisibility(View.VISIBLE);
                                mBinding.tvMore.setVisibility(View.VISIBLE);


                                /*selectedSeriesPos = 0;
                                mBinding.tvSeriesName.setText(seriesList.get(selectedSeriesPos).getName());
                                selectedSeriesId = seriesList.get(selectedSeriesPos).getId();*/

                                String[] strings = new String[seriesList.size()];
                                for (int i = 0; i < seriesList.size(); i++) {
                                    strings[i] = seriesList.get(i).getName();
                                }
                                mBinding.spinner.setAdapter(new SpinnerAdapter(LeaderboardActivity.this, strings));


                                mBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        if (seriesId != 0 && isLeaderboard == 1 && isFirstTime) {
                                            for (SeriesDataModel series : seriesList) {
                                                if (Integer.parseInt(series.getId()) == seriesId) {
                                                    mBinding.tvSeriesName.setText(series.getName());
                                                    selectedSeriesId = series.getId();
                                                    infoUrl = series.getBanner_image_url();
                                                    bannerImageUrl = series.getBanner_image();

                                                    if (series.getIs_banner_visible() == 1) {
                                                        mBinding.ivLeaderboardBanner.setVisibility(View.VISIBLE);
                                                    } else {
                                                        mBinding.ivLeaderboardBanner.setVisibility(View.GONE);
                                                    }
                                                }
                                            }
                                        } else {
                                            mBinding.tvSeriesName.setText(seriesList.get(position).getName());
                                            selectedSeriesId = seriesList.get(position).getId();
                                            infoUrl = seriesList.get(position).getBanner_image_url();
                                            bannerImageUrl = seriesList.get(position).getBanner_image();

                                            if (seriesList.get(position).getIs_banner_visible() == 1) {
                                                mBinding.ivLeaderboardBanner.setVisibility(View.VISIBLE);
                                            } else {
                                                mBinding.ivLeaderboardBanner.setVisibility(View.GONE);
                                            }
                                        }

                                        if (bannerImageUrl != null && !bannerImageUrl.equals("")) {
                                            Picasso.get()
                                                    .load(bannerImageUrl.replace(" ", "%20"))
                                                    .placeholder(R.drawable.bg_leaderboard_banner)
                                                    .error(R.drawable.bg_leaderboard_banner)
                                                    .into(mBinding.ivLeaderboardBanner);
                                        } else {
                                            mBinding.ivLeaderboardBanner.setImageResource(R.drawable.bg_leaderboard_banner);
                                        }
                                        getSeriesLeaderboard();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }
                        }
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    public void getSeriesLeaderboard() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setSeries_id(selectedSeriesId);
        CustomCallAdapter.CustomCall<SeriesLeaderbardResponse> bankDetailResponseCustomCall = oAuthRestService.getSeriesLeaderboard(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<SeriesLeaderbardResponse>() {
            @Override
            public void success(Response<SeriesLeaderbardResponse> response) {
                mBinding.setRefreshing(false);
                isFirstTime = false;
                if (response.isSuccessful() && response.body() != null) {
                    SeriesLeaderbardData seriesLeaderbardData = response.body().getResult();
                    leaderboardRankList.clear();
                    leaderboardList = seriesLeaderbardData.getLeaderboard();

                    isDummyData = seriesLeaderbardData.isIs_dummy_data();

                    if (leaderboardList != null) {
                        if (leaderboardList.size() < 3) {
                            mBinding.playerRanksLayout.setVisibility(View.GONE);
                            mBinding.listLayout.setVisibility(View.GONE);
                        } else {
                            mBinding.playerRanksLayout.setVisibility(View.VISIBLE);
                            mBinding.listLayout.setVisibility(View.VISIBLE);
                            AppUtils.loadImage(mBinding.rankImage1, leaderboardList.get(0).getImage());
                            mBinding.points1.setText(leaderboardList.get(0).getPoints() + " pts");
                            mBinding.name1.setText(leaderboardList.get(0).getTeam());
                            leaderboardRankList.add(leaderboardList.get(0));
                            leaderboardList.remove(0);

                            AppUtils.loadImage(mBinding.rankImage2, leaderboardList.get(0).getImage());
                            mBinding.points2.setText(leaderboardList.get(0).getPoints() + " pts");
                            mBinding.name2.setText(leaderboardList.get(0).getTeam());
                            leaderboardRankList.add(leaderboardList.get(0));
                            leaderboardList.remove(0);

                            AppUtils.loadImage(mBinding.rankImage3, leaderboardList.get(0).getImage());
                            mBinding.points3.setText(leaderboardList.get(0).getPoints() + " pts");
                            mBinding.name3.setText(leaderboardList.get(0).getTeam());
                            leaderboardRankList.add(leaderboardList.get(0));
                            leaderboardList.remove(0);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LeaderboardActivity.this);
                            mBinding.recyclerView.setLayoutManager(mLayoutManager);
                            mBinding.recyclerView.setAdapter(new LeaderboardListAdapter(getApplicationContext(), leaderboardList, isDummyData));
                        }
                    } else {
                        mBinding.playerRanksLayout.setVisibility(View.GONE);
                        mBinding.listLayout.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                isFirstTime = false;
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }
}
