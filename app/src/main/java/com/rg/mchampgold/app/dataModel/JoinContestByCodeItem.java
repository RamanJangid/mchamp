package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class JoinContestByCodeItem {

	@SerializedName("entryfee")
	private double entryfee;

	@SerializedName("challengeid")
	private int challengeid;

	@SerializedName("marathon")
	private int marathon;

	@SerializedName("message")
	private String message;

	public void setEntryfee(int entryfee){
		this.entryfee = entryfee;
	}

	public double getEntryfee(){
		return entryfee;
	}

	public void setChallengeid(int challengeid){
		this.challengeid = challengeid;
	}

	public int getChallengeid(){
		return challengeid;
	}

	public void setMarathon(int marathon){
		this.marathon = marathon;
	}

	public int getMarathon(){
		return marathon;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"JoinContestByCodeItem{" +
			"entryfee = '" + entryfee + '\'' + 
			",challengeid = '" + challengeid + '\'' + 
			",marathon = '" + marathon + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}