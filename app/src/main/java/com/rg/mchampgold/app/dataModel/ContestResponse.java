package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ContestResponse{

	@SerializedName("result")
	private ContestData result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;


	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ContestData getResult() {
		return result;
	}

	public void setResult(ContestData result) {
		this.result = result;
	}
}