package com.rg.mchampgold.app.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.OtpVerfiyRequest;
import com.rg.mchampgold.app.api.response.RegisterResponse;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.SendOtpResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.IncomingSMSListener;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityOtpVerifyBinding;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import retrofit2.Response;


public class OtpVerifyActivity extends AppCompatActivity implements IncomingSMSListener {


    ActivityOtpVerifyBinding mainBinding;
    String mobileNo = "";
    String otp = "";
    String userId = "";


    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        MyApplication.getAppComponent().inject(OtpVerifyActivity.this);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_otp_verify);
        startSMS_Tracking();
        initialize();
    }

    private void initialize() {
        setSupportActionBar(mainBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("OTP Verify");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().containsKey("MOBILE")) {
                mobileNo = getIntent().getExtras().getString("MOBILE");
                userId = getIntent().getExtras().getString("userId");
            }
        }

        mainBinding.tvMobile.setText("+91 " + mobileNo);
        mainBinding.resendTxt.setVisibility(View.VISIBLE);

        long diffInMs = Constants.OTP_SEND_TIME;

        CountDownTimer cT = new CountDownTimer(diffInMs, 1000) {

            public void onTick(long millisUntilFinished) {
                mainBinding.resendTxt.setText(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) < 10 ? "00:0" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) :
                        "00:" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));

                mainBinding.resendTxt.setClickable(false);
            }

            public void onFinish() {
                mainBinding.resendTxt.setText(Html.fromHtml("<u>" + getString(R.string.resend_otp) + "</u>"));
                mainBinding.resendTxt.setClickable(true);

            }
        };
        cT.start();


      /*  mainBinding.etPinFirst.addTextChangedListener(new GenericTextWatcher(mainBinding.etPinFirst));
        mainBinding.etPinSecond.addTextChangedListener(new GenericTextWatcher(mainBinding.etPinSecond));
        mainBinding.etPinThird.addTextChangedListener(new GenericTextWatcher(mainBinding.etPinThird));
        mainBinding.etPinFourth.addTextChangedListener(new GenericTextWatcher(mainBinding.etPinFourth));
        mainBinding.etPinFifth.addTextChangedListener(new GenericTextWatcher(mainBinding.etPinFifth));
        mainBinding.etPinSix.addTextChangedListener(new GenericTextWatcher(mainBinding.etPinSix));*/

        mainBinding.btnSubmit.setOnClickListener(v -> {

            otp = mainBinding.otpLayout.getText().toString().trim();
          /*  otp = mainBinding.etPinFirst.getText().toString().trim()
                    + mainBinding.etPinSecond.getText().toString().trim()
                    + mainBinding.etPinThird.getText().toString().trim()
                    + mainBinding.etPinFourth.getText().toString().trim()
                    + mainBinding.etPinFifth.getText().toString().trim()
                    + mainBinding.etPinSix.getText().toString().trim();*/
            if (!otp.equals("") && otp.length() == 6) {
                otpVerify();
            } else {
                Toast.makeText(OtpVerifyActivity.this, "Please enter your six digit otp", Toast.LENGTH_SHORT).show();
            }
        });


        mainBinding.resendTxt.setOnClickListener(view -> resendOTP());
    }


    private void otpVerify() {
        mainBinding.setRefreshing(true);
        OtpVerfiyRequest otpVerfiyRequest = new OtpVerfiyRequest();
        otpVerfiyRequest.setMobile(mobileNo);
        otpVerfiyRequest.setOtp(otp);
        //  if(!MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID).equalsIgnoreCase(""))
        //  otpVerfiyRequest.setUserId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        otpVerfiyRequest.setUserId(userId);
        CustomCallAdapter.CustomCall<RegisterResponse> userLogin = oAuthRestService.otpRegisterVerify(otpVerfiyRequest);
        userLogin.enqueue(new CustomCallAdapter.CustomCallback<RegisterResponse>() {
            @Override
            public void success(Response<RegisterResponse> response) {
                mainBinding.setRefreshing(false);
                mainBinding.resendTxt.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body() != null) {
                    RegisterResponse registerResponse = response.body();
                    if (registerResponse.getStatus() == 1) {
                        MyApplication.tinyDB.putBoolean(Constants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_ID, registerResponse.getResult().getUserId() + "");
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_NAME, registerResponse.getResult().getUsername());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_MOBILE, registerResponse.getResult().getMobile());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_EMAIL, registerResponse.getResult().getEmail());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TOKEN, registerResponse.getResult().getCustomUserToken());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_REFER_CODE, registerResponse.getResult().getRefercode());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, registerResponse.getResult().getBankVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, registerResponse.getResult().getPanVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, registerResponse.getResult().getMobileVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, registerResponse.getResult().getEmailVerify());
                        MyApplication.tinyDB.putString(Constants.AUTHTOKEN, registerResponse.getResult().getCustomUserToken());
                        Intent intent = new Intent(OtpVerifyActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(OtpVerifyActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(OtpVerifyActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mainBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    private void resendOTP() {
        mainBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setMobile(mobileNo);
        baseRequest.setUser_id(userId);
        baseRequest.setType("1");
        CustomCallAdapter.CustomCall<SendOtpResponse> userLogin = oAuthRestService.sendOTP(baseRequest);
        userLogin.enqueue(new CustomCallAdapter.CustomCallback<SendOtpResponse>() {
            @Override
            public void success(Response<SendOtpResponse> response) {
                mainBinding.setRefreshing(false);
                mainBinding.resendTxt.setVisibility(View.VISIBLE);
                if (response.isSuccessful() && response.body() != null) {
                    SendOtpResponse sendOtpResponse = response.body();
                    Toast.makeText(OtpVerifyActivity.this, sendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    long diffInMs = Constants.OTP_SEND_TIME;
                    CountDownTimer cT = new CountDownTimer(diffInMs, 1000) {

                        public void onTick(long millisUntilFinished) {
                            mainBinding.resendTxt.setText(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) < 10 ? "00:0" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) :
                                    "00:" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));
                            mainBinding.resendTxt.setClickable(false);
                        }

                        public void onFinish() {
                            mainBinding.resendTxt.setText(Html.fromHtml("<u>" + getString(R.string.resend_otp) + "</u>"));
                            mainBinding.resendTxt.setClickable(true);
                        }
                    };
                    cT.start();
                } else {
                    Toast.makeText(OtpVerifyActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mainBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onOTPReceived(String otp) {
        //char otp_digits[] = otp.toCharArray();
        if (mainBinding != null) {

            mainBinding.otpLayout.setText(otp);
            /*mainBinding.etPinFirst.setText(otp_digits[0] + "");
            mainBinding.etPinSecond.setText(otp_digits[1] + "");
            mainBinding.etPinThird.setText(otp_digits[2] + "");
            mainBinding.etPinFourth.setText(otp_digits[3] + "");
            mainBinding.etPinFifth.setText(otp_digits[4] + "");
            mainBinding.etPinSix.setText(otp_digits[5] + "");*/
         /*   this.otp = mainBinding.etPinFirst.getText().toString().trim()
                    + mainBinding.etPinSecond.getText().toString().trim()
                    + mainBinding.etPinThird.getText().toString().trim()
                    + mainBinding.etPinFourth.getText().toString().trim()
                    + mainBinding.etPinFifth.getText().toString().trim()
                    + mainBinding.etPinSix.getText().toString().trim();*/


            this.otp = mainBinding.otpLayout.getText().toString().trim();
            if (otp !=null && !otp.equalsIgnoreCase(""))
            otpVerify();
        }
    }

    @Override
    public void onOTPTimeOut() {
        AppUtils.showErrorr(this, "Timeout");
    }

 /*   public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.et_pin_first:
                    if (text.length() == 1)
                        mainBinding.etPinSecond.requestFocus();
                    break;
                case R.id.et_pin_second:
                    if (text.length() == 1)
                        mainBinding.etPinThird.requestFocus();
                    else if (text.length() == 0)
                        mainBinding.etPinFirst.requestFocus();
                    break;
                case R.id.et_pin_third:
                    if (text.length() == 1)
                        mainBinding.etPinFourth.requestFocus();
                    else if (text.length() == 0)
                        mainBinding.etPinSecond.requestFocus();
                    break;
                case R.id.et_pin_fourth:
                    if (text.length() == 1)
                        mainBinding.etPinFifth.requestFocus();
                    else if (text.length() == 0)
                        mainBinding.etPinThird.requestFocus();
                    break;

                case R.id.et_pin_fifth:
                    if (text.length() == 1)
                        mainBinding.etPinSix.requestFocus();
                    else if (text.length() == 0)
                        mainBinding.etPinFourth.requestFocus();
                    break;

                case R.id.et_pin_six:
                    if (text.length() == 0)
                        mainBinding.etPinFifth.requestFocus();
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void startSMS_Tracking() {
        MyApplication.listner=this;
        SmsRetrieverClient client = SmsRetriever.getClient(this);

        // Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(aVoid -> {
            // Successfully started retriever, expect broadcast intent

            // ...
//            IntentFilter filter1 = new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED");
//            registerReceiver(new MySMSBroadcastReceiver(this), filter1);


            //   IntentFilter filter2 = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            //  registerReceiver(new IncomingSmsReceiver(this), filter2);

        });

        task.addOnFailureListener(e -> {
            // Failed to start retriever, inspect Exception for more details
            // ...
          //  Log.e("CA", "Error");
        });


    }

}