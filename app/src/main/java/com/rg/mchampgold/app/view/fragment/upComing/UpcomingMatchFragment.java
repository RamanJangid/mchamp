package com.rg.mchampgold.app.view.fragment.upComing;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.dataModel.Match;
import com.rg.mchampgold.app.dataModel.MatchListResponse;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.adapter.MatchItemAdapter;
import com.rg.mchampgold.app.view.interfaces.OnMatchItemClickListener;
import com.rg.mchampgold.common.api.Resource;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentGeneralBinding;

import java.util.ArrayList;


public class UpcomingMatchFragment extends Fragment implements OnMatchItemClickListener {

    private FragmentGeneralBinding fragmentGeneralBinding;
    private MatchItemAdapter mAdapter;
    private UpComingMatchListViewModel upComingMatchListViewModel;
    private ArrayList<Match> list = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentGeneralBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_general, container, false);
        return fragmentGeneralBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        upComingMatchListViewModel = UpComingMatchListViewModel.create(this);
        MyApplication.getAppComponent().inject(upComingMatchListViewModel);
        setupRecyclerView();
        getData(upComingMatchListViewModel.getSearchData());
    }

    private void setupRecyclerView() {
        mAdapter = new MatchItemAdapter(getActivity(),list,this, null);
        fragmentGeneralBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        fragmentGeneralBinding.recyclerView.setLayoutManager(mLayoutManager);
        fragmentGeneralBinding.recyclerView.setAdapter(mAdapter);
    }





    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }


    private void getData(LiveData<Resource<MatchListResponse>> liveData) {
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        upComingMatchListViewModel.load(baseRequest);
        liveData.observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                     fragmentGeneralBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                     fragmentGeneralBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext,arrayListResource.getException().getErrorModel().errorMessage,Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                   // Log.e("callingSucess","UPcoming");
                    fragmentGeneralBinding.setRefreshing(false);
                    if(arrayListResource.getData().getStatus()==1) {
                        list = arrayListResource.getData().getResult();
                        if(list.size()>0) {
                            setupRecyclerView();
                            fragmentGeneralBinding.rlNoMatch.setVisibility(View.GONE);
                        }
                        else {
                            fragmentGeneralBinding.rlNoMatch.setVisibility(View.VISIBLE);
                        }
                    }
                    else {
                      Toast.makeText(MyApplication.appContext,arrayListResource.getData().getMessage(),Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }


    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onMatchItemClick(String matchKey, String teamVsName, String teamFirstUrl, String teamSecondUrl, String timerStatus, int position, int isLeaderboard, int series) {
      //  Log.e("Upcoming",matchKey);
        Intent intent = new Intent(getActivity(), UpComingContestActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY,matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS,teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL,teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL,teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT,timerStatus);
        intent.putExtra(Constants.SPORT_KEY,list.get(position).getSport_key());
        startActivity(intent);
    }
}