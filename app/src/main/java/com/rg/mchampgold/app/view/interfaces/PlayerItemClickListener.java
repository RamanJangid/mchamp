package com.rg.mchampgold.app.view.interfaces;

public interface PlayerItemClickListener {
    void onPlayerClick(boolean isSelect, int position, int type);
}
