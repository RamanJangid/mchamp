package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PlayerInfoResult {

	@SerializedName("country")
	private String country;

	@SerializedName("playerimage")
	private String playerimage;

	@SerializedName("teams")
	private String teams;

	@SerializedName("battingstyle")
	private String battingstyle;

	@SerializedName("total_points")
	private String totalPoints;

	@SerializedName("playerpoints")
	private int playerpoints;

	@SerializedName("matches")
	private ArrayList<PlayerInfoMatchesItem> matches;

	@SerializedName("playerrole")
	private String playerrole;

	@SerializedName("playerkey")
	private String playerkey;

	@SerializedName("bowlingstyle")
	private String bowlingstyle;

	@SerializedName("dob")
	private String dob;

	@SerializedName("playercredit")
	private double playercredit;

	@SerializedName("playername")
	private String playername;

	@SerializedName("per")
	private double per;


	public void setPlayerimage(String playerimage){
		this.playerimage = playerimage;
	}

	public String getPlayerimage(){
		return playerimage;
	}

	public void setTeams(String teams){
		this.teams = teams;
	}

	public String getTeams(){
		return teams;
	}

	public void setBattingstyle(String battingstyle){
		this.battingstyle = battingstyle;
	}

	public String getBattingstyle(){
		return battingstyle;
	}

	public void setTotalPoints(String totalPoints){
		this.totalPoints = totalPoints;
	}

	public String getTotalPoints(){
		return totalPoints;
	}

	public void setPlayerpoints(int playerpoints){
		this.playerpoints = playerpoints;
	}

	public int getPlayerpoints(){
		return playerpoints;
	}

	public void setMatches(ArrayList<PlayerInfoMatchesItem> matches){
		this.matches = matches;
	}

	public ArrayList<PlayerInfoMatchesItem> getMatches(){
		return matches == null?new ArrayList<>():matches;
	}

	public void setPlayerrole(String playerrole){
		this.playerrole = playerrole;
	}

	public String getPlayerrole(){
		return playerrole;
	}

	public void setPlayerkey(String playerkey){
		this.playerkey = playerkey;
	}

	public String getPlayerkey(){
		return playerkey;
	}

	public void setBowlingstyle(String bowlingstyle){
		this.bowlingstyle = bowlingstyle;
	}

	public String getBowlingstyle(){
		return bowlingstyle;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setPlayercredit(double playercredit){
		this.playercredit = playercredit;
	}

	public double getPlayercredit(){
		return playercredit;
	}

	public void setPlayername(String playername){
		this.playername = playername;
	}

	public String getPlayername(){
		return playername;
	}

	public void setPer(double per){
		this.per = per;
	}

	public double getPer(){
		return per;
	}

	@Override
 	public String toString(){
		return 
			"PlayerInfoResult{" +
			"country = '" + country + '\'' + 
			",playerimage = '" + playerimage + '\'' + 
			",teams = '" + teams + '\'' + 
			",battingstyle = '" + battingstyle + '\'' + 
			",total_points = '" + totalPoints + '\'' + 
			",playerpoints = '" + playerpoints + '\'' + 
			",matches = '" + matches + '\'' + 
			",playerrole = '" + playerrole + '\'' + 
			",playerkey = '" + playerkey + '\'' + 
			",bowlingstyle = '" + bowlingstyle + '\'' + 
			",dob = '" + dob + '\'' + 
			",playercredit = '" + playercredit + '\'' + 
			",playername = '" + playername + '\'' + 
			",per = '" + per + '\'' + 
			"}";
		}


		public String playerCredit() {
			return String.valueOf(playercredit);
		}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public String showPlayerPoint(){
		return ""+playerpoints;
	}

	public String getPlayerPerct(){
		return per+"%";
	}
}