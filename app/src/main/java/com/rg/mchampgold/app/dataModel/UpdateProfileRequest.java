package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class UpdateProfileRequest {

    @SerializedName("country")
    private String country;

    @SerializedName("pincode")
    private String pincode;

    @SerializedName("address")
    private String address;

    @SerializedName("gender")
    private String gender;

    @SerializedName("city")
    private String city;

    @SerializedName("dob")
    private String dob;

    @SerializedName("allow_notfy")
    private String allowNotfy;

    @SerializedName("user_id")
    private String id;

    @SerializedName("state")
    private String state;

    @SerializedName("team")
    private String team;

    @SerializedName("username")
    private String username;

    @SerializedName("mobile")
    private String mobile;

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPincode() {
        return pincode;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDob() {
        return dob;
    }

    public void setAllowNotfy(String allowNotfy) {
        this.allowNotfy = allowNotfy;
    }

    public String getAllowNotfy() {
        return allowNotfy;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getTeam() {
        return team;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return
                "UpdateProfileRequest{" +
                        "country = '" + country + '\'' +
                        ",pincode = '" + pincode + '\'' +
                        ",address = '" + address + '\'' +
                        ",gender = '" + gender + '\'' +
                        ",city = '" + city + '\'' +
                        ",dob = '" + dob + '\'' +
                        ",allow_notfy = '" + allowNotfy + '\'' +
                        ",id = '" + id + '\'' +
                        ",state = '" + state + '\'' +
                        ",team = '" + team + '\'' +
                        ",username = '" + username + '\'' +
                        "}";
    }
}