package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.BannerListItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.databinding.BannerListItemLayoutBinding;

import java.util.ArrayList;

public class BannerListAdapter extends RecyclerView.Adapter<BannerListAdapter.ViewHolder> {

    private ArrayList<BannerListItem> bannerListItems;
    private AdapterView.OnItemClickListener listener;
    Context context;


    class ViewHolder extends RecyclerView.ViewHolder {
        final BannerListItemLayoutBinding binding;

        ViewHolder(BannerListItemLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public BannerListAdapter(Context context, ArrayList<BannerListItem> bannerListItems, AdapterView.OnItemClickListener listener) {
        this.context = context;
        this.bannerListItems = bannerListItems;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BannerListItemLayoutBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.banner_list_item_layout,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        AppUtils.loadImageBanner(holder.binding.bannerImage, bannerListItems.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return bannerListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }
}