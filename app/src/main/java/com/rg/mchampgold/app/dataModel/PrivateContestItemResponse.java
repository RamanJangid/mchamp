package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PrivateContestItemResponse {

	@SerializedName("challengeid")
	private int challengeid;

	@SerializedName("message")
	private boolean message;

	@SerializedName("status")
	private int status;

	public void setChallengeid(int challengeid){
		this.challengeid = challengeid;
	}

	public int getChallengeid(){
		return challengeid;
	}

	public void setMessage(boolean message){
		this.message = message;
	}

	public boolean isMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PrivateContestItemResponse{" +
			"challengeid = '" + challengeid + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}