package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class MatchListResponse {

	@SerializedName("result")
	private ArrayList<Match> result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public ArrayList<Match> getResult() {
		return result;
	}

	public void setResult(ArrayList<Match> result) {
		this.result = result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MatchListResponse{" +
			"result = '" + result + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}