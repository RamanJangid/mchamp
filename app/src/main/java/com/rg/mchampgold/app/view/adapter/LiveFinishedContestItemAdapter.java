package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.LiveFinishedContestData;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.databinding.RecyclerItemLiveFinishedBinding;

import java.util.ArrayList;
import java.util.List;

public class LiveFinishedContestItemAdapter extends RecyclerView.Adapter<LiveFinishedContestItemAdapter.ViewHolder> {

    private List<LiveFinishedContestData> liveFinishedContestData;
    OnContestItemClickListener listener;
    Context context;
    private boolean isFromFinished;
    String category = "";

    static class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerItemLiveFinishedBinding binding;

        ViewHolder(RecyclerItemLiveFinishedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public LiveFinishedContestItemAdapter(Context context, List<LiveFinishedContestData> liveFinishedContestData, OnContestItemClickListener listener, boolean isFromFinished) {
        this.liveFinishedContestData = liveFinishedContestData;
        this.context = context;
        this.listener = listener;
        this.isFromFinished = isFromFinished;
        category = "";
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerItemLiveFinishedBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_live_finished,
                        parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.setContestData(liveFinishedContestData.get(position));

        holder.itemView.setOnClickListener(view -> {
            Contest contest = new Contest();
            contest.setId(liveFinishedContestData.get(position).getChallengeId());
            contest.setGetjoinedpercentage(liveFinishedContestData.get(position).getGetjoinedpercentage() + "");
            contest.setChallenge_type(liveFinishedContestData.get(position).getChallengeType());
            contest.setFirst_rank_prize(liveFinishedContestData.get(position).getFirst_rank_prize());
            contest.setRefercode(liveFinishedContestData.get(position).getRefercode());
            contest.setPdf(liveFinishedContestData.get(position).getPdf());
            contest.setEntryfee(liveFinishedContestData.get(position).getEntryfee() + "");
            contest.setTotalwinners(liveFinishedContestData.get(position).getTotalwinners());
            contest.setWinAmount(liveFinishedContestData.get(position).getWinAmount());
            contest.setMaximumUser(liveFinishedContestData.get(position).getMaximumUser());
            contest.setJoinedusers(liveFinishedContestData.get(position).getJoinedusers());
            contest.setMultiEntry(liveFinishedContestData.get(position).getMultiEntry());
            contest.setMaxTeamLimit(liveFinishedContestData.get(position).getMaxTeamLimit());
            contest.setConfirmedChallenge(liveFinishedContestData.get(position).getConfirmed_challenge());
            contest.setWd(liveFinishedContestData.get(position).getWd());
            contest.setUser_joined_count(liveFinishedContestData.get(position).getUser_joined_count());
            contest.setFlexibleLeague(liveFinishedContestData.get(position).getFlexibleLeague());
            contest.setFlexibleWinAmount(liveFinishedContestData.get(position).getFlexibleWinAmount().substring(1));
            contest.setFlexibleFirstRankPrize(liveFinishedContestData.get(position).getFlexibleFirstRankPrize().substring(1));
            listener.onContestClick(contest, true);
        });

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return liveFinishedContestData.size();
    }


    public void updateData(ArrayList<LiveFinishedContestData> list) {
        category = "";
        liveFinishedContestData = list;
        notifyDataSetChanged();
    }

}