
package com.rg.mchampgold.app.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.LiveMatchesResponse;
import com.rg.mchampgold.app.dataModel.MatchListItem;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.LiveMatchesTabFragmentBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

public class LiveMatchersTabFragment extends Fragment {
    @Inject
    OAuthRestService oAuthRestService;
    LiveMatchesTabFragmentBinding liveMatchersTabFragment;
    ArrayList<MatchListItem> matchListItems = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        liveMatchersTabFragment = DataBindingUtil.inflate(inflater, R.layout.live_matches_tab_fragment, container, false);
        return  liveMatchersTabFragment.getRoot();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LiveMatcheslist();
    }
    private void setupRecyclerView() {
        setupViewPager(liveMatchersTabFragment.viewPager);
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        for (int i=0; i<matchListItems.size(); i++){
            adapter.addFrag(new LiveMatchesFragment(matchListItems.get(i).getMatchkey(),i), matchListItems.get(i).getShort_name());
        }
        viewPager.setAdapter(adapter);
        if (matchListItems.size()<4){
            liveMatchersTabFragment.tabLayout.setTabMode(TabLayout.MODE_FIXED);
        }else {
            liveMatchersTabFragment.tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        liveMatchersTabFragment.tabLayout.setupWithViewPager(viewPager);
        liveMatchersTabFragment.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Fragment liveMatchesFragment=adapter.getItem(tab.getPosition());
                if (liveMatchesFragment instanceof LiveMatchesFragment){
                    ((LiveMatchesFragment) liveMatchesFragment).LiveMatcheslist();
                    ((LiveMatchesFragment) liveMatchesFragment).getData();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void LiveMatcheslist() {
        liveMatchersTabFragment.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<LiveMatchesResponse> orderIdResponse = oAuthRestService.liveMatches(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<LiveMatchesResponse>() {
            @Override
            public void success(Response<LiveMatchesResponse> response) {
                liveMatchersTabFragment.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    LiveMatchesResponse bannerListResponse = response.body();
                    if (bannerListResponse.getStatus() == 1 && bannerListResponse.getResult().size() > 0) {
                        matchListItems = bannerListResponse.getResult();
                        if (matchListItems.size()>0){
                            setupRecyclerView();
                            liveMatchersTabFragment.rlNoMatch.setVisibility(View.GONE);
                        }else {
                            liveMatchersTabFragment.rlNoMatch.setVisibility(View.VISIBLE);
                        }
                    }else {
                        liveMatchersTabFragment.rlNoMatch.setVisibility(View.VISIBLE);
                    }
                } else {
                    liveMatchersTabFragment.rlNoMatch.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                liveMatchersTabFragment.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }
}
