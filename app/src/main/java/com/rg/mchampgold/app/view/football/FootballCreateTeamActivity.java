package com.rg.mchampgold.app.view.football;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.dataModel.Limit;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.FootballSelectedPlayer;
import com.rg.mchampgold.app.view.activity.ChooseCandVCActivity;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.app.view.activity.PlayerInfoActivity;
import com.rg.mchampgold.app.view.adapter.SelectedUnSelectedPlayerAdapter;
import com.rg.mchampgold.app.view.fragment.CreateTeamPlayerFragment;
import com.rg.mchampgold.app.view.fragment.PlayingStatusSheetFragment;
import com.rg.mchampgold.app.viewModel.GetPlayerDataViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityCreateTeamBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FootballCreateTeamActivity extends AppCompatActivity{
    ActivityCreateTeamBinding mBinding;
    SelectedUnSelectedPlayerAdapter mSelectedUnSelectedPlayerAdapter;
    private GetPlayerDataViewModel createTeamViewModel;
    String matchKey;
    String teamName;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;


    ArrayList<Player> gkList = new ArrayList<>();
    ArrayList<Player> defList = new ArrayList<>();
    ArrayList<Player> midList = new ArrayList<>();
    ArrayList<Player> stList = new ArrayList<>();

    ArrayList<Player> allPlayerList = new ArrayList<>();


    private static int GK = 1;
    private static int DEF = 2;
    private static int MID = 3;
    private static int ST = 4;

/*    Football >>
    GK > 1
    DEF > 3-5
    MID > 3-5
    ST > 1-3
    Maximum > 11*/

    FootballSelectedPlayer selectedPlayer;


    boolean exeedCredit = false;

    public static Activity createTeamAc;
    ArrayList<Player> selectedList = new ArrayList<>();
    int teamId;
    Context context;
    boolean isFromEditOrClone;
    String headerText;
    boolean isShowTimer;


    int selectedType = GK;

    int fantasyType;
    int totalPlayerCount;
    int maxTeamPlayerCount;
    double totalCredit;
    Limit limit;
    String sport_key="FOOTBALL";
    int counterValue=0;

    public boolean isPointSorted = true,
            isCreditSorted = true,
            isPLayerSorted = true;
    public String playerStatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        createTeamViewModel = GetPlayerDataViewModel.create(FootballCreateTeamActivity.this);
        MyApplication.getAppComponent().inject(createTeamViewModel);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_team);
        context = FootballCreateTeamActivity.this;
        initialize();
        createTeamAc = this;
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(getString(R.string.create_team));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if(getIntent()!=null && getIntent().getExtras()!=null) {
            if(getIntent().getExtras().getBoolean("isFromEditOrClone")) {
                isFromEditOrClone = getIntent().getExtras().getBoolean("isFromEditOrClone");
                selectedList = (ArrayList<Player>) getIntent().getSerializableExtra("selectedList");
                teamId = getIntent().getExtras().getInt(Constants.KEY_TEAM_ID);
            }
            teamName = getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            matchKey=  getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName=  getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl=  getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl=  getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            fantasyType=  getIntent().getExtras().getInt(Constants.KEY_FANTASY_TYPE_STATUS);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT,"");
            sport_key = getIntent().getExtras().getString(Constants.SPORT_KEY,"FOOTBALL");
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER,false);
        }

        setTeamNames();

        String teams[] = teamVsName.split(" ");
        mBinding.tvTeam1.setText(teams[0]);
        mBinding.tvTeam2.setText(teams[2]);
        AppUtils.loadImageMatch(mBinding.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.ivTeam2, teamSecondUrl);
//        mBinding.matchHeaderInfo.tvTeamVs.setText(teamVsName);
//        mBinding.ivTeamFirst.setImageURI(teamFirstUrl);
//        mBinding.ivTeamSecond.setImageURI(teamSecondUrl);

        if(isShowTimer) {
            showTimer();
        }
        else {
            if(headerText.equalsIgnoreCase("Winner Declared")) {
//                mBinding.matchHeaderInfo.tvTimeTimer.setText("Winner Declared");
//                mBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#f70073"));
            }
            else if(headerText.equalsIgnoreCase("In Progress")) {
//                mBinding.matchHeaderInfo.tvTimeTimer.setText("In Progress");
//                mBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#16ae28"));
            }
        }

        mBinding.tvPlayerCountPick.setText("Pick 1 Goalkeeper");


        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FragmentManager fm = getSupportFragmentManager();
                switch (position) {
                    case 0:
                        selectedType = GK;
                        mBinding.tabLayout.getTabAt(0).setText(Constants.GK + (selectedPlayer.getWk_selected() == 0?"":"(" + selectedPlayer.getWk_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 1 Goalkeeper");
                        if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                            ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(gkList, GK);
                        break;
                    case 1:
                        selectedType = DEF;
                        mBinding.tabLayout.getTabAt(1).setText(Constants.DEF + (selectedPlayer.getBat_selected() == 0?"":"(" + selectedPlayer.getBat_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 2-6 Defender");
                        if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                            ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(defList, DEF);
                        break;
                    case 2:
                        selectedType = MID;
                        mBinding.tabLayout.getTabAt(2).setText(Constants.MID  + (selectedPlayer.getAr_selected() == 0?"":"(" + selectedPlayer.getAr_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 2-6 Midfielder");
                        if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                            ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(midList, MID);
                        break;
                    case 3:
                        selectedType = ST;
                        mBinding.tabLayout.getTabAt(3).setText(Constants.ST + (selectedPlayer.getBowl_selected() == 0?"":"(" + selectedPlayer.getBowl_selected() + ")"));
                        if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                            mBinding.tvPlayerCountPick.setText("Pick 1-3 Forward");
                        ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(stList, ST);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mSelectedUnSelectedPlayerAdapter = new SelectedUnSelectedPlayerAdapter(0,totalPlayerCount,getApplicationContext());
        createTeamData();
        mBinding.rvSelected.setAdapter(mSelectedUnSelectedPlayerAdapter);
        setupRecyclerView();




        mBinding.btnCreateTeam.setOnClickListener(v -> {

            if(selectedPlayer.getSelectedPlayer() == totalPlayerCount){
                ArrayList<Player> sellectedList = new ArrayList<>();

                for (Player player : gkList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }


                for (Player player : midList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }


                for (Player player : stList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }


                for (Player player : defList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }


                Intent intent = new Intent(FootballCreateTeamActivity.this, ChooseCandVCActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                intent.putExtra("playerList",sellectedList);
                intent.putExtra(Constants.KEY_TEAM_ID,teamId);
                intent.putExtra( Constants.KEY_STATUS_HEADER_TEXT,headerText);
                intent.putExtra( Constants.KEY_STATUS_IS_TIMER_HEADER,isShowTimer);
                intent.putExtra(Constants.SPORT_KEY,sport_key);
				intent.putExtra("localTeamCount", String.valueOf(selectedPlayer.getLocalTeamplayerCount()));
                intent.putExtra("visitorTeamCount", String.valueOf(selectedPlayer.getVisitorTeamPlayerCount()));

                if(isFromEditOrClone)
                    intent.putExtra("isFromEditOrClone",true);
                else
                    intent.putExtra("isFromEditOrClone",false);

                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);

                startActivityForResult(intent,101);

                //startActivity(intent);
            }
            else {
                showToast("Please select "+totalPlayerCount+" players");
            }

        });

        mBinding.ivTeamPreview.setOnClickListener(view -> {
            Intent intent = new Intent(FootballCreateTeamActivity.this, FootballTeamPreviewActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS,  teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_TEAM_NAME, teamName);

            ArrayList<Player> selectedWkList = new ArrayList<>();
            ArrayList<Player> selectedBatLiSt = new ArrayList<>();
            ArrayList<Player> selectedArList = new ArrayList<>();
            ArrayList<Player> selectedBowlList = new ArrayList<>();
            for (Player player : gkList) {
                if (player.isIsSelected())
                    selectedWkList.add(player);
            }

            for (Player player : defList) {
                if (player.isIsSelected())
                    selectedBatLiSt.add(player);
            }

            for (Player player : midList) {
                if (player.isIsSelected())
                    selectedArList.add(player);
            }

            for (Player player : stList) {
                if (player.isIsSelected())
                    selectedBowlList.add(player);
            }

            intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedWkList);
            intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedBatLiSt);
            intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedArList);
            intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedBowlList);

            startActivity(intent);
        });
        mBinding.ivInfoSelection.setOnClickListener(view -> {
            showSelectionPopupDialog();
        });

        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (mBinding.tabLayout.getSelectedTabPosition() == 0) {
                    mBinding.tvPlayerCountPick.setText("Pick 1 Goalkeeper");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 1) {
                    mBinding.tvPlayerCountPick.setText("Pick 2-6 Defender");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 2) {
                    mBinding.tvPlayerCountPick.setText("Pick 2-6 Midfielder");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 3) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-3 Forward");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mBinding.tvPoints.setOnClickListener(view -> {

            FragmentManager fm = getSupportFragmentManager();
            if (fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment) {
                FootballCreateTeamPlayerFragment footballCreateTeamPlayerFragment =
                        ((FootballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()));

                if (isPointSorted) {
                    sortWithPoints(false);
                    footballCreateTeamPlayerFragment.sortWithPoints(false);
                    isPointSorted = false;
                    mBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPointSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    sortWithPoints(true);
                    footballCreateTeamPlayerFragment.sortWithPoints(true);
                    isPointSorted = true;
                    mBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPointSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                isCreditSorted = true;
                isPLayerSorted = true;
            }
        });

        mBinding.tvCredits.setOnClickListener(view -> {

            FragmentManager fm = getSupportFragmentManager();

            if (fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment) {

                FootballCreateTeamPlayerFragment footballCreateTeamPlayerFragment =
                        ((FootballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()));

                if (isCreditSorted) {
                    sortWithCredit(false);
                    footballCreateTeamPlayerFragment.sortWithCredit(false);
                    isCreditSorted = false;
                    mBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivCreditSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    isCreditSorted = true;
                    sortWithCredit(true);
                    footballCreateTeamPlayerFragment.sortWithCredit(true);
                    mBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivCreditSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                isPointSorted = true;
                isPLayerSorted = true;
            }
        });


        mBinding.llPlayer.setOnClickListener(view -> {

            FragmentManager fm = getSupportFragmentManager();

            if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment) {

                FootballCreateTeamPlayerFragment footballCreateTeamPlayerFragment =
                        ((FootballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()));
                if (isPLayerSorted) {
                    sortByPlayer(false);
                    footballCreateTeamPlayerFragment.sortByPlayer(false);
                    isPLayerSorted = false;
                    mBinding.ivPlayerSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPlayerSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    isPLayerSorted = true;
                    sortByPlayer(true);
                    footballCreateTeamPlayerFragment.sortByPlayer(true);
                    mBinding.ivPlayerSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPlayerSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                isPointSorted = true;
                isCreditSorted = true;
            }
        });

        mBinding.llPlayingStatus.setOnClickListener(view -> {
            showPlayingStatusDialog();
        });

    }

    private void sortWithPoints(boolean flag) {
        if (flag) {
            Collections.sort(gkList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(defList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(midList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(stList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
        } else {
            Collections.sort(gkList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(defList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(midList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(stList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
        }
    }


    private void sortWithCredit(boolean flag) {
        if (flag) {
            Collections.sort(gkList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(defList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(midList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(stList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
        } else {
            Collections.sort(gkList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(defList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(midList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(stList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
        }
    }

    private void sortByPlayer(boolean flag) {
        if (flag) {
            Collections.sort(gkList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(defList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(midList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(stList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
        } else {
            Collections.sort(gkList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(defList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(midList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(stList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
        }
    }

    private void showPlayingStatusDialog() {
        PlayingStatusSheetFragment playingStatusSheetFragment = new PlayingStatusSheetFragment(context);
        playingStatusSheetFragment.show(getSupportFragmentManager(), playingStatusSheetFragment.getTag());
    }

    public void changePlayerStatus(String playNotPlayText) {
        playerStatus = playNotPlayText;
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()) instanceof FootballCreateTeamPlayerFragment)
            ((FootballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition())).changePlayingStatus();
    }

    private void showSelectionPopupDialog() {
        Dialog dialog = new Dialog(FootballCreateTeamActivity.this);
        dialog.setContentView(R.layout.football_selection_rule_popup);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.findViewById(R.id.tv_got_it).setOnClickListener(view1 -> dialog.dismiss());
        dialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_team,menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem clearTeam = menu.findItem(R.id.clear_team);
        clearTeam.setVisible(true);
        LinearLayout rootView = (LinearLayout) clearTeam.getActionView();
        rootView.setOnClickListener(view -> {
            if (selectedPlayer.getSelectedPlayer() > 0) {

                Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.layout_clear_popup);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                final TextView tvCancelTeam = dialog.findViewById(R.id.tv_cancel_team);
                final TextView tvClearTeam = dialog.findViewById(R.id.tv_clear_team);

                tvCancelTeam.setOnClickListener(view1 -> dialog.dismiss());

                tvClearTeam.setOnClickListener(view12 -> {

                    createTeamData();

                    for (int i = 0; i < allPlayerList.size(); i++) {
                        allPlayerList.get(i).setSelected(false);
                    }

                    mBinding.tvLocalTeam.setText("0");
                    mBinding.tvVisitorTeam.setText("0");
                    setupViewPager(mBinding.viewPager);
                    mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                    mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                    mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                    isCreditSorted = true;
                    isPLayerSorted = true;
                    isPointSorted = true;
                    dialog.dismiss();
                });

                dialog.show();
            } else {
                AppUtils.showErrorr(this, "No player selected to clear.");
            }

        });


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();/*   case R.id.navigation_notification:
                openNotificationActivity();
                return true;*//*case R.id.navigation_wallet:
                openWalletActivity();
                return true;
*/
        if (itemId == R.id.how_to_play) {
            AppUtils.openWebViewActivity(getString(R.string.how_to_play), MyApplication.how_to_play_url);
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openNotificationActivity() {
        startActivity(new Intent(FootballCreateTeamActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(FootballCreateTeamActivity.this, MyWalletActivity.class));

    }

    private void setupRecyclerView() {
        getData();
    }


    private void getData() {
        MyTeamRequest request = new MyTeamRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sport_key);
        createTeamViewModel.loadPlayerListRequest(request);
        createTeamViewModel.getPlayerList().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext,arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if(arrayListResource.getData().getStatus()==1 && arrayListResource.getData().getResult().size()>0) {
                        allPlayerList = arrayListResource.getData().getResult();
                        limit = arrayListResource.getData().getLimit();

                        totalCredit = limit.getTotalCredits();
                        totalPlayerCount = limit.getTotalPlayers();
                        mBinding.totalPlayers.setText("/"+totalPlayerCount);
                        maxTeamPlayerCount = limit.getTeamMaxPlayer();
                        mBinding.tvMsgError.setText("Max "+maxTeamPlayerCount+" Players from a team");
                        setData();

                        for (Player player : allPlayerList) {
                            if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_GK))
                                gkList.add(player);
                            else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_MID))
                                midList.add(player);
                            else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_DEF))
                                defList.add(player);
                            else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ST))
                                stList.add(player);
                        }

                        if(selectedList.size()>0) {
                            for (int i = 0; i < allPlayerList.size(); i++) {
                                for (Player player : selectedList) {
                                    if (player.getId()==allPlayerList.get(i).getId()) {
                                        allPlayerList.get(i).setSelected(true);
                                        if(player.getCaptain() ==1)
                                            allPlayerList.get(i).setCaptain(true);
                                        if(player.getVicecaptain() ==1)
                                            allPlayerList.get(i).setVcCaptain(true);
                                    }
                                }
                            }
                            setSelectedCountForEditOrClone();
                        }

                        setupViewPager(mBinding.viewPager);
                        sortWithCredit(false);
                        isCreditSorted = false;
                        isPointSorted = true;
                        isPLayerSorted = true;
                        mBinding.viewPager.setOffscreenPageLimit(4);

//
//                        if (!MyApplication.tinyDB.getBoolean(Constants.SKIP_CREATETEAM_INSTRUCTION, false)) {
//
//                            callIntroductionScreen(
//                                    R.id.tabLayout,
//                                    "Player Category",
//                                    "Select a balanced team to help you win",
//                                    ShowcaseView.BELOW_SHOWCASE
//                            );
//                            MyApplication.tinyDB.putBoolean(Constants.SKIP_CREATETEAM_INSTRUCTION, true);
//                        }


                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }

    private void setSelectedCountForEditOrClone() {
        int countWK = 0;
        int countBAT = 0;
        int countBALL = 0;
        int countALL = 0;
        int totalCount =0;
        int team1Count = 0;
        int team2Count = 0;
        double usedCredit =0;

        for (Player player : allPlayerList) {
            if (player.isIsSelected()) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_GK)) {
                    countWK++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_DEF)) {
                    countBAT++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_MID)) {
                    countALL++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_ST)) {
                    countBALL++;
                }

                if(player.getTeam().equalsIgnoreCase("team1")) {
                    team1Count++;
                }

                if(player.getTeam().equalsIgnoreCase("team2")) {
                    team2Count++;
                }

                totalCount++;
                usedCredit += player.getCredit();
            }
        }


        selectedPlayer.setWk_selected(countWK);
        selectedPlayer.setBat_selected(countBAT);
        selectedPlayer.setAr_selected(countALL);
        selectedPlayer.setBowl_selected(countBALL);
        selectedPlayer.setSelectedPlayer(totalPlayerCount);
        selectedPlayer.setLocalTeamplayerCount(team1Count);
        selectedPlayer.setVisitorTeamPlayerCount(team2Count);
        selectedPlayer.setTotal_credit(usedCredit);

        updateTeamData(
                0,
                selectedPlayer.getWk_selected(),
                selectedPlayer.getBat_selected(),
                selectedPlayer.getAr_selected(),
                selectedPlayer.getBowl_selected(),
                selectedPlayer.getSelectedPlayer(),
                selectedPlayer.getLocalTeamplayerCount(),
                selectedPlayer.getVisitorTeamPlayerCount(),
                selectedPlayer.getTotal_credit());
    }

    public void setTeamNames() {
        if(teamVsName!=null) {
            String teams[] = teamVsName.split("Vs");
            mBinding.tvTeamNameFirst.setText(teams[0]);
            mBinding.tvTeamNameSecond.setText(teams[1]);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        FootballCreateTeamActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("This team will not be saved!");
        builder.setMessage("Are you sure you want to go back?").setPositiveButton("CONTINUE", dialogClickListener)
                .setNegativeButton("CANCEL", dialogClickListener).show();

    }


    private void showTimer()
    {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                   /* if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d " + twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    } else {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    }*/
                    mBinding.tvTimeTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");

                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.tvTimeTimer.setText("00h 00m 00s");
                }
            };
            countDownTimer.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void openPlayerInfoActivity(String playerId, String playerName, String team, String image, boolean is_added, int pos, int type) {
        Intent intent = new Intent(this, PlayerInfoActivity.class);
        intent.putExtra("matchKey", matchKey);
        intent.putExtra("playerId", playerId);
        intent.putExtra("playerName", playerName);
        intent.putExtra("team", team);
        intent.putExtra("image", image);
        intent.putExtra("flag", "0");
        intent.putExtra("is_added", is_added);
        intent.putExtra("pos", pos);
        intent.putExtra("type", type);
        intent.putExtra(Constants.SPORT_KEY, sport_key);
        startActivity(intent);
    }




    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment) {
            mFragmentList.add(fragment);
            //mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return Constants.GK+" " + (selectedPlayer.getWk_selected() == 0?"":"(" + selectedPlayer.getWk_selected() + ")");
                case 1:
                    return Constants.DEF+" " + (selectedPlayer.getBat_selected() == 0?"":"(" + selectedPlayer.getBat_selected() + ")");
                case 2:
                    return Constants.MID+" " + (selectedPlayer.getAr_selected() == 0?"":"(" + selectedPlayer.getAr_selected() + ")");
                case 3:
                    return Constants.ST+" " + (selectedPlayer.getBowl_selected() == 0?"":"(" + selectedPlayer.getBowl_selected() + ")");
            }
            return "";
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new FootballCreateTeamPlayerFragment(allPlayerList,gkList, GK));
        adapter.addFrag(new FootballCreateTeamPlayerFragment(allPlayerList,defList, DEF));
        adapter.addFrag(new FootballCreateTeamPlayerFragment(allPlayerList,midList, MID));
        adapter.addFrag(new FootballCreateTeamPlayerFragment(allPlayerList,stList, ST));
        viewPager.setAdapter(adapter);
        mBinding.tabLayout.setupWithViewPager(viewPager);

    }


    public void onPlayerClick(boolean isSelect, int position, int type) {

        if (type == GK) {
            double player_credit = 0.0;

            if (isSelect) {
                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount) {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players.");
                    return;
                }
                if(selectedPlayer.getWk_selected()>=1) {
                    showTeamValidation("You can select only 1 Goal-Keeper.");
                    return;
                }

                if (gkList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }

                if (selectedPlayer.getWk_selected() < selectedPlayer.getWk_max_count()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getWk_selected() < selectedPlayer.getWk_min_count() || selectedPlayer.getExtra_player() > 0) {
                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getWk_selected() >= selectedPlayer.getWk_min_count()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = gkList.get(position).getCredit();



                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (gkList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            gkList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected()+1,
                                    selectedPlayer.getBat_selected(),
                                    selectedPlayer.getAr_selected(),
                                    selectedPlayer.getBowl_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );

                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getWk_selected() > 0) {
                    //showTeamValidation("Pick 1 Goal-Keeper");
                    player_credit = gkList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;

                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getWk_selected() > selectedPlayer.getWk_min_count()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (gkList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    gkList.get(position).setSelected(isSelect);

                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected()-1,
                            selectedPlayer.getBat_selected(),
                            selectedPlayer.getAr_selected(),
                            selectedPlayer.getBowl_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

        else if (type == MID) {
            double player_credit = 0.0;
            if (isSelect) {
                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount) {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players.");
                    return;
                }

                if(selectedPlayer.getAr_selected()>=6) {
                    showTeamValidation("You can select only 5 Midfielder");
                    return;
                }

                if (midList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        //mBinding.tvPlayerCountPick.setText("You can select only 7 from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        //mBinding.tvPlayerCountPick.setText("You can select only 7 from each team.");
                        return;
                    }
                }



                if (selectedPlayer.getAr_selected() < selectedPlayer.getAr_maxcount()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getAr_selected() < selectedPlayer.getAr_mincount() || selectedPlayer.getExtra_player() > 0) {

                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getAr_selected() >= selectedPlayer.getAr_mincount()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = midList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                //mBinding.tvPlayerCountPick.setText("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (midList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            midList.get(position).setSelected(isSelect);
                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected(),
                                    selectedPlayer.getBat_selected(),
                                    selectedPlayer.getAr_selected()+1,
                                    selectedPlayer.getBowl_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getAr_selected() > 0) {
                    //  showTeamValidation("Pick 3-5 Midfielder");
                    player_credit = midList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;



                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getAr_selected() > selectedPlayer.getAr_mincount()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (midList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    midList.get(position).setSelected(isSelect);

                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected(),
                            selectedPlayer.getBat_selected(),
                            selectedPlayer.getAr_selected()-1,
                            selectedPlayer.getBowl_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

        else if (type == DEF) {
            double player_credit = 0.0;


            if (isSelect) {

                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount) {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players");
                    return;
                }

                if(selectedPlayer.getBat_selected()>=6) {
                    showTeamValidation("You can select only 5 Defender");
                    return;
                }

                if (defList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }


                if (selectedPlayer.getBat_selected() < selectedPlayer.getBat_maxcount()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getBat_selected() < selectedPlayer.getBat_mincount() || selectedPlayer.getExtra_player() > 0) {

                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getBat_selected() >= selectedPlayer.getBat_mincount()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = defList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (defList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() +1;

                            defList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected(),
                                    selectedPlayer.getBat_selected()+1,
                                    selectedPlayer.getAr_selected(),
                                    selectedPlayer.getBowl_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getBat_selected() > 0) {
                    //  showTeamValidation("Pick 3-5 Defender");
                    player_credit = defList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;

                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getBat_selected() > selectedPlayer.getBat_mincount()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (defList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() -1;

                    defList.get(position).setSelected(isSelect);
                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected(),
                            selectedPlayer.getBat_selected()-1,
                            selectedPlayer.getAr_selected(),
                            selectedPlayer.getBowl_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

        else if (type == ST) {
            double player_credit = 0.0;

            if (isSelect) {

                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount)
                {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players.");
                    return;
                }

                if(selectedPlayer.getBowl_selected()>=3) {
                    showTeamValidation("You can select only 3 Forward.");
                    return;
                }

                if (stList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }



                if (selectedPlayer.getBowl_selected() < selectedPlayer.getBowl_maxcount()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getBowl_selected() < selectedPlayer.getBowl_mincount() || selectedPlayer.getExtra_player() > 0) {

                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getBowl_selected() >= selectedPlayer.getBowl_mincount()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = stList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                //mBinding.tvPlayerCountPick.setText("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (stList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() +1;

                            stList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected(),
                                    selectedPlayer.getBat_selected(),
                                    selectedPlayer.getAr_selected(),
                                    selectedPlayer.getBowl_selected()+1,
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {

                if (selectedPlayer.getBowl_selected() > 0) {
                    //  showTeamValidation("Pick 1-3 Forward");
                    player_credit = stList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;


                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getBowl_selected() > selectedPlayer.getBowl_mincount()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (stList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    stList.get(position).setSelected(isSelect);
                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected(),
                            selectedPlayer.getBat_selected(),
                            selectedPlayer.getAr_selected(),
                            selectedPlayer.getBowl_selected()-1,
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }
    }

    public void createTeamData() {

        //gkList, GK,WK
        //   midList, DEF,BAT
        //stList, MID,AR
        //defList, ST,BOwl

        selectedPlayer = new FootballSelectedPlayer();
        selectedPlayer.setExtra_player(5);
        selectedPlayer.setWk_min_count(1);
        selectedPlayer.setWk_max_count(1);
        selectedPlayer.setWk_selected(0);

        selectedPlayer.setBat_mincount(2);
        selectedPlayer.setBat_maxcount(6);
        selectedPlayer.setBat_selected(0);

        selectedPlayer.setBowl_mincount(1);
        selectedPlayer.setBowl_maxcount(3);
        selectedPlayer.setBowl_selected(0);

        selectedPlayer.setAr_mincount(2);
        selectedPlayer.setAr_maxcount(6);
        selectedPlayer.setAr_selected(0);

        selectedPlayer.setSelectedPlayer(0);

        selectedPlayer.setLocalTeamplayerCount(0);
        selectedPlayer.setVisitorTeamPlayerCount(0);
        selectedPlayer.setTotal_credit(0.0);
        updateUi();
    }

    public void updateTeamData(
            int extra_player,
            int wk_selected,
            int bat_selected,
            int ar_selected,
            int bowl_selected,
            int selectPlayer,
            int localTeamplayerCount,
            int visitorTeamPlayerCount,
            double total_credit)
    {
        exeedCredit = false;
        selectedPlayer.setExtra_player(extra_player);
        selectedPlayer.setWk_selected(wk_selected);
        selectedPlayer.setBat_selected(bat_selected);
        selectedPlayer.setAr_selected(ar_selected);
        selectedPlayer.setBowl_selected(bowl_selected);
        selectedPlayer.setSelectedPlayer(selectPlayer);
        selectedPlayer.setLocalTeamplayerCount(localTeamplayerCount);
        selectedPlayer.setVisitorTeamPlayerCount(visitorTeamPlayerCount);
        selectedPlayer.setTotal_credit(total_credit);
        mBinding.tvLocalTeam.setText("("+localTeamplayerCount+")");
        mBinding.tvVisitorTeam.setText("("+visitorTeamPlayerCount+")");
        updateUi();
    }

    private void updateUi() {
        mBinding.tvSelectedPlayer.setText(String.valueOf(selectedPlayer.getSelectedPlayer()));


        if (selectedPlayer.getLocalTeamplayerCount()+selectedPlayer.getVisitorTeamPlayerCount() == 11)
        {
            mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_dark_blue);
        }
        else
        {
            mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_grey);
        }

      /*  if (isFromEditOrClone) {
            mBinding.tvUsedCredit.setText(selectedPlayer.getTotal_credit()+"");
        } else {
            mBinding.tvUsedCredit.setText(selectedPlayer.getTotal_credit()+"");
        }*/

        if (selectedPlayer.getTotal_credit() < 0)
            selectedPlayer.setTotal_credit(0);

        String creditLeft = String.valueOf(totalCredit-selectedPlayer.getTotal_credit());

        mBinding.tvUsedCredit.setText(creditLeft);

        mBinding.tvTeamCountFirst.setText("("+selectedPlayer.getLocalTeamplayerCount()+")");
        mBinding.tvTeamCountSecond.setText("("+selectedPlayer.getVisitorTeamPlayerCount()+")");

        mSelectedUnSelectedPlayerAdapter.update(selectedPlayer.getSelectedPlayer());
        if (mBinding.tabLayout.getTabCount() > 0) {
            // Log.e("childCount",mBinding.tabLayout.getChildCount()+"");
            // Log.e("tabCount",mBinding.tabLayout.getTabCount()+"");
//            callFragmentRefresh();
            if (MyApplication.footballPlayerItemAdapter!=null) {
                if (mBinding.tabLayout.getSelectedTabPosition() == 0) {
                    mBinding.tabLayout.getTabAt(0).setText("GK " + (selectedPlayer.getWk_selected() == 0 ? "" : "(" + selectedPlayer.getWk_selected() + ")"));
                    MyApplication.footballPlayerItemAdapter.updateData(gkList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 1) {
                    mBinding.tabLayout.getTabAt(1).setText("DEF " + (selectedPlayer.getBat_selected() == 0 ? "" : "(" + selectedPlayer.getBat_selected() + ")"));
                    MyApplication.footballPlayerItemAdapter.updateData(defList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 2) {
                    mBinding.tabLayout.getTabAt(2).setText("MID " + (selectedPlayer.getAr_selected() == 0 ? "" : "(" + selectedPlayer.getAr_selected() + ")"));
                    MyApplication.footballPlayerItemAdapter.updateData(midList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 3) {
                    mBinding.tabLayout.getTabAt(3).setText("ST " + (selectedPlayer.getBowl_selected() == 0 ? "" : "(" + selectedPlayer.getBowl_selected() + ")"));
                    MyApplication.footballPlayerItemAdapter.updateData(stList, selectedType);
                }
            }
        }
    }

    public void callFragmentRefresh() {
        if(mBinding.viewPager.getAdapter()!=null)
            mBinding.viewPager.getAdapter().notifyDataSetChanged();

        FragmentManager fm = getSupportFragmentManager();
        switch (selectedType) {
            case 1:
                mBinding.tabLayout.getTabAt(0).setText(Constants.GK+" " + (selectedPlayer.getWk_selected() == 0?"":"(" + selectedPlayer.getWk_selected() + ")"));
                if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                    ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(gkList,selectedType);
                break;
            case 2:
                mBinding.tabLayout.getTabAt(1).setText(Constants.DEF+" "+ (selectedPlayer.getBat_selected() == 0?"":"(" + selectedPlayer.getBat_selected() + ")"));
                if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                    ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(defList,selectedType);
                break;
            case 3:
                mBinding.tabLayout.getTabAt(2).setText(Constants.MID+" " + (selectedPlayer.getAr_selected() == 0?"":"(" + selectedPlayer.getAr_selected() + ")"));
                if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                    ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(midList,selectedType);
                break;
            case 4:
                mBinding.tabLayout.getTabAt(3).setText(Constants.ST+" " + (selectedPlayer.getBowl_selected() == 0?"":"(" + selectedPlayer.getBowl_selected() + ")"));
                if(fm.getFragments().get(0) instanceof FootballCreateTeamPlayerFragment)
                    ((FootballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(stList,selectedType);
                break;
        }

    }

    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public void minimumPlayerWarning() {
        //gkList, GK,WK
        //   midList, DEF,BAT
        //stList, MID,AR
        //defList, ST,BOwl
        if(selectedPlayer.getBowl_selected()<1) {
            showTeamValidation("You must select at least 1 State-Forward.");
        }
        else if(selectedPlayer.getBat_selected()<2) {
            showTeamValidation("You must select at least 2 Defender.");
        }
        else  if(selectedPlayer.getAr_selected()<2) {
            showTeamValidation("You must select at least 2 Midfielder");
        }
        else  if(selectedPlayer.getWk_selected()<1) {
            showTeamValidation("You must select at least 1 Goal-Keeper.");
        }
    }


    public void showTeamValidation(String mesg) {

        AppUtils.showErrorr(this, mesg);

      /*  final Flashbar flashbar = new Flashbar.Builder(this)
                .gravity(Flashbar.Gravity.TOP)
                .message(mesg)
                .backgroundDrawable(R.drawable.bg_gradient_create_team_warning)
                .showIcon()
                .icon(R.drawable.close)
                .iconAnimation(FlashAnim.with(this)
                        .animateIcon()
                        .pulse()
                        .alpha()
                        .duration(1000)
                        .accelerate())
                .build();

        flashbar.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flashbar.dismiss();
            }
        },2000);
    }

       */

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 101) {
            if(resultCode == Activity.RESULT_OK){
                Intent returnIntent = new Intent();
                returnIntent.putExtra("isTeamCreated",data.getBooleanExtra("isTeamCreated",false));
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }

        }
    }

    private void setData() {
//        mBinding.tvTotalCredit.setText(" /"+ String.valueOf(totalCredit));
//        mBinding.tvTotalPlayer.setText(" /"+ String.valueOf(totalPlayerCount));
        //   mBinding.tvMaxPlayerWarning.setText("Max "+maxTeamPlayerCount+" Players from a one team");
        mSelectedUnSelectedPlayerAdapter.updateTotalPlayerCount(totalPlayerCount);

    }


//    void callIntroductionScreen(int target, String title, String description, int abovE_SHOWCASE) {
//
//
//        ShowcaseView showcaseView = new ShowcaseView.Builder(this).withNewStyleShowcase()
//                .setTarget(new ViewTarget(target, this))
//                .setContentTitle(title)
//                .setContentText(description)
//                .setStyle(counterValue == 0 ? R.style.CustomShowcaseTheme : R.style.CustomShowcaseTheme)
//                .hideOnTouchOutside().setShowcaseEventListener(this)
//                .build();
//
//        showcaseView.forceTextPosition(abovE_SHOWCASE);
//        counterValue = counterValue + 1;
//
//        new Handler().postDelayed(() -> showcaseView.hideButton(), 2500);
//
//    }
//
//
//    @Override
//    public void onShowcaseViewHide(ShowcaseView showcaseView) {
//
//        switch (counterValue) {
//            case 1: {
//                callIntroductionScreen(
//                        R.id.ll_credit,
//                        "Credit Counter",
//                        "Use 100 credits to pick your players", ShowcaseView.BELOW_SHOWCASE
//                );
//                break;
//            }
//            case 2: {
//                callIntroductionScreen(
//                        R.id.ll_players,
//                        "Player Counter",
//                        "Pick 11 players to create your team"
//                        ,
//                        ShowcaseView.BELOW_SHOWCASE
//                );
//                break;
//            }
//
//        }
//
//    }
//
//    @Override
//    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
//
//    }
//
//    @Override
//    public void onShowcaseViewShow(ShowcaseView showcaseView) {
//
//    }
//
//    @Override
//    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {
//
//    }
}
