package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class MyTeamResponse{

	@SerializedName("result")
	private TeamITem teamITem;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;


	public TeamITem getTeamITem() {
		return teamITem == null?new TeamITem():teamITem;
	}

	public void setTeamITem(TeamITem teamITem) {
		this.teamITem = teamITem;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MyTeamResponse{" + 
			",message = '" + message + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}
}