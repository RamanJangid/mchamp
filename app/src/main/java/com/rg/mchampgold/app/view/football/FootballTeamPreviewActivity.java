package com.rg.mchampgold.app.view.football;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityFootballTeamPreviewBinding;

import java.util.ArrayList;


public class FootballTeamPreviewActivity extends AppCompatActivity {

    ActivityFootballTeamPreviewBinding activityTeamPreviewBinding;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    String teamName = "";
    String headerText;
    String sportKey;
    boolean isShowTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(FootballTeamPreviewActivity.this);
        activityTeamPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_football_team_preview);
        initialize();

        LinearLayoutManager horizontalLayoutManagaerr = new LinearLayoutManager(FootballTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.wickRecyclerView.setLayoutManager(horizontalLayoutManagaerr);

        LinearLayoutManager horizontalLayoutManagaer =
                new LinearLayoutManager(FootballTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.bolRecyclerView.setLayoutManager(horizontalLayoutManagaer);

        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(FootballTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.allRecyclerView.setLayoutManager(horizontalLayoutManagaer1);

        LinearLayoutManager horizontalLayoutManagaer2 = new LinearLayoutManager(FootballTeamPreviewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPreviewBinding.batRecyclerView.setLayoutManager(horizontalLayoutManagaer2);
        if (MyApplication.fromMyTeams) {
            MyApplication.fromMyTeams = false;
//            activityTeamPreviewBinding.icClose.setVisibility(View.GONE);
            activityTeamPreviewBinding.edtText.setVisibility(View.VISIBLE);
        } else {
//            activityTeamPreviewBinding.icClose.setVisibility(View.VISIBLE);
            activityTeamPreviewBinding.edtText.setVisibility(View.GONE);
        }

        activityTeamPreviewBinding.edtText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editOrClone();
            }
        });
    }

    public void editOrClone() {
        Intent intent = new Intent(FootballTeamPreviewActivity.this, FootballCreateTeamActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_TEAM_ID, MyApplication.teamId);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY, sportKey);
        intent.putExtra("isFromEditOrClone", true);
        intent.putExtra("selectedList", MyApplication.teamList);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;
        } else if (itemId == R.id.navigation_wallet) {
            openWalletActivity();
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void initialize() {
       /* setSupportActionBar(activityTeamPreviewBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.team_preview));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
*/

        ArrayList<Player> listBat = new ArrayList<>();
        ArrayList<Player> listBowl = new ArrayList<>();
        ArrayList<Player> listAr = new ArrayList<>();
        ArrayList<Player> listWK = new ArrayList<>();

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            teamName = getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT, "");
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER, false);
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
            listWK = (ArrayList<Player>) getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_WK);
            listBowl = (ArrayList<Player>) getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_BOWL);
            listBat = (ArrayList<Player>) getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_BAT);
            listAr = (ArrayList<Player>) getIntent().getExtras().getSerializable(Constants.KEY_TEAM_LIST_AR);

        }
      /*  activityTeamPreviewBinding.matchHeaderInfo.tvTeamVs.setText(teamVsName);
        activityTeamPreviewBinding.matchHeaderInfo.ivTeamFirst.setImageURI(teamFirstUrl);
        activityTeamPreviewBinding.matchHeaderInfo.ivTeamSecond.setImageURI(teamSecondUrl);

        if(isShowTimer) {
            showTimer();
        }
        else {
            if(headerText.equalsIgnoreCase("Winner Declared")) {
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText("Winner Declared");
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#f70073"));
            }
            else if(headerText.equalsIgnoreCase("In Progress")) {
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText("In Progress");
                activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#16ae28"));
            }
        }*/
        if (teamName != null)
            activityTeamPreviewBinding.teamName.setText(teamName);

        activityTeamPreviewBinding.wickRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.wickRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false, listWK,matchKey));

        activityTeamPreviewBinding.batRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.batRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false, listBat,matchKey));

        activityTeamPreviewBinding.bolRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.bolRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false, listBowl,matchKey));

        activityTeamPreviewBinding.allRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//        activityTeamPreviewBinding.allRecyclerView.setAdapter(new PreviewPlayerItemAdapter(false, listAr,matchKey));

        activityTeamPreviewBinding.icClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


    private void openNotificationActivity() {
        startActivity(new Intent(FootballTeamPreviewActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(FootballTeamPreviewActivity.this, MyWalletActivity.class));

    }


   /* private void showTimer()
    {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000)) % 24;
                    if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                        activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d " + twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    } else {
                        activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText(twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    }

                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    activityTeamPreviewBinding.matchHeaderInfo.tvTimeTimer.setText("00h 00m 00s");
                }
            };
            countDownTimer.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
