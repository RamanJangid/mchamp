package com.rg.mchampgold.app.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.rg.mchampgold.common.utils.Constants;

public class IncomingSmsReceiver extends BroadcastReceiver {

    IncomingSMSListener listener = null;

    public IncomingSmsReceiver(IncomingSMSListener listener) {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;

                    String message = currentMessage.getDisplayMessageBody();

                 //   Log.e("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                    if (message.toLowerCase().contains("<#>Dear "+ Constants.APP_NAME +" user Your Verification code")) {
                        String otp = message.substring(message.indexOf("code is: "), message.indexOf("Thank"));
                        listener.onOTPReceived(otp);
                    }

                } // end for loop
            } // bundle is null


        } catch (Exception e) {
         //   Log.e("SmsReceiver", "Exception smsReceiver" + e);
            listener.onOTPTimeOut();

        }
    }
}
