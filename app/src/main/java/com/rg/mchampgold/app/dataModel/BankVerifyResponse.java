package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class BankVerifyResponse{

	@SerializedName("result")
	private BankVerifyResponseItem result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setResult(BankVerifyResponseItem result){
		this.result = result;
	}

	public BankVerifyResponseItem getResult(){
		return result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"BankVerifyResponse{" + 
			"result = '" + result + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}