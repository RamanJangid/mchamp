package com.rg.mchampgold.app.view.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.JoinedContestTeam;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.UpComingContestDetailActivity;
import com.rg.mchampgold.app.view.fragment.LeaderBoardFragment;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.RecyclerItemJoinedContestTeamBinding;

import java.util.ArrayList;
import java.util.List;

public class ContestJoinTeamItemAdapter extends RecyclerView.Adapter<ContestJoinTeamItemAdapter.ViewHolder> {

    private List<JoinedContestTeam> moreInfoDataList;
    Context context;
    boolean isContestDetail;
    String sportKey;
    LeaderBoardFragment leaderBoardFragment;
    private boolean isPopUpVisible = false;
    private boolean isFromLive = false;


    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemJoinedContestTeamBinding binding;

        ViewHolder(RecyclerItemJoinedContestTeamBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public ContestJoinTeamItemAdapter(boolean isFromLive,boolean isContestDetail ,Context context,ArrayList<JoinedContestTeam> moreInfoDataList,String sportKey,LeaderBoardFragment leaderBoardFragment) {
        this.moreInfoDataList = moreInfoDataList;
        this.context = context;
        this.isContestDetail =isContestDetail;
        this.sportKey =sportKey;
        this.isFromLive =isFromLive;
        this.leaderBoardFragment =leaderBoardFragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemJoinedContestTeamBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_joined_contest_team,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setContestData(moreInfoDataList.get(position));

        /*if (leaderBoardFragment.isCompareTeamActive) {
            if (MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID).equalsIgnoreCase(String.valueOf(moreInfoDataList.get(position).getUserid()))){
                leaderBoardFragment.team1Id= String.valueOf(moreInfoDataList.get(position).getTeamid());
                holder.itemView.setAlpha(0.5f);
                holder.itemView.setEnabled(false);
            }
        }else{
            holder.itemView.setAlpha(1.0f);
            holder.itemView.setEnabled(true);
        }*/

        if (isContestDetail) {
            holder.binding.tvRank.setVisibility(View.GONE);
            holder.binding.tvPoints.setVisibility(View.GONE);
            holder.binding.tvAmount.setVisibility(View.GONE);
            if (moreInfoDataList.get(position).isIsjoined()) {
                holder.binding.ivTeamSwitch.setVisibility(View.VISIBLE);
            } else {
                holder.binding.ivTeamSwitch.setVisibility(View.GONE);
            }
        } else {
            holder.binding.tvRank.setVisibility(View.VISIBLE);
            holder.binding.tvPoints.setVisibility(View.VISIBLE);
            holder.binding.tvAmount.setVisibility(View.VISIBLE);
            holder.binding.ivTeamSwitch.setVisibility(View.GONE);
        }

        AppUtils.loadImageleaderboard(holder.binding.ivTeamImage, moreInfoDataList.get(position).getUser_image());

        holder.binding.ivTeamSwitch.setOnClickListener(view -> {
            if (context instanceof UpComingContestDetailActivity) {
                ((UpComingContestDetailActivity) context).switchTeam(moreInfoDataList.get(position).getJoinId()+"",
                        moreInfoDataList.get(position).getTeamid());
            }
        });

        holder.itemView.setOnClickListener(view -> {

            if (isContestDetail) {
                if (moreInfoDataList.get(position).isCurrentTeam()) {
                    if (context instanceof UpComingContestDetailActivity)
                        ((UpComingContestDetailActivity) context).openPlayerPointActivityForLeatherBoard(isFromLive,
                                moreInfoDataList.get(position).getTeamid(),
                                moreInfoDataList.get(position).getChallengeId(),
                                moreInfoDataList.get(position).getTeamname(),
                                moreInfoDataList.get(position).getPoints(),
                                sportKey);
                } else {
                    Toast.makeText(context, "Sorry!! Team preview is available after match started.", Toast.LENGTH_SHORT).show();
                }
            } else {
                if (context instanceof UpComingContestDetailActivity)
                    ((UpComingContestDetailActivity) context).openPlayerPointActivityForLeatherBoard(isFromLive,
                            moreInfoDataList.get(position).getTeamid(),
                            moreInfoDataList.get(position).getChallengeId(),
                            moreInfoDataList.get(position).getTeamname(),
                            moreInfoDataList.get(position).getPoints(),
                            sportKey);
            }

            /*if (leaderBoardFragment.isCompareTeamActive) {
                leaderBoardFragment.team2Id= String.valueOf(moreInfoDataList.get(position).getTeamid());
                leaderBoardFragment.openCompareTeamActivity(moreInfoDataList.get(position).getChallengeId());
            }else {

            }*/
        });

        holder.binding.executePendingBindings();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void showCompareTeamPopUp(int position, ViewHolder holder) {
        if (!isPopUpVisible) {

            if (moreInfoDataList.size() > 1) {
                if (position == 1) {

                    LayoutInflater inflater = (LayoutInflater)
                            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View popupView = inflater.inflate(R.layout.custom_pop_up_layout, null, false);

                    // create the popup window
                    int width = LinearLayout.LayoutParams.WRAP_CONTENT;
                    int height = LinearLayout.LayoutParams.WRAP_CONTENT;
                    boolean focusable = true; // lets taps outside the popup also dismiss it
                    final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

                    // show the popup window
                    // which view you pass in doesn't matter, it is only used for the window tolken
//                popupWindow.showAtLocation(holder.binding.contestTeamLayout, Gravity.CENTER, 0, holder.binding.contestTeamLayout.getHeight());
                    popupWindow.setBackgroundDrawable(new ColorDrawable());
                    popupWindow.setOutsideTouchable(true);
                    popupWindow.showAtLocation(holder.itemView, Gravity.CENTER, 0, holder.itemView.getHeight());
                    popupWindow.showAsDropDown(holder.itemView);

                    isPopUpVisible = true;

                    // dismiss the popup window when touched
                    popupView.setOnTouchListener((v, event) -> {
                        popupWindow.dismiss();
                        return true;
                    });
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }


    public void updateData(ArrayList<JoinedContestTeam> list) {
        moreInfoDataList = list;
        notifyDataSetChanged();
    }

    public String getSwitchTeamJoinId() {
        for (int i = 0; i < moreInfoDataList.size(); i++) {
            if ((moreInfoDataList.get(i).getUserid() + "").equalsIgnoreCase(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID))) {
                return moreInfoDataList.get(i).getJoinId() + "";
            }
        }
        return "";
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}