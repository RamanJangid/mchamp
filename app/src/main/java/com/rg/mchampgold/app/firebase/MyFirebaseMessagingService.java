package com.rg.mchampgold.app.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.common.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getName();

    Bitmap bitmap;
    String id;
    String prod_id="";
    String title="Select2Win";
    String message;
    String type="";
    int x = 0;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

      /*  // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (*//* Check if data needs to be processed by long running job *//* true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //cheduleJob();
            } else {
                // Handle message within 10 seconds
                //handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

*/




        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        Log.i(TAG+"1", "From: " + remoteMessage.getFrom());

        Log.i("Received notification",remoteMessage.toString());

        System.out.println("From: " + remoteMessage.getFrom());

        id=  remoteMessage.getMessageId();

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.i(TAG+"2", "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.i(TAG+"3", "Message Notification Body: " + remoteMessage.getNotification().getTitle());
            message= remoteMessage.getNotification().getBody();
            title= remoteMessage.getNotification().getTitle();
        }

        Log.i("mess",remoteMessage.getData().toString());

        System.out.println("mess  "+remoteMessage.getData().toString());

        String data=remoteMessage.getData().toString();
        Log.i(TAG,data);
        try {
            JSONObject mainob=new JSONObject(data);

            JSONObject ob=mainob.getJSONObject("data");

            title = ob.getString("title");
            message = ob.getString("message");

            type= "Notification";

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i(TAG+"4", "Message Notification Body: " + message);
        Log.i(TAG+"5", "Message Notification Body: " + prod_id);
        Log.i(TAG+"6", "Message Notification Body: " + type);


        noti(message,title, bitmap, type);


    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_FCM_TOKEN,token);
    }


    public void noti(String messageBody, String title, Bitmap image, String TrueOrFalse) {

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntent;
        notificationIntent = new Intent(this, NotificationActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent =
                PendingIntent.getActivity(this, 0,
                        notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        String message=messageBody;
        int icon = R.mipmap.ic_launcher;
        long when = System.currentTimeMillis();

        Notification notification;

        if(!type.equals("")) {
            notification = new NotificationCompat.Builder(this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(intent)
                    .setSmallIcon(icon)
                    .setWhen(when)
                    .setStyle(new NotificationCompat.BigPictureStyle()
                            .bigPicture(image).setSummaryText(message))
                    .build();
        }

        else {
            notification = new NotificationCompat.Builder(this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setContentIntent(intent)
                    .setSmallIcon(icon)
                    .setWhen(when)
                    .build();
        }
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        x= new Random().nextInt(101);
        notificationManager.notify(x, notification);

        NotificationCompat.Builder mBuilder =new NotificationCompat.Builder(getApplicationContext(), getPackageName());
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);
//        bigText.setBigContentTitle("title 2");
//        bigText.setSummaryText("title 3");

        Log.i("FCM Message",message);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        mBuilder.setPriority(Notification.PRIORITY_MAX);
        mBuilder.setStyle(bigText);
        mBuilder.setContentText(message).build();

        /*notification = new NotificationCompat.Builder(this,"notify_001")
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(intent)
                .setSmallIcon(R.drawable.logo_without_name)
                .setWhen(when)
                .setPriority(Notification.PRIORITY_MAX)
                .setStyle(bigText)
                .build();*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(getPackageName(),
                    getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(x, mBuilder.build());
    }


    private void sendNotification(String messageBody,String title, Bitmap image, String another, String extra) {
        Intent intent= null;
        intent = new Intent(this, NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(getPackageName(),
                    getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(new Random().nextInt(101), notificationBuilder.build());

    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }
    }
}
