
package com.rg.mchampgold.app.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.AddCashBannerListResponse;
import com.rg.mchampgold.app.dataModel.BannerListItem;
import com.rg.mchampgold.app.dataModel.NormalResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.BannerListAdapter;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityAddBalanceBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;


public class AddBalanceActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    String orderId = "";
    int promoCodeId;
    ActivityAddBalanceBinding mBinding;
    BannerListAdapter mAdapter;
    ArrayList<BannerListItem> bannerListItems = new ArrayList<>();

    @Inject
    OAuthRestService oAuthRestService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_balance);
        MyApplication.getAppComponent().inject(AddBalanceActivity.this);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        initialize();
        AddCashBannerList();
        if (getIntent().hasExtra("balance_required")){
            mBinding.addMoney.setText(String.valueOf((int) Math.ceil(getIntent().getDoubleExtra("balance_required",0))));
        }
        mBinding.add100.setOnClickListener(view -> {
            if (!mBinding.addMoney.getText().toString().equals("")) {
                mBinding.addMoney.setText(String.valueOf((Integer.parseInt(mBinding.addMoney.getText().toString()) + 100)));
            } else {
                mBinding.addMoney.setText("100");
            }
        });

        mBinding.add300.setOnClickListener(view -> {
            if (!mBinding.addMoney.getText().toString().equals("")) {
                mBinding.addMoney.setText(String.valueOf((Integer.parseInt(mBinding.addMoney.getText().toString()) + 300)));
            } else {
                mBinding.addMoney.setText("300");
            }
        });

        mBinding.add500.setOnClickListener(view -> {
            if (!mBinding.addMoney.getText().toString().equals("")) {
                mBinding.addMoney.setText(String.valueOf((Integer.parseInt(mBinding.addMoney.getText().toString()) + 500)));
            } else {
                mBinding.addMoney.setText("500");
            }
        });

        mBinding.tvBonus.setOnClickListener(v -> AppUtils.openWebViewActivity("", MyApplication.bonus_terms));

        mBinding.btnAddCash.setOnClickListener(view -> {
            if (!mBinding.addMoney.getText().toString().trim().equals("")) {
                if (Double.parseDouble(mBinding.addMoney.getText().toString())>0) {
                    orderId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID) + System.currentTimeMillis();
                    openPaymentOptionActivity();
                }else {
                    AppUtils.showErrorr(AddBalanceActivity.this, "Please enter amount greater than 0");
                }
                // getOrderId();
            } else {
                AppUtils.showErrorr(AddBalanceActivity.this, "Please enter amount");
            }
        });

        mBinding.btnPromoApply.setOnClickListener(view -> {
            if (!mBinding.etPromoCode.getText().toString().trim().equals("")) {
                applyPromoCode();
            } else {
                AppUtils.showErrorr(AddBalanceActivity.this, "Please add Promo code");
            }
        });
        if (MyApplication.promo_code.equalsIgnoreCase("1")){
            mBinding.promoTxt.setVisibility(View.GONE);
            mBinding.llPromo.setVisibility(View.GONE);
        }
//        if (getIntent().hasExtra("balance"))
//            mBinding.tvDeposit.setText(getString(R.string.rupee) + getIntent().getDoubleExtra("balance", 0));
//        if (getIntent().hasExtra("bonus"))
//            mBinding.tvWinning.setText(getString(R.string.rupee) + getIntent().getDoubleExtra("bonus", 0));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.add_cash));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mBinding.tvDeposit.setText(getString(R.string.rupee) +   MyApplication.tinyDB.getString(Constants.KEY_USER_BALANCE));
        mBinding.tvWinning.setText(getString(R.string.rupee) +  MyApplication.tinyDB.getString(Constants.KEY_USER_WINING_AMOUNT));
        mBinding.tvFancash.setText(getString(R.string.rupee) +  MyApplication.tinyDB.getString(Constants.KEY_USER_BONUS_BALANCE));
    }



    private void applyPromoCode() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setPromo(mBinding.etPromoCode.getText().toString().trim());
        CustomCallAdapter.CustomCall<NormalResponse> orderIdResponse = oAuthRestService.applyPromoCode(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<NormalResponse>() {
            @Override
            public void success(Response<NormalResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    NormalResponse orderIdResponse1 = response.body();
                    if (orderIdResponse1.getStatus() == 1) {
                        promoCodeId = orderIdResponse1.getPromo_id();
                        mBinding.tvAppliedCode.setText("Promo code Applied");
                         Toast.makeText(AddBalanceActivity.this,"Promo code Applied",Toast.LENGTH_SHORT).show();
                    } else {
                        mBinding.tvAppliedCode.setText("");
                        Toast.makeText(AddBalanceActivity.this, "Invalid Promo code", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(AddBalanceActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    private void AddCashBannerList() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<AddCashBannerListResponse> orderIdResponse = oAuthRestService.addCashBannerList(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<AddCashBannerListResponse>() {
            @Override
            public void success(Response<AddCashBannerListResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    AddCashBannerListResponse bannerListResponse = response.body();
                    if (bannerListResponse.getStatus() == 1) {

                        if (!TextUtils.isEmpty(bannerListResponse.getResult().getAddCashBannerUrl())) {
                            AppUtils.loadImageBanner(mBinding.bannerImage, bannerListResponse.getResult().getAddCashBannerUrl());
                            mBinding.bannerCard.setVisibility(View.VISIBLE);
                            mBinding.tvBonus.setVisibility(View.VISIBLE);

                        } else {
                            mBinding.bannerCard.setVisibility(View.GONE);
                            mBinding.tvBonus.setVisibility(View.GONE);

                        }


//                        bannerListItems = bannerListResponse.getResult();
//                        setupRecyclerView();
                    }
                } else {
                    Toast.makeText(AddBalanceActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    public void openPaymentOptionActivity() {
        Intent intent = new Intent(AddBalanceActivity.this, PaymentOptionActivity.class);
        intent.putExtra("addedValue", mBinding.addMoney.getText().toString().trim());
        intent.putExtra("orderId", orderId);
        intent.putExtra("promoId", promoCodeId);
        startActivityForResult(intent, 101);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == 101) {
            finish();
        }
    }
    private void setupRecyclerView() {
        mAdapter = new BannerListAdapter(getApplicationContext(), bannerListItems, this);
        mBinding.bannerRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.bannerRecycler.setLayoutManager(mLayoutManager);
        mBinding.bannerRecycler.setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
