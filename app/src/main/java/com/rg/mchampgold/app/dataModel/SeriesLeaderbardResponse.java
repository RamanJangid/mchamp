package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class SeriesLeaderbardResponse {

    @SerializedName("status")
    int status;

    @SerializedName("result")
    SeriesLeaderbardData result;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public SeriesLeaderbardData getResult() {
        return result;
    }

    public void setResult(SeriesLeaderbardData result) {
        this.result = result;
    }
}
