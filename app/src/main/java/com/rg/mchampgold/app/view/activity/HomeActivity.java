package com.rg.mchampgold.app.view.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.rg.mchampgold.BuildConfig;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.CheckVersionCodeResponse;

import com.rg.mchampgold.app.dataModel.MyBalanceResponse;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.addCash.AccountsFragment;
import com.rg.mchampgold.app.view.fragment.MyWalletFragment;
import com.rg.mchampgold.app.view.fragment.home.HomeFragment;
import com.rg.mchampgold.app.view.more.MoreFragment;
import com.rg.mchampgold.app.view.myMatches.MyMatchesFragment;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityHomeBinding;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;

import javax.inject.Inject;

import retrofit2.Response;


public class HomeActivity extends AppCompatActivity {


    public ActivityHomeBinding mainBinding;
    public BottomNavigationView navigation;
    @Inject
    OAuthRestService oAuthRestService;
    int currentVersion = 0;
    private int onlineVersion = 0;
    String tag = "1";
    public Menu homeMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        MyApplication.getAppComponent().inject(HomeActivity.this);
        if (getIntent().hasExtra("userId")) {
            if (getIntent().getStringExtra("userId").equalsIgnoreCase("")) {
                Toast.makeText(this, "Invalid Credentials.", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                AppUtils.setFantasyCredentials(getIntent().getStringExtra("userId"), getIntent().getStringExtra("deviceId"));
            }
        } else {
            Toast.makeText(this, "Invalid Credentials.", Toast.LENGTH_SHORT).show();
            finish();
        }
        initialize();
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        if (getIntent().hasExtra("matchFinish")) {
            if (getIntent().getBooleanExtra("matchFinish", false)) {
                AppUtils.showFinishMatchState(this);
            }
        }
    }

    private void getUserBalance() {
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<MyBalanceResponse> myBalanceResponseCustomCall = oAuthRestService.getUserBalance(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<MyBalanceResponse>() {
            @Override
            public void success(Response<MyBalanceResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    MyBalanceResponse myBalanceResponse = response.body();
                    if (myBalanceResponse.getStatus() == 1 && myBalanceResponse.getResult().size() > 0) {
                        MyBalanceResultItem myBalanceResultItem = myBalanceResponse.getResult().get(0);
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, myBalanceResultItem.getBalance() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_WINING_AMOUNT, myBalanceResultItem.getWinning() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BONUS_BALANCE, myBalanceResultItem.getBonus() + "");
                        if (homeMenu != null && !(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MyMatchesFragment)) {
                            AppUtils.setWalletBalance(homeMenu, getResources().getColor(R.color.colorYellow), true, 0, 0);
                        }
                    } else {
                        AppUtils.showErrorr(HomeActivity.this, myBalanceResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                e.printStackTrace();
            }
        });
    }

    void initialize() {
        mainBinding.mChampHome.setOnClickListener(view -> {
            finish();
        });

        navigation = mainBinding.bottomNavigation;
        setSupportActionBar(mainBinding.mytoolbar);
        navigation.setSelectedItemId(R.id.navigation_home);
        setToolBarTitle("");
        mainBinding.ivLogo.setVisibility(View.VISIBLE);
        loadFragment(new HomeFragment());
        navigation.setItemIconTintList(null);
        // mainBinding.bottomNavigation.setItemIconTintList(null);
        mainBinding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            Fragment fragment = null;
            int itemId = item.getItemId();
            if (itemId == R.id.navigation_home) {
                if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof HomeFragment)) {
                    fragment = new HomeFragment();
                    setToolBarTitle("");
                    tag = "1";
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.accent)));
                    mainBinding.ivLogo.setVisibility(View.VISIBLE);
                    //  mainBinding.livefragmentContainer.setVisibility(View.GONE);
                    homeMenu.findItem(R.id.navigation_notification).setVisible(true);
                    homeMenu.findItem(R.id.navigation_wallet).setVisible(true);
                    homeMenu.findItem(R.id.navigation_award).setVisible(true);
                }
            } else if (itemId == R.id.navigation_my_matches) {
                if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MyMatchesFragment)) {
                    fragment = new MyMatchesFragment();
                    tag = "2";
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.accent)));
                    setToolBarTitle(getString(R.string.title_menu_my_matches));
                    mainBinding.ivLogo.setVisibility(View.GONE);
                    homeMenu.findItem(R.id.navigation_notification).setVisible(false);
                    homeMenu.findItem(R.id.navigation_wallet).setVisible(false);
                    homeMenu.findItem(R.id.navigation_award).setVisible(true);
                    // mainBinding.livefragmentContainer.setVisibility(View.GONE);
                }
            } else if (itemId == R.id.navigation_add_cash) {
//                if (MyApplication.walletClickListener != null) {
//                    MyApplication.walletClickListener.onFantasyAccountClick();
//                }
//                return false;
                if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof AccountsFragment)) {
                    fragment = new MyWalletFragment();
                    tag = "3";
                    mainBinding.ivLogo.setVisibility(View.GONE);
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.accent)));
                    //   mainBinding.livefragmentContainer.setVisibility(View.GONE);
                    setToolBarTitle(getString(R.string.title_menu_add_cash));
                    homeMenu.findItem(R.id.navigation_notification).setVisible(false);
                    homeMenu.findItem(R.id.navigation_wallet).setVisible(false);
                    homeMenu.findItem(R.id.navigation_award).setVisible(false);
                }
            } else if (itemId == R.id.navigation_more) {
                if (!(getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof MoreFragment)) {
                    fragment = new MoreFragment();
                    tag = "4";
                    setToolBarTitle(getString(R.string.title_menu_more));
                    getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.accent)));
                    mainBinding.ivLogo.setVisibility(View.GONE);
                    homeMenu.findItem(R.id.navigation_notification).setVisible(false);
                    homeMenu.findItem(R.id.navigation_wallet).setVisible(false);
                    homeMenu.findItem(R.id.navigation_award).setVisible(false);
                    //  mainBinding.livefragmentContainer.setVisibility(View.GONE);
                }
            }
            return loadFragment(fragment);

        });
    }

    public boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment, tag)
                    .commit();
            return true;
        }
//        getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.livefragment_container)).commit();
        return false;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
//            showLogoutDialog();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        homeMenu = menu;
        for (int i = 0; i < menu.size(); i++) {
            Drawable drawable = menu.getItem(i).getIcon();
            if (drawable != null) {
                drawable.mutate();
                drawable.setColorFilter(getResources().getColor(R.color.colorYellow), PorterDuff.Mode.SRC_ATOP);
            }
        }
        AppUtils.setWalletBalance(homeMenu, getResources().getColor(R.color.colorYellow), true, 0, 0);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.navigation_notification) {
            openNotificationActivity();
            return true;

           /* case R.id.navigation_wallet:
                openWalletActivity();
                return true;*/
        }
        return super.onOptionsItemSelected(item);
    }

    private void openNotificationActivity() {
        startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserBalance();
       /* if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0) != 1) {
            {
                startActivity(new Intent(this, OtpVerifyActivity.class));
            }

        } else*/

    }


    public void setToolBarTitle(String title) {
        if (getSupportActionBar() != null) {
            //     getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(title);

            // getSupportActionBar().getTitle().
        }

    }


    private class DownloadFileFromUrl extends AsyncTask<String, String, String> {
        File file = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        @Override
        protected String doInBackground(String... strings) {
            int count = 0;
            try {
                URL url = new URL(strings[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lenghtOfFile = connection.getContentLength();
                BufferedInputStream input = new BufferedInputStream(url.openStream());
                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/Android/data/" + getPackageName() + "/cache/");
                myDir.mkdirs();
                file = new File(myDir, getString(R.string.app_name) + ".apk");
                Log.i("file ", "" + file);
                if (file.exists())
                    file.delete();
                // Output stream to write file
                FileOutputStream output = new FileOutputStream(file);

                //   int data = new ByteArray(1024);
                byte data[] = new byte[1024];

                Long total = (long) 0.0;
                while (count != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (total * 100 / lenghtOfFile));
                    count = input.read(data);
                    // writing data to file
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                //   Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if (file != null) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                intent.setDataAndType(FileProvider.getUriForFile(getApplicationContext(),
                        getPackageName() + ".provider",
                        file), "application/vnd.android.package-archive");
                startActivity(intent);
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            pDialog.setProgress(Integer.parseInt(values[0]));
        }
    }

    ProgressDialog pDialog = null;

    private void showProgress() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Downloading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.show();
    }
}
