package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PanDetailItem {

	@SerializedName("image")
	private String image;

	@SerializedName("imagetype")
	private String imagetype;

	@SerializedName("pandob")
	private String pandob;

	@SerializedName("panname")
	private String panname;

	@SerializedName("pannumber")
	private String pannumber;

	@SerializedName("status")
	private int status;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setImagetype(String imagetype){
		this.imagetype = imagetype;
	}

	public String getImagetype(){
		return imagetype;
	}

	public void setPandob(String pandob){
		this.pandob = pandob;
	}

	public String getPandob(){
		return pandob;
	}

	public void setPanname(String panname){
		this.panname = panname;
	}

	public String getPanname(){
		return panname;
	}

	public void setPannumber(String pannumber){
		this.pannumber = pannumber;
	}

	public String getPannumber(){
		return pannumber;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"PanDetailItem{" +
			"image = '" + image + '\'' + 
			",imagetype = '" + imagetype + '\'' + 
			",pandob = '" + pandob + '\'' + 
			",panname = '" + panname + '\'' + 
			",pannumber = '" + pannumber + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}