package com.rg.mchampgold.app.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.JoinedContesttItem;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.adapter.JoinedContestItemAdapter;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.app.viewModel.ContestDetailsViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentJoinedContestBinding;

import java.util.ArrayList;

import javax.inject.Inject;


public class MyJoinedContestFragment extends Fragment implements OnContestItemClickListener {

    FragmentJoinedContestBinding mBinding;
    ContestDetailsViewModel contestDetailsViewModel;
    JoinedContestItemAdapter mAdapter;
    String matchKey;
    Context context;
    ArrayList<JoinedContesttItem> list = new ArrayList<>();
    int teamCount;
    String sportKey;

    @Inject
    OAuthRestService oAuthRestService;
    public int joinedContestCount = 0;


    public static MyJoinedContestFragment newInstance(String matchKey) {
        MyJoinedContestFragment myFragment = new MyJoinedContestFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KEY_MATCH_KEY, matchKey);
        myFragment.setArguments(args);
        return myFragment;
    }

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_joined_contest, container, false);
        setupRecyclerView();
        /*UpComingContestActivity.fab_create_team.setVisibility(View.GONE);*/

        mBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            getData();
            mBinding.swipeRefreshLayout.setRefreshing(false);
        });

        return mBinding.getRoot();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        contestDetailsViewModel = ContestDetailsViewModel.create(this);
        MyApplication.getAppComponent().inject(contestDetailsViewModel);
        if (getArguments() != null) {
            matchKey = getArguments().getString(Constants.KEY_MATCH_KEY);
            sportKey = getArguments().getString(Constants.SPORT_KEY);
        }
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
       /* UpComingContestActivity.fab_create_team.setVisibility(View.GONE);*/
        // MyApplication.getAppComponent().inject(this);
    }


    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void setupRecyclerView() {
        mAdapter = new JoinedContestItemAdapter(getActivity(), list, this,matchKey);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
        //getData();
        mBinding.noContestToJoin.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).movetoContest());
    }

    @SuppressLint("RestrictedApi")
    private void getData() {
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        contestDetailsViewModel.loadJoinedContestRequest(request);
        contestDetailsViewModel.getJoinedContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getJoinedContestItem().getContest().size() > 0) {
                        mBinding.rlNoTeam.setVisibility(View.GONE);
                        mBinding.rlMainLayout.setVisibility(View.VISIBLE);
                        list = arrayListResource.getData().getJoinedContestItem().getContest();
                        teamCount = arrayListResource.getData().getJoinedContestItem().getUserTeams();
                        joinedContestCount = arrayListResource.getData().getJoinedContestItem().getJoined_leagues();
                        mAdapter.updateData(list);

                       /* if (teamCount == 0)
                            UpComingContestActivity.fab_create_team.setVisibility(View.GONE);*/
                        if (getActivity() != null && getActivity() instanceof UpComingContestActivity)
                            ((UpComingContestActivity) getActivity()).setTabTitle(teamCount, joinedContestCount);
                    } else {
                        mBinding.rlNoTeam.setVisibility(View.VISIBLE);
                        mBinding.rlMainLayout.setVisibility(View.GONE);
                    }
                    break;
                }
            }

        });
    }


    @Override
    public void onContestClick(Contest contest, boolean isForDetail) {
        ((UpComingContestActivity) getActivity()).openJoinedContestActivity(isForDetail, contest);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisible()) {
            if (isVisibleToUser) {
                getData();
            }
        }
    }
}