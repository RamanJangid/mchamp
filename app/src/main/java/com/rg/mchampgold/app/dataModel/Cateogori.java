package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Cateogori {

	@SerializedName("total_contest")
	private int totalContest;

	@SerializedName("user_teams")
	private int userTeamCount;

	@SerializedName("joined_leagues")
	private int joinedContestCount;

	@SerializedName("categories")
	private ArrayList<CategoriesItem> categories;

	public void setTotalContest(int totalContest){
		this.totalContest = totalContest;
	}

	public int getTotalContest(){
		return totalContest;
	}


	public ArrayList<CategoriesItem> getCategories() {
		return categories == null?new ArrayList<>():categories;
	}

	public void setCategories(ArrayList<CategoriesItem> categories) {
		this.categories = categories;
	}

	@Override
 	public String toString(){
		return 
			"Cateogori{" +
			"total_contest = '" + totalContest + '\'' + 
			",categories = '" + categories + '\'' + 
			"}";
		}

	public int getUserTeamCount() {
		return userTeamCount;
	}

	public void setUserTeamCount(int userTeamCount) {
		this.userTeamCount = userTeamCount;
	}

	public int getJoinedContestCount() {
		return joinedContestCount;
	}

	public void setJoinedContestCount(int joinedContestCount) {
		this.joinedContestCount = joinedContestCount;
	}
}