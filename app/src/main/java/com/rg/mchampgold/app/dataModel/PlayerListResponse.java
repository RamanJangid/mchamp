package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PlayerListResponse{

	@SerializedName("result")
	private ArrayList<Player> result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	@SerializedName("limit")
	private Limit limit;

	public void setResult(ArrayList<Player> result){
		this.result = result;
	}

	public ArrayList<Player> getResult(){
		return result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Override
 	public String toString(){
		return 
			"PlayerListResponse{" + 
			"result = '" + result + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
	public Limit getLimit() {
		return limit == null? new Limit():limit;
	}

	public void setLimit(Limit limit) {
		this.limit = limit;
	}
}