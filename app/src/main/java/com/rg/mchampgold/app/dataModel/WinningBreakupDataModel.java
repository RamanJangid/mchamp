package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class WinningBreakupDataModel {

    @SerializedName("price")
    String price;

    @SerializedName("position")
    String position;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
