package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.CategoriesItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.databinding.RecyclerCategoryItemContestBinding;

import java.util.ArrayList;
import java.util.List;

public class CategoryContestItemAdapter extends RecyclerView.Adapter<CategoryContestItemAdapter.ViewHolder> /*implements HeaderItemDecoration.StickyHeaderInterface*/ {

    private List<CategoriesItem> moreInfoDataList;
    private OnContestItemClickListener listener;
    Context context;

/*
    public HeaderItemDecoration.StickyHeaderInterface stickyHeaderInterface;

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 0;
        do {
            if (this.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        }
        while (itemPosition >= 0);

        return headerPosition;
    }


    @Override
    public int getHeaderLayout(int headerPosition) {
        return R.id.contest_header_card_view;
    }


    @Override
    public void bindHeaderData(View header, int headerPosition) {
      //  AppUtils.loadImageCategory(holder.binding.imgCategory, moreInfoDataList.get(position).getContestImageUrl());
    }

    @Override
    public boolean isHeader(int itemPosition) {
        return false;
    }
*/


    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerCategoryItemContestBinding binding;

        ViewHolder(RecyclerCategoryItemContestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public CategoryContestItemAdapter(Context context, List<CategoriesItem> moreInfoDataList, OnContestItemClickListener listener) {
        this.context = context;
        this.moreInfoDataList = moreInfoDataList;
        this.listener = listener;
//        stickyHeaderInterface = this;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerCategoryItemContestBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_category_item_contest,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setCategoriesItem(moreInfoDataList.get(position));

        ContestItemAdapter mAdapter = new ContestItemAdapter(context, moreInfoDataList.get(position).getLeagues(), listener, false);
        holder.binding.rvSubContest.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.binding.rvSubContest.setLayoutManager(mLayoutManager);
        holder.binding.rvSubContest.setAdapter(mAdapter);

        if (moreInfoDataList.get(position).getLeagues().size() > 3) {
            holder.binding.llViewMore.setVisibility(View.VISIBLE);
        } else {
            holder.binding.llViewMore.setVisibility(View.GONE);
        }

        AppUtils.loadImageCategory(holder.binding.imgCategory, moreInfoDataList.get(position).getContestImageUrl());

        holder.binding.llViewMore.setOnClickListener(view -> ((UpComingContestActivity) context).openAllContestActivity(moreInfoDataList.get(position).getId()));
        holder.binding.executePendingBindings();

    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }


    public void updateData(ArrayList<CategoriesItem> list) {
        moreInfoDataList = list;
        notifyDataSetChanged();
    }


}