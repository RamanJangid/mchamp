package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.MultiSportsPlayerBreakPointItem;
import com.rg.mchampgold.app.dataModel.PlayerPointItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.MultiSportsPlayerPointBreakAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityPlayerPointsBreakupBinding;

import java.util.ArrayList;

public class BreakupPlayerPointsActivity extends AppCompatActivity {
    ActivityPlayerPointsBreakupBinding mBinding;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    String playerName;
    String pImage;
    String selectedBy,point;
    int isSelected;
    MultiSportsPlayerPointBreakAdapter mAdapter;
    ArrayList<MultiSportsPlayerBreakPointItem> list = new ArrayList<>();
    PlayerPointItem playerPointItem = new PlayerPointItem();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_player_points_breakup);
          if(getIntent()!=null&&getIntent().getExtras()!=null) {
              list   =(ArrayList<MultiSportsPlayerBreakPointItem>)getIntent().getExtras().getSerializable("playerPointItem");
              playerName = getIntent().getExtras().getString("playerName");
              selectedBy = getIntent().getExtras().getString("selectedBy");
              point = getIntent().getExtras().getString("point");
              pImage = getIntent().getExtras().getString("pImage");
              isSelected = getIntent().getExtras().getInt("isSelected");
              AppUtils.loadImage(mBinding.image,pImage);
          }
        initialize();
//        setData();
        if (isSelected == 1) {
            mBinding.isselectedTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.player_in_your_team, 0);
            mBinding.isselectedTxt.setText("In your team");
        } else {
            mBinding.isselectedTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.player_not_your_team, 0);
            mBinding.isselectedTxt.setText("Not in your team");
        }
        mBinding.selectedBy.setText(selectedBy);
        mBinding.points.setText(point);
        mBinding.name.setText(playerName);
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.points_breakup));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
        }
        setupRecyclerView();
    }
    private void setupRecyclerView() {
        mAdapter = new MultiSportsPlayerPointBreakAdapter(BreakupPlayerPointsActivity.this, list);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
    }
    private void setData() {

        if (playerPointItem.getIsSelected() == 1) {
            mBinding.isselectedTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.player_in_your_team, 0);
            mBinding.isselectedTxt.setText("In your team");
        } else {
            mBinding.isselectedTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.player_not_your_team, 0);
            mBinding.isselectedTxt.setText("Not in your team");
        }


        mBinding.selectedBy.setText("" + playerPointItem.getSelectedBy());
        mBinding.credits.setText("" + playerPointItem.getCredit());
        mBinding.points.setText("" + playerPointItem.getTotal());
        mBinding.name.setText(playerPointItem.getPlayerName());
        mBinding.totalPoint.setText(String.valueOf(playerPointItem.getTotal()));
        mBinding.startingPoint.setText(String.valueOf(playerPointItem.getStartingpoints()));
        mBinding.runPoints.setText(String.valueOf(playerPointItem.getRuns()));
        mBinding.fourPoints.setText(String.valueOf(playerPointItem.getFours()));
        mBinding.sixPoints.setText(String.valueOf(playerPointItem.getSixs()));
        mBinding.centuryPoints.setText(String.valueOf(playerPointItem.getCentury()));
        mBinding.Points150.setText(String.valueOf(playerPointItem.getPoint150()));
        mBinding.Points200.setText(String.valueOf(playerPointItem.getPoint200()));
        mBinding.strikeRatePoints.setText(String.valueOf(playerPointItem.getStrikeRate()));
        mBinding.wicketspoints.setText(String.valueOf(playerPointItem.getWickets()));
        mBinding.economypoints.setText(String.valueOf(playerPointItem.getEconomyRate()));
        mBinding.runoutPoints.setText(String.valueOf(playerPointItem.getRunouts()));
        mBinding.maidenPoints.setText(String.valueOf(playerPointItem.getMaidens()));
        mBinding.catchPoints.setText(String.valueOf(playerPointItem.getCatchPoints()));
        mBinding.stumpingpoints.setText(String.valueOf(playerPointItem.getStumping()));
        mBinding.notOutPoints.setText(String.valueOf(playerPointItem.getNotOut()));
        mBinding.winningBonus.setText(String.valueOf(playerPointItem.getWinnerPoint()));
        mBinding.duck.setText(playerPointItem.getDuck() == null ? "0" : playerPointItem.getDuck());
        mBinding.notOutPoints.setText(String.valueOf(playerPointItem.getNotOut()));
        mBinding.actualnOutTxt.setText(String.valueOf(playerPointItem.getActualNotout()));
        mBinding.actuabonusTotal.setText("" + playerPointItem.getActualWinning());
        mBinding.actualcatTxt.setText("" + playerPointItem.getActualCatch());
        mBinding.actualStartTxt.setText("" + playerPointItem.getActualStartingpoints());
        mBinding.actualrunTxt.setText("" + playerPointItem.getActualRuns());
        mBinding.actualfourTxt.setText("" + playerPointItem.getActualFours());
        mBinding.actualduckTxt.setText("" + playerPointItem.getActualDuck());
        mBinding.actualerTxt.setText("" + playerPointItem.getActualEconomyRate());
        mBinding.actualmdoTxt.setText("" + playerPointItem.getActualMaidens());
        mBinding.actualsixTxt.setText("" + playerPointItem.getActualSixs());
        mBinding.actualsrTxt.setText("" + playerPointItem.getActualStrikeRate());
        mBinding.actualhndrTxt.setText("" + playerPointItem.getCentury());
        mBinding.actualwktsTxt.setText("" + playerPointItem.getActualWicket());
        mBinding.actualroutTxt.setText("" + playerPointItem.getActualRunouts());
        mBinding.actualTotal.setText("" + playerPointItem.getTotal());
        mBinding.actuastumplTotal.setText("" + playerPointItem.getActualStumping());
        if (playerPointItem.getIsTopplayer() == 1) {
            mBinding.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.top_player, 0);
        } else {
            mBinding.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.top_player_disable, 0);
        }

        if (playerPointItem.getActualHalcentury() == 1) {
            mBinding.actualfifthTxt.setText("" + playerPointItem.getActualHalcentury());
            mBinding.halfcenturyPoints.setText(String.valueOf(playerPointItem.getHalcentury()));
            mBinding.cancutryTxt.setText("50's");
        } else if (playerPointItem.getActualCentury() == 1) {
            mBinding.actualfifthTxt.setText("" + playerPointItem.getActualCentury());
            mBinding.halfcenturyPoints.setText(String.valueOf(playerPointItem.getCentury()));
            mBinding.cancutryTxt.setText("100's");
        } else if (playerPointItem.getActualPoint150() == 1) {
            mBinding.actualfifthTxt.setText("" + playerPointItem.getActualPoint150());
            mBinding.halfcenturyPoints.setText(String.valueOf(playerPointItem.getPoint150()));
            mBinding.cancutryTxt.setText("150's");
        } else if (playerPointItem.getActualPoint200() == 1) {
            mBinding.actualfifthTxt.setText("" + playerPointItem.getActualPoint200());
            mBinding.halfcenturyPoints.setText(String.valueOf(playerPointItem.getPoint200()));
            mBinding.cancutryTxt.setText("200's");
        }

        if (playerPointItem.getActualHalcentury() == 0 && playerPointItem.getActualCentury() == 0 && playerPointItem.getActualPoint150() == 0 && playerPointItem.getActualPoint200() == 0) {
            mBinding.haflLayout.setVisibility(View.GONE);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
