package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class LiveMatchesScorePlayerListItem {

    @SerializedName("name")
    private String name;

    @SerializedName("how_out")
    private String how_out;

    @SerializedName("run")
    private int run;

    @SerializedName("ball")
    private int ball;

    @SerializedName("four")
    private int four;

    @SerializedName("six")
    private int six;

    @SerializedName("inning")
    private int inning;

    @SerializedName("app_points")
    private int app_points;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHow_out() {
        return how_out;
    }

    public void setHow_out(String how_out) {
        this.how_out = how_out;
    }

    public int getRun() {
        return run;
    }

    public void setRun(int run) {
        this.run = run;
    }

    public int getBall() {
        return ball;
    }

    public void setBall(int ball) {
        this.ball = ball;
    }

    public int getFour() {
        return four;
    }

    public void setFour(int four) {
        this.four = four;
    }

    public int getSix() {
        return six;
    }

    public void setSix(int six) {
        this.six = six;
    }

    public int getInning() {
        return inning;
    }

    public void setInning(int inning) {
        this.inning = inning;
    }

    public int getApp_points() {
        return app_points;
    }

    public void setApp_points(int app_points) {
        this.app_points = app_points;
    }
}
