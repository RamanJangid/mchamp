package com.rg.mchampgold.app.api.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class ScratchHistoryResponse implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("max_scratch_amount")
    @Expose
    private Integer maxScratchAmount;
    @SerializedName("min_scratch_amount")
    @Expose
    private Integer minScratchAmount;

    @Expose
    @SerializedName("total_amount")
    private Integer total_amount;


    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getMaxScratchAmount() {
        return maxScratchAmount;
    }

    public void setMaxScratchAmount(Integer maxScratchAmount) {
        this.maxScratchAmount = maxScratchAmount;
    }

    public Integer getMinScratchAmount() {
        return minScratchAmount;
    }

    public void setMinScratchAmount(Integer minScratchAmount) {
        this.minScratchAmount = minScratchAmount;
    }


    public Integer getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Integer total_amount) {
        this.total_amount = total_amount;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }


    public class Result {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("refercode")
        @Expose
        private String refercode;
        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("refer_id")
        @Expose
        private Integer referId;
        @SerializedName("coupon_type")
        @Expose
        private Integer couponType;
        @SerializedName("coupon_amount")
        @Expose
        private String couponAmount;
        @SerializedName("is_percentage")
        @Expose
        private Integer isPercentage;
        @SerializedName("additional")
        @Expose
        private String additional;
        @SerializedName("active")
        @Expose
        private Integer active;
        @SerializedName("is_scratched")
        @Expose
        private Integer isScratched;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getRefercode() {
            return refercode;
        }

        public void setRefercode(String refercode) {
            this.refercode = refercode;
        }

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getReferId() {
            return referId;
        }

        public void setReferId(Integer referId) {
            this.referId = referId;
        }

        public Integer getCouponType() {
            return couponType;
        }

        public void setCouponType(Integer couponType) {
            this.couponType = couponType;
        }

        public String getCouponAmount() {
            return couponAmount;
        }

        public void setCouponAmount(String couponAmount) {
            this.couponAmount = couponAmount;
        }

        public Integer getIsPercentage() {
            return isPercentage;
        }

        public void setIsPercentage(Integer isPercentage) {
            this.isPercentage = isPercentage;
        }

        public String getAdditional() {
            return additional;
        }

        public void setAdditional(String additional) {
            this.additional = additional;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

        public Integer getIsScratched() {
            return isScratched;
        }

        public void setIsScratched(Integer isScratched) {
            this.isScratched = isScratched;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

    }

}