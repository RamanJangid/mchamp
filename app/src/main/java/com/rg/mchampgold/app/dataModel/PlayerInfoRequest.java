package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PlayerInfoRequest {
	@SerializedName("matchkey")
	private String matchKey;

	@SerializedName("playerid")
	private String playerid;

	@SerializedName("sport_key")
	private String sport_key;

	@SerializedName("user_id")
	private String user_id;

	public String getMatchKey() {
		return matchKey;
	}

	public void setMatchKey(String matchKey) {
		this.matchKey = matchKey;
	}

	public String getPlayerid() {
		return playerid;
	}

	public void setPlayerid(String playerid) {
		this.playerid = playerid;
	}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
}