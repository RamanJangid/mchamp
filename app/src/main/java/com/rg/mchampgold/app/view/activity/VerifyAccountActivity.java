package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.AllVerifyItem;
import com.rg.mchampgold.app.dataModel.AllVerifyResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.fragment.BankVerificationFragment;
import com.rg.mchampgold.app.view.fragment.MobileVarificationFragment;
import com.rg.mchampgold.app.view.fragment.PanValidationFragment;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityVerifyAccountBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class VerifyAccountActivity extends AppCompatActivity {
    public ActivityVerifyAccountBinding mBinding;
    AllVerifyItem allVerifyItem;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_verify_account);
        MyApplication.getAppComponent().inject(VerifyAccountActivity.this);
        initialize();
        allVerify();

    }

    public class SectionPagerAdapter extends FragmentStatePagerAdapter {

        SectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
//                case 0:
//                    return new MobileVarificationFragment();
                case 0:
                    return new PanValidationFragment();
                default:
                    return new BankVerificationFragment();
            }
        }

        @Override
        public int getCount() {

             return 2;
            /*if ((MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0) == 1) &&
                    (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 0) == 1)
                    && (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0) == 1 || MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0) == 0)) {
                return 3;
            } else if ((MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0) == 1) &&
                    (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 0) == 1)) {
                return 2;
            } else {
                return 1;
            }*/
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
//                case 0:
//                    return "Email & Mobile";
                case 0:
                    return "PAN Card";
                default:
                    return "Bank";
            }
        }

    }


    private void allVerify() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<AllVerifyResponse> allVerifyResponseCustomCall = oAuthRestService.allVerify(baseRequest);
        allVerifyResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<AllVerifyResponse>() {
            @Override
            public void success(Response<AllVerifyResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    AllVerifyResponse allVerifyResponse = response.body();
                    if (allVerifyResponse.getStatus() == 1) {
                        allVerifyItem = allVerifyResponse.getResult();

                        if (allVerifyItem.getBankVerify() == 1)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 1);
                        else if (allVerifyItem.getBankVerify() == 0)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 0);
                        else if (allVerifyItem.getBankVerify() == -1)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, -1);
                        else if (allVerifyItem.getBankVerify() == 2) {
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 2);
                            // session.setBank_comment(verifyList.get(0).getBank_comment());
                        }
                        if (allVerifyItem.getPanVerify() == 1)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 1);
                        else if (allVerifyItem.getPanVerify() == 0)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0);
                        else if (allVerifyItem.getPanVerify() == -1)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, -1);
                        else if (allVerifyItem.getPanVerify() == 2) {
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 2);
                            //session.setPan_comment(verifyList.get(0).getPan_comment());
                        }

                        if (allVerifyItem.getEmailVerify() == 1)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 1);
                        else
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 0);

                        if (allVerifyItem.getMobileVerify() == 1)
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 1);
                        else
                            MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0);
                        notifyProgressView();
                        mBinding.tabLayout.setupWithViewPager(mBinding.vp);
                        mBinding.vp.setAdapter(new SectionPagerAdapter(getSupportFragmentManager()));


                    } else {
                        AppUtils.showErrorr(VerifyAccountActivity.this, allVerifyResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });


    }

    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.verify_account));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        mBinding.vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mBinding.vp.setCurrentItem(position);
                //  notifyProgressView();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mBinding.vp.setCurrentItem(tab.getPosition());
                //  notifyProgressView();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void notifyProgressView() {

        int is_email_verify = allVerifyItem.getEmailVerify();
        int is_bank_verify = allVerifyItem.getBankVerify();
        int is_pan_verify = allVerifyItem.getPanVerify();
        int pos = mBinding.vp.getCurrentItem();

        if (is_email_verify == 1 && is_bank_verify == 1 && is_pan_verify == 1) {
            mBinding.ivEmailVerify.setImageResource(R.drawable.ic_green_tick);
            mBinding.ivPanVerify.setImageResource(R.drawable.ic_green_tick);
            mBinding.ivBankVerify.setImageResource(R.drawable.ic_green_tick);
        } else if (is_email_verify == 1) {
            mBinding.ivEmailVerify.setImageResource(R.drawable.ic_green_tick);

            if (pos == 1)
                mBinding.ivPanVerify.setImageResource(R.drawable.ic_pan_verified);
            else
                mBinding.ivPanVerify.setImageResource(R.drawable.ic_pan_blank);

            if (pos == 2)
                mBinding.ivBankVerify.setImageResource(R.drawable.ic_bank_verified);
            else
                mBinding.ivBankVerify.setImageResource(R.drawable.ic_bank_blank);

        } else if (is_pan_verify == 1) {
            mBinding.ivEmailVerify.setImageResource(R.drawable.ic_email_green);
            mBinding.ivPanVerify.setImageResource(R.drawable.ic_green_tick);
            if (pos == 2)
                mBinding.ivBankVerify.setImageResource(R.drawable.ic_bank_verified);
            else
                mBinding.ivBankVerify.setImageResource(R.drawable.ic_bank_blank);

        } else if (is_bank_verify == 1) {
            mBinding.ivEmailVerify.setImageResource(R.drawable.ic_email_green);
            mBinding.ivPanVerify.setImageResource(R.drawable.ic_pan_verified);
            mBinding.ivBankVerify.setImageResource(R.drawable.ic_green_tick);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
