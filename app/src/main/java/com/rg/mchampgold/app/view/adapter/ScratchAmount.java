package com.rg.mchampgold.app.view.adapter;

public class ScratchAmount {

    boolean is_scratch = false;
    double scratched_amount = 0.0;

    public ScratchAmount(boolean is_scratch, double scratched_amount) {
        this.is_scratch = is_scratch;
        this.scratched_amount = scratched_amount;
    }

    public boolean isIs_scratch() {
        return is_scratch;
    }

    public void setIs_scratch(boolean is_scratch) {
        this.is_scratch = is_scratch;
    }

    public double getScratched_amount() {
        return scratched_amount;
    }

    public void setScratched_amount(double scratched_amount) {
        this.scratched_amount = scratched_amount;
    }

    public ScratchAmount() {
    }
}
