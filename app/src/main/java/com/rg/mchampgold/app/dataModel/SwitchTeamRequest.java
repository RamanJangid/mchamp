package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SwitchTeamRequest{

	@SerializedName("challenge_id")
	private String challengeId;

	@SerializedName("teamid")
	private String teamid;

	@SerializedName("matchkey")
	private String matchkey;

	@SerializedName("joinid")
	private String joinid;

	@SerializedName("user_id")
	private String userid;

	@SerializedName("sport_key")
	private String sport_key;

	public void setChallengeId(String challengeId){
		this.challengeId = challengeId;
	}

	public String getChallengeId(){
		return challengeId;
	}

	public void setTeamid(String teamid){
		this.teamid = teamid;
	}

	public String getTeamid(){
		return teamid;
	}

	public void setMatchkey(String matchkey){
		this.matchkey = matchkey;
	}

	public String getMatchkey(){
		return matchkey;
	}

	public void setJoinid(String joinid){
		this.joinid = joinid;
	}

	public String getJoinid(){
		return joinid;
	}

	public void setUserid(String userid){
		this.userid = userid;
	}

	public String getUserid(){
		return userid;
	}

	@Override
 	public String toString(){
		return 
			"SwitchTeamRequest{" + 
			"challenge_id = '" + challengeId + '\'' + 
			",teamid = '" + teamid + '\'' + 
			",matchkey = '" + matchkey + '\'' + 
			",joinid = '" + joinid + '\'' + 
			",userid = '" + userid + '\'' + 
			"}";
		}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}
}