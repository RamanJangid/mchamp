package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SeriesLeaderbardDeatilsResponse {

    @SerializedName("success")
    boolean success;

    @SerializedName("message")
    String message;

    @SerializedName("name")
    String name;

    @SerializedName("image")
    String image;

    @SerializedName("rank")
    String rank;

    @SerializedName("points")
    String points;

    @SerializedName("amount")
    String amount;

    @SerializedName("series_name")
    String series_name;

    @SerializedName("series_date")
    String series_date;

    @SerializedName("week_number")
    String week_number;

    @SerializedName("match_list")
    ArrayList<LeaderboardMatchData> match_list;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSeries_name() {
        return series_name;
    }

    public void setSeries_name(String series_name) {
        this.series_name = series_name;
    }

    public String getSeries_date() {
        return series_date;
    }

    public void setSeries_date(String series_date) {
        this.series_date = series_date;
    }

    public String getWeek_number() {
        return week_number;
    }

    public void setWeek_number(String week_number) {
        this.week_number = week_number;
    }

    public ArrayList<LeaderboardMatchData> getMatch_list() {
        return match_list;
    }

    public void setMatch_list(ArrayList<LeaderboardMatchData> match_list) {
        this.match_list = match_list;
    }
}
