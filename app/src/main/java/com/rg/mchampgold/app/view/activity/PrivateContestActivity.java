package com.rg.mchampgold.app.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.CreatePrivateContestRequest;
import com.rg.mchampgold.app.dataModel.CreatePrivateContestResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.TeamCreatedListener;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityPrivateContestBinding;

import javax.inject.Inject;

import retrofit2.Response;

public class PrivateContestActivity extends AppCompatActivity implements TeamCreatedListener {

    ActivityPrivateContestBinding mBinding;

    @Inject
    OAuthRestService oAuthRestService;
    String matchKey = "";
    String sportKey = "";
    Double entryfees;
    int isMultiJoin = 0;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    Context context;
    String headerText;
    String date;
    int challengeId;
    int teamCount = 0;

    public static TeamCreatedListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_private_contest);
        MyApplication.getAppComponent().inject(PrivateContestActivity.this);
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.create_contest));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            date = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
            teamCount = getIntent().getExtras().getInt(Constants.USER_TEAM_COUNT, 0);
        }

        String teams[] = teamVsName.split(" ");
        mBinding.matchHeaderInfo.tvTeam1.setText(teams[0]);
        mBinding.matchHeaderInfo.tvTeam2.setText(teams[2]);

        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam2, teamSecondUrl);

        mBinding.switchMultipleEntry.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b)
                isMultiJoin = 1;
            else
                isMultiJoin = 0;
        });

        TextWatcher watch1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!mBinding.amount.getText().toString().trim().equals("") && !mBinding.amount.getText().toString().trim().equals("") && !mBinding.numWinners.getText().toString().trim().equals("")) {
                    Double total = (Double.parseDouble(mBinding.amount.getText().toString().trim()) * Constants.DEFAULT_PERCENT) / Double.parseDouble(mBinding.numWinners.getText().toString().trim());
                    mBinding.entryFee.setText("₹ " + String.format("%.1f", total));
                    entryfees = total;
                } else {
                    mBinding.entryFee.setText("₹ 0.0");
                    entryfees = 0.0;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        TextWatcher watch2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!mBinding.amount.getText().toString().trim().equals("") && !mBinding.amount.getText().toString().trim().equals("") && !mBinding.numWinners.getText().toString().trim().equals("")) {
                    Double total = (Double.parseDouble(mBinding.amount.getText().toString().trim()) * Constants.DEFAULT_PERCENT) / Double.parseDouble(mBinding.numWinners.getText().toString().trim());
                    mBinding.entryFee.setText("₹ " + String.format("%.1f", total));
                    entryfees = total;
                } else {
                    mBinding.entryFee.setText("₹ 0.0");
                    entryfees = 0.0;
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        mBinding.btnCreate.setOnClickListener(view -> validate());

        mBinding.amount.addTextChangedListener(watch1);
        mBinding.numWinners.addTextChangedListener(watch2);


        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(date), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));

                    mBinding.matchHeaderInfo.tvMatchTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");


                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.matchHeaderInfo.tvMatchTimer.setText("00h 00m 00s");
                }
            };
            countDownTimer.start();
        } catch (Exception e) {

        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

      /*  if (item.getItemId() == R.id.navigation_wallet) {
            startActivity(new Intent(PrivateContestActivity.this, MyWalletActivity.class));
            return true;
        }*/


        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.menu_wallet, menu);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                challengeId = data.getIntExtra("challengeId", 0);
                openTeamForJoin();
            }
        } else if (requestCode == 101) {
            finish();
        }
    }

    private void createPrivateContest() {
        mBinding.setRefreshing(true);
        CreatePrivateContestRequest request = new CreatePrivateContestRequest();
        request.setName(mBinding.name.getText().toString().trim());
        request.setUserid(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchkey(matchKey);
        request.setSport_key(sportKey);
        request.setWinAmount(mBinding.amount.getText().toString().trim());
        request.setMaximumUser(mBinding.numWinners.getText().toString().trim());
        request.setEntryfee(String.valueOf(entryfees));
        request.setMultiEntry(String.valueOf(isMultiJoin));
        request.setIsPublic(String.valueOf(0));
        CustomCallAdapter.CustomCall<CreatePrivateContestResponse> userFullDetailsResponseCustomCall = oAuthRestService.createContest(request);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<CreatePrivateContestResponse>() {
            @Override
            public void success(Response<CreatePrivateContestResponse> response) {
                mBinding.setRefreshing(false);
                CreatePrivateContestResponse findJoinTeamResponse = response.body();
                if (findJoinTeamResponse != null) {
                    if (findJoinTeamResponse.getStatus() == 1) {
                        //   findJoinTeamResponse.getResult().get(0).get
                        Toast.makeText(PrivateContestActivity.this, "Contest Created Successfully", Toast.LENGTH_SHORT).show();
                        challengeId = findJoinTeamResponse.getResult().get(0).getChallengeid();
                        openTeamForJoin();

                    } else {
                        Toast.makeText(PrivateContestActivity.this, findJoinTeamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PrivateContestActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }

    private void validate() {
        if (mBinding.name.getText().toString().trim().equals("")) {
            AppUtils.showErrorr(PrivateContestActivity.this, "Please Enter Challenge Name");
            return;
        } else if (mBinding.amount.getText().toString().trim().equals("")) {
            AppUtils.showErrorr(PrivateContestActivity.this, "Please Enter Winning Amount");
            return;
        } else if (Integer.parseInt(mBinding.amount.getText().toString().trim()) > 10000) {
            AppUtils.showErrorr(PrivateContestActivity.this, "Please enter a valid amount");
            return;
        } else if (mBinding.numWinners.getText().toString().trim().equals("")) {
            AppUtils.showErrorr(PrivateContestActivity.this, "Please Enter Challenge Size");
            return;
        } else if (mBinding.numWinners.getText().toString().trim().equals("")) {
            AppUtils.showErrorr(PrivateContestActivity.this, "Please Enter Challenge Size");
            return;
        } else if (Integer.parseInt(mBinding.numWinners.getText().toString().trim()) < 2) {
            AppUtils.showErrorr(PrivateContestActivity.this, "You need to choose minimum 2 Challenges size");
            return;
        } else if (Integer.parseInt(mBinding.numWinners.getText().toString().trim()) > 100) {
            AppUtils.showErrorr(PrivateContestActivity.this, "You can choose upto 100 Challenges size");
            return;
        } else {
            CreatePrivateContestRequest request = new CreatePrivateContestRequest();
            request.setName(mBinding.name.getText().toString().trim());
            request.setUserid(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
            request.setMatchkey(matchKey);
            request.setWinAmount(mBinding.amount.getText().toString().trim());
            request.setMaximumUser(mBinding.numWinners.getText().toString().trim());
            request.setEntryfee(String.valueOf(entryfees));
            request.setMultiEntry(String.valueOf(isMultiJoin));
            request.setSport_key(sportKey);
            request.setIsPublic(String.valueOf(0));

            Intent intent = new Intent(PrivateContestActivity.this, WinnerBreakupMatchManagerActivity.class);
            intent.putExtra("request", request);
            intent.putExtra("teamvsname", teamVsName);
            intent.putExtra("team1url", teamFirstUrl);
            intent.putExtra("team2url", teamSecondUrl);
            intent.putExtra("date", headerText);
            startActivityForResult(intent, 100);
        }
    }


    public void openTeamForJoin() {
        if (teamCount > 0) {
            Contest contest = new Contest();
            contest.setId(challengeId);
            contest.setEntryfee(String.format("%.2f", entryfees));
            Intent intent = new Intent(PrivateContestActivity.this, MyTeamsActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
            intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.SPORT_KEY, sportKey);
            startActivityForResult(intent, 101);
        } else {

            listener = this;
            Intent intent = new Intent(PrivateContestActivity.this, CreateTeamActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.SPORT_KEY, sportKey);
            startActivity(intent);
        }
    }


    @Override
    public void getTeamCreated(boolean team_created) {
        if (team_created) {
            teamCount++;
            openTeamForJoin();
        }
    }
}
