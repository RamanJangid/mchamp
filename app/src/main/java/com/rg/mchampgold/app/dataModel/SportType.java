package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class SportType {

	@SerializedName("sport_name")
	private String sportName;

	@SerializedName("sport_key")
	private String sport_key;


	public String getSportName() {
		return sportName;
	}

	public void setSportName(String sportName) {
		this.sportName = sportName;
	}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}
}
