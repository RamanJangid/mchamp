package com.rg.mchampgold.app.view.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.utils.Constants;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

public class RazorPayMerchantActivity extends AppCompatActivity implements PaymentResultListener {


    private static final String TAG = RazorPayMerchantActivity.class.getSimpleName();

    String amount = "";
    String orderId = "";
    String method = "";
    String email = "";
    String phone = "";


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        email = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL);
        phone = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_MOBILE);
        if (getIntent() != null && getIntent().getExtras() != null) {
            amount = getIntent().getExtras().getString("addedValue");
            orderId = getIntent().getExtras().getString("orderId");
            method = getIntent().getExtras().getString("method");
        }

        int amoun = Integer.parseInt(amount);
        amoun = amoun * 100;
        amount = String.valueOf(amoun);
        Checkout.preload(getApplicationContext());
        startPayment();
    }


    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */

        final Activity activity = this;
        final Checkout co = new Checkout();
        co.setImage(R.mipmap.ic_launcher);

        try {
            JSONObject options = new JSONObject();
            options.put("name", getString(R.string.app_name));
            options.put("description", "Add Cash");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png");
            options.put("currency", "INR");
            options.put("order_id", orderId);
            options.put("amount", amount);
            options.put("payment_capture", 1);
            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", phone);
            preFill.put("method", method);
            options.put("prefill", preFill);
            co.open(activity, options);
          //  Log.e("options",options.toString());

        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {
        try {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", razorpayPaymentID);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
            // Toast.makeText(this, "Payment Successful: " + razorpayPaymentID, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int code, String response) {
        try {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", "");
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
            //  Toast.makeText(this, "Payment failed: " + code + " " + response, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "Exception in onPaymentError", e);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog d = new AlertDialog.Builder(RazorPayMerchantActivity.this).create();
        d.setMessage("Are you sure to cancel the payment");
        d.setTitle("Cancel Payment");
        d.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", (dialogInterface, i) -> finish());
        d.setButton(AlertDialog.BUTTON_NEGATIVE, "No", (dialogInterface, i) -> {});
        d.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
