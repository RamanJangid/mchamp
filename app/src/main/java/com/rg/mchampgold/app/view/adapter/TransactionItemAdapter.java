package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.TransactionItem;
import com.rg.mchampgold.databinding.RecyclerItemTransactionHistoryBinding;

import java.util.ArrayList;

public class TransactionItemAdapter extends RecyclerView.Adapter<TransactionItemAdapter.ViewHolder> {

    private ArrayList<TransactionItem> transactionItems;

    private Context context;

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemTransactionHistoryBinding binding;

        ViewHolder(RecyclerItemTransactionHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public TransactionItemAdapter(ArrayList<TransactionItem> transactionItems, Context context) {
        this.transactionItems = transactionItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemTransactionHistoryBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_transaction_history,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setTransaction(transactionItems.get(position));
        holder.binding.rlTransactionHistoryItem.setOnClickListener(view -> showTransactionDetailDialog(transactionItems.get(position)));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return transactionItems.size();
    }


    public void updateData(ArrayList<TransactionItem> list) {
        transactionItems = list;
        notifyDataSetChanged();
    }


    private void showTransactionDetailDialog(TransactionItem data) {
        View view = LayoutInflater.from(context).inflate(R.layout.transaction_detail_popup, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        view.findViewById(R.id.iv_close).setOnClickListener(view1 -> alertDialog.dismiss());
        TextView tv_match, tv_contest, tv_amount, tv_available,title;
        tv_contest = view.findViewById(R.id.tv_contest_name);
        tv_match = view.findViewById(R.id.tv_match);
        tv_amount = view.findViewById(R.id.tv_amount);
        tv_available = view.findViewById(R.id.tv_available_balance);
        title = view.findViewById(R.id.title);

        tv_amount.setText(" " + data.getTypeTransactionAmount());
        tv_available.setText(context.getString(R.string.rupee) + " " + data.getAvailable());
        tv_match.setText(data.getMatchname().equals("") ? "--" : data.getMatchname());
        tv_contest.setText(data.getChallengename().equals("") ? "--" : data.getChallengename());
        title.setText(data.getTransactionType().equals("") ? "Title" : data.getTransactionType());

        alertDialog.show();
    }

}