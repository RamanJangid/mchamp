package com.rg.mchampgold.app.view.adapter;


import android.view.LayoutInflater;

import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;

import com.rg.mchampgold.app.dataModel.WinnerScoreCardItem;
import com.rg.mchampgold.app.view.interfaces.OnMoreItemClickListener;
import com.rg.mchampgold.databinding.RecyclerItemPriceCardBinding;

import java.util.ArrayList;
import java.util.List;

public class WinnerBreakupItemAdapter extends RecyclerView.Adapter<WinnerBreakupItemAdapter.ViewHolder> {

    private List<WinnerScoreCardItem> moreInfoDataList;
    private OnMoreItemClickListener listener;


    public class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemPriceCardBinding binding;

        public ViewHolder(RecyclerItemPriceCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public WinnerBreakupItemAdapter(List<WinnerScoreCardItem> moreInfoDataList) {
        this.moreInfoDataList = moreInfoDataList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerItemPriceCardBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_price_card,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setPriceCard(moreInfoDataList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }

    public void update(ArrayList<WinnerScoreCardItem>  list) {
        moreInfoDataList = list;
    }



}