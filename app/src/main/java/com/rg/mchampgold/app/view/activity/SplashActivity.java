package com.rg.mchampgold.app.view.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.firebase.messaging.FirebaseMessaging;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.extraLib.ConnectionDetector;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.RootUtil;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivitySplashBinding;
import com.splunk.mint.Mint;

import javax.inject.Inject;

public class SplashActivity extends AppCompatActivity {

    //  int currentVersion;
    // private int onlineVersion =0;
    ConnectionDetector cd;
    String TAG = "main page";
    ActivitySplashBinding activitySplashBinding;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        MyApplication.getAppComponent().inject(SplashActivity.this);
        cd = new ConnectionDetector(getApplicationContext());
        activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        if (RootUtil.isDeviceRooted()) {
            showAlertDialogAndExitApp("This device is rooted. You can't use this app.");
        } else {
            snack();
        }

        // enable crash analytics added by pkb
        Mint.initAndStartSession(this.getApplication(), "7d923529");
        FirebaseMessaging.getInstance().subscribeToTopic("global");
    }

    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                (dialog, which) -> {
                    dialog.dismiss();
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                });

        alertDialog.show();
    }


    @SuppressLint("WrongConstant")
    public void snack() {
        String[] perarr = new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.INTERNET,
                Manifest.permission.VIBRATE,
                Manifest.permission.CAMERA,
                Manifest.permission.GET_ACCOUNTS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.DELETE_CACHE_FILES,
                Manifest.permission.MANAGE_DOCUMENTS
        };

        if (Build.VERSION.SDK_INT < 21) {
            final AlertDialog d = new AlertDialog.Builder(SplashActivity.this).create();
            d.setCanceledOnTouchOutside(false);
            d.setTitle("Android version not supported");
            d.setMessage(getString(R.string.app_name) + " doesn't support version lower than android LOLLIPOP");
            d.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", (dialog, which) -> finish());
            d.show();
        } else {
            if (checkPermission()) {
                if (cd.isConnectingToInternet()) {
                    // if (currentVersion >= onlineVersion) {
                    openMainActivity();
                    // }
                } else {
                    LayoutInflater inflater1 = (getLayoutInflater());
                    View layout = inflater1.inflate(R.layout.custom_toast1,
                            findViewById(R.id.toast_layout_root));

                    TextView text = layout.findViewById(R.id.text);
                    text.setText("No Internet connection !!");

                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, +150);
                    toast.setDuration(2000);
                    toast.setView(layout);
                    toast.show();
                }
            } else {
                ActivityCompat.requestPermissions(SplashActivity.this, perarr, 1);
            }

        }
    }


    private void openMainActivity() {

        new Handler().postDelayed(() -> {
            if (MyApplication.tinyDB.getBoolean(Constants.SHARED_PREFERENCES_IS_LOGGED_IN, false))
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            else
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        }, Constants.SPLASH_TIMEOUT);

    }


/*
    public void checkVersionCode()
    {
        activitySplashBinding.setRefreshing(true);
        CustomCallAdapter.CustomCall<CheckVersionCodeResponse> checkVersionCodeResponseCustomCall = oAuthRestService.checkVersionCode();
        checkVersionCodeResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<CheckVersionCodeResponse>() {
            @Override
            public void success(Response<CheckVersionCodeResponse> response) {
                activitySplashBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    CheckVersionCodeResponse normalResponse = response.body();
                    if(normalResponse.getStatus() ==1) {
                        onlineVersion = normalResponse.getResult().getStatus();
                        Log.i("ONline Version", String.valueOf(onlineVersion));
                        snack();
                        if (currentVersion < onlineVersion) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);

                            builder.setMessage("New Version for "+Constants.APP_NAME+" is available for download. Kindly update for latest features.")
                                    .setCancelable(false)
                                    .setPositiveButton("Update", (dialog, id) -> {
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MyApplication.apk_url)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MyApplication.apk_url)));
                                        }
                                    });

                            *//*.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //  Action for 'NO' Button
                                    dialog.dismiss();
                                    snack();
                                }
                            });*//*
                            AlertDialog alert = builder.create();
                            alert.setCanceledOnTouchOutside(false);
                            alert.setOnCancelListener(dialogInterface -> finish());
                            alert.show();

                        } else {

                        }

                    }
                    else {
                        AppUtils.showErrorr(SplashActivity.this,normalResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                activitySplashBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }*/


    @SuppressLint("WrongConstant")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length == 9) {
            if (cd.isConnectingToInternet()) {
                // if (currentVersion >= onlineVersion) {
                openMainActivity();
                //  }
            } else {
                LayoutInflater inflater1 = (getLayoutInflater());
                View layout = inflater1.inflate(R.layout.custom_toast1, findViewById(R.id.toast_layout_root));
                TextView text = layout.findViewById(R.id.text);
                text.setText("No Internet connection !!");
                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.BOTTOM, 0, +150);
                toast.setDuration(2000);
                toast.setView(layout);
                toast.show();
            }
        }
    }

    private boolean checkPermission() {

        String permission1 = "android.permission.VIBRATE";
        int res1 = getApplicationContext().checkCallingOrSelfPermission(permission1);

        String permission2 = "android.permission.GET_ACCOUNTS";
        int res2 = getApplicationContext().checkCallingOrSelfPermission(permission2);

        String permission3 = "android.permission.INTERNET";
        int res3 = getApplicationContext().checkCallingOrSelfPermission(permission3);

        String permission4 = "android.permission.READ_PHONE_STATE";
        int res4 = getApplicationContext().checkCallingOrSelfPermission(permission4);

        String permission5 = "android.permission.READ_EXTERNAL_STORAGE";
        int res5 = getApplicationContext().checkCallingOrSelfPermission(permission5);

        String permission6 = "android.permission.WRITE_EXTERNAL_STORAGE";
        int res6 = getApplicationContext().checkCallingOrSelfPermission(permission6);

        String permission7 = "android.permission.DELETE_CACHE_FILES";
        int res7 = getApplicationContext().checkCallingOrSelfPermission(permission7);

        String permission8 = "android.permission.MANAGE_DOCUMENTS";
        int res8 = getApplicationContext().checkCallingOrSelfPermission(permission8);

        String permission9 = "android.permission.CAMERA";
        int res9 = getApplicationContext().checkCallingOrSelfPermission(permission9);

        if (res1 == PackageManager.PERMISSION_GRANTED && res2 == PackageManager.PERMISSION_GRANTED && res3 == PackageManager.PERMISSION_GRANTED && res4 == PackageManager.PERMISSION_GRANTED && res5 == PackageManager.PERMISSION_GRANTED && res6 == PackageManager.PERMISSION_GRANTED && res7 == PackageManager.PERMISSION_GRANTED && res8 == PackageManager.PERMISSION_GRANTED && res9 == PackageManager.PERMISSION_GRANTED )
            return (true);
        else
            return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
