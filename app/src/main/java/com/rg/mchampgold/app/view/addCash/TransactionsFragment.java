package com.rg.mchampgold.app.view.addCash;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.dataModel.MyTransactionHistoryResponse;
import com.rg.mchampgold.app.dataModel.TransactionItem;
import com.rg.mchampgold.app.dataModel.TransactionRequest;
import com.rg.mchampgold.app.utils.MyDividerItemDecoration;
import com.rg.mchampgold.app.view.adapter.TransactionItemAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentTransactionsBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;


public class TransactionsFragment extends Fragment {

    private FragmentTransactionsBinding fragmentTransactionsBinding;
    private MyBalanceResultItem myBalanceResultItem;
    TransactionItemAdapter mAdapter;
    ArrayList<TransactionItem> transactionItems = new ArrayList<>();

    @Inject
    OAuthRestService oAuthRestService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentTransactionsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_transactions, container, false);
        setupRecyclerView();
        return fragmentTransactionsBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        MyApplication.getAppComponent().inject(this);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // MyApplication.getAppComponent().inject(this);
    }



    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void setupRecyclerView() {
        getTransaction();
        mAdapter = new TransactionItemAdapter(transactionItems,getActivity());
        fragmentTransactionsBinding.rvTransaction.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        fragmentTransactionsBinding.rvTransaction.setLayoutManager(mLayoutManager);
        // adding custom divider line with padding 3dp
        fragmentTransactionsBinding.rvTransaction.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, 1));
        fragmentTransactionsBinding.rvTransaction.setItemAnimator(new DefaultItemAnimator());
        fragmentTransactionsBinding.rvTransaction.setAdapter(mAdapter);
    }


    private void getTransaction() {
        fragmentTransactionsBinding.setRefreshing(true);
        TransactionRequest baseRequest = new TransactionRequest();
        baseRequest.setPage(1);
        baseRequest.setUserId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<MyTransactionHistoryResponse> myBalanceResponseCustomCall = oAuthRestService.getMyTransaction(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<MyTransactionHistoryResponse>() {
            @Override
            public void success(Response<MyTransactionHistoryResponse> response) {
                fragmentTransactionsBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    MyTransactionHistoryResponse playingHistoryResponse= response.body();
                    if (playingHistoryResponse.getStatus() == 1 && playingHistoryResponse.getTransactionHistoryItem()!=null) {
                        if(playingHistoryResponse.getTransactionHistoryItem().getData().size()>0) {
                            transactionItems = playingHistoryResponse.getTransactionHistoryItem().getData();
                            fragmentTransactionsBinding.tvNoTransaction.setVisibility(View.GONE);
                            mAdapter.updateData(transactionItems);
                        }
                        else {
                            fragmentTransactionsBinding.tvNoTransaction.setVisibility(View.VISIBLE);
                        }
                    } else {
                        fragmentTransactionsBinding.tvNoTransaction.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentTransactionsBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


}