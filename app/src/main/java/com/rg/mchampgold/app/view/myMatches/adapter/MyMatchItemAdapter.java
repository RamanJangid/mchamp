package com.rg.mchampgold.app.view.myMatches.adapter;


import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.Match;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.JoinedContestActivity;
import com.rg.mchampgold.app.view.interfaces.OnMatchItemClickListener;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.RecyclerMyItemMatchBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MyMatchItemAdapter extends RecyclerView.Adapter<MyMatchItemAdapter.ViewHolder> {
    private boolean isFromFinished;
    private List<Match> moreInfoDataList;
    private OnMatchItemClickListener listener;
    private AppCompatActivity activity;
    private RecyclerView recyclerView;

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerMyItemMatchBinding binding;
        CountDownTimer countDownTimer;

        ViewHolder(RecyclerMyItemMatchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public MyMatchItemAdapter(Activity activity, List<Match> moreInfoDataList, OnMatchItemClickListener listener, RecyclerView recyclerView, boolean isFromFinished) {
        this.moreInfoDataList = moreInfoDataList;
        this.listener = listener;
        this.recyclerView = recyclerView;
        this.activity = (AppCompatActivity) activity;
        this.isFromFinished = isFromFinished;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerMyItemMatchBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_my_item_match,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        //if (isFromFinished) holder.binding.tvAmount.setVisibility(View.VISIBLE);

        holder.binding.tvTimer.setText("");
        Match match = moreInfoDataList.get(position);
        holder.binding.setMoreInfo(match);
        AppUtils.loadImageMatch(holder.binding.ivTeamFirst, match.getTeam1logo());
        AppUtils.loadImageMatch(holder.binding.ivTeamSecond, match.getTeam2logo());
        holder.binding.tvMatchInfo.setText(match.getMatchStatus());
        if(match.getWinnerstatus().equalsIgnoreCase("IsAbandoned"))
        {
            holder.binding.tvMatchInfo.setTextColor(activity.getResources().getColor(R.color.color_red));
        }
        else
        {
            holder.binding.tvMatchInfo.setTextColor(activity.getResources().getColor(R.color.color_green));
        }
        //holder.binding.llLinup.setBackgroundResource(R.drawable.lineup_mymatch);
     /*   if(moreInfoDataList.get(position).getLineup() ==1) {
            holder.binding.tvLinupOut.setVisibility(View.VISIBLE);
            //  holder.binding.llLinup.setBackgroundResource(R.drawable.linup_res);
            //   timeLeft.setTextColor(context.getResources().getColor(R.color.color_green));
            //    tvLinup.setVisibility(View.VISIBLE);

        }
        else {
            holder.binding.tvLinupOut.setVisibility(View.GONE);
            // holder.binding.llLinup.setBackgroundResource(R.drawable.linup_res_white);
            //  timeLeft.setTextColor(context.getResources().getColor(R.color.color_red));
            //  tvLinup.setVisibility(View.GONE);
        }*/


        if (moreInfoDataList.get(position).getIsLeaderboard() == 1) {
            holder.binding.ivLeaderboard.setVisibility(View.VISIBLE);
        } else {
            holder.binding.ivLeaderboard.setVisibility(View.GONE);
        }

        /*if (match.getLineup() == 1) {
            holder.binding.tvLinupOut.setVisibility(View.VISIBLE);
        } else {
            holder.binding.tvLinupOut.setVisibility(View.INVISIBLE);
        }*/


        if (moreInfoDataList.get(position).getMatchStatusKey() == Constants.KEY_LIVE_MATCH) {
            //holder.binding.tvMatchInfo.setText("In Progress");
        } else if (moreInfoDataList.get(position).getMatchStatusKey() == Constants.KEY_FINISHED_MATCH) {
            ///holder.binding.tvMatchInfo.setText("Winner Declared");
            holder.binding.winningLayout.setVisibility(View.VISIBLE);
            holder.binding.arrow.setVisibility(View.GONE);
        } else if (moreInfoDataList.get(position).getMatchStatusKey() == Constants.KEY_UPCOMING_MATCH) {
            String eDate = "2017-09-10 12:05:00";
            if (moreInfoDataList.get(position).getLaunchStatus().equalsIgnoreCase("launched")) {

                if (match.getLineup() == 1) {
                    holder.binding.tvLinupOut.setVisibility(View.VISIBLE);
                } else {
                    holder.binding.tvLinupOut.setVisibility(View.INVISIBLE);
                }

                holder.binding.tvTimer.setVisibility(View.VISIBLE);
                holder.binding.tvMatchInfo.setVisibility(View.GONE);
                Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                final int minute = c.get(Calendar.MINUTE);
                int sec = c.get(Calendar.SECOND);
                int mYear1 = c.get(Calendar.YEAR);
                int mMonth1 = c.get(Calendar.MONTH);
                int mDay1 = c.get(Calendar.DAY_OF_MONTH);

                Date startDate = null, endDate = null;
                String sDate = "2017-09-08 10:05:00";


                sDate = mYear1 + "-" + (mMonth1 + 1) + "-" + mDay1 + " " + hour + ":" + minute + ":" + sec;
                eDate = moreInfoDataList.get(position).getTimeStart();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                try {
                    startDate = dateFormat.parse(sDate);
                    endDate = dateFormat.parse(eDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (holder.countDownTimer != null) {
                    holder.countDownTimer.cancel();
                }


                final long diffInMs = endDate.getTime() - startDate.getTime();

                try {
                    holder.countDownTimer = new CountDownTimer(diffInMs, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {

                            long time = millisUntilFinished;
                            long seconds = time / 1000 % 60;
                            long minutes = (time / (1000 * 60)) % 60;
                            long diffHours = (time / (60 * 60 * 1000));

                          /*  if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                                holder.binding.tvMatchInfo.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d " + twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                            } else {
                                holder.binding.tvMatchInfo.setText(twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                            }*/

                                            /* if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                                    holder.binding.tvTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d : " + twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                                } else {*/

                            holder.binding.tvTimer.setText(twoDigitString(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");

                            //    holder.binding.tvTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                            //  }

                        }

                        private String twoDigitString(long number) {
                            if (number == 0) {
                                return "00";
                            } else if (number / 10 == 0) {
                                return "0" + number;
                            }
                            return String.valueOf(number);
                        }

                        @Override
                        public void onFinish() {
                            if (moreInfoDataList.size() > 0) {
                                recyclerView.getRecycledViewPool().clear();
                                moreInfoDataList.remove(match);
                                notifyDataSetChanged();
                            }
                        }
                    };

                    holder.countDownTimer.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                holder.binding.tvMatchInfo.setVisibility(View.GONE);
                holder.binding.tvTimer.setVisibility(View.VISIBLE);
                holder.binding.tvTimer.setText(match.getStartDate());
            }
        }

        holder.itemView.setOnClickListener(v -> {
            if (moreInfoDataList.get(position).getMatchStatusKey() == Constants.KEY_LIVE_MATCH) {
                listener.onMatchItemClick(moreInfoDataList.get(position).getMatchkey(),
                        moreInfoDataList.get(position).getTeam1display() + " Vs " + moreInfoDataList.get(position).getTeam2display(),
                        moreInfoDataList.get(position).getTeam1logo(),
                        moreInfoDataList.get(position).getTeam2logo(), moreInfoDataList.get(position).getMatchStatus(), position, match.getIsLeaderboard(), match.getSeries());

            } else if (moreInfoDataList.get(position).getMatchStatusKey() == Constants.KEY_FINISHED_MATCH) {
                listener.onMatchItemClick(
                        moreInfoDataList.get(position).getMatchkey(),
                        moreInfoDataList.get(position).getTeam1display() + " Vs " + moreInfoDataList.get(position).getTeam2display(),
                        moreInfoDataList.get(position).getTeam1logo(),
                        moreInfoDataList.get(position).getTeam2logo(),
                        moreInfoDataList.get(position).getMatchStatus(), position, match.getIsLeaderboard(), match.getSeries());
            } else if (moreInfoDataList.get(position).getMatchStatusKey() == Constants.KEY_UPCOMING_MATCH) {
                Intent intent = new Intent(activity, JoinedContestActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, moreInfoDataList.get(position).getMatchkey());
                intent.putExtra(Constants.KEY_TEAM_VS, moreInfoDataList.get(position).getTeam1display() + " Vs " + moreInfoDataList.get(position).getTeam2display());
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, moreInfoDataList.get(position).getTeam1logo());
                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, moreInfoDataList.get(position).getTeam2logo());
                intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, moreInfoDataList.get(position).getTimeStart());
                intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
                intent.putExtra(Constants.SPORT_KEY, moreInfoDataList.get(position).getSport_key());
                intent.putExtra(Constants.KEY_SERIES_ID, moreInfoDataList.get(position).getSeries());
                intent.putExtra(Constants.KEY_IS_LEADERBOARD, moreInfoDataList.get(position).getIsLeaderboard());
                activity.startActivity(intent);

/*                Intent intent = new Intent(activity, UpComingContestActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, match.getMatchkey());
                intent.putExtra(Constants.KEY_TEAM_VS, match.getTeam1display() + " Vs " + match.getTeam2display());
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, match.getTeam1logo());
                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, match.getTeam2logo());
                intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, match.getTimeStart());
                intent.putExtra(Constants.SPORT_KEY, match.getSport_key());
                intent.putExtra(Constants.KEY_SERIES_ID, match.getSeries());
                intent.putExtra(Constants.KEY_IS_LEADERBOARD, match.getIsLeaderboard());
                activity.startActivity(intent);*/
            }
        });
        holder.binding.executePendingBindings();


    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }


    public void updateData(ArrayList<Match> list) {
        moreInfoDataList = list;
        // searchItemFilteredList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}