package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.GetNotificationResponse;
import com.rg.mchampgold.app.dataModel.NotificationItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.NotificationItemAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityNotificationBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {

    ActivityNotificationBinding mBinding;
    NotificationItemAdapter mAdapter;

    @Inject
    OAuthRestService oAuthRestService;
    ArrayList<NotificationItem> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_notification);
        MyApplication.getAppComponent().inject(NotificationActivity.this);
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.your_notifications));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        getNotification();
        mAdapter = new NotificationItemAdapter(list);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(NotificationActivity.this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void getNotification() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<GetNotificationResponse> userFullDetailsResponseCustomCall = oAuthRestService.getUserNotification(baseRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<GetNotificationResponse>() {
            @Override
            public void success(Response<GetNotificationResponse> response) {
                mBinding.setRefreshing(false);
                GetNotificationResponse getNotificationResponse = response.body();
                if(getNotificationResponse.getStatus() ==1) {
                    if(getNotificationResponse.getResult().size()>0)
                        mAdapter.update(getNotificationResponse.getResult());
                    else {
                        Toast.makeText(NotificationActivity.this,"No Notification you got yet",Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(NotificationActivity.this,getNotificationResponse.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }

}