package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class JoinItem {

	@SerializedName("status")
	private boolean status;

	@SerializedName("join_message")
	private String message;

	@SerializedName("refercode")
	private String referCode;

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	public String getReferCode() {
		return referCode;
	}

	public void setReferCode(String referCode) {
		this.referCode = referCode;
	}


	@Override
 	public String toString(){
		return 
			"JoinItem{" +
			"status = '" + status + '\'' + 
			"}";
		}

	public String getMessage() {
		return message == null?"":message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}