package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PanVerificationRequest{

	@SerializedName("image")
	private String image;

	@SerializedName("pan_number")
	private String panNumber;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("state")
	private String state;

	@SerializedName("pan_name")
	private String panName;

	@SerializedName("pan_dob")
	private String panDob;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setPanNumber(String panNumber){
		this.panNumber = panNumber;
	}

	public String getPanNumber(){
		return panNumber;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setPanName(String panName){
		this.panName = panName;
	}

	public String getPanName(){
		return panName;
	}

	public void setPanDob(String panDob){
		this.panDob = panDob;
	}

	public String getPanDob(){
		return panDob;
	}

	@Override
 	public String toString(){
		return 
			"PanVerificationRequest{" + 
			"image = '" + image + '\'' + 
			",pan_number = '" + panNumber + '\'' + 
			",user_id = '" + userId + '\'' + 
			",state = '" + state + '\'' + 
			",pan_name = '" + panName + '\'' + 
			",pan_dob = '" + panDob + '\'' + 
			"}";
		}
}