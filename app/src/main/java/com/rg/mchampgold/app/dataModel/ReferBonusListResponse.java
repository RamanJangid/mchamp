package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ReferBonusListResponse{

	@SerializedName("result")
	private ArrayList<ReferLIstItem> result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;


	@SerializedName("total_user")
	private String totalUser = "0";

	@SerializedName("total_amount")
	private String totalAmount = "0";



	public void setResult(ArrayList<ReferLIstItem> result){
		this.result = result;
	}

	public ArrayList<ReferLIstItem> getResult(){
		return result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ReferBonusListResponse{" + 
			"result = '" + result + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}


	public String getTotalUser() {
		return totalUser == null?"":totalUser;
	}

	public void setTotalUser(String totalUser) {
		this.totalUser = totalUser;
	}

	public String getTotalAmount() {
		return totalAmount == null?"":totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
}