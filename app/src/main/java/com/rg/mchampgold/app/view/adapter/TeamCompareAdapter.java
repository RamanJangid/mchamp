package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.CommonPlayerListModel;
import com.rg.mchampgold.app.dataModel.OtherPlayerListModel;
import com.rg.mchampgold.app.dataModel.TeamCompareCVCModel;
import com.rg.mchampgold.app.view.activity.TeamCompareActivity;
import com.rg.mchampgold.databinding.ComparePlayersItemLayoutBinding;

import java.util.ArrayList;
import java.util.List;

public class TeamCompareAdapter extends RecyclerView.Adapter<TeamCompareAdapter.ViewHolder> {

    private TeamCompareCVCModel cvc_list;
    private List<CommonPlayerListModel> common_list;
    private List<ArrayList<OtherPlayerListModel>> other_list;
    private Context context;
    private boolean isForJoinContest;
    int teamId = 0;
    String sportKey="";
    String team_id1,team_id2;
    TeamCompareActivity activity;

    class ViewHolder extends RecyclerView.ViewHolder {
        final ComparePlayersItemLayoutBinding binding;

        ViewHolder(ComparePlayersItemLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public TeamCompareAdapter(TeamCompareCVCModel cvc_list, List<CommonPlayerListModel> common_list, List<ArrayList<OtherPlayerListModel>> other_list, Context context, String team_id1, String team_id2, TeamCompareActivity activity, String sportKey) {
      this.cvc_list=cvc_list;
      this.common_list=common_list;
      this.other_list=other_list;
      this.context=context;
      this.team_id1=team_id1;
      this.team_id2=team_id2;
      this.activity=activity;
      this.sportKey = sportKey;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ComparePlayersItemLayoutBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.compare_players_item_layout,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TeamCompareSubAdapter adapter=null;
        if (position==0){
            holder.binding.diffText.setText(activity.model.getCvc_diff_text());
            holder.binding.diffPoints.setText(activity.model.getCvc_diff_points());
            adapter = new TeamCompareSubAdapter(cvc_list,null,null,context,position,team_id1,team_id2, sportKey);
        }else if (position==1){
            holder.binding.diffText.setText(activity.model.getOthers_diff_text());
            holder.binding.diffPoints.setText(activity.model.getOthers_diff_points());
            adapter = new TeamCompareSubAdapter(null,other_list,null,context,position,team_id1,team_id2, sportKey);
        }else {
            holder.binding.diffText.setText(activity.model.getCommon_diff_text());
            holder.binding.diffPoints.setText(activity.model.getCommon_diff_points());
            adapter = new TeamCompareSubAdapter(null,null,common_list,context,position,team_id1,team_id2, sportKey);
        }
        holder.binding.subPlayersRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        holder.binding.subPlayersRecycler.setLayoutManager(mLayoutManager);
        holder.binding.subPlayersRecycler.setAdapter(adapter);
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return 3;
    }


}