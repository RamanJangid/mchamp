package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.AllContestActivity;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.databinding.RecyclerItemContestBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ContestItemAdapter extends RecyclerView.Adapter<ContestItemAdapter.ViewHolder> {

    private List<Contest> moreInfoDataList;
    private OnContestItemClickListener listener;
    Context context;
    private boolean isForAllContest;


    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemContestBinding binding;

        ViewHolder(RecyclerItemContestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public ContestItemAdapter(Context context, List<Contest> moreInfoDataList, OnContestItemClickListener listener, boolean isForAllContest) {
        this.context = context;
        this.moreInfoDataList = moreInfoDataList;
        this.listener = listener;
        this.isForAllContest = isForAllContest;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemContestBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_contest,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setContestData(moreInfoDataList.get(position));
        holder.binding.tagMText.setText(String.valueOf(moreInfoDataList.get(position).getMaxTeamLimit()));
        holder.itemView.setOnClickListener(v -> listener.onContestClick(moreInfoDataList.get(position), true));


        if (moreInfoDataList.get(position).getIs_free() == 1) {
            holder.binding.tvIsFree.setPaintFlags( holder.binding.tvIsFree.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.binding.tvIsFree.setVisibility(View.VISIBLE);
            holder.binding.tvIsFree.setText("₹"+moreInfoDataList.get(position).getDis_price());
            holder.binding.btnJoin.setText("FREE");
        } else {
            holder.binding.tvIsFree.setVisibility(View.GONE);
            holder.binding.btnJoin.setText(moreInfoDataList.get(position).showJoinAmount());
        }

        holder.binding.tvTotalWinners.setText(moreInfoDataList.get(position).getTotalwinners() + " Team Win");
        holder.binding.tvFirstPrize.setText(context.getString(R.string.rupee) + moreInfoDataList.get(position).getFirst_rank_prize());

        if (moreInfoDataList.get(position).getChallenge_type().equals("percentage")) {
            holder.binding.txtStartValue.setText(moreInfoDataList.get(position).getJoinedusers() + " teams already entered");
            holder.binding.txtEndValue.setText("");
            holder.binding.progressBar.setMax(16);
            holder.binding.progressBar.setProgress(8);

            if (moreInfoDataList.get(position).getWinning_percentage() != null) {
                if (!moreInfoDataList.get(position).getWinning_percentage().equals("") && !moreInfoDataList.get(position).getWinning_percentage().equals("0")) {
                    holder.binding.tvTotalWinners.setText(moreInfoDataList.get(position).getWinning_percentage() + "% Win");
                }
            }
        } else {
            holder.binding.progressBar.setMax(moreInfoDataList.get(position).getMaximumUser());
            holder.binding.progressBar.setProgress(moreInfoDataList.get(position).getJoinedusers());

            int left = (moreInfoDataList.get(position).getMaximumUser()) - (moreInfoDataList.get(position).getJoinedusers());
            if (left != 0)
                holder.binding.txtStartValue.setText(left + " Spots left");
            else
                holder.binding.txtStartValue.setText("Contest Closed");

            holder.binding.txtEndValue.setText(moreInfoDataList.get(position).getMaximumUser() + " Spots");
            holder.binding.tvTotalWinners.setText(moreInfoDataList.get(position).getTotalwinners() + " Team Win");
        }


        holder.binding.llTotalWinnersContest.setOnClickListener(view -> {
            if (moreInfoDataList.get(position).getTotalwinners() > 0) {
                if (context instanceof UpComingContestActivity)
                    ((UpComingContestActivity) context).getWinnerPriceCard(moreInfoDataList.get(position).getId(), moreInfoDataList.get(position).getWinAmount() + "");

                if (context instanceof AllContestActivity)
                    ((AllContestActivity) context).getWinnerPriceCard(moreInfoDataList.get(position).getId(), moreInfoDataList.get(position).getWinAmount() + "");
            }
        });

        holder.binding.btnJoin.setOnClickListener(view -> {
            if (holder.binding.btnJoin.getText().toString().equalsIgnoreCase("Invite")) {
                if (context instanceof UpComingContestActivity)
                    ((UpComingContestActivity) context).openShareIntent();
            } else {
                listener.onContestClick(moreInfoDataList.get(position), false);
            }
        });

        holder.binding.tagC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });

        holder.binding.tagM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });

        holder.binding.tagWd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });

        holder.binding.tagB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        if (isForAllContest) {
            return moreInfoDataList.size();
        } else {
            if (moreInfoDataList.size() > 3)
                return 3;
            else
                return moreInfoDataList.size();
        }
    }


    public void updateData(ArrayList<Contest> list) {
        moreInfoDataList = list;
        notifyDataSetChanged();
    }

    public void sortWithPrizePool(boolean flag) {

        if (flag) {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(contest.getWinAmount()).compareTo(t1.getWinAmount()));
        } else {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(t1.getWinAmount()).compareTo(contest.getWinAmount()));
        }
        notifyDataSetChanged();
    }

    public void sortWithSpot(boolean flag) {
        if (flag) {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(contest.getLeftUser()).compareTo(t1.getLeftUser()));
        } else {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(t1.getLeftUser()).compareTo(contest.getLeftUser()));
        }
        notifyDataSetChanged();
    }

    public void sortWithWinners(boolean flag) {

        if (flag) {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(contest.getTotalwinners()).compareTo(t1.getTotalwinners()));
        } else {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(t1.getTotalwinners()).compareTo(contest.getTotalwinners()));
        }
        notifyDataSetChanged();
    }

    public void sortWithEntry(boolean flag) {
        if (flag) {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(contest.getEntryfee()).compareTo(Integer.valueOf(t1.getEntryfee())));
        } else {
            Collections.sort(moreInfoDataList, (contest, t1) -> Integer.valueOf(t1.getEntryfee()).compareTo(Integer.valueOf(contest.getEntryfee())));
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }
}