package com.rg.mchampgold.app.view.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.JoinedContestTeam;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.TeamCompareActivity;
import com.rg.mchampgold.app.view.activity.UpComingContestDetailActivity;
import com.rg.mchampgold.app.view.adapter.ContestJoinTeamItemAdapter;
import com.rg.mchampgold.app.viewModel.ContestDetailsViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentLeaderboardBinding;

import java.util.ArrayList;

import javax.inject.Inject;


public class LeaderBoardFragment extends Fragment {

    private FragmentLeaderboardBinding fragmentLeaderBoardBinding;
    ArrayList<JoinedContestTeam> list = new ArrayList<>();
    ContestJoinTeamItemAdapter mAdapter;
    ContestDetailsViewModel contestDetailsViewModel;

    @Inject
    OAuthRestService oAuthRestService;

    private String contestId;
    private String matchKey;
    private String pdfUrl = "";
    public String team1Id = "0", team2Id = "0";
    public boolean isCompareTeamActive = false;
    Context context;

    boolean isForContestDetails;

    boolean isShowTimer;
    boolean isFromLive = false;
    String sportKey = "";
    private static final String TAG = "LeaderBoard=> ";

    public static LeaderBoardFragment newInstance(boolean isFromLive, String matchKey, String contestId, boolean isShowTimer, boolean isForContestDetails, String pdfUrl, String sportKey) {
        LeaderBoardFragment myFragment = new LeaderBoardFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KEY_MATCH_KEY, matchKey);
        args.putString(Constants.CONTEST_ID, contestId);
        args.putBoolean("isShowTimer", isShowTimer);
        args.putBoolean("isForContestDetails", isForContestDetails);
        args.putString("pdfUrl", pdfUrl);
        args.putString("sportKey", sportKey);
        args.putBoolean("isFromLive", isFromLive);
        myFragment.setArguments(args);
        return myFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLeaderBoardBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_leaderboard, container, false);
        fragmentLeaderBoardBinding.compareClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (isCompareTeamActive) {
                    isCompareTeamActive = false;
                    fragmentLeaderBoardBinding.compareClick.setAlpha(1.0f);
                } else {
                    isCompareTeamActive = true;
                    fragmentLeaderBoardBinding.compareClick.setAlpha(0.5f);
                    showCompareTeamPopUp();
                }
                mAdapter.notifyDataSetChanged();*/
            }
        });
        return fragmentLeaderBoardBinding.getRoot();
    }

    private void setupRecyclerView() {
        mAdapter = new ContestJoinTeamItemAdapter(isFromLive,isForContestDetails, context, list, sportKey, this);
        fragmentLeaderBoardBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        fragmentLeaderBoardBinding.recyclerView.setLayoutManager(mLayoutManager);
        fragmentLeaderBoardBinding.recyclerView.setAdapter(mAdapter);


        if (!isShowTimer) {
            //mBinding.tvRank.setVisibility(View.VISIBLE);
            //mBinding.tvPoints.setVisibility(View.VISIBLE);
            fragmentLeaderBoardBinding.downloadTeam.setVisibility(View.VISIBLE);
            fragmentLeaderBoardBinding.topLayout.setVisibility(View.VISIBLE);
        } else {
            //mBinding.tvRank.setVisibility(View.GONE);
            // mBinding.tvPoints.setVisibility(View.GONE);
            fragmentLeaderBoardBinding.downloadTeam.setVisibility(View.GONE);
            fragmentLeaderBoardBinding.topLayout.setVisibility(View.GONE);
        }


        fragmentLeaderBoardBinding.downloadTeam.setOnClickListener(view -> {

            if (!pdfUrl.equals(""))
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pdfUrl)));
            else {
                AppUtils.showErrorr((AppCompatActivity) getActivity(), "PDF Not Ready Yet");
            }
        });
        getData(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contestDetailsViewModel = ContestDetailsViewModel.create(this);
        MyApplication.getAppComponent().inject(contestDetailsViewModel);
        MyApplication.getAppComponent().inject(this);
        context = (UpComingContestDetailActivity) getActivity();
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            contestId = getArguments().getString(Constants.CONTEST_ID);
            matchKey = getArguments().getString(Constants.KEY_MATCH_KEY);
            isShowTimer = getArguments().getBoolean("isShowTimer");
            isForContestDetails = getArguments().getBoolean("isForContestDetails");
            pdfUrl = getArguments().getString("pdfUrl");
            sportKey = getArguments().getString("sportKey");
            isFromLive = getArguments().getBoolean("isFromLive");
        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        setupRecyclerView();
    }


    private void getData(boolean showLeaderBoard) {
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setShowLeaderBoard(showLeaderBoard);
        request.setLeagueId(contestId);
        request.setSport_key(sportKey);
        request.setPage("1");
        contestDetailsViewModel.loadContestRequest(request);
        contestDetailsViewModel.getContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    fragmentLeaderBoardBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    fragmentLeaderBoardBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    fragmentLeaderBoardBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getResult().getValue().size() > 0) {
                        list = arrayListResource.getData().getResult().getValue();
                        mAdapter.updateData(list);
                        if (list.size() > 0)
                            fragmentLeaderBoardBinding.downloadTeam.setText("Download (" + list.size() + ")");

                        /*if (isFromLive && list.size() > 1) {
                            fragmentLeaderBoardBinding.compareClick.setVisibility(View.VISIBLE);
                        } else {
                            fragmentLeaderBoardBinding.compareClick.setVisibility(View.GONE);
                        }*/

                        //setTeamContestCount();
                    } else {
                        // Toast.makeText(MyApplication.appContext,arrayListResource.getData().getMessage(),Toast.LENGTH_SHORT).show();
                        // setTeamContestCount();
                    }
                    break;
                }
            }

        });
    }

    public void openCompareTeamActivity(int challengeId) {
        Intent intent = new Intent(getActivity(), TeamCompareActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.SPORT_KEY, sportKey);
        intent.putExtra(Constants.KEY_TEAM_ID, team1Id);
        intent.putExtra(Constants.KEY_TEAM_ID2, team2Id);
        intent.putExtra("challengeId", challengeId);
        startActivity(intent);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void showCompareTeamPopUp() {

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.custom_pop_up_layout, null, false);

        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);

        popupWindow.showAsDropDown(fragmentLeaderBoardBinding.topLayout,
                fragmentLeaderBoardBinding.topLayout.getWidth()/5,
                0,
                Gravity.CENTER);

        // dismiss the popup window when touched
        popupView.setOnTouchListener((v, event) -> {
            popupWindow.dismiss();
            return true;
        });
    }
}