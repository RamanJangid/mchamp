package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class TeamFindItem {

	@SerializedName("teamid")
	private int teamid;

	@SerializedName("teamnumber")
	private int teamnumber;

	public void setTeamid(int teamid){
		this.teamid = teamid;
	}

	public int getTeamid(){
		return teamid;
	}

	public void setTeamnumber(int teamnumber){
		this.teamnumber = teamnumber;
	}

	public int getTeamnumber(){
		return teamnumber;
	}

	@Override
 	public String toString(){
		return 
			"TeamFindItem{" +
			"teamid = '" + teamid + '\'' + 
			",teamnumber = '" + teamnumber + '\'' + 
			"}";
		}
}