package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class UserDetailValue {

	@SerializedName("DayOfBirth")
	private String dayOfBirth;

	@SerializedName("country")
	private String country;

	@SerializedName("totalchallenges")
	private int totalchallenges;

	@SerializedName("gender")
	private String gender;

	@SerializedName("city")
	private String city;

	@SerializedName("totalwon")
	private Double totalwon;

	@SerializedName("dobfreeze")
	private int dobfreeze;

	@SerializedName("teamfreeze")
	private int teamfreeze;

	@SerializedName("provider")
	private String provider;

	@SerializedName("YearOfBirth")
	private String yearOfBirth;

	@SerializedName("id")
	private int id;

	@SerializedName("state")
	private String state;

	@SerializedName("email")
	private String email;

	@SerializedName("image")
	private String image;

	@SerializedName("pincode")
	private String pincode;

	@SerializedName("address")
	private String address;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("verified")
	private int verified;

	@SerializedName("team")
	private String team;

	@SerializedName("mobilefreeze")
	private int mobilefreeze;

	@SerializedName("emailfreeze")
	private int emailfreeze;

	@SerializedName("activation_status")
	private String activationStatus;

	@SerializedName("dob")
	private String dob;

	@SerializedName("walletamaount")
	private int walletamaount;

	@SerializedName("refercode")
	private String refercode;

	@SerializedName("MonthOfBirth")
	private String monthOfBirth;

	@SerializedName("statefreeze")
	private int statefreeze;

	@SerializedName("username")
	private String username;

	public int getEmailfreeze() {
		return emailfreeze;
	}

	public void setEmailfreeze(int emailfreeze) {
		this.emailfreeze = emailfreeze;
	}

	public void setDayOfBirth(String dayOfBirth){
		this.dayOfBirth = dayOfBirth;
	}

	public String getDayOfBirth(){
		return dayOfBirth;
	}

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setTotalchallenges(int totalchallenges){
		this.totalchallenges = totalchallenges;
	}

	public int getTotalchallenges(){
		return totalchallenges;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender == null ? "" : gender;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public Double getTotalwon() {
		return totalwon;
	}

	public void setTotalwon(Double totalwon) {
		this.totalwon = totalwon;
	}

	public void setDobfreeze(int dobfreeze){
		this.dobfreeze = dobfreeze;
	}

	public int getDobfreeze(){
		return dobfreeze;
	}

	public void setTeamfreeze(int teamfreeze){
		this.teamfreeze = teamfreeze;
	}

	public int getTeamfreeze(){
		return teamfreeze;
	}

	public void setProvider(String provider){
		this.provider = provider;
	}

	public String getProvider(){
		return provider;
	}

	public void setYearOfBirth(String yearOfBirth){
		this.yearOfBirth = yearOfBirth;
	}

	public String getYearOfBirth(){
		return yearOfBirth;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setPincode(String pincode){
		this.pincode = pincode;
	}

	public String getPincode(){
		return pincode;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setVerified(int verified){
		this.verified = verified;
	}

	public int getVerified(){
		return verified;
	}

	public void setTeam(String team){
		this.team = team;
	}

	public String getTeam(){
		return team;
	}

	public void setMobilefreeze(int mobilefreeze){
		this.mobilefreeze = mobilefreeze;
	}

	public int getMobilefreeze(){
		return mobilefreeze;
	}

	public void setActivationStatus(String activationStatus){
		this.activationStatus = activationStatus;
	}

	public String getActivationStatus(){
		return activationStatus;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setWalletamaount(int walletamaount){
		this.walletamaount = walletamaount;
	}

	public int getWalletamaount(){
		return walletamaount;
	}

	public void setRefercode(String refercode){
		this.refercode = refercode;
	}

	public String getRefercode(){
		return refercode;
	}

	public void setMonthOfBirth(String monthOfBirth){
		this.monthOfBirth = monthOfBirth;
	}

	public String getMonthOfBirth(){
		return monthOfBirth;
	}

	public void setStatefreeze(int statefreeze){
		this.statefreeze = statefreeze;
	}

	public int getStatefreeze(){
		return statefreeze;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"UserDetailValue{" +
			"dayOfBirth = '" + dayOfBirth + '\'' + 
			",country = '" + country + '\'' + 
			",totalchallenges = '" + totalchallenges + '\'' + 
			",gender = '" + gender + '\'' + 
			",city = '" + city + '\'' + 
			",totalwon = '" + totalwon + '\'' + 
			",dobfreeze = '" + dobfreeze + '\'' + 
			",teamfreeze = '" + teamfreeze + '\'' + 
			",provider = '" + provider + '\'' + 
			",yearOfBirth = '" + yearOfBirth + '\'' + 
			",id = '" + id + '\'' + 
			",state = '" + state + '\'' + 
			",email = '" + email + '\'' + 
			",image = '" + image + '\'' + 
			",pincode = '" + pincode + '\'' + 
			",address = '" + address + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",verified = '" + verified + '\'' + 
			",team = '" + team + '\'' + 
			",mobilefreeze = '" + mobilefreeze + '\'' + 
			",activation_status = '" + activationStatus + '\'' + 
			",dob = '" + dob + '\'' + 
			",walletamaount = '" + walletamaount + '\'' + 
			",refercode = '" + refercode + '\'' + 
			",monthOfBirth = '" + monthOfBirth + '\'' + 
			",statefreeze = '" + statefreeze + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}