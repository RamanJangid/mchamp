package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.dataModel.MultiSportsPlayerPointItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.PlayerPointsItemAdapter;
import com.rg.mchampgold.app.viewModel.ContestViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityPlayerPointsBinding;

import java.util.ArrayList;

public class PlayerPointsActivity extends AppCompatActivity {
    ActivityPlayerPointsBinding mBinding;
    PlayerPointsItemAdapter mAdapter;
    private ContestViewModel contestViewModel;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String headerText;
    String teamSecondUrl;
    boolean isSelectedBySort=false;
    boolean isPointsSort=false;
	String sportKey="";
    ArrayList<MultiSportsPlayerPointItem> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        contestViewModel = ContestViewModel.create(PlayerPointsActivity.this);
        MyApplication.getAppComponent().inject(contestViewModel);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_player_points);
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.player_points));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
			if (getIntent().hasExtra(Constants.KEY_STATUS_HEADER_TEXT))
                headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
        }

        String teams[] = teamVsName.split(" ");
        mBinding.matchinfo.tvTeam1.setText(teams[0]);
        mBinding.matchinfo.tvTeam2.setText(teams[2]);

        AppUtils.loadImageMatch(mBinding.matchinfo.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.matchinfo.ivTeam2, teamSecondUrl);


        if (headerText.equalsIgnoreCase("Winner Declared")) {
            mBinding.matchinfo.tvMatchTimer.setText("Winner Declared");
        } else if (headerText.equalsIgnoreCase("In Progress")) {
            mBinding.matchinfo.tvMatchTimer.setText("In Progress");
        }
        else
        {
            mBinding.matchinfo.tvMatchTimer.setText(headerText);
        }
        mBinding.selectedBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isSelectedBySort) {
                    mBinding.selectedBy.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_down_sort),null);
                    isSelectedBySort=true;
                    mAdapter.sortWithSelectedBy(true);
                }else {
                    mBinding.selectedBy.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_up_sort),null);
                    isSelectedBySort=false;
                    mAdapter.sortWithSelectedBy(false);
                }
            }
        });
        mBinding.points.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isPointsSort){
                    mBinding.points.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_down_sort),null);
                    isPointsSort=true;
                    mAdapter.sortWithPoints(true);
                }else {
                    mBinding.points.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_up_sort),null);
                    isPointsSort=false;
                    mAdapter.sortWithPoints(false);
                }
            }
        });

        setupRecyclerView();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        mAdapter = new PlayerPointsItemAdapter(PlayerPointsActivity.this, list);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
        getData();
    }


    private void getData() {
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        contestViewModel.loadPlayerPointRequest(request);
        contestViewModel.getPlayerPoints().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1) {
                        list = arrayListResource.getData().getResult();
                        mAdapter.updateData(list);
                        mBinding.points.setCompoundDrawablesWithIntrinsicBounds(null,null,getResources().getDrawable(R.drawable.ic_up_sort),null);
                        isPointsSort=false;
                        mAdapter.sortWithPoints(false);
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }


}
