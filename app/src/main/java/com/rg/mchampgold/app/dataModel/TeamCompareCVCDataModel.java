package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;
import com.rg.mchampgold.app.utils.AppUtils;

public class TeamCompareCVCDataModel {

    @SerializedName("team")
    String team;
    @SerializedName("playerrole")
    String playerrole;
    @SerializedName("playercredit")
    String playercredit;
    @SerializedName("role")
    String role;
    @SerializedName("playerpoints")
    String playerpoints;
    @SerializedName("playername")
    String playername;
    @SerializedName("pid")
    String pid;
    @SerializedName("image")
    String image;
    @SerializedName("team_id")
    String team_id;

    public String getTeam() {
        return team==null?"":team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getPlayerrole() {
        return playerrole==null?"":playerrole;
    }

    public void setPlayerrole(String playerrole) {
        this.playerrole = playerrole;
    }

    public String getPlayercredit() {
        return playercredit==null?"":playercredit;
    }

    public void setPlayercredit(String playercredit) {
        this.playercredit = playercredit;
    }

    public String getRole() {
        return role==null?"":role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPlayerpoints() {
        return playerpoints==null?"":playerpoints.split("\\.")[0];
    }

    public void setPlayerpoints(String playerpoints) {
        this.playerpoints = playerpoints;
    }

    public String getPlayername() {
        return playername==null?"":AppUtils.getShortName(playername);
    }

    public void setPlayername(String playername) {
        this.playername = playername;
    }

    public String getPid() {
        return pid==null?"":pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getImage() {
        return image==null?"":image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTeam_id() {
        return team_id==null?"":team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }
}
