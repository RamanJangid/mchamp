package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("com.robohorse.robopojogenerator")
public class PlayerPointItem implements Serializable {

	@SerializedName("actual_century")
	private int actualCentury;

	@SerializedName("actual_notout")
	private String actualNotout;

	@SerializedName("not_out")
	private int notOut;

	@SerializedName("actual_point150")
	private int actualPoint150;

	@SerializedName("halcentury")
	private int halcentury;

	@SerializedName("catch_points")
	private int catchPoints;

	@SerializedName("actual_sixs")
	private String actualSixs;

	@SerializedName("strike_rate")
	private int strikeRate;

	@SerializedName("negative")
	private int negative;

	@SerializedName("total")
	private double total;

	@SerializedName("player_id")
	private int playerId;

	@SerializedName("actual_wicket")
	private String actualWicket;

	@SerializedName("actual_runouts")
	private String actualRunouts;

	@SerializedName("actual_maidens")
	private String actualMaidens;

	@SerializedName("duck")
	private String duck = "0";

	@SerializedName("is_topplayer")
	private int isTopplayer;

	@SerializedName("maidens")
	private int maidens;

	@SerializedName("isSelected")
	private int isSelected;

	@SerializedName("actual_economy_rate")
	private double actualEconomyRate;

	@SerializedName("economy_rate")
	private int economyRate;

	@SerializedName("player_name")
	private String playerName;

	@SerializedName("credit")
	private int credit;

	@SerializedName("fours")
	private double fours;

	@SerializedName("actual_halcentury")
	private int actualHalcentury;

	@SerializedName("runouts")
	private String runouts;

	@SerializedName("selected_by")
	private double selectedBy;

	@SerializedName("sixs")
	private int sixs;

	@SerializedName("point200")
	private int point200;

	@SerializedName("actual_duck")
	private String actualDuck;

	@SerializedName("actual_stumping")
	private String actualStumping;

	@SerializedName("team")
	private String team;

	@SerializedName("actual_runs")
	private String actualRuns;

	@SerializedName("actual_strike_rate")
	private double actualStrikeRate;

	@SerializedName("player_key")
	private String playerKey;

	@SerializedName("actual_winning")
	private String actualWinning;

	@SerializedName("century")
	private int century;

	@SerializedName("startingpoints")
	private int startingpoints;

	@SerializedName("actual_fours")
	private String actualFours;

	@SerializedName("actual_catch")
	private String actualCatch;

	@SerializedName("actual_startingpoints")
	private String actualStartingpoints;

	@SerializedName("point150")
	private int point150;

	@SerializedName("negative_points_actual")
	private int negativePointsActual;

	@SerializedName("actual_point200")
	private int actualPoint200;

	@SerializedName("runs")
	private double runs;

	@SerializedName("wickets")
	private int wickets;

	@SerializedName("stumping")
	private int stumping;

	@SerializedName("winner_point")
	private int winnerPoint;

	@SerializedName("image")
	private String image;

	public void setActualCentury(int actualCentury){
		this.actualCentury = actualCentury;
	}

	public int getActualCentury(){
		return actualCentury;
	}

	public void setActualNotout(String actualNotout){
		this.actualNotout = actualNotout;
	}

	public String getActualNotout(){
		return actualNotout;
	}

	public void setNotOut(int notOut){
		this.notOut = notOut;
	}

	public int getNotOut(){
		return notOut;
	}

	public void setActualPoint150(int actualPoint150){
		this.actualPoint150 = actualPoint150;
	}

	public int getActualPoint150(){
		return actualPoint150;
	}

	public void setHalcentury(int halcentury){
		this.halcentury = halcentury;
	}

	public int getHalcentury(){
		return halcentury;
	}

	public void setCatchPoints(int catchPoints){
		this.catchPoints = catchPoints;
	}

	public int getCatchPoints(){
		return catchPoints;
	}

	public void setActualSixs(String actualSixs){
		this.actualSixs = actualSixs;
	}

	public String getActualSixs(){
		return actualSixs;
	}

	public void setStrikeRate(int strikeRate){
		this.strikeRate = strikeRate;
	}

	public int getStrikeRate(){
		return strikeRate;
	}

	public void setNegative(int negative){
		this.negative = negative;
	}

	public int getNegative(){
		return negative;
	}

	public void setTotal(double total){
		this.total = total;
	}

	public double getTotal(){
		return total;
	}

	public void setPlayerId(int playerId){
		this.playerId = playerId;
	}

	public int getPlayerId(){
		return playerId;
	}

	public void setActualWicket(String actualWicket){
		this.actualWicket = actualWicket;
	}

	public String getActualWicket(){
		return actualWicket;
	}

	public void setActualRunouts(String actualRunouts){
		this.actualRunouts = actualRunouts;
	}

	public String getActualRunouts(){
		return actualRunouts;
	}

	public void setActualMaidens(String actualMaidens){
		this.actualMaidens = actualMaidens;
	}

	public String getActualMaidens(){
		return actualMaidens;
	}

	public void setDuck(String duck){
		this.duck = duck;
	}

	public String getDuck(){
		return duck;
	}

	public void setIsTopplayer(int isTopplayer){
		this.isTopplayer = isTopplayer;
	}

	public int getIsTopplayer(){
		return isTopplayer;
	}

	public void setMaidens(int maidens){
		this.maidens = maidens;
	}

	public int getMaidens(){
		return maidens;
	}

	public void setIsSelected(int isSelected){
		this.isSelected = isSelected;
	}

	public int getIsSelected(){
		return isSelected;
	}

	public void setActualEconomyRate(double actualEconomyRate){
		this.actualEconomyRate = actualEconomyRate;
	}

	public double getActualEconomyRate(){
		return actualEconomyRate;
	}

	public void setEconomyRate(int economyRate){
		this.economyRate = economyRate;
	}

	public int getEconomyRate(){
		return economyRate;
	}

	public void setPlayerName(String playerName){
		this.playerName = playerName;
	}

	public String getPlayerName(){
		return playerName;
	}

	public void setCredit(int credit){
		this.credit = credit;
	}

	public int getCredit(){
		return credit;
	}

	public void setFours(int fours){
		this.fours = fours;
	}

	public double getFours(){
		return fours;
	}

	public void setActualHalcentury(int actualHalcentury){
		this.actualHalcentury = actualHalcentury;
	}

	public int getActualHalcentury(){
		return actualHalcentury;
	}

	public void setRunouts(String runouts){
		this.runouts = runouts;
	}

	public String getRunouts(){
		return runouts;
	}

	public void setSelectedBy(double selectedBy){
		this.selectedBy = selectedBy;
	}

	public double getSelectedBy(){
		return selectedBy;
	}

	public void setSixs(int sixs){
		this.sixs = sixs;
	}

	public int getSixs(){
		return sixs;
	}

	public void setPoint200(int point200){
		this.point200 = point200;
	}

	public int getPoint200(){
		return point200;
	}

	public void setActualDuck(String actualDuck){
		this.actualDuck = actualDuck;
	}

	public String getActualDuck(){
		return actualDuck;
	}

	public void setActualStumping(String actualStumping){
		this.actualStumping = actualStumping;
	}

	public String getActualStumping(){
		return actualStumping;
	}

	public void setTeam(String team){
		this.team = team;
	}

	public String getTeam(){
		return team;
	}

	public void setActualRuns(String actualRuns){
		this.actualRuns = actualRuns;
	}

	public String getActualRuns(){
		return actualRuns;
	}

	public void setActualStrikeRate(int actualStrikeRate){
		this.actualStrikeRate = actualStrikeRate;
	}

	public double getActualStrikeRate(){
		return actualStrikeRate;
	}

	public void setPlayerKey(String playerKey){
		this.playerKey = playerKey;
	}

	public String getPlayerKey(){
		return playerKey;
	}

	public void setActualWinning(String actualWinning){
		this.actualWinning = actualWinning;
	}

	public String getActualWinning(){
		return actualWinning;
	}

	public void setCentury(int century){
		this.century = century;
	}

	public int getCentury(){
		return century;
	}

	public void setStartingpoints(int startingpoints){
		this.startingpoints = startingpoints;
	}

	public int getStartingpoints(){
		return startingpoints;
	}

	public void setActualFours(String actualFours){
		this.actualFours = actualFours;
	}

	public String getActualFours(){
		return actualFours;
	}

	public void setActualCatch(String actualCatch){
		this.actualCatch = actualCatch;
	}

	public String getActualCatch(){
		return actualCatch;
	}

	public void setActualStartingpoints(String actualStartingpoints){
		this.actualStartingpoints = actualStartingpoints;
	}

	public String getActualStartingpoints(){
		return actualStartingpoints;
	}

	public void setPoint150(int point150){
		this.point150 = point150;
	}

	public int getPoint150(){
		return point150;
	}

	public void setNegativePointsActual(int negativePointsActual){
		this.negativePointsActual = negativePointsActual;
	}

	public int getNegativePointsActual(){
		return negativePointsActual;
	}

	public void setActualPoint200(int actualPoint200){
		this.actualPoint200 = actualPoint200;
	}

	public int getActualPoint200(){
		return actualPoint200;
	}

	public void setRuns(int runs){
		this.runs = runs;
	}

	public double getRuns(){
		return runs;
	}

	public void setWickets(int wickets){
		this.wickets = wickets;
	}

	public int getWickets(){
		return wickets;
	}

	public void setStumping(int stumping){
		this.stumping = stumping;
	}

	public int getStumping(){
		return stumping;
	}

	public void setWinnerPoint(int winnerPoint){
		this.winnerPoint = winnerPoint;
	}

	public int getWinnerPoint(){
		return winnerPoint;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
 	public String toString(){
		return 
			"PlayerPointItem{" +
			"actual_century = '" + actualCentury + '\'' + 
			",actual_notout = '" + actualNotout + '\'' + 
			",not_out = '" + notOut + '\'' + 
			",actual_point150 = '" + actualPoint150 + '\'' + 
			",halcentury = '" + halcentury + '\'' + 
			",catch_points = '" + catchPoints + '\'' + 
			",actual_sixs = '" + actualSixs + '\'' + 
			",strike_rate = '" + strikeRate + '\'' + 
			",negative = '" + negative + '\'' + 
			",total = '" + total + '\'' + 
			",player_id = '" + playerId + '\'' + 
			",actual_wicket = '" + actualWicket + '\'' + 
			",actual_runouts = '" + actualRunouts + '\'' + 
			",actual_maidens = '" + actualMaidens + '\'' + 
			",duck = '" + duck + '\'' + 
			",is_topplayer = '" + isTopplayer + '\'' + 
			",maidens = '" + maidens + '\'' + 
			",isSelected = '" + isSelected + '\'' + 
			",actual_economy_rate = '" + actualEconomyRate + '\'' + 
			",economy_rate = '" + economyRate + '\'' + 
			",player_name = '" + playerName + '\'' + 
			",credit = '" + credit + '\'' + 
			",fours = '" + fours + '\'' + 
			",actual_halcentury = '" + actualHalcentury + '\'' + 
			",runouts = '" + runouts + '\'' + 
			",selected_by = '" + selectedBy + '\'' + 
			",sixs = '" + sixs + '\'' + 
			",point200 = '" + point200 + '\'' + 
			",actual_duck = '" + actualDuck + '\'' + 
			",actual_stumping = '" + actualStumping + '\'' + 
			",team = '" + team + '\'' + 
			",actual_runs = '" + actualRuns + '\'' + 
			",actual_strike_rate = '" + actualStrikeRate + '\'' + 
			",player_key = '" + playerKey + '\'' + 
			",actual_winning = '" + actualWinning + '\'' + 
			",century = '" + century + '\'' + 
			",startingpoints = '" + startingpoints + '\'' + 
			",actual_fours = '" + actualFours + '\'' + 
			",actual_catch = '" + actualCatch + '\'' + 
			",actual_startingpoints = '" + actualStartingpoints + '\'' + 
			",point150 = '" + point150 + '\'' + 
			",negative_points_actual = '" + negativePointsActual + '\'' + 
			",actual_point200 = '" + actualPoint200 + '\'' + 
			",runs = '" + runs + '\'' + 
			",wickets = '" + wickets + '\'' + 
			",stumping = '" + stumping + '\'' + 
			",winner_point = '" + winnerPoint + '\'' + 
			"}";
		}

		public String showSelectedBy() {
			return selectedBy+"%";
		}
   	public String showPoints() {
		return ""+total;
	}

}