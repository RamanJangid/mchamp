package com.rg.mchampgold.app;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;

import com.facebook.FacebookSdk;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.rg.mchampgold.BuildConfig;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.di.AppComponent;
import com.rg.mchampgold.app.di.DaggerAppComponent;
import com.rg.mchampgold.app.di.module.AppModule;
import com.rg.mchampgold.app.utils.AppSignatureHelper;
import com.rg.mchampgold.app.utils.IncomingSMSListener;
import com.rg.mchampgold.app.view.adapter.PlayerItemAdapter;
import com.rg.mchampgold.app.view.basketball.BasketballPlayerItemAdapter;
import com.rg.mchampgold.app.view.football.FootballPlayerItemAdapter;
import com.rg.mchampgold.app.view.interfaces.OnWalletClickListener;
import com.rg.mchampgold.common.di.module.NetModule;
import com.rg.mchampgold.common.utils.TinyDB;

import java.util.ArrayList;

public class MyApplication extends Application {
    public static String base_api_url = "http://fantasy.rgisports.com/repos/dev/mChamp11/";//dev url

    public static String cashfree_notify_url = "https://admin.mchampgold.com/admin/cashfree-notify";
    public static String cash_free_app_id = "748490639955d860f0fdd818a94847";
    public static String cashfree_checksum = base_api_url + "api/auth/get_cashfree_checksum";

    public static String twitter_url = "https://twitter.com/mchampgoldfantasy";
    public static String fb_url = "https://www.facebook.com/mchampgoldofficial";
    public static String instagram_url = "https://www.instagram.com/mchampgoldofficial";
    public static String telegram_url = "https://telegram.me/mchampgoldofficial";

    public static String paytm_mid = "CometE80324536003101";

    //    public static String base_api_url = "https://cric22.in/";//live url
    public static String paytm_checksum_url = base_api_url + "api/auth/get_paytm_checksum";//dev url
    public static String apk_url = "https://api.mchampgold.com/APK/mchampgold.apk";
    public static String web_pages_url = base_api_url + "app/";
    public static String terms_url = web_pages_url + "terms_and_conditions";
    public static String fantasy_point_url = web_pages_url + "fantasy_point_system";
    public static String privacy_url = web_pages_url + "privacy_policy";
    public static String about_us_url = web_pages_url + "about_us";
    public static String how_to_play_url = web_pages_url + "how_to_play";
    public static String legality_url = web_pages_url + "legalities-app";
    public static String bonus_terms = "https://mchampgold.com/app/bonus-terms-and-conditions";
    public static String invite_bonus = "50";
    public static String signup_bonus = "50";
    private static AppComponent appComponent;
    public static Boolean isFromLiveFinished = false;
    public static TinyDB tinyDB;
    public static PlayerItemAdapter playerItemAdapter;
    public static FootballPlayerItemAdapter footballPlayerItemAdapter;
    public static BasketballPlayerItemAdapter basketballplayerItemAdapter;
    public static boolean isCreated = false;
    public static Context appContext;
    public static boolean fromMyTeams = false;
    public static ArrayList<Player> teamList = new ArrayList<>();
    public static int teamId = 0;
    public static Dialog dialog;
    public static boolean teamJoined = false;
    public static int teamJoinedCount = 0;
    public static IncomingSMSListener listner;
    public static OnWalletClickListener walletClickListener;
    private FirebaseRemoteConfig firebaseRemoteConfig;
    public static boolean isPopVisible = true;
    public static String promo_code = "0";
    public static String refer_earn = "0";
    public static String refer_show = "1";
    public static String referCode = "";
    public static String isSelectedTeam = "ALL";
    @Override
    public void onCreate() {
        super.onCreate();

        setupFirebaseRemoteConfig();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        SharedPreferences spPrivate = getSharedPreferences("private", MODE_PRIVATE);
        tinyDB = new TinyDB(spPrivate);
        appContext = getApplicationContext();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).netModule(new NetModule(base_api_url)).build();
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.setAutoInitEnabled(true);
        FacebookSdk.fullyInitialize();

        // it is used to create app signatures in case of auto verify sms.
        ArrayList<String> sign = new AppSignatureHelper(getApplicationContext()).getAppSignatures();

        //  adjustFontScale(getResources().getConfiguration());

    }

    public AppComponent getComponent() {
        return appComponent;
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    private void setupFirebaseRemoteConfig() {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
    }

    public static void showLoader(Context context) {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.cancel();
            }
        }
        createLoaderView(context);
    }

    public static void hideLoader() {
        if (dialog != null) {
            dialog.cancel();
        }
    }

    public static void createLoaderView(Context context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loader_view_layout);
        dialog.setCancelable(false);
        dialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0);
        dialog.show();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //  adjustFontScale( newConfig);
    }

    public void adjustFontScale(Configuration configuration) {
        if (configuration.fontScale > 1.30) {
            configuration.fontScale = (float) 1.30;
            DisplayMetrics metrics = getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            //  metrics.scaledDensity = configuration.fontScale * metrics.density;

            getBaseContext().getResources().updateConfiguration(configuration, metrics);
        }
    }


}
