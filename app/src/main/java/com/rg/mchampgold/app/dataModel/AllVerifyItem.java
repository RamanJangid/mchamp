package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class AllVerifyItem {

	@SerializedName("pan_data")
	private PanData panData;

	@SerializedName("mobile_verify")
	private int mobileVerify;

	@SerializedName("email_verify")
	private int emailVerify;

	@SerializedName("bank_verify")
	private int bankVerify;

	@SerializedName("pan_verify")
	private int panVerify;

	public void setPanData(PanData panData){
		this.panData = panData;
	}

	public PanData getPanData(){
		return panData;
	}

	public void setMobileVerify(int mobileVerify){
		this.mobileVerify = mobileVerify;
	}

	public int getMobileVerify(){
		return mobileVerify;
	}

	public void setEmailVerify(int emailVerify){
		this.emailVerify = emailVerify;
	}

	public int getEmailVerify(){
		return emailVerify;
	}

	public void setBankVerify(int bankVerify){
		this.bankVerify = bankVerify;
	}

	public int getBankVerify(){
		return bankVerify;
	}

	public void setPanVerify(int panVerify){
		this.panVerify = panVerify;
	}

	public int getPanVerify(){
		return panVerify;
	}

	@Override
 	public String toString(){
		return 
			"AllVerifyItem{" +
			"pan_data = '" + panData + '\'' + 
			",mobile_verify = '" + mobileVerify + '\'' + 
			",email_verify = '" + emailVerify + '\'' + 
			",bank_verify = '" + bankVerify + '\'' + 
			",pan_verify = '" + panVerify + '\'' + 
			"}";
		}
}