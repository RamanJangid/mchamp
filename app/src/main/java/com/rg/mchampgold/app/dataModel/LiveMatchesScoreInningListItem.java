package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class LiveMatchesScoreInningListItem {

    @SerializedName("players")
    private ArrayList<LiveMatchesScorePlayerListItem> players;

    @SerializedName("bowlers")
    private ArrayList<LiveMatchesScoreBowlerListItem> bowlers;

    @SerializedName("short_name")
    private String short_name;

    @SerializedName("scores_full")
    private String scores_full;

    @SerializedName("overs")
    private float overs;

    @SerializedName("team_id")
    private int team_id;

    @SerializedName("inning")
    private int inning;

    @SerializedName("sort")
    private int sort;

    @SerializedName("extra_run")
    private int extra_run;

    public ArrayList<LiveMatchesScorePlayerListItem> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<LiveMatchesScorePlayerListItem> players) {
        this.players = players;
    }

    public ArrayList<LiveMatchesScoreBowlerListItem> getBowlers() {
        return bowlers;
    }

    public void setBowlers(ArrayList<LiveMatchesScoreBowlerListItem> bowlers) {
        this.bowlers = bowlers;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getScores_full() {
        return scores_full;
    }

    public void setScores_full(String scores_full) {
        this.scores_full = scores_full;
    }

    public float getOvers() {
        return overs;
    }

    public void setOvers(float overs) {
        this.overs = overs;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public int getInning() {
        return inning;
    }

    public void setInning(int inning) {
        this.inning = inning;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getExtra_run() {
        return extra_run;
    }

    public void setExtra_run(int extra_run) {
        this.extra_run = extra_run;
    }
}
