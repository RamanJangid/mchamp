package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class MyBalanceResultItem {

	@SerializedName("total")
	private double total;

	@SerializedName("balance")
	private double balance;

	@SerializedName("winning")
	private double winning;

	@SerializedName("totalamount")
	private double totalamount;

	@SerializedName("expireamount")
	private double expireamount;

	@SerializedName("total_match_play")
	private int totalMatchPlay;

	@SerializedName("bonus")
	private double bonus;

	@SerializedName("total_league_play")
	private int totalLeaguePlay;

	@SerializedName("total_winning")
	private double totalWinning;

	@SerializedName("total_contest_win")
	private int totalContestWin;

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public double getWinning() {
		return winning;
	}

	public void setWinning(double winning) {
		this.winning = winning;
	}

	public double getTotalamount() {
		return totalamount;
	}

	public void setTotalamount(double totalamount) {
		this.totalamount = totalamount;
	}

	public double getExpireamount() {
		return expireamount;
	}

	public void setExpireamount(double expireamount) {
		this.expireamount = expireamount;
	}

	public int getTotalMatchPlay() {
		return totalMatchPlay;
	}

	public void setTotalMatchPlay(int totalMatchPlay) {
		this.totalMatchPlay = totalMatchPlay;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}

	public int getTotalLeaguePlay() {
		return totalLeaguePlay;
	}

	public void setTotalLeaguePlay(int totalLeaguePlay) {
		this.totalLeaguePlay = totalLeaguePlay;
	}

	public double getTotalWinning() {
		return totalWinning;
	}

	public void setTotalWinning(double totalWinning) {
		this.totalWinning = totalWinning;
	}

	public int getTotalContestWin() {
		return totalContestWin;
	}

	public void setTotalContestWin(int totalContestWin) {
		this.totalContestWin = totalContestWin;
	}

	@Override
	public String toString() {
		return "MyBalanceResultItem{" +
				"total=" + total +
				", balance=" + balance +
				", winning=" + winning +
				", totalamount=" + totalamount +
				", expireamount=" + expireamount +
				", totalMatchPlay=" + totalMatchPlay +
				", bonus=" + bonus +
				", totalLeaguePlay=" + totalLeaguePlay +
				", totalWinning=" + totalWinning +
				", totalContestWin=" + totalContestWin +
				'}';
	}
}