package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class BannerListResponse{

	@SerializedName("result")
	private ArrayList<BannerListItem> result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;


	public int getOnline_version() {
		return online_version;
	}

	public void setOnline_version(int online_version) {
		this.online_version = online_version;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	@SerializedName("version")
	private int online_version;

	@SerializedName("version_code")
	private String online_version_string;


	@SerializedName("app_download_url")
	private String downloadUrl;

	@SerializedName("base_url")
	private String base_url = "";


	@SerializedName("api_base_url")
	private String api_base_url = "";

	@SerializedName("popup_status")
	private String popup_status;

	@SerializedName("popup_image")
	private String popup_image;

	@SerializedName("notification")
	private String notification;

	@SerializedName("promo_code")
	private String promo_code;

	@SerializedName("refer_earn")
	private String refer_earn;

	@SerializedName("update_text")
	private String updateText;

	@SerializedName("user_team_name")
	private String user_team_name;

	@SerializedName("user_state")
	private String user_state;

	@SerializedName("user_id")
	private String user_id;

	@SerializedName("auth_token")
	private String auth_token;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getAuth_token() {
		return auth_token;
	}

	public void setAuth_token(String auth_token) {
		this.auth_token = auth_token;
	}

	public String getUser_team_name() {
		return user_team_name;
	}

	public void setUser_team_name(String user_team_name) {
		this.user_team_name = user_team_name;
	}

	public String getUser_state() {
		return user_state;
	}

	public void setUser_state(String user_state) {
		this.user_state = user_state;
	}

	public String getApi_base_url() {
		return api_base_url;
	}

	public void setApi_base_url(String api_base_url) {
		this.api_base_url = api_base_url;
	}

	public String getOnline_version_string() {
		return online_version_string;
	}

	public void setOnline_version_string(String online_version_string) {
		this.online_version_string = online_version_string;
	}

	public void setResult(ArrayList<BannerListItem> result){
		this.result = result;
	}

	public ArrayList<BannerListItem> getResult(){
		return result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public String getBase_url() {
		return base_url;
	}

	public void setBase_url(String base_url) {
		this.base_url = base_url;
	}

	public String getPopup_status() {
		return popup_status==null?popup_status="0":popup_status;
	}

	public void setPopup_status(String popup_status) {
		this.popup_status = popup_status;
	}

	public String getPopup_image() {
		return popup_image;
	}

	public void setPopup_image(String popup_image) {
		this.popup_image = popup_image;
	}

	public String getNotification() {
		return notification==null?"0":notification;
	}

	public void setNotification(String notification) {
		this.notification = notification;
	}

	public String getPromo_code() {
		return promo_code==null?"0":promo_code;
	}

	public void setPromo_code(String promo_code) {
		this.promo_code = promo_code;
	}

	public String getRefer_earn() {
		return refer_earn==null?"0":refer_earn;
	}

	public void setRefer_earn(String refer_earn) {
		this.refer_earn = refer_earn;
	}

	public String getUpdateText() {
		return updateText == null? "" : updateText;
	}

	public void setUpdateText(String updateText) {
		this.updateText = updateText;
	}
}