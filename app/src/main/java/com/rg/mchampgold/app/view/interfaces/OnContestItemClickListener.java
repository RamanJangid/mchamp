package com.rg.mchampgold.app.view.interfaces;

import com.rg.mchampgold.app.dataModel.Contest;

public interface OnContestItemClickListener {
    void onContestClick(Contest contest, boolean isForDetail);
}
