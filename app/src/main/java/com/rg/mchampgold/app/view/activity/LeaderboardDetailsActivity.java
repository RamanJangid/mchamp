package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.LeaderboardMatchData;
import com.rg.mchampgold.app.dataModel.SeriesLeaderbardDeatilsResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.UserStatesMatchAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.databinding.LeaderboardDetailsLayoutBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class LeaderboardDetailsActivity extends AppCompatActivity {
    @Inject
    OAuthRestService oAuthRestService;
    LeaderboardDetailsLayoutBinding mBinding;
    ArrayList<LeaderboardMatchData> match_list=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(LeaderboardDetailsActivity.this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.leaderboard_details_layout);
        setSupportActionBar(mBinding.topToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("User Stats");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        getLeaderboardDetails();
    }
    public void getLeaderboardDetails() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest=new BaseRequest();
        baseRequest.setUser_id(getIntent().getStringExtra("user_id"));
        baseRequest.setSeries_id(getIntent().getStringExtra("series_id"));
        CustomCallAdapter.CustomCall<SeriesLeaderbardDeatilsResponse> bankDetailResponseCustomCall = oAuthRestService.getLeaderboardDetails(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<SeriesLeaderbardDeatilsResponse>() {
            @Override
            public void success(Response<SeriesLeaderbardDeatilsResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    match_list=response.body().getMatch_list();
                    AppUtils.loadImage(mBinding.ivPlayer,response.body().getImage());
                    mBinding.tvUserName.setText(response.body().getName());
                    mBinding.week.setText("Week "+response.body().getWeek_number());
                    mBinding.date.setText(response.body().getSeries_date());
                    mBinding.points.setText(response.body().getPoints());
                    mBinding.rank.setText("#"+response.body().getRank());
                    mBinding.seriesName.setText(response.body().getSeries_name());
                    mBinding.match.setText("Matches ("+match_list.size()+")");
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LeaderboardDetailsActivity.this);
                    mBinding.recyclerView.setLayoutManager(mLayoutManager);
                    mBinding.recyclerView.setAdapter(new UserStatesMatchAdapter(getApplicationContext(),match_list));
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
