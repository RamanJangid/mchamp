package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TeamCompareCVCModel {

    @SerializedName("captain")
    ArrayList<TeamCompareCVCDataModel> captain;

    @SerializedName("vice_captain")
    ArrayList<TeamCompareCVCDataModel> vice_captain;

    public ArrayList<TeamCompareCVCDataModel> getCaptain() {
        return captain;
    }

    public void setCaptain(ArrayList<TeamCompareCVCDataModel> captain) {
        this.captain = captain;
    }

    public ArrayList<TeamCompareCVCDataModel> getVice_captain() {
        return vice_captain;
    }

    public void setVice_captain(ArrayList<TeamCompareCVCDataModel> vice_captain) {
        this.vice_captain = vice_captain;
    }
}
