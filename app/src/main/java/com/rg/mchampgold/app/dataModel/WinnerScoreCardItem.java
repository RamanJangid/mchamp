package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class WinnerScoreCardItem {

	@SerializedName("winners")
	private int winners;

	@SerializedName("start_position")
	private String startPosition;

	@SerializedName("total")
	private double total;

	@SerializedName("price")
	private String price;

	@SerializedName("description")
	private String description;

	@SerializedName("id")
	private int id;

	public void setWinners(int winners){
		this.winners = winners;
	}

	public int getWinners(){
		return winners;
	}

	public void setStartPosition(String startPosition){
		this.startPosition = startPosition;
	}

	public String getStartPosition(){
		return startPosition;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"WinnerScoreCardItem{" +
			"winners = '" + winners + '\'' + 
			",start_position = '" + startPosition + '\'' + 
			",total = '" + total + '\'' + 
			",price = '" + price + '\'' + 
			",description = '" + description + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}

	public String showRank() {
		return "Rank "+startPosition;
	}

	public String showPrice() {
		return "₹"+price;
	}
}