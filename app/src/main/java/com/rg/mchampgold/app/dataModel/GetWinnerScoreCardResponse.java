package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GetWinnerScoreCardResponse{

	@SerializedName("result")
	private ArrayList<WinnerScoreCardItem> result;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setResult(ArrayList<WinnerScoreCardItem> result){
		this.result = result;
	}

	public ArrayList<WinnerScoreCardItem> getResult(){
		return result;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"GetWinnerScoreCardResponse{" + 
			"result = '" + result + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}