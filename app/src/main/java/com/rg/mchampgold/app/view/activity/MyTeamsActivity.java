package com.rg.mchampgold.app.view.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.JoinItem;
import com.rg.mchampgold.app.dataModel.MyBalanceResponse;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.SwitchTeamRequest;
import com.rg.mchampgold.app.dataModel.SwitchTeamResponse;
import com.rg.mchampgold.app.dataModel.Team;
import com.rg.mchampgold.app.dataModel.UsableBalanceItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.TextViewLinkHandler;
import com.rg.mchampgold.app.view.adapter.TeamItemAdapter;
import com.rg.mchampgold.app.view.basketball.BasketBallCreateTeamActivity;
import com.rg.mchampgold.app.view.basketball.BasketBallTeamPreviewActivity;
import com.rg.mchampgold.app.view.football.FootballCreateTeamActivity;
import com.rg.mchampgold.app.view.football.FootballTeamPreviewActivity;
import com.rg.mchampgold.app.viewModel.TeamViewModel;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityMyTeamsBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class MyTeamsActivity extends AppCompatActivity {

    public ActivityMyTeamsBinding mBinding;
    TeamItemAdapter mAdapter;
    TeamItemAdapter mAdapter1;
    private TeamViewModel teamViewModel;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    ArrayList<Team> listTemp = new ArrayList<>();
    ArrayList<Team> list = new ArrayList<>();
    ArrayList<Team> joinedList = new ArrayList<>();
    int teamCount;
    Context context;
    boolean isForJoinContest;
    double availableB;
    double usableB;
    Contest contest;
    String headerText;
    boolean isShowTimer;
    String joinnigB;

    boolean isSwitchTeam;

    String isJoinID = "";
    String sportKey = "";

    @Inject
    OAuthRestService oAuthRestService;
    private int multiEntry = 0;
    Dialog dialog;
    private int maxTeamLimit = 0;
    private boolean isSelectAll = false;
    private int remainingCount = 0;

    double entryFees;
    private boolean haveToGetData = false;
    private String switchTeamId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        teamViewModel = TeamViewModel.create(MyTeamsActivity.this);
        MyApplication.getAppComponent().inject(teamViewModel);
        MyApplication.getAppComponent().inject(MyTeamsActivity.this);
        context = this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_teams);
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            isForJoinContest = getIntent().getExtras().getBoolean(Constants.KEY_IS_FOR_JOIN_CONTEST, false);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT, "");
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER, false);
            contest = (Contest) getIntent().getExtras().getSerializable(Constants.KEY_CONTEST_DATA);
            isSwitchTeam = getIntent().getExtras().getBoolean(Constants.KEY_IS_FOR_SWITCH_TEAM, false);
            isJoinID = getIntent().getExtras().getString("isForJoinedId");
            switchTeamId = getIntent().getExtras().getString("team_id", "");
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
        }

        if (getSupportActionBar() != null) {
            if (isSwitchTeam) {
                getSupportActionBar().setTitle("Switch Team");
                mBinding.btnJoinContest.setText("Switch Team");
            }
            else{
                getSupportActionBar().setTitle(getString(R.string.my_teams));
            }

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (contest != null) {
            multiEntry = contest.getMultiEntry();
            maxTeamLimit = contest.getMaxTeamLimit();
            if (multiEntry == 1 && !isSwitchTeam) {
                mBinding.tvTeamLimit.setText("You can enter upto " + maxTeamLimit + " teams in this contest");
                mBinding.tvTeamLimit.setVisibility(View.VISIBLE);

            }
        }

        mBinding.ivSelectAll.setOnClickListener(v -> {
            mBinding.llSelectAll.performClick();
        });

        mBinding.llSelectAll.setOnClickListener(v -> {
            if (isSelectAll) {
                mBinding.ivSelectAll.setChecked(false);
                isSelectAll = false;
            } else {
                mBinding.ivSelectAll.setChecked(true);
                isSelectAll = true;
            }
            mAdapter.selectAll(isSelectAll);
        });


    /*    mBinding.matchHeaderInfo.tvTeamVs.setText(teamVsName);
        mBinding.matchHeaderInfo.ivTeamFirst.setImageURI(teamFirstUrl);
        mBinding.matchHeaderInfo.ivTeamSecond.setImageURI(teamSecondUrl);

        if(isShowTimer) {
            showTimer();
        }
        else {
            if(headerText.equalsIgnoreCase("Winner Declared")) {
                mBinding.matchHeaderInfo.tvTimeTimer.setText("Winner Declared");
                mBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#f70073"));
            }
            else if(headerText.equalsIgnoreCase("In Progress")) {
                mBinding.matchHeaderInfo.tvTimeTimer.setText("In Progress");
                mBinding.matchHeaderInfo.tvTimeTimer.setTextColor(getColor(R.color.color_match_progress));
            }
        }*/

        if (isForJoinContest) {
            //     mBinding.btnJoinLayout.setVisibility(View.VISIBLE);
            //    mBinding.btnCreateTeam.setVisibility(View.GONE);
        } else {
            //    mBinding.btnJoinLayout.setVisibility(View.GONE);
            //    mBinding.btnCreateTeam.setVisibility(View.VISIBLE);
        }

        setupRecyclerView();

   /*     mBinding.btnCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyTeamsActivity.this,CreateTeamActivity.class);
                intent.putExtra( Constants.KEY_MATCH_KEY,matchKey);
                intent.putExtra( Constants.KEY_TEAM_VS,teamVsName);
                intent.putExtra( Constants.KEY_TEAM_FIRST_URL,teamFirstUrl);
                intent.putExtra( Constants.KEY_TEAM_SECOND_URL,teamSecondUrl);
                intent.putExtra( Constants.KEY_STATUS_HEADER_TEXT,headerText);
                intent.putExtra( Constants.KEY_STATUS_IS_TIMER_HEADER,isShowTimer);
                startActivity(intent);
            }
        });*/

        mBinding.btnCreateTeamS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                if (sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
                    intent = new Intent(MyTeamsActivity.this, FootballCreateTeamActivity.class);
                } else if (sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
                    intent = new Intent(MyTeamsActivity.this, BasketBallCreateTeamActivity.class);
                } else {
                    intent = new Intent(MyTeamsActivity.this, CreateTeamActivity.class);
                }
                intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
                intent.putExtra(Constants.SPORT_KEY, sportKey);
                startActivity(intent);
            }
        });

        mBinding.btnJoinContest.setOnClickListener(view -> {
            if (!mAdapter.getMultiTeamId().equals("")) {
                if (isSwitchTeam) {
                    switchTeam();
                } else {
                    checkBalance();
                }
            } else {
                AppUtils.showErrorr(MyTeamsActivity.this, "Please select at least one team to join contest");
            }
        });


       /* mBinding.btnJoinContest.setOnClickListener(view -> {
            if (mAdapter.getSelectedTeamId() != 0) {
                checkBalance();
            } else {
                AppUtils.showErrorr(MyTeamsActivity.this, "Please select atleast one team to join contest");
            }
        });*/
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        mAdapter = new TeamItemAdapter(list, context, isForJoinContest, sportKey, multiEntry, isSwitchTeam, maxTeamLimit);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);

        mAdapter1 = new TeamItemAdapter(joinedList, context, isForJoinContest, sportKey, multiEntry, isSwitchTeam, maxTeamLimit);
        mBinding.recyclerView2.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this);
        mBinding.recyclerView2.setLayoutManager(mLayoutManager1);
        mBinding.recyclerView2.setAdapter(mAdapter1);
        //  getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        teamViewModel.getContestData().removeObservers(this);
        getData();
    }

    private void getData() {
        haveToGetData = true;
        listTemp.clear();
        list.clear();
        joinedList.clear();
        MyTeamRequest request = new MyTeamRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        if (contest != null)
            request.setChallengeId(contest.getId() + "");
        else
            request.setChallengeId(0 + "");
        teamViewModel.loadMyTeamRequest(request);
        teamViewModel.getContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (haveToGetData) {
                        haveToGetData = false;
                        if (arrayListResource.getData().getStatus() == 1) {
                            if (arrayListResource.getData().getTeamITem().getTeams().size() > 0) {
                                listTemp = arrayListResource.getData().getTeamITem().getTeams();
                                teamCount = arrayListResource.getData().getTeamITem().getTeams().size();

                                int joinedCount = 0;
                                if (multiEntry == 1 && !isSwitchTeam) {
                                    for (int i = 0; i < listTemp.size(); i++) {
                                        if (listTemp.get(i).getIsJoined() == 1) {
                                            joinedCount++;
                                            joinedList.add(listTemp.get(i));
                                        } else {
                                            list.add(listTemp.get(i));
                                        }
                                    }

                                    remainingCount = teamCount - joinedCount;

                                    if (remainingCount <= (maxTeamLimit - joinedCount)) {
                                        mBinding.llSelectAll.setVisibility(View.VISIBLE);
                                        mBinding.tvSelectAll.setText("Select All (" + (teamCount - joinedCount) + ")");
                                        mBinding.ivSelectAll.setChecked(false);
                                        isSelectAll = false;
                                    } else {
                                        mBinding.llSelectAll.setVisibility(View.GONE);
                                    }
                                } else {
                                    for (int i = 0; i < listTemp.size(); i++) {
                                        if (listTemp.get(i).getIsJoined() == 1) {
                                            joinedList.add(listTemp.get(i));
                                        } else {
                                            list.add(listTemp.get(i));
                                        }
                                        if (String.valueOf(listTemp.get(i).getTeamid()).equals(switchTeamId) && isSwitchTeam) {
                                            mBinding.tvTeamLimit.setVisibility(View.VISIBLE);
                                            mBinding.tvTeamLimit.setText("Choose a team to replace Team " + listTemp.get(i).getTeamnumber());
                                        }
                                    }
                                }
                                mAdapter.updateData(list, joinedCount);
                                mAdapter1.updateData(joinedList, joinedCount);
                                if (joinedList.size() > 0) {
                                    mBinding.joinedTxt.setVisibility(View.VISIBLE);
                                    mBinding.recyclerView2.setVisibility(View.VISIBLE);
                                } else {
                                    mBinding.joinedTxt.setVisibility(View.GONE);
                                    mBinding.recyclerView2.setVisibility(View.GONE);
                                }
                            }
                            setTeamContestCount();
                        } else {
                            Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                    break;
                }
            }

        });
    }


    private void checkBalance() {
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setLeagueId(contest.getId() + "");
        teamViewModel.loadBalanceRequest(request);
        teamViewModel.getBalanceData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getResult().size() > 0) {
                        UsableBalanceItem balanceItem = arrayListResource.getData().getResult().get(0);
                        availableB = balanceItem.getUsertotalbalance();
                        usableB = balanceItem.getUsablebalance();
                        createDialog();
                        setTeamContestCount();
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }

    public void createDialog() {

        if (dialog != null) {
            if (dialog.isShowing()) {
                return;
            }
        }
        dialog = new Dialog(MyTeamsActivity.this);
        dialog.setContentView(R.layout.joined_team_confirm_dialog);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        LinearLayout okBtn = dialog.findViewById(R.id.ok_btn);
        TextView currentBalTxt = dialog.findViewById(R.id.currentBalTxt);
        TextView joinedBaltxt = dialog.findViewById(R.id.joinedBaltxt);
        TextView remaingBaltxt = dialog.findViewById(R.id.remaingBaltxt);
        TextView tv_tc_join_dialog = dialog.findViewById(R.id.tv_tc_join_dialog);
        RelativeLayout cancelButton = dialog.findViewById(R.id.cancel_button);
        TextView tPay = dialog.findViewById(R.id.toPay);
        RelativeLayout switchTeamBtn = dialog.findViewById(R.id.switch_team_Btn);

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        entryFees = Double.parseDouble((contest.getEntryfee())) * mAdapter.getSelectedTeamCount();

        currentBalTxt.setText("₹ " + decimalFormat.format(availableB));
        joinedBaltxt.setText("₹ " + decimalFormat.format(entryFees));
        tv_tc_join_dialog.setText(Html.fromHtml("By joining this contest, you accept <font color='" + getResources().getColor(R.color.pink) + "'>" + getString(R.string.app_names) + "</font> <a href='" + MyApplication.terms_url + "'>T&amp;C</a> and <a href='" + MyApplication.privacy_url + "'>Privacy Policy</a> and confirm that you are not a resident of Assam, Odisha, Sikkim, Andhra Pradesh, Tamil Nadu, Nagaland or Telangana."));
        tv_tc_join_dialog.setMovementMethod(LinkMovementMethod.getInstance());
        tv_tc_join_dialog.setMovementMethod(new TextViewLinkHandler() {
            @Override
            public void onLinkClick(String url) {
                if (url.equalsIgnoreCase(MyApplication.terms_url)) {
                    AppUtils.openWebViewActivity(getString(R.string.terms_conditions), MyApplication.terms_url);
                } else {
                    AppUtils.openWebViewActivity(getString(R.string.privacy_policy), MyApplication.privacy_url);
                }
//                Toast.makeText(mainBinding.tvTc.getContext(), url, Toast.LENGTH_SHORT).show();
            }
        });
        double remainBal = usableB;
        tPay.setText("₹ " + (entryFees - remainBal));

        if (remainBal > 0) {
            remaingBaltxt.setText("₹ " + decimalFormat.format(remainBal));
        } else {
            remaingBaltxt.setText("₹ 0.0");
        }

        dialog.show();

        okBtn.setOnClickListener(view13 -> dialog.dismiss());

        cancelButton.setOnClickListener(view1 -> dialog.dismiss());

        switchTeamBtn.setOnClickListener(view12 -> {
            dialog.dismiss();
            if (isSwitchTeam) {
                switchTeam();
            } else {
                joinChallenge();
            }
//            if (contest.getIsBonus() == 1) {
//                if ((usableB + availableB) < entryFees) {
//                    double requiredBalance = entryFees - (usableB + availableB);
//                    startActivity(new Intent(MyTeamsActivity.this, AddBalanceActivity.class).putExtra("balance_required", requiredBalance));
//                } else {
//                    if (isSwitchTeam) {
//                        switchTeam();
//                    } else {
//                        joinChallenge();
//                    }
//                }
//
//            } else {
//                if (availableB < entryFees) {
//                    double requiredBalance = entryFees - (availableB);
//                    startActivity(new Intent(MyTeamsActivity.this, AddBalanceActivity.class).putExtra("balance_required", requiredBalance));
//                } else {
//                    if (isSwitchTeam) {
//                        switchTeam();
//                    } else {
//                        joinChallenge();
//                    }
//
//                }
//            }


        });

    }

    private void joinChallenge() {
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setLeagueId(contest.getId() + "");
        request.setMatchKey(contest.getMatchkey());
        request.setTeamId(mAdapter.getMultiTeamId());
        request.setSport_key(sportKey);
        teamViewModel.loadJoinContestRequest(request);
        teamViewModel.joinContest().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getResult().size() > 0) {
                        ArrayList<JoinItem> joinItems = arrayListResource.getData().getResult();
                        if (joinItems.get(0).isStatus()) {
                            MyApplication.referCode = joinItems.get(0).getReferCode();
                            getUserBalance();
                        } else {
                            AppUtils.showErrorr(MyTeamsActivity.this, joinItems.get(0).getMessage());
                        }
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    break;
                }
            }

        });
    }

    private void getUserBalance() {
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<MyBalanceResponse> myBalanceResponseCustomCall = oAuthRestService.getUserBalance(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<MyBalanceResponse>() {
            @Override
            public void success(Response<MyBalanceResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    MyBalanceResponse myBalanceResponse = response.body();
                    if (myBalanceResponse.getStatus() == 1 && myBalanceResponse.getResult().size() > 0) {

                        MyBalanceResultItem myBalanceResultItem = myBalanceResponse.getResult().get(0);

                        /*logSpendCreditsEvent(new Gson().toJson(myBalanceResultItem),
                                MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID),
                                myBalanceResultItem.getTotalamount());*/

                        MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, myBalanceResultItem.getBalance() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_WINING_AMOUNT, myBalanceResultItem.getWinning() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BONUS_BALANCE, myBalanceResultItem.getBonus() + "");
                        Toast.makeText(MyTeamsActivity.this, "You have Successfully join this contest", Toast.LENGTH_SHORT).show();
                        MyApplication.teamJoinedCount = mAdapter.getMultiTeamId().split(",").length;
                        MyApplication.teamJoined = true;
                        finish();
                    } else {
                        AppUtils.showErrorr(MyTeamsActivity.this, myBalanceResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                e.printStackTrace();
            }
        });
    }

    private void setTeamContestCount() {
        if (teamCount == 0)
            Toast.makeText(MyApplication.appContext, Constants.NO_TEAM_CREATED, Toast.LENGTH_SHORT).show();

        mBinding.btnCreateTeamS.setText("Create Team " + (teamCount + 1));
        if (teamCount > 11)
            mBinding.btnCreateTeamS.setVisibility(View.GONE);
        else
            mBinding.btnCreateTeamS.setVisibility(View.VISIBLE);

        //   teamCount = teamCount + 1;
    }


    public void editOrClone(ArrayList<Player> list, int teamId, String teamName) {
        Intent intent;
        if (sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(MyTeamsActivity.this, FootballCreateTeamActivity.class);
        } else if (sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(MyTeamsActivity.this, BasketBallCreateTeamActivity.class);
        } else {
            intent = new Intent(MyTeamsActivity.this, CreateTeamActivity.class);
        }
        intent.putExtra(Constants.KEY_TEAM_NAME, teamName);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_TEAM_ID, teamId);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra("isFromEditOrClone", true);
        intent.putExtra("selectedList", list);
        intent.putExtra(Constants.SPORT_KEY, sportKey);
        startActivity(intent);
    }


    public void openPreviewActivity(ArrayList<Player> list, String teamName) {
        ArrayList<Player> selectedWkList = new ArrayList<>();
        ArrayList<Player> selectedBatLiSt = new ArrayList<>();
        ArrayList<Player> selectedArList = new ArrayList<>();
        ArrayList<Player> selectedBowlList = new ArrayList<>();
        ArrayList<Player> selectedcList = new ArrayList<>();
        Intent intent;
        if (sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(MyTeamsActivity.this, FootballTeamPreviewActivity.class);
            for (Player player : list) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_GK))
                    selectedWkList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_DEF))
                    selectedBatLiSt.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_MID))
                    selectedArList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ST))
                    selectedBowlList.add(player);
            }
        } else if (sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(MyTeamsActivity.this, BasketBallTeamPreviewActivity.class);
            for (Player player : list) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PG))
                    selectedWkList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SG))
                    selectedBatLiSt.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SF))
                    selectedArList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PF))
                    selectedBowlList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_C))
                    selectedcList.add(player);
            }
            intent.putExtra(Constants.KEY_TEAM_LIST_C, selectedcList);
        } else {
            intent = new Intent(MyTeamsActivity.this, TeamPreviewActivity.class);
            for (Player player : list) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_KEEP))
                    selectedWkList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BAT))
                    selectedBatLiSt.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ALL_R))
                    selectedArList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BOL))
                    selectedBowlList.add(player);
            }
        }
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_TEAM_NAME, teamName);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, isShowTimer);
        intent.putExtra(Constants.SPORT_KEY, sportKey);


        intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedWkList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedBatLiSt);
        intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedArList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedBowlList);

        startActivity(intent);
    }

    private void switchTeam() {
        mBinding.setRefreshing(true);
        SwitchTeamRequest teamRequest = new SwitchTeamRequest();
        teamRequest.setMatchkey(matchKey);
        teamRequest.setChallengeId(contest.getId() + "");
        teamRequest.setUserid(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        teamRequest.setTeamid(mAdapter.getMultiTeamId());
        teamRequest.setJoinid(isJoinID);
        teamRequest.setSport_key(sportKey);
        CustomCallAdapter.CustomCall<SwitchTeamResponse> userFullDetailsResponseCustomCall = oAuthRestService.switchTeam(teamRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<SwitchTeamResponse>() {
            @Override
            public void success(Response<SwitchTeamResponse> response) {
                mBinding.setRefreshing(false);
                SwitchTeamResponse findJoinTeamResponse = response.body();
                if (findJoinTeamResponse != null) {
                    if (findJoinTeamResponse.getStatus() == 1) {
                        Toast.makeText(MyTeamsActivity.this, findJoinTeamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(MyTeamsActivity.this, findJoinTeamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MyTeamsActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }

    public void changeNoOfTeamText(int currentSelectedTeamCount) {
        if (currentSelectedTeamCount < remainingCount || currentSelectedTeamCount == 0) {
            mBinding.ivSelectAll.setChecked(false);
            isSelectAll = false;
        } else if (remainingCount == currentSelectedTeamCount) {
            mBinding.ivSelectAll.setChecked(true);
            isSelectAll = true;
        }
//        mBinding.tvCountOfTeam.setText(currentSelectedTeamCount + "");
    }
}
