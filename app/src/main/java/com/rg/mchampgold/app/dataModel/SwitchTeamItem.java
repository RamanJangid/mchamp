package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class SwitchTeamItem {

	@SerializedName("msg")
	private String msg;

	@SerializedName("status")
	private int status;

	@SerializedName("teamnumber")
	private int teamnumber;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setTeamnumber(int teamnumber){
		this.teamnumber = teamnumber;
	}

	public int getTeamnumber(){
		return teamnumber;
	}

	@Override
 	public String toString(){
		return 
			"SwitchTeamItem{" +
			"msg = '" + msg + '\'' + 
			",status = '" + status + '\'' + 
			",teamnumber = '" + teamnumber + '\'' + 
			"}";
		}
}