package com.rg.mchampgold.app.view.myMatches.upComing;


import androidx.arch.core.util.Function;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.dataModel.Match;
import com.rg.mchampgold.app.dataModel.MatchListResponse;
import com.rg.mchampgold.app.repository.MatchRepository;
import com.rg.mchampgold.common.api.Resource;
import com.rg.mchampgold.common.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;


public class MyMatchesUpComingMatchListViewModel extends ViewModel {

    private MatchRepository mRepository;
    private String searchKey;

    private final MutableLiveData<BaseRequest> networkInfoObservable = new MutableLiveData<>();
    private LiveData<Resource<MatchListResponse>> searchData = Transformations.switchMap(networkInfoObservable, new Function<BaseRequest, LiveData<Resource<MatchListResponse>>>() {
        @Override
        public LiveData<Resource<MatchListResponse>> apply(final BaseRequest input) {
            LiveData<Resource<MatchListResponse>> resourceLiveData = mRepository.getMyMatchesList(input);
            final MediatorLiveData<Resource<MatchListResponse>> mediator = new MediatorLiveData<Resource<MatchListResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<MatchListResponse>>() {
                @Override
                public void onChanged(Resource<MatchListResponse> arrayListResource) {
                    MatchListResponse resp = arrayListResource.getData();
                    Resource<MatchListResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            MatchListResponse matchListResponse  = transform(resp);
                            response = Resource.success(matchListResponse);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    // Transform stockist order into flexible items.
    private MatchListResponse transform(MatchListResponse response) {
        MatchListResponse matchListResponse = new MatchListResponse();
        matchListResponse.setStatus(response.getStatus());
        matchListResponse.setMessage(response.getMessage());
        ArrayList<Match> matches = new ArrayList<>();
        for(Match match:response.getResult()) {
          if(match.getMatchStatusKey() == Constants.KEY_UPCOMING_MATCH)
              matches.add(match);
        }
        matchListResponse.setResult(matches);
        return matchListResponse;

    }

    /*
    Get the view model instance.
     */
    public static MyMatchesUpComingMatchListViewModel create(Fragment fragment) {
        MyMatchesUpComingMatchListViewModel viewModel = ViewModelProviders.of(fragment).get(MyMatchesUpComingMatchListViewModel.class);
        return viewModel;
    }


    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<MatchListResponse>> getSearchData() {
        return searchData;
    }




    @Inject
    public void setRepository(MatchRepository repository) {
        this.mRepository = repository;
    }


    public void load(BaseRequest request) {
        networkInfoObservable.setValue(request);
    }

    public void clear() {
        networkInfoObservable.setValue(null);
    }

    @Override
    protected void onCleared() {
        super.onCleared();

    }


}