package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.LeaderboardMatchData;
import com.rg.mchampgold.app.view.interfaces.OnMoreItemClickListener;
import com.rg.mchampgold.databinding.UserStateMatchItemBinding;

import java.util.ArrayList;

public class UserStatesMatchAdapter extends RecyclerView.Adapter<UserStatesMatchAdapter.ViewHolder> {

    private ArrayList<LeaderboardMatchData> moreInfoDataList;
    private OnMoreItemClickListener listener;
    private boolean isForPaymentOptions;
    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        final UserStateMatchItemBinding binding;

        public ViewHolder(UserStateMatchItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public UserStatesMatchAdapter(Context context,ArrayList<LeaderboardMatchData> moreInfoDataList) {
        this.moreInfoDataList = moreInfoDataList;
        this.context=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        UserStateMatchItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.user_state_match_item,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setData(moreInfoDataList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }



}