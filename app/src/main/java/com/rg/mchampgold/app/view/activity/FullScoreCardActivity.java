package com.rg.mchampgold.app.view.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.LiveMatchesScoreCardResponse;
import com.rg.mchampgold.app.dataModel.LiveMatchesScoreTeamListItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.fragment.FullScoreCardInningsTabFragment;
import com.rg.mchampgold.app.view.fragment.PlayerFullScoreCardFragment;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FullScoreCardLayoutBinding;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

public class FullScoreCardActivity extends AppCompatActivity {
    @Inject
    OAuthRestService oAuthRestService;
    FullScoreCardLayoutBinding fullScoreCArdActivityBinding;
    String teamvsteam;
    String teamName1;
    String teamName1Image;
    String scoreFull1;
    String teamName2;
    String teamName2Image;
    String scoreFull2;
    String match_key;
    ArrayList<LiveMatchesScoreTeamListItem> teamsListItems=new ArrayList<>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        fullScoreCArdActivityBinding = DataBindingUtil.setContentView(this, R.layout.full_score_card_layout);
        MyApplication.getAppComponent().inject(this);
        match_key=getIntent().getStringExtra("match_key");
        teamvsteam=getIntent().getStringExtra("teamvsteam");
        teamName1=getIntent().getStringExtra("teamName1");
        teamName1Image=getIntent().getStringExtra("teamName1Image");
        scoreFull1=getIntent().getStringExtra("scoreFull1");
        teamName2=getIntent().getStringExtra("teamName2");
        teamName2Image=getIntent().getStringExtra("teamName2Image");
        scoreFull2=getIntent().getStringExtra("scoreFull2");
        setSupportActionBar(fullScoreCArdActivityBinding.fullcardtoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(teamvsteam);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        LiveScoreBoardlist();

    }

    private void setupRecyclerView() {
        setupViewPager(fullScoreCArdActivityBinding.viewPager);
        fullScoreCArdActivityBinding.tabLayout.getTabAt(0).setCustomView(createTabItemView(0,teamName1Image));
        fullScoreCArdActivityBinding.tabLayout.getTabAt(1).setCustomView(createTabItemView(1,teamName2Image));
    }

    private View createTabItemView(int pos,String imgUri) {
        TabLayout.LayoutParams ll_params = new TabLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        TabLayout.LayoutParams tv_params = new TabLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout linearLayout=new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setLayoutParams(ll_params);
        TextView textView = new TextView(this);
        textView.setTextSize(14);
        textView.setTextColor(getResources().getColor(R.color.black));
        textView.setTypeface(Typeface.createFromAsset(getAssets(),"font/roboto_regular.ttf"));
        tv_params.setMarginStart(15);
        textView.setLayoutParams(tv_params);
        textView.setText(pos==0?teamName1+"("+ scoreFull1+")":teamName2+"("+ scoreFull2+")");
        ImageView imageView = new ImageView(this);
        TabLayout.LayoutParams params = new TabLayout.LayoutParams(100,
                100);
        imageView.setLayoutParams(params);
        AppUtils.loadImage(imageView,imgUri);
        linearLayout.addView(imageView);
        linearLayout.addView(textView);
        return linearLayout;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (teamsListItems.get(0).getInnings().size()>1){
            adapter.addFrag(new FullScoreCardInningsTabFragment(teamsListItems.get(0).getInnings(),0), teamName1+"("+ scoreFull1+")");
        }else {
            adapter.addFrag(new PlayerFullScoreCardFragment(teamsListItems.get(0).getInnings().get(0)), teamName1+"("+ scoreFull1+")");
        }
        if (teamsListItems.get(1).getInnings().size()>1){
            adapter.addFrag(new FullScoreCardInningsTabFragment(teamsListItems.get(1).getInnings(),1), teamName2+"("+ scoreFull2+")");
        }else {
            adapter.addFrag(new PlayerFullScoreCardFragment(teamsListItems.get(1).getInnings().get(0)), teamName2+"("+ scoreFull2+")");
        }
        viewPager.setAdapter(adapter);
        fullScoreCArdActivityBinding.tabLayout.setupWithViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void LiveScoreBoardlist() {
        fullScoreCArdActivityBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setMatchkey(match_key);
        CustomCallAdapter.CustomCall<LiveMatchesScoreCardResponse> orderIdResponse = oAuthRestService.liveScoreBoard(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<LiveMatchesScoreCardResponse>() {
            @Override
            public void success(Response<LiveMatchesScoreCardResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    LiveMatchesScoreCardResponse bannerListResponse = response.body();
                    if (bannerListResponse.getStatus() == 1 && bannerListResponse.getResult().size() > 0) {
                        teamsListItems=bannerListResponse.getResult();
                        setupRecyclerView();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
                fullScoreCArdActivityBinding.setRefreshing(false);
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                e.printStackTrace();
                fullScoreCArdActivityBinding.setRefreshing(false);
            }
        });
    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
