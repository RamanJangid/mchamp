package com.rg.mchampgold.app.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.ImageUploadResponse;
import com.rg.mchampgold.app.dataModel.PanDetailItem;
import com.rg.mchampgold.app.dataModel.PanDetatilResponse;
import com.rg.mchampgold.app.dataModel.PanVerificationRequest;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.ProgressRequestBody;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentPanValidationBinding;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class PanValidationFragment extends Fragment implements ProgressRequestBody.UploadCallbacks {

    private FragmentPanValidationBinding mBinding;
    private Context context;
    String fileName = "";
    private String fileImage = "";


    private static int GALLERY_REQUEST_CODE = 100;
    private static int CAMERA_REQUEST_CODE = 101;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pan_validation, container, false);
        context = getActivity();

        AppUtils.disableCopyPaste(mBinding.name);
        AppUtils.disableCopyPaste(mBinding.panNumber);
        AppUtils.disableCopyPaste(mBinding.confirmPanNumber);

        if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0) == 1) {
            mBinding.verifiedLayout.setVisibility(View.VISIBLE);
            mBinding.notVerifiedLayout.setVisibility(View.GONE);
        } else {
            mBinding.notVerifiedLayout.setVisibility(View.VISIBLE);
            mBinding.verifiedLayout.setVisibility(View.GONE);
        }

        mBinding.btnSubmit.setOnClickListener(view -> validate());

        mBinding.btnUpload.setOnClickListener(view -> {

            if (getActivity().checkCallingOrSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                if (getActivity().checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED&&getActivity().checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    showImageSelectionDialog();
                }else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 22);
                }
            }else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 22);
            }

        });
        getPainDetail();

        return mBinding.getRoot();
    }

    private void showImageSelectionDialog() {

        LayoutInflater inflater1 = getLayoutInflater();
        View alertLayout = inflater1.inflate(R.layout.layout_pic_upload, null);

        final TextView tvGallery = alertLayout.findViewById(R.id.tv_gallery);
        final TextView tvCamera = alertLayout.findViewById(R.id.tv_camera);
        final TextView tvCancel = alertLayout.findViewById(R.id.tv_cancel);

        /*AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(alertLayout);
        AlertDialog alert = builder.create();*/

        Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(alertLayout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


        tvGallery.setOnClickListener(view1 -> {
            dialog.dismiss();
            openGallery();
        });


        tvCamera.setOnClickListener(view12 -> {
            dialog.dismiss();
            openCamera();
        });

        tvCancel.setOnClickListener(view13 -> dialog.dismiss());
        dialog.show();


    }


    public boolean validPan() {
        String text = mBinding.panNumber.getText().toString();
        if (text.length() != 10) {
            return false;
        } else if (!text.matches("(([A-Za-z]{5})([0-9]{4})([a-zA-Z]))")) {
//            Toast.makeText(context, "11111", Toast.LENGTH_SHORT).show();
            return false;
        } else
            return true;
    }


    public void validate() {
        if (mBinding.name.getText().toString().length() < 4)
            AppUtils.showErrorr((AppCompatActivity) context, "Please enter a valid name.");
        else if (!validPan())
            AppUtils.showErrorr((AppCompatActivity) context, "Please enter a valid PAN Number.");
        else if (!mBinding.panNumber.getText().toString().trim().equalsIgnoreCase(mBinding.confirmPanNumber.getText().toString().trim())) {
            AppUtils.showErrorr((AppCompatActivity) context, "Pan Number and Confirm Pan Number are not matching");
        } else if (fileName.equals("")) {
            AppUtils.showErrorr((AppCompatActivity) context, "Upload PAN Card Image.");
        } else {
            verifyPanDetail();
        }
    }

    private void getPainDetail() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<PanDetatilResponse> bankDetailResponseCustomCall = oAuthRestService.getPanDetail(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<PanDetatilResponse>() {
            @Override
            public void success(Response<PanDetatilResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    PanDetatilResponse panDetatilResponse = response.body();
                    if (panDetatilResponse.getStatus() == 1 && panDetatilResponse.getResult() != null) {
                        PanDetailItem panDetailItem = panDetatilResponse.getResult();

                        if (panDetailItem.getStatus() == 0) {
                            mBinding.ivEmailVerified.setImageResource(R.drawable.ic_verification_pan);
                            verifiedLayout("Your PAN details are sent for verification", "Under Review");
                        } else if (panDetailItem.getStatus() == 1) {
                            verifiedLayout("Your PAN Details are verified", panDetailItem.getPannumber());
                            mBinding.ivEmailVerified.setImageResource(R.drawable.ic_email_verified);
                        } else {
                            notVerifiedLayout();
                        }
                    } else {
                        Toast.makeText(context, panDetatilResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    ProgressDialog pDialog = null;

    private void showProgress() {
        pDialog = new ProgressDialog(getActivity(), R.style.RoundedCornersDialog);
        pDialog.setMessage("Uploading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void uploadPanImage() {
//        mBinding.setRefreshing(true);
        showProgress();
        String userId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);
        RequestBody requestBodyUserId = RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        File file = new File(fileImage);
        long length = (file.length() / (1024 * 1024));
        if (length > 1) {
            file = AppUtils.saveBitmapToFile(file);
        }
        ProgressRequestBody progressRequestBody = new ProgressRequestBody(file, "multipart/form-data", this);
//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part uploadPic = MultipartBody.Part.createFormData("file", file.getName(), progressRequestBody);
        CustomCallAdapter.CustomCall<ImageUploadResponse> imageUploadResponseCustomCall = oAuthRestService.uploadPanImageMu(requestBodyUserId, uploadPic);
        imageUploadResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<ImageUploadResponse>() {

            @Override
            public void success(Response<ImageUploadResponse> response) {
//                mBinding.setRefreshing(false);
                if (pDialog != null) pDialog.dismiss();

                ImageUploadResponse imageUploadResponse = response.body();
                if (imageUploadResponse.getStatus() == 1) {
                    if (imageUploadResponse.getResult().get(0).getStatus() == 1) {
                        mBinding.imageProgress.setVisibility(View.VISIBLE);
                        fileName = imageUploadResponse.getResult().get(0).getImage();
                        AppUtils.loadPanBankImage(mBinding.imageProgress,mBinding.ivPanCard, fileName, true);
                        AppUtils.showSuccess((AppCompatActivity) context, imageUploadResponse.getMessage());
                    } else {
                        AppUtils.showErrorr((AppCompatActivity) context, imageUploadResponse.getMessage());
                    }
                } else {
                    AppUtils.showErrorr((AppCompatActivity) context, imageUploadResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
//                mBinding.setRefreshing(false);
                if (pDialog != null) pDialog.dismiss();
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {
        pDialog.setProgress(percentage);
    }

    @Override
    public void onError() {
        pDialog.dismiss();
    }

    @Override
    public void onFinish() {
        pDialog.setProgress(100);
    }


    public void verifyPanDetail() {
        mBinding.setRefreshing(true);
        PanVerificationRequest panVerificationRequest = new PanVerificationRequest();
        panVerificationRequest.setUserId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        panVerificationRequest.setPanName(mBinding.name.getText().toString().trim());
        panVerificationRequest.setPanNumber(mBinding.panNumber.getText().toString().trim());
        panVerificationRequest.setImage(fileName);
        //  panVerificationRequest.setPanDob(mBinding.dob.getText().toString().trim());
        //   panVerificationRequest.setState(mBinding.stateSpinner.getSelectedItem().toString());
        CustomCallAdapter.CustomCall<PanDetatilResponse> bankDetailResponseCustomCall = oAuthRestService.panVerify(panVerificationRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<PanDetatilResponse>() {
            @Override
            public void success(Response<PanDetatilResponse> response) {
                mBinding.setRefreshing(false);
                PanDetatilResponse panDetatilResponse = response.body();
                if (panDetatilResponse.getStatus() == 1) {
                    if (panDetatilResponse.getStatus() == 1) {
                        AppUtils.showSuccess((AppCompatActivity) context, panDetatilResponse.getMessage());
                        verifiedLayout("Your PAN details are sent for verification", "Under Review");
                    } else {
                        AppUtils.showErrorr((AppCompatActivity) context, panDetatilResponse.getMessage());
                    }
                } else {
                    AppUtils.showErrorr((AppCompatActivity) context, panDetatilResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean flag = false;
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri image = data.getData();
                flag = true;
                fileImage = image == null ? "" : AppUtils.getCompressImagePath(image, getActivity());

            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                flag = true;
                fileImage = currentPhotoPath;
            }
        }
        if (fileImage != null) {
            if (!fileImage.equalsIgnoreCase("")) {
                if (flag) {
                    uploadPanImage();
                    return;
                }
            }
        }
        AppUtils.showErrorr((AppCompatActivity) getActivity(), "Can't load this file");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0) == 0) {
            //   mBinding.panVerified.setText("Your PAN Card details are sent for verification.");
        } else if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0) == -1) {

        }
    }


    private void verifiedLayout(String title, String status) {
        mBinding.verifiedLayout.setVisibility(View.VISIBLE);
        mBinding.notVerifiedLayout.setVisibility(View.GONE);
        mBinding.tvPanTitle.setText(title);
        mBinding.tvPanNumber.setText(Html.fromHtml("<u>" + status + "</u>"));
    }

    private void notVerifiedLayout() {
        mBinding.verifiedLayout.setVisibility(View.GONE);
        mBinding.notVerifiedLayout.setVisibility(View.VISIBLE);
    }


    private void openGallery() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), GALLERY_REQUEST_CODE);
    }

    private void openCamera() {
        dispatchTakePictureIntent();

    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getActivity().getPackageName() + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 22) {
            if (grantResults.length != 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showImageSelectionDialog();
                }
            }
        }
    }
}
