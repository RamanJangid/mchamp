package com.rg.mchampgold.app.view.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.CreatePrivateContestRequest;
import com.rg.mchampgold.app.dataModel.CreatePrivateContestResponse;
import com.rg.mchampgold.app.dataModel.WinnerBreakUpData;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.databinding.ActivityWinnerBreakupMatchMangerBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;


public class WinnerBreakupMatchManagerActivity extends AppCompatActivity {

    ActivityWinnerBreakupMatchMangerBinding mBinding;
    CreatePrivateContestRequest request = new CreatePrivateContestRequest();
    ArrayList<WinnerBreakUpData> winnerBreakUpDatas = new ArrayList<>();

    int totalAmout;
    int totalChallenge;
    float percentAR[];
    float amountAR[];

    @Inject
    OAuthRestService oAuthRestService;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_winner_breakup_match_manger);
        MyApplication.getAppComponent().inject(WinnerBreakupMatchManagerActivity.this);
        initialize();
    }

    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.M)
    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.create_contest));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if (getIntent().getExtras() != null) {
            request = (CreatePrivateContestRequest) getIntent().getExtras().getSerializable("request");
        }

        String teams[] = getIntent().getStringExtra("teamvsname").split(" ");
        mBinding.cardViewMatchInfo.tvTeam1.setText(teams[0]);
        mBinding.cardViewMatchInfo.tvTeam2.setText(teams[2]);
        AppUtils.loadImageMatch(mBinding.cardViewMatchInfo.ivTeam1, getIntent().getStringExtra("team1url"));
        AppUtils.loadImageMatch(mBinding.cardViewMatchInfo.ivTeam2, getIntent().getStringExtra("team2url"));

        mBinding.tvContestSize.setText(request.getMaximumUser() + "");
        mBinding.tvEntryfee.setText(getString(R.string.rupee) + String.format("%.2f", Double.parseDouble(request.getEntryfee())));
        mBinding.tvPrizePool.setText(getString(R.string.rupee) + request.getWinAmount());
        showTimer();

        mBinding.btnSet.setOnClickListener(view -> {

            if (!mBinding.totalNumber.getText().toString().trim().equals("")) {
                if (Integer.parseInt(mBinding.totalNumber.getText().toString()) > Integer.parseInt(request.getMaximumUser())) {
                    mBinding.totalNumber.setText("");
                    mBinding.priceCardLL.removeAllViews();
                    LayoutInflater inflater1 = getLayoutInflater();
                    View layout = inflater1.inflate(R.layout.custom_toast1, view.findViewById(R.id.toast_layout_root));
                    TextView text = layout.findViewById(R.id.text);
                    text.setText("Number of winners can't be more than number of participants.");
                    Toast toast = new Toast(getApplicationContext());
                    toast.setGravity(Gravity.BOTTOM, 0, +150);
                    toast.setDuration(2000);
                    toast.setView(layout);
                    toast.show();
                } else {

                    AppUtils.hideKeyboard(this);
                    final int number = Integer.parseInt(mBinding.totalNumber.getText().toString());
                    mBinding.priceCardLL.removeAllViews();

                    percentAR = new float[number];
                    amountAR = new float[number];

                    float per = 100 / Float.parseFloat(String.valueOf(number));
                    float wAmount = Float.parseFloat(request.getWinAmount()) / Float.parseFloat(String.valueOf(number));
                    WinnerBreakUpData data;
                    winnerBreakUpDatas = new ArrayList<>();

                    for (int i = 0; i < number; i++) {
                        percentAR[i] = per;
                        amountAR[i] = wAmount;
                        data = new WinnerBreakUpData();
                        String winAmount = String.format("%.2f", wAmount);
                        data.setWinningAmmount(winAmount);
                        String winPer = String.format("%.2f", percentAR[i]);
                        data.setWinningPer(winPer);
                        winnerBreakUpDatas.add(data);
                    }


                    for (int i = 0; i < number; i++) {

                        final LinearLayout ll = new LinearLayout(WinnerBreakupMatchManagerActivity.this);
                        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        ll.setPadding(16, 8, 16, 8);
                        ll.setLayoutParams(lp);
                        ll.setOrientation(LinearLayout.HORIZONTAL);

                        TextView rank = new TextView(WinnerBreakupMatchManagerActivity.this);
                        LinearLayout.LayoutParams lpRank = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
                        lpRank.weight = 1.0f;
                        lpRank.setMargins(50, 0, 0, 0);
                        rank.setLayoutParams(lpRank);

                        rank.setTextColor(getColor(R.color.textColorGray));

                        rank.setText("#" + (i + 1));
                        ll.addView(rank);

                        final EditText percent = new EditText(WinnerBreakupMatchManagerActivity.this);
                        LinearLayout.LayoutParams lppercent = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
                        lppercent.weight = 1.2f;
                        lppercent.setMargins(0, 0, 20, 0);
                        percent.setLayoutParams(lppercent);
                        percent.setId(i);
                        percent.setGravity(Gravity.CENTER);
                        percent.setBackgroundTintList(getResources().getColorStateList(R.color.colorBackgroundBaseSecond));
                        percent.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        percent.setTag(i + 5000001);
                        percent.setTextSize(12);
                        //  percent.setBackgroundResource(R.drawable.green_border);
                        percent.setTextColor(getColor(R.color.textColorGray));
                        percent.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
                        if (i == (number - 1))
                            percent.setEnabled(false);
                        percent.setText(String.format("%.2f", percentAR[i]));
                        ll.addView(percent);

                        final TextView amount = new TextView(WinnerBreakupMatchManagerActivity.this);
                        LinearLayout.LayoutParams lpamount = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT);
                        lpamount.weight = 1.5f;
                        amount.setLayoutParams(lpamount);
                        amount.setId(i + 1000001);
                        amount.setGravity(Gravity.CENTER);
                        amount.setTextColor(getColor(R.color.colorBlack));
                        amount.setText("₹ " + String.format("%.2f", amountAR[i]));
                        ll.addView(amount);

                       /* View layer = new View(this);
                        LinearLayout.LayoutParams viewParma = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
                        layer.setLayoutParams(viewParma);
                        layer.setBackgroundColor(Color.parseColor("#e1e1e1"));
                        ll.addView(layer);*/



                        final int finalI = i;
                        percent.setOnFocusChangeListener((view1, b) -> {
                            if (!b) {
                                float used = 0.0f, available = 0.0f;
                                int j = 0;
                                for (j = 0; j < finalI; j++) {
                                    used = used + percentAR[j];
                                }

                                available = (100.0f - used) - Float.parseFloat(percent.getText().toString());

                                float left = available / (number - (j + 1));

                                if (left > 0) {
                                    percentAR[finalI] = Float.parseFloat(percent.getText().toString());
                                    amountAR[finalI] = percentAR[finalI] * Float.parseFloat(request.getWinAmount()) / 100;

                                    for (int k = j + 1; k < number; k++) {
                                        percentAR[k] = left;
                                        amountAR[k] = left * Float.parseFloat(request.getWinAmount()) / 100;
                                        ;
                                    }

                                }


                                for (int ii = 0; ii < number; ii++) {
                                    Log.i("new array is", String.valueOf(percentAR[ii]));
                                    try {
                                        EditText ee = mBinding.priceCardLL.findViewById(ii);
                                        ee.setText(String.format("%.2f", percentAR[ii]));
                                        TextView aa = mBinding.priceCardLL.findViewById(ii + 1000001);
                                        aa.setText("₹ " + String.format("%.2f", amountAR[ii]));

                                        winnerBreakUpDatas.get(ii).setWinningAmmount(amountAR[ii] + "");
                                        winnerBreakUpDatas.get(ii).setWinningPer(percentAR[ii] + "");

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        });


                        mBinding.priceCardLL.addView(ll);
                    }

                }
            } else {
                AppUtils.showErrorr(WinnerBreakupMatchManagerActivity.this, "Please enter challenges count");
            }


           /* if(!mBinding.totalNumber.getText().toString().trim().equals("")) {

                if(winnerBreakUpDatas.size()>0)
                    winnerBreakUpDatas.clear();
                int size = Integer.parseInt(mBinding.totalNumber.getText().toString().trim());
             //   int winningPercantage =((Integer.parseInt(request.getWinAmount()))/size)/100;
                int winingAmount = (Integer.parseInt(request.getWinAmount()))/size;
                int winningPercantage = 100/Integer.parseInt(request.getMaximumUser());
              //  float per = 100 / Float.parseFloat(String.valueOf(number));


                for(int i=1;i<=size;i++) {
                    WinnerBreakUpData winnerBreakUpData = new WinnerBreakUpData();
                    winnerBreakUpData.setRank(i+"");
                    winnerBreakUpData.setWinningAmmount(winingAmount+"");
                    winnerBreakUpData.setWinningPer(winningPercantage+"");
                    winnerBreakUpDatas.add(winnerBreakUpData);
                }
                mAdapter.updateData(winnerBreakUpDatas);
            }
            else {
                AppUtils.showErrorr(WinnerBreakupMatchManagerActivity.this,"Please Enter Number of winners");
            }*/

        });
        setRecycleView();


        mBinding.btnCreate.setOnClickListener(view -> {
            if (!mBinding.totalNumber.getText().toString().trim().equals("") && amountAR != null)
                createPrivateContest();
            else
                AppUtils.showErrorr(WinnerBreakupMatchManagerActivity.this, "Please enter total no of winners");
        });
    }

    private void showTimer() {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(getIntent().getStringExtra("date")), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                    mBinding.cardViewMatchInfo.tvMatchTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.cardViewMatchInfo.tvMatchTimer.setText("00h 00m 00s");
                    finish();

                }
            };
            countDownTimer.start();
        } catch (Exception e) {

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

/*
        if (item.getItemId() == R.id.navigation_wallet) {
            startActivity(new Intent(WinnerBreakupMatchManagerActivity.this, MyWalletActivity.class));
            return true;
        }
*/
        return super.onOptionsItemSelected(item);
    }


    public void setRecycleView() {

        totalChallenge = Integer.parseInt(request.getMaximumUser());
        totalAmout = Integer.parseInt(request.getWinAmount());

        //  mAdapter = new WinnerBreakUpItemAdapter(winnerBreakUpDatas,totalAmout,totalChallenge);
      /*  mBinding.rvWinnerBreakup.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.rvWinnerBreakup.setLayoutManager(mLayoutManager);

        // adding custom divider line with padding 3dp
        mBinding.rvWinnerBreakup.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 1));
        mBinding.rvWinnerBreakup.setItemAnimator(new DefaultItemAnimator());
        mBinding.rvWinnerBreakup.setAdapter(mAdapter);*/
    }

    private void createPrivateContest() {


        mBinding.setRefreshing(true);

        float total_amount = 0.0f, total_percent = 0.0f;
        for (int ii = 0; ii < amountAR.length; ii++) {

            try {
                EditText ee = mBinding.priceCardLL.findViewById(ii);
                percentAR[ii] = Float.parseFloat(ee.getText().toString().trim());
                TextView aa = mBinding.priceCardLL.findViewById(ii + 1000001);
                amountAR[ii] = Float.parseFloat(aa.getText().toString().trim().replace("₹ ", ""));
                winnerBreakUpDatas.get(ii).setWinningPer(percentAR[ii] + "");
                winnerBreakUpDatas.get(ii).setWinningAmmount(String.valueOf((totalAmout * Float.parseFloat(winnerBreakUpDatas.get(ii).getWinningPer())) / 100));

              //  Log.e("response", winnerBreakUpDatas.get(ii).getWinningAmmount() + " " + winnerBreakUpDatas.get(ii).getWinningPer());

                total_amount += Float.parseFloat(winnerBreakUpDatas.get(ii).getWinningAmmount());
                total_percent += Float.parseFloat(winnerBreakUpDatas.get(ii).getWinningPer());

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        request.setPricecards(winnerBreakUpDatas);

        if (Math.round(total_amount) == totalAmout && Math.round(total_percent) == 100) {


            CustomCallAdapter.CustomCall<CreatePrivateContestResponse> userFullDetailsResponseCustomCall = oAuthRestService.createContest(request);
            userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<CreatePrivateContestResponse>() {
                @Override
                public void success(Response<CreatePrivateContestResponse> response) {
                    mBinding.setRefreshing(false);
                    CreatePrivateContestResponse findJoinTeamResponse = response.body();
                    if (findJoinTeamResponse != null) {
                        if (findJoinTeamResponse.getStatus() == 1) {
                            //   findJoinTeamResponse.getResult().get(0).get
                            Toast.makeText(WinnerBreakupMatchManagerActivity.this, "Contest Created Successfully", Toast.LENGTH_SHORT).show();
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("challengeId", findJoinTeamResponse.getResult().get(0).getChallengeid());
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        } else {
                            Toast.makeText(WinnerBreakupMatchManagerActivity.this, findJoinTeamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(WinnerBreakupMatchManagerActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void failure(ApiException e) {
                    mBinding.setRefreshing(false);
                }
            });
        } else {
            mBinding.setRefreshing(false);
            AppUtils.showErrorr(WinnerBreakupMatchManagerActivity.this, "Total winning percentage must be equal to 100");

        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //   getMenuInflater().inflate(R.menu.menu_wallet, menu);
        return true;
    }


}
