package com.rg.mchampgold.app.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.TeamPreviewPointRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.TeamPointPreviewResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.PreviewPlayerItemAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityTeamPointPreviewBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;


public class TeamPreviewPointActivity extends AppCompatActivity {

    ActivityTeamPointPreviewBinding activityTeamPointPreviewBinding;

    @Inject
    OAuthRestService oAuthRestService;

    List<Player> listBat = new ArrayList<>();
    List<Player> listBowl = new ArrayList<>();
    List<Player> listAr = new ArrayList<>();
    List<Player> listWK = new ArrayList<>();

    String teamId;
    String challengeId;
    boolean isForLeaderBoard;
    String teamName="";
    String tPoints="";
    String matchKey="";
    private int team1Counts;
    private int team2Counts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(TeamPreviewPointActivity.this);
        AppUtils.setStatusBar(this,getResources().getColor(R.color.match_pitch_bg));
        activityTeamPointPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_team_point_preview);
        initialize();

        LinearLayoutManager horizontalLayoutManagaerr = new LinearLayoutManager(TeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.wickRecyclerView.setLayoutManager(horizontalLayoutManagaerr);

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(TeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.bolRecyclerView.setLayoutManager(horizontalLayoutManagaer);

        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(TeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.allRecyclerView.setLayoutManager(horizontalLayoutManagaer1);

        LinearLayoutManager horizontalLayoutManagaer2 = new LinearLayoutManager(TeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.batRecyclerView.setLayoutManager(horizontalLayoutManagaer2);

        activityTeamPointPreviewBinding.icClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;

           /* case R.id.navigation_wallet:
                openWalletActivity();
                return true;*/
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void initialize() {
    /*    setSupportActionBar(activityTeamPointPreviewBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.team_preview));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
*/


        if(getIntent()!=null && getIntent().getExtras()!=null) {
            teamId=  getIntent().getExtras().getInt("teamId")+"";
            challengeId=  getIntent().getExtras().getInt("challengeId")+"";
            isForLeaderBoard=  getIntent().getExtras().getBoolean("isForLeaderBoard");
            teamName=  getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            tPoints=  getIntent().getExtras().getString("tPoints");
            matchKey=  getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
        }

        activityTeamPointPreviewBinding.icClose.setOnClickListener(view -> finish());
        activityTeamPointPreviewBinding.teamName.setText(teamName);
        // activityTeamPointPreviewBinding.totalPoints.setText("Total Points "+tPoints);
        activityTeamPointPreviewBinding.icRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getPlayerInfo();
            }
        });
        getPlayerInfo();

    }


    private void openNotificationActivity() {
        startActivity(new Intent(TeamPreviewPointActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(TeamPreviewPointActivity.this,MyWalletActivity.class));
    }

    public void getPlayerInfo() {
        activityTeamPointPreviewBinding.setRefreshing(true);
        TeamPreviewPointRequest teamPreviewPointRequest = new TeamPreviewPointRequest();
        teamPreviewPointRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        teamPreviewPointRequest.setChallenge(challengeId);
        teamPreviewPointRequest.setTeamid(teamId);
        CustomCallAdapter.CustomCall<TeamPointPreviewResponse> bankDetailResponseCustomCall = oAuthRestService.getPreviewPoints(teamPreviewPointRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<TeamPointPreviewResponse>() {
            @Override
            public void success(Response<TeamPointPreviewResponse> response) {
                activityTeamPointPreviewBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    TeamPointPreviewResponse teamPointPreviewResponse = response.body();
                    if(teamPointPreviewResponse.getStatus() ==1) {
                        if(teamPointPreviewResponse.getResult().getStatus() ==1) {
                            activityTeamPointPreviewBinding.team.setText(teamPointPreviewResponse.getResult().getTeamname());
                            listWK = teamPointPreviewResponse.getResult().getKeeper();
                            listBat = teamPointPreviewResponse.getResult().getBatsman();
                            listBowl = teamPointPreviewResponse.getResult().getBowler();
                            listAr = teamPointPreviewResponse.getResult().getAllrounder();

                            Double credits = 0.0;
                            double points = 0.0;
                            for (int i = 0; i < listWK.size(); i++) {
                                credits = credits + listWK.get(i).getCredit();
                                points = points + listWK.get(i).getTotalpoints();
                                if (!"team1".equalsIgnoreCase(listWK.get(i).getTeam())) {

                                    team2Counts ++;

                                } else {

                                    team1Counts ++;
                                }
                            }
                            for (int i = 0; i < listBowl.size(); i++) {
                                credits = credits + listBowl.get(i).getCredit();
                                points = points + listBowl.get(i).getTotalpoints();

                                if (!"team1".equalsIgnoreCase(listBowl.get(i).getTeam())) {

                                    team2Counts ++;

                                } else {

                                    team1Counts ++;
                                }
                            }
                            for (int i = 0; i < listBat.size(); i++) {
                                credits = credits + listBat.get(i).getCredit();
                                points = points + listBat.get(i).getTotalpoints();

                                if (!"team1".equalsIgnoreCase(listBat.get(i).getTeam())) {

                                    team2Counts ++;

                                } else {

                                    team1Counts ++;
                                }
                            }
                            for (int i = 0; i < listAr.size(); i++) {
                                credits = credits + listAr.get(i).getCredit();
                                points = points + listAr.get(i).getTotalpoints();

                                if (!"team1".equalsIgnoreCase(listAr.get(i).getTeam())) {

                                    team2Counts ++;

                                } else {

                                    team1Counts ++;
                                }
                            }
                            if (isForLeaderBoard) {
                                activityTeamPointPreviewBinding.title.setText("Total Points:");
                                activityTeamPointPreviewBinding.totalCredits.setText(String.valueOf(points));
                            }else {
                                activityTeamPointPreviewBinding.title.setText("Total Credits:");
                                activityTeamPointPreviewBinding.totalCredits.setText(String.valueOf(credits));
                            }

                            activityTeamPointPreviewBinding.wickRecyclerView.setOnTouchListener((view, motionEvent) -> true);
                            activityTeamPointPreviewBinding.wickRecyclerView.setAdapter(new PreviewPlayerItemAdapter(TeamPreviewPointActivity.this,isForLeaderBoard,listWK,matchKey));

                            activityTeamPointPreviewBinding.batRecyclerView.setOnTouchListener((view, motionEvent) -> true);
                            activityTeamPointPreviewBinding.batRecyclerView.setAdapter(new PreviewPlayerItemAdapter(TeamPreviewPointActivity.this,isForLeaderBoard,listBat,matchKey));

                            activityTeamPointPreviewBinding.bolRecyclerView.setOnTouchListener((view, motionEvent) -> true);
                            activityTeamPointPreviewBinding.bolRecyclerView.setAdapter(new PreviewPlayerItemAdapter(TeamPreviewPointActivity.this,isForLeaderBoard,listBowl,matchKey));

                            activityTeamPointPreviewBinding.allRecyclerView.setOnTouchListener((view, motionEvent) -> true);
                            activityTeamPointPreviewBinding.allRecyclerView.setAdapter(new PreviewPlayerItemAdapter(TeamPreviewPointActivity.this,isForLeaderBoard,listAr,matchKey));
                        }else {
                            AppUtils.showErrorr(TeamPreviewPointActivity.this,teamPointPreviewResponse.getMessage());
                        }

                    }
                    else {
                        AppUtils.showErrorr(TeamPreviewPointActivity.this,teamPointPreviewResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                activityTeamPointPreviewBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

}
