package com.rg.mchampgold.app.view.basketball;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.TeamPreviewPointRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.TeamPointPreviewResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityBasketballTeamPreviewBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;


public class BasketBallTeamPreviewPointActivity extends AppCompatActivity {

    ActivityBasketballTeamPreviewBinding activityTeamPointPreviewBinding;

    @Inject
    OAuthRestService oAuthRestService;

    List<Player> listPG = new ArrayList<>();
    List<Player> listSG = new ArrayList<>();
    List<Player> listSF = new ArrayList<>();
    List<Player> listPF = new ArrayList<>();
    List<Player> listC = new ArrayList<>();


    String teamId;
    String challengeId;
    boolean isForLeaderBoard;
    String teamName="";
    String tPoints="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(BasketBallTeamPreviewPointActivity.this);
        activityTeamPointPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_basketball_team_preview);
        initialize();

        LinearLayoutManager horizontalLayoutManagaerr = new LinearLayoutManager(BasketBallTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.pgRecyclerView.setLayoutManager(horizontalLayoutManagaerr);

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(BasketBallTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.sgRecyclerView.setLayoutManager(horizontalLayoutManagaer);

        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(BasketBallTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.sfRecyclerView.setLayoutManager(horizontalLayoutManagaer1);

        LinearLayoutManager horizontalLayoutManagaer2 = new LinearLayoutManager(BasketBallTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.pfRecyclerView.setLayoutManager(horizontalLayoutManagaer2);

        LinearLayoutManager horizontalLayoutManagaer3 = new LinearLayoutManager(BasketBallTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.centreRecyclerView.setLayoutManager(horizontalLayoutManagaer3);

        activityTeamPointPreviewBinding.icClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;
        } else if (itemId == R.id.navigation_wallet) {
            openWalletActivity();
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void initialize() {
    /*    setSupportActionBar(activityTeamPointPreviewBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.team_preview));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
*/


        if(getIntent()!=null && getIntent().getExtras()!=null) {
            teamId=  getIntent().getExtras().getInt("teamId")+"";
            challengeId=  getIntent().getExtras().getInt("challengeId")+"";
            isForLeaderBoard=  getIntent().getExtras().getBoolean("isForLeaderBoard");
            teamName=  getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            tPoints=  getIntent().getExtras().getString("tPoints");
        }

        activityTeamPointPreviewBinding.icClose.setOnClickListener(view -> finish());
        activityTeamPointPreviewBinding.teamName.setText(teamName);
        // activityTeamPointPreviewBinding.totalPoints.setText("Total Points "+tPoints);

        getPlayerInfo();

    }


    private void openNotificationActivity() {
        startActivity(new Intent(BasketBallTeamPreviewPointActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(BasketBallTeamPreviewPointActivity.this, MyWalletActivity.class));
    }

    public void getPlayerInfo() {
        activityTeamPointPreviewBinding.setRefreshing(true);
        TeamPreviewPointRequest teamPreviewPointRequest = new TeamPreviewPointRequest();
        teamPreviewPointRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        teamPreviewPointRequest.setChallenge(challengeId);
        teamPreviewPointRequest.setTeamid(teamId);
        teamPreviewPointRequest.setSport_key(Constants.TAG_BASKETBALL);
        CustomCallAdapter.CustomCall<TeamPointPreviewResponse> bankDetailResponseCustomCall = oAuthRestService.getPreviewPoints(teamPreviewPointRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<TeamPointPreviewResponse>() {
            @Override
            public void success(Response<TeamPointPreviewResponse> response) {
                activityTeamPointPreviewBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    TeamPointPreviewResponse teamPointPreviewResponse = response.body();
                    if(teamPointPreviewResponse.getStatus() ==1) {
                        activityTeamPointPreviewBinding.team.setText(teamPointPreviewResponse.getResult().getTeamname());
                        listPG = teamPointPreviewResponse.getResult().getPgList();
                        listSG = teamPointPreviewResponse.getResult().getSgList();
                        listSF = teamPointPreviewResponse.getResult().getSmallForwardList();
                        listPF = teamPointPreviewResponse.getResult().getPowerForwardList();
                        listC = teamPointPreviewResponse.getResult().getCentreList();
                        Double credits=0.0;
                        for (int i=0; i<listPG.size(); i++){
                            credits=credits+listPG.get(i).getCredit();
                        }
                        for (int i=0; i<listSG.size(); i++){
                            credits=credits+listSG.get(i).getCredit();
                        }
                        for (int i=0; i<listSF.size(); i++){
                            credits=credits+listSF.get(i).getCredit();
                        }
                        for (int i=0; i<listPF.size(); i++){
                            credits=credits+listPF.get(i).getCredit();
                        }
                        for (int i=0; i<listC.size(); i++){
                            credits=credits+listC.get(i).getCredit();
                        }
                        activityTeamPointPreviewBinding.totalCredits.setText(String.valueOf(credits)+"/100");
                        activityTeamPointPreviewBinding.pgRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.pgRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listPG,matchKey));

                        activityTeamPointPreviewBinding.sgRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.sgRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listSG,matchKey));

                        activityTeamPointPreviewBinding.sfRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.sfRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listSF,matchKey));

                        activityTeamPointPreviewBinding.pfRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.pfRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listPF,matchKey));

                        activityTeamPointPreviewBinding.centreRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.centreRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listC,matchKey));

                    }
                    else {
                        AppUtils.showErrorr(BasketBallTeamPreviewPointActivity.this,teamPointPreviewResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                activityTeamPointPreviewBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

}
