package com.rg.mchampgold.app.view.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

/*import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;*/
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.Limit;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.SelectedPlayer;
import com.rg.mchampgold.app.view.adapter.SlectedUnSelectedPlayerAdapter;
import com.rg.mchampgold.app.view.fragment.CreateTeamPlayerFragment;
import com.rg.mchampgold.app.view.fragment.PlayingStatusSheetFragment;
import com.rg.mchampgold.app.viewModel.GetPlayerDataViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityCreateTeamBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static bolts.Task.delay;

public class CreateTeamActivity extends AppCompatActivity /*implements OnShowcaseEventListener*/ {
    ActivityCreateTeamBinding mBinding;
    SlectedUnSelectedPlayerAdapter mSlectedUnSelectedPlayerAdapter;
    private GetPlayerDataViewModel createTeamViewModel;
    String teamName;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;

    ArrayList<Player> team1wkList = new ArrayList<>();
    ArrayList<Player> team2wkList = new ArrayList<>();
    ArrayList<Player> wkListTemp = new ArrayList<>();
    ArrayList<Player> wkList = new ArrayList<>();
    ArrayList<Player> team1bolList = new ArrayList<>();
    ArrayList<Player> team2bolList = new ArrayList<>();
    ArrayList<Player> bolListTemp = new ArrayList<>();
    ArrayList<Player> bolList = new ArrayList<>();
    ArrayList<Player> team1batList = new ArrayList<>();
    ArrayList<Player> team2batList = new ArrayList<>();
    ArrayList<Player> batListTemp = new ArrayList<>();
    ArrayList<Player> batList = new ArrayList<>();
    ArrayList<Player> team1arList = new ArrayList<>();
    ArrayList<Player> team2arList = new ArrayList<>();
    ArrayList<Player> arListTemp = new ArrayList<>();
    ArrayList<Player> arList = new ArrayList<>();

    ArrayList<Player> allPlayerList = new ArrayList<>();

    private static int WK = 1;
    private static int BAT = 2;
    private static int AR = 3;
    private static int BOWLER = 4;
    public SelectedPlayer selectedPlayer;
    public boolean exeedCredit = false;
    public static Activity createTeamAc;
    ArrayList<Player> selectedList = new ArrayList<>();
    int teamId;
    Context context;
    boolean isFromEditOrClone;
    String headerText;
    boolean isShowTimer;
    int selectedType = WK;
    int counterValue = 0;
	String sport_key="CRICKET";
    Limit limit;
    public int totalPlayerCount;
    public int maxTeamPlayerCount;
    double totalCredit;
    public boolean isPointSorted = true, isCreditSorted = true, isPLayerSorted = true;
    public String playerStatus = "";
    private int teamCount = 1;
    private Contest contest;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        createTeamViewModel = GetPlayerDataViewModel.create(CreateTeamActivity.this);
        MyApplication.getAppComponent().inject(createTeamViewModel);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_team);
        context = CreateTeamActivity.this;
        //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        initialize();
        //}
        createTeamAc = this;
        MyApplication.isSelectedTeam = "ALL";

        mBinding.llFillter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialog dialog = new BottomSheetDialog(context);
                dialog.setContentView(R.layout.filterbyteam_layout);
                ImageView close = dialog.findViewById(R.id.close);
                TextView team1 = dialog.findViewById(R.id.team1);
                TextView team2 = dialog.findViewById(R.id.team2);
                RelativeLayout team1Click = dialog.findViewById(R.id.team1Click);
                RelativeLayout team2Click = dialog.findViewById(R.id.team2Click);
                RelativeLayout allClick = dialog.findViewById(R.id.allClick);
                RadioButton team1RadioButton = dialog.findViewById(R.id.team1RadioButton);
                RadioButton team2RadioButton = dialog.findViewById(R.id.team2RadioButton);
                RadioButton allRadioButton = dialog.findViewById(R.id.allRadioButton);
                String teams[] = teamVsName.split(" ");
                team1.setText(teams[0]);
                team2.setText(teams[2]);
                if (MyApplication.isSelectedTeam.equalsIgnoreCase("ALl")) {
                    allRadioButton.setChecked(true);
                } else if (MyApplication.isSelectedTeam.equalsIgnoreCase("Team1")) {
                    team1RadioButton.setChecked(true);
                } else {
                    team2RadioButton.setChecked(true);
                }
                team1Click.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        team1RadioButton.setChecked(true);
                        team2RadioButton.setChecked(false);
                        allRadioButton.setChecked(false);
                        MyApplication.isSelectedTeam = "Team1";
                        filterByTeam();
                        dialog.dismiss();
                    }
                });
                team2Click.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        team1RadioButton.setChecked(false);
                        allRadioButton.setChecked(false);
                        team2RadioButton.setChecked(true);
                        MyApplication.isSelectedTeam = "Team2";
                        filterByTeam();
                        dialog.dismiss();
                    }
                });
                allClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        team1RadioButton.setChecked(false);
                        team2RadioButton.setChecked(false);
                        allRadioButton.setChecked(true);
                        MyApplication.isSelectedTeam = "All";
                        filterByTeam();
                        dialog.dismiss();
                    }
                });
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    public void filterByTeam() {
        if (MyApplication.isSelectedTeam.equalsIgnoreCase("ALl")) {
            wkList = wkListTemp;
            batList = batListTemp;
            arList = arListTemp;
            bolList = bolListTemp;
        } else if (MyApplication.isSelectedTeam.equalsIgnoreCase("Team1")) {
            wkList = team1wkList;
            batList = team1batList;
            arList = team1arList;
            bolList = team1bolList;
        } else {
            wkList = team2wkList;
            batList = team2batList;
            arList = team2arList;
            bolList = team2bolList;
        }

        Fragment fragment = getSupportFragmentManager()
                .findFragmentByTag("android:switcher:" + mBinding.viewPager.getId() + ":" + mBinding.viewPager.getCurrentItem());
        CreateTeamPlayerFragment createTeamPlayerFragment = ((CreateTeamPlayerFragment) fragment);
        if (mBinding.tabLayout.getSelectedTabPosition() == 0) {
            mBinding.tabLayout.getTabAt(0).setText("WK " + (selectedPlayer.getWk_selected() == 0 ? "" : "(" + selectedPlayer.getWk_selected() + ")"));
            createTeamPlayerFragment.playerItemAdapter.updateData(wkList, selectedType);

        } else if (mBinding.tabLayout.getSelectedTabPosition() == 1) {
            mBinding.tabLayout.getTabAt(1).setText("BAT " + (selectedPlayer.getBat_selected() == 0 ? "" : "(" + selectedPlayer.getBat_selected() + ")"));

            createTeamPlayerFragment.playerItemAdapter.updateData(batList, selectedType);

        } else if (mBinding.tabLayout.getSelectedTabPosition() == 2) {
            mBinding.tabLayout.getTabAt(2).setText("AR " + (selectedPlayer.getAr_selected() == 0 ? "" : "(" + selectedPlayer.getAr_selected() + ")"));
            createTeamPlayerFragment.playerItemAdapter.updateData(arList, selectedType);
        } else if (mBinding.tabLayout.getSelectedTabPosition() == 3) {
            mBinding.tabLayout.getTabAt(3).setText("BOWL " + (selectedPlayer.getBowl_selected() == 0 ? "" : "(" + selectedPlayer.getBowl_selected() + ")"));
            createTeamPlayerFragment.playerItemAdapter.updateData(bolList, selectedType);
        }
    }


    void initialize() {
        AppUtils.setStatusBar(CreateTeamActivity.this, getResources().getColor(R.color.accent));
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.create_team));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            if (getIntent().getExtras().getBoolean("isFromEditOrClone")) {
                isFromEditOrClone = getIntent().getExtras().getBoolean("isFromEditOrClone");
                selectedList = (ArrayList<Player>) getIntent().getSerializableExtra("selectedList");
                teamId = getIntent().getExtras().getInt(Constants.KEY_TEAM_ID);
            }

            teamName = getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT, "");
			sport_key = getIntent().getExtras().getString(Constants.SPORT_KEY,"BASKETBALL");
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER, false);
            teamCount = getIntent().getExtras().getInt(Constants.KEY_TEAM_COUNT, 1);
            contest = (Contest) getIntent().getSerializableExtra(Constants.KEY_CONTEST_DATA);
        }

        setTeamNames();
        String teams[] = teamVsName.split(" ");
        mBinding.tvTeam1.setText(teams[0]);
        mBinding.tvTeam2.setText(teams[2]);
        AppUtils.loadImageMatch(mBinding.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.ivTeam2, teamSecondUrl);

        if (isShowTimer) {
            showTimer();
        } else {
            if (headerText.equalsIgnoreCase("Winner Declared")) {
                mBinding.tvTimeTimer.setText("Winner Declared");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    mBinding.tvTimeTimer.setTextColor(getResources().getColor(R.color.color_match_declared));
                }
            } else if (headerText.equalsIgnoreCase("In Progress")) {
                mBinding.tvTimeTimer.setText("In Progress");
                mBinding.tvTimeTimer.setTextColor(getResources().getColor(R.color.color_match_progress));
            }

            else
            {
                mBinding.tvTimeTimer.setText(headerText);
                mBinding.tvTimeTimer.setTextColor(getResources().getColor(R.color.color_match_progress));
            }
        }


        selectedPlayer = new SelectedPlayer();


        mBinding.tvPlayerCountPick.setText("Pick "+selectedPlayer.getWk_min_count()+" Wicket-Keepers");

        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FragmentManager fm = getSupportFragmentManager();

                switch (position) {
                    case 0:
                        selectedType = WK;
                        mBinding.tabLayout.getTabAt(0).setText("WK " + (selectedPlayer.getWk_selected() == 0 ? "" : "(" + selectedPlayer.getWk_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick "+selectedPlayer.getWk_min_count()+" Wicket-Keepers");
                        if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                            ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(wkList, WK);
                        break;
                    case 1:
                        selectedType = BAT;
                        mBinding.tabLayout.getTabAt(1).setText("BAT " + (selectedPlayer.getBat_selected() == 0 ? "" : "(" + selectedPlayer.getBat_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick "+selectedPlayer.getBat_mincount()+"-"+selectedPlayer.getBat_maxcount()+" Batsmens");
                        if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                            ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(batList, BAT);
                        break;
                    case 2:
                        selectedType = AR;
                        mBinding.tabLayout.getTabAt(2).setText("AR " + (selectedPlayer.getAr_selected() == 0 ? "" : "(" + selectedPlayer.getAr_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick "+selectedPlayer.getAr_mincount()+"-"+selectedPlayer.getAr_maxcount()+" All-Rounders");
                        if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                            ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(arList, AR);
                        break;
                    case 3:
                        selectedType = BOWLER;
                        mBinding.tabLayout.getTabAt(3).setText("BOWL " + (selectedPlayer.getBowl_selected() == 0 ? "" : "(" + selectedPlayer.getBowl_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick "+selectedPlayer.getBowl_mincount()+"-"+selectedPlayer.getBowl_maxcount()+" Bowlers");
                        if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                            ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(bolList, BOWLER);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mSlectedUnSelectedPlayerAdapter = new SlectedUnSelectedPlayerAdapter(0,getApplicationContext());
        createTeamData();
        mBinding.rvSelected.setAdapter(mSlectedUnSelectedPlayerAdapter);
        setupRecyclerView();

        mBinding.btnCreateTeam.setOnClickListener(v -> {
            if (selectedPlayer.getSelectedPlayer() == selectedPlayer.getTotal_player_count()) {
                ArrayList<Player> sellectedList = new ArrayList<>();


                for (Player player : wkListTemp) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }

                for (Player player : batListTemp) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }


                for (Player player : arListTemp) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }


                for (Player player : bolListTemp) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }


                Intent intent = new Intent(CreateTeamActivity.this, ChooseCandVCActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                intent.putExtra("playerList", sellectedList);
                intent.putExtra(Constants.KEY_TEAM_ID, teamId);
                intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
                intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, isShowTimer);
                intent.putExtra(Constants.SPORT_KEY,sport_key);
                intent.putExtra(Constants.KEY_TEAM_COUNT,teamCount);
                intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
                intent.putExtra("localTeamCount", String.valueOf(selectedPlayer.getLocalTeamplayerCount()));
                intent.putExtra("visitorTeamCount", String.valueOf(selectedPlayer.getVisitorTeamPlayerCount()));

                if (isFromEditOrClone)
                    intent.putExtra("isFromEditOrClone", true);
                else
                    intent.putExtra("isFromEditOrClone", false);

                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
                startActivity(intent);
            } else {
                showTeamValidation("Please select "+selectedPlayer.getTotal_player_count()+" players.");
            }

        });


        mBinding.ivTeamPreview.setOnClickListener(view -> {
            Intent intent = new Intent(CreateTeamActivity.this, TeamPreviewActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_TEAM_NAME, teamName);
            ArrayList<Player> selectedWkList = new ArrayList<>();
            ArrayList<Player> selectedBatLiSt = new ArrayList<>();
            ArrayList<Player> selectedArList = new ArrayList<>();
            ArrayList<Player> selectedBowlList = new ArrayList<>();
            for (Player player : wkListTemp) {
                if (player.isIsSelected())
                    selectedWkList.add(player);
            }

            for (Player player : batListTemp) {
                if (player.isIsSelected())
                    selectedBatLiSt.add(player);
            }

            for (Player player : arListTemp) {
                if (player.isIsSelected())
                    selectedArList.add(player);
            }

            for (Player player : bolListTemp) {
                if (player.isIsSelected())
                    selectedBowlList.add(player);
            }

            intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedWkList);
            intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedBatLiSt);
            intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedArList);
            intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedBowlList);

            startActivity(intent);
        });
        mBinding.ivInfoSelection.setOnClickListener(view -> {
            showSelectionPopupDialog();
        });

        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (mBinding.tabLayout.getSelectedTabPosition() == 0) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-4 Wicket-Keepers");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 1) {
                    mBinding.tvPlayerCountPick.setText("Pick 3-6 Batsmen");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 2) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-4 All-Rounders");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 3) {
                    mBinding.tvPlayerCountPick.setText("Pick 3-6 Bowlers");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mBinding.tvPoints.setOnClickListener(view -> {

            Fragment fragment = getSupportFragmentManager()
                    .findFragmentByTag("android:switcher:" + mBinding.viewPager.getId() + ":" + mBinding.viewPager.getCurrentItem());
            if (fragment != null) {
                CreateTeamPlayerFragment createTeamPlayerFragment = ((CreateTeamPlayerFragment)fragment);

                if (isPointSorted) {
                    sortWithPoints(false);
                    createTeamPlayerFragment.sortWithPoints(false);
                    isPointSorted = false;
                    mBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPointSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    sortWithPoints(true);
                    createTeamPlayerFragment.sortWithPoints(true);
                    isPointSorted = true;
                    mBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPointSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                isCreditSorted = true;
                isPLayerSorted = true;
            }

            /*FragmentManager fm = getSupportFragmentManager();
            if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment) {
                CreateTeamPlayerFragment createTeamPlayerFragment = ((CreateTeamPlayerFragment) fm.getFragments().get(0));


            }*/
        });

        mBinding.tvCredits.setOnClickListener(view -> {

            Fragment fragment = getSupportFragmentManager()
                    .findFragmentByTag("android:switcher:" + mBinding.viewPager.getId() + ":" + mBinding.viewPager.getCurrentItem());
            if (fragment != null) {
                CreateTeamPlayerFragment createTeamPlayerFragment = ((CreateTeamPlayerFragment)fragment);

                if (isCreditSorted) {
                    sortWithCredit(false);
                    createTeamPlayerFragment.sortWithCredit(false);
                    isCreditSorted = false;
                    mBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivCreditSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    isCreditSorted = true;
                    sortWithCredit(true);
                    createTeamPlayerFragment.sortWithCredit(true);
                    mBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivCreditSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                isPointSorted = true;
                isPLayerSorted = true;
            }
        });

        mBinding.llPlayer.setOnClickListener(view -> {

            Fragment fragment = getSupportFragmentManager()
                    .findFragmentByTag("android:switcher:" + mBinding.viewPager.getId() + ":" + mBinding.viewPager.getCurrentItem());
            if (fragment != null) {

                CreateTeamPlayerFragment createTeamPlayerFragment = ((CreateTeamPlayerFragment)fragment);
                if (isPLayerSorted) {
                    sortByPlayer(false);
                    createTeamPlayerFragment.sortByPlayer(false);
                    isPLayerSorted = false;
                    mBinding.ivPlayerSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPlayerSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    isPLayerSorted = true;
                    sortByPlayer(true);
                    createTeamPlayerFragment.sortByPlayer(true);
                    mBinding.ivPlayerSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPlayerSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                isPointSorted = true;
                isCreditSorted = true;
            }
        });


        mBinding.llPlayingStatus.setOnClickListener(view -> {
            showPlayingStatusDialog();
        });
    }

    private void sortWithPoints(boolean flag) {
        if (flag) {
            Collections.sort(wkList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(batList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(arList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(bolList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
        } else {
            Collections.sort(wkList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(batList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(arList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(bolList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
        }
    }


    private void sortWithCredit(boolean flag) {
        if (flag) {
            Collections.sort(wkList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(batList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(arList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(bolList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
        } else {
            Collections.sort(wkList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(batList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(arList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(bolList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
        }
    }

    private void sortByPlayer(boolean flag) {
        if (flag) {
            Collections.sort(wkList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(batList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(arList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(bolList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
        } else {
            Collections.sort(wkList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(batList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(arList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(bolList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
        }
    }

    private void showPlayingStatusDialog() {
        PlayingStatusSheetFragment playingStatusSheetFragment = new PlayingStatusSheetFragment(context);
        playingStatusSheetFragment.show(getSupportFragmentManager(), playingStatusSheetFragment.getTag());
    }

    public void changePlayerStatus(String playNotPlayText) {
        playerStatus = playNotPlayText;
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()) instanceof CreateTeamPlayerFragment)
            ((CreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition())).changePlayingStatus();
    }

    private void showSelectionPopupDialog() {
        Dialog dialog = new Dialog(CreateTeamActivity.this);
        dialog.setContentView(R.layout.selection_rules_popup);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.findViewById(R.id.tv_got_it).setOnClickListener(view1 -> dialog.dismiss());
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_team, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();/*   case R.id.navigation_notification:
                openNotificationActivity();
                return true;*//*case R.id.navigation_wallet:
                openWalletActivity();
                return true;
*/
        if (itemId == R.id.how_to_play) {
            AppUtils.openWebViewActivity(getString(R.string.how_to_play), MyApplication.how_to_play_url);
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openNotificationActivity() {
        startActivity(new Intent(CreateTeamActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(CreateTeamActivity.this, MyWalletActivity.class));

    }

    private void setupRecyclerView() {
        getData();
    }


    private void getData() {
        MyTeamRequest request = new MyTeamRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
		request.setSport_key(sport_key);
        createTeamViewModel.loadPlayerListRequest(request);
        createTeamViewModel.getPlayerList().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getResult().size() > 0) {
                        allPlayerList = arrayListResource.getData().getResult();
                        limit = arrayListResource.getData().getLimit();

                        totalCredit = limit.getTotalCredits();
                        totalPlayerCount = limit.getTotalPlayers();
                        mBinding.totalPlayers.setText("/"+totalPlayerCount);
                        maxTeamPlayerCount = limit.getTeamMaxPlayer();

                        mBinding.tvMsgError.setText("Select maximum "+maxTeamPlayerCount+" Players from each team");
                        for (Player player : allPlayerList) {
                            if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_KEEP)) {
                                wkList.add(player);
                                wkListTemp.add(player);
                                if (player.getTeam().equalsIgnoreCase("team1")) {
                                    team1wkList.add(player);
                                } else {
                                    team2wkList.add(player);
                                }
                            } else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BAT)) {
                                batList.add(player);
                                batListTemp.add(player);
                                if (player.getTeam().equalsIgnoreCase("team1")) {
                                    team1batList.add(player);
                                } else {
                                    team2batList.add(player);
                                }
                            } else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BOL)) {
                                bolList.add(player);
                                bolListTemp.add(player);
                                if (player.getTeam().equalsIgnoreCase("team1")) {
                                    team1bolList.add(player);
                                } else {
                                    team2bolList.add(player);
                                }
                            } else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ALL_R)) {
                                arList.add(player);
                                arListTemp.add(player);
                                if (player.getTeam().equalsIgnoreCase("team1")) {
                                    team1arList.add(player);
                                } else {
                                    team2arList.add(player);
                                }
                            }
                        }
                        if (selectedList.size() > 0) {
                            for (int i = 0; i < allPlayerList.size(); i++) {
                                for (Player player : selectedList) {
                                    if (player.getId()==allPlayerList.get(i).getId()) {
                                        allPlayerList.get(i).setSelected(true);
                                        if (player.getCaptain() == 1)
                                            allPlayerList.get(i).setCaptain(true);
                                        if (player.getVicecaptain() == 1)
                                            allPlayerList.get(i).setVcCaptain(true);
                                    }
                                }
                            }
                            setSelectedCountForEditOrClone();
                        }

                        setupViewPager(mBinding.viewPager);
                        sortWithCredit(false);
                        isCreditSorted = false;
                        isPointSorted = true;
                        isPLayerSorted = true;


                       /* if (!MyApplication.tinyDB.getBoolean(Constants.SKIP_CREATETEAM_INSTRUCTION, false)) {

                            callIntroductionScreen(
                                    R.id.tabLayout,
                                    "Player Category",
                                    "Select a balanced team to help you win",
                                    ShowcaseView.BELOW_SHOWCASE
                            );
                            MyApplication.tinyDB.putBoolean(Constants.SKIP_CREATETEAM_INSTRUCTION, true);
                        }
*/

                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }


    private void setSelectedCountForEditOrClone() {
        int countWK = 0;
        int countBAT = 0;
        int countBALL = 0;
        int countALL = 0;
        int totalCount = 0;
        int team1Count = 0;
        int team2Count = 0;
        double usedCredit = 0;

        for (Player player : allPlayerList) {
            if (player.isIsSelected()) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_KEEP)) {
                    countWK++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_BAT)) {
                    countBAT++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_BOL)) {
                    countBALL++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_ALL_R)) {
                    countALL++;
                }

                if (player.getTeam().equalsIgnoreCase("team1")) {
                    team1Count++;
                }

                if (player.getTeam().equalsIgnoreCase("team2")) {
                    team2Count++;
                }

                totalCount++;
                usedCredit += player.getCredit();
            }
        }


        selectedPlayer.setWk_selected(countWK);
        selectedPlayer.setBat_selected(countBAT);
        selectedPlayer.setAr_selected(countALL);
        selectedPlayer.setBowl_selected(countBALL);
        selectedPlayer.setSelectedPlayer(selectedPlayer.getTotal_player_count());
        selectedPlayer.setLocalTeamplayerCount(team1Count);
        selectedPlayer.setVisitorTeamPlayerCount(team2Count);
        selectedPlayer.setTotal_credit(usedCredit);

        updateTeamData(
                0,
                selectedPlayer.getWk_selected(),
                selectedPlayer.getBat_selected(),
                selectedPlayer.getAr_selected(),
                selectedPlayer.getBowl_selected(),
                selectedPlayer.getSelectedPlayer(),
                selectedPlayer.getLocalTeamplayerCount(),
                selectedPlayer.getVisitorTeamPlayerCount(),
                selectedPlayer.getTotal_credit());
    }


    public void setTeamNames() {
        if (teamVsName != null) {
            String teams[] = teamVsName.split("Vs");
            mBinding.tvTeamNameFirst.setText(teams[0]);
            mBinding.tvTeamNameSecond.setText(teams[1]);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void showTimer() {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                   /* if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d " + twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    } else {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    }*/
                    mBinding.tvTimeTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");

                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.tvTimeTimer.setText("00h 00m 00s");
                    startActivity(new Intent(CreateTeamActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).putExtra("matchFinish",true));                }
            };
            countDownTimer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void openPlayerInfoActivity(String playerId, String playerName, String team, String image, boolean is_added, int pos, int type) {
        Intent intent = new Intent(this, PlayerInfoActivity.class);
        intent.putExtra("matchKey", matchKey);
        intent.putExtra("playerId", playerId);
        intent.putExtra("playerName", playerName);
        intent.putExtra("team", team);
        intent.putExtra("image", image);
        intent.putExtra("flag", "0");
        intent.putExtra("is_added", is_added);
        intent.putExtra("pos", pos);
        intent.putExtra("type", type);
        intent.putExtra(Constants.SPORT_KEY, sport_key);
        startActivity(intent);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment) {
            mFragmentList.add(fragment);
            //mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "WK " + (selectedPlayer.getWk_selected() == 0 ? "" : "(" + selectedPlayer.getWk_selected() + ")");
                case 1:
                    return "BAT " + (selectedPlayer.getBat_selected() == 0 ? "" : "(" + selectedPlayer.getBat_selected() + ")");
                case 2:
                    return "AR " + (selectedPlayer.getAr_selected() == 0 ? "" : "(" + selectedPlayer.getAr_selected() + ")");
                case 3:
                    return "BOWL " + (selectedPlayer.getBowl_selected() == 0 ? "" : "(" + selectedPlayer.getBowl_selected() + ")");
            }
            return "";
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new CreateTeamPlayerFragment(allPlayerList, wkList, WK));
        adapter.addFrag(new CreateTeamPlayerFragment(allPlayerList, batList, BAT));
        adapter.addFrag(new CreateTeamPlayerFragment(allPlayerList, arList, AR));
        adapter.addFrag(new CreateTeamPlayerFragment(allPlayerList, bolList, BOWLER));
        viewPager.setAdapter(adapter);
        mBinding.tabLayout.setupWithViewPager(viewPager);
    }


    public void onPlayerClick(boolean isSelect, int position, int type) {

        if (type == WK) {
            double player_credit;

            if (isSelect) {
                if (selectedPlayer.getSelectedPlayer() >= selectedPlayer.getTotal_player_count()) {
                    showTeamValidation("You can choose maximum "+selectedPlayer.getTotal_player_count()+" players.");

                    return;
                }
                if (selectedPlayer.getWk_selected() > selectedPlayer.getWk_max_count()) {
                    showTeamValidation("You can select only "+selectedPlayer.getWk_max_count()+" Wicket-Keeper.");
                    return;
                }

                if (wkList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                }

                if (selectedPlayer.getWk_selected() < selectedPlayer.getWk_max_count()) {
                    if (selectedPlayer.getSelectedPlayer() < selectedPlayer.getTotal_player_count()) {
                        if (selectedPlayer.getWk_selected() < selectedPlayer.getWk_min_count() || selectedPlayer.getExtra_player() > 0) {

                            int extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getWk_selected() >= selectedPlayer.getWk_min_count()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = wkList.get(position).getCredit();


                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > 100) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (wkList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            wkList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected() + 1,
                                    selectedPlayer.getBat_selected(),
                                    selectedPlayer.getAr_selected(),
                                    selectedPlayer.getBowl_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );

                        } else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {

                if (selectedPlayer.getWk_selected() > 0) {
                    player_credit = wkList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;

                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getWk_selected() > selectedPlayer.getWk_min_count()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (wkList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    wkList.get(position).setSelected(isSelect);

                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected() - 1,
                            selectedPlayer.getBat_selected(),
                            selectedPlayer.getAr_selected(),
                            selectedPlayer.getBowl_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                    if (selectedPlayer.getWk_selected() < selectedPlayer.getWk_min_count()) {
                        showTeamValidation("Pick "+selectedPlayer.getWk_min_count()+"-"+selectedPlayer.getWk_max_count()+" Wicket-Keeper");
                    }
                }
            }
        } else if (type == AR) {
            double player_credit;
            if (isSelect) {
                if (selectedPlayer.getSelectedPlayer() >= selectedPlayer.getTotal_player_count()) {
                    showTeamValidation("You can choose maximum "+selectedPlayer.getTotal_player_count()+" players.");

                    return;
                }

                if (selectedPlayer.getAr_selected() > selectedPlayer.getAr_maxcount()) {
                    showTeamValidation("You can select only "+selectedPlayer.getAr_maxcount()+" All-Rounders.");
                    return;
                }

                if (arList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                }


                if (selectedPlayer.getAr_selected() < selectedPlayer.getAr_maxcount()) {
                    if (selectedPlayer.getSelectedPlayer() < selectedPlayer.getTotal_player_count()) {
                        if (selectedPlayer.getAr_selected() < selectedPlayer.getAr_mincount() || selectedPlayer.getExtra_player() > 0) {

                            int extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getAr_selected() >= selectedPlayer.getAr_mincount()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = arList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > 100) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (arList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            arList.get(position).setSelected(isSelect);
                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected(),
                                    selectedPlayer.getBat_selected(),
                                    selectedPlayer.getAr_selected() + 1,
                                    selectedPlayer.getBowl_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        } else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getAr_selected() > 0) {
                    player_credit = arList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;


                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getAr_selected() > selectedPlayer.getAr_mincount()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (arList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    arList.get(position).setSelected(isSelect);

                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected(),
                            selectedPlayer.getBat_selected(),
                            selectedPlayer.getAr_selected() - 1,
                            selectedPlayer.getBowl_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                    if (selectedPlayer.getAr_selected() < selectedPlayer.getAr_mincount()) {
                        showTeamValidation("Pick "+selectedPlayer.getAr_mincount()+"-"+selectedPlayer.getAr_maxcount()+" All-Rounders");
                    }
                }
            }
        } else if (type == BAT) {
            double player_credit;


            if (isSelect) {

                if (selectedPlayer.getSelectedPlayer() >= selectedPlayer.getTotal_player_count()) {
                    showTeamValidation("You can choose maximum "+selectedPlayer.getTotal_player_count()+" players");
                    return;
                }

                if (selectedPlayer.getBat_selected() >= selectedPlayer.getBat_maxcount()) {
                    showTeamValidation("You can select only "+selectedPlayer.getBat_maxcount()+" Batsmen");
                    return;
                }

                if (batList .get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                }


                if (selectedPlayer.getBat_selected() < selectedPlayer.getBat_maxcount()) {
                    if (selectedPlayer.getSelectedPlayer() < selectedPlayer.getTotal_player_count()) {
                        if (selectedPlayer.getBat_selected() < selectedPlayer.getBat_mincount() || selectedPlayer.getExtra_player() > 0) {

                            int extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getBat_selected() >= selectedPlayer.getBat_mincount()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = batList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > 100) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");

                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (batList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            batList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected(),
                                    selectedPlayer.getBat_selected() + 1,
                                    selectedPlayer.getAr_selected(),
                                    selectedPlayer.getBowl_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        } else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getBat_selected() > 0) {
                    player_credit = batList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;

                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getBat_selected() > selectedPlayer.getBat_mincount()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (batList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    batList.get(position).setSelected(isSelect);
                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected(),
                            selectedPlayer.getBat_selected() - 1,
                            selectedPlayer.getAr_selected(),
                            selectedPlayer.getBowl_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                    if (selectedPlayer.getBat_selected() < selectedPlayer.getBat_mincount()) {
                        showTeamValidation("Pick "+selectedPlayer.getBat_mincount()+"-"+selectedPlayer.getBat_maxcount()+" Batsmen");
                    }
                }
            }
        } else if (type == BOWLER) {
            double player_credit = 0.0;

            if (isSelect) {

                if (selectedPlayer.getSelectedPlayer() >= selectedPlayer.getTotal_player_count()) {
                    showTeamValidation("You can choose maximum "+selectedPlayer.getTotal_player_count()+" players.");

                    return;
                }

                if (selectedPlayer.getBowl_selected() >= selectedPlayer.getBowl_maxcount()) {
                    showTeamValidation("You can select only "+selectedPlayer.getBowl_maxcount()+" Bowlers.");
                    return;
                }

                if (bolList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamplayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" players from each team.");
                        return;
                    }
                }


                if (selectedPlayer.getBowl_selected() < selectedPlayer.getBowl_maxcount()) {
                    if (selectedPlayer.getSelectedPlayer() < selectedPlayer.getTotal_player_count()) {
                        if (selectedPlayer.getBowl_selected() < selectedPlayer.getBowl_mincount() || selectedPlayer.getExtra_player() > 0) {

                            int extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getBowl_selected() >= selectedPlayer.getBowl_mincount()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = bolList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > 100) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");

                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (bolList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            bolList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getWk_selected(),
                                    selectedPlayer.getBat_selected(),
                                    selectedPlayer.getAr_selected(),
                                    selectedPlayer.getBowl_selected() + 1,
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        } else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {

                if (selectedPlayer.getBowl_selected() > 0) {
                    player_credit = bolList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;


                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getBowl_selected() > selectedPlayer.getBowl_mincount()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (bolList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamplayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    bolList.get(position).setSelected(isSelect);
                    updateTeamData(
                            extra,
                            selectedPlayer.getWk_selected(),
                            selectedPlayer.getBat_selected(),
                            selectedPlayer.getAr_selected(),
                            selectedPlayer.getBowl_selected() - 1,
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                    if (selectedPlayer.getBowl_selected() < selectedPlayer.getBowl_mincount()) {
                        showTeamValidation("Pick "+selectedPlayer.getBowl_mincount()+"-"+selectedPlayer.getBowl_maxcount()+" Bowlers");
                    }
                }
            }
        }
    }

    public void createTeamData() {

        selectedPlayer.setExtra_player(3);
        selectedPlayer.setWk_min_count(1);
        selectedPlayer.setWk_max_count(4);
        selectedPlayer.setWk_selected(0);
        selectedPlayer.setBat_mincount(3);
        selectedPlayer.setBat_maxcount(6);
        selectedPlayer.setBat_selected(0);
        selectedPlayer.setBowl_mincount(3);
        selectedPlayer.setBowl_maxcount(6);
        selectedPlayer.setBowl_selected(0);
        selectedPlayer.setAr_mincount(1);
        selectedPlayer.setAr_maxcount(4);
        selectedPlayer.setAr_selected(0);
        selectedPlayer.setSelectedPlayer(0);
        selectedPlayer.setTotal_player_count(11);
        selectedPlayer.setLocalTeamMaxplayerCount(6);
        selectedPlayer.setVisitorTeamMaxplayerCount(6);
        selectedPlayer.setLocalTeamplayerCount(0);
        selectedPlayer.setVisitorTeamPlayerCount(0);
        selectedPlayer.setTotal_credit(0.0);
        updateUi();
    }

    public void updateTeamData(
            int extra_player,
            int wk_selected,
            int bat_selected,
            int ar_selected,
            int bowl_selected,
            int selectPlayer,
            int localTeamplayerCount,
            int visitorTeamPlayerCount,
            double total_credit) {
        exeedCredit = false;
        selectedPlayer.setExtra_player(extra_player);
        selectedPlayer.setWk_selected(wk_selected);
        selectedPlayer.setBat_selected(bat_selected);
        selectedPlayer.setAr_selected(ar_selected);
        selectedPlayer.setBowl_selected(bowl_selected);
        selectedPlayer.setSelectedPlayer(selectPlayer);
        selectedPlayer.setLocalTeamplayerCount(localTeamplayerCount);
        selectedPlayer.setVisitorTeamPlayerCount(visitorTeamPlayerCount);
        selectedPlayer.setTotal_credit(total_credit);
        mBinding.tvLocalTeam.setText("("+localTeamplayerCount+")");
        mBinding.tvVisitorTeam.setText("("+visitorTeamPlayerCount+")");
        updateUi();
    }

    private void updateUi() {
        mBinding.tvSelectedPlayer.setText(selectedPlayer.getSelectedPlayer()+"");

        if (selectedPlayer.getLocalTeamplayerCount()+selectedPlayer.getVisitorTeamPlayerCount() == selectedPlayer.getTotal_player_count())
        {
            mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_golden);
            mBinding.btnCreateTeam.setTextColor(getResources().getColor(R.color.black));
        }
        else
        {
            mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_grey);
            mBinding.btnCreateTeam.setTextColor(getResources().getColor(R.color.white));
        }

      /*  if (isFromEditOrClone) {
            mBinding.tvUsedCredit.setText(selectedPlayer.getTotal_credit()+"");
        } else {
            mBinding.tvUsedCredit.setText(selectedPlayer.getTotal_credit()+"");
        }*/

        if (selectedPlayer.getTotal_credit() < 0)
            selectedPlayer.setTotal_credit(0);

        String creditLeft = String.valueOf(100 - selectedPlayer.getTotal_credit());

        mBinding.tvUsedCredit.setText(creditLeft +"");

        mBinding.tvTeamCountFirst.setText(selectedPlayer.getLocalTeamplayerCount() + "");
        mBinding.tvTeamCountSecond.setText(selectedPlayer.getVisitorTeamPlayerCount() + "");

        mSlectedUnSelectedPlayerAdapter.update(selectedPlayer.getSelectedPlayer());
        /*if (mBinding.tabLayout.getTabCount() > 0) {
            callFragmentRefresh();
        }*/


        if (mBinding.tabLayout.getTabCount() > 0) {

            if (mBinding.viewPager.getAdapter() != null)
                mBinding.viewPager.getAdapter().notifyDataSetChanged();

            if (MyApplication.playerItemAdapter!=null) {
                if (mBinding.tabLayout.getSelectedTabPosition() == 0) {
                    mBinding.tabLayout.getTabAt(0).setText("WK " + (selectedPlayer.getWk_selected() == 0 ? "" : "(" + selectedPlayer.getWk_selected() + ")"));
                    MyApplication.playerItemAdapter.updateData(wkList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 1) {
                    mBinding.tabLayout.getTabAt(1).setText("BAT " + (selectedPlayer.getBat_selected() == 0 ? "" : "(" + selectedPlayer.getBat_selected() + ")"));
                    MyApplication.playerItemAdapter.updateData(batList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 2) {
                    mBinding.tabLayout.getTabAt(2).setText("AR " + (selectedPlayer.getAr_selected() == 0 ? "" : "(" + selectedPlayer.getAr_selected() + ")"));
                    MyApplication.playerItemAdapter.updateData(arList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 3) {
                    mBinding.tabLayout.getTabAt(3).setText("BOWL " + (selectedPlayer.getBowl_selected() == 0 ? "" : "(" + selectedPlayer.getBowl_selected() + ")"));
                    MyApplication.playerItemAdapter.updateData(bolList, selectedType);
                }
            }
        }
    }

    public void callFragmentRefresh() {
        if (mBinding.viewPager.getAdapter() != null)
            mBinding.viewPager.getAdapter().notifyDataSetChanged();

        FragmentManager fm = getSupportFragmentManager();
        switch (selectedType) {
            case 1:
                mBinding.tabLayout.getTabAt(0).setText("WK " + (selectedPlayer.getWk_selected() == 0 ? "" : "(" + selectedPlayer.getWk_selected() + ")"));
                if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                    ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(wkList, selectedType);
                break;
            case 2:
                mBinding.tabLayout.getTabAt(1).setText("BAT " + (selectedPlayer.getBat_selected() == 0 ? "" : "(" + selectedPlayer.getBat_selected() + ")"));
                if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                    ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(batList, selectedType);
                break;
            case 3:
                mBinding.tabLayout.getTabAt(2).setText("AR " + (selectedPlayer.getAr_selected() == 0 ? "" : "(" + selectedPlayer.getAr_selected() + ")"));
                if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                    ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(arList, selectedType);
                break;
            case 4:
                mBinding.tabLayout.getTabAt(3).setText("BOWL " + (selectedPlayer.getBowl_selected() == 0 ? "" : "(" + selectedPlayer.getBowl_selected() + ")"));
                if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment)
                    ((CreateTeamPlayerFragment) fm.getFragments().get(0)).refresh(bolList, selectedType);
                break;
        }

    }

    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public void minimumPlayerWarning() {
        if (selectedPlayer.getBowl_selected() < selectedPlayer.getBowl_mincount()) {
            showTeamValidation("You must select at least "+selectedPlayer.getBowl_mincount()+" Bowlers.");
        } else if (selectedPlayer.getBat_selected() < selectedPlayer.getBat_mincount()) {
            showTeamValidation("You must select at least "+selectedPlayer.getBat_mincount()+" Batsmen.");
        } else if (selectedPlayer.getAr_selected() < selectedPlayer.getAr_mincount()) {
            showTeamValidation("You must select at least "+selectedPlayer.getAr_mincount()+" All-Rounders.");
        }
        else if (selectedPlayer.getWk_selected() < selectedPlayer.getWk_min_count()) {
            showTeamValidation("You must select at least "+selectedPlayer.getWk_min_count()+" Wicket-Keepers.");
        }
    }


    public void showTeamValidation(String mesg) {
        AppUtils.showErrorr(this, mesg);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem clearTeam = menu.findItem(R.id.clear_team);
        clearTeam.setVisible(true);
        LinearLayout rootView = (LinearLayout) clearTeam.getActionView();
        rootView.setOnClickListener(view -> {
          clearTeam();
        });


        return true;

    }


   void clearTeam()
    {
        if (selectedPlayer.getSelectedPlayer() > 0) {

            Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.layout_clear_popup);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

            final TextView tvCancelTeam = dialog.findViewById(R.id.tv_cancel_team);
            final TextView tvClearTeam = dialog.findViewById(R.id.tv_clear_team);

            tvCancelTeam.setOnClickListener(view1 -> dialog.dismiss());


            tvClearTeam.setOnClickListener(view12 -> {
                createTeamData();
                for (int i = 0; i < allPlayerList.size(); i++) {
                    allPlayerList.get(i).setSelected(false);
                }
                mBinding.tvLocalTeam.setText("0");
                mBinding.tvVisitorTeam.setText("0");
                mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                isCreditSorted = true;
                isPLayerSorted = true;
                isPointSorted = true;
                setupViewPager(mBinding.viewPager);
                dialog.dismiss();
            });
            dialog.show();
        }
        else {
            AppUtils.showErrorr(this, "No player selected to clear.");
        }
    }

    @Override
    public void onBackPressed() {
        if (selectedPlayer.getSelectedPlayer() > 0) {
            DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        CreateTeamActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to discard team changes?").setPositiveButton("DISCARD", dialogClickListener)

                    .setNegativeButton("CANCEL", dialogClickListener).show();
        } else {
            CreateTeamActivity.super.onBackPressed();
        }
    }



/*

    void callIntroductionScreen(int target, String title, String description, int abovE_SHOWCASE) {


        ShowcaseView showcaseView = new ShowcaseView.Builder(this).withNewStyleShowcase()
                .setTarget(new ViewTarget(target, this))
                .setContentTitle(title)
                .setContentText(description)
                .setStyle(counterValue == 0 ? R.style.CustomShowcaseTheme : R.style.CustomShowcaseTheme)
                .hideOnTouchOutside().setShowcaseEventListener(this)
                .build();

        showcaseView.forceTextPosition(abovE_SHOWCASE);
        counterValue = counterValue + 1;

        new Handler().postDelayed(() -> showcaseView.hideButton(), 2500);

    }


    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {

        switch (counterValue) {
            case 1: {
                callIntroductionScreen(
                        R.id.ll_credit,
                        "Credit Counter",
                        "Use 100 credits to pick your players", ShowcaseView.BELOW_SHOWCASE
                );
                break;
            }
            case 2: {
                callIntroductionScreen(
                        R.id.ll_players,
                        "Player Counter",
                        "Pick 11 players to create your team"
                        ,
                        ShowcaseView.BELOW_SHOWCASE
                );
                break;
            }

        }

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

    }
*/


}
