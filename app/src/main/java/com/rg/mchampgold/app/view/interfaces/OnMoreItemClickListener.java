package com.rg.mchampgold.app.view.interfaces;

public interface OnMoreItemClickListener {
    void onMoreItemClick(int position, String title);
}
