package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.PlayerInfoMatchesItem;
import com.rg.mchampgold.app.dataModel.PlayerInfoRequest;
import com.rg.mchampgold.app.dataModel.PlayerInfoResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.PlayerInfoItemAdapter;
import com.rg.mchampgold.app.view.adapter.PlayerItemAdapter;
import com.rg.mchampgold.app.view.basketball.BasketballPlayerItemAdapter;
import com.rg.mchampgold.app.view.football.FootballPlayerItemAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityPlayerInfoBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class PlayerInfoActivity extends AppCompatActivity {
    ActivityPlayerInfoBinding mBinding;
    PlayerInfoItemAdapter mAdapter;
    String matchKey;
    String playerId;
    String playerName;
    String team;
    String sportKey ="";
    ArrayList<PlayerInfoMatchesItem> list = new ArrayList<>();
    String image_url;
    String sport_key;
    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(PlayerInfoActivity.this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_player_info);
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.topToolbar);
        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString("matchKey");
            playerId = getIntent().getExtras().getString("playerId");
            playerName = getIntent().getExtras().getString("playerName");


            team = getIntent().getExtras().getString("team");
			sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
            if (getIntent().hasExtra("image")) {
                image_url = getIntent().getExtras().getString("image");
                AppUtils.loadPlayerImage(mBinding.ivPlayer, image_url);
            }
        }

        if (getIntent().hasExtra("flag")) {
            mBinding.btnAddInTeam.setVisibility(getIntent().getStringExtra("flag").equals("0") ? View.VISIBLE : View.GONE);
        }

        if (getIntent().hasExtra("is_added")) {


            boolean is_added = getIntent().getBooleanExtra("is_added", false);
            if (is_added) {
                mBinding.btnAddInTeam.setBackgroundResource(R.drawable.rounded_corner_filled_color_primary);
                mBinding.btnAddInTeam.setText("Remove");
                mBinding.btnAddInTeam.setTextColor(getResources().getColor(R.color.white));

            } else {
                mBinding.btnAddInTeam.setBackgroundResource(R.drawable.rounded_corner_filled_color_primary);
                mBinding.btnAddInTeam.setText("Add in Team");
                mBinding.btnAddInTeam.setTextColor(getResources().getColor(R.color.white));
            }

            mBinding.btnAddInTeam.setOnClickListener(view ->
            {
                if (is_added) {
                    if (sportKey.equalsIgnoreCase("CRICKET")) {
                    PlayerItemAdapter.listener.onPlayerClick(false, getIntent().getIntExtra("pos", 0), getIntent().getIntExtra("type", 0));
                    }else if (sportKey.equalsIgnoreCase("FOOTBALL")){
                        FootballPlayerItemAdapter.listener.onPlayerClick(false, getIntent().getIntExtra("pos", 0), getIntent().getIntExtra("type", 0));
                    }else {
                        BasketballPlayerItemAdapter.listener.onPlayerClick(false, getIntent().getIntExtra("pos", 0), getIntent().getIntExtra("type", 0));
                    }
                    mBinding.btnAddInTeam.setBackgroundResource(R.drawable.rounded_corner_filled_orange);
                    mBinding.btnAddInTeam.setText("Add in Team");
                    mBinding.btnAddInTeam.setTextColor(getResources().getColor(R.color.colorWhite));
                } else {
                    if (sportKey.equalsIgnoreCase("CRICKET")) {
                    PlayerItemAdapter.listener.onPlayerClick(true, getIntent().getIntExtra("pos", 0), getIntent().getIntExtra("type", 0));
                    }else if (sportKey.equalsIgnoreCase("FOOTBALL")){
                        FootballPlayerItemAdapter.listener.onPlayerClick(true, getIntent().getIntExtra("pos", 0), getIntent().getIntExtra("type", 0));
                    }else {
                        BasketballPlayerItemAdapter.listener.onPlayerClick(true, getIntent().getIntExtra("pos", 0), getIntent().getIntExtra("type", 0));
                    }
                    mBinding.btnAddInTeam.setBackgroundResource(R.drawable.rounded_corner_filled_dark_blue);
                    mBinding.btnAddInTeam.setText("Remove");
                    mBinding.btnAddInTeam.setTextColor(getResources().getColor(R.color.colorWhite));
                }
                finish();
            });


        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        mBinding.ivCloseInfo.setOnClickListener(v -> {
            finish();
        });


      /*  if (!"team1".equalsIgnoreCase(team)) {
            mBinding.ivPlayer.setImageResource(R.drawable.player_team_two);

        } else {
            mBinding.ivPlayer.setImageResource(R.drawable.player_team_one);
        }*/


        setupRecyclerView();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        mAdapter = new PlayerInfoItemAdapter(list);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
        getPlayerInfo();


    }

    public void getPlayerInfo() {
        mBinding.setRefreshing(true);
        PlayerInfoRequest playerInfoRequest = new PlayerInfoRequest();
        playerInfoRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        playerInfoRequest.setMatchKey(matchKey);
        playerInfoRequest.setPlayerid(playerId);
        playerInfoRequest.setSport_key(sportKey);
        CustomCallAdapter.CustomCall<PlayerInfoResponse> bankDetailResponseCustomCall = oAuthRestService.getPlayerInfo(playerInfoRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<PlayerInfoResponse>() {
            @Override
            public void success(Response<PlayerInfoResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    PlayerInfoResponse playerInfoResponse = response.body();
                    if (playerInfoResponse.getStatus() == 1) {
                        mAdapter.updateData(playerInfoResponse.getResult().getMatches());
                        mBinding.setPlayerInfoResult(playerInfoResponse.getResult());

//                        mBinding.nationalityTV.setText(playerInfoResponse.getResult().getTeams());
//                        mBinding.positionTv.setText(playerInfoResponse.getResult().getPlayerrole());
                    } else {
                        AppUtils.showErrorr(PlayerInfoActivity.this, playerInfoResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


}
