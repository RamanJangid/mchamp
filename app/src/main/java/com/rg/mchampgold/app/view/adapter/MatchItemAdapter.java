package com.rg.mchampgold.app.view.adapter;

import android.app.Activity;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.Match;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.fragment.home.HomeFragment;
import com.rg.mchampgold.app.view.interfaces.OnMatchItemClickListener;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.RecyclerItemMatchBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MatchItemAdapter extends RecyclerView.Adapter<MatchItemAdapter.ViewHolder> {

    private List<Match> moreInfoDataList;
    private OnMatchItemClickListener listener;
    private AppCompatActivity activity;
    HomeFragment homeFragment;

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemMatchBinding binding;

        ViewHolder(RecyclerItemMatchBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public MatchItemAdapter(Activity activity, List<Match> moreInfoDataList, OnMatchItemClickListener listener,HomeFragment homeFragment) {
        this.moreInfoDataList = moreInfoDataList;
        this.listener = listener;
        this.homeFragment = homeFragment;
        this.activity = (AppCompatActivity) activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemMatchBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_match,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.binding.tvTimer.setText("");
        Match match = moreInfoDataList.get(position);
        holder.binding.setMoreInfo(match);

        AppUtils.loadImageMatch(holder.binding.ivTeamFirst, match.getTeam1logo());
        AppUtils.loadImageMatch(holder.binding.ivTeamSecond, match.getTeam2logo());

        if (moreInfoDataList.get(position).getIsLeaderboard() == 1) {
            holder.binding.ivLeaderboard.setVisibility(View.VISIBLE);
        }else {
            holder.binding.ivLeaderboard.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(moreInfoDataList.get(position).getTossText())) {
            holder.binding.llTossText.setVisibility(View.GONE);
        } else {
            holder.binding.llTossText.setVisibility(View.VISIBLE);
            holder.binding.tvTossText.setText(moreInfoDataList.get(position).getTossText().trim());
        }

        holder.binding.tvMatchInfo.setText(match.getMatchStatus());

        if (match.getLineup() == 1) {
            holder.binding.tvLinupOut.setVisibility(View.VISIBLE);
//            holder.binding.llLinup.setBackgroundResource(R.drawable.linup_res);
            //   timeLeft.setTextColor(context.getResources().getColor(R.color.color_green));
            //    tvLinup.setVisibility(View.VISIBLE);

        } else {
            holder.binding.tvLinupOut.setVisibility(View.INVISIBLE);
//            holder.binding.llLinup.setBackgroundResource(R.drawable.linup_res_white);
            //  timeLeft.setTextColor(context.getResources().getColor(R.color.color_red));
            //  tvLinup.setVisibility(View.GONE);
        }


        if (match.getMatchStatusKey() == Constants.KEY_LIVE_MATCH) {
            // holder.binding.tvMatchInfo.setText("In Progress");
        } else if (match.getMatchStatusKey() == Constants.KEY_FINISHED_MATCH) {
            //  holder.binding.tvMatchInfo.setText("Winner Declared");

        } else if (match.getMatchStatusKey() == Constants.KEY_UPCOMING_MATCH) {
            String eDate = "2017-09-10 12:05:00";
            if (match.getLaunchStatus().equalsIgnoreCase("launched")) {
                holder.binding.tvTimer.setVisibility(View.VISIBLE);
                holder.binding.tvMatchInfo.setVisibility(View.GONE);
                Calendar c = Calendar.getInstance();
                int hour = c.get(Calendar.HOUR_OF_DAY);
                final int minute = c.get(Calendar.MINUTE);
                int sec = c.get(Calendar.SECOND);
                int mYear1 = c.get(Calendar.YEAR);
                int mMonth1 = c.get(Calendar.MONTH);
                int mDay1 = c.get(Calendar.DAY_OF_MONTH);


                Date startDate = null, endDate = null;
                String sDate = "2017-09-08 10:05:00";

                sDate = mYear1 + "-" + (mMonth1 + 1) + "-" + mDay1 + " " + hour + ":" + minute + ":" + sec;
                eDate = match.getTimeStart();

                //  Log.e("start_date", sDate.toString());
                //  Log.e("end_date", eDate.toString());


                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    startDate = dateFormat.parse(sDate);
                    endDate = dateFormat.parse(eDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                final long diffInMs = endDate.getTime() - startDate.getTime();

                if (holder.binding.tvTimer.getText().toString().trim().equals("")) {
                    try {
                        CountDownTimer countDownTimer = new CountDownTimer(diffInMs, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {

                                long time = millisUntilFinished;
                                long seconds = time / 1000 % 60;
                                long minutes = (time / (1000 * 60)) % 60;
                                long diffHours = (time / (60 * 60 * 1000));
                                //  if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                                //    holder.binding.tvTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d : " + twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                                // } else {

                                // Log.e("time", TimeUnit.MILLISECONDS.toHours(millisUntilFinished) + "");
                                holder.binding.tvTimer.setText(twoDigitString(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)) + "h : " + twoDigitString(minutes) + "m :" + twoDigitString(seconds) + "s");

                                // }

/*                         holder.binding.tvTimer.setText(String.format(String.format("%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished)) + "h : "
                           + String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished))) + "m : "
                           + String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + "s"));*/

                            }

                            private String twoDigitString(long number) {
                                if (number == 0) {
                                    return "00";
                                } else if (number / 10 == 0) {
                                    return "0" + number;
                                }
                                return String.valueOf(number);
                            }

                            @Override
                            public void onFinish() {
                                //   if (position < moreInfoDataList.size()) {
                                moreInfoDataList.remove(match);
                                updateData(moreInfoDataList);
//                                homeFragment.callAdapter();
                                //  }

                            }
                        };
                        countDownTimer.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                holder.binding.tvMatchInfo.setVisibility(View.GONE);
                holder.binding.tvTimer.setVisibility(View.VISIBLE);
                holder.binding.tvTimer.setText(match.getStartDate());
            }
        }

        holder.itemView.setOnClickListener(v -> {
            if (match.getMatchStatusKey() == Constants.KEY_LIVE_MATCH) {
                listener.onMatchItemClick(match.getMatchkey(),
                        match.getTeam1display() + " Vs " + match.getTeam2display(),
                        match.getTeam1logo(),
                        match.getTeam2logo(), match.getMatchStatus(),position, match.getIsLeaderboard(), match.getSeries());

            } else if (match.getMatchStatusKey() == Constants.KEY_FINISHED_MATCH) {
                listener.onMatchItemClick(match.getMatchkey(),
                        match.getTeam1display() + " Vs " + match.getTeam2display(),
                        match.getTeam1logo(),
                        match.getTeam2logo(), match.getMatchStatus(),position, match.getIsLeaderboard(), match.getSeries());
            } else if (match.getMatchStatusKey() == Constants.KEY_UPCOMING_MATCH) {
                if (match.getLaunchStatus().equalsIgnoreCase("launched")) {
                    listener.onMatchItemClick(match.getMatchkey(),
                            match.getTeam1display() + " Vs " + match.getTeam2display(),
                            match.getTeam1logo(),
                            match.getTeam2logo(), match.getTimeStart(),position,
                            match.getIsLeaderboard(),
                            match.getSeries());

                } else {
                    AppUtils.showErrorr(activity, "To be Available Soon");
                }
            }
        });
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }


    public void updateData(List<Match> list) {
        this.moreInfoDataList = list;
        // searchItemFilteredList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}