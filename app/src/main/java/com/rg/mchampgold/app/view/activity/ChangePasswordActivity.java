package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ChangePasswordRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.NormalResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityChangePasswordBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class ChangePasswordActivity extends AppCompatActivity {

    ActivityChangePasswordBinding mBinding;

    @Inject
    OAuthRestService oAuthRestService;
    String oldPassword = "";
    String newPassword = "";
    String cnfPassword = "";

    int passwordNotVisibleForOld = 0;
    int passwordNotVisibleForNew = 0;
    int passwordNotVisibleForConfirm = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        MyApplication.getAppComponent().inject(ChangePasswordActivity.this);
        initialize();
    }


    void initialize() {

        AppUtils.disableCopyPaste(mBinding.etNewPassword);
        AppUtils.disableCopyPaste(mBinding.etOldPassword);
        AppUtils.disableCopyPaste(mBinding.etConfirmPassword);

        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mBinding.ivOldShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBinding.etOldPassword.getText().toString().trim().equals("")) {
                    if (passwordNotVisibleForOld == 0) {
                        mBinding.etOldPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        mBinding.ivOldShowPassword.setImageResource(R.drawable.ic_password_view);
                        passwordNotVisibleForOld = 1;
                    } else {
                        mBinding.ivOldShowPassword.setImageResource(R.drawable.view);
                        mBinding.etOldPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        passwordNotVisibleForOld = 0;
                    }
                    mBinding.etOldPassword.setSelection(mBinding.etOldPassword.length());

                }
            }
        });

        mBinding.ivNewShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBinding.etNewPassword.getText().toString().trim().equals("")) {
                    if (passwordNotVisibleForNew == 0) {
                        mBinding.etNewPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        mBinding.ivNewShowPassword.setImageResource(R.drawable.ic_password_view);
                        passwordNotVisibleForNew = 1;
                    } else {
                        mBinding.ivNewShowPassword.setImageResource(R.drawable.view);
                        mBinding.etNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        passwordNotVisibleForNew = 0;
                    }
                    mBinding.etNewPassword.setSelection(mBinding.etNewPassword.length());

                }
            }
        });


        mBinding.ivConfirmShowPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBinding.etConfirmPassword.getText().toString().trim().equals("")) {
                    if (passwordNotVisibleForConfirm == 0) {
                        mBinding.etConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        mBinding.ivConfirmShowPassword.setImageResource(R.drawable.ic_password_view);
                        passwordNotVisibleForConfirm = 1;
                    } else {
                        mBinding.ivConfirmShowPassword.setImageResource(R.drawable.view);
                        mBinding.etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        passwordNotVisibleForConfirm = 0;
                    }
                    mBinding.etConfirmPassword.setSelection(mBinding.etConfirmPassword.length());

                }
            }
        });


        mBinding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPassword = mBinding.etNewPassword.getText().toString().trim();
                cnfPassword = mBinding.etConfirmPassword.getText().toString().trim();
                oldPassword = mBinding.etOldPassword.getText().toString().trim();
                if (oldPassword.length() < 4)
                    Toast.makeText(ChangePasswordActivity.this, "Please enter valid old password", Toast.LENGTH_SHORT).show();
                else if (!AppUtils.isValidPassword(newPassword))
                    Toast.makeText(ChangePasswordActivity.this, "Password must contain minimum 8 characters at least 1 Lower Case, 1 Upper Case, 1 Number and 1 Special Character", Toast.LENGTH_SHORT).show();
                else if (newPassword.length() < 8)
                    Toast.makeText(ChangePasswordActivity.this, "Please enter valid new password", Toast.LENGTH_SHORT).show();
                else if (cnfPassword.length() < 8)
                    Toast.makeText(ChangePasswordActivity.this, "Please enter valid confirm password", Toast.LENGTH_SHORT).show();
                else if (!cnfPassword.equalsIgnoreCase(newPassword))
                    Toast.makeText(ChangePasswordActivity.this, "Your new password and confirm password must be same", Toast.LENGTH_SHORT).show();
                else {
                    changePassword();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void changePassword() {
        mBinding.setRefreshing(true);
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();
        changePasswordRequest.setUserId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        changePasswordRequest.setOldpassword(mBinding.etOldPassword.getText().toString().trim());
        changePasswordRequest.setNewpassword(mBinding.etConfirmPassword.getText().toString().trim());
        CustomCallAdapter.CustomCall<NormalResponse> userFullDetailsResponseCustomCall = oAuthRestService.changePassword(changePasswordRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<NormalResponse>() {
            @Override
            public void success(Response<NormalResponse> response) {
                mBinding.setRefreshing(false);
                NormalResponse normalResponse = response.body();
                if (normalResponse.getStatus() == 1) {
                    finish();
                }
                Toast.makeText(ChangePasswordActivity.this, normalResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }
}
