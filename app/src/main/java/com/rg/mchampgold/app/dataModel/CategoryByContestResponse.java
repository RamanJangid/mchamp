package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CategoryByContestResponse{

	@SerializedName("result")
	private Cateogori result;

	@SerializedName("status")
	private int status;

	@SerializedName("message")
	private String message;

	public void setResult(Cateogori result){
		this.result = result;
	}

	public Cateogori getResult(){
		return result;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CategoryByContestResponse{" + 
			"result = '" + result + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}


	public String getMessage() {
		return message == null?"":message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}