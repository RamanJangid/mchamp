package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class TopScoreItems {

    @SerializedName("id")
    int id;
    @SerializedName("player_id")
    int player_id;
    @SerializedName("batsman_id")
    int batsman_id;
    @SerializedName("name")
    String name;
    @SerializedName("matchkey")
    String matchkey;
    @SerializedName("team_id")
    int team_id;
    @SerializedName("run")
    int run;
    @SerializedName("ball")
    int ball;
    @SerializedName("four")
    int four;
    @SerializedName("six")
    int six;
    @SerializedName("app_points")
    int app_points;
    @SerializedName("created_at")
    String created_at;
    @SerializedName("updated_at")
    String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(int player_id) {
        this.player_id = player_id;
    }

    public int getBatsman_id() {
        return batsman_id;
    }

    public void setBatsman_id(int batsman_id) {
        this.batsman_id = batsman_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatchkey() {
        return matchkey;
    }

    public void setMatchkey(String matchkey) {
        this.matchkey = matchkey;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public int getRun() {
        return run;
    }

    public void setRun(int run) {
        this.run = run;
    }

    public int getBall() {
        return ball;
    }

    public void setBall(int ball) {
        this.ball = ball;
    }

    public int getFour() {
        return four;
    }

    public void setFour(int four) {
        this.four = four;
    }

    public int getSix() {
        return six;
    }

    public void setSix(int six) {
        this.six = six;
    }

    public int getApp_points() {
        return app_points;
    }

    public void setApp_points(int app_points) {
        this.app_points = app_points;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
