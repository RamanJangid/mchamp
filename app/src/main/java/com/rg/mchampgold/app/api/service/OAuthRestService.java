package com.rg.mchampgold.app.api.service;


import com.rg.mchampgold.app.api.request.AddPaymentRequest;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.ChangePasswordRequest;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.request.GetOrderIdRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.request.LoginRequest;
import com.rg.mchampgold.app.api.request.OtpVerfiyRequest;
import com.rg.mchampgold.app.api.request.RegisterRequest;
import com.rg.mchampgold.app.api.request.SocialLoginRequest;
import com.rg.mchampgold.app.api.request.TeamNameUpdateRequest;
import com.rg.mchampgold.app.api.request.TeamPreviewPointRequest;
import com.rg.mchampgold.app.api.response.LoginResponse;
import com.rg.mchampgold.app.api.response.RegisterResponse;
import com.rg.mchampgold.app.api.response.ScratchHistoryResponse;
import com.rg.mchampgold.app.api.response.UserOAuthResponse;
import com.rg.mchampgold.app.dataModel.AddCashBannerListResponse;
import com.rg.mchampgold.app.dataModel.AddPaymentValueResponse;
import com.rg.mchampgold.app.dataModel.AllVerifyResponse;
import com.rg.mchampgold.app.dataModel.BalanceResponse;
import com.rg.mchampgold.app.dataModel.BankDetailResponse;
import com.rg.mchampgold.app.dataModel.BankVerifyRequest;
import com.rg.mchampgold.app.dataModel.BankVerifyResponse;
import com.rg.mchampgold.app.dataModel.BannerListResponse;
import com.rg.mchampgold.app.dataModel.CategoryByContestResponse;
import com.rg.mchampgold.app.dataModel.CheckVersionCodeResponse;
import com.rg.mchampgold.app.dataModel.ContestResponse;
import com.rg.mchampgold.app.dataModel.CreatePrivateContestRequest;
import com.rg.mchampgold.app.dataModel.CreatePrivateContestResponse;
import com.rg.mchampgold.app.dataModel.FindJoinTeamResponse;
import com.rg.mchampgold.app.dataModel.GetNotificationResponse;
import com.rg.mchampgold.app.dataModel.GetUserFullDetailsResponse;
import com.rg.mchampgold.app.dataModel.GetWinnerScoreCardResponse;
import com.rg.mchampgold.app.dataModel.ImageUploadResponse;
import com.rg.mchampgold.app.dataModel.InvestmentResponse;
import com.rg.mchampgold.app.dataModel.JoinByContestCodeResponse;
import com.rg.mchampgold.app.dataModel.JoinContestByCodeRequest;
import com.rg.mchampgold.app.dataModel.JoinContestResponse;
import com.rg.mchampgold.app.dataModel.LiveMatchesResponse;
import com.rg.mchampgold.app.dataModel.LiveMatchesScoreCardResponse;
import com.rg.mchampgold.app.dataModel.MyBalanceResponse;
import com.rg.mchampgold.app.dataModel.MyTransactionHistoryResponse;
import com.rg.mchampgold.app.dataModel.NormalResponse;
import com.rg.mchampgold.app.dataModel.OrderIdResponse;
import com.rg.mchampgold.app.dataModel.PanDetatilResponse;
import com.rg.mchampgold.app.dataModel.PanVerificationRequest;
import com.rg.mchampgold.app.dataModel.PlayerInfoRequest;
import com.rg.mchampgold.app.dataModel.PlayerInfoResponse;
import com.rg.mchampgold.app.dataModel.PlayerPointsResponse;
import com.rg.mchampgold.app.dataModel.PlayingHistoryResponse;
import com.rg.mchampgold.app.dataModel.ReferBonusListResponse;
import com.rg.mchampgold.app.dataModel.SendOtpResponse;
import com.rg.mchampgold.app.dataModel.SeriesLeaderbardDeatilsResponse;
import com.rg.mchampgold.app.dataModel.SeriesLeaderbardResponse;
import com.rg.mchampgold.app.dataModel.SeriesResponse;
import com.rg.mchampgold.app.dataModel.SwitchTeamRequest;
import com.rg.mchampgold.app.dataModel.SwitchTeamResponse;
import com.rg.mchampgold.app.dataModel.TeamCompareResponse;
import com.rg.mchampgold.app.dataModel.TeamPointPreviewResponse;
import com.rg.mchampgold.app.dataModel.TransactionRequest;
import com.rg.mchampgold.app.dataModel.UpdateProfileRequest;
import com.rg.mchampgold.app.dataModel.UpdateProfileResponse;
import com.rg.mchampgold.app.dataModel.UserImageUploadResponse;
import com.rg.mchampgold.app.dataModel.WIthDrawAmoutResponse;
import com.rg.mchampgold.app.view.activity.FindScratchReponse;
import com.rg.mchampgold.app.view.adapter.AddScratchRequest;
import com.rg.mchampgold.common.api.CustomCallAdapter;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface OAuthRestService {

    @FormUrlEncoded
    @POST("login/v2/")
    CustomCallAdapter.CustomCall<UserOAuthResponse> userLogin(@Field("client_id") String client_id, @Field("client_secret") String client_secret, @Field("grant_type") String grant_type, @Field("username") String username, @Field("scope") String scope);

    @Headers("VERSION: 5")
    @POST("api/auth/register")
    CustomCallAdapter.CustomCall<RegisterResponse> userRegister(@Body RegisterRequest registerRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/register_new")
    CustomCallAdapter.CustomCall<RegisterResponse> newUserRegister(@Body RegisterRequest registerRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/login")
    CustomCallAdapter.CustomCall<LoginResponse> userLogin(@Body LoginRequest loginRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/verify_otp")
    CustomCallAdapter.CustomCall<RegisterResponse> otpVerify(@Body OtpVerfiyRequest otpVerfiyRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/verify-otp-register")
    CustomCallAdapter.CustomCall<RegisterResponse> otpRegisterVerify(@Body OtpVerfiyRequest otpVerfiyRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/send_new_otp")
    CustomCallAdapter.CustomCall<SendOtpResponse> sendOTP(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/social-login")
    CustomCallAdapter.CustomCall<RegisterResponse> userLoginSocial(@Body SocialLoginRequest socialLoginRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/razorpay")
    CustomCallAdapter.CustomCall<OrderIdResponse> getOrderId(@Body GetOrderIdRequest orderIdRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/android-add-fund-api")
    CustomCallAdapter.CustomCall<AddPaymentValueResponse> addPayment(@Body AddPaymentRequest addPaymentRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/user-full-details")
    CustomCallAdapter.CustomCall<GetUserFullDetailsResponse> getUserFullDetails(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/mybalance")
    CustomCallAdapter.CustomCall<MyBalanceResponse> getUserBalance(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/edit-profile")
    CustomCallAdapter.CustomCall<UpdateProfileResponse> updateProfile(@Body UpdateProfileRequest updateProfileRequest);

    @Headers("VERSION: 5")
    @Multipart
    @POST("api/auth/update-profile-image")
    CustomCallAdapter.CustomCall<UserImageUploadResponse> uploadUserImage(@Part("user_id") RequestBody userId, @Part MultipartBody.Part file);

    @Headers("VERSION: 5")
    @POST("api/auth/all-verify")
    CustomCallAdapter.CustomCall<AllVerifyResponse> allVerify(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/seebankdetails")
    CustomCallAdapter.CustomCall<BankDetailResponse> getBankDetail(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/see-pan-details")
    CustomCallAdapter.CustomCall<PanDetatilResponse> getPanDetail(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/bank-verify")
    CustomCallAdapter.CustomCall<BankVerifyResponse> bankVerify(@Body BankVerifyRequest bankVerifyRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/verify-pan-request")
    CustomCallAdapter.CustomCall<PanDetatilResponse> panVerify(@Body PanVerificationRequest panVerificationRequest);

    @Headers("VERSION: 5")
    @FormUrlEncoded
    @POST("api/auth/upload-pan-image-android")
    CustomCallAdapter.CustomCall<ImageUploadResponse> uploadPanImage(@Field("file") String image, @Field("user_id") String id);


    @Headers("VERSION: 5")
    @Multipart
    @POST("api/auth/upload-pan-image-android")
    CustomCallAdapter.CustomCall<ImageUploadResponse> uploadPanImageMu(@Part("user_id") RequestBody userId,@Part MultipartBody.Part file);

    @Headers("VERSION: 5")
    @Multipart
    @POST("api/auth/upload-bank-image-android")
    CustomCallAdapter.CustomCall<ImageUploadResponse> uploadBankImage(@Part("user_id") RequestBody userId,@Part MultipartBody.Part file);

    @Headers("VERSION: 5")
    @POST("api/auth/request-withdraw")
    CustomCallAdapter.CustomCall<WIthDrawAmoutResponse> withDrawAmount(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/refer-bonus-list")
    CustomCallAdapter.CustomCall<ReferBonusListResponse> getReferBonusList(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/getscorecards")
    CustomCallAdapter.CustomCall<GetWinnerScoreCardResponse> getWinnersPriceCard(@Body ContestRequest contestRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/get-banners")
    CustomCallAdapter.CustomCall<BannerListResponse> getBannerList(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/playerfullinfo")
    CustomCallAdapter.CustomCall<PlayerInfoResponse> getPlayerInfo(@Body PlayerInfoRequest playerInfoRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/getteamtoshow")
    CustomCallAdapter.CustomCall<TeamPointPreviewResponse> getPreviewPoints(@Body TeamPreviewPointRequest teamPreviewPointRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/updateteamname")
    CustomCallAdapter.CustomCall<NormalResponse> upDateTeamName(@Body TeamNameUpdateRequest teamNameUpdateRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/forget-password")
    CustomCallAdapter.CustomCall<NormalResponse> forgotPassword(@Body BaseRequest teamNameUpdateRequest);

    @Headers("VERSION: 5")
    @GET("api/auth/version")
    CustomCallAdapter.CustomCall<CheckVersionCodeResponse> checkVersionCode();

    @Headers("VERSION: 5")
    @POST("api/auth/playerfullinfo")
    CustomCallAdapter.CustomCall<PlayerInfoResponse> verifyEmailOrMobile(@Body PlayerInfoRequest playerInfoRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/change-password")
    CustomCallAdapter.CustomCall<NormalResponse> changePassword(@Body ChangePasswordRequest changePasswordRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/usernotifications")
    CustomCallAdapter.CustomCall<GetNotificationResponse> getUserNotification(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/send-new-mail")
    CustomCallAdapter.CustomCall<NormalResponse> verifyEmailByOtp(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/send_new_otp")
    CustomCallAdapter.CustomCall<NormalResponse> verifyByMobile(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/my-play-history")
    CustomCallAdapter.CustomCall<PlayingHistoryResponse> getMyPlayingHistory(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/join-by-code")
    CustomCallAdapter.CustomCall<JoinByContestCodeResponse> joinByContestCode(@Body JoinContestByCodeRequest joinContestByCodeRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/find-join-team")
    CustomCallAdapter.CustomCall<FindJoinTeamResponse> findJoinTeam(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/myusablebalance")
    CustomCallAdapter.CustomCall<BalanceResponse> getUsableBalance(@Body JoinContestRequest joinContestRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/joinleague")
    CustomCallAdapter.CustomCall<JoinContestResponse> joinContest(@Body JoinContestRequest joinContestRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/mytransaction")
    CustomCallAdapter.CustomCall<MyTransactionHistoryResponse> getMyTransaction(@Body TransactionRequest transactionRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/get-challenges-by-category")
    CustomCallAdapter.CustomCall<CategoryByContestResponse> getContestByCategory(@Body ContestRequest contestRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/category-leagues")
    CustomCallAdapter.CustomCall<ContestResponse> getContestByCategoryCode(@Body ContestRequest contestRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/create-challenge")
    CustomCallAdapter.CustomCall<CreatePrivateContestResponse> createContest(@Body CreatePrivateContestRequest createPrivateContestRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/verify-promo-code")
    CustomCallAdapter.CustomCall<NormalResponse> applyPromoCode(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/updateteamleauge")
    CustomCallAdapter.CustomCall<SwitchTeamResponse> switchTeam(@Body SwitchTeamRequest switchTeamRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/find-scratch-card")
    CustomCallAdapter.CustomCall<FindScratchReponse> findScratchCard(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/open-scratch-card")
    CustomCallAdapter.CustomCall<FindScratchReponse> openScratchCard(@Body AddScratchRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/scratch-cards-list")
    CustomCallAdapter.CustomCall<ScratchHistoryResponse> scratchCardList(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/add-cash-banners")
    CustomCallAdapter.CustomCall<AddCashBannerListResponse> addCashBannerList(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/live-matches")
    CustomCallAdapter.CustomCall<LiveMatchesResponse> liveMatches(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/live-score-board")
    CustomCallAdapter.CustomCall<LiveMatchesScoreCardResponse> liveScoreBoard(@Body BaseRequest baseRequest);


    @Headers("VERSION: 5")
    @POST("api/auth/compare")
    CustomCallAdapter.CustomCall<TeamCompareResponse> compare(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/investments")
    CustomCallAdapter.CustomCall<InvestmentResponse> investmentData(@Body BaseRequest baseRequest);

    @Headers("VERSION: 5")
    @POST("api/auth/showPlayerPoints")
    CustomCallAdapter.CustomCall<PlayerPointsResponse> showPlayerPoints(@Body BaseRequest baseRequest);

    @POST("api/auth/get-series")
    CustomCallAdapter.CustomCall<SeriesResponse> getSeries();

    @POST("api/auth/get-series-leaderboard")
    CustomCallAdapter.CustomCall<SeriesLeaderbardResponse> getSeriesLeaderboard(@Body BaseRequest baseRequest);

    @POST("api/auth/get-match-leaderboard")
    CustomCallAdapter.CustomCall<SeriesLeaderbardDeatilsResponse> getLeaderboardDetails(@Body BaseRequest baseRequest);
}
