package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Generated("com.robohorse.robopojogenerator")
public class TransactionItem {

    @SerializedName("deduct_amount")
    private String deductAmount;

    @SerializedName("transaction_id")
    private String transactionId;

    @SerializedName("date")
    private String date;

    @SerializedName("amount")
    private String amount;

    @SerializedName("transaction_by")
    private String transactionBy;

    @SerializedName("created")
    private String created;

    @SerializedName("available")
    private String available;

    @SerializedName("transaction_type")
    private String transactionType;

    @SerializedName("challengename")
    private String challengename = "";

    @SerializedName("tour")
    private String tour;

    @SerializedName("matchname")
    private String matchname = "";

    @SerializedName("add_amount")
    private String addAmount;

    @SerializedName("paymentstatus")
    private String paymentstatus;

    @SerializedName("id")
    private int id;

    @SerializedName("teamname")
    private String teamname;

    @SerializedName("username")
    private String username;


    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }


    public void setTransactionBy(String transactionBy) {
        this.transactionBy = transactionBy;
    }

    public String getTransactionBy() {
        return transactionBy;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreated() {
        return created;
    }

    public String getDeductAmount() {
        return deductAmount;
    }

    public void setDeductAmount(String deductAmount) {
        this.deductAmount = deductAmount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getAddAmount() {
        return addAmount;
    }

    public void setAddAmount(String addAmount) {
        this.addAmount = addAmount;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setChallengename(String challengename) {
        this.challengename = challengename;
    }

    public String getChallengename() {
        return challengename;
    }

    public void setTour(String tour) {
        this.tour = tour;
    }

    public String getTour() {
        return tour;
    }

    public void setMatchname(String matchname) {
        this.matchname = matchname;
    }

    public String getMatchname() {
        return matchname;
    }


    public void setPaymentstatus(String paymentstatus) {
        this.paymentstatus = paymentstatus;
    }

    public String getPaymentstatus() {
        return paymentstatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }

    public String getTeamname() {
        return teamname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return
                "TransactionItem{" +
                        "deduct_amount = '" + deductAmount + '\'' +
                        ",transaction_id = '" + transactionId + '\'' +
                        ",date = '" + date + '\'' +
                        ",amount = '" + amount + '\'' +
                        ",transaction_by = '" + transactionBy + '\'' +
                        ",created = '" + created + '\'' +
                        ",available = '" + available + '\'' +
                        ",transaction_type = '" + transactionType + '\'' +
                        ",challengename = '" + challengename + '\'' +
                        ",tour = '" + tour + '\'' +
                        ",matchname = '" + matchname + '\'' +
                        ",add_amount = '" + addAmount + '\'' +
                        ",paymentstatus = '" + paymentstatus + '\'' +
                        ",id = '" + id + '\'' +
                        ",teamname = '" + teamname + '\'' +
                        ",username = '" + username + '\'' +
                        "}";
    }

    public String dayWithMonth() {
        String string = created;
        Date date1 = null;
        try {

            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(string);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd MMMM");

            return simpleDateFormat.format(date1);
        } catch (ParseException e) {
            return "";
        }
    }

    public String getTypeTransactionAmount() {
        if (Double.parseDouble(deductAmount) > 0) {
            return "- " + "₹" + deductAmount;
        } else if (Double.parseDouble(deductAmount) == 0) {
            if (Double.parseDouble(addAmount) > 0) {
                return "+ " + "₹" + addAmount;
            }
            return "" + "₹" + deductAmount;
        } else
            return "+ " + "₹" + addAmount;

    }

    public int getColorCode() {
        if (Double.parseDouble(deductAmount) > 0) {
            return MyApplication.appContext.getResources().getColor(R.color.colorThemeRed);
        } else if (Double.parseDouble(deductAmount) == 0) {
            if (Double.parseDouble(addAmount) > 0) {
                return MyApplication.appContext.getResources().getColor(R.color.colorGreen);
            }
            return MyApplication.appContext.getResources().getColor(R.color.colorYellow);
        } else
            return MyApplication.appContext.getResources().getColor(R.color.colorGreen);
    }


}