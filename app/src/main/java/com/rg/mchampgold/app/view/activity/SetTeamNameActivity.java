package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.TeamNameUpdateRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.NormalResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.TextViewLinkHandler;
import com.rg.mchampgold.app.view.adapter.SpinnerAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivitySetTeamNameBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class SetTeamNameActivity extends AppCompatActivity {

    private String userId = "", dob = "";
    ActivitySetTeamNameBinding mBinding;
    String[] stateAr;
    private String state;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_set_team_name);
        MyApplication.getAppComponent().inject(SetTeamNameActivity.this);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        AppUtils.disableCopyPaste(mBinding.etUserReferCode);
        AppUtils.disableCopyPaste(mBinding.userTeam);

        if (MyApplication.refer_show != null) {
            if (MyApplication.refer_show.equalsIgnoreCase("0")) {
                mBinding.etUserReferCode.setVisibility(View.GONE);
            }
        }
        mBinding.tvTc.setText(Html.fromHtml("By registering, I agree to " + getString(R.string.app_names) + " <a href='" + MyApplication.terms_url + "'>T&amp;C </a> and <a href='" + MyApplication.privacy_url + "'>Privacy Policy</a>"));
        mBinding.tvTc.setMovementMethod(new TextViewLinkHandler() {
            @Override
            public void onLinkClick(String url) {
                if (url.equalsIgnoreCase(MyApplication.terms_url)) {
                    AppUtils.openWebViewActivity(getString(R.string.terms_conditions), MyApplication.terms_url);
                } else {
                    AppUtils.openWebViewActivity(getString(R.string.privacy_policy), MyApplication.privacy_url);
                }
//                Toast.makeText(mainBinding.tvTc.getContext(), url, Toast.LENGTH_SHORT).show();
            }
        });

        stateAr = new String[getResources().getStringArray(R.array.india_states).length];
        stateAr = getResources().getStringArray(R.array.india_states);

        mBinding.stateSpinner.setAdapter(new SpinnerAdapter(this, stateAr));

        mBinding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    //   mBinding.stateSpinner.setEnabled(false);
                    state = stateAr[i];
                }
                ((TextView) mBinding.stateSpinner.getSelectedView()).setTextColor(getResources().getColor(R.color.colorBlack));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinding.submit.setOnClickListener(view -> {
            String user_team = mBinding.userTeam.getText().toString().trim();
            if (user_team.equals("")) {
                showMsg("Please enter team name.");
            } else if (user_team.length() < 3) {
                showMsg("Team name should be 3 to 9 character without space");
            } else if (user_team.length() > 9) {
                showMsg("Team name should be 3 to 9 characters without space");
            } else if (TextUtils.isEmpty(state)) {
                showMsg("Please select your state");
            } else {
                updateTeamName(mBinding.userTeam.getText().toString(), state);
            }
        });

    }

    private void showMsg(String msg) {
        AppUtils.showErrorr(SetTeamNameActivity.this, msg);
    }

    public void updateTeamName(String teamName, String state) {
        mBinding.setRefreshing(true);
        TeamNameUpdateRequest teamNameUpdateRequest = new TeamNameUpdateRequest();
        teamNameUpdateRequest.setState(state);
        teamNameUpdateRequest.setTeamname(teamName);
        teamNameUpdateRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        if (!mBinding.etUserReferCode.getText().toString().trim().equalsIgnoreCase(""))
            teamNameUpdateRequest.setUserReferCode(mBinding.etUserReferCode.getText().toString().trim());
        CustomCallAdapter.CustomCall<NormalResponse> bankDetailResponseCustomCall = oAuthRestService.upDateTeamName(teamNameUpdateRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<NormalResponse>() {
            @Override
            public void success(Response<NormalResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    NormalResponse normalResponse = response.body();
                    if (normalResponse.getStatus() == 1) {
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TEAM_NAME, teamName);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_STATE, state);
                        finish();
                    } else {
                        AppUtils.showErrorr(SetTeamNameActivity.this, normalResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
    }
}
