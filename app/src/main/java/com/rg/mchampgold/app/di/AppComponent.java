package com.rg.mchampgold.app.di;

import android.content.Context;

import com.rg.mchampgold.app.di.module.AppModule;
import com.rg.mchampgold.app.payTm.PaytmMerchantActivity;
import com.rg.mchampgold.app.view.VerifyOtpBtmSheet;
import com.rg.mchampgold.app.view.activity.AddBalanceActivity;
import com.rg.mchampgold.app.view.activity.AllContestActivity;
import com.rg.mchampgold.app.view.activity.ChangePasswordActivity;
import com.rg.mchampgold.app.view.activity.ChooseCandVCActivity;
import com.rg.mchampgold.app.view.activity.ForgotPasswordActivity;
import com.rg.mchampgold.app.view.activity.FullScoreCardActivity;
import com.rg.mchampgold.app.view.activity.HomeActivity;
import com.rg.mchampgold.app.view.activity.InvestmentActivity;
import com.rg.mchampgold.app.view.activity.InviteFriendActivity;
import com.rg.mchampgold.app.view.activity.InvitedFriendActivity;
import com.rg.mchampgold.app.view.activity.JoinContestByInviteCodeActivity;
import com.rg.mchampgold.app.view.activity.LeaderboardActivity;
import com.rg.mchampgold.app.view.activity.LeaderboardDetailsActivity;
import com.rg.mchampgold.app.view.activity.LiveFinishedContestActivity;
import com.rg.mchampgold.app.view.activity.LoginActivity;
import com.rg.mchampgold.app.view.activity.MyTeamsActivity;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.app.view.activity.OtpVerifyActivity;
import com.rg.mchampgold.app.view.activity.PaymentOptionActivity;
import com.rg.mchampgold.app.view.activity.PersonalDetailsActivity;
import com.rg.mchampgold.app.view.activity.PlayerInfoActivity;
import com.rg.mchampgold.app.view.activity.PrivateContestActivity;
import com.rg.mchampgold.app.view.activity.RegisterActivity;
import com.rg.mchampgold.app.view.activity.ScratchCardHistoryActivity;
import com.rg.mchampgold.app.view.activity.SetTeamNameActivity;
import com.rg.mchampgold.app.view.activity.SplashActivity;
import com.rg.mchampgold.app.view.activity.TeamCompareActivity;
import com.rg.mchampgold.app.view.activity.TeamPreviewPointActivity;
import com.rg.mchampgold.app.view.activity.UpComingContestDetailActivity;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.activity.VerifyAccountActivity;
import com.rg.mchampgold.app.view.activity.WinnerBreakupMatchManagerActivity;
import com.rg.mchampgold.app.view.activity.WithdrawCashActivity;
import com.rg.mchampgold.app.view.adapter.JoinedContestItemAdapter;
import com.rg.mchampgold.app.view.adapter.PreviewPlayerItemAdapter;
import com.rg.mchampgold.app.view.addCash.AccountsActivity;
import com.rg.mchampgold.app.view.addCash.AccountsFragment;
import com.rg.mchampgold.app.view.addCash.BalanceFragment;
import com.rg.mchampgold.app.view.addCash.PlayingHistoryActivity;
import com.rg.mchampgold.app.view.addCash.PlayingHistoryFragment;
import com.rg.mchampgold.app.view.addCash.TransactionsActivity;
import com.rg.mchampgold.app.view.addCash.TransactionsFragment;
import com.rg.mchampgold.app.view.basketball.BasketBallTeamPreviewPointActivity;
import com.rg.mchampgold.app.view.football.FootballTeamPreviewPointActivity;
import com.rg.mchampgold.app.view.fragment.BankVerificationFragment;
import com.rg.mchampgold.app.view.fragment.ContestStatsFragment;
import com.rg.mchampgold.app.view.fragment.LeaderBoardFragment;
import com.rg.mchampgold.app.view.fragment.LiveMatchersTabFragment;
import com.rg.mchampgold.app.view.fragment.LiveMatchesFragment;
import com.rg.mchampgold.app.view.fragment.MobileVarificationFragment;
import com.rg.mchampgold.app.view.fragment.MyTeamFragment;
import com.rg.mchampgold.app.view.fragment.MyWalletFragment;
import com.rg.mchampgold.app.view.fragment.PanValidationFragment;
import com.rg.mchampgold.app.view.fragment.PlayingStatusSheetFragment;
import com.rg.mchampgold.app.view.fragment.UpComingContestFragment;
import com.rg.mchampgold.app.view.fragment.WinningBreakUpFragment;

import com.rg.mchampgold.app.view.fragment.home.HomeFragment;

import com.rg.mchampgold.app.view.fragment.upComing.UpComingMatchListViewModel;
import com.rg.mchampgold.app.view.myMatches.finished.MyMatchesFinishedMatchListViewModel;
import com.rg.mchampgold.app.view.myMatches.live.MyMatchesLiveMatchListViewModel;
import com.rg.mchampgold.app.view.myMatches.upComing.MyMatchesUpComingMatchListViewModel;
import com.rg.mchampgold.app.viewModel.ContestDetailsViewModel;
import com.rg.mchampgold.app.viewModel.ContestViewModel;
import com.rg.mchampgold.app.viewModel.CreateTeamViewModel;
import com.rg.mchampgold.app.viewModel.GetPlayerDataViewModel;
import com.rg.mchampgold.app.viewModel.TeamViewModel;
import com.rg.mchampgold.common.api.RestHelper;
import com.rg.mchampgold.common.di.module.NetModule;
import com.rg.mchampgold.common.di.module.RepositoryModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(
        modules = {AppModule.class, NetModule.class, RepositoryModule.class}
)
public interface AppComponent {

    public void inject(Context content);

    public void inject(RestHelper restHelper);


    public void inject(FullScoreCardActivity fullScoreCardActivity);
    public void inject(LiveMatchesFragment liveMatchesFragment);
    public void inject(LiveMatchersTabFragment liveMatchersTabFragment);
    public void inject(HomeFragment homeFragment);
    public void inject(MyWalletFragment MyWalletFragment);

    public void inject(UpComingMatchListViewModel upComingMatchListViewModel);

    public void inject(UpComingContestFragment upCommingContestFragment);


    public void inject(MyMatchesFinishedMatchListViewModel finishedMatchListViewModel);

    public void inject(MyMatchesUpComingMatchListViewModel finishedMatchListViewModel);

    public void inject(MyMatchesLiveMatchListViewModel inishedMatchListViewModel);

    public void inject(LoginActivity loginActivity);

    public void inject(PlayingHistoryActivity loginActivity);

    public void inject(AccountsActivity accountsActivity);

    public void inject(TransactionsActivity transactionsActivity);

    public void inject(WinnerBreakupMatchManagerActivity winnerBreakupMatchManagerActivity);

    public void inject(NotificationActivity notificationActivity);

    public void inject(PlayerInfoActivity playerInfoActivity);

    public void inject(MyTeamsActivity myTeamsActivity);

    public void inject(AllContestActivity allContestActivity);

    public void inject(PrivateContestActivity playerInfoActivity);

    public void inject(ChangePasswordActivity changePasswordActivity);

    public void inject(SetTeamNameActivity playerInfoActivity);

    public void inject(PaytmMerchantActivity merchantActivity);

    public void inject(MyTeamFragment myTeamFragment);

    public void inject(LiveFinishedContestActivity liveFinishedContestActivity);

    public void inject(UpComingContestDetailActivity upCommingContestDetailsActivity);

    public void inject(WithdrawCashActivity withdrawCashActivity);

    public void inject(InviteFriendActivity inviteFriendActivity);

    public void inject(InvitedFriendActivity inviteFriendActivity);

    public void inject(AccountsFragment accountsFragment);

    public void inject(BalanceFragment balanceFragment);

    public void inject(WinningBreakUpFragment winningBreakUpFragment);

    public void inject(LeaderBoardFragment leaderBoradFragment);

    public void inject(PlayingHistoryFragment playingHistoryFragment);

    public void inject(ContestStatsFragment contestStatsFragment);

    public void inject(TransactionsFragment transactionsFragment);

    public void inject(MobileVarificationFragment mobileVarificationFragment);

    public void inject(BankVerificationFragment bankVerificationFragment);

    public void inject(VerifyOtpBtmSheet verifyOtpBtmSheet);

    public void inject(PanValidationFragment panValidationFragment);

    public void inject(PersonalDetailsActivity personalDetailsActivity);

    public void inject(VerifyAccountActivity verifyAccountActivity);

    public void inject(HomeActivity homeActivity);

    public void inject(TeamPreviewPointActivity teamPreviewPointActivity);

    public void inject(AddBalanceActivity addBalanceActivity);

    public void inject(MyWalletActivity myWalletActivity);

    public void inject(JoinContestByInviteCodeActivity joinContestByInviteCodeActivity);

    public void inject(PaymentOptionActivity paymentOptionActivity);

    public void inject(RegisterActivity registerActivity);

    public void inject(OtpVerifyActivity otpVerifyActivity);

    public void inject(ContestViewModel contestViewModel);

    public void inject(TeamViewModel teamViewModel);

    public void inject(GetPlayerDataViewModel getPlayerDataViewModel);

    public void inject(CreateTeamViewModel createTeamViewModel);

    public void inject(ContestDetailsViewModel contestDetailsViewModel);

    public void inject(UpComingContestActivity commingContestActivity);

    public void inject(BasketBallTeamPreviewPointActivity basketBallTeamPreviewActivity);

    public void inject(FootballTeamPreviewPointActivity footballTeamPreviewActivity);

    public void inject(SplashActivity splashActivity);

    public void inject(ForgotPasswordActivity forgotPasswordActivity);

    public void inject(ScratchCardHistoryActivity scratchCardHistoryActivity);

    public void inject(JoinedContestItemAdapter adapter);

    public void inject(TeamCompareActivity activity);

    public void inject(InvestmentActivity investmentActivity);

    public void inject(PlayingStatusSheetFragment playingStatusSheetFragment);

    public void inject(ChooseCandVCActivity chooseCandVCActivity);

    public void inject(LeaderboardActivity leaderboardActivity);

    public void inject(LeaderboardDetailsActivity leaderboardDetailsActivity);

    public void inject(PreviewPlayerItemAdapter adapter);

}
