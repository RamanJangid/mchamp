package com.rg.mchampgold.app.view.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.BalanceResponse;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.FindJoinTeamResponse;
import com.rg.mchampgold.app.dataModel.JoinByContestCodeResponse;
import com.rg.mchampgold.app.dataModel.JoinContestByCodeRequest;
import com.rg.mchampgold.app.dataModel.JoinContestResponse;
import com.rg.mchampgold.app.dataModel.JoinItem;
import com.rg.mchampgold.app.dataModel.TeamFindItem;
import com.rg.mchampgold.app.dataModel.UsableBalanceItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.SpinnerAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityJoinContestByCodeBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class JoinContestByInviteCodeActivity extends AppCompatActivity {
    ActivityJoinContestByCodeBinding mBinding;

    @Inject
    OAuthRestService oAuthRestService;
    int challenge_id;
    String joinnigB;
    String matchKey = "";
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    Context context;
    String headerText;
    String date;
    double availableB;
    double usableB;
    int isBonous;
    String sportKey = "";
    Contest contest = new Contest();
    ArrayList<TeamFindItem> teamList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_join_contest_by_code);
        context = JoinContestByInviteCodeActivity.this;
        MyApplication.getAppComponent().inject(JoinContestByInviteCodeActivity.this);
        initialize();
    }

    void initialize() {
        AppUtils.disableCopyPaste(mBinding.promoCode);

        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.invite_contest_code));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            date = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
        }


        mBinding.btnJoinContest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBinding.promoCode.getText().toString().equals(""))
                    AppUtils.showErrorr(JoinContestByInviteCodeActivity.this, "Please enter contest code.");
                else {
                    joinContestByCode();
                }
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;

           /* case R.id.navigation_wallet:
                openWalletActivity();
                return true;*/
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openNotificationActivity() {
        startActivity(new Intent(JoinContestByInviteCodeActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(JoinContestByInviteCodeActivity.this, MyWalletActivity.class));

    }

    private void joinContestByCode() {
        mBinding.setRefreshing(true);
        JoinContestByCodeRequest joinContestByCodeRequest = new JoinContestByCodeRequest();
        joinContestByCodeRequest.setSport_key(sportKey);
        joinContestByCodeRequest.setUserId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        if (matchKey == null)
            matchKey = "";
        joinContestByCodeRequest.setMatchkey(matchKey);
        joinContestByCodeRequest.setGetcode(mBinding.promoCode.getText().toString().trim());
        CustomCallAdapter.CustomCall<JoinByContestCodeResponse> userFullDetailsResponseCustomCall = oAuthRestService.joinByContestCode(joinContestByCodeRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<JoinByContestCodeResponse>() {
            @Override
            public void success(Response<JoinByContestCodeResponse> response) {
                mBinding.setRefreshing(false);
                JoinByContestCodeResponse normalResponse = response.body();
                if (normalResponse != null) {
                    if (normalResponse.getStatus() == 1) {
                        if (normalResponse.getResult().get(0).getMessage().equals("Challenge opened")) {
                            challenge_id = normalResponse.getResult().get(0).getChallengeid();
                            contest.setId(challenge_id);
                            contest.setEntryfee(normalResponse.getResult().get(0).getEntryfee() + "");
                            joinnigB = String.valueOf(normalResponse.getResult().get(0).getEntryfee());
                            findJoinTeam();
                        } else if (normalResponse.getResult().get(0).getMessage().equals("already used")) {
                            AppUtils.showErrorr(JoinContestByInviteCodeActivity.this, "Invite code already used");
                        } else if (normalResponse.getResult().get(0).getMessage().equals("Challenge closed")) {
                            AppUtils.showErrorr(JoinContestByInviteCodeActivity.this, "Sorry, this League is full! Please join another League.");
                        } else if (normalResponse.getResult().get(0).getMessage().equals("invalid code")) {

                            AppUtils.showErrorr(JoinContestByInviteCodeActivity.this, "Invalid code");
                        } else {

                            AppUtils.showErrorr(JoinContestByInviteCodeActivity.this, normalResponse.getResult().get(0).getMessage());
                        }
                    } else {
                        Toast.makeText(JoinContestByInviteCodeActivity.this, normalResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(JoinContestByInviteCodeActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }


    private void findJoinTeam() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setMatchkey(matchKey);
        baseRequest.setSport_key(sportKey);
        baseRequest.setChallenge_id(challenge_id);
        CustomCallAdapter.CustomCall<FindJoinTeamResponse> userFullDetailsResponseCustomCall = oAuthRestService.findJoinTeam(baseRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<FindJoinTeamResponse>() {
            @Override
            public void success(Response<FindJoinTeamResponse> response) {
                mBinding.setRefreshing(false);
                FindJoinTeamResponse findJoinTeamResponse = response.body();
                if (findJoinTeamResponse != null) {
                    if (findJoinTeamResponse.getStatus() == 1) {
                        if (findJoinTeamResponse.getResult().size() > 0) {
                            teamList = new ArrayList<>(findJoinTeamResponse.getResult());
                            checkBalance();
                        } else {
                            Intent intent = new Intent(JoinContestByInviteCodeActivity.this, MyTeamsActivity.class);
                            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
                            intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
                            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
                            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
                            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
                            intent.putExtra(Constants.SPORT_KEY, sportKey);
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(JoinContestByInviteCodeActivity.this, findJoinTeamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(JoinContestByInviteCodeActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }


    private void checkBalance() {
        mBinding.setRefreshing(true);
        JoinContestRequest joinContestRequest = new JoinContestRequest();
        joinContestRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        joinContestRequest.setLeagueId(challenge_id + "");
        CustomCallAdapter.CustomCall<BalanceResponse> userFullDetailsResponseCustomCall = oAuthRestService.getUsableBalance(joinContestRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<BalanceResponse>() {
            @Override
            public void success(Response<BalanceResponse> response) {
                mBinding.setRefreshing(false);
                BalanceResponse findJoinTeamResponse = response.body();
                if (findJoinTeamResponse != null) {
                    if (findJoinTeamResponse.getStatus() == 1) {

                        if (findJoinTeamResponse.getResult().size() > 0) {
                            UsableBalanceItem balanceItem = findJoinTeamResponse.getResult().get(0);
                            availableB = balanceItem.getUsertotalbalance();
                            usableB = balanceItem.getUsablebalance();
                            isBonous = balanceItem.getIs_bonus();
                            createDialog();
                        } else {
                        }
                    } else {
                        Toast.makeText(JoinContestByInviteCodeActivity.this, findJoinTeamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(JoinContestByInviteCodeActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }

    public void createDialog() {

        Dialog dialog = new Dialog(JoinContestByInviteCodeActivity.this);
        dialog.setContentView(R.layout.joined_team_confirm_dialog);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        LinearLayout okBtn = dialog.findViewById(R.id.ok_btn);
        TextView currentBalTxt = dialog.findViewById(R.id.currentBalTxt);
        TextView joinedBaltxt = dialog.findViewById(R.id.joinedBaltxt);
        TextView tPay = dialog.findViewById(R.id.toPay);
        TextView remaingBaltxt = dialog.findViewById(R.id.remaingBaltxt);
        TextView tv_tc_join_dialog = dialog.findViewById(R.id.tv_tc_join_dialog);
        RelativeLayout cancelButton = dialog.findViewById(R.id.cancel_button);
        RelativeLayout switchTeamBtn = dialog.findViewById(R.id.switch_team_Btn);
        LinearLayout llTeamLayout = dialog.findViewById(R.id.ll_team_layout);
        DecimalFormat decimalFormat = new DecimalFormat("#.##");


        currentBalTxt.setText("₹ " + decimalFormat.format(availableB));
        joinedBaltxt.setText("₹ " + decimalFormat.format(Double.parseDouble(joinnigB)));
        tPay.setText("₹ " + decimalFormat.format(Double.parseDouble(joinnigB) - usableB));

        tv_tc_join_dialog.setText(Html.fromHtml("By joining this contest, you accept " + getString(R.string.app_name) + " <a href='" + MyApplication.terms_url + "'>Terms &amp; Conditions </a> and <a href='" + MyApplication.privacy_url + "'>Privacy Policy</a> Confirm that you are not a resident of Assam, Odisha, Sikkim, Andhra Pradesh, Tamil Nadu, Nagaland or Telangana."));
        tv_tc_join_dialog.setMovementMethod(LinkMovementMethod.getInstance());

        final Spinner teamSpinner;

        teamSpinner = dialog.findViewById(R.id.teamList);
        String ar[] = new String[teamList.size() + 1];
        final String[] selectedTeam = {""};


        ar[0] = "Select Team";
        for (int i = 0; i < teamList.size(); i++) {
            ar[i + 1] = "Team " + teamList.get(i).getTeamnumber();
        }

        teamSpinner.setAdapter(new SpinnerAdapter(context, ar));
        teamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0)
                    selectedTeam[0] = teamList.get(i - 1).getTeamid() + "";
                else
                    selectedTeam[0] = "0";
                ((TextView) teamSpinner.getSelectedView()).setTextColor(Color.BLACK);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        llTeamLayout.setVisibility(View.VISIBLE);

        double remainBal = usableB; //- Double.parseDouble(joinnigB);

        if (remainBal > 0) {
            //  DecimalFormat decimalFormat = new DecimalFormat("#.##");
            //   remaingBaltxt.setText("₹ " + decimalFormat.format(remainBal));
            remaingBaltxt.setText("₹ " + remainBal);
        } else {
            remaingBaltxt.setText("₹ 0.0");
        }

        dialog.show();

        okBtn.setOnClickListener(view1 -> dialog.dismiss());

        cancelButton.setOnClickListener(view13 -> dialog.dismiss());

        switchTeamBtn.setOnClickListener(view12 -> {
            dialog.dismiss();
            if (contest.getIsBonus() == 1) {
                if ((usableB + availableB) < Double.parseDouble(contest.getEntryfee())) {
                    startActivity(new Intent(JoinContestByInviteCodeActivity.this, AddBalanceActivity.class));
                } else {
                    joinContest(teamList.get(teamSpinner.getSelectedItemPosition() - 1).getTeamid());
                }

            } else {
                if (availableB < Double.parseDouble(contest.getEntryfee())) {
                    startActivity(new Intent(JoinContestByInviteCodeActivity.this, AddBalanceActivity.class));
                } else {
                    joinContest(teamList.get(teamSpinner.getSelectedItemPosition() - 1).getTeamid());
                }
            }

        });

    }


    private void joinContest(int team_id) {
        mBinding.setRefreshing(true);
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setLeagueId(challenge_id + "");
        request.setSport_key(sportKey);
        request.setMatchKey(matchKey);
        request.setTeamId(team_id + "");
        CustomCallAdapter.CustomCall<JoinContestResponse> userFullDetailsResponseCustomCall = oAuthRestService.joinContest(request);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<JoinContestResponse>() {
            @Override
            public void success(Response<JoinContestResponse> response) {
                mBinding.setRefreshing(false);
                JoinContestResponse findJoinTeamResponse = response.body();
                if (findJoinTeamResponse != null) {
                    if (findJoinTeamResponse.getStatus() == 1) {
                        ArrayList<JoinItem> joinItems = findJoinTeamResponse.getResult();
                        if (joinItems.get(0).isStatus()) {

                            Toast.makeText(JoinContestByInviteCodeActivity.this, "You have Successfully join this contest.", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    } else {
                        Toast.makeText(JoinContestByInviteCodeActivity.this, findJoinTeamResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(JoinContestByInviteCodeActivity.this, "Oops something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
            }
        });

    }

}
