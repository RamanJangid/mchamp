package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.CommonPlayerListModel;
import com.rg.mchampgold.app.dataModel.OtherPlayerListModel;
import com.rg.mchampgold.app.dataModel.TeamCompareCVCDataModel;
import com.rg.mchampgold.app.dataModel.TeamCompareCVCModel;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ComparePlayerSubItemRecyclerBinding;

import java.util.ArrayList;
import java.util.List;

public class TeamCompareSubAdapter extends RecyclerView.Adapter<TeamCompareSubAdapter.ViewHolder> {

    private TeamCompareCVCModel cvc_list;
    private List<CommonPlayerListModel> common_list;
    private List<ArrayList<OtherPlayerListModel>> other_list;
    private Context context;
    private boolean isForJoinContest;
    int teamId = 0;
    String sportKey="";
    int pos;
    String team_id1,team_id2;

    class ViewHolder extends RecyclerView.ViewHolder {
        final ComparePlayerSubItemRecyclerBinding binding;

        ViewHolder(ComparePlayerSubItemRecyclerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public TeamCompareSubAdapter(TeamCompareCVCModel cvc_list, List<ArrayList<OtherPlayerListModel>> other_list, List<CommonPlayerListModel> common_list, Context context, int pos, String team_id1, String team_id2, String sportKey) {
        this.cvc_list=cvc_list;
        this.common_list=common_list;
        this.other_list=other_list;
        this.context=context;
        this.pos=pos;
        this.team_id1=team_id1;
        this.team_id2=team_id2;
        this.sportKey = sportKey;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ComparePlayerSubItemRecyclerBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.compare_player_sub_item_recycler,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int team1Pos=0;
        int team2Pos=1;
        if (pos==0){
            team1Pos=getCVCTeam1Position(cvc_list.getCaptain());
            team2Pos=getCVCTeam2Position(cvc_list.getCaptain());
            if (position==0) {
                holder.binding.cVcTxt1.setText("SK");
                holder.binding.cVcTxt2.setText("SK");
                AppUtils.loadPlayerImage(holder.binding.playerImg1,cvc_list.getCaptain().get(team1Pos).getImage());
                AppUtils.loadPlayerImage(holder.binding.playerImg2, cvc_list.getCaptain().get(team2Pos).getImage());
                holder.binding.playerName1.setText(cvc_list.getCaptain().get(team1Pos).getPlayername());
                holder.binding.playerName2.setText(cvc_list.getCaptain().get(team2Pos).getPlayername());
                holder.binding.playerRole1.setText(cvc_list.getCaptain().get(team1Pos).getRole());
                holder.binding.playerRole2.setText(cvc_list.getCaptain().get(team2Pos).getRole());
                holder.binding.playerPoint1.setText(cvc_list.getCaptain().get(team1Pos).getPlayerpoints());
                holder.binding.playerPoint2.setText(cvc_list.getCaptain().get(team2Pos).getPlayerpoints());
            }else {

                if (!sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.binding.cVcTxt1.setBackgroundTintList(context.getResources().getColorStateList(R.color.colorBaseBackground));
                        holder.binding.cVcTxt2.setBackgroundTintList(context.getResources().getColorStateList(R.color.colorBaseBackground));
                    }
                    holder.binding.cVcTxt1.setText("K");
                    holder.binding.cVcTxt2.setText("K");
                    AppUtils.loadPlayerImage(holder.binding.playerImg1, cvc_list.getVice_captain().get(team1Pos).getImage());
                    AppUtils.loadPlayerImage(holder.binding.playerImg2, cvc_list.getVice_captain().get(team2Pos).getImage());
                    holder.binding.playerName1.setText(cvc_list.getVice_captain().get(team1Pos).getPlayername());
                    holder.binding.playerName2.setText(cvc_list.getVice_captain().get(team2Pos).getPlayername());
                    holder.binding.playerRole1.setText(cvc_list.getVice_captain().get(team1Pos).getRole());
                    holder.binding.playerRole2.setText(cvc_list.getVice_captain().get(team2Pos).getRole());
                    holder.binding.playerPoint1.setText(cvc_list.getVice_captain().get(team1Pos).getPlayerpoints());
                    holder.binding.playerPoint2.setText(cvc_list.getVice_captain().get(team2Pos).getPlayerpoints());
                } else {
                    holder.binding.llCompareItem.setVisibility(View.GONE);
                }
            }
        }else if (pos==1){
            team1Pos=getOtherTeam1Position(other_list.get(position));
            team2Pos=getOtherTeam2Position(other_list.get(position));
            holder.binding.cVcTxt1.setVisibility(View.GONE);
            holder.binding.cVcTxt2.setVisibility(View.GONE);
            AppUtils.loadPlayerImage(holder.binding.playerImg1,other_list.get(position).get(team1Pos).getImage());
            AppUtils.loadPlayerImage(holder.binding.playerImg2, other_list.get(position).get(team2Pos).getImage());
            holder.binding.playerName1.setText(other_list.get(position).get(team1Pos).getPlayername());
            holder.binding.playerName2.setText(other_list.get(position).get(team2Pos).getPlayername());
            holder.binding.playerRole1.setText(other_list.get(position).get(team1Pos).getRole());
            holder.binding.playerRole2.setText(other_list.get(position).get(team2Pos).getRole());
            holder.binding.playerPoint1.setText(other_list.get(position).get(team1Pos).getPlayerpoints());
            holder.binding.playerPoint2.setText(other_list.get(position).get(team2Pos).getPlayerpoints());
        }else {
            holder.binding.cVcTxt1.setVisibility(View.GONE);
            holder.binding.cVcTxt2.setVisibility(View.GONE);
            AppUtils.loadPlayerImage(holder.binding.playerImg1,common_list.get(position).getImage());
            AppUtils.loadPlayerImage(holder.binding.playerImg2, common_list.get(position).getImage());
            holder.binding.playerName1.setText(common_list.get(position).getPlayername());
            holder.binding.playerName2.setText(common_list.get(position).getPlayername());
            holder.binding.playerRole1.setText(common_list.get(position).getRole());
            holder.binding.playerRole2.setText(common_list.get(position).getRole());
            holder.binding.playerPoint1.setText(common_list.get(position).getPlayerpoints());
            holder.binding.playerPoint2.setText(common_list.get(position).getPlayerpoints());
        }
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return pos==0?2:pos==1?other_list.size():common_list.size();
    }

    public int getCVCTeam1Position(ArrayList<TeamCompareCVCDataModel> list){
        int position=0;
        for (int i=0; i< list.size(); i++){
            if (list.get(i).getTeam_id().equalsIgnoreCase(team_id1)){
                position=i;
            }
        }
        return position;
    }
    public int getCVCTeam2Position(ArrayList<TeamCompareCVCDataModel> list){
        int position=0;
        for (int i=0; i< list.size(); i++){
            if (list.get(i).getTeam_id().equalsIgnoreCase(team_id2)){
                position=i;
            }
        }
        return position;
    }
    public int getOtherTeam1Position(ArrayList<OtherPlayerListModel> list){
        int position=0;
        for (int i=0; i< list.size(); i++){
            if (list.get(i).getTeam_id().equalsIgnoreCase(team_id1)){
                position=i;
            }
        }
        return position;
    }
    public int getOtherTeam2Position(ArrayList<OtherPlayerListModel> list){
        int position=0;
        for (int i=0; i< list.size(); i++){
            if (list.get(i).getTeam_id().equalsIgnoreCase(team_id2)){
                position=i;
            }
        }
        return position;
    }

}