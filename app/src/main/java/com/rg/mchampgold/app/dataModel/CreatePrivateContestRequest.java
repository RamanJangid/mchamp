package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CreatePrivateContestRequest implements Serializable {

	@SerializedName("entryfee")
	private String entryfee;

	@SerializedName("pricecards")
	private ArrayList<WinnerBreakUpData> pricecards;

	@SerializedName("matchkey")
	private String matchkey;

	@SerializedName("name")
	private String name;

	@SerializedName("is_public")
	private String isPublic;

	@SerializedName("multi_entry")
	private String multiEntry;

	@SerializedName("win_amount")
	private String winAmount;

	@SerializedName("user_id")
	private String userid;

	@SerializedName("maximum_user")
	private String maximumUser;

	@SerializedName("sport_key")
	private String sport_key="";

	public void setEntryfee(String entryfee){
		this.entryfee = entryfee;
	}

	public String getEntryfee(){
		return entryfee;
	}

	public ArrayList<WinnerBreakUpData> getPricecards() {
		return pricecards;
	}

	public void setPricecards(ArrayList<WinnerBreakUpData> pricecards) {
		this.pricecards = pricecards;
	}

	public void setMatchkey(String matchkey){
		this.matchkey = matchkey;
	}

	public String getMatchkey(){
		return matchkey;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setIsPublic(String isPublic){
		this.isPublic = isPublic;
	}

	public String getIsPublic(){
		return isPublic;
	}

	public void setMultiEntry(String multiEntry){
		this.multiEntry = multiEntry;
	}

	public String getMultiEntry(){
		return multiEntry;
	}

	public void setWinAmount(String winAmount){
		this.winAmount = winAmount;
	}

	public String getWinAmount(){
		return winAmount;
	}

	public void setUserid(String userid){
		this.userid = userid;
	}

	public String getUserid(){
		return userid;
	}

	public void setMaximumUser(String maximumUser){
		this.maximumUser = maximumUser;
	}

	public String getMaximumUser(){
		return maximumUser;
	}

	@Override
 	public String toString(){
		return 
			"CreatePrivateContestRequest{" + 
			"entryfee = '" + entryfee + '\'' + 
			",pricecards = '" + pricecards + '\'' + 
			",matchkey = '" + matchkey + '\'' + 
			",name = '" + name + '\'' + 
			",is_public = '" + isPublic + '\'' + 
			",multi_entry = '" + multiEntry + '\'' + 
			",win_amount = '" + winAmount + '\'' + 
			",userid = '" + userid + '\'' + 
			",maximum_user = '" + maximumUser + '\'' + 
			"}";
		}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}
}