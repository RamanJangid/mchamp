package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class WithdrawAmountResponseItem {

	@SerializedName("msg")
	private String msg;

	@SerializedName("amount")
	private double amount;

	@SerializedName("wining")
	private double wining;

	@SerializedName("status")
	private int status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setAmount(double amount){
		this.amount = amount;
	}

	public double getAmount(){
		return amount;
	}

	public void setWining(int wining){
		this.wining = wining;
	}

	public double getWining(){
		return wining;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"WithdrawAmountResponseItem{" +
			"msg = '" + msg + '\'' + 
			",amount = '" + amount + '\'' + 
			",wining = '" + wining + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}