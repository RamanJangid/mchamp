package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class UserDetailItem {

	@SerializedName("value")
	private UserDetailValue value;

	public void setValue(UserDetailValue value){
		this.value = value;
	}

	public UserDetailValue getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"UserDetailItem{" +
			"value = '" + value + '\'' + 
			"}";
		}
}