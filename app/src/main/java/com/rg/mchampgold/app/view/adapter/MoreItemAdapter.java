package com.rg.mchampgold.app.view.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.BuildConfig;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.MoreInfoData;
import com.rg.mchampgold.app.view.interfaces.OnMoreItemClickListener;
import com.rg.mchampgold.databinding.RecyclerItemMoreBinding;

import java.util.List;

public class MoreItemAdapter extends RecyclerView.Adapter<MoreItemAdapter.ViewHolder> {

    private List<MoreInfoData> moreInfoDataList;
    private OnMoreItemClickListener listener;
    private boolean isForPaymentOptions;


    public class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemMoreBinding binding;

        public ViewHolder(RecyclerItemMoreBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public MoreItemAdapter(List<MoreInfoData> moreInfoDataList, OnMoreItemClickListener listener,boolean isForPaymentOptions) {
        this.moreInfoDataList = moreInfoDataList;
        this.listener = listener;
        this.isForPaymentOptions = isForPaymentOptions;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerItemMoreBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_more,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setMoreInfo(moreInfoDataList.get(position));
//        if(isForPaymentOptions) {
//            holder.binding.imgForward.setVisibility(View.GONE);
//            holder.binding.tvName.setTextColor(Color.parseColor("#828282"));
//        }
//        else {
//            holder.binding.imgForward.setVisibility(View.VISIBLE);
//            holder.binding.tvName.setTextColor(Color.parseColor("#04263f"));
//        }
        holder.binding.ivMenu.setImageResource(moreInfoDataList.get(position).getResourceId());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMoreItemClick(position, moreInfoDataList.get(position).getName());
            }
        });
//        if (position==moreInfoDataList.size()-1){
//            holder.binding.item.setVisibility(View.GONE);
//            holder.binding.view.setVisibility(View.GONE);
//            holder.binding.versionName.setVisibility(View.VISIBLE);
//        }else {
//            holder.binding.versionName.setVisibility(View.GONE);
//            holder.binding.item.setVisibility(View.VISIBLE);
//            holder.binding.view.setVisibility(View.VISIBLE);
//        }
//        holder.binding.versionName.setText("Version "+ BuildConfig.VERSION_NAME +" ("+BuildConfig.VERSION_CODE+")");
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }



}