package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class LiveMatchesResponse {
    @SerializedName("matches")
    private ArrayList<MatchListItem> result;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;

    public ArrayList<MatchListItem> getResult() {
        return result;
    }

    public void setResult(ArrayList<MatchListItem> result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
