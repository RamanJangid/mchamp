package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PlayersItem{

	@SerializedName("role")
	private String role;

	@SerializedName("matchkey")
	private String matchkey;

	@SerializedName("name")
	private String name;

	@SerializedName("teamcode")
	private String teamcode;

	@SerializedName("vicecaptain")
	private int vicecaptain;

	@SerializedName("teamcolor")
	private String teamcolor;

	@SerializedName("id")
	private int id;

	@SerializedName("team")
	private String team;

	@SerializedName("captain")
	private int captain;

	@SerializedName("credit")
	private String credit;

	@SerializedName("points")
	private int points;

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setMatchkey(String matchkey){
		this.matchkey = matchkey;
	}

	public String getMatchkey(){
		return matchkey;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setTeamcode(String teamcode){
		this.teamcode = teamcode;
	}

	public String getTeamcode(){
		return teamcode;
	}

	public void setVicecaptain(int vicecaptain){
		this.vicecaptain = vicecaptain;
	}

	public int getVicecaptain(){
		return vicecaptain;
	}

	public void setTeamcolor(String teamcolor){
		this.teamcolor = teamcolor;
	}

	public String getTeamcolor(){
		return teamcolor;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTeam(String team){
		this.team = team;
	}

	public String getTeam(){
		return team;
	}

	public void setCaptain(int captain){
		this.captain = captain;
	}

	public int getCaptain(){
		return captain;
	}

	public void setCredit(String credit){
		this.credit = credit;
	}

	public String getCredit(){
		return credit;
	}

	public void setPoints(int points){
		this.points = points;
	}

	public int getPoints(){
		return points;
	}

	@Override
 	public String toString(){
		return 
			"PlayersItem{" + 
			"role = '" + role + '\'' + 
			",matchkey = '" + matchkey + '\'' + 
			",name = '" + name + '\'' + 
			",teamcode = '" + teamcode + '\'' + 
			",vicecaptain = '" + vicecaptain + '\'' + 
			",teamcolor = '" + teamcolor + '\'' + 
			",id = '" + id + '\'' + 
			",team = '" + team + '\'' + 
			",captain = '" + captain + '\'' + 
			",credit = '" + credit + '\'' + 
			",points = '" + points + '\'' + 
			"}";
		}
}