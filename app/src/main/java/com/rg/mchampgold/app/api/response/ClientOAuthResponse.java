package com.rg.mchampgold.app.api.response;

public class ClientOAuthResponse {
    public String access_token;
    public String token_type;
    public int expires_in;
    public String scope;
}
