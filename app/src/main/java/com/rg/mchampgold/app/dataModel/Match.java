package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Generated("com.robohorse.robopojogenerator")
public class Match {

    @SerializedName("team1display")
    private String team1display;

    @SerializedName("team1name")
    private String team1name;

    @SerializedName("team1logo")
    private String team1logo;

    @SerializedName("time_start")
    private String timeStart;

    @SerializedName("launch_status")
    private String launchStatus;

    @SerializedName("format")
    private String format;

    @SerializedName("matchkey")
    private String matchkey;

    @SerializedName("team2name")
    private String team2name;

    @SerializedName("team2logo")
    private String team2logo;

    @SerializedName("matchopenstatus")
    private String matchopenstatus;

    @SerializedName("match_status_key")
    private int matchStatusKey;

    @SerializedName("match_status")
    private String matchStatus;

    @SerializedName("seriesname")
    private String seriesname;

    @SerializedName("joined_count")
    private int joinedCount;

    @SerializedName("winnerstatus")
    private String winnerstatus;

    @SerializedName("final_status")
    private String finalStatus;

    @SerializedName("series")
    private int series;

    @SerializedName("name")
    private String name;

    @SerializedName("short_name")
    private String shortName;

    @SerializedName("id")
    private int id;

    @SerializedName("team2display")
    private String team2display;

    @SerializedName("lineup")
    private int lineup;

    @SerializedName("sport_key")
    private String sport_key;

    @SerializedName("team_count")
    private String teamCount;

    @SerializedName("winning_total")
    private String amount;


    @SerializedName("is_leaderboard")
    private int isLeaderboard;

    @SerializedName("toss_text")
    private String tossText;

    public String getAmount() {
        return "₹" + amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTeamCount() {

        return teamCount + " Team";
    }

    public void setTeamCount(String teamCount) {
        this.teamCount = teamCount;
    }

    public void setTeam1display(String team1display) {
        this.team1display = team1display;
    }

    public String getTeam1display() {
        return team1display;
    }

    public void setTeam1name(String team1name) {
        this.team1name = team1name;
    }

    public String getTeam1name() {
        return team1name;
    }

    public void setTeam1logo(String team1logo) {
        this.team1logo = team1logo;
    }

    public String getTeam1logo() {
        return team1logo;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setLaunchStatus(String launchStatus) {
        this.launchStatus = launchStatus;
    }

    public String getLaunchStatus() {
        return launchStatus;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    public void setMatchkey(String matchkey) {
        this.matchkey = matchkey;
    }

    public String getMatchkey() {
        return matchkey;
    }

    public void setTeam2name(String team2name) {
        this.team2name = team2name;
    }

    public String getTeam2name() {
        return team2name;
    }

    public void setTeam2logo(String team2logo) {
        this.team2logo = team2logo;
    }

    public String getTeam2logo() {
        return team2logo;
    }

    public void setMatchopenstatus(String matchopenstatus) {
        this.matchopenstatus = matchopenstatus;
    }

    public String getMatchopenstatus() {
        return matchopenstatus;
    }

    public void setMatchStatusKey(int matchStatusKey) {
        this.matchStatusKey = matchStatusKey;
    }

    public int getMatchStatusKey() {
        return matchStatusKey;
    }

    public void setMatchStatus(String matchStatus) {
        this.matchStatus = matchStatus;
    }

    public String getMatchStatus() {
        return matchStatus;
    }

    public void setSeriesname(String seriesname) {
        this.seriesname = seriesname;
    }

    public String getSeriesname() {
        return seriesname;
    }

    public void setJoinedCount(int joinedCount) {
        this.joinedCount = joinedCount;
    }

    public void setWinnerstatus(String winnerstatus) {
        this.winnerstatus = winnerstatus;
    }

    public String getWinnerstatus() {
        return winnerstatus;
    }

    public void setFinalStatus(String finalStatus) {
        this.finalStatus = finalStatus;
    }

    public String getFinalStatus() {
        return finalStatus;
    }

    public void setSeries(int series) {
        this.series = series;
    }

    public int getSeries() {
        return series;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setTeam2display(String team2display) {
        this.team2display = team2display;
    }

    public String getTeam2display() {
        return team2display;
    }

    public String getStartDate() {
        String string = timeStart;
        Date date1 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(string);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM,yyyy");
            return simpleDateFormat.format(date1);
        } catch (ParseException e) {
            return "";
        }
    }

    public String getSport_key() {
        return sport_key;
    }

    public void setSport_key(String sport_key) {
        this.sport_key = sport_key;
    }

    public String getJoinedCount() {
        return joinedCount + " Contests";
    }

    public int getLineup() {
        return lineup;
    }

    public void setLineup(int lineup) {
        this.lineup = lineup;
    }

    public int getIsLeaderboard() {
        return isLeaderboard;
    }

    public void setIsLeaderboard(int isLeaderboard) {
        this.isLeaderboard = isLeaderboard;
    }

    public String getTossText() {
        return tossText;
    }

    public void setTossText(String tossText) {
        this.tossText = tossText;
    }

    @Override
    public String toString() {
        return "Match{" +
                "team1display='" + team1display + '\'' +
                ", team1name='" + team1name + '\'' +
                ", team1logo='" + team1logo + '\'' +
                ", timeStart='" + timeStart + '\'' +
                ", launchStatus='" + launchStatus + '\'' +
                ", format='" + format + '\'' +
                ", matchkey='" + matchkey + '\'' +
                ", team2name='" + team2name + '\'' +
                ", team2logo='" + team2logo + '\'' +
                ", matchopenstatus='" + matchopenstatus + '\'' +
                ", matchStatusKey=" + matchStatusKey +
                ", matchStatus='" + matchStatus + '\'' +
                ", seriesname='" + seriesname + '\'' +
                ", joinedCount=" + joinedCount +
                ", winnerstatus='" + winnerstatus + '\'' +
                ", finalStatus='" + finalStatus + '\'' +
                ", series=" + series +
                ", name='" + name + '\'' +
                ", shortName='" + shortName + '\'' +
                ", id=" + id +
                ", team2display='" + team2display + '\'' +
                ", lineup=" + lineup +
                ", sport_key='" + sport_key + '\'' +
                ", teamCount='" + teamCount + '\'' +
                ", amount='" + amount + '\'' +
                ", isLeaderboard=" + isLeaderboard +
                ", tossText='" + tossText + '\'' +
                '}';
    }
}