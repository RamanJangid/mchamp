package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class RefreshScoreResponse{

	@SerializedName("result")
	private	RefreshScoreItem refreshScoreItem;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;


	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"RefreshScoreResponse{" + 
			",message = '" + message + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}

	public RefreshScoreItem getRefreshScoreItem() {
		return refreshScoreItem == null?new RefreshScoreItem():refreshScoreItem;
	}

	public void setRefreshScoreItem(RefreshScoreItem refreshScoreItem) {
		this.refreshScoreItem = refreshScoreItem;
	}
}