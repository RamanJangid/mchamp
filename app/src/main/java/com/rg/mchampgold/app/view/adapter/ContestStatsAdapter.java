package com.rg.mchampgold.app.view.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.MultiSportsPlayerPointItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.BreakupPlayerPointsActivity;
import com.rg.mchampgold.app.view.activity.UpComingContestDetailActivity;
import com.rg.mchampgold.databinding.RecyclerItemContestStatsBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public  class ContestStatsAdapter extends RecyclerView.Adapter<ContestStatsAdapter.ContestStatsHolder> {


    private List<MultiSportsPlayerPointItem> list;
    private UpComingContestDetailActivity upComingContestDetailActivity;

    public ContestStatsAdapter(UpComingContestDetailActivity upComingContestDetailActivity, ArrayList<MultiSportsPlayerPointItem> list) {
        this.upComingContestDetailActivity = upComingContestDetailActivity;
        this.list = list;
    }

    @NonNull
    @Override
    public ContestStatsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemContestStatsBinding recyclerItemContestStatsBinding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.recycler_item_contest_stats, parent, false);
        return new ContestStatsHolder(recyclerItemContestStatsBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ContestStatsHolder holder, int position) {
        holder.binding.setMultiSportsPlayerPointItem(list.get(position));
        AppUtils.loadImage(holder.binding.ivPlayer,list.get(position).getImage());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(upComingContestDetailActivity, BreakupPlayerPointsActivity.class);
            intent.putExtra("playerPointItem",list.get(position).getBreakup_points());
            intent.putExtra("playerName",list.get(position).getPlayer_name());
            intent.putExtra("selectedBy",list.get(position).showSelectedBy());
            intent.putExtra("point",list.get(position).getPoints());
            intent.putExtra("isSelected",list.get(position).getIsSelected());
            intent.putExtra("pImage",list.get(position).getImage());
            upComingContestDetailActivity.startActivity(intent);
        });

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ContestStatsHolder extends RecyclerView.ViewHolder {
        RecyclerItemContestStatsBinding binding;

        public ContestStatsHolder(RecyclerItemContestStatsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public void sortWithPoints(boolean flag) {
        if (flag) {
            Collections.sort(list, (contest, t1) -> Double.valueOf(contest.getPoints()).compareTo(Double.valueOf(t1.getPoints())));
        } else {
            Collections.sort(list, (contest, t1) -> Double.valueOf(t1.getPoints()).compareTo(Double.valueOf(contest.getPoints())));
        }
        notifyDataSetChanged();
    }

    public void sortWithSelectedBy(boolean flag) {
        if (flag) {
            Collections.sort(list, (contest, t1) -> Double.valueOf(contest.getSelected_by()).compareTo(Double.valueOf(t1.getSelected_by())));
        } else {
            Collections.sort(list, (contest, t1) -> Double.valueOf(t1.getSelected_by()).compareTo(Double.valueOf(contest.getSelected_by())));
        }
        notifyDataSetChanged();
    }

    public void sortWithCaptaindBy(boolean flag) {
        if (flag) {
            Collections.sort(list, (contest, t1) -> Double.valueOf(contest.getCaptainBy()).compareTo(Double.valueOf(t1.getCaptainBy())));
        } else {
            Collections.sort(list, (contest, t1) -> Double.valueOf(t1.getCaptainBy()).compareTo(Double.valueOf(contest.getCaptainBy())));
        }
        notifyDataSetChanged();
    }

    public void updateData(ArrayList<MultiSportsPlayerPointItem> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
