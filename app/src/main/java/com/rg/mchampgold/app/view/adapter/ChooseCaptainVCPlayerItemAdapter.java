package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.ChooseCandVCActivity;
import com.rg.mchampgold.app.view.interfaces.PlayerItemClickListener;
import com.rg.mchampgold.databinding.RecyclerItemCVcPlayerBinding;

import java.util.ArrayList;
import java.util.Collections;

public class ChooseCaptainVCPlayerItemAdapter extends RecyclerView.Adapter {

    private ArrayList<Player> playerList;
    private PlayerItemClickListener listener;
    Context context;
    String sportKey="";
    String selectedCaptainName = "";
    String selectedVcCaptainName = "";
    public boolean isCap=false,isVice=false;

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemCVcPlayerBinding binding;

        ViewHolder(RecyclerItemCVcPlayerBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public ChooseCaptainVCPlayerItemAdapter(String selectedCaptainName, String selectedVcCaptainName, Context context, ArrayList<Player> playerList, PlayerItemClickListener listener,String sportKey) {
        this.playerList = playerList;
        this.listener = listener;
        this.context = context;
        this.selectedCaptainName = selectedCaptainName;
        this.selectedVcCaptainName = selectedVcCaptainName;
        this.sportKey = sportKey;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemCVcPlayerBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_c_vc_player,
                        parent, false);
        return new ViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).binding.setPlayer(playerList.get(position));

        AppUtils.loadPlayerImage(((ViewHolder) holder).binding.ivPlayer, playerList.get(position).getImage());
        if (sportKey.equalsIgnoreCase("BASKETBALL")){
            if (playerList.get(position).isCaptain()){
                ((ChooseCandVCActivity) context).mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_golden);
                ((ChooseCandVCActivity) context).mBinding.btnCreateTeam.setTextColor(context.getResources().getColor(R.color.black));
            }
            ((ViewHolder) holder).binding.llVicecaptain.setVisibility(View.INVISIBLE);
        }else {
            if (playerList.get(position).isCaptain()){
                isCap=true;
            }
            if (playerList.get(position).isVcCaptain()){
                isVice=true;
            }
            if (isCap&&isVice){
                ((ChooseCandVCActivity) context).mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_golden);
                ((ChooseCandVCActivity) context).mBinding.btnCreateTeam.setTextColor(context.getResources().getColor(R.color.black));
            }
        }
        ((ViewHolder) holder).binding.captain.setOnClickListener(v -> {
            for (int i = 0; i < playerList.size(); i++) {
                playerList.get(i).setCaptain(false);
            }

            if (playerList.get(position).isVcCaptain()) {
                isVice=false;
                selectedVcCaptainName = "";
                playerList.get(position).setVcCaptain(false);
            }

            playerList.get(position).setCaptain(true);

            selectedCaptainName = playerList.get(position).getName();

            if (context instanceof ChooseCandVCActivity)
                ((ChooseCandVCActivity) context).setCaptainVcCaptionName(selectedCaptainName, selectedVcCaptainName, playerList);
            notifyDataSetChanged();

        });

        ((ViewHolder) holder).binding.vicecaptain.setOnClickListener(view -> {
            for (int i = 0; i < playerList.size(); i++) {
                playerList.get(i).setVcCaptain(false);

            }
            if (playerList.get(position).isCaptain()) {
                isCap=false;
                selectedCaptainName = "";
                playerList.get(position).setCaptain(false);
            }

            playerList.get(position).setVcCaptain(true);
            selectedVcCaptainName = playerList.get(position).getName();
            if (context instanceof ChooseCandVCActivity)
                ((ChooseCandVCActivity) context).setCaptainVcCaptionName(selectedCaptainName, selectedVcCaptainName, playerList);
            notifyDataSetChanged();
        });


        ((ViewHolder) holder).binding.ivPlayer.setOnClickListener(view -> {
            if (context instanceof ChooseCandVCActivity)
                ((ChooseCandVCActivity) context).openPlayerInfoActivity(playerList.get(position).getId() + "", playerList.get(position).getName(), playerList.get(position).getTeam(), playerList.get(position).getImage());
        });


        /*((ViewHolder)holder).binding.playerInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((ViewHolder)holder).binding.ivPlayer.performClick();
                }
         });*/

        ((ViewHolder) holder).binding.executePendingBindings();

    }


    @Override
    public int getItemCount() {
        return playerList.size();
    }


    public void updateData(ArrayList<Player> list) {
        playerList = list;
        notifyDataSetChanged();
    }

    public void sortWithPoints(boolean flag) {
        if (flag) {
            Collections.sort(playerList, (contest, t1) -> Double.valueOf(contest.getSeriesPoints()).compareTo(t1.getSeriesPoints()));
        } else {
            Collections.sort(playerList, (contest, t1) -> Double.valueOf(t1.getSeriesPoints()).compareTo(contest.getSeriesPoints()));
        }
        notifyDataSetChanged();
    }


    public void sortWithCaptain(boolean flag) {
        if (flag) {
            Collections.sort(playerList, (contest, t1) -> Double.valueOf(contest.getCaptain_selected_by()).compareTo(Double.valueOf(t1.getCaptain_selected_by())));
        } else {
            Collections.sort(playerList, (contest, t1) -> Double.valueOf(t1.getCaptain_selected_by()).compareTo(Double.valueOf(contest.getCaptain_selected_by())));
        }
        notifyDataSetChanged();
    }

    public void sortWithViceCaptain(boolean flag) {
        if (flag) {
            Collections.sort(playerList, (contest, t1) -> Double.valueOf(contest.getVice_captain_selected_by()).compareTo(Double.valueOf(t1.getVice_captain_selected_by())));
        } else {
            Collections.sort(playerList, (contest, t1) -> Double.valueOf(t1.getVice_captain_selected_by()).compareTo(Double.valueOf(contest.getVice_captain_selected_by())));
        }
        notifyDataSetChanged();
    }
}