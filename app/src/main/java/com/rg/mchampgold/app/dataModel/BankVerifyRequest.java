package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class BankVerifyRequest{

	@SerializedName("image")
	private String image;

	@SerializedName("accno")
	private String accno;

	@SerializedName("caccno")
	private String caccno;

	@SerializedName("user_id")
	private String user_id;

	@SerializedName("bankname")
	private String bankname;

	@SerializedName("state")
	private String state;

	@SerializedName("ifsc")
	private String ifsc;

	@SerializedName("bankbranch")
	private String bankbranch;


	@SerializedName("ac_holder_name")
	private String acHolderName;

	public String getAcHolderName() {
		return acHolderName;
	}

	public void setAcHolderName(String acHolderName) {
		this.acHolderName = acHolderName;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setAccno(String accno){
		this.accno = accno;
	}

	public String getAccno(){
		return accno;
	}

	public void setCaccno(String caccno){
		this.caccno = caccno;
	}

	public String getCaccno(){
		return caccno;
	}


	public void setBankname(String bankname){
		this.bankname = bankname;
	}

	public String getBankname(){
		return bankname;
	}

	public void setState(String state){
		this.state = state;
	}

	public String getState(){
		return state;
	}

	public void setIfsc(String ifsc){
		this.ifsc = ifsc;
	}

	public String getIfsc(){
		return ifsc;
	}


	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getBankbranch() {
		return bankbranch;
	}

	public void setBankbranch(String bankbranch) {
		this.bankbranch = bankbranch;
	}
}