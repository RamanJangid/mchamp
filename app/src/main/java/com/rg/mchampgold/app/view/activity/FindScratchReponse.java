package com.rg.mchampgold.app.view.activity;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class FindScratchReponse {


    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;


    @SerializedName("open")
    private int open;

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setStatus(int status){
        this.status = status;
    }

    public int getStatus(){
        return status;
    }

    public int getOpen() {
        return open;
    }

    public void setOpen(int promo_id) {
        this.open = promo_id;
    }
}