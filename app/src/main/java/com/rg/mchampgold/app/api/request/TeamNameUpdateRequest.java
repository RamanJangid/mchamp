package com.rg.mchampgold.app.api.request;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class TeamNameUpdateRequest {

	@SerializedName("user_id")
	private String user_id;

	@SerializedName("teamname")
	private String teamname;

	@SerializedName("state")
	private String state;
	@SerializedName("user_refer_code")
	private String userReferCode;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getTeamname() {
		return teamname;
	}

	public void setTeamname(String teamname) {
		this.teamname = teamname;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getUserReferCode() {
		return userReferCode;
	}

	public void setUserReferCode(String userReferCode) {
		this.userReferCode = userReferCode;
	}
}