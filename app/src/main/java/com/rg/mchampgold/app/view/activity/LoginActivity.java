package com.rg.mchampgold.app.view.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.LoginRequest;
import com.rg.mchampgold.app.api.request.SocialLoginRequest;
import com.rg.mchampgold.app.api.response.LoginResponse;
import com.rg.mchampgold.app.api.response.RegisterResponse;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.common.utils.NetworkUtils;
import com.rg.mchampgold.databinding.ActivityLoginBinding;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.inject.Inject;

import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding mainBinding;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1001;
    int passwordNotVisible = 0;
    private CallbackManager callbackManager;
    @Inject
    OAuthRestService oAuthRestService;
    String fcmToken = "";
    String deviceId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        callbackManager = CallbackManager.Factory.create();
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_FCM_TOKEN, instanceIdResult.getToken());
            }
        });
        initialize();
        MyApplication.getAppComponent().inject(LoginActivity.this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        PrintHashKey();
    }


    private void initialize() {

        AppUtils.disableCopyPaste(mainBinding.etEmail);
        AppUtils.disableCopyPaste(mainBinding.etPassword);

        mainBinding.showPass.setOnClickListener(view -> {
            if (!mainBinding.etPassword.getText().toString().trim().equals("")) {
                if (passwordNotVisible == 0) {
                    mainBinding.etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    mainBinding.showPass.setImageResource(R.drawable.ic_password_view);
                    passwordNotVisible = 1;
                } else {
                    mainBinding.showPass.setImageResource(R.drawable.view);
                    mainBinding.etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordNotVisible = 0;
                }
                mainBinding.etPassword.setSelection(mainBinding.etPassword.length());
            }
        });

        mainBinding.btnLogin.setOnClickListener(v -> {


            String email = mainBinding.etEmail.getText().toString().trim();
            String password = mainBinding.etPassword.getText().toString().trim();

            if (email.isEmpty() && password.isEmpty()) {
                Toast.makeText(MyApplication.appContext, "Please enter Email address and Password.", Toast.LENGTH_SHORT).show();
                return;
            } else if (email.isEmpty()) {
                Toast.makeText(MyApplication.appContext, "Enter a valid user id.", Toast.LENGTH_SHORT).show();
                return;
            } else if (email.contains("@") && !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                Toast.makeText(MyApplication.appContext, "Invalid Email Id.", Toast.LENGTH_SHORT).show();
                return;
            } else if (password.isEmpty()) {
                //mainBinding.etPassword.setError("Please Fill The Password.");
                Toast.makeText(MyApplication.appContext, "Please enter Password.", Toast.LENGTH_SHORT).show();
                return;
            }

            if (NetworkUtils.isNetworkAvailable())
                loginUser();
            else {
                AppUtils.showErrorr(this, getString(R.string.internet_off));
            }

        });
        mainBinding.tvRegisterNow.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));

        mainBinding.tvForgotPassword.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class)));

        mainBinding.btnGoogleLogin.setOnClickListener(v -> {
                    fcmToken = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_FCM_TOKEN);
                    if (NetworkUtils.isNetworkAvailable()) {
                        googleSignIn();
                    } else {
                        AppUtils.showErrorr(this, getString(R.string.internet_off));
                    }
                }
        );

        mainBinding.btnFacebookLogin.setOnClickListener(v -> {
            fcmToken = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_FCM_TOKEN);
            if (NetworkUtils.isNetworkAvailable()) {

                LoginManager.getInstance().logOut();

                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                SocialLoginRequest socialLoginRequest = new SocialLoginRequest();
                                GraphRequest request = GraphRequest.newMeRequest(
                                        loginResult.getAccessToken(),
                                        (object, response) -> {
                                            Log.v("FBLoginActivity", response.toString());
                                            JSONObject json = response.getJSONObject();
                                            //socialLoginRequest.setEmail(json.optString("email").equals("")?json.optString("id")+"@facebook.com":json.optString("email"));
                                            socialLoginRequest.setEmail(json.optString("email"));
                                            socialLoginRequest.setName(json.optString("name"));
                                            socialLoginRequest.setImage("https://graph.facebook.com/" + json.optString("id") + "/picture?width=150&width=150");
                                            socialLoginRequest.setSocialLoginType("facebook");
                                            socialLoginRequest.setFcmToken(fcmToken);
                                            socialLoginRequest.setSocial_id(json.optString("id"));
                                            socialLoginRequest.setDeviceId(deviceId);
                                            loginUserWithSocial(socialLoginRequest);

                                        });

                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,name,email");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                Toast.makeText(LoginActivity.this, "Login Cancelled.", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                Toast.makeText(LoginActivity.this, "Can't connect facebook, Please try later !! ", Toast.LENGTH_SHORT).show();
                            }
                        }
                );
            } else {
                AppUtils.showErrorr(this, getString(R.string.internet_off));
            }
        });

    }


    // google
    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, task -> {
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {

                //  Log.e("id", account.getId() + "");
                SocialLoginRequest loginRequest = new SocialLoginRequest();
                loginRequest.setName(account.getDisplayName());
                loginRequest.setEmail(account.getEmail());
                if (account.getPhotoUrl() != null)
                    loginRequest.setImage(account.getPhotoUrl().toString());
                loginRequest.setIdToken(account.getIdToken());
                loginRequest.setSocialLoginType("gmail");
                loginRequest.setFcmToken(fcmToken);
                loginRequest.setDeviceId(deviceId);
                loginRequest.setSocial_id(account.getId());
                loginUserWithSocial(loginRequest);
            } else {
                Toast.makeText(LoginActivity.this, "Sorry unable to fetch details", Toast.LENGTH_SHORT).show();
                return;
            }
            signOut();
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }


    private void PrintHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.print("Key hash is : " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }


    // Login user with Server.
    private void loginUser() {
        fcmToken = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_FCM_TOKEN);
        mainBinding.setRefreshing(true);
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail(mainBinding.etEmail.getText().toString().trim());
        loginRequest.setPassword(mainBinding.etPassword.getText().toString().trim());
        loginRequest.setFcmToken(fcmToken);
        loginRequest.setDeviceId(deviceId);
        CustomCallAdapter.CustomCall<LoginResponse> userLogin = oAuthRestService.userLogin(loginRequest);
        userLogin.enqueue(new CustomCallAdapter.CustomCallback<LoginResponse>() {
            @Override
            public void success(Response<LoginResponse> response) {
                mainBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    LoginResponse loginResponse = response.body();
                    if (loginResponse.getStatus() == 1) {
                        MyApplication.tinyDB.putBoolean(Constants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_ID, loginResponse.getResult().getUserId() + "");
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_NAME, loginResponse.getResult().getUsername());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_MOBILE, loginResponse.getResult().getMobile());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_EMAIL, loginResponse.getResult().getEmail());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TOKEN, loginResponse.getResult().getCustomUserToken());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_REFER_CODE, loginResponse.getResult().getRefercode());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TEAM_NAME, loginResponse.getResult().getTeamName());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_PIC, loginResponse.getResult().getUserProfileImage());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, loginResponse.getResult().getBankVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, loginResponse.getResult().getPanVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, loginResponse.getResult().getMobileVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, loginResponse.getResult().getEmailVerify());
                        MyApplication.tinyDB.putString(Constants.AUTHTOKEN, loginResponse.getResult().getCustomUserToken());
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mainBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void loginUserWithSocial(SocialLoginRequest socialLoginRequest) {
        mainBinding.setRefreshing(true);
        CustomCallAdapter.CustomCall<RegisterResponse> userLogin = oAuthRestService.userLoginSocial(socialLoginRequest);
        userLogin.enqueue(new CustomCallAdapter.CustomCallback<RegisterResponse>() {
            @Override
            public void success(Response<RegisterResponse> response) {
                mainBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    RegisterResponse registerResponse = response.body();
                    if (registerResponse.getStatus() == 1) {
                        MyApplication.tinyDB.putBoolean(Constants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_ID, registerResponse.getResult().getUserId() + "");
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_NAME, registerResponse.getResult().getUsername());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_MOBILE, registerResponse.getResult().getMobile());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_EMAIL, registerResponse.getResult().getEmail());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TOKEN, registerResponse.getResult().getCustomUserToken());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_PIC, registerResponse.getResult().getUserProfileImage());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_REFER_CODE, registerResponse.getResult().getRefercode());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TEAM_NAME, registerResponse.getResult().getTeamName());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, registerResponse.getResult().getBankVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, registerResponse.getResult().getPanVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, registerResponse.getResult().getMobileVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, registerResponse.getResult().getEmailVerify());
                        MyApplication.tinyDB.putString(Constants.AUTHTOKEN, registerResponse.getResult().getCustomUserToken());
                        MyApplication.refer_show = registerResponse.getResult().getRefer_show();
                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        if (socialLoginRequest.getSocialLoginType().equalsIgnoreCase("facebook"))
                            LoginManager.getInstance().logOut();
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mainBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

}