package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class WinnerBreakUpData {

	@SerializedName("rank")
	private String rank;

	@SerializedName("winningPer")
	private String winningPer;

	@SerializedName("winningAmmount")
	private String winningAmmount;


	public String getRank() {
		return rank;
	}

	public String showRank() {
		return "#"+rank;
	}

	public String showWinningPer() {
		return winningPer;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getWinningPer() {
		return winningPer;
	}

	public void setWinningPer(String winningPer) {
		this.winningPer = winningPer;
	}

	public String getWinningAmmount() {
		return winningAmmount;
	}
	public String showWinningAmmount() {
		return "₹ "+winningAmmount;
	}

	public void setWinningAmmount(String winningAmmount) {
		this.winningAmmount = winningAmmount;
	}
}