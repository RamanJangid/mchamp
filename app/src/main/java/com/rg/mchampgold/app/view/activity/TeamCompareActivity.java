package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.TeamCompareModel;
import com.rg.mchampgold.app.dataModel.TeamCompareResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.TeamCompareAdapter;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.TeamCompareLayoutBinding;

import javax.inject.Inject;

import retrofit2.Response;

public class TeamCompareActivity extends AppCompatActivity {
    TeamCompareAdapter adapter;
    TeamCompareLayoutBinding mBinding;
    public TeamCompareModel model;
    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.team_compare_layout);
        MyApplication.getAppComponent().inject(this);
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.compare_team));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        AppUtils.setStatusBarColor(this,0);
        TeamCompareData();
    }
    private void setupRecyclerView() {
        adapter =new TeamCompareAdapter(model.getCompare_c_vc(),model.getCommon_players(),model.getOther_players(),getApplicationContext(),getIntent().getStringExtra(Constants.KEY_TEAM_ID),getIntent().getStringExtra(Constants.KEY_TEAM_ID2),this,
                getIntent().getStringExtra(Constants.SPORT_KEY));
        mBinding.playersRecycler.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mBinding.playersRecycler.setLayoutManager(mLayoutManager);
        mBinding.playersRecycler.setAdapter(adapter);
        //getData();
    }
    private void setUpTeamData(){
        AppUtils.loadImage(mBinding.team1Image,model.getTeam1_image());
        AppUtils.loadImage(mBinding.team2Image,model.getTeam2_image());
        mBinding.team1Name.setText(model.getTeam1_name());
        mBinding.team2Name.setText(model.getTeam2_name());
        mBinding.team1Points.setText(model.getTeam1_points());
        mBinding.team2Points.setText(model.getTeam2_points());
        mBinding.team1Rank.setText(model.getTeam1_rank());
        mBinding.team2rank.setText(model.getTeam2_rank());
        mBinding.diffText.setText(model.getDiff_text());
        mBinding.diffPoints.setText(model.getDiff_points());
    }
    public void TeamCompareData() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setMatchkey(getIntent().getStringExtra(Constants.KEY_MATCH_KEY));
        baseRequest.setSport_key(getIntent().getStringExtra(Constants.SPORT_KEY));
        baseRequest.setTeam1_id(getIntent().getStringExtra(Constants.KEY_TEAM_ID));
        baseRequest.setTeam2_id(getIntent().getStringExtra(Constants.KEY_TEAM_ID2));
        baseRequest.setChallenge_id(getIntent().getIntExtra("challengeId",0));
        CustomCallAdapter.CustomCall<TeamCompareResponse> orderIdResponse = oAuthRestService.compare(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<TeamCompareResponse>() {
            @Override
            public void success(Response<TeamCompareResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    TeamCompareResponse bannerListResponse = response.body();
                    if (bannerListResponse.getStatus() == 1) {
                        model=bannerListResponse.getResult();
                        setUpTeamData();
                        setupRecyclerView();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
