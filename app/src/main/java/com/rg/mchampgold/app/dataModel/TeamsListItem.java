package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class TeamsListItem {

    @SerializedName("id")
    int id;
    @SerializedName("team_id")
    int team_id;
    @SerializedName("matchkey")
    String matchkey;
    @SerializedName("name")
    String name;
    @SerializedName("short_name")
    String short_name;
    @SerializedName("scores_full")
    String scores_full;
    @SerializedName("scores")
    int scores;
    @SerializedName("overs")
    float overs;
    @SerializedName("image")
    String image;
    @SerializedName("created_at")
    String created_at;
    @SerializedName("updated_at")
    String updated_at;
    @SerializedName("scores_inning_1")
    String scores_inning_1;
    @SerializedName("overs_inning_1")
    String overs_inning_1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getMatchkey() {
        return matchkey;
    }

    public void setMatchkey(String matchkey) {
        this.matchkey = matchkey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getScores_full() {
        return scores_full;
    }

    public void setScores_full(String scores_full) {
        this.scores_full = scores_full;
    }

    public int getScores() {
        return scores;
    }

    public void setScores(int scores) {
        this.scores = scores;
    }

    public float getOvers() {
        return overs;
    }

    public void setOvers(float overs) {
        this.overs = overs;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getScores_inning_1() {
        return scores_inning_1;
    }

    public void setScores_inning_1(String scores_inning_1) {
        this.scores_inning_1 = scores_inning_1;
    }

    public String getOvers_inning_1() {
        return overs_inning_1;
    }

    public void setOvers_inning_1(String overs_inning_1) {
        this.overs_inning_1 = overs_inning_1;
    }
}
