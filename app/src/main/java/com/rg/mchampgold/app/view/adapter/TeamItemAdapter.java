package com.rg.mchampgold.app.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.Team;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.MyTeamsActivity;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.RecyclerItemTeamBinding;

import java.util.ArrayList;
import java.util.List;

public class TeamItemAdapter extends RecyclerView.Adapter<TeamItemAdapter.ViewHolder> {

    private int multiEntry, maxTeamLimit,
            selectedTeamCount = 0;
    private boolean isSwitchTeam;
    private List<Team> teamList;
    private Context context;
    private boolean isForJoinContest;
    int teamId;
    String sportKey = "";
    private int joinedCount;

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemTeamBinding binding;

        ViewHolder(RecyclerItemTeamBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public TeamItemAdapter(List<Team> teamList, Context context, boolean isForJoinContest, String sportKey, int multiEntry, boolean isSwitchTeam, int maxTeamLimit) {
        this.teamList = teamList;
        this.context = context;
        this.sportKey = sportKey;
        this.isForJoinContest = isForJoinContest;
        this.multiEntry = multiEntry;
        this.isSwitchTeam = isSwitchTeam;
        this.maxTeamLimit = maxTeamLimit;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemTeamBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_team,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setTeam(teamList.get(position));

        AppUtils.loadImage(holder.binding.ivCaptain, teamList.get(position).captainImage());
        AppUtils.loadImage(holder.binding.ivVcaptain, teamList.get(position).vcCaptainImage());

        holder.binding.cloneLL.setOnClickListener(view -> {
            if (context instanceof MyTeamsActivity)
                ((MyTeamsActivity) context).editOrClone(teamList.get(position).getPlayers(), 0,"Team " + teamList.get(position).getTeamnumber());
            else if (context instanceof UpComingContestActivity)
                ((UpComingContestActivity) context).editOrClone(teamList.get(position).getPlayers(), 0,"Team " + teamList.get(position).getTeamnumber());

        });

        if (sportKey.equalsIgnoreCase("BASKETBALL")) {
            holder.binding.rlVicecaptain.setVisibility(View.GONE);
        }

        if (teamList.size() >= 11) {
            holder.binding.cloneLL.setVisibility(View.GONE);
        } else {
            holder.binding.cloneLL.setVisibility(View.VISIBLE);
        }

        holder.binding.editLL.setOnClickListener(view -> {
            if (context instanceof MyTeamsActivity)
                ((MyTeamsActivity) context).editOrClone(teamList.get(position).getPlayers(), teamList.get(position).getTeamid(),"Team " + teamList.get(position).getTeamnumber());
            else if (context instanceof UpComingContestActivity)
                ((UpComingContestActivity) context).editOrClone(teamList.get(position).getPlayers(), teamList.get(position).getTeamid(),"Team " + teamList.get(position).getTeamnumber());
        });


        if (isForJoinContest) {
            holder.binding.radioButton.setVisibility(View.VISIBLE);
            holder.binding.llEditClonePreview.setVisibility(View.GONE);
            if (teamList.get(position).isSelected()) {
                holder.binding.radioButton.setChecked(true);
                holder.binding.viewSelected.setBackground(context.getResources().getDrawable(R.drawable.join_background_selector_));
            } else {
                holder.binding.viewSelected.setBackground(null);
//                holder.binding.viewSelected.setBackground(context.getResources().getDrawable(R.drawable.join_background_unselector_));
                holder.binding.radioButton.setChecked(false);
            }

        } else {
            holder.binding.viewSelected.setBackground(null);
//            holder.binding.viewSelected.setBackground(context.getResources().getDrawable(R.drawable.join_background_unselector_));
            holder.binding.radioButton.setVisibility(View.GONE);
            holder.binding.llEditClonePreview.setVisibility(View.VISIBLE);
        }


        if (teamList.get(position).getIsJoined() == 1) {

            holder.binding.teamPreviewLayout.setAlpha(0.3f);
//            holder.binding.radioButton.setChecked(true);
            holder.binding.radioButton.setVisibility(View.INVISIBLE);
//            holder.binding.viewSelected.setBackground(context.getResources().getDrawable(R.drawable.join_background_selector_));

        } else {
//            if (teamList.size()==1){
//                holder.binding.radioButton.setChecked(true);
//                holder.binding.viewSelected.setBackground(context.getResources().getDrawable(R.drawable.join_background_selector_));
//            }
            holder.binding.teamPreviewLayout.setAlpha(1.0f);
            holder.binding.teamPreviewLayout.setOnClickListener(view -> {
                MyApplication.teamList = teamList.get(position).getPlayers();
                MyApplication.teamId = teamList.get(position).getTeamid();
                MyApplication.fromMyTeams = true;
                if (context instanceof MyTeamsActivity)
                    ((MyTeamsActivity) context).openPreviewActivity(teamList.get(position).getPlayers(), "Team " + teamList.get(position).getTeamnumber());
                else if (context instanceof UpComingContestActivity)
                    ((UpComingContestActivity) context).openPreviewActivity(teamList.get(position).getPlayers(), "Team " + teamList.get(position).getTeamnumber());

            });
        }


        holder.binding.teamCheckView.setOnCheckedChangeListener((compoundButton, b) -> {
            if (teamList.get(position).getIsJoined() != 1) {
                if (isForJoinContest) {
                    if (isSwitchTeam || multiEntry == 0) {

                        for (int i = 0; i < teamList.size(); i++) {
                            teamList.get(i).setSelected(false);
                        }
                        teamId = teamList.get(position).getTeamid();
                        teamList.get(position).setSelected(true);
                        selectedTeamCount = 1;
                    } else {

                        if (teamList.get(position).isSelected()) {
                            teamList.get(position).setSelected(false);
                            selectedTeamCount--;
//                            ((MyTeamsActivity) context).mBinding.btnJoinContest.setBackgroundColor(context.getResources().getColor(R.color.pink));
                        } else if (!teamList.get(position).isSelected() && maxTeamLimit != selectedTeamCount + joinedCount) {
                            teamList.get(position).setSelected(true);
                            selectedTeamCount++;
                        } else
                            AppUtils.showErrorr((AppCompatActivity) context, "You can only enter with upto " + maxTeamLimit + " teams");

                        if (context instanceof MyTeamsActivity) {
                            ((MyTeamsActivity) context).changeNoOfTeamText(selectedTeamCount);
                        }
                    }
                    notifyDataSetChanged();
                }
            }
        });

        holder.itemView.setOnClickListener(view -> holder.binding.teamCheckView.performClick());
        holder.binding.radioButton.setOnClickListener(v -> holder.binding.teamCheckView.performClick());

        if (sportKey.equalsIgnoreCase("BASKETBALL")) {
            holder.binding.numC.setVisibility(View.VISIBLE);
            holder.binding.numC.setText(CCount(position));
        } else {
            holder.binding.numC.setVisibility(View.GONE);
        }
        holder.binding.numWicketKeeper.setText(keeperCount(position));
        holder.binding.numBat.setText(batsmanCount(position));
        holder.binding.numAllrounder.setText(allRounderCount(position));
        holder.binding.numBowlers.setText(bolwerCount(position));

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    public void selectAll(boolean isSelectAll) {
        selectedTeamCount = 0;
        for (int i = 0; i < teamList.size(); i++) {
            teamList.get(i).setSelected(false);
            if (teamList.get(i).getIsJoined() != 1) {
                if (isSelectAll) {
                    teamList.get(i).setSelected(true);
                    selectedTeamCount++;
                }
            }
        }

        if (context instanceof MyTeamsActivity) {
            ((MyTeamsActivity) context).changeNoOfTeamText(selectedTeamCount);
        }

        notifyDataSetChanged();
    }

    public double getSelectedTeamCount() {
        if (teamList.size() == 1 && teamList.get(0).getIsJoined() != 1) {
            return 1;
        }
        return selectedTeamCount;
    }

    public void updateData(ArrayList<Team> list, int joinedCount) {
        teamList = list;
        selectedTeamCount = 0;
        this.joinedCount = joinedCount;
        notifyDataSetChanged();
    }


    public String getMultiTeamId() {
        StringBuilder stringBuilder = new StringBuilder();
//        if (teamList.size() == 1 && teamList.get(0).getIsJoined() != 1 && multiEntry == 0 && !isSwitchTeam) {
//            stringBuilder.append(teamList.get(0).getTeamid());
//        } else
            if (isSwitchTeam || multiEntry == 0) {
            if (teamId != 0) {
                stringBuilder.append(teamId);
            }
        } else if (multiEntry == 1) {
            for (int i = 0; i < teamList.size(); i++) {
                if (teamList.get(i).isSelected()) {
                    stringBuilder.append(teamList.get(i).getTeamid()).append(",");
                }
            }
        }

        return stringBuilder.toString().trim();
    }

    public int getSelectedTeamId() {
        return teamId;
    }

    public String keeperCount(int position) {
        int i = 0;
        String name = "WK ";
        for (Player player : teamList.get(position).getPlayers()) {
            if (sportKey.equalsIgnoreCase("CRICKET")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_KEEP))
                    i++;
                name = "WK ";
            } else if (sportKey.equalsIgnoreCase("FOOTBALL")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_GK))
                    i++;
                name = "GK ";
            } else {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PG))
                    i++;
                name = "PG ";
            }
        }
        return name + "(" + i + ")";
    }

    public String batsmanCount(int position) {
        int i = 0;
        String name = "BAT ";
        for (Player player : teamList.get(position).getPlayers()) {
            if (sportKey.equalsIgnoreCase("CRICKET")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BAT))
                    i++;
                name = "BAT ";
            } else if (sportKey.equalsIgnoreCase("FOOTBALL")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_DEF))
                    i++;
                name = "DEF ";
            } else {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SG))
                    i++;
                name = "SG ";
            }
        }
        return name + "(" + i + ")";
    }

    public String allRounderCount(int position) {
        int i = 0;
        String name = "AR ";
        for (Player player : teamList.get(position).getPlayers()) {
            if (sportKey.equalsIgnoreCase("CRICKET")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ALL_R))
                    i++;
                name = "AR ";
            } else if (sportKey.equalsIgnoreCase("FOOTBALL")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_MID))
                    i++;
                name = "MID ";
            } else {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SF))
                    i++;
                name = "SF ";
            }
        }
        return name + "(" + i + ")";
    }

    public String bolwerCount(int position) {
        int i = 0;
        String name = "BOWL ";
        for (Player player : teamList.get(position).getPlayers()) {
            if (sportKey.equalsIgnoreCase("CRICKET")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BOL))
                    i++;
                name = "BOWL ";
            } else if (sportKey.equalsIgnoreCase("FOOTBALL")) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ST))
                    i++;
                name = "ST ";
            } else {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PF))
                    i++;
                name = "PF ";
            }
        }
        return name + "(" + i + ")";
    }

    public String CCount(int position) {
        int i = 0;
        for (Player player : teamList.get(position).getPlayers()) {
            if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_C))
                i++;
        }
        return "C " + "(" + i + ")";
    }

}