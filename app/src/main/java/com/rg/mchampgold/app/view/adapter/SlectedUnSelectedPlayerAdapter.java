package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;

import com.rg.mchampgold.databinding.LayoutPlayerSelectedBinding;

public class SlectedUnSelectedPlayerAdapter extends RecyclerView.Adapter<SlectedUnSelectedPlayerAdapter.ViewHolder> {

    private int selectedPlayerCount;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        final LayoutPlayerSelectedBinding binding;

        public ViewHolder(LayoutPlayerSelectedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public SlectedUnSelectedPlayerAdapter(int selectedPlayerCount, Context context) {
        this.selectedPlayerCount = selectedPlayerCount;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutPlayerSelectedBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_player_selected,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position < selectedPlayerCount) {
            holder.binding.ivSelectedUnselected.setImageResource(R.drawable.ic_team_selected_progress);
            if (position + 1 == selectedPlayerCount) {
                holder.binding.tvPos.setText((position + 1) + "");
            } else {
                holder.binding.tvPos.setText("");
            }
        } else {
            holder.binding.ivSelectedUnselected.setImageResource(R.drawable.ic_team_unselected_progress);
            holder.binding.tvPos.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return 11;
    }

    public void update(int selectedPlayerCount) {
        this.selectedPlayerCount = selectedPlayerCount;
        notifyDataSetChanged();
    }


}