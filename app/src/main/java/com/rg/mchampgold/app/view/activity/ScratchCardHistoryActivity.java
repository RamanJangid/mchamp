package com.rg.mchampgold.app.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;

import android.view.MenuItem;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.response.ScratchHistoryResponse;
import com.rg.mchampgold.app.api.service.OAuthRestService;

import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.ScratchCardHistoryAdapter;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.common.utils.NetworkUtils;
import com.rg.mchampgold.databinding.ActivityScratchCardHistoryBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class ScratchCardHistoryActivity extends AppCompatActivity {

    ScratchCardHistoryAdapter mAdapter;
    public ActivityScratchCardHistoryBinding binding;

    @Inject
    public OAuthRestService oAuthRestService;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        binding = DataBindingUtil.setContentView(ScratchCardHistoryActivity.this, R.layout.activity_scratch_card_history);
        AppUtils.setStatusBarColor(ScratchCardHistoryActivity.this, Color.parseColor("#0d141a"));
        MyApplication.getAppComponent().inject(ScratchCardHistoryActivity.this);
        setSupportActionBar(binding.toolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        if (NetworkUtils.isNetworkAvailable())
            fetchScratchList();
    }

    private void fetchScratchList() {
        binding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<ScratchHistoryResponse> orderIdResponse = oAuthRestService.scratchCardList(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<ScratchHistoryResponse>() {
            @Override
            public void success(Response<ScratchHistoryResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        setUpRecycler(response.body());
                    } else {
                        binding.setRefreshing(false);
                        AppUtils.showErrorr(ScratchCardHistoryActivity.this, Constants.ERROR_MSG);
                    }
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                e.printStackTrace();
                binding.setRefreshing(false);
                AppUtils.showErrorr(ScratchCardHistoryActivity.this, e.getLocalizedMessage());
            }
        });
    }


    private void setUpRecycler(ScratchHistoryResponse result) {
        if (result.getResult().size() > 0) {
            binding.recyclerScratchHistory.setHasFixedSize(true);
            binding.recyclerScratchHistory.setLayoutManager(new GridLayoutManager(this,2));
            mAdapter = new ScratchCardHistoryAdapter(result, this);
            binding.recyclerScratchHistory.setAdapter(mAdapter);
        }
        binding.tvTotalEarnedAmount.setText(result.getTotal_amount() + "");
        binding.setRefreshing(false);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
