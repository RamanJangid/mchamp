package com.rg.mchampgold.app.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.SeriesLeaderboardDataModel;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.LeaderboardDetailsActivity;
import com.rg.mchampgold.app.view.interfaces.OnMoreItemClickListener;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.RankListItemLayoutBinding;

import java.util.ArrayList;

public class LeaderboardListAdapter extends RecyclerView.Adapter<LeaderboardListAdapter.ViewHolder> {

    private final boolean isDummyData;
    private ArrayList<SeriesLeaderboardDataModel> moreInfoDataList;
    private OnMoreItemClickListener listener;
    private boolean isForPaymentOptions;
    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        final RankListItemLayoutBinding binding;

        public ViewHolder(RankListItemLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public LeaderboardListAdapter(Context context, ArrayList<SeriesLeaderboardDataModel> moreInfoDataList, boolean isDummyData) {
        this.moreInfoDataList = moreInfoDataList;
        this.context=context;
        this.isDummyData = isDummyData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RankListItemLayoutBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.rank_list_item_layout,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setData(moreInfoDataList.get(position));
        AppUtils.loadImage(holder.binding.image,moreInfoDataList.get(position).getImage());

        if (moreInfoDataList.get(position).getUser_id().equals(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID))) {
            holder.binding.llUser.setBackgroundColor(Color.parseColor("#282726"));
        }

        holder.binding.clickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDummyData) {
                    Intent intent=new Intent(context, LeaderboardDetailsActivity.class);
                    intent.putExtra("series_id",moreInfoDataList.get(position).getSeries_id());
                    intent.putExtra("user_id",moreInfoDataList.get(position).getUser_id());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }
        });
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }



}