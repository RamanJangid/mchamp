package com.rg.mchampgold.app.dataModel;

import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class JoinedContesttItem {

    @SerializedName("is_private")
    private int isPrivate;

    @SerializedName("can_invite")
    private boolean canInvite;

    @SerializedName("totalwinners")
    private int totalwinners = 0;

    public String getFirst_rank_prize() {
        return first_rank_prize;
    }

    public void setFirst_rank_prize(String first_rank_prize) {
        this.first_rank_prize = first_rank_prize;
    }

    @SerializedName("entryfee")
    private int entryfee;

    @SerializedName("first_rank_prize")
    private String first_rank_prize = "0";

    @SerializedName("jointeams")
    private List<JointeamsItem> jointeams;

    @SerializedName("joinedusers")
    private int joinedusers;

    public String getChallenge_type() {
        return challenge_type;
    }

    public void setChallenge_type(String challenge_type) {
        this.challenge_type = challenge_type;
    }

    @SerializedName("getjoinedpercentage")
    private String getjoinedpercentage;


    @SerializedName("challenge_type")
    private String challenge_type = "";

    @SerializedName("challenge_id")
    private int challengeId;

    @SerializedName("win_amount")
    private int winamount;

    @SerializedName("matchkey")
    private String matchkey;

    @SerializedName("multi_entry")
    private int multiEntry;

    @SerializedName("joinid")
    private int joinid;

    @SerializedName("grand")
    private int grand;

    @SerializedName("confirmed_challenge")
    private int confirmed_challenge;

    @SerializedName("pricecardstatus")
    private int pricecardstatus;

    @SerializedName("ismarathon")
    private int ismarathon;

    @SerializedName("teamid")
    private int teamid;


    @SerializedName("dis_price")
    private String dis_price = "0";

    public String getDis_price() {
        return dis_price;
    }

    public void setDis_price(String dis_price) {
        this.dis_price = dis_price;
    }



    @SerializedName("is_free")
    private int is_free;

    public int getIs_free() {
        return is_free;
    }

    public void setIs_free(int is_free) {
        this.is_free = is_free;
    }

    @SerializedName("refercode")
    private String refercode;

    @SerializedName("is_bonus")
    private int isBonus;

    @SerializedName("name")
    private String name;

    @SerializedName("userrank")
    private int userrank;

    @SerializedName("maximum_user")
    private int maximumUser;

    @SerializedName("status")
    private int status;

    @SerializedName("bonus_percent")
    private String bonusPercent;

    @SerializedName("isjoined")
    private boolean isjoined;

    @SerializedName("winning_percentage")
    private String winning_percentage = "";

    @SerializedName("announcement")
    private String announcement = "";

    @SerializedName("max_team_limit")
    private int maxTeamLimit;

    @SerializedName("wd")
    private int wd;

    @SerializedName("user_joined_count")
    private int user_joined_count;

    @SerializedName("category")
    private String category;

    public int getMaxTeamLimit() {
        return maxTeamLimit;
    }

    public void setMaxTeamLimit(int maxTeamLimit) {
        this.maxTeamLimit = maxTeamLimit;
    }

    public String getWinning_percentage() {
        return winning_percentage;
    }

    public void setWinning_percentage(String winning_percentage) {
        this.winning_percentage = winning_percentage;
    }


    public void setIsPrivate(int isPrivate) {
        this.isPrivate = isPrivate;
    }

    public int getIsPrivate() {
        return isPrivate;
    }

    public void setCanInvite(boolean canInvite) {
        this.canInvite = canInvite;
    }

    public boolean isCanInvite() {
        return canInvite;
    }

    public void setTotalwinners(int totalwinners) {
        this.totalwinners = totalwinners;
    }

    public int getTotalwinners() {
        return totalwinners;
    }

    public void setEntryfee(int entryfee) {
        this.entryfee = entryfee;
    }

    public int getEntryfee() {
        return entryfee;
    }

    public void setJointeams(List<JointeamsItem> jointeams) {
        this.jointeams = jointeams;
    }

    public List<JointeamsItem> getJointeams() {
        return jointeams;
    }

    public void setJoinedusers(int joinedusers) {
        this.joinedusers = joinedusers;
    }

    public int getJoinedusers() {
        return joinedusers;
    }

    public void setGetjoinedpercentage(String getjoinedpercentage) {
        this.getjoinedpercentage = getjoinedpercentage;
    }

    public String getGetjoinedpercentage() {
        return getjoinedpercentage;
    }

    public void setChallengeId(int challengeId) {
        this.challengeId = challengeId;
    }

    public int getChallengeId() {
        return challengeId;
    }

    public void setWinamount(int winamount) {
        this.winamount = winamount;
    }

    public int getWinamount() {
        return winamount;
    }

    public void setMatchkey(String matchkey) {
        this.matchkey = matchkey;
    }

    public String getMatchkey() {
        return matchkey;
    }

    public void setMultiEntry(int multiEntry) {
        this.multiEntry = multiEntry;
    }

    public int getMultiEntry() {
        return multiEntry;
    }

    public void setJoinid(int joinid) {
        this.joinid = joinid;
    }

    public int getJoinid() {
        return joinid;
    }

    public void setGrand(int grand) {
        this.grand = grand;
    }

    public int getGrand() {
        return grand;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed_challenge = confirmed;
    }

    public int getConfirmed() {
        return confirmed_challenge;
    }

    public void setPricecardstatus(int pricecardstatus) {
        this.pricecardstatus = pricecardstatus;
    }

    public int getPricecardstatus() {
        return pricecardstatus;
    }

    public void setIsmarathon(int ismarathon) {
        this.ismarathon = ismarathon;
    }

    public int getIsmarathon() {
        return ismarathon;
    }

    public void setTeamid(int teamid) {
        this.teamid = teamid;
    }

    public int getTeamid() {
        return teamid;
    }

    public void setRefercode(String refercode) {
        this.refercode = refercode;
    }

    public String getRefercode() {
        return refercode;
    }

    public void setIsBonus(int isBonus) {
        this.isBonus = isBonus;
    }

    public int getIsBonus() {
        return isBonus;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setUserrank(int userrank) {
        this.userrank = userrank;
    }

    public int getUserrank() {
        return userrank;
    }

    public void setMaximumUser(int maximumUser) {
        this.maximumUser = maximumUser;
    }

    public int getMaximumUser() {
        return maximumUser;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "JoinedContesttItem{" +
                        "is_private = '" + isPrivate + '\'' +
                        ",can_invite = '" + canInvite + '\'' +
                        ",totalwinners = '" + totalwinners + '\'' +
                        ",entryfee = '" + entryfee + '\'' +
                        ",jointeams = '" + jointeams + '\'' +
                        ",joinedusers = '" + joinedusers + '\'' +
                        ",getjoinedpercentage = '" + getjoinedpercentage + '\'' +
                        ",challenge_id = '" + challengeId + '\'' +
                        ",winamount = '" + winamount + '\'' +
                        ",matchkey = '" + matchkey + '\'' +
                        ",multi_entry = '" + multiEntry + '\'' +
                        ",joinid = '" + joinid + '\'' +
                        ",grand = '" + grand + '\'' +
                        ",confirmed = '" + confirmed_challenge + '\'' +
                        ",pricecardstatus = '" + pricecardstatus + '\'' +
                        ",ismarathon = '" + ismarathon + '\'' +
                        ",teamid = '" + teamid + '\'' +
                        ",refercode = '" + refercode + '\'' +
                        ",is_bonus = '" + isBonus + '\'' +
                        ",name = '" + name + '\'' +
                        ",userrank = '" + userrank + '\'' +
                        ",maximum_user = '" + maximumUser + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    public String showWinningAmout() {
        return "₹" + winamount;
    }

    public String showJoinAmount() {
//        if (isjoined)
//            if (multiEntry == 1)
//                return "JOIN+";
//            else
//                return "INVITE";
//        else
            return "₹" + entryfee;
    }

    public String showLeftusers() {
        return "Only " + (maximumUser - joinedusers) + " Spots left";
    }

    public String showTotalUsers() {
        return maximumUser + " Teams";
    }


    public String showTotalWinners() {
        return totalwinners + " Team Win";
    }

    //for bonous

    public boolean isShowBTag() {
        return isBonus == 1 ? true : false;
    }
    //for confirm

    public boolean isShowCTag() {
        return confirmed_challenge == 1 ? true : false;
    }
    //for multi user

    public boolean isShowWDTag() {
        return wd == 1 ? true : false;
    }
    //for multi user

    public boolean isShowMTag() {
        return multiEntry == 1 ? true : false;
    }

    public String totalWinnersSt() {
        return totalwinners + "";
    }

    public String getBonusPercent() {
        return bonusPercent;
    }

    public void setBonusPercent(String bonusPercent) {
        this.bonusPercent = bonusPercent;
    }

    public boolean isIsjoined() {
        return isjoined;
    }

    public void setIsjoined(boolean isjoined) {
        this.isjoined = isjoined;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public boolean isAnnounced() {
        if (!announcement.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public String showMultiEntries() {
        if (multiEntry == 0) {
            return "Single Entry";
        } else {
            return "Upto 11 Entries";
        }
    }

    public int getWd() {
        return wd;
    }

    public void setWd(int wd) {
        this.wd = wd;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getConfirmed_challenge() {
        return confirmed_challenge;
    }

    public void setConfirmed_challenge(int confirmed_challenge) {
        this.confirmed_challenge = confirmed_challenge;
    }

    public int getUser_joined_count() {
        return user_joined_count;
    }

    public void setUser_joined_count(int user_joined_count) {
        this.user_joined_count = user_joined_count;
    }
}