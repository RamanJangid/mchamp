package com.rg.mchampgold.app.view.interfaces;

public interface OnMatchItemClickListener {
    void onMatchItemClick(String matchKey, String teamVsName, String teamFirstUrl, String teamSecondUrl, String date, int position, int isLeaderboard, int series);
}
