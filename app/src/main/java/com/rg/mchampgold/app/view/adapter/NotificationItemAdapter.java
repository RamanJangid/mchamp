package com.rg.mchampgold.app.view.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.NotificationItem;
import com.rg.mchampgold.databinding.RecyclerItemNotificationBinding;

import java.util.ArrayList;
import java.util.List;

public class NotificationItemAdapter extends RecyclerView.Adapter<NotificationItemAdapter.ViewHolder> {

    private List<NotificationItem> notificationItems;


    public class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemNotificationBinding binding;

        public ViewHolder(RecyclerItemNotificationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public NotificationItemAdapter(List<NotificationItem> notificationItems) {
        this.notificationItems = notificationItems;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerItemNotificationBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_notification,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setNotification(notificationItems.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return notificationItems.size();
    }

    public void update(ArrayList<NotificationItem> notificationItems) {
        this.notificationItems = notificationItems;
        notifyDataSetChanged();
    }
}