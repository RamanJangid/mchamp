package com.rg.mchampgold.app.view.adapter;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.MultiSportsPlayerBreakPointItem;
import com.rg.mchampgold.databinding.MultisportsplayerbreakpointsitemBinding;

import java.util.List;

public class MultiSportsPlayerPointBreakAdapter extends RecyclerView.Adapter<MultiSportsPlayerPointBreakAdapter.ViewHolder> {

    private List<MultiSportsPlayerBreakPointItem> playerPointItems;
    Activity activity;


    class ViewHolder extends RecyclerView.ViewHolder {
        final MultisportsplayerbreakpointsitemBinding binding;

        ViewHolder(MultisportsplayerbreakpointsitemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public MultiSportsPlayerPointBreakAdapter(Activity activity,List<MultiSportsPlayerBreakPointItem> playerPointItems) {
        this.playerPointItems = playerPointItems;
        this.activity = activity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MultisportsplayerbreakpointsitemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.multisportsplayerbreakpointsitem,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setMultiSportsPlayerBreakPointItem(playerPointItems.get(position));
        holder.binding.executePendingBindings();
    }
    @Override
    public int getItemCount() {
        return playerPointItems.size();
    }


}