package com.rg.mchampgold.app.view.adapter;


import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.response.ScratchHistoryResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.FindScratchReponse;
import com.rg.mchampgold.app.view.activity.InviteFriendActivity;
import com.rg.mchampgold.app.view.activity.ScratchCardHistoryActivity;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.RecyclerItemScratchHistoryBinding;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import in.myinnos.androidscratchcard.ScratchCard;
import retrofit2.Response;

public class ScratchCardHistoryAdapter extends RecyclerView.Adapter<ScratchCardHistoryAdapter.ViewHolder> {

    private ScratchHistoryResponse scratchAmountArrayList;
    private Context context;


    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemScratchHistoryBinding binding;

        ViewHolder(RecyclerItemScratchHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public ScratchCardHistoryAdapter(ScratchHistoryResponse transactionItems, Context context) {
        this.scratchAmountArrayList = transactionItems;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemScratchHistoryBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_scratch_history,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setScratch(scratchAmountArrayList.getResult().get(position));
        holder.binding.tvEarnedAmount.setText(context.getString(R.string.rupee) + scratchAmountArrayList.getResult().get(position).getCouponAmount());
        holder.binding.ivScratch.setVisibility(scratchAmountArrayList.getResult().get(position).getIsScratched() == 1 ? View.GONE : View.VISIBLE);
        holder.binding.linearScratchView.setVisibility(scratchAmountArrayList.getResult().get(position).getIsScratched() == 1 ? View.VISIBLE : View.GONE);
        holder.binding.linearScratchViewDiamond.setOnClickListener(view -> {
            if (scratchAmountArrayList.getResult().get(position).getIsScratched() != 1) {
                showScratchCardDialog(position);
            } else {
                showAlreadyScratchCardDialog(position);
            }
        });

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return scratchAmountArrayList.getResult().size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void updateData(ArrayList<ScratchHistoryResponse.Result> list) {
        scratchAmountArrayList.setResult(list);
        notifyDataSetChanged();
    }

    private void showScratchCardDialog(int position) {
        Dialog dialogue = new Dialog(context);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog_scratch_card);
        dialogue.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogue.setTitle(null);
        dialogue.findViewById(R.id.iv_scratch_close).setOnClickListener(view -> dialogue.dismiss());
//        int amount = new Random().nextInt(((scratchAmountArrayList.getMaxScratchAmount() - scratchAmountArrayList.getMinScratchAmount()) + 1) + scratchAmountArrayList.getMinScratchAmount());
        int amount=0;
        if(scratchAmountArrayList.getMaxScratchAmount().equals( scratchAmountArrayList.getMinScratchAmount())) {
            amount = scratchAmountArrayList.getMaxScratchAmount();
        }
        else {
            //return r.nextInt((max - min) + 1) + min;
            amount = new Random().nextInt((scratchAmountArrayList.getMaxScratchAmount() - scratchAmountArrayList.getMinScratchAmount())+1)+ scratchAmountArrayList.getMinScratchAmount();
        }
        TextView tvEarnedAmount = dialogue.findViewById(R.id.tv_earned_amount);
        tvEarnedAmount.setText(context.getString(R.string.rupee) + amount);
        ScratchCard scratchCard = dialogue.findViewById(R.id.iv_scratch);
        int finalAmount = amount;
        scratchCard.setOnScratchListener((scratchCard1, visiblePercent) -> {
            if (visiblePercent > 0.1) {
                // if(amount>0) {
                scratchCard1.setVisibility(View.GONE);
                //   dialogue.linear_scratch_view.visibility = VISIBLE
                //    dialogue.tv_t_c_scratch.visibility = VISIBLE
                //surprise amount added by pkb

                if (finalAmount > 0) {
                    dialogue.findViewById(R.id.iv_tv_share_friends).setVisibility(View.VISIBLE);
                    dialogue.findViewById(R.id.tv_t_c_scratch).setVisibility(View.VISIBLE);

                    notifyDataSetChanged();

                    dialogue.findViewById(R.id.iv_tv_share_friends).setOnClickListener(view -> {
                        dialogue.dismiss();
                        context.startActivity(new Intent(MyApplication.appContext, InviteFriendActivity.class));
                    });


                    call_AddScratch_Card(dialogue, finalAmount, scratchAmountArrayList.getResult().get(position).getId(), position);
                } else {
                    tvEarnedAmount.setTextSize(20f);
                    tvEarnedAmount.setText(context.getString(R.string.str_better_luck));
                    scratchAmountArrayList.getResult().remove(position);
                    notifyDataSetChanged();
                }

                // }
            }
        });

        //  dialogue.tv_scratch_title.text = data.scratch_card_title

        if (dialogue.isShowing())
            dialogue.dismiss();
        if (isActivityRunning(context.getPackageName())) {
            // if (scratch_card_is_enable)
            dialogue.show();
        }
    }


    private void showAlreadyScratchCardDialog(int position) {
        Dialog dialogue = new Dialog(context);
        dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogue.setContentView(R.layout.dialog_scratch_card);
        dialogue.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogue.setTitle(null);
        dialogue.findViewById(R.id.iv_scratch_close).setOnClickListener(view -> dialogue.dismiss());

        TextView tvEarnedAmount = dialogue.findViewById(R.id.tv_earned_amount);
        tvEarnedAmount.setText(context.getString(R.string.rupee) + scratchAmountArrayList.getResult().get(position).getCouponAmount());
        ScratchCard scratchCard = dialogue.findViewById(R.id.iv_scratch);
        scratchCard.setVisibility(View.GONE);

        if (Float.parseFloat(scratchAmountArrayList.getResult().get(position).getCouponAmount()) > 0) {
            dialogue.findViewById(R.id.iv_tv_share_friends).setVisibility(View.VISIBLE);
            dialogue.findViewById(R.id.tv_t_c_scratch).setVisibility(View.VISIBLE);
            TextView tvDate = dialogue.findViewById(R.id.tv_t_c_scratch);
            try {
                Date scratchedDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(scratchAmountArrayList.getResult().get(position).getUpdatedAt());
                tvDate.setText("Paid on " + new SimpleDateFormat("dd MMMM yyyy").format(scratchedDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            dialogue.findViewById(R.id.iv_tv_share_friends).setOnClickListener(view -> {
                dialogue.dismiss();
                context.startActivity(new Intent(MyApplication.appContext, InviteFriendActivity.class));
            });

        } else {
            tvEarnedAmount.setTextSize(20f);
            tvEarnedAmount.setText(context.getString(R.string.str_better_luck));
        }


        if (dialogue.isShowing())
            dialogue.dismiss();
        if (isActivityRunning(context.getPackageName())) {
            // if (scratch_card_is_enable)
            dialogue.show();
        }
    }

    private boolean isActivityRunning(String packageName) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equals(packageName);
    }


    private void call_AddScratch_Card(Dialog dialog, double amount, Integer list_id, int position) {

        ((ScratchCardHistoryActivity) context).binding.setRefreshing(true);
        AddScratchRequest baseRequest = new AddScratchRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setList_id(list_id);
        baseRequest.setAmount(amount);
        CustomCallAdapter.CustomCall<FindScratchReponse> orderIdResponse = ((ScratchCardHistoryActivity) context).oAuthRestService.openScratchCard(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<FindScratchReponse>() {
            @Override
            public void success(Response<FindScratchReponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {
                        ((ScratchCardHistoryActivity) context).binding.setRefreshing(false);
                        scratchAmountArrayList.getResult().get(position).setIsScratched(1);
                        scratchAmountArrayList.getResult().get(position).setCouponAmount(amount + "");
                        TextView tvDate = dialog.findViewById(R.id.tv_t_c_scratch);
                        tvDate.setText("Paid on " + new SimpleDateFormat("dd MMMM yyyy").format(new Date()));
                        ((ScratchCardHistoryActivity) context).binding.tvTotalEarnedAmount.setText((scratchAmountArrayList.getTotal_amount() + amount) + "");
                        notifyDataSetChanged();

                    }
                    // dialog.dismiss();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                e.printStackTrace();
                ((ScratchCardHistoryActivity) context).binding.setRefreshing(true);
                AppUtils.showErrorr(((ScratchCardHistoryActivity) context), e.getLocalizedMessage());
            }
        });
    }


}