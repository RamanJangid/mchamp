package com.rg.mchampgold.app.view.fragment.home;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;


import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.rg.mchampgold.BuildConfig;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.BannerListItem;
import com.rg.mchampgold.app.dataModel.BannerListResponse;
import com.rg.mchampgold.app.dataModel.Match;
import com.rg.mchampgold.app.dataModel.MatchListResponse;
import com.rg.mchampgold.app.dataModel.SportType;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.HomeActivity;
import com.rg.mchampgold.app.view.activity.SetTeamNameActivity;
import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.adapter.MatchItemAdapter;
import com.rg.mchampgold.app.view.adapter.SliderBannerAdapter;
import com.rg.mchampgold.app.view.fragment.upComing.UpComingMatchListViewModel;
import com.rg.mchampgold.app.view.interfaces.OnMatchItemClickListener;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.api.Resource;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentHomeBinding;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import retrofit2.Response;


public class HomeFragment extends Fragment implements OnMatchItemClickListener {

    private FragmentHomeBinding fragmentHomeBinding;

    @Inject
    OAuthRestService oAuthRestService;
    private UpComingMatchListViewModel upComingMatchListViewModel;
    private MatchItemAdapter mAdapter;
    public ArrayList<Match> list = new ArrayList<>();
    ArrayList<BannerListItem> bannerListItems = new ArrayList<>();
    SwipeRefreshLayout swipe_refresh_layout;
    int currentPage = 0;
    Timer timer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 5000;
    String sportKey = "CRICKET";
    int currentVersion=0;
    private int onlineVersion = 0;
    private TextView deSelectTab;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentHomeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        /*ArrayList<SportType> list=new ArrayList<>();
        SportType sportType=new SportType();
        sportType.setSportName("CRICKET");
        sportType.setSport_key("CRICKET");
        list.add(sportType);
        SportType sportType2=new SportType();
        sportType2.setSportName("BASKETBALL");
        sportType2.setSport_key("BASKETBALL");
        list.add(sportType2);
        SportType sportType1=new SportType();
        sportType1.setSportName("FOOTBALL");
        sportType1.setSport_key("FOOTBALL");
        list.add(sportType1);
        setSportsCategory(list);*/

        fragmentHomeBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            upComingMatchListViewModel.getSearchData().removeObservers(HomeFragment.this);
            getData(upComingMatchListViewModel.getSearchData());
        });
        return fragmentHomeBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        upComingMatchListViewModel = UpComingMatchListViewModel.create(this);
        MyApplication.getAppComponent().inject(upComingMatchListViewModel);
        MyApplication.tinyDB.putString(Constants.SPORT_KEY, sportKey);
        getBannerList();
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        mAdapter = new MatchItemAdapter(getActivity(), list, this, this);
        fragmentHomeBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        fragmentHomeBinding.recyclerView.setLayoutManager(mLayoutManager);
        fragmentHomeBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void setupViewPager(ViewPager viewPager) {
   /*     ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new UpcomingMatchFragment(), getString(R.string.upcoming));
        adapter.addFrag(new LiveMatchFragment(), getString(R.string.live));
        adapter.addFrag(new FinishedMatchFragment(), getString(R.string.finished));
        viewPager.setAdapter(adapter);*/
        //fragmentHomeBinding.tabLayout.setupWithViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void getBannerList() {
        //  fragmentHomeBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_M_USER_ID));
        CustomCallAdapter.CustomCall<BannerListResponse> bankDetailResponseCustomCall = oAuthRestService.getBannerList(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<BannerListResponse>() {
            @Override
            public void success(Response<BannerListResponse> response) {
                //    mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    BannerListResponse bannerListResponse = response.body();
                    if (bannerListResponse.getStatus() == 1 && bannerListResponse.getResult().size() > 0) {
                        bannerListItems = bannerListResponse.getResult();
                        if (bannerListResponse.getNotification().equalsIgnoreCase("1")) {
                            if (getActivity() != null && isAdded()) {
                                if (((HomeActivity) getActivity()).homeMenu != null)
                                    ((HomeActivity) getActivity()).homeMenu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.notification_dot));
                            }
                        }
                        if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
                            if (getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof HomeFragment) {
                                fragmentHomeBinding.viewPagerBanner.setAdapter(new SliderBannerAdapter(getActivity(), bannerListItems));
                                fragmentHomeBinding.tabDots.setupWithViewPager(fragmentHomeBinding.viewPagerBanner);
                                autoScroll();
                            }
                        }

                        if (bannerListResponse.getPopup_status().equalsIgnoreCase("1") && MyApplication.isPopVisible) {
                            MyApplication.tinyDB.putBoolean(Constants.IS_POPUP_IMAGE_SHOWING, true);
                            showPopUpImage(bannerListResponse.getPopup_image());
                        }

                        onlineVersion = bannerListResponse.getOnline_version();

                        if (bannerListResponse.getApi_base_url().trim().length() > 5)
                            MyApplication.base_api_url = bannerListResponse.getApi_base_url();

                        if (bannerListResponse.getBase_url().trim().length() > 5)
                            MyApplication.web_pages_url = bannerListResponse.getBase_url();

//                        currentVersion = BuildConfig.VERSION_CODE;

                        MyApplication.promo_code = bannerListResponse.getPromo_code();
                        MyApplication.refer_earn = bannerListResponse.getRefer_earn();
                        MyApplication.apk_url = bannerListResponse.getDownloadUrl();

//                        if (currentVersion < onlineVersion) {
//                            showAppUpdateDialog(bannerListResponse.getDownloadUrl(), bannerListResponse.getOnline_version_string(), bannerListResponse.getUpdateText());
//                        }
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_ID, bannerListResponse.getUser_id());
                        MyApplication.tinyDB.putString(Constants.AUTHTOKEN, bannerListResponse.getAuth_token());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TEAM_NAME,bannerListResponse.getUser_team_name());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_STATE,bannerListResponse.getUser_state());
                        if (MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_TEAM_NAME).equalsIgnoreCase("")||MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_STATE).equalsIgnoreCase("")) {
                            startActivity(new Intent(getActivity(), SetTeamNameActivity.class));
                        }
                        getData(upComingMatchListViewModel.getSearchData());
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                // mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void setSportsCategory(ArrayList<SportType> list) {
        String jsonList = new Gson().toJson(list);
        MyApplication.tinyDB.putString(Constants.SHARED_SPORTS_LIST, jsonList);
        if (list.size() > 1) {
            fragmentHomeBinding.sportTab.setVisibility(View.VISIBLE);
        } else {
            fragmentHomeBinding.sportTab.setVisibility(View.GONE);
        }
        for (int i = 0; i < list.size(); i++) {
            TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabOne.setText(list.get(i).getSportName());

            fragmentHomeBinding.sportTab.addTab(fragmentHomeBinding.sportTab.newTab().setText(list.get(i).getSportName()));
            if (list.get(i).getSportName().equalsIgnoreCase("CRICKET")) {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cricket_tab_selector, 0, 0);
                tabOne.setTextColor(getResources().getColor(R.color.pink));
                deSelectTab = tabOne;
            } else if (list.get(i).getSportName().equalsIgnoreCase("FOOTBALL")) {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.football_tab_selector, 0, 0);
            } else {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.basketball_tab_selector, 0, 0);
            }
            fragmentHomeBinding.sportTab.getTabAt(i).setCustomView(tabOne);
        }

        fragmentHomeBinding.sportTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                TextView textView = view.findViewById(R.id.tab);
                textView.setTextColor(getResources().getColor(R.color.pink));
                deSelectTab.setTextColor(getResources().getColor(R.color.unselected_tab));
                deSelectTab = textView;

                sportKey = list.get(tab.getPosition()).getSport_key();
                MyApplication.tinyDB.putString(Constants.SPORT_KEY, sportKey);

//                if (sportKey.equals(Constants.TAG_FOOTBALL)) {
//                    fragmentHomeBinding.llComingSoon.setVisibility(View.VISIBLE);
//                } else {
//                    fragmentHomeBinding.llComingSoon.setVisibility(View.GONE);
//
//                }
                upComingMatchListViewModel.getSearchData().removeObservers(HomeFragment.this);
                getData(upComingMatchListViewModel.getSearchData());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void showAppUpdateDialog(String download_url, String version_code, String updateText) {
        if (getActivity() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(updateText)
//            builder.setMessage("New Version(" + version_code + ") for " + getString(R.string.app_name) + " is available for download. Kindly update for latest features.")
                    .setCancelable(false)
                    .setPositiveButton("Update", (dialog, id) -> {
                        if (getActivity() instanceof HomeActivity) {
                            new DownloadFileFromUrl().execute(download_url);
                        }

                    });
            AlertDialog alert = builder.create();
            alert.setCanceledOnTouchOutside(false);
            alert.setCancelable(false);
            alert.setOnCancelListener(dialogInterface -> getActivity().finish());
            alert.show();
        }
    }

    @Override
    public void onMatchItemClick(String matchKey, String teamVsName, String teamFirstUrl, String teamSecondUrl, String timerStatus, int position, int isLeaderboard, int series) {
        //   Log.e("Upcoming",matchKey);
        Intent intent = new Intent(getActivity(), UpComingContestActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, timerStatus);
        intent.putExtra(Constants.SPORT_KEY, list.get(position).getSport_key());
        intent.putExtra(Constants.KEY_SERIES_ID, series);
        intent.putExtra(Constants.KEY_IS_LEADERBOARD, isLeaderboard);
        startActivity(intent);
    }

    private void getData(LiveData<Resource<MatchListResponse>> liveData) {
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setSport_key(MyApplication.tinyDB.getSportKey(Constants.SPORT_KEY));
        upComingMatchListViewModel.load(baseRequest);
        liveData.observe(getActivity(), arrayListResource -> {
            //   Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    fragmentHomeBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    if (fragmentHomeBinding.swipeRefreshLayout != null)
                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    fragmentHomeBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    if (fragmentHomeBinding.swipeRefreshLayout != null)
                        fragmentHomeBinding.swipeRefreshLayout.setRefreshing(false);
                    fragmentHomeBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1) {
                        list = arrayListResource.getData().getResult();
                        if (list.size() > 0) {
                            mAdapter.updateData(list);
                            fragmentHomeBinding.rlNoMatch.setVisibility(View.GONE);
                        } else {
                            fragmentHomeBinding.rlNoMatch.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }

    private void autoScroll() {
        final Handler handler = new Handler();
        final Runnable Update = () -> {

            if (currentPage == bannerListItems.size()) {
                currentPage = 0;
            }

            fragmentHomeBinding.viewPagerBanner.setCurrentItem(currentPage, true);
            currentPage = currentPage + 1;
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);
    }


    public class DownloadFileFromUrl extends AsyncTask<String, String, String> {
        File file = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress();
        }

        @Override
        protected String doInBackground(String... strings) {
            int count = 0;
            try {
                URL url = new URL(strings[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length

                //   Log.e("url", url.getPath());
                int lenghtOfFile = connection.getContentLength();
                BufferedInputStream input = new BufferedInputStream(url.openStream());

                String root = Environment.getExternalStorageDirectory().toString();
                File myDir = new File(root + "/Android/data/" + getActivity().getPackageName() + "/cache/");
                myDir.mkdirs();
                file = new File(myDir, getString(R.string.app_name) + ".apk");
                Log.i("file ", "" + file);
                if (file.exists())
                    file.delete();
                // Output stream to write file
                FileOutputStream output = new FileOutputStream(file);

                //   int data = new ByteArray(1024);
                byte data[] = new byte[1024];


                Long total = (long) 0.0;
                while (count != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    //  pDialog.setProgress(Integer.parseInt(values[0]));
                    publishProgress("" + (total * 100 / lenghtOfFile));
                    //   Log.e("progress", "" + (total * 100 / lenghtOfFile));
                    count = input.read(data);
                    // writing data to file
                    output.write(data, 0, count);

                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if (file != null) {
                if (Build.VERSION.SDK_INT <= 23) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                    intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                    intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setDataAndType(FileProvider.getUriForFile(getActivity(),
                            getActivity().getPackageName() + ".provider",
                            file), "application/vnd.android.package-archive");
                    startActivity(intent);
                }
            }
        }


        @Override
        protected void onProgressUpdate(String... values) {
            pDialog.setProgress(Integer.parseInt(values[0]));
        }
    }

    ProgressDialog pDialog = null;

    private void showProgress() {
        pDialog = new ProgressDialog(getActivity(), R.style.RoundedCornersDialog);
        pDialog.setMessage("Downloading file. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void showPopUpImage(String image) {
        if (getActivity() != null && isAdded()) {

            Dialog dialogue = new Dialog(getActivity());
            dialogue.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogue.setContentView(R.layout.popup_image_dialog);
            dialogue.getWindow().setLayout(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
            dialogue.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogue.setCancelable(false);
            dialogue.setCanceledOnTouchOutside(false);
            dialogue.setTitle(null);
            ImageView imageView = dialogue.findViewById(R.id.image);
            CardView img_Close = dialogue.findViewById(R.id.img_Close);
            AppUtils.loadPopupImage(imageView, image);
            img_Close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                MyApplication.tinyDB.putBoolean(Constants.IS_POPUP_IMAGE_SHOWING,false);
                    MyApplication.isPopVisible = false;
                    dialogue.dismiss();
                }
            });
            if (dialogue.isShowing())
                dialogue.dismiss();

            dialogue.show();
        }


    }

    public void callAdapter() {
        mAdapter = new MatchItemAdapter(getActivity(), list, this, this);
        Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (!fragmentHomeBinding.recyclerView.isComputingLayout()) {
                    fragmentHomeBinding.recyclerView.setAdapter(mAdapter);
                } else {
                    handler.postDelayed(this::run, 200);
                }
            }
        };
        handler.postDelayed(runnable, 200);
        if (list.size() > 0) {
            fragmentHomeBinding.rlNoMatch.setVisibility(View.GONE);
        } else {
            fragmentHomeBinding.rlNoMatch.setVisibility(View.VISIBLE);
        }
    }
}