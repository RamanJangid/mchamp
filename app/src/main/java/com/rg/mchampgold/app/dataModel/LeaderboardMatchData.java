package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class LeaderboardMatchData {

    @SerializedName("points")
    String points;
    @SerializedName("match_name")
    String match_name;
    @SerializedName("match_date")
    String match_date;

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getMatch_name() {
        return match_name;
    }

    public void setMatch_name(String match_name) {
        this.match_name = match_name;
    }

    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }
}
