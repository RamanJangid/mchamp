package com.rg.mchampgold.app.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.ReferBonusListResponse;
import com.rg.mchampgold.app.dataModel.ReferLIstItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.ReferFriendItemAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityInvitedFriendBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class InvitedFriendActivity extends AppCompatActivity {

    ActivityInvitedFriendBinding binding;
    String userReferCode = "";
    @Inject
    OAuthRestService oAuthRestService;

    String totalAmount = "0";
    String totalUser = "0";

    ArrayList<ReferLIstItem> arrayList;
    ReferFriendItemAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_invited_friend);
        MyApplication.getAppComponent().inject(InvitedFriendActivity.this);
        initialize();
        userReferCode = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_REFER_CODE);

        /*if(getIntent()!=null) {
          totalAmount = getIntent().getExtras().getDouble("amount");
          arrayList = (ArrayList<ReferLIstItem>) getIntent().getExtras().getSerializable("list");
        }*/
        if (arrayList != null) {
            if (arrayList.size() > 0) {
                binding.noRefer.setVisibility(View.GONE);
                binding.totalAmount.setText("₹ " + totalAmount);
                mAdapter = new ReferFriendItemAdapter(arrayList);
                binding.recyclerView.setHasFixedSize(true);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
                binding.recyclerView.setLayoutManager(mLayoutManager);
                binding.recyclerView.setAdapter(mAdapter);
            } else {
                binding.totalAmount.setText("₹ " + totalAmount);
                binding.totalRefer.setText( totalUser);
                binding.noRefer.setVisibility(View.VISIBLE);
            }
        } else {
            binding.totalAmount.setText("₹ " + totalAmount);
            binding.totalRefer.setText( totalUser);
            binding.noRefer.setVisibility(View.VISIBLE);
        }

        getReferBonusList();

    }

    void initialize() {
        setSupportActionBar(binding.linearToolBar.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.refer_list));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    public void inviteCodeFriend() {
        String shareBody =
                "Hi, Inviting you to join "+Constants.APP_NAME+" and play fantasy sports to win cash daily.\n" +
                        "Use Refer code-" + userReferCode +
                        "\nYou can get upto ₹ "+MyApplication.signup_bonus+" signup bonus as a gift from "+Constants.APP_NAME+". Download App from ~ " + MyApplication.apk_url + ".";

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    private void getReferBonusList() {
        binding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<ReferBonusListResponse> bankDetailResponseCustomCall = oAuthRestService.getReferBonusList(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<ReferBonusListResponse>() {
            @Override
            public void success(Response<ReferBonusListResponse> response) {
                binding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    ReferBonusListResponse referBonusListResponse = response.body();
                    if (referBonusListResponse.getStatus() == 1 && referBonusListResponse.getResult().size() > 0) {
                        //  double amount = 0.0;
                        arrayList = referBonusListResponse.getResult();
                        if (arrayList.size() > 0) {
                            totalAmount = referBonusListResponse.getTotalAmount();
                            totalUser = referBonusListResponse.getTotalUser();
                            mAdapter = new ReferFriendItemAdapter(arrayList);
                            binding.recyclerView.setHasFixedSize(true);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                            binding.recyclerView.setLayoutManager(mLayoutManager);
                            binding.recyclerView.setAdapter(mAdapter);
                            binding.totalAmount.setText("₹ " + totalAmount);
                            binding.totalRefer.setText(totalUser);
                            binding.noRefer.setVisibility(View.GONE);
                        } else {
                            binding.totalRefer.setText(totalUser);
                            binding.noRefer.setVisibility(View.VISIBLE);
                        }
                        // binding.totalAmountTxt.setText("₹ "+ amount);
                        //  binding.totalUserJoinTxt.setText(""+referBonusListResponse.getResult().size()+" friends joined!");
                    } else {
                        binding.totalAmount.setText("₹ " + totalAmount);
                        binding.totalRefer.setText( totalUser);
                        //AppUtils.mShowDialogd(InviteFriendActivity.this,referBonusListResponse.getMessage());
                        //     binding.totalAmountTxt.setText("₹ 0.0");
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                binding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }
}
