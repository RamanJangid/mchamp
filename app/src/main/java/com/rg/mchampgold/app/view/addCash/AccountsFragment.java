package com.rg.mchampgold.app.view.addCash;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.MyBalanceResponse;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.dataModel.UserImageUploadResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.AddBalanceActivity;
import com.rg.mchampgold.app.view.activity.HomeActivity;
import com.rg.mchampgold.app.view.activity.LoginActivity;
import com.rg.mchampgold.app.view.activity.PersonalDetailsActivity;
import com.rg.mchampgold.app.view.activity.ScratchCardHistoryActivity;
import com.rg.mchampgold.app.view.activity.VerifyAccountActivity;
import com.rg.mchampgold.app.view.activity.WithdrawCashActivity;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentAccountsBinding;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class AccountsFragment extends Fragment {

    private FragmentAccountsBinding fragmentMoreBinding;
    TabAdapter mAdapter;
    private MyBalanceResultItem myBalanceResultItem;

    String fileName = "";
    String Simage = "";
    private String fileImage = "";


    private static int GALLERY_REQUEST_CODE = 100;
    private static int CAMERA_REQUEST_CODE = 101;

    @Inject
    OAuthRestService oAuthRestService;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMoreBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_accounts, container, false);
        fragmentMoreBinding.btnEditProfile.setOnClickListener(view -> startActivity(new Intent(getActivity(), PersonalDetailsActivity.class)));
        fragmentMoreBinding.btnVerifyAccount.setOnClickListener(view -> startActivity(new Intent(getActivity(), VerifyAccountActivity.class)));

        ((HomeActivity) getActivity()).setToolBarTitle("Account");

        fragmentMoreBinding.btnAddCash.setOnClickListener(view -> {
            if (myBalanceResultItem != null) {
                startActivity(new Intent(getActivity(), AddBalanceActivity.class)
                        .putExtra("balance", myBalanceResultItem.getBalance()).putExtra("bonus", myBalanceResultItem.getBonus()));
            }
        });

        fragmentMoreBinding.btnWithdraw.setOnClickListener(view -> {
            if (isNotVerified()) {
                showNotVerifiedAlert();
            } else {
                startActivity(new Intent(getActivity(), WithdrawCashActivity.class));
            }
        });

        String user_name = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_NAME);
        String email = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL);
        String mobile = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_MOBILE);
        String status = isNotVerified() ? "&#10006; "+Constants.UNVERIFIED : "&#10003; "+Constants.VERIFIED;
        int color = isNotVerified() ? getActivity().getColor(R.color.colorThemeRed) : getActivity().getColor(R.color.colorThemeGreen);

        fragmentMoreBinding.tvUserName.setText(user_name.equals("") ? "Not Available" : user_name);
        fragmentMoreBinding.tvEmail.setText(email.equals("") ? "Not Available" : email);
        fragmentMoreBinding.tvMobile.setText(mobile.equals("") ? "Not Available" : mobile);
        fragmentMoreBinding.tvStatus.setText(Html.fromHtml(status));
        fragmentMoreBinding.tvStatus.setBackgroundColor(color);

        AppUtils.loadImageCircle(fragmentMoreBinding.ivUserProfile, MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_PIC));

      /*  fragmentMoreBinding.cardViewWallet.cardView.setOnClickListener(view -> {
            ((HomeActivity) getActivity()).setToolBarTitle("My Balance");
            ((HomeActivity) getActivity()).loadFragmentStack(new BalanceFragment());
        });*/


        fragmentMoreBinding.cardViewPlayingHistory.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), PlayingHistoryActivity.class));
        });


        fragmentMoreBinding.cardViewTransactionHistory.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), TransactionsActivity.class));
        });

        fragmentMoreBinding.cardViewShowScratchCard.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), ScratchCardHistoryActivity.class));
        });


        fragmentMoreBinding.ivUserProfile.setOnClickListener(view -> {
            if (getActivity().checkCallingOrSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    getActivity().checkCallingOrSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                showImageSelectionDialog();
            else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 22);
            }
        });

        /* fragmentMoreBinding.unutilizedTxt.setText("₹"+myBalanceResultItem.getBalance());
        fragmentMoreBinding.winningsTxt.setText("₹"+myBalanceResultItem.getWinning());
        fragmentMoreBinding.cashBonusTxt.setText("₹"+myBalanceResultItem.getBonus());
        fragmentMoreBinding.totalBalanceTxt.setText("₹"+myBalanceResultItem.getTotal());*/
        /* mAdapter = new TabAdapter(getFragmentManager());
        mAdapter.addFragment(new BalanceFragment(), "Balance");
        mAdapter.addFragment(new PlayingHistoryFragment(), "Playing History");
        mAdapter.addFragment(new TransactionsFragment(), "Transactions");
        fragmentMoreBinding.viewPager.setAdapter(mAdapter);
        fragmentMoreBinding.tabLayout.setupWithViewPager(fragmentMoreBinding.viewPager);*/


        fragmentMoreBinding.cardViewLogout.setOnClickListener(view -> showLogoutDialog());

        return fragmentMoreBinding.getRoot();
    }


    private void showLogoutDialog() {
        android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(getActivity());

        // set title
        alertDialogBuilder.setTitle("Logout");

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you want to logout?")
                .setCancelable(false)
                .setPositiveButton("YES", (dialog, id) -> logout())
                .setNegativeButton("NO", (dialog, id) -> dialog.cancel());

        // create alert dialog
        android.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    private void showImageSelectionDialog() {

        LayoutInflater inflater1 = getLayoutInflater();
        View alertLayout = inflater1.inflate(R.layout.layout_pic_upload, null);

        final TextView tvGallery = alertLayout.findViewById(R.id.tv_gallery);
        final TextView tvCamera = alertLayout.findViewById(R.id.tv_camera);
        final TextView tvCancel = alertLayout.findViewById(R.id.tv_cancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(alertLayout);
        AlertDialog alert = builder.create();


        tvGallery.setOnClickListener(view1 -> {
            alert.dismiss();
            openGallery();
        });


        tvCamera.setOnClickListener(view12 -> {
            alert.dismiss();
            openCamera();
        });

        tvCancel.setOnClickListener(view13 -> alert.dismiss());
        alert.show();


    }


    private void showNotVerifiedAlert() {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Error")
                .setContentText("Please Verify your account")
                .show();

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 22) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showImageSelectionDialog();
            } else {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 22);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        MyApplication.getAppComponent().inject(this);
    }


    private boolean isNotVerified() {
        if ((MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 0) == 1)
                && (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0) == 1)
                && (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 0) == 1)
                && (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 0) == 1))
            return false;
        else
            return true;
    }


    private void logout() {
        MyApplication.tinyDB.clear();
        Intent i = new Intent(getActivity(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        getActivity().finish();

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getUserBalance();
    }


    private void getUserBalance() {
        fragmentMoreBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<MyBalanceResponse> myBalanceResponseCustomCall = oAuthRestService.getUserBalance(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<MyBalanceResponse>() {
            @Override
            public void success(Response<MyBalanceResponse> response) {
                fragmentMoreBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    MyBalanceResponse myBalanceResponse = response.body();
                    if (myBalanceResponse.getStatus() == 1 && myBalanceResponse.getResult().size() > 0) {
                        myBalanceResultItem = myBalanceResponse.getResult().get(0);
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, myBalanceResultItem.getBalance() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_WINING_AMOUNT, myBalanceResultItem.getWinning() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BONUS_BALANCE, myBalanceResultItem.getBonus() + "");
                        setUserBalance();
                    } else {
                        AppUtils.showErrorr((HomeActivity) getActivity(), myBalanceResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentMoreBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void setUserBalance() {
        fragmentMoreBinding.cardViewWallet.tvDepositBalance.setText("₹" + myBalanceResultItem.getBalance());
        fragmentMoreBinding.cardViewWallet.tvWinning.setText("₹" + myBalanceResultItem.getWinning());
        fragmentMoreBinding.cardViewWallet.tvBonusCash.setText("₹" + myBalanceResultItem.getBonus());
        fragmentMoreBinding.cardViewWallet.tvTotalBalance.setText("" + myBalanceResultItem.getTotal());
    }


    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity)getActivity()).homeMenu.findItem(R.id.navigation_wallet).setVisible(false);
        fragmentMoreBinding.tvUserName.setText(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_NAME));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //  MenuItem item = menu.findItem(R.id.navigation_wallet);
        MenuItem itemNotification = menu.findItem(R.id.navigation_notification);
        if (itemNotification != null)
            itemNotification.setVisible(false);
    }

    public class TabAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }


    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    private void openCamera() {
   /*     Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI = FileProvider.getUriForFile(this,
                "com.example.android.fileprovider",
                photoFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

        startActivityForResult(intent, CAMERA_REQUEST_CODE);*/

        dispatchTakePictureIntent();

    }

    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getActivity().getPackageName() + ".provider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        boolean flag = false;
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri image = data.getData();
                flag = true;
                fileImage = getPath(image);
            } else {

            }
        } else if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                flag = true;
                fileImage = currentPhotoPath;
            }
        }

        if (flag) {
            uploadUserImage();
        }
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }

    private void uploadUserImage() {
        fragmentMoreBinding.setRefreshing(true);
        String userId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);
        RequestBody requestBodyUserId = RequestBody.create(MediaType.parse("multipart/form-data"), userId);
        File file = new File(fileImage);
        long length = (file.length() / (1024 * 1024));
        if (length > 1) {
            file = saveBitmapToFile(file);
        }
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part uploadPic = MultipartBody.Part.createFormData("file", file.getName(), requestFile);
        CustomCallAdapter.CustomCall<UserImageUploadResponse> imageUploadResponseCustomCall = oAuthRestService.uploadUserImage(requestBodyUserId, uploadPic);
        imageUploadResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<UserImageUploadResponse>() {
            @Override
            public void success(Response<UserImageUploadResponse> response) {
                fragmentMoreBinding.setRefreshing(false);
                UserImageUploadResponse imageUploadResponse = response.body();
                if (imageUploadResponse.getStatus() == 1) {
                    if (imageUploadResponse.getResult().get(0).getStatus() == 1) {
                        fileName = imageUploadResponse.getResult().get(0).getImage();

                     //   Log.e("filename", fileName);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_PIC, fileName);
                        AppUtils.loadImageCircle(fragmentMoreBinding.ivUserProfile, MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_PIC));
                        AppUtils.showSuccess((AppCompatActivity) getActivity(), "Image uploaded.");
                    } else {
                        AppUtils.showErrorr((AppCompatActivity) getActivity(), "Error in image uploading.");
                    }
                } else {
                    AppUtils.showErrorr((AppCompatActivity) getActivity(), imageUploadResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentMoreBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    private File saveBitmapToFile(File file) {
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }


}