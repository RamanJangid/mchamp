package com.rg.mchampgold.app.view.adapter;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.MultiSportsPlayerPointItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.BreakupPlayerPointsActivity;
import com.rg.mchampgold.databinding.RecyclerItemPlayerPointsBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlayerPointsItemAdapter extends RecyclerView.Adapter<PlayerPointsItemAdapter.ViewHolder> {

    private List<MultiSportsPlayerPointItem> playerPointItems;
    Activity activity;


    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemPlayerPointsBinding binding;

        ViewHolder(RecyclerItemPlayerPointsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public PlayerPointsItemAdapter(Activity activity,List<MultiSportsPlayerPointItem> playerPointItems) {
        this.playerPointItems = playerPointItems;
        this.activity = activity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemPlayerPointsBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_player_points,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setMultiSportsPlayerPointItem(playerPointItems.get(position));

        if (playerPointItems.get(position).getIsSelected() == 1) {
            holder.binding.playerInYourTeamIv.setVisibility(View.VISIBLE);
            holder.binding.llPlayer.setBackgroundColor(Color.parseColor("#aad5b7"));
        } else if (playerPointItems.get(position).getIsSelected() == 0) {
            holder.binding.playerInYourTeamIv.setVisibility(View.INVISIBLE);
            holder.binding.llPlayer.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }

        if (playerPointItems.get(position).getIsTopplayer() == 1) {
            holder.binding.topPlayerIv.setVisibility(View.VISIBLE);
        } else if (playerPointItems.get(position).getIsTopplayer() == 0) {
            holder.binding.topPlayerIv.setVisibility(View.INVISIBLE);
        }

        AppUtils.loadImage(holder.binding.ivTeamLogo,playerPointItems.get(position).getImage());

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(activity, BreakupPlayerPointsActivity.class);
            intent.putExtra("playerPointItem",playerPointItems.get(position).getBreakup_points());
            intent.putExtra("playerName",playerPointItems.get(position).getPlayer_name());
            intent.putExtra("selectedBy",playerPointItems.get(position).showSelectedBy());
            intent.putExtra("point",playerPointItems.get(position).getPoints());
            intent.putExtra("isSelected",playerPointItems.get(position).getIsSelected());
            intent.putExtra("pImage",playerPointItems.get(position).getImage());
            activity.startActivity(intent);
        });

        holder.binding.executePendingBindings();
    }
    public void sortWithPoints(boolean flag) {
        if (flag) {
            Collections.sort(playerPointItems, (contest, t1) -> Double.valueOf(contest.getPoints()).compareTo(Double.valueOf(t1.getPoints())));
        } else {
            Collections.sort(playerPointItems, (contest, t1) -> Double.valueOf(t1.getPoints()).compareTo(Double.valueOf(contest.getPoints())));
        }
        notifyDataSetChanged();
    }
    public void sortWithSelectedBy(boolean flag) {
        if (flag) {
            Collections.sort(playerPointItems, (contest, t1) -> Double.valueOf(contest.getSelected_by()).compareTo(Double.valueOf(t1.getSelected_by())));
        } else {
            Collections.sort(playerPointItems, (contest, t1) -> Double.valueOf(t1.getSelected_by()).compareTo(Double.valueOf(contest.getSelected_by())));
        }
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return playerPointItems.size();
    }


    public void updateData(ArrayList<MultiSportsPlayerPointItem> list) {
        playerPointItems = list;
        notifyDataSetChanged();
    }
}