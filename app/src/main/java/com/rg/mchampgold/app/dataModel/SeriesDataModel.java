package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class SeriesDataModel {
    @SerializedName("id")
    String id;
    @SerializedName("name")
    String name;
    @SerializedName("banner_image")
    String banner_image;
    @SerializedName("banner_image_url")
    String banner_image_url;
    @SerializedName("is_banner_visible")
    Integer is_banner_visible;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBanner_image() {
        return banner_image;
    }

    public void setBanner_image(String banner_image) {
        this.banner_image = banner_image;
    }

    public String getBanner_image_url() {
        return banner_image_url;
    }

    public void setBanner_image_url(String banner_image_url) {
        this.banner_image_url = banner_image_url;
    }

    public Integer getIs_banner_visible() {
        return is_banner_visible;
    }

    public void setIs_banner_visible(Integer is_banner_visible) {
        this.is_banner_visible = is_banner_visible;
    }
}
