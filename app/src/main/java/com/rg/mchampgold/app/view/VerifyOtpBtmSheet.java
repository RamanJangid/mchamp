package com.rg.mchampgold.app.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.Task;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.OtpVerfiyRequest;
import com.rg.mchampgold.app.api.response.RegisterResponse;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.NormalResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.IncomingSMSListener;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.LayoutBottomsheetBinding;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import retrofit2.Response;

public class VerifyOtpBtmSheet extends DialogFragment implements IncomingSMSListener {
    public static final String TAG = "ActionBottomDialog";
    static String userMObile;
    private ItemClickListener mListener;


    IncomingSMSListener listener;


    @Inject
    OAuthRestService oAuthRestService;


    LayoutBottomsheetBinding mBinding;

    public static VerifyOtpBtmSheet newInstance(String mobile) {
        userMObile = mobile;
        return new VerifyOtpBtmSheet();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.layout_bottomsheet, container, false);

        AppUtils.disableCopyPaste(mBinding.etOtp);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        startSMS_Tracking();
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.tvMobile.setText(userMObile);


        long diffInMs = Constants.OTP_SEND_TIME;

        CountDownTimer cT = new CountDownTimer(diffInMs, 1000) {

            public void onTick(long millisUntilFinished) {
                mBinding.resendTxt.setText(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) < 10 ? "00:0" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) :
                        "00:" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));

                mBinding.resendTxt.setClickable(false);
            }

            public void onFinish() {
                mBinding.resendTxt.setText(Html.fromHtml("<u>Resend OTP ?</u>"));
                mBinding.resendTxt.setClickable(true);

            }
        };
        cT.start();


        mBinding.resendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOTP(userMObile);
            }
        });


        mBinding.btnVerifyOtp.setOnClickListener(view1 -> {
            if (mBinding.etOtp.getText().toString().trim().length() != 6)
                AppUtils.showErrorr((AppCompatActivity) getActivity(), "Please enter valid 6 digit OTP");
            else {
                otpVerify(userMObile, mBinding.etOtp.getText().toString().trim());
            }

        });


    }

    private void resendOTP(String mobile) {

        mBinding.setRefreshing(true);
        String userId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(userId);
        baseRequest.setMobile(mobile);
        baseRequest.setType("2");
        CustomCallAdapter.CustomCall<NormalResponse> normalResponseCustomCall = oAuthRestService.verifyByMobile(baseRequest);
        normalResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<NormalResponse>() {
            @Override
            public void success(Response<NormalResponse> response) {
                mBinding.setRefreshing(false);
                NormalResponse normalResponse = response.body();
                if (normalResponse.getStatus() == 1) {

                    long diffInMs = Constants.OTP_SEND_TIME;

                    CountDownTimer cT = new CountDownTimer(diffInMs, 1000) {

                        public void onTick(long millisUntilFinished) {
                            if (mBinding.resendTxt != null) {
                                mBinding.resendTxt.setText(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) < 10 ? "00:0" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) :
                                        "00:" + TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished));

                                mBinding.resendTxt.setClickable(false);
                            }
                        }

                        public void onFinish() {
                            if (mBinding.resendTxt != null) {
                                mBinding.resendTxt.setText(Html.fromHtml("<u>Resend OTP ?</u>"));
                                mBinding.resendTxt.setClickable(true);
                            }
                        }
                    };
                    cT.start();

                } else {
                    AppUtils.showErrorr((AppCompatActivity) getActivity(), normalResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });

    }

    private void otpVerify(String mobileNo, String otp) {
        mBinding.setRefreshing(true);
        OtpVerfiyRequest otpVerfiyRequest = new OtpVerfiyRequest();
        otpVerfiyRequest.setMobile(mobileNo);
        otpVerfiyRequest.setOtp(otp);
        if (!MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID).equalsIgnoreCase(""))
            otpVerfiyRequest.setUserId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<RegisterResponse> userLogin = oAuthRestService.otpVerify(otpVerfiyRequest);
        userLogin.enqueue(new CustomCallAdapter.CustomCallback<RegisterResponse>() {
            @Override
            public void success(Response<RegisterResponse> response) {
                mBinding.setRefreshing(false);
                dismiss();
                if (response.isSuccessful() && response.body() != null) {
                    RegisterResponse registerResponse = response.body();
                    if (registerResponse.getStatus() == 1) {
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_MOBILE, registerResponse.getResult().getMobile());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, registerResponse.getResult().getMobileVerify());
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());

                    } else {
                        Toast.makeText(getActivity(), registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //  mListener = (ItemClickListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        // mListener = null;
        // getActivity().unregisterReceiver(receiver);
    }


    void startSMS_Tracking() {
        MyApplication.listner = this;
        SmsRetrieverClient client = SmsRetriever.getClient(getActivity());

        // Starts SmsRetriever, which waits for ONE matching SMS message until timeout
// (5 minutes). The matching SMS message will be sent via a Broadcast Intent with
// action SmsRetriever#SMS_RETRIEVED_ACTION.
        Task<Void> task = client.startSmsRetriever();

// Listen for success/failure of the start Task. If in a background thread, this
// can be made blocking using Tasks.await(task, [timeout]);
        task.addOnSuccessListener(aVoid -> {
            // Successfully started retriever, expect broadcast intent

            // ...
//            IntentFilter filter1 = new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED");
//            getActivity().registerReceiver(new MySMSBroadcastReceiver(), filter1);

            //  IntentFilter filter2 = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            // getActivity().registerReceiver(new IncomingSmsReceiver(this), filter2);

        });

        task.addOnFailureListener(e -> {
            // Failed to start retriever, inspect Exception for more details
            // ...
            //  Log.e("CA", "Error");
        });


    }

    @Override
    public void onOTPReceived(String otp) {
        if (mBinding != null) {
            mBinding.etOtp.setText(otp);
            otpVerify(userMObile, mBinding.etOtp.getText().toString().trim());
        }
    }

    @Override
    public void onOTPTimeOut() {
        AppUtils.showErrorr((AppCompatActivity) getActivity(), "Timeout");
    }


    public interface ItemClickListener {
        void onItemClick();
    }


}
