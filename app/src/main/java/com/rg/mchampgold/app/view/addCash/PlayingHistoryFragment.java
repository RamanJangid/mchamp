package com.rg.mchampgold.app.view.addCash;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.PlayingHistoryItem;
import com.rg.mchampgold.app.dataModel.PlayingHistoryResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.HomeActivity;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentPlayingHistoryBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class PlayingHistoryFragment extends Fragment {

    private FragmentPlayingHistoryBinding fragmentPlayingHistoryBinding;
    @Inject
    OAuthRestService oAuthRestService;
    PlayingHistoryItem playingHistoryItem;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentPlayingHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_playing_history, container, false);
        return fragmentPlayingHistoryBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getPlayingHistory();
    }



    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void getPlayingHistory() {
        fragmentPlayingHistoryBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<PlayingHistoryResponse> myBalanceResponseCustomCall = oAuthRestService.getMyPlayingHistory(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<PlayingHistoryResponse>() {
            @Override
            public void success(Response<PlayingHistoryResponse> response) {
                fragmentPlayingHistoryBinding.setRefreshing(false);

                if (response.isSuccessful() && response.body() != null) {
                    PlayingHistoryResponse playingHistoryResponse = response.body();
                    if (playingHistoryResponse.getStatus() == 1 && playingHistoryResponse.getResult() != null) {
                        playingHistoryItem = playingHistoryResponse.getResult();
                        setPlayingHistory();
                    } else {
                        AppUtils.showErrorr((HomeActivity) getActivity(), playingHistoryResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentPlayingHistoryBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void setPlayingHistory() {
        if (playingHistoryItem.getTotalMatchPlay()==1||playingHistoryItem.getTotalMatchPlay()==0){
            fragmentPlayingHistoryBinding.totalMatch.setText("Match Played");
        }else {
            fragmentPlayingHistoryBinding.totalMatch.setText("Matches Played");
        }
        if (playingHistoryItem.getTotalLeaguePlay()==1||playingHistoryItem.getTotalLeaguePlay()==0){
            fragmentPlayingHistoryBinding.totalContast.setText("Contest Played");
        }else {
            fragmentPlayingHistoryBinding.totalContast.setText("Contests Played");
        }
        if (playingHistoryItem.getTotalContestWin()==1||playingHistoryItem.getTotalContestWin()==0){
            fragmentPlayingHistoryBinding.totalContestWin.setText("Contest Won");
        }else {
            fragmentPlayingHistoryBinding.totalContestWin.setText("Contests Won");
        }
        fragmentPlayingHistoryBinding.totalMatchTxt.setText(playingHistoryItem.getTotalMatchPlay() + "");
        fragmentPlayingHistoryBinding.totalContastTxt.setText(playingHistoryItem.getTotalLeaguePlay() + "");
        fragmentPlayingHistoryBinding.totalContestWinTxt.setText(playingHistoryItem.getTotalContestWin() + "");
        fragmentPlayingHistoryBinding.totalWinningsTxt.setText("₹" + playingHistoryItem.getTotalWinning());
    }


}