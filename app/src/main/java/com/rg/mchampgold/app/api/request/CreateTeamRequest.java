package com.rg.mchampgold.app.api.request;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CreateTeamRequest {

	@SerializedName("matchkey")
	private String matchKey;

	@SerializedName("user_id")
	private String user_id;

	@SerializedName("players")
	private String players;

	@SerializedName("vicecaptain")
	private String viceCaptain;

	@SerializedName("captain")
	private String captain;

	@SerializedName("teamid")
	private int  teamId;

	@SerializedName("sport_key")
	private String sport_key;


	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}


	public String getMatchKey() {
		return matchKey;
	}

	public void setMatchKey(String matchKey) {
		this.matchKey = matchKey;
	}

	public String getPlayers() {
		return players;
	}

	public void setPlayers(String players) {
		this.players = players;
	}

	public String getViceCaptain() {
		return viceCaptain;
	}

	public void setViceCaptain(String viceCaptain) {
		this.viceCaptain = viceCaptain;
	}

	public String getCaptain() {
		return captain;
	}

	public void setCaptain(String captain) {
		this.captain = captain;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}
}