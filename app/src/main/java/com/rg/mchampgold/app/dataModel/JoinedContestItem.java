package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class JoinedContestItem {
    @SerializedName("contest")
    private ArrayList<JoinedContesttItem> contest;

    @SerializedName("user_teams")
    private int userTeams;

    @SerializedName("joined_leagues")
    private int joined_leagues;

    public ArrayList<JoinedContesttItem> getContest() {
        return contest == null?new ArrayList<>():contest;
    }

    public void setContest(ArrayList<JoinedContesttItem> contest) {
        this.contest = contest;
    }

    public int getUserTeams() {
        return userTeams;
    }

    public void setUserTeams(int userTeams) {
        this.userTeams = userTeams;
    }

    public int getJoined_leagues() {
        return joined_leagues;
    }

    public void setJoined_leagues(int joined_leagues) {
        this.joined_leagues = joined_leagues;
    }
}
