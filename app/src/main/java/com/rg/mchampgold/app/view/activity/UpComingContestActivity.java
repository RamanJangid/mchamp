package com.rg.mchampgold.app.view.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.GetWinnerScoreCardResponse;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.WinnerScoreCardItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.basketball.BasketBallCreateTeamActivity;
import com.rg.mchampgold.app.view.basketball.BasketBallTeamPreviewActivity;
import com.rg.mchampgold.app.view.football.FootballCreateTeamActivity;
import com.rg.mchampgold.app.view.football.FootballTeamPreviewActivity;
import com.rg.mchampgold.app.view.fragment.MyJoinedContestFragment;
import com.rg.mchampgold.app.view.fragment.MyTeamFragment;
import com.rg.mchampgold.app.view.fragment.UpComingContestFragment;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.app.viewModel.ContestViewModel;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityUpcommingContestBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;

public class UpComingContestActivity extends AppCompatActivity implements OnContestItemClickListener {

    ActivityUpcommingContestBinding mBinding;
    private ContestViewModel contestViewModel;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    ArrayList<Contest> list = new ArrayList<>();
    String date;
    String userReferCode = "";
    String sportKey = "";
    private int teamCount;
    private int joinedContestCount;
    Context context;
    boolean isForContestDetails;
    Menu menuTemp;

    @Inject
    OAuthRestService oAuthRestService;
    TabAdapter mTabAdapter;
    private int isLeaderboard;
    private int seriesId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        contestViewModel = ContestViewModel.create(UpComingContestActivity.this);
        MyApplication.getAppComponent().inject(contestViewModel);
        MyApplication.getAppComponent().inject(UpComingContestActivity.this);
        context = UpComingContestActivity.this;
        userReferCode = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_REFER_CODE);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_upcomming_contest);
        initialize();
    }

    @SuppressLint("RestrictedApi")
    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.contest));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            date = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            isForContestDetails = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS);
            sportKey  = getIntent().getExtras().getString(Constants.SPORT_KEY);
            seriesId = getIntent().getExtras().getInt(Constants.KEY_SERIES_ID,0);
            isLeaderboard = getIntent().getExtras().getInt(Constants.KEY_IS_LEADERBOARD, 0);
        }

        String teams[] = teamVsName.split(" ");
        mBinding.matchHeaderInfo.tvTeam1.setText(teams[0]);
        mBinding.matchHeaderInfo.tvTeam2.setText(teams[2]);

        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam2, teamSecondUrl);


        mTabAdapter = new TabAdapter(getSupportFragmentManager());

        MyTeamFragment myTeamFragment = new MyTeamFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_MATCH_KEY, matchKey);
        bundle.putString(Constants.SPORT_KEY, sportKey);
        myTeamFragment.setArguments(bundle);

        UpComingContestFragment upCommingContestFragment = new UpComingContestFragment();
        upCommingContestFragment.setArguments(bundle);

        MyJoinedContestFragment myJoinedContestFragment = new MyJoinedContestFragment();
        myJoinedContestFragment.setArguments(bundle);

        mTabAdapter.addFragment(upCommingContestFragment, "Contests");
        mTabAdapter.addFragment(myJoinedContestFragment, "My Contests (0)");
        mTabAdapter.addFragment(myTeamFragment, "My Teams (0)");
        mBinding.viewPager.setAdapter(mTabAdapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager);
        mBinding.viewPager.setOffscreenPageLimit(3);
        // set tab icon added by pkb
        //setTabIcon();


        View root = mBinding.tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(Color.parseColor("#e1e1e1"));
            final float scale = getResources().getDisplayMetrics().density;
            int pixels = (int) (1 * scale + 0.5f);
            drawable.setSize(pixels, root.getHeight());
            ((LinearLayout) root).setDividerDrawable(drawable);
        }

        mBinding.tabLayout.setSelectedTabIndicatorColor(Color.TRANSPARENT);

        try {

            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(date), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                    mBinding.matchHeaderInfo.tvMatchTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.matchHeaderInfo.tvMatchTimer.setText("00h : 00m : 00s");
                    startActivity(new Intent(UpComingContestActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).putExtra("matchFinish",true));                }
            };
            countDownTimer.start();
        } catch (Exception e) {

        }


    }


    private void setTabIcon() {
        int[] tabIcons = {
                R.drawable.ic_tab_contest_selector,
                R.drawable.ic_tab_joined_contest_selector,
                R.drawable.ic_tab_account_selector
        };
        for (int i = 0; i < mBinding.tabLayout.getTabCount(); i++) {
            mBinding.tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }
    }



/*
    public void creteTeam() {
        Intent intent = new Intent(UpComingContestActivity.this, CreateTeamActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        startActivity(intent);
    }
*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wallet, menu);
        menuTemp=menu;
        AppUtils.setWalletBalance(menuTemp,getResources().getColor(R.color.pink), false, isLeaderboard, seriesId);
        //   AppUtils.loadImageCircle((ImageView) menu.findItem(R.id.iv_profile).getActionView(), MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_PIC));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;
        } else if (itemId == R.id.navigation_wallet) {
            openWalletActivity();
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private void openNotificationActivity() {
        startActivity(new Intent(UpComingContestActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(UpComingContestActivity.this, MyWalletActivity.class));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (menuTemp!=null){
            AppUtils.setWalletBalance(menuTemp,getResources().getColor(R.color.pink), false, isLeaderboard, seriesId);
        }
        // getData();
    }

    @SuppressLint("RestrictedApi")
    private void getData() {
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        contestViewModel.loadContestRequest(request);
        contestViewModel.getContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1) {
                        list = arrayListResource.getData().getResult().getContestArrayList();
                        teamCount = arrayListResource.getData().getResult().getUserTeamCount();
                        joinedContestCount = arrayListResource.getData().getResult().getJoinedContestCount();
                        setTabTitle(teamCount, joinedContestCount);
                        // fab_create_team.setVisibility(teamCount == 0 ? View.VISIBLE : View.GONE);
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }


    @Override
    public void onContestClick(Contest contest, boolean isForDetail) {
        if (isForDetail) {
            Intent intent = new Intent(UpComingContestActivity.this, UpComingContestDetailActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
            intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS, true);
            intent.putExtra(Constants.SPORT_KEY,sportKey);
            intent.putExtra(Constants.KEY_SERIES_ID, seriesId);
            intent.putExtra(Constants.KEY_IS_LEADERBOARD, isLeaderboard);
            startActivity(intent);
        } else {
            if (teamCount > 0) {
                Intent intent = new Intent(UpComingContestActivity.this, MyTeamsActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
                intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
                intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
                intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
                intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
                intent.putExtra(Constants.SPORT_KEY,sportKey);
                startActivity(intent);
            } else {
                creteTeam(contest);
            }
        }
    }

    private void creteTeam(Contest contest) {
        Intent intent;
        if(sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(UpComingContestActivity.this, FootballCreateTeamActivity.class);
        }
        else if(sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(UpComingContestActivity.this, BasketBallCreateTeamActivity.class);
        }
        else {
            intent = new Intent(UpComingContestActivity.this,CreateTeamActivity.class);
        }
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY,sportKey);
        intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
        intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
        startActivity(intent);

    }

    public void openShareIntent() {
        String shareBody =
                "Hi, Inviting you to join "+Constants.APP_NAME+" and play fantasy sports to win cash daily.\n" +
                        "Use Contest code - " + userReferCode +
                        "\nTo join this Exclusive invite-only contest on "+Constants.APP_NAME+" for the " + teamVsName + " match, Download App from ~ " + MyApplication.apk_url + ".";
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    public class TabAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            //if (position == 0)
                return mFragmentTitleList.get(position);
          /*  if (position == 1) {
                return mFragmentTitleList.get(position) + "(" + joinedContestCount + ")";
            }

            if (position == 2) {
                return mFragmentTitleList.get(position) + "(" + teamCount + ")";

            }*/

         //   return "";

        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }


    public void creteTeam() {
        Intent intent;
        if(sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(UpComingContestActivity.this, FootballCreateTeamActivity.class);
        }
        else if(sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(UpComingContestActivity.this, BasketBallCreateTeamActivity.class);
        }
        else {
            intent = new Intent(UpComingContestActivity.this,CreateTeamActivity.class);
        }
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY,sportKey);
        startActivity(intent);
    }

    public void joinByContestCode() {
        Intent intent = new Intent(UpComingContestActivity.this, JoinContestByInviteCodeActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY,sportKey);
        startActivity(intent);
    }

    public void editOrClone(ArrayList<Player> list, int teamId, String teamName) {
        Intent intent;
        if(sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(UpComingContestActivity.this, FootballCreateTeamActivity.class);
        }
        else if(sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(UpComingContestActivity.this, BasketBallCreateTeamActivity.class);
        }
        else {
            intent = new Intent(UpComingContestActivity.this,CreateTeamActivity.class);
        }
        intent.putExtra(Constants.KEY_TEAM_NAME, teamName);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_TEAM_ID, teamId);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra("isFromEditOrClone", true);
        intent.putExtra("selectedList", list);
        intent.putExtra(Constants.SPORT_KEY,sportKey);
        startActivity(intent);
    }


    public void openPreviewActivity(ArrayList<Player> list, String teamName) {

        ArrayList<Player> selectedWkList = new ArrayList<>();
        ArrayList<Player> selectedBatLiSt = new ArrayList<>();
        ArrayList<Player> selectedArList = new ArrayList<>();
        ArrayList<Player> selectedBowlList = new ArrayList<>();
        ArrayList<Player> selectedcList = new ArrayList<>();
        Intent intent;
        if(sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(UpComingContestActivity.this, FootballTeamPreviewActivity.class);
            for (Player player : list) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_GK))
                    selectedWkList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_DEF))
                    selectedBatLiSt.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_MID))
                    selectedArList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ST))
                    selectedBowlList.add(player);
            }
        }
        else if(sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(UpComingContestActivity.this, BasketBallTeamPreviewActivity.class);
            for (Player player : list) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PG))
                    selectedWkList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SG))
                    selectedBatLiSt.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SF))
                    selectedArList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PF))
                    selectedBowlList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_C))
                    selectedcList.add(player);
            }
            intent.putExtra(Constants.KEY_TEAM_LIST_C, selectedcList);
        }
        else {
            intent = new Intent(UpComingContestActivity.this,TeamPreviewActivity.class);

            for (Player player : list) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_KEEP))
                    selectedWkList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BAT))
                    selectedBatLiSt.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ALL_R))
                    selectedArList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_BOL))
                    selectedBowlList.add(player);
            }

        }
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_TEAM_NAME, teamName);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, isForContestDetails);
        intent.putExtra(Constants.SPORT_KEY,sportKey);

        intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedWkList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedBatLiSt);
        intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedArList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedBowlList);

        startActivity(intent);
    }

    public void movetoContest() {
        mBinding.tabLayout.getTabAt(0).select();
    }


    public void setTabTitle(int teamCount, int joinedContestCount) {
        this.teamCount = teamCount;
        this.joinedContestCount = joinedContestCount;
        if (mBinding.tabLayout.getTabCount() <= 3) {
            //mBinding.tabLayout.getTabAt(1).setText("Joined Contests");
             mBinding.tabLayout.getTabAt(1).setText("My Contests " + "(" + joinedContestCount + ")");
           // mBinding.tabLayout.getTabAt(2).setText("My Teams");
             mBinding.tabLayout.getTabAt(2).setText("My Teams " + "(" + teamCount + ")");
        }
    }

    public void openJoinedContestActivity(boolean isForDetail, Contest contest) {
        if (isForDetail) {
            Intent intent = new Intent(UpComingContestActivity.this, UpComingContestDetailActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
            intent.putExtra(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS, true);
            intent.putExtra(Constants.SPORT_KEY,sportKey);
            intent.putExtra(Constants.KEY_SERIES_ID, seriesId);
            intent.putExtra(Constants.KEY_IS_LEADERBOARD, isLeaderboard);
            startActivity(intent);
        } else {
            Intent intent = new Intent(UpComingContestActivity.this, MyTeamsActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
            intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.SPORT_KEY,sportKey);
            startActivity(intent);
        }
    }


    public void openAllContestActivity(int categoryId) {
        Intent intent = new Intent(UpComingContestActivity.this, AllContestActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS, true);
        intent.putExtra(Constants.KEY_ALL_CONTEST, categoryId);
        intent.putExtra(Constants.SPORT_KEY,sportKey);
        startActivity(intent);
    }

    public void openPrivateCreateContest(int teamCount) {
        Intent intent = new Intent(UpComingContestActivity.this, PrivateContestActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY,sportKey);
        intent.putExtra(Constants.USER_TEAM_COUNT,teamCount);
        startActivity(intent);
    }


    public void getWinnerPriceCard(int contestId, String amount) {
        ContestRequest contestRequest = new ContestRequest();
        contestRequest.setMatchKey(matchKey);
        contestRequest.setLeagueId(contestId + "");
        CustomCallAdapter.CustomCall<GetWinnerScoreCardResponse> bankDetailResponseCustomCall = oAuthRestService.getWinnersPriceCard(contestRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<GetWinnerScoreCardResponse>() {
            @Override
            public void success(Response<GetWinnerScoreCardResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    GetWinnerScoreCardResponse getWinnerScoreCardResponse = response.body();
                    if (getWinnerScoreCardResponse.getStatus() == 1 && getWinnerScoreCardResponse.getResult().size() > 0) {
                        ArrayList<WinnerScoreCardItem> priceList = getWinnerScoreCardResponse.getResult();
                        if (priceList.size() > 0) {
                            AppUtils.showWinningPopup(UpComingContestActivity.this, priceList, "" + amount);
                        }
                    }
                }
            }

            @Override
            public void failure(ApiException e) {

                e.printStackTrace();
            }
        });
    }

}
