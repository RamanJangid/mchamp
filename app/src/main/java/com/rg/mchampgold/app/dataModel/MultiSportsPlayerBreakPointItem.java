package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MultiSportsPlayerBreakPointItem implements Serializable {

    @SerializedName("event_name")
    private String event_name;

    @SerializedName("actual")
    private String actual;

    @SerializedName("actual_points")
    private String actual_points;

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }

    public String getActual_points() {
        return actual_points;
    }

    public void setActual_points(String actual_points) {
        this.actual_points = actual_points;
    }
}
