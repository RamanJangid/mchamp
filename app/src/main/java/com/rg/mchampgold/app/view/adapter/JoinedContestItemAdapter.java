package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.JoinedContesttItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.databinding.RecyclerJoinedItemContestBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class JoinedContestItemAdapter extends RecyclerView.Adapter<JoinedContestItemAdapter.ViewHolder> {

    private List<JoinedContesttItem> moreInfoDataList;
    private OnContestItemClickListener listener;
    Context context;
    String matchKey;
    String category="";
    @Inject
    OAuthRestService oAuthRestService;

    class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerJoinedItemContestBinding binding;

        ViewHolder(RecyclerJoinedItemContestBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public JoinedContestItemAdapter(Context context, List<JoinedContesttItem> moreInfoDataList, OnContestItemClickListener listener, String matchkey) {
        this.moreInfoDataList = moreInfoDataList;
        this.context = context;
        this.listener = listener;
        this.matchKey = matchkey;
        category="";
        MyApplication.getAppComponent().inject(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerJoinedItemContestBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_joined_item_contest,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.binding.setContestData(moreInfoDataList.get(position));
        holder.binding.tagMText.setText(String.valueOf(moreInfoDataList.get(position).getMaxTeamLimit()));
        if (category.equalsIgnoreCase(moreInfoDataList.get(position).getCategory())){
            holder.binding.category.setVisibility(View.GONE);
        }else {
            category=moreInfoDataList.get(position).getCategory();
            holder.binding.category.setText(category);
            holder.binding.category.setVisibility(View.VISIBLE);
        }

        if (moreInfoDataList.get(position).getIs_free() == 1) {
            holder.binding.tvIsFree.setVisibility(View.VISIBLE);
            holder.binding.tvIsFree.setPaintFlags(holder.binding.tvIsFree.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.binding.tvIsFree.setText("₹"+moreInfoDataList.get(position).getDis_price());
            holder.binding.btnJoin.setText("₹"+moreInfoDataList.get(position).getEntryfee());
//            holder.binding.btnJoin.setText(moreInfoDataList.get(position).showJoinAmount());
        } else {
            holder.binding.tvIsFree.setVisibility(View.GONE);
            holder.binding.btnJoin.setText("₹"+moreInfoDataList.get(position).getEntryfee());
//            holder.binding.btnJoin.setText(moreInfoDataList.get(position).showJoinAmount());
        }


        holder.binding.tvFirstPrize.setText(moreInfoDataList.get(position).getFirst_rank_prize());
        if (moreInfoDataList.get(position).getChallenge_type().equals("percentage")) {
            holder.binding.txtStartValue.setText(moreInfoDataList.get(position).getJoinedusers() + " teams already entered");
            holder.binding.txtEndValue.setText("");
            holder.binding.progressBar.setMax(16);
            holder.binding.progressBar.setProgress(8);

            if (moreInfoDataList.get(position).getWinning_percentage() != null) {
                if (!moreInfoDataList.get(position).getWinning_percentage().equals("") && !moreInfoDataList.get(position).getWinning_percentage().equals("0")) {
                    holder.binding.llTotalWinnersContest.setVisibility(View.VISIBLE);
                    holder.binding.tvTotalWinners.setText(moreInfoDataList.get(position).getWinning_percentage() + "% Win");
                }
            }

        } else {

            holder.binding.progressBar.setMax(moreInfoDataList.get(position).getMaximumUser());
            holder.binding.progressBar.setProgress(moreInfoDataList.get(position).getJoinedusers());

            int left = (moreInfoDataList.get(position).getMaximumUser()) - (moreInfoDataList.get(position).getJoinedusers());
            if (left != 0)
                holder.binding.txtStartValue.setText("" + left + " Spots left");
            else
                holder.binding.txtStartValue.setText("Contest Closed");
            holder.binding.txtEndValue.setText(moreInfoDataList.get(position).getMaximumUser() + " Spots");
            holder.binding.llTotalWinnersContest.setVisibility(View.VISIBLE);
            holder.binding.tvTotalWinners.setText(moreInfoDataList.get(position).getTotalwinners() + " Team Win");
        }


      /*  holder.binding.llTotalWinnersContest.setOnClickListener(view -> {
            if (moreInfoDataList.get(position).getTotalwinners() > 0) {
                getWinnerPriceCard(moreInfoDataList.get(position).getChallengeId() , moreInfoDataList.get(position).getWinamount() + "");
            }
        });
*/


        holder.itemView.setOnClickListener(view -> {
            Contest contest = new Contest();
            contest.setId(moreInfoDataList.get(position).getChallengeId());
            contest.setChallenge_type(moreInfoDataList.get(position).getChallenge_type());
            contest.setWinning_percentage(moreInfoDataList.get(position).getWinning_percentage());
            contest.setFirst_rank_prize(moreInfoDataList.get(position).getFirst_rank_prize());
            contest.setRefercode(moreInfoDataList.get(position).getRefercode());
            contest.setIs_free(moreInfoDataList.get(position).getIs_free());
            contest.setDis_price(moreInfoDataList.get(position).getDis_price());
            contest.setId(moreInfoDataList.get(position).getChallengeId());
            contest.setGetjoinedpercentage(moreInfoDataList.get(position).getGetjoinedpercentage() + "");
            contest.setEntryfee(moreInfoDataList.get(position).getEntryfee() + "");
            contest.setTotalwinners(moreInfoDataList.get(position).getTotalwinners());
            contest.setWinAmount(moreInfoDataList.get(position).getWinamount());
            contest.setIsjoined(moreInfoDataList.get(position).isIsjoined());
            contest.setMultiEntry(moreInfoDataList.get(position).getMultiEntry());
            contest.setIsBonus(moreInfoDataList.get(position).getIsBonus());
            contest.setBonusPercent(moreInfoDataList.get(position).getBonusPercent());
            contest.setConfirmedChallenge(moreInfoDataList.get(position).getConfirmed());
            contest.setMaximumUser(moreInfoDataList.get(position).getMaximumUser());
            contest.setJoinedusers(moreInfoDataList.get(position).getJoinedusers());
            contest.setMaxTeamLimit(moreInfoDataList.get(position).getMaxTeamLimit());
            contest.setWd(moreInfoDataList.get(position).getWd());
            contest.setUser_joined_count(moreInfoDataList.get(position).getUser_joined_count());
            listener.onContestClick(contest, true);
        });
        holder.binding.tagC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });
        holder.binding.tagM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });
        holder.binding.tagWd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });

        holder.binding.tagB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,moreInfoDataList.get(position).getMaxTeamLimit(),moreInfoDataList.get(position).isShowCTag(),
                        moreInfoDataList.get(position).isShowMTag(),moreInfoDataList.get(position).isShowWDTag(),
                        moreInfoDataList.get(position).isShowBTag(), moreInfoDataList.get(position).getBonusPercent());
            }
        });

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return moreInfoDataList.size();
    }


    public void updateData(ArrayList<JoinedContesttItem> list) {
        category="";
        moreInfoDataList = list;
        // searchItemFilteredList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }


/*
    private void getWinnerPriceCard(int contestId, String amount) {
        ContestRequest contestRequest = new ContestRequest();
        contestRequest.setMatchKey(matchKey);
        contestRequest.setLeagueId(contestId+"");
        CustomCallAdapter.CustomCall<GetWinnerScoreCardResponse> bankDetailResponseCustomCall = oAuthRestService.getWinnersPriceCard(contestRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<GetWinnerScoreCardResponse>() {
            @Override
            public void success(Response<GetWinnerScoreCardResponse> response) {

                if (response.isSuccessful() && response.body() != null) {
                    GetWinnerScoreCardResponse getWinnerScoreCardResponse = response.body();
                    if (getWinnerScoreCardResponse.getStatus() == 1 && getWinnerScoreCardResponse.getResult().size() > 0) {
                        ArrayList<WinnerScoreCardItem> priceList = getWinnerScoreCardResponse.getResult();
                        if (priceList.size() > 0) {
                           // showWinningPopup(context, priceList, "" + amount);
                        }
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                e.printStackTrace();
            }
        });
    }*/


    /*void showWinningPopup(Context context, ArrayList<WinnerScoreCardItem> priceCardlist, String s) {
        ExpandableHeightListView priceCard;
        TextView totalWinnersAmount;

        View pricView = LayoutInflater.from(context).inflate(R.layout.price_card_dialog,null);
        AlertDialog.Builder d = new AlertDialog.Builder(context);
        d.setView(pricView);
        priceCard = pricView.findViewById(R.id.priceCard);
        totalWinnersAmount = pricView.findViewById(R.id.totalWinnersAmount);
        totalWinnersAmount.setText("₹ "+s);
        priceCard.setExpanded(true);
        priceCard.setAdapter(new PriceItemAdapter(context, priceCardlist));
        AlertDialog alertDialog = d.create();
        alertDialog.show();
    }*/

}