package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.LiveMatchesScoreBowlerListItem;
import com.rg.mchampgold.app.dataModel.LiveMatchesScorePlayerListItem;
import com.rg.mchampgold.databinding.FullScoreCardItemLayoutBinding;

import java.util.List;

public class LiveScoreCardAdapter extends RecyclerView.Adapter<LiveScoreCardAdapter.ViewHolder> {

    private List<LiveMatchesScorePlayerListItem> playerListItems;
    private List<LiveMatchesScoreBowlerListItem> bowlerListItems;
    Context context;

    class ViewHolder extends RecyclerView.ViewHolder {
        final FullScoreCardItemLayoutBinding binding;

        ViewHolder(FullScoreCardItemLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public LiveScoreCardAdapter(Context context, List<LiveMatchesScorePlayerListItem> playerListItems, List<LiveMatchesScoreBowlerListItem> bowlerListItems) {
        this.playerListItems = playerListItems;
        this.bowlerListItems = bowlerListItems;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FullScoreCardItemLayoutBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.full_score_card_item_layout,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (playerListItems!=null){
            holder.binding.playerName.setText(playerListItems.get(position).getName());
            holder.binding.howOut.setText(playerListItems.get(position).getHow_out());
            holder.binding.run.setText(String.valueOf(playerListItems.get(position).getRun()));
            holder.binding.ball.setText(String.valueOf(playerListItems.get(position).getBall()));
            holder.binding.four.setText(String.valueOf(playerListItems.get(position).getFour()));
            holder.binding.six.setText(String.valueOf(playerListItems.get(position).getSix()));
            holder.binding.point.setText(String.valueOf(playerListItems.get(position).getApp_points()));
        }else {

            holder.binding.playerName.setText(bowlerListItems.get(position).getName());
            holder.binding.howOut.setVisibility(View.GONE);
            holder.binding.run.setText(String.valueOf(bowlerListItems.get(position).getOvers()));
            holder.binding.ball.setText(String.valueOf(bowlerListItems.get(position).getMaidens()));
            holder.binding.four.setText(String.valueOf(bowlerListItems.get(position).getRuns()));
            holder.binding.six.setText(String.valueOf(bowlerListItems.get(position).getWickets()));
            holder.binding.point.setText(String.valueOf(bowlerListItems.get(position).getDots()));
        }
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return playerListItems!=null?playerListItems.size():bowlerListItems.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

}