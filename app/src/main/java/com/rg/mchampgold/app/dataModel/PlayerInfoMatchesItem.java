package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PlayerInfoMatchesItem {

	@SerializedName("matchdate")
	private String matchdate;

	@SerializedName("matchname")
	private String matchname;

	@SerializedName("total_points")
	private double totalPoints;

	@SerializedName("selectper")
	private String selectper;

	@SerializedName("short_name")
	private String shortName;

	@SerializedName("playername")
	private String playername;

	public void setMatchdate(String matchdate){
		this.matchdate = matchdate;
	}

	public String getMatchdate(){
		return matchdate;
	}

	public void setMatchname(String matchname){
		this.matchname = matchname;
	}

	public String getMatchname(){
		return matchname;
	}

	public void setTotalPoints(double totalPoints){
		this.totalPoints = totalPoints;
	}

	public double getTotalPoints(){
		return totalPoints;
	}

	public void setSelectper(String selectper){
		this.selectper = selectper;
	}

	public String getSelectper(){
		return selectper;
	}

	public void setShortName(String shortName){
		this.shortName = shortName;
	}

	public String getShortName(){
		return shortName;
	}

	public void setPlayername(String playername){
		this.playername = playername;
	}

	public String getPlayername(){
		return playername;
	}

	@Override
 	public String toString(){
		return 
			"PlayerInfoMatchesItem{" +
			"matchdate = '" + matchdate + '\'' + 
			",matchname = '" + matchname + '\'' + 
			",total_points = '" + totalPoints + '\'' + 
			",selectper = '" + selectper + '\'' + 
			",short_name = '" + shortName + '\'' + 
			",playername = '" + playername + '\'' + 
			"}";
		}

	public String showTotalPoints(){
		return ""+totalPoints;
	}

}