package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class LeaguesItem{

	@SerializedName("totalwinners")
	private String totalwinners;

	@SerializedName("entryfee")
	private int entryfee;

	@SerializedName("challenge_category_id")
	private int challengeCategoryId;

	@SerializedName("challenge_type")
	private String challengeType;

	@SerializedName("joinedusers")
	private int joinedusers;

	@SerializedName("isselected")
	private boolean isselected;

	@SerializedName("matchkey")
	private String matchkey;

	@SerializedName("multi_entry")
	private int multiEntry;

	@SerializedName("isselectedid")
	private String isselectedid;

	@SerializedName("is_running")
	private int isRunning;

	@SerializedName("winning_percentage")
	private int winningPercentage;

	@SerializedName("confirmed_challenge")
	private int confirmedChallenge;

	@SerializedName("is_bonus")
	private int isBonus;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("win_amount")
	private int winAmount;

	@SerializedName("maximum_user")
	private int maximumUser;

	@SerializedName("status")
	private int status;

	public void setTotalwinners(String totalwinners){
		this.totalwinners = totalwinners;
	}

	public String getTotalwinners(){
		return totalwinners;
	}

	public void setEntryfee(int entryfee){
		this.entryfee = entryfee;
	}

	public int getEntryfee(){
		return entryfee;
	}

	public void setChallengeCategoryId(int challengeCategoryId){
		this.challengeCategoryId = challengeCategoryId;
	}

	public int getChallengeCategoryId(){
		return challengeCategoryId;
	}

	public void setChallengeType(String challengeType){
		this.challengeType = challengeType;
	}

	public String getChallengeType(){
		return challengeType;
	}

	public void setJoinedusers(int joinedusers){
		this.joinedusers = joinedusers;
	}

	public int getJoinedusers(){
		return joinedusers;
	}

	public void setIsselected(boolean isselected){
		this.isselected = isselected;
	}

	public boolean isIsselected(){
		return isselected;
	}

	public void setMatchkey(String matchkey){
		this.matchkey = matchkey;
	}

	public String getMatchkey(){
		return matchkey;
	}

	public void setMultiEntry(int multiEntry){
		this.multiEntry = multiEntry;
	}

	public int getMultiEntry(){
		return multiEntry;
	}

	public void setIsselectedid(String isselectedid){
		this.isselectedid = isselectedid;
	}

	public String getIsselectedid(){
		return isselectedid;
	}

	public void setIsRunning(int isRunning){
		this.isRunning = isRunning;
	}

	public int getIsRunning(){
		return isRunning;
	}

	public void setWinningPercentage(int winningPercentage){
		this.winningPercentage = winningPercentage;
	}

	public int getWinningPercentage(){
		return winningPercentage;
	}

	public void setConfirmedChallenge(int confirmedChallenge){
		this.confirmedChallenge = confirmedChallenge;
	}

	public int getConfirmedChallenge(){
		return confirmedChallenge;
	}

	public void setIsBonus(int isBonus){
		this.isBonus = isBonus;
	}

	public int getIsBonus(){
		return isBonus;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setWinAmount(int winAmount){
		this.winAmount = winAmount;
	}

	public int getWinAmount(){
		return winAmount;
	}

	public void setMaximumUser(int maximumUser){
		this.maximumUser = maximumUser;
	}

	public int getMaximumUser(){
		return maximumUser;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"LeaguesItem{" + 
			"totalwinners = '" + totalwinners + '\'' + 
			",entryfee = '" + entryfee + '\'' + 
			",challenge_category_id = '" + challengeCategoryId + '\'' + 
			",challenge_type = '" + challengeType + '\'' + 
			",joinedusers = '" + joinedusers + '\'' + 
			",isselected = '" + isselected + '\'' + 
			",matchkey = '" + matchkey + '\'' + 
			",multi_entry = '" + multiEntry + '\'' + 
			",isselectedid = '" + isselectedid + '\'' + 
			",is_running = '" + isRunning + '\'' + 
			",winning_percentage = '" + winningPercentage + '\'' + 
			",confirmed_challenge = '" + confirmedChallenge + '\'' + 
			",is_bonus = '" + isBonus + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",win_amount = '" + winAmount + '\'' + 
			",maximum_user = '" + maximumUser + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}