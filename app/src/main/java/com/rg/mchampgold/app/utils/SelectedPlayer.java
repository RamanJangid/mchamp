package com.rg.mchampgold.app.utils;

public class SelectedPlayer {

    private int extra_player = 3;
    private int wk_min_count = 1;
    private int wk_max_count = 4;
    private int wk_selected =0;

    private int total_player_count = 11;

    private int ar_mincount = 1;
    private int ar_maxcount = 4;
    private int ar_selected = 0;

    private int bat_mincount = 3;
    private int bat_maxcount = 6;
    private int bat_selected = 0;

    private int bowl_mincount = 3;
    private int bowl_maxcount = 6;
    private int bowl_selected = 0;

    private int selectedPlayer = 0;

    private int localTeamplayerCount = 0;
    private int localTeamMaxplayerCount = 6;
    private int visitorTeamMaxplayerCount = 6;
    private int visitorTeamPlayerCount = 0;

    private double total_credit = 0.0;


    public int getTotal_player_count() {
        return total_player_count;
    }

    public int getExtra_player() {
        return extra_player;
    }

    public void setExtra_player(int extra_player) {
        this.extra_player = extra_player;
    }

    public int getWk_min_count() {
        return wk_min_count;
    }

    public void setWk_min_count(int wk_min_count) {
        this.wk_min_count = wk_min_count;
    }

    public int getWk_max_count() {
        return wk_max_count;
    }

    public void setWk_max_count(int wk_max_count) {
        this.wk_max_count = wk_max_count;
    }

    public int getWk_selected() {
        return wk_selected;
    }

    public void setWk_selected(int wk_selected) {
        this.wk_selected = wk_selected;
    }

    public int getAr_mincount() {
        return ar_mincount;
    }

    public void setAr_mincount(int ar_mincount) {
        this.ar_mincount = ar_mincount;
    }

    public int getAr_maxcount() {
        return ar_maxcount;
    }

    public void setAr_maxcount(int ar_maxcount) {
        this.ar_maxcount = ar_maxcount;
    }

    public int getAr_selected() {
        return ar_selected;
    }

    public void setAr_selected(int ar_selected) {
        this.ar_selected = ar_selected;
    }

    public int getBat_mincount() {
        return bat_mincount;
    }

    public void setBat_mincount(int bat_mincount) {
        this.bat_mincount = bat_mincount;
    }

    public int getBat_maxcount() {
        return bat_maxcount;
    }

    public void setBat_maxcount(int bat_maxcount) {
        this.bat_maxcount = bat_maxcount;
    }

    public int getBat_selected() {
        return bat_selected;
    }

    public void setBat_selected(int bat_selected) {
        this.bat_selected = bat_selected;
    }

    public int getBowl_mincount() {
        return bowl_mincount;
    }

    public void setBowl_mincount(int bowl_mincount) {
        this.bowl_mincount = bowl_mincount;
    }

    public int getBowl_maxcount() {
        return bowl_maxcount;
    }

    public void setBowl_maxcount(int bowl_maxcount) {
        this.bowl_maxcount = bowl_maxcount;
    }

    public int getBowl_selected() {
        return bowl_selected;
    }

    public void setBowl_selected(int bowl_selected) {
        this.bowl_selected = bowl_selected;
    }

    public int getSelectedPlayer() {
        return selectedPlayer;
    }

    public void setSelectedPlayer(int selectedPlayer) {
        this.selectedPlayer = selectedPlayer;
    }

    public int getLocalTeamMaxplayerCount() {
        return localTeamMaxplayerCount;
    }

    public int getVisitorTeamMaxplayerCount() {
        return visitorTeamMaxplayerCount;
    }

    public int getLocalTeamplayerCount() {
        return localTeamplayerCount;
    }

    public void setLocalTeamplayerCount(int localTeamplayerCount) {
        this.localTeamplayerCount = localTeamplayerCount;
    }

    public int getVisitorTeamPlayerCount() {
        return visitorTeamPlayerCount;
    }


    public void setVisitorTeamPlayerCount(int visitorTeamPlayerCount) {
        this.visitorTeamPlayerCount = visitorTeamPlayerCount;
    }

    public double getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(double total_credit) {
        this.total_credit = total_credit;
    }


    public void setTotal_player_count(int total_player_count) {
        this.total_player_count = total_player_count;
    }

    public void setLocalTeamMaxplayerCount(int localTeamMaxplayerCount) {
        this.localTeamMaxplayerCount = localTeamMaxplayerCount;
    }

    public void setVisitorTeamMaxplayerCount(int visitorTeamMaxplayerCount) {
        this.visitorTeamMaxplayerCount = visitorTeamMaxplayerCount;
    }
}
