package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;
import com.rg.mchampgold.R;

import java.io.Serializable;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Contest implements Serializable {
    @SerializedName("totalwinners")
    private int totalwinners = 0;

    @SerializedName("entryfee")
    private String entryfee;

    @SerializedName("first_rank_prize")
    private String first_rank_prize = "0";


    @SerializedName("getjoinedpercentage")
    private String getjoinedpercentage;


    @SerializedName("dis_price")
    private String dis_price = "0";

    public String getDis_price() {
        return dis_price;
    }

    public void setDis_price(String dis_price) {
        this.dis_price = dis_price;
    }

    @SerializedName("joinedusers")
    private int joinedusers;

    @SerializedName("is_free")
    private int is_free;

    public int getIs_free() {
        return is_free;
    }

    public void setIs_free(int is_free) {
        this.is_free = is_free;
    }

    @SerializedName("challenge_type")
    private String challenge_type = "";

    @SerializedName("isjoined")
    private boolean isjoined;

    @SerializedName("isselected")
    private boolean isselected;

    @SerializedName("matchkey")
    private String matchkey;

    @SerializedName("multi_entry")
    private int multiEntry;

    @SerializedName("isselectedid")
    private String isselectedid;

    @SerializedName("is_running")
    private int isRunning;

    @SerializedName("confirmed_challenge")
    private int confirmedChallenge;

    @SerializedName("is_bonus")
    private int isBonus;

    @SerializedName("refercode")
    private String refercode = "";

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("win_amount")
    private int winAmount;

    @SerializedName("maximum_user")
    private int maximumUser;

    @SerializedName("status")
    private int status;

    @SerializedName("bonus_percent")
    private String bonusPercent;

    @SerializedName("pdf")
    private String pdf;

    @SerializedName("announcement")
    private String announcement = "";

    @SerializedName("max_team_limit")
    private int maxTeamLimit;

    @SerializedName("wd")
    private int wd;

    @SerializedName("user_joined_count")
    private int user_joined_count;

    @SerializedName("flexible_league")
    private int flexibleLeague;

    @SerializedName("flexible_win_amount")
    private String flexibleWinAmount;

    @SerializedName("flexible_first_rank_prize")
    private String flexibleFirstRankPrize;

    public int getMaxTeamLimit() {
        return maxTeamLimit;
    }

    public void setMaxTeamLimit(int maxTeamLimit) {
        this.maxTeamLimit = maxTeamLimit;
    }


    public String getFirst_rank_prize() {
        return first_rank_prize;
    }

    public void setFirst_rank_prize(String first_rank_prize) {
        this.first_rank_prize = first_rank_prize;
    }

    @SerializedName("winning_percentage")
    private String winning_percentage = "";

    public String getWinning_percentage() {
        return winning_percentage;
    }

    public void setWinning_percentage(String winning_percentage) {
        this.winning_percentage = winning_percentage;
    }


    public String getChallenge_type() {
        return challenge_type;
    }

    public void setChallenge_type(String challenge_type) {
        this.challenge_type = challenge_type;
    }

    public void setTotalwinners(int totalwinners) {
        this.totalwinners = totalwinners;
    }

    public int getTotalwinners() {
        return totalwinners;
    }


    public String getEntryfee() {
        return entryfee;
    }

    public void setEntryfee(String entryfee) {
        this.entryfee = entryfee;
    }

    public void setGetjoinedpercentage(String getjoinedpercentage) {
        this.getjoinedpercentage = getjoinedpercentage;
    }

    public String getGetjoinedpercentage() {
        return getjoinedpercentage == null ? "0" : getjoinedpercentage;
    }

    public void setJoinedusers(int joinedusers) {
        this.joinedusers = joinedusers;
    }

    public int getJoinedusers() {
        return joinedusers;
    }

    public void setIsjoined(boolean isjoined) {
        this.isjoined = isjoined;
    }

    public boolean isIsjoined() {
        return isjoined;
    }

    public void setIsselected(boolean isselected) {
        this.isselected = isselected;
    }

    public boolean isIsselected() {
        return isselected;
    }

    public void setMatchkey(String matchkey) {
        this.matchkey = matchkey;
    }

    public String getMatchkey() {
        return matchkey;
    }

    public void setMultiEntry(int multiEntry) {
        this.multiEntry = multiEntry;
    }

    public int getMultiEntry() {
        return multiEntry;
    }

    public void setIsselectedid(String isselectedid) {
        this.isselectedid = isselectedid;
    }

    public String getIsselectedid() {
        return isselectedid;
    }

    public void setIsRunning(int isRunning) {
        this.isRunning = isRunning;
    }

    public int getIsRunning() {
        return isRunning;
    }

    public void setConfirmedChallenge(int confirmedChallenge) {
        this.confirmedChallenge = confirmedChallenge;
    }

    public int getConfirmedChallenge() {
        return confirmedChallenge;
    }

    public void setIsBonus(int isBonus) {
        this.isBonus = isBonus;
    }

    public int getIsBonus() {
        return isBonus;
    }

    public void setRefercode(String refercode) {
        this.refercode = refercode;
    }

    public String getRefercode() {
        return refercode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setWinAmount(int winAmount) {
        this.winAmount = winAmount;
    }

    public int getWinAmount() {
        return winAmount;
    }

    public void setMaximumUser(int maximumUser) {
        this.maximumUser = maximumUser;
    }

    public int getMaximumUser() {
        return maximumUser;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }


    public String getBonusPercent() {
        return bonusPercent;
    }

    public void setBonusPercent(String bonusPercent) {
        this.bonusPercent = bonusPercent;
    }

    public String showJoinAmountFree() {
//        if (isjoined) {
//            if (multiEntry == 1)
//                return "JOIN+";
//            else
//                return "INVITE";
//        } else
        return "FREE";
    }


    public String showJoinAmount() {
//        if (isjoined) {
//            if (multiEntry == 1)
//                return "JOIN+";
//            else
//                return "INVITE";
//        } else
        return "₹" + entryfee;
    }

    public String showJoinAmountForDetail() {
        return "₹" + entryfee;
    }

    public String showLeftusers() {
        return "Only " + (maximumUser - joinedusers) + " Spots left";
    }

    public int getLeftUser() {
        return maximumUser - joinedusers;
    }


    public String showTotalUsers() {
        return maximumUser + " Spots";
    }

    public String showTotalJoindUsers() {
        return joinedusers + " Spots";
    }


    public String showTotalWinners() {
        return "Winners " + totalwinners;
    }

    //for bonous
    public boolean isShowBTag() {
        if (isBonus == 1)
            return true;
        else
            return false;
    }

    //for confirm
    public boolean isShowCTag() {

        if (confirmedChallenge == 1)
            return true;
        else
            return false;
    }

    //for multi user
    public boolean isShowMTag() {
        if (multiEntry == 1)
            return true;
        else
            return false;
    }

    //for multi user
    public boolean isShowWDTag() {
        if (wd == 1)
            return true;
        else
            return false;
    }

    public String totalWinnersSt() {
        return "Winners " + totalwinners + "";
    }


    public String getPdf() {
        return pdf == null ? "" : pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public boolean isAnnounced() {
        if (!announcement.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String showMultiEntries() {
        if (multiEntry == 0) {
            return "Single Entry";
        } else {
            return "Upto 11 Entries";
        }
    }

    public int getWd() {
        return wd;
    }

    public void setWd(int wd) {
        this.wd = wd;
    }

    public int getUser_joined_count() {
        return user_joined_count;
    }

    public void setUser_joined_count(int user_joined_count) {
        this.user_joined_count = user_joined_count;
    }

    public String showFirstRankPrize() {
            return "₹" + first_rank_prize;
    }

    public int getPrizeColorCode() {
        return flexibleLeague == 1 ? R.color.unselected_tab : R.color.match_title_color;
    }

    public boolean isFlexibleLeague() {
        return flexibleLeague == 1;
    }

    public String getFlexibleWinAmount() {
        return "₹" + flexibleWinAmount;
    }

    public void setFlexibleWinAmount(String flexibleWinAmount) {
        this.flexibleWinAmount = flexibleWinAmount;
    }

    public String getFlexibleFirstRankPrize() {
        return "₹" + flexibleFirstRankPrize;
    }

    public void setFlexibleFirstRankPrize(String flexibleFirstRankPrize) {
        this.flexibleFirstRankPrize = flexibleFirstRankPrize;
    }

    public int getFlexibleLeague() {
        return flexibleLeague;
    }

    public void setFlexibleLeague(int flexibleLeague) {
        this.flexibleLeague = flexibleLeague;
    }

    public String showWinningAmout() {
        return "₹" + winAmount;
    }

    @Override
    public String toString() {
        return "Contest{" +
                "totalwinners=" + totalwinners +
                ", entryfee='" + entryfee + '\'' +
                ", first_rank_prize='" + first_rank_prize + '\'' +
                ", getjoinedpercentage='" + getjoinedpercentage + '\'' +
                ", dis_price='" + dis_price + '\'' +
                ", joinedusers=" + joinedusers +
                ", is_free=" + is_free +
                ", challenge_type='" + challenge_type + '\'' +
                ", isjoined=" + isjoined +
                ", isselected=" + isselected +
                ", matchkey='" + matchkey + '\'' +
                ", multiEntry=" + multiEntry +
                ", isselectedid='" + isselectedid + '\'' +
                ", isRunning=" + isRunning +
                ", confirmedChallenge=" + confirmedChallenge +
                ", isBonus=" + isBonus +
                ", refercode='" + refercode + '\'' +
                ", name='" + name + '\'' +
                ", id=" + id +
                ", winAmount=" + winAmount +
                ", maximumUser=" + maximumUser +
                ", status=" + status +
                ", bonusPercent='" + bonusPercent + '\'' +
                ", pdf='" + pdf + '\'' +
                ", announcement='" + announcement + '\'' +
                ", maxTeamLimit=" + maxTeamLimit +
                ", wd=" + wd +
                ", user_joined_count=" + user_joined_count +
                ", flexibleLeague=" + flexibleLeague +
                ", flexibleWinAmount='" + flexibleWinAmount + '\'' +
                ", flexibleFirstRankPrize='" + flexibleFirstRankPrize + '\'' +
                ", winning_percentage='" + winning_percentage + '\'' +
                '}';
    }
}