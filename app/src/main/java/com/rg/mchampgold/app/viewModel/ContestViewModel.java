package com.rg.mchampgold.app.viewModel;


import androidx.arch.core.util.Function;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.dataModel.ContestResponse;
import com.rg.mchampgold.app.dataModel.PlayerPointsResponse;
import com.rg.mchampgold.app.dataModel.RefreshScoreResponse;
import com.rg.mchampgold.app.repository.MatchRepository;
import com.rg.mchampgold.common.api.Resource;

import javax.inject.Inject;


public class ContestViewModel extends ViewModel {

    private MatchRepository mRepository;

    private final MutableLiveData<String> networkInfoObservable = new MutableLiveData<String>();

    private final MutableLiveData<ContestRequest> contestRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<ContestResponse>> contestLiveData = Transformations.switchMap(contestRequestMutableLiveData, new Function<ContestRequest, LiveData<Resource<ContestResponse>>>() {
        @Override
        public LiveData<Resource<ContestResponse>> apply(final ContestRequest input) {
            LiveData<Resource<ContestResponse>> resourceLiveData = mRepository.getContestList(input);
            final MediatorLiveData<Resource<ContestResponse>> mediator = new MediatorLiveData<Resource<ContestResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<ContestResponse>>() {
                @Override
                public void onChanged(Resource<ContestResponse> arrayListResource) {
                    ContestResponse resp = arrayListResource.getData();
                    Resource<ContestResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    /*
    Get the view model instance.
     */
    public static ContestViewModel create(FragmentActivity activity) {
        ContestViewModel viewModel = ViewModelProviders.of(activity).get(ContestViewModel.class);
        return viewModel;
    }

    public static ContestViewModel create(Fragment fragment) {
        ContestViewModel viewModel = ViewModelProviders.of(fragment).get(ContestViewModel.class);
        return viewModel;
    }

    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<ContestResponse>> getContestData() {
        return contestLiveData;
    }

    public void loadContestRequest(ContestRequest contestRequest) {
        contestRequestMutableLiveData.setValue(contestRequest);
    }


    @Inject
    public void setRepository(MatchRepository repository) {
        this.mRepository = repository;
    }


    public void clear() {
        networkInfoObservable.setValue(null);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }



    private final MutableLiveData<ContestRequest> refreshScoreRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<RefreshScoreResponse>> refreshScoreLiveData = Transformations.switchMap(refreshScoreRequestMutableLiveData, new Function<ContestRequest, LiveData<Resource<RefreshScoreResponse>>>() {
        @Override
        public LiveData<Resource<RefreshScoreResponse>> apply(final ContestRequest input) {
            LiveData<Resource<RefreshScoreResponse>> resourceLiveData = mRepository.getRefreshScore(input);
            final MediatorLiveData<Resource<RefreshScoreResponse>> mediator = new MediatorLiveData<Resource<RefreshScoreResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<RefreshScoreResponse>>() {
                @Override
                public void onChanged(Resource<RefreshScoreResponse> arrayListResource) {
                    RefreshScoreResponse resp = arrayListResource.getData();
                    Resource<RefreshScoreResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });

    public void loadRefreshScore(ContestRequest contestRequest) {
        refreshScoreRequestMutableLiveData.setValue(contestRequest);
    }


    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<RefreshScoreResponse>> getRefreshScore() {
        return refreshScoreLiveData;
    }

    private final MutableLiveData<ContestRequest> playerPointsRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<PlayerPointsResponse>> playerPointsLiveData = Transformations.switchMap(playerPointsRequestMutableLiveData, new Function<ContestRequest, LiveData<Resource<PlayerPointsResponse>>>() {
        @Override
        public LiveData<Resource<PlayerPointsResponse>> apply(final ContestRequest input) {
            LiveData<Resource<PlayerPointsResponse>> resourceLiveData = mRepository.getPlayerPoints(input);
            final MediatorLiveData<Resource<PlayerPointsResponse>> mediator = new MediatorLiveData<Resource<PlayerPointsResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<PlayerPointsResponse>>() {
                @Override
                public void onChanged(Resource<PlayerPointsResponse> arrayListResource) {
                    PlayerPointsResponse resp = arrayListResource.getData();
                    Resource<PlayerPointsResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });

    public void loadPlayerPointRequest(ContestRequest contestRequest) {
        playerPointsRequestMutableLiveData.setValue(contestRequest);
    }


    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<PlayerPointsResponse>> getPlayerPoints() {
        return playerPointsLiveData;
    }





/*    private final MutableLiveData<ContestRequest> contestByCategoryRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<CategoryByContestResponse>> contestCategoryResponse = Transformations.switchMap(contestByCategoryRequestMutableLiveData, new Function<ContestRequest, LiveData<Resource<CategoryByContestResponse>>>() {
        @Override
        public LiveData<Resource<CategoryByContestResponse>> apply(final ContestRequest input) {
            LiveData<Resource<CategoryByContestResponse>> resourceLiveData = mRepository.getContestList(input);
            final MediatorLiveData<Resource<CategoryByContestResponse>> mediator = new MediatorLiveData<Resource<CategoryByContestResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<CategoryByContestResponse>>() {
                @Override
                public void onChanged(Resource<CategoryByContestResponse> arrayListResource) {
                    CategoryByContestResponse resp = arrayListResource.getData();
                    Resource<ContestResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });*/
































}