package com.rg.mchampgold.app.view.adapter;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.PlayerInfoMatchesItem;
import com.rg.mchampgold.databinding.RecyclerItemPlayerInfoBinding;

import java.util.ArrayList;

public class PlayerInfoItemAdapter extends RecyclerView.Adapter<PlayerInfoItemAdapter.ViewHolder> {

    private ArrayList<PlayerInfoMatchesItem> playerInfoMatchesItems;


    public class ViewHolder extends RecyclerView.ViewHolder {
        final RecyclerItemPlayerInfoBinding binding;

        public ViewHolder(RecyclerItemPlayerInfoBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public PlayerInfoItemAdapter(ArrayList<PlayerInfoMatchesItem> moreInfoDataList) {
        this.playerInfoMatchesItems = moreInfoDataList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerItemPlayerInfoBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_player_info,parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setPlayerInfoMatchesItem(playerInfoMatchesItems.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return playerInfoMatchesItems.size();
    }


    public void updateData(ArrayList<PlayerInfoMatchesItem> playerInfoMatchesItems) {
        this.playerInfoMatchesItems = playerInfoMatchesItems;
        notifyDataSetChanged();
    }
}