package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class InvestmentResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    private String message;
    @SerializedName("result")
    @Expose
    private Result result;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("investment")
        @Expose
        private Investment investment;
        @SerializedName("wallet")
        @Expose
        private Wallet wallet;

        public Investment getInvestment() {
            return investment;
        }

        public void setInvestment(Investment investment) {
            this.investment = investment;
        }

        public Wallet getWallet() {
            return wallet;
        }

        public void setWallet(Wallet wallet) {
            this.wallet = wallet;
        }

    }
    public class Wallet {

        @SerializedName("total_deposit")
        @Expose
        private String totalDeposit;
        @SerializedName("total_withdrawal")
        @Expose
        private String totalWithdrawal;
        @SerializedName("profit")
        @Expose
        private String profit;
        @SerializedName("loss")
        @Expose
        private String loss;

        public String getTotalDeposit() {
            return totalDeposit;
        }

        public void setTotalDeposit(String totalDeposit) {
            this.totalDeposit = totalDeposit;
        }

        public String getTotalWithdrawal() {
            return totalWithdrawal;
        }

        public void setTotalWithdrawal(String totalWithdrawal) {
            this.totalWithdrawal = totalWithdrawal;
        }

        public String getProfit() {
            return profit;
        }

        public void setProfit(String profit) {
            this.profit = profit;
        }

        public String getLoss() {
            return loss;
        }

        public void setLoss(String loss) {
            this.loss = loss;
        }

    }

    public class Investment {

        @SerializedName("total_invest")
        @Expose
        private String totalInvest;
        @SerializedName("total_recieve")
        @Expose
        private String totalRecieve;
        @SerializedName("profit")
        @Expose
        private String profit;
        @SerializedName("loss")
        @Expose
        private String loss;

        public String getTotalInvest() {
            return totalInvest;
        }

        public void setTotalInvest(String totalInvest) {
            this.totalInvest = totalInvest;
        }

        public String getTotalRecieve() {
            return totalRecieve;
        }

        public void setTotalRecieve(String totalRecieve) {
            this.totalRecieve = totalRecieve;
        }

        public String getProfit() {
            return profit;
        }

        public void setProfit(String profit) {
            this.profit = profit;
        }

        public String getLoss() {
            return loss;
        }

        public void setLoss(String loss) {
            this.loss = loss;
        }

    }


}

