package com.rg.mchampgold.app.utils;

public class BasketBallSelectedPlayer {

    private int extra_player = 3;
    private int selectedPlayer = 0;
    private int localTeamPlayerCount = 0;
    private int visitorTeamPlayerCount = 0;

    private double total_credit = 0.0;

    //PG Point-Guard
    private int pg_min_count = 1;
    private int pg_max_count = 4;
    private int pg_selected =0;

    //SG Shooting-Guard
    private int sg_min_count = 1;
    private int sg_max_count = 4;
    private int sg_selected = 0;

    //SF Small-Forward
    private int sf_min_count = 1;
    private int sf_max_count = 4;
    private int sf_selected = 0;


    //PF Power-Forward
    private int pf_min_count = 1;
    private int pf_max_count = 4;
    private int pf_selected = 0;


    //C Centre
    private int c_min_count = 1;
    private int c_max_count = 4;
    private int c_selected = 0;

    private int wk_min_count = 1;
    private int wk_max_count = 1;
    private int wk_selected =0;

    private int ar_mincount = 1;
    private int ar_maxcount = 4;
    private int ar_selected = 0;

    private int bat_mincount = 3;
    private int bat_maxcount = 5;
    private int bat_selected = 0;


    private int bowl_mincount = 3;
    private int bowl_maxcount = 5;
    private int bowl_selected = 0;

    public int getExtra_player() {
        return extra_player;
    }

    public void setExtra_player(int extra_player) {
        this.extra_player = extra_player;
    }

    public int getSelectedPlayer() {
        return selectedPlayer;
    }

    public void setSelectedPlayer(int selectedPlayer) {
        this.selectedPlayer = selectedPlayer;
    }

    public int getLocalTeamPlayerCount() {
        return localTeamPlayerCount;
    }

    public void setLocalTeamPlayerCount(int localTeamPlayerCount) {
        this.localTeamPlayerCount = localTeamPlayerCount;
    }

    public int getVisitorTeamPlayerCount() {
        return visitorTeamPlayerCount;
    }

    public void setVisitorTeamPlayerCount(int visitorTeamPlayerCount) {
        this.visitorTeamPlayerCount = visitorTeamPlayerCount;
    }

    public double getTotal_credit() {
        return total_credit;
    }

    public void setTotal_credit(double total_credit) {
        this.total_credit = total_credit;
    }

    public int getPg_min_count() {
        return pg_min_count;
    }

    public void setPg_min_count(int pg_min_count) {
        this.pg_min_count = pg_min_count;
    }

    public int getPg_max_count() {
        return pg_max_count;
    }

    public void setPg_max_count(int pg_max_count) {
        this.pg_max_count = pg_max_count;
    }

    public int getPg_selected() {
        return pg_selected;
    }

    public void setPg_selected(int pg_selected) {
        this.pg_selected = pg_selected;
    }

    public int getSg_min_count() {
        return sg_min_count;
    }

    public void setSg_min_count(int sg_min_count) {
        this.sg_min_count = sg_min_count;
    }

    public int getSg_max_count() {
        return sg_max_count;
    }

    public void setSg_max_count(int sg_max_count) {
        this.sg_max_count = sg_max_count;
    }

    public int getSg_selected() {
        return sg_selected;
    }

    public void setSg_selected(int sg_selected) {
        this.sg_selected = sg_selected;
    }

    public int getSf_min_count() {
        return sf_min_count;
    }

    public void setSf_min_count(int sf_min_count) {
        this.sf_min_count = sf_min_count;
    }

    public int getSf_max_count() {
        return sf_max_count;
    }

    public void setSf_max_count(int sf_max_count) {
        this.sf_max_count = sf_max_count;
    }

    public int getSf_selected() {
        return sf_selected;
    }

    public void setSf_selected(int sf_selected) {
        this.sf_selected = sf_selected;
    }

    public int getPf_min_count() {
        return pf_min_count;
    }

    public void setPf_min_count(int pf_min_count) {
        this.pf_min_count = pf_min_count;
    }

    public int getPf_max_count() {
        return pf_max_count;
    }

    public void setPf_max_count(int pf_max_count) {
        this.pf_max_count = pf_max_count;
    }

    public int getPf_selected() {
        return pf_selected;
    }

    public void setPf_selected(int pf_selected) {
        this.pf_selected = pf_selected;
    }

    public int getC_min_count() {
        return c_min_count;
    }

    public void setC_min_count(int c_min_count) {
        this.c_min_count = c_min_count;
    }

    public int getC_max_count() {
        return c_max_count;
    }

    public void setC_max_count(int c_max_count) {
        this.c_max_count = c_max_count;
    }

    public int getC_selected() {
        return c_selected;
    }

    public void setC_selected(int c_selected) {
        this.c_selected = c_selected;
    }

    public int getWk_min_count() {
        return wk_min_count;
    }

    public void setWk_min_count(int wk_min_count) {
        this.wk_min_count = wk_min_count;
    }

    public int getWk_max_count() {
        return wk_max_count;
    }

    public void setWk_max_count(int wk_max_count) {
        this.wk_max_count = wk_max_count;
    }

    public int getWk_selected() {
        return wk_selected;
    }

    public void setWk_selected(int wk_selected) {
        this.wk_selected = wk_selected;
    }

    public int getAr_mincount() {
        return ar_mincount;
    }

    public void setAr_mincount(int ar_mincount) {
        this.ar_mincount = ar_mincount;
    }

    public int getAr_maxcount() {
        return ar_maxcount;
    }

    public void setAr_maxcount(int ar_maxcount) {
        this.ar_maxcount = ar_maxcount;
    }

    public int getAr_selected() {
        return ar_selected;
    }

    public void setAr_selected(int ar_selected) {
        this.ar_selected = ar_selected;
    }

    public int getBat_mincount() {
        return bat_mincount;
    }

    public void setBat_mincount(int bat_mincount) {
        this.bat_mincount = bat_mincount;
    }

    public int getBat_maxcount() {
        return bat_maxcount;
    }

    public void setBat_maxcount(int bat_maxcount) {
        this.bat_maxcount = bat_maxcount;
    }

    public int getBat_selected() {
        return bat_selected;
    }

    public void setBat_selected(int bat_selected) {
        this.bat_selected = bat_selected;
    }

    public int getBowl_mincount() {
        return bowl_mincount;
    }

    public void setBowl_mincount(int bowl_mincount) {
        this.bowl_mincount = bowl_mincount;
    }

    public int getBowl_maxcount() {
        return bowl_maxcount;
    }

    public void setBowl_maxcount(int bowl_maxcount) {
        this.bowl_maxcount = bowl_maxcount;
    }

    public int getBowl_selected() {
        return bowl_selected;
    }

    public void setBowl_selected(int bowl_selected) {
        this.bowl_selected = bowl_selected;
    }
}
