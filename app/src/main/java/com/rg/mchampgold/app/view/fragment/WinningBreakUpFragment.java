package com.rg.mchampgold.app.view.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.GetWinnerScoreCardResponse;
import com.rg.mchampgold.app.dataModel.WinnerScoreCardItem;
import com.rg.mchampgold.app.utils.MyDividerItemDecoration;
import com.rg.mchampgold.app.view.adapter.WinnerBreakupItemAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentWinningBreakupBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;


public class WinningBreakUpFragment extends Fragment {


    private String contestId;
    private String matchKey;
    private String winningAmount,winning_percent ="";
    private WinnerBreakupItemAdapter mAdapter;
    ArrayList<WinnerScoreCardItem> list = new ArrayList<>();

    @Inject
    OAuthRestService oAuthRestService;
    String sportKey="";


    private FragmentWinningBreakupBinding fragmentWinningBreakupBinding;


    public static WinningBreakUpFragment newInstance(String matchKey, String contestId, String winningAmount, String winning_percent,String sportKey) {
        WinningBreakUpFragment myFragment = new WinningBreakUpFragment();
        Bundle args = new Bundle();
        args.putString(Constants.KEY_MATCH_KEY,matchKey);
        args.putString(Constants.CONTEST_ID,contestId);
        args.putString(Constants.KEY_WINING_AMOUNT,winningAmount);
        args.putString(Constants.KEY_WINING_PERCENT,winning_percent);
        args.putString("sportKey", sportKey);
        myFragment.setArguments(args);
        return myFragment;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentWinningBreakupBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_winning_breakup, container, false);
        setupRecyclerView();
        return fragmentWinningBreakupBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getAppComponent().inject(this);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            contestId = getArguments().getString(Constants.CONTEST_ID);
            matchKey = getArguments().getString(Constants.KEY_MATCH_KEY);
            winningAmount = getArguments().getString(Constants.KEY_WINING_AMOUNT);
            sportKey = getArguments().getString("sportKey");
        }

    }

    private void setupRecyclerView() {
        mAdapter = new WinnerBreakupItemAdapter(list);
        fragmentWinningBreakupBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        fragmentWinningBreakupBinding.recyclerView.setLayoutManager(mLayoutManager);
        fragmentWinningBreakupBinding.recyclerView.setAdapter(mAdapter);

        // adding custom divider line with padding 3dp
        fragmentWinningBreakupBinding.recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, 1));
        fragmentWinningBreakupBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        fragmentWinningBreakupBinding.recyclerView.setAdapter(mAdapter);

        getWinnerPriceCard();


      /*  if (winning_percent!=null) {
            if (!winning_percent.equals("") && !winning_percent.equals("0")) {
                fragmentWinningBreakupBinding.tvWinningPercentage.setVisibility(View.VISIBLE);
                fragmentWinningBreakupBinding.tvWinningPercentage.setText("Winners " + winning_percent + "%");
            } else {
                fragmentWinningBreakupBinding.tvWinningPercentage.setVisibility(View.GONE);
            }
        }*/
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void getWinnerPriceCard() {
        fragmentWinningBreakupBinding.setRefreshing(true);
        ContestRequest contestRequest = new ContestRequest();
        contestRequest.setMatchKey(matchKey);
        contestRequest.setLeagueId(contestId);
        contestRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<GetWinnerScoreCardResponse> bankDetailResponseCustomCall = oAuthRestService.getWinnersPriceCard(contestRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<GetWinnerScoreCardResponse>() {
            @Override
            public void success(Response<GetWinnerScoreCardResponse> response) {
                fragmentWinningBreakupBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    GetWinnerScoreCardResponse  getWinnerScoreCardResponse = response.body();
                    if(getWinnerScoreCardResponse.getStatus() ==1 && getWinnerScoreCardResponse.getResult().size()>0) {
                        list = getWinnerScoreCardResponse.getResult();
                        mAdapter.update(list);
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentWinningBreakupBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisible()){
            if(isVisibleToUser){

            }
        }
    }

}