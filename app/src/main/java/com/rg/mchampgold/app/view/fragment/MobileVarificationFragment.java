package com.rg.mchampgold.app.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.NormalResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.VerifyOtpBtmSheet;

import com.rg.mchampgold.app.view.activity.VerifyAccountActivity;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentMobileVarificationBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class MobileVarificationFragment extends Fragment implements VerifyOtpBtmSheet.ItemClickListener {

    Context context;
    FragmentMobileVarificationBinding mBinding;
    @Inject
    OAuthRestService oAuthRestService;
    public static final int DIALOG_FRAGMENT = 1;
    private int mStackLevel = 0;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);

        if (savedInstanceState != null) {
            mStackLevel = savedInstanceState.getInt("level");
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("level", mStackLevel);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_mobile_varification, container, false);
        context = getActivity();

        AppUtils.disableCopyPaste(mBinding.etMobile);

        showVerifiedNotVerifiedLayout();

        mBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyEmailByOtp(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL));
            }
        });

        mBinding.btnSendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBinding.etMobile.getText().toString().trim().length() != 10)
                    AppUtils.showErrorr((AppCompatActivity) context, "Please enter valid mobile number");
                else {
                    verifyByMobile(mBinding.etMobile.getText().toString().trim());
                }
            }
        });
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        showVerifiedNotVerifiedLayout();
    }

    private void showVerifiedNotVerifiedLayout() {
        if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 0) == 1) {
            mBinding.emailVerifiedLayout.setVisibility(View.VISIBLE);
            mBinding.emailNotVerifiedLayout.setVisibility(View.GONE);
        } else {
            mBinding.emailNotVerifiedLayout.setVisibility(View.VISIBLE);
            mBinding.emailVerifiedLayout.setVisibility(View.GONE);
        }

        mBinding.tvEmailDesc.setText(Html.fromHtml("We have sent email to <u><b>" + MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL) +
                "</b></u> .Please click on link mentioned in email to verify your email."));
        //mBinding.tvEmailSend.setText(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL));
        mBinding.tvVerifiedEmail.setText(Html.fromHtml("<u>" + MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL) + "</u>"));

        if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0) == 1) {
            mBinding.mobileVerifiedLayout.setVisibility(View.VISIBLE);
            mBinding.mobileNotVerifiedLayout.setVisibility(View.GONE);
        } else {
            mBinding.mobileNotVerifiedLayout.setVisibility(View.VISIBLE);
            mBinding.mobileVerifiedLayout.setVisibility(View.GONE);
        }

        mBinding.tvVerifiedMobileNo.setText(Html.fromHtml("<u>"+MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_MOBILE)+"</u>"));
    }


    private void verifyEmailByOtp(String email) {
        mBinding.setRefreshing(true);
        String userId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(userId);
        baseRequest.setEmail(email);
        CustomCallAdapter.CustomCall<NormalResponse> normalResponseCustomCall = oAuthRestService.verifyEmailByOtp(baseRequest);
        normalResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<NormalResponse>() {
            @Override
            public void success(Response<NormalResponse> response) {
                mBinding.setRefreshing(false);
                NormalResponse normalResponse = response.body();
                if (normalResponse.getStatus() == 1) {
                    AppUtils.showSuccess((AppCompatActivity) context, normalResponse.getMessage());
                    //   mBinding.tvEmailSend.setText(email);
                } else {
                    AppUtils.showErrorr((AppCompatActivity) context, normalResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void verifyByMobile(String mobile) {
        mBinding.setRefreshing(true);
        String userId = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(userId);
        baseRequest.setMobile(mobile);
        baseRequest.setType("2");
        CustomCallAdapter.CustomCall<NormalResponse> normalResponseCustomCall = oAuthRestService.verifyByMobile(baseRequest);
        normalResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<NormalResponse>() {
            @Override
            public void success(Response<NormalResponse> response) {
                mBinding.setRefreshing(false);
                NormalResponse normalResponse = response.body();
                if (normalResponse.getStatus() == 1) {
                    showBottomSheet(mobile);
                } else {
                    AppUtils.showErrorr((AppCompatActivity) context, normalResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void showBottomSheet(String mobile) {
        mStackLevel++;

        VerifyOtpBtmSheet addPhotoBottomDialogFragment = VerifyOtpBtmSheet.newInstance(mobile);
        addPhotoBottomDialogFragment.setTargetFragment(this, DIALOG_FRAGMENT);
        addPhotoBottomDialogFragment.show(getFragmentManager(),
                VerifyOtpBtmSheet.TAG);


    }



    @Override
    public void onItemClick() {
        showVerifiedNotVerifiedLayout();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case DIALOG_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    ((VerifyAccountActivity) getActivity()).mBinding.vp.getAdapter().notifyDataSetChanged();
                    showVerifiedNotVerifiedLayout();
                }

                break;
        }
    }


}
