package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.InvestmentResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityInvestmentBinding;

import javax.inject.Inject;

import retrofit2.Response;

public class InvestmentActivity extends AppCompatActivity {

    private static final String TAG = "InvestmentActivity=>";
    ActivityInvestmentBinding binding;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         binding = DataBindingUtil.setContentView(this, R.layout.activity_investment);
        MyApplication.getAppComponent().inject(InvestmentActivity.this);
        intialize();
    }

    private void intialize() {

        setSupportActionBar(binding.linearToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Fantasy Alayser");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        getInvestmentData();
    }

    private void getInvestmentData() {
        binding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<InvestmentResponse> call = oAuthRestService.investmentData(baseRequest);
        call.enqueue(new CustomCallAdapter.CustomCallback<InvestmentResponse>() {
            @Override
            public void success(Response<InvestmentResponse> response) {
                binding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus() == 1) {

                        binding.investmentTv.setText("₹"+response.body().getResult().getInvestment().getTotalInvest());
                        binding.receivedTv.setText("₹"+response.body().getResult().getInvestment().getTotalRecieve());
                        binding.investProfitTv.setText("₹"+response.body().getResult().getInvestment().getProfit());
                        binding.investLossTv.setText("₹"+response.body().getResult().getInvestment().getLoss());
                        binding.depositTv.setText("₹"+response.body().getResult().getWallet().getTotalDeposit());
                        binding.withdrawTv.setText("₹"+response.body().getResult().getWallet().getTotalWithdrawal());
                        binding.walletProfitTv.setText("₹"+response.body().getResult().getWallet().getProfit());
                        binding.walletLossTv.setText("₹"+response.body().getResult().getWallet().getLoss());

                    } else {
                        AppUtils.showErrorr(InvestmentActivity.this, response.body().getMessage());

                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                binding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
