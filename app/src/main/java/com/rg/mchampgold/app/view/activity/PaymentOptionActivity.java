package com.rg.mchampgold.app.view.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

//import com.gocashfree.cashfreesdk.CFClientInterface;
//import com.gocashfree.cashfreesdk.CFPaymentService;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.AddPaymentRequest;
import com.rg.mchampgold.app.api.request.GetOrderIdRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.AddPaymentValueResponse;
import com.rg.mchampgold.app.dataModel.MoreInfoData;
import com.rg.mchampgold.app.dataModel.OrderIdResponse;
import com.rg.mchampgold.app.payTm.PaytmMerchantActivity;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.MyDividerItemDecoration;
import com.rg.mchampgold.app.view.adapter.PaymentOptionsAdapter;
import com.rg.mchampgold.app.view.interfaces.OnMoreItemClickListener;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityPaymentOptionsBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import retrofit2.Response;

//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_NOTIFY_URL;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;
//import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_PAYMENT_MODES;

public class PaymentOptionActivity extends AppCompatActivity implements OnMoreItemClickListener {

    ActivityPaymentOptionsBinding mBinding;
    String addedValue = "";
    String orderId = "";
    String razorPayMentId = "";
    @Inject
    OAuthRestService oAuthRestService;
    private PaymentOptionsAdapter mAdapter;
    private int promoId;

    FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment_options);
        MyApplication.getAppComponent().inject(PaymentOptionActivity.this);
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            addedValue = getIntent().getExtras().getString("addedValue");
            orderId = getIntent().getExtras().getString("orderId");
            promoId = getIntent().getExtras().getInt("promoId", 0);
        }

        mBinding.tvAddedAmount.setText("₹" + addedValue);
        initialize();
        setupRecyclerView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            setResult(Activity.RESULT_CANCELED);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupRecyclerView() {
        String[] stringTitleArray = getResources().getStringArray(R.array.payment_options_array);
        int[] imgs = new int[]{
                R.drawable.ic_debit,
                R.drawable.ic_net_banking,
                R.drawable.ic_add_wallet,
                R.drawable.ic_upi,
                R.drawable.paytm,
//                R.drawable.ic_cashfree,
//                R.drawable.razorpay

        };


        List<MoreInfoData> moreInfoDataList = new ArrayList<>();
        for (int i = 0; i < stringTitleArray.length; i++) {
            MoreInfoData moreInfoData = new MoreInfoData();
            moreInfoData.setName(stringTitleArray[i]);
            moreInfoData.setResourceId(imgs[i]);
            moreInfoDataList.add(moreInfoData);
        }

        mAdapter = new PaymentOptionsAdapter(moreInfoDataList, this, true);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        // adding custom divider line with padding 3dp
        mBinding.recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 1));
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.payment_options));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void openRazorPayMerchantActivity(String paymentMethod) {
        Intent intent = new Intent(PaymentOptionActivity.this, RazorPayMerchantActivity.class);
        intent.putExtra("addedValue", addedValue);
        intent.putExtra("orderId", orderId);
        intent.putExtra("method", paymentMethod);
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            if (!data.getExtras().getString("result").equals("")) {
                razorPayMentId = data.getExtras().getString("result");
                addBalance(Constants.RAZORPAY);
            } else {
                Toast.makeText(this, "Payment failed", Toast.LENGTH_SHORT).show();
                finishActivity();
            }
        } else if (requestCode == 102) {
            finishActivity();
        }
    }

    private void addBalance(String payment_type) {
        mBinding.setRefreshing(true);
        AddPaymentRequest addPaymentRequest = new AddPaymentRequest();
        addPaymentRequest.setAmount(addedValue);
        addPaymentRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        addPaymentRequest.setOrder_id(orderId);
        addPaymentRequest.setPaymentby(payment_type);
        addPaymentRequest.setPromo_id(promoId + "");
        addPaymentRequest.setPayment_id(razorPayMentId);
        CustomCallAdapter.CustomCall<AddPaymentValueResponse> orderIdResponse = oAuthRestService.addPayment(addPaymentRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<AddPaymentValueResponse>() {
            @Override
            public void success(Response<AddPaymentValueResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    AddPaymentValueResponse addPaymentValueResponse = response.body();
                    if (addPaymentValueResponse.getStatus() == 1) {
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, addPaymentValueResponse.getResult().getAmount() + "");
                        if (addPaymentValueResponse.getResult().getMsg().equalsIgnoreCase("Payment done.")) {
                            AppUtils.showSuccess(PaymentOptionActivity.this, "Payment add successfully");
                        }
                    } else {
                        Toast.makeText(PaymentOptionActivity.this, addPaymentValueResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(PaymentOptionActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
                finishActivity();
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void finishActivity() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    // get Order Id.
    private void getOrderId(String paymentMethod) {
        mBinding.setRefreshing(true);
        GetOrderIdRequest getOrderIdRequest = new GetOrderIdRequest();
        getOrderIdRequest.setAmount(addedValue);
        getOrderIdRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<OrderIdResponse> orderIdResponse = oAuthRestService.getOrderId(getOrderIdRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<OrderIdResponse>() {
            @Override
            public void success(Response<OrderIdResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    OrderIdResponse orderIdResponse1 = response.body();
                    orderId = orderIdResponse1.getId();
                    openRazorPayMerchantActivity(paymentMethod);
                } else {
                    Toast.makeText(PaymentOptionActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    @Override
    public void onMoreItemClick(int position, String title) {
        switch (position) {
//            case 0:
//                getOrderId("card");
//                break;
//
//            case 1:
//                getOrderId("netbanking");
//                break;
//
//            case 2:
//                getOrderId("wallet");
//                break;
//
//            case 3:
//                getOrderId("upi");
//                break;

//            case 4:
//                Intent intent = new Intent(PaymentOptionActivity.this, PaytmMerchantActivity.class);
//                intent.putExtra("addedValue", addedValue);
//                intent.putExtra("promoId", promoId);
//                startActivityForResult(intent, 102);
//                break;

//            case 5:
//                doCashfreePayment();
//                break;

            /* case 2:
                startActivity(new Intent(MyApplication.appContext, InviteFriendActivity.class));
                break;
            case 3:
                startActivity(new Intent(MyApplication.appContext, VerifyAccountActivity.class));
                break;
            case 4:
                //openWebViewActivity(title,"fantasy_point_system.html");
                break;
            case 5:
                //  openWebViewActivity(title,"referlist.html");
                break;
            case 6:
                //  openWebViewActivity(title,"privacy-policy/");
                break;

            case 7:
                // openWebViewActivity(title,"terms/");
                break;

            case 8:
                // openWebViewActivity(title,"about-us/");
                break;

            case 9:
                //   openWebViewActivity(title,"how_to_play.html");
                break;
            case 10:
                startActivity(new Intent(MyApplication.appContext, ContactusActivity.class));
                break;
            case 11:
                //   logout();
                break;*/
            default:
                break;
        }
    }


      /* mBinding.llCard.setOnClickListener(view -> openRazorPayMerchantActivity("card"));

        mBinding.llNetBanking.setOnClickListener(view -> openRazorPayMerchantActivity("netbanking"));

        mBinding.llWallet.setOnClickListener(view -> openRazorPayMerchantActivity("ic_my_account_wallet"));

        mBinding.llUpi.setOnClickListener(view -> openRazorPayMerchantActivity("upi"));

        mBinding.paytmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PaymentOptionActivity.this,PaytmMerchantActivity.class);
                intent.putExtra("addedValue",addedValue);
                startActivityForResult(intent,102);
            }
        });*/


//    public void doCashfreePayment() {
//        String stage = "PROD";
//
//        Map<String, String> params = new HashMap<>();
//        // Change this to reflect your own APP_ID. Refer CashfreeSDK Documentation or contact .
//        params.put(PARAM_APP_ID, MyApplication.cash_free_app_id);
//        params.put(PARAM_ORDER_ID, orderId);
//        params.put(PARAM_ORDER_AMOUNT, addedValue);
//        params.put(PARAM_ORDER_NOTE, "");
//        params.put(PARAM_CUSTOMER_NAME, MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_NAME));
//        params.put(PARAM_CUSTOMER_PHONE, MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_MOBILE));
//        params.put(PARAM_CUSTOMER_EMAIL, MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL));
//        params.put(PARAM_PAYMENT_MODES, "");
//        params.put(PARAM_NOTIFY_URL, MyApplication.cashfree_notify_url);
//
//        for (Map.Entry entry : params.entrySet()) {
//            Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
//        }
//
//        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
//        cfPaymentService.setOrientation(0);
//        cfPaymentService.doPayment(this, params, MyApplication.cashfree_checksum, this, stage);
//
//    }
//
//    @SuppressLint("WrongConstant")
//    @Override
//    public void onSuccess(Map<String, String> map) {
//
//        if (map.get("txStatus").equalsIgnoreCase("SUCCESS")) {
//            addBalance(Constants.PAYTM);
//         /*   Intent ii = new Intent(PaymentOptionActivity.this, PaymentSuccess.class);
//            ii.putExtra("payid", map.get("orderId"));
//            ii.putExtra("TXNID", map.get("orderId"));
//            ii.putExtra("email", MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_EMAIL));
//            ii.putExtra("from", "cashfree");
//            ii.putExtra("price", addedValue);
//            startActivity(ii);
//            finish();*/
//        } else {
//            LayoutInflater inflater1 = (getLayoutInflater());
//            View layout = inflater1.inflate(R.layout.custom_toast1,
//                    findViewById(R.id.toast_layout_root));
//            TextView text = layout.findViewById(R.id.text);
//            text.setText("Sorry, your payment failed. No charges were made.");
//            Toast toast = new Toast(getApplicationContext());
//            toast.setGravity(Gravity.BOTTOM, 0, +150);
//            toast.setDuration(2000);
//            toast.setView(layout);
//            toast.show();
//            finish();
//        }
//    }
//
//    @SuppressLint("WrongConstant")
//    @Override
//    public void onFailure(Map<String, String> map) {
//        //  Log.e("onSuccess", " " + map);
//
//        if (map.get("txMsg").equalsIgnoreCase("ERROR:Invalid customer phone number provided")) {
//            LayoutInflater inflater1 = (getLayoutInflater());
//            View layout = inflater1.inflate(R.layout.custom_toast1,
//                    findViewById(R.id.toast_layout_root));
//
//            TextView text = (TextView) layout.findViewById(R.id.text);
//            text.setText("Please verify your mobile number first!");
//            Toast toast = new Toast(getApplicationContext());
//            toast.setGravity(Gravity.BOTTOM, 0, +150);
//            toast.setDuration(2000);
//            toast.setView(layout);
//            toast.show();
//        }
//    }
//
//    @Override
//    public void onNavigateBack() {
//
//    }

}
