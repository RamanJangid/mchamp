package com.rg.mchampgold.app.view.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.BankDetailItem;
import com.rg.mchampgold.app.dataModel.BankDetailResponse;
import com.rg.mchampgold.app.dataModel.WIthDrawAmoutResponse;
import com.rg.mchampgold.app.dataModel.WithdrawAmountResponseItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityWithdrawCashBinding;

import javax.inject.Inject;

import retrofit2.Response;

public class WithdrawCashActivity extends AppCompatActivity {

    ActivityWithdrawCashBinding mBinding;

    @Inject
    OAuthRestService oAuthRestService;
    String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_withdraw_cash);
        MyApplication.getAppComponent().inject(WithdrawCashActivity.this);
        initialize();

        mBinding.bankCardView.setOnClickListener(v -> {
            mBinding.llBank.setBackgroundResource(R.drawable.blue_border_round_corners);
            mBinding.llPaytm.setBackground(null);
            type = "bank";
        });

        mBinding.paytmCardView.setOnClickListener(v -> {
            mBinding.llPaytm.setBackgroundResource(R.drawable.blue_border_round_corners);
            mBinding.llBank.setBackground(null);
            type = "paytm";
        });

        mBinding.btnProceed.setOnClickListener(view -> {
            if (type.equalsIgnoreCase("paytm")) {
                if (mBinding.withdrawMoney.getText().toString().equals("") || Integer.parseInt(mBinding.withdrawMoney.getText().toString()) < Constants.MIN_PAYTM_WITHDRAW_AMOUNT)
                    AppUtils.showErrorr(WithdrawCashActivity.this, "Please enter minimum ₹ " + Constants.MIN_PAYTM_WITHDRAW_AMOUNT + " balance");
                else if (Integer.parseInt(mBinding.withdrawMoney.getText().toString()) > Constants.MAX_PAYTM_WITHDRAW_AMOUNT)
                    AppUtils.showErrorr(WithdrawCashActivity.this, "You can withdrawal up to " + Constants.MAX_PAYTM_WITHDRAW_AMOUNT + " from paytm");
                else if (Double.parseDouble(mBinding.withdrawMoney.getText().toString()) > Double.parseDouble(MyApplication.tinyDB.getString(Constants.KEY_USER_WINING_AMOUNT)))
                    AppUtils.showErrorr(WithdrawCashActivity.this, "Insufficient Fund");
                else {
                    withdrawUserBalance(mBinding.withdrawMoney.getText().toString());
                }
            } else if (type.equalsIgnoreCase("bank")) {
                if (mBinding.withdrawMoney.getText().toString().equals("") || Integer.parseInt(mBinding.withdrawMoney.getText().toString()) < Constants.MIN_BANK_WITHDRAW_AMOUNT)
                    AppUtils.showErrorr(WithdrawCashActivity.this, "Please enter minimum ₹ " + Constants.MIN_BANK_WITHDRAW_AMOUNT + " balance");

                else if (Integer.parseInt(mBinding.withdrawMoney.getText().toString()) > Constants.MAX_BANK_WITHDRAW_AMOUNT)
                    AppUtils.showErrorr(WithdrawCashActivity.this, "You can withdrawal up to " + Constants.MAX_BANK_WITHDRAW_AMOUNT + " from bank");
                else if (Double.parseDouble(mBinding.withdrawMoney.getText().toString()) > Double.parseDouble(MyApplication.tinyDB.getString(Constants.KEY_USER_WINING_AMOUNT)))
                    AppUtils.showErrorr(WithdrawCashActivity.this, "Insufficient Fund");
                else {
                    withdrawUserBalance(mBinding.withdrawMoney.getText().toString());
                }
            } else {
                AppUtils.showErrorr(WithdrawCashActivity.this, "Please choose a withdrawal option");
            }

            /*if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 0) == 1) {

                int selectedId = mBinding.withDrawRadio.getCheckedRadioButtonId();


                // find the radiobutton by returned id
                *//*RadioButton radioSexButton = findViewById(selectedId);

                if (radioSexButton.getText().toString().equalsIgnoreCase("Paytm")) {
                    type = "paytm";
                } else {
                    type = "bank";
                }*//*



            } else {
                AppUtils.showErrorr(WithdrawCashActivity.this, "You need to verify your account first");
            }*/
        });

    }


    public void withdrawUserBalance(String amount) {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setAmount(mBinding.withdrawMoney.getText().toString().trim());
        baseRequest.setPaymentType(type);
        CustomCallAdapter.CustomCall<WIthDrawAmoutResponse> myBalanceResponseCustomCall = oAuthRestService.withDrawAmount(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<WIthDrawAmoutResponse>() {
            @Override
            public void success(Response<WIthDrawAmoutResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    WIthDrawAmoutResponse wIthDrawAmoutResponse = response.body();
                    if (wIthDrawAmoutResponse.getStatus() == 1) {
                        WithdrawAmountResponseItem withdrawAmountResponseItem = wIthDrawAmoutResponse.getResult();
                        if (withdrawAmountResponseItem.getStatus() == 0)
                            AppUtils.showWithdrawError(WithdrawCashActivity.this, withdrawAmountResponseItem.getMsg());
                        else if (withdrawAmountResponseItem.getStatus() == 3)
                            AppUtils.showWithdrawError(WithdrawCashActivity.this, "You need to verify your account first.");
                        else {
                            showSuccess(withdrawAmountResponseItem.getMsg());
//                            AppUtils.showWithdrawSuccess(WithdrawCashActivity.this, withdrawAmountResponseItem.getMsg());
                            if (withdrawAmountResponseItem.getStatus() == 1) {
                                mBinding.availableCash.setText(String.format("₹ %s", MyApplication.tinyDB.getString(Constants.KEY_USER_WINING_AMOUNT)));
                                MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, withdrawAmountResponseItem.getAmount() + "");
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        finish();
//                                    }
//                                },2500);
                            }
                        }
                    } else {
                        AppUtils.showWithdrawError(WithdrawCashActivity.this, wIthDrawAmoutResponse.getMessage());
                    }
                }else {
                    AppUtils.showWithdrawError(WithdrawCashActivity.this, "Something went wrong...");
                }
            }

            @Override
            public void failure(ApiException e) {
                AppUtils.showErrorr(WithdrawCashActivity.this, "Something went wrong...");
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        mBinding.availableCash.setText("₹ " + MyApplication.tinyDB.getString(Constants.KEY_USER_WINING_AMOUNT));
    }

    public void showSuccess(String msg) {

        Dialog dialog = new Dialog(WithdrawCashActivity.this);
        dialog.setContentView(R.layout.show_finish_match_status);
        TextView title=dialog.findViewById(R.id.title);
        TextView message=dialog.findViewById(R.id.message);
        title.setText("Success");
        message.setText(msg);
        Button close=dialog.findViewById(R.id.btnClose);
        close.setOnClickListener(v -> finish());
        close.setText("OK");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }
    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.withdraw));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0) == 1) {
            mBinding.paytmCardView.setVisibility(View.VISIBLE);
        } else {
            mBinding.paytmCardView.setVisibility(View.GONE);
        }

        if (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 0) == 1) {
            mBinding.bankCardView.setVisibility(View.VISIBLE);
        } else {
            mBinding.bankCardView.setVisibility(View.GONE);
        }


//        mBinding.tvPaytmMinMaxText.setText("Paytm min:- " + Constants.MIN_PAYTM_WITHDRAW_AMOUNT + " max:- " + Constants.MAX_PAYTM_WITHDRAW_AMOUNT);
        mBinding.tvBankMinMax.setText("Bank min:- " + Constants.MIN_BANK_WITHDRAW_AMOUNT + " , Max:- " + Constants.MAX_BANK_WITHDRAW_AMOUNT);
        mBinding.payTmAccNoTv.setText("+91 " + MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_MOBILE));

        getBankDetails();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void getBankDetails() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<BankDetailResponse> bankDetailResponseCustomCall = oAuthRestService.getBankDetail(baseRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<BankDetailResponse>() {
            @Override
            public void success(Response<BankDetailResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    BankDetailResponse bankDetailResponse = response.body();
                    if (bankDetailResponse.getStatus() == 1 && bankDetailResponse.getResult().size() > 0) {
                        BankDetailItem bankDetailItem = bankDetailResponse.getResult().get(0);
                        if (bankDetailItem.getStatus() == 1 || bankDetailItem.getStatus() == 0) {

                            mBinding.userAccountNoTxt.setText(bankDetailItem.getAccno());
                            mBinding.userBankNameTxt.setText(bankDetailItem.getBankname());

                        } else {
                            //AppUtils.mShowDialogd(WithdrawCashActivity.this,"You need to verify your account first. ");
                        }
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }
}
