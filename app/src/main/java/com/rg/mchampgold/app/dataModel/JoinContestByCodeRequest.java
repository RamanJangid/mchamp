package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class JoinContestByCodeRequest{

	@SerializedName("user_id")
	private String userId;

	@SerializedName("matchkey")
	private String matchkey;

	@SerializedName("getcode")
	private String getcode;

	@SerializedName("sport_key")
	private String sport_key;

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setMatchkey(String matchkey){
		this.matchkey = matchkey;
	}

	public String getMatchkey(){
		return matchkey;
	}

	public void setGetcode(String getcode){
		this.getcode = getcode;
	}

	public String getGetcode(){
		return getcode;
	}

	@Override
 	public String toString(){
		return 
			"JoinContestByCodeRequest{" + 
			"user_id = '" + userId + '\'' + 
			",matchkey = '" + matchkey + '\'' + 
			",getcode = '" + getcode + '\'' + 
			"}";
		}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}
}