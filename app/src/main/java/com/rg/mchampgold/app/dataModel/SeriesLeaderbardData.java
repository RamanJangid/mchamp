package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SeriesLeaderbardData {

    @SerializedName("leaderboard")
    ArrayList<SeriesLeaderboardDataModel> leaderboard;
    @SerializedName("wining_breakup")
    ArrayList<WinningBreakupDataModel> wining_breakup;
    @SerializedName("is_dummy_data")
    boolean is_dummy_data;

    public ArrayList<SeriesLeaderboardDataModel> getLeaderboard() {
        return leaderboard;
    }

    public void setLeaderboard(ArrayList<SeriesLeaderboardDataModel> leaderboard) {
        this.leaderboard = leaderboard;
    }

    public ArrayList<WinningBreakupDataModel> getWining_breakup() {
        return wining_breakup;
    }

    public void setWining_breakup(ArrayList<WinningBreakupDataModel> wining_breakup) {
        this.wining_breakup = wining_breakup;
    }

    public boolean isIs_dummy_data() {
        return is_dummy_data;
    }

    public void setIs_dummy_data(boolean is_dummy_data) {
        this.is_dummy_data = is_dummy_data;
    }
}
