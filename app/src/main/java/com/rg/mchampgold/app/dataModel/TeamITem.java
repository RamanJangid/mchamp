package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TeamITem {
    @SerializedName("teams")
    private ArrayList<Team> teams;

    public ArrayList<Team> getTeams() {
        return teams == null?new ArrayList<>():teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    @SerializedName("user_teams")
    private int userTeams;

    @SerializedName("joined_leagues")
    private int joined_leagues;

    @Override
    public String toString(){
        return
                "PlayingHistoryItem{" +
                        "teams = '" + teams + '\'' +
                        "}";
    }

    public int getUserTeams() {
        return userTeams;
    }

    public void setUserTeams(int userTeams) {
        this.userTeams = userTeams;
    }

    public int getJoined_leagues() {
        return joined_leagues;
    }

    public void setJoined_leagues(int joined_leagues) {
        this.joined_leagues = joined_leagues;
    }
}
