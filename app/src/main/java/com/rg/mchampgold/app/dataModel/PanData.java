package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PanData{

	@SerializedName("pan_number")
	private String panNumber;

	@SerializedName("pan_holder_name")
	private String panHolderName;

	@SerializedName("pan_image")
	private String panImage;

	@SerializedName("pan_dob")
	private String panDob;

	public void setPanNumber(String panNumber){
		this.panNumber = panNumber;
	}

	public String getPanNumber(){
		return panNumber;
	}

	public void setPanHolderName(String panHolderName){
		this.panHolderName = panHolderName;
	}

	public String getPanHolderName(){
		return panHolderName;
	}

	public void setPanImage(String panImage){
		this.panImage = panImage;
	}

	public String getPanImage(){
		return panImage;
	}

	public void setPanDob(String panDob){
		this.panDob = panDob;
	}

	public String getPanDob(){
		return panDob;
	}

	@Override
 	public String toString(){
		return 
			"PanData{" + 
			"pan_number = '" + panNumber + '\'' + 
			",pan_holder_name = '" + panHolderName + '\'' + 
			",pan_image = '" + panImage + '\'' + 
			",pan_dob = '" + panDob + '\'' + 
			"}";
		}
}