package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class UsableBalanceItem {

	@SerializedName("usablebalance")
	private double usablebalance;

	@SerializedName("usertotalbalance")
	private double usertotalbalance;

	@SerializedName("marathon")
	private double marathon;


	@SerializedName("is_bonus")
	private int is_bonus;

	public double getUsablebalance() {
		return usablebalance;
	}

	public void setUsablebalance(double usablebalance) {
		this.usablebalance = usablebalance;
	}

	public double getUsertotalbalance() {
		return usertotalbalance;
	}

	public void setUsertotalbalance(double usertotalbalance) {
		this.usertotalbalance = usertotalbalance;
	}

	public double getMarathon() {
		return marathon;
	}

	public void setMarathon(double marathon) {
		this.marathon = marathon;
	}

	public int getIs_bonus() {
		return is_bonus;
	}

	public void setIs_bonus(int is_bonus) {
		this.is_bonus = is_bonus;
	}
}