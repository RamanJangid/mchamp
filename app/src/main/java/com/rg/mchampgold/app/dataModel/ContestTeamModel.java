package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class ContestTeamModel {

    @SerializedName("team_number")
    String team_number;

    @SerializedName("points")
    String points;

    @SerializedName("rank")
    String rank;

    @SerializedName("win_amount")
    String win_amount;

    public String getTeam_number() {
        return team_number;
    }

    public void setTeam_number(String team_number) {
        this.team_number = team_number;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getWin_amount() {
        return win_amount;
    }

    public void setWin_amount(String win_amount) {
        this.win_amount = win_amount;
    }
}
