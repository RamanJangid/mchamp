package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class AddFundItem {

	@SerializedName("msg")
	private String msg;

	@SerializedName("amount")
	private double amount;

	@SerializedName("status")
	private int status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setAmount(int amount){
		this.amount = amount;
	}

	public double getAmount(){
		return amount;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AddFundItem{" +
			"msg = '" + msg + '\'' + 
			",amount = '" + amount + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}