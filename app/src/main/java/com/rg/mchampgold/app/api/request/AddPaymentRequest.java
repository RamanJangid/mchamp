package com.rg.mchampgold.app.api.request;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class AddPaymentRequest {

	@SerializedName("amount")
	private String amount;

	@SerializedName("user_id")
	private String user_id;

	@SerializedName("paymentby")
	private String paymentby;

	@SerializedName("order_id")
	private String order_id;

	@SerializedName("offer_id")
	private String offer_id;

	@SerializedName("payment_id")
	private String payment_id;


	@SerializedName("promo_id")
	private String promo_id;

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getPaymentby() {
		return paymentby;
	}

	public void setPaymentby(String paymentby) {
		this.paymentby = paymentby;
	}

	public String getOrder_id() {
		return order_id;
	}

	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

	public String getOffer_id() {
		return offer_id;
	}

	public void setOffer_id(String offer_id) {
		this.offer_id = offer_id;
	}

	public String getPayment_id() {
		return payment_id;
	}

	public void setPayment_id(String payment_id) {
		this.payment_id = payment_id;
	}

	public String getPromo_id() {
		return promo_id;
	}

	public void setPromo_id(String promo_id) {
		this.promo_id = promo_id;
	}
}