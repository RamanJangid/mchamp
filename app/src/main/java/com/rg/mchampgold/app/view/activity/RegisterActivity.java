package com.rg.mchampgold.app.view.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.RegisterRequest;
import com.rg.mchampgold.app.api.request.SocialLoginRequest;
import com.rg.mchampgold.app.api.response.RegisterResponse;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.TextViewLinkHandler;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.common.utils.NetworkUtils;
import com.rg.mchampgold.databinding.ActivityRegisterBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import javax.inject.Inject;

import retrofit2.Response;


public class RegisterActivity extends AppCompatActivity {

    ActivityRegisterBinding mainBinding;

    @Inject
    OAuthRestService oAuthRestService;

    int passwordNotVisible = 0;
    int cnfPasswordNotVisible = 0;

    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1001;

    private CallbackManager callbackManager;
    String fcmToken = "";
    String deviceId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        // Utils.setFullScreen(this);
        MyApplication.getAppComponent().inject(RegisterActivity.this);
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        callbackManager = CallbackManager.Factory.create();
        fcmToken = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_FCM_TOKEN);
        initialize();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        if (getSupportActionBar() != null) {
            setSupportActionBar(mainBinding.mytoolbar);
           getSupportActionBar().setTitle("");
        }

    }

    private void initialize() {

        AppUtils.disableCopyPaste(mainBinding.etCnfPassword);
        AppUtils.disableCopyPaste(mainBinding.etMobileNo);
        AppUtils.disableCopyPaste(mainBinding.etPassword);
        AppUtils.disableCopyPaste(mainBinding.etDob);
        AppUtils.disableCopyPaste(mainBinding.etEmail);
        AppUtils.disableCopyPaste(mainBinding.etReferCode);

        setSupportActionBar(mainBinding.mytoolbar);
        if (getSupportActionBar() != null) {
           // getSupportActionBar().setTitle("OTP Verify");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        mainBinding.ivShowPassword.setOnClickListener(view -> {
            if (!mainBinding.etPassword.getText().toString().trim().equals("")) {
                if (passwordNotVisible == 0) {
                    mainBinding.etPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    mainBinding.ivShowPassword.setImageResource(R.drawable.ic_password_view);
                    passwordNotVisible = 1;
                } else {
                    mainBinding.ivShowPassword.setImageResource(R.drawable.view);
                    mainBinding.etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordNotVisible = 0;
                }
                mainBinding.etPassword.setSelection(mainBinding.etPassword.length());

            }
        });

        mainBinding.ivShowCnfPassword.setOnClickListener(view -> {
            if (!mainBinding.etCnfPassword.getText().toString().trim().equals("")) {
                if (cnfPasswordNotVisible == 0) {
                    mainBinding.etCnfPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    mainBinding.ivShowCnfPassword.setImageResource(R.drawable.ic_password_view);
                    cnfPasswordNotVisible = 1;
                } else {
                    mainBinding.ivShowCnfPassword.setImageResource(R.drawable.view);
                    mainBinding.etCnfPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    cnfPasswordNotVisible = 0;
                }
                mainBinding.etCnfPassword.setSelection(mainBinding.etCnfPassword.length());

            }
        });

        //mainBinding.termsCC.setText(mainBinding.termsCC.getText().toString()+ Html.fromHtml(getResources().getString(R.string.term_conditions_second)));
        /*mainBinding.btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(RegisterActivity.this, Arrays.asList("public_profile","email"));
            }
        });*/


//        mainBinding.tvTc.setText(Html.fromHtml("By registering, I agree to <u><b>T&C</b></u>"));
        mainBinding.tvTc.setText(Html.fromHtml("By registering, I agree to " + getString(R.string.app_names) + " <a href='" + MyApplication.terms_url + "'>T&amp;C </a> and <a href='" + MyApplication.privacy_url + "'>Privacy Policy</a>"));
//        mainBinding.tvTc.setOnClickListener(view -> startActivity(new Intent(MyApplication.appContext, WebActivity.class).putExtra("title", "Terms & Conditions").putExtra("type", MyApplication.terms_url)));
        mainBinding.tvDateOfBirth.setOnClickListener(view -> pickDate(mainBinding.etDob));
        mainBinding.etDob.setOnClickListener(view -> pickDate(mainBinding.etDob));
        mainBinding.tvTc.setMovementMethod(new TextViewLinkHandler() {
            @Override
            public void onLinkClick(String url) {
                if (url.equalsIgnoreCase(MyApplication.terms_url)){
                    AppUtils.openWebViewActivity(getString(R.string.terms_conditions), MyApplication.terms_url);
                }else {
                    AppUtils.openWebViewActivity(getString(R.string.privacy_policy), MyApplication.privacy_url);
                }
//                Toast.makeText(mainBinding.tvTc.getContext(), url, Toast.LENGTH_SHORT).show();
            }
        });

/*

        mainBinding.tvPp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyApplication.appContext, WebActivity.class).putExtra("title", "Privacy Policy").putExtra("type", "privacy.html"));
            }
        });
*/


        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        SocialLoginRequest socialLoginRequest = new SocialLoginRequest();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                (object, response) -> {
                                    Log.v("FBLoginActivity", response.toString());

                                    JSONObject json = response.getJSONObject();
                                    try {
                                        socialLoginRequest.setEmail(json.getString("email"));
                                        socialLoginRequest.setName(json.getString("name"));
                                        socialLoginRequest.setImage(Profile.getCurrentProfile().getProfilePictureUri(100, 100).toString());
                                        socialLoginRequest.setSocialLoginType("facebook");
                                        socialLoginRequest.setDeviceId(deviceId);
                                        socialLoginRequest.setFcmToken(fcmToken);
                                        loginUserWithSocial(socialLoginRequest);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(RegisterActivity.this, "Login Canceled.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(RegisterActivity.this, "Cannot connect facebook error.", Toast.LENGTH_SHORT).show();
                    }
                });

       /* mainBinding.btnGoogleLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });*/

        mainBinding.tvLoginNow.setOnClickListener(v -> finish());

        mainBinding.btnRegister.setOnClickListener(v -> validation());
    }

    // google
    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(this, task -> {
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }

    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            if (account != null) {
                SocialLoginRequest loginRequest = new SocialLoginRequest();
                loginRequest.setName(account.getDisplayName());
                loginRequest.setEmail(account.getEmail());
                loginRequest.setFcmToken(fcmToken);
                loginRequest.setDeviceId(deviceId);
                if (account.getPhotoUrl() != null)
                    loginRequest.setImage(account.getPhotoUrl().toString());
                loginRequest.setIdToken(account.getIdToken());
                loginRequest.setSocialLoginType("gmail");
                loginUserWithSocial(loginRequest);
            } else {
                Toast.makeText(RegisterActivity.this, "Sorry unable to fetch details", Toast.LENGTH_SHORT).show();
                return;
            }

            signOut();
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }


    private void validation() {
        String referCode = mainBinding.etReferCode.getText().toString().trim();
        String email = mainBinding.etEmail.getText().toString().trim();
        String phone = mainBinding.etMobileNo.getText().toString().trim();
        String password = mainBinding.etPassword.getText().toString().trim();
        String cnfPassword = mainBinding.etCnfPassword.getText().toString().trim();

        if (phone.isEmpty())
            showToast("Please enter valid mobile number.");
        else if (phone.length() < 10)
            showToast("Please enter 10 digits mobile number.");
        else if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            showToast("Please enter valid email address");
        else if (mainBinding.etDob.getText().toString().isEmpty())
            showToast("Please select date of birth.");
        else if (password.isEmpty())
            showToast("Please enter valid password.");
        else if (!AppUtils.isValidPassword(password)) {
            /*showToast("Password must contain minimum 8 Characters, at least 1 Lower Case, 1 Upper Case, 1 Number and 1 Special Character.");*/


            final Toast toast = Toast.makeText(getApplicationContext(), "Password must contain minimum 8 Characters, at least 1 Lower Case, 1 Upper Case, 1 Number and 1 Special Character.", Toast.LENGTH_LONG);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 10000);
        }
        else if (password.length() < 8)
            showToast("Password should be 8 to 16 char long");
        else if (cnfPassword.isEmpty())
            showToast("Please enter confirm password.");
        else if (cnfPassword.length() < 8)
            showToast("Password should be 8 to 16 char long");
        else if (!password.equals(cnfPassword))
            showToast("Password & Confirm password not matched");

//        else if (!mainBinding.termsCC.isChecked()) {
//            showToast("Please accept "+Constants.APP_NAME+" T&Cs");
//        }
        else {
            if (NetworkUtils.isNetworkAvailable()) {
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.setReferCode(referCode);
                registerRequest.setEmail(email);
                registerRequest.setMobile(phone);
                registerRequest.setPassword(password);
                registerRequest.setDob(mainBinding.etDob.getText().toString().trim());
                registerRequest.setFcmToken(fcmToken);
                registerRequest.setDeviceId(deviceId);
                registerUser(registerRequest);
            } else {
                Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void registerUser(RegisterRequest registerRequest) {
        mainBinding.setRefreshing(true);
//        CustomCallAdapter.CustomCall<RegisterResponse> userLogin = oAuthRestService.userRegister(registerRequest);
        CustomCallAdapter.CustomCall<RegisterResponse> userLogin = oAuthRestService.newUserRegister(registerRequest);
        userLogin.enqueue(new CustomCallAdapter.CustomCallback<RegisterResponse>() {
            @Override
            public void success(Response<RegisterResponse> response) {
                mainBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    RegisterResponse registerResponse = response.body();
                    if (registerResponse.getStatus() == 1) {
                        MyApplication.refer_show=registerResponse.getResult().getRefer_show();

                        MyApplication.tinyDB.putBoolean(Constants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_ID, registerResponse.getResult().getUserId() + "");
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_NAME, registerResponse.getResult().getUsername());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_MOBILE, registerResponse.getResult().getMobile());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_EMAIL, registerResponse.getResult().getEmail());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TOKEN, registerResponse.getResult().getCustomUserToken());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_REFER_CODE, registerResponse.getResult().getRefercode());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, registerResponse.getResult().getBankVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, registerResponse.getResult().getPanVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, registerResponse.getResult().getMobileVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, registerResponse.getResult().getEmailVerify());
                        MyApplication.tinyDB.putString(Constants.AUTHTOKEN, registerResponse.getResult().getCustomUserToken());
                        Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();

                        /*Intent intent = new Intent(RegisterActivity.this, OtpVerifyActivity.class);
                        intent.putExtra("MOBILE", mainBinding.etMobileNo.getText().toString().trim());
                        intent.putExtra("flag", "register");
                        intent.putExtra("userId", registerResponse.getResult().getUserId() + "");
                        startActivity(intent);
                        // finish();*/
                    } else {
                        Toast.makeText(RegisterActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mainBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });


    }

    private void loginUserWithSocial(SocialLoginRequest socialLoginRequest) {
        mainBinding.setRefreshing(true);
        CustomCallAdapter.CustomCall<RegisterResponse> userLogin = oAuthRestService.userLoginSocial(socialLoginRequest);
        userLogin.enqueue(new CustomCallAdapter.CustomCallback<RegisterResponse>() {
            @Override
            public void success(Response<RegisterResponse> response) {
                mainBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    RegisterResponse registerResponse = response.body();
                    if (registerResponse.getStatus() == 1) {
                        mainBinding.etEmail.setText(registerResponse.getResult().getEmail());
                        MyApplication.tinyDB.putBoolean(Constants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_ID, registerResponse.getResult().getUserId() + "");
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_NAME, registerResponse.getResult().getUsername());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_MOBILE, registerResponse.getResult().getMobile());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_EMAIL, registerResponse.getResult().getEmail());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TOKEN, registerResponse.getResult().getCustomUserToken());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_REFER_CODE, registerResponse.getResult().getRefercode());
                        MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_TEAM_NAME, registerResponse.getResult().getTeamName());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, registerResponse.getResult().getBankVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, registerResponse.getResult().getPanVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, registerResponse.getResult().getMobileVerify());
                        MyApplication.tinyDB.putInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, registerResponse.getResult().getEmailVerify());
                        MyApplication.refer_show=registerResponse.getResult().getRefer_show();
                        startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
                        if (socialLoginRequest.getSocialLoginType().equalsIgnoreCase("facebook"))
                            LoginManager.getInstance().logOut();
                        finish();
                    } else {
                        Toast.makeText(RegisterActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                mainBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    public void pickDate(final TextView dialog) {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(RegisterActivity.this, (datepicker, selectedyear, selectedmonth, selectedday) -> {
            String dayOfMonth=String.valueOf(selectedday).length()==1?"0"+selectedday: String.valueOf(selectedday);
            String month=String.valueOf(selectedmonth + 1).length()==1?"0"+(selectedmonth + 1): String.valueOf((selectedmonth + 1));
            String d =  dayOfMonth+ "/" + month + "/" + selectedyear;
            //            String d = (selectedmonth + 1) + "/" + selectedday + "/" + selectedyear;
            dialog.setText(d);
        }, mYear, mMonth, mDay);

        mDatePicker.getDatePicker().setMaxDate((long) (System.currentTimeMillis() - (5.681e+11)));
        mDatePicker.setTitle("Select Birth Date");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDatePicker.getDatePicker().setFirstDayOfWeek(Calendar.MONDAY);
        }


        mDatePicker.show();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home)
        {
           finish();
        }

        return super.onOptionsItemSelected(item);
    }
}