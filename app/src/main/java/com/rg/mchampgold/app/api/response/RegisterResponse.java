package com.rg.mchampgold.app.api.response;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class RegisterResponse{

	@SerializedName("otp")
	private int otp;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;


	@SerializedName("result")
	private Result result;

	public void setResult(Result result){
		this.result = result;
	}

	public Result getResult(){
		return result == null?new Result():result;
	}

	public void setOtp(int otp){
		this.otp = otp;
	}

	public int getOtp(){
		return otp;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}



	@Override
 	public String toString(){
		return 
			"CheckVersionCodeResponse{" +
			"otp = '" + otp + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}



}