package com.rg.mchampgold.app.api.response;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Result{

	@SerializedName("pincode")
	private String pincode;

	@SerializedName("address")
	private String address;

	@SerializedName("device_id")
	private String deviceId;

	@SerializedName("gender")
	private String gender;

	@SerializedName("city")
	private String city;

	@SerializedName("jwt_token")
	private String customUserToken;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("bank_verify")
	private int bankVerify;

	@SerializedName("mobile_verify")
	private int mobileVerify;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("dob")
	private String dob;

	@SerializedName("refercode")
	private String refercode;

	@SerializedName("email_verify")
	private int emailVerify;

	@SerializedName("pan_verify")
	private int panVerify;

	@SerializedName("fcmToken")
	private String fcmToken;

	@SerializedName("email")
	private String email;

	@SerializedName("username")
	private String username;

	@SerializedName("team")
	private String teamName;

	@SerializedName("refer_show")
	private String refer_show;

	public void setPincode(String pincode){
		this.pincode = pincode;
	}

	public String getPincode(){
		return pincode;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}

	public String getDeviceId(){
		return deviceId;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setCustomUserToken(String customUserToken){
		this.customUserToken = customUserToken;
	}

	public String getCustomUserToken(){
		return customUserToken;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}


	public void setBankVerify(int bankVerify){
		this.bankVerify = bankVerify;
	}

	public int getBankVerify(){
		return bankVerify;
	}

	public void setMobileVerify(int mobileVerify){
		this.mobileVerify = mobileVerify;
	}

	public int getMobileVerify(){
		return mobileVerify;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setRefercode(String refercode){
		this.refercode = refercode;
	}

	public String getRefercode(){
		return refercode;
	}

	public void setEmailVerify(int emailVerify){
		this.emailVerify = emailVerify;
	}

	public int getEmailVerify(){
		return emailVerify;
	}

	public void setPanVerify(int panVerify){
		this.panVerify = panVerify;
	}

	public int getPanVerify(){
		return panVerify;
	}

	public void setFcmToken(String fcmToken){
		this.fcmToken = fcmToken;
	}

	public String getFcmToken(){
		return fcmToken;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
	public String toString(){
		return
				"UserData{" +
						"pincode = '" + pincode + '\'' +
						",address = '" + address + '\'' +
						",device_id = '" + deviceId + '\'' +
						",gender = '" + gender + '\'' +
						",city = '" + city + '\'' +
						",custom_user_token = '" + customUserToken + '\'' +
						",mobile = '" + mobile + '\'' +
						",bank_verify = '" + bankVerify + '\'' +
						",mobile_verify = '" + mobileVerify + '\'' +
						",user_id = '" + userId + '\'' +
						",dob = '" + dob + '\'' +
						",refercode = '" + refercode + '\'' +
						",email_verify = '" + emailVerify + '\'' +
						",pan_verify = '" + panVerify + '\'' +
						",fcmToken = '" + fcmToken + '\'' +
						",email = '" + email + '\'' +
						",username = '" + username + '\'' +
						"}";
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}



	@SerializedName("user_profile_image")
	private String userProfileImage;

	public String getUserProfileImage() {
		return userProfileImage;
	}

	public void setUserProfileImage(String userProfileImage) {
		this.userProfileImage = userProfileImage;
	}

	public String getRefer_show() {
		return refer_show;
	}

	public void setRefer_show(String refer_show) {
		this.refer_show = refer_show;
	}
}