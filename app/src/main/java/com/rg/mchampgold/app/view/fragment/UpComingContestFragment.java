package com.rg.mchampgold.app.view.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.CategoriesItem;
import com.rg.mchampgold.app.dataModel.CategoryByContestResponse;

import com.rg.mchampgold.app.view.activity.UpComingContestActivity;
import com.rg.mchampgold.app.view.adapter.CategoryContestItemAdapter;
import com.rg.mchampgold.app.viewModel.ContestViewModel;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentUpcommingContestBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;


public class UpComingContestFragment extends Fragment {

    FragmentUpcommingContestBinding mBinding;
    String matchKey;
    private ContestViewModel contestViewModel;
    ArrayList<CategoriesItem> list = new ArrayList<>();
    CategoryContestItemAdapter mAdapter;
    private int teamCount = 0;
    private int joinedContestCount = 0;
    private int totalContest = 0;
	String sportKey="";
    LinearLayoutManager mLayoutManager;

    @Inject
    OAuthRestService oAuthRestService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_upcomming_contest, container, false);
        setupRecyclerView();
        mBinding.btnCreateTeam.setVisibility(View.GONE);
        mBinding.joinContestByCode.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).joinByContestCode());
        mBinding.btnCreateTeam.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).creteTeam());
        mBinding.btbCreatePrivateContest.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).openPrivateCreateContest(teamCount));
        mBinding.tvAllContest.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).openAllContestActivity(111));
        mBinding.rlAllContests.setOnClickListener(view -> ((UpComingContestActivity) getActivity()).openAllContestActivity(111));
        mBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            getContestByCategory();
            mBinding.swipeRefreshLayout.setRefreshing(false);
        });


        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                // TODO Auto-generated method stub
                //super.onScrollStateChanged(recyclerView, newState);
                try {
                    int firstPos = mLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        mBinding.swipeRefreshLayout.setEnabled(false);
                    } else {
                        mBinding.swipeRefreshLayout.setEnabled(true);
                        if (mBinding.recyclerView.getScrollState() == 1)
                            if (mBinding.swipeRefreshLayout.isRefreshing())
                                mBinding.recyclerView.stopScroll();
                    }

                } catch (Exception e) {
                   // Log.e("", "Scroll Error : " + e.getLocalizedMessage());
                }
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        contestViewModel = ContestViewModel.create(this);
        MyApplication.getAppComponent().inject(contestViewModel);
        MyApplication.getAppComponent().inject(this);
        if (getArguments() != null) {
            matchKey = getArguments().getString(Constants.KEY_MATCH_KEY);
            sportKey = getArguments().getString(Constants.SPORT_KEY);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getContestByCategory();
    }

    private void setupRecyclerView() {
        mAdapter = new CategoryContestItemAdapter(getActivity(), list, (UpComingContestActivity) getActivity());
        mBinding.recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        //    mBinding.recyclerView.addItemDecoration(new HeaderItemDecoration(mBinding.recyclerView, mAdapter.stickyHeaderInterface));
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
    }


    private void getContestByCategory() {
        mBinding.setRefreshing(true);
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        CustomCallAdapter.CustomCall<CategoryByContestResponse> myBalanceResponseCustomCall = oAuthRestService.getContestByCategory(request);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<CategoryByContestResponse>() {
            @SuppressLint("RestrictedApi")
            @Override
            public void success(Response<CategoryByContestResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    CategoryByContestResponse categoryByContestResponse = response.body();
                    if (categoryByContestResponse.getStatus() == 1 && categoryByContestResponse.getResult() != null) {
                        if (categoryByContestResponse.getResult().getCategories().size() > 0) {
                            list = categoryByContestResponse.getResult().getCategories();
                            mAdapter.updateData(list);
                            totalContest = categoryByContestResponse.getResult().getTotalContest();
                            teamCount = categoryByContestResponse.getResult().getUserTeamCount();
                            joinedContestCount = categoryByContestResponse.getResult().getJoinedContestCount();
                            mBinding.tvAllContest.setText("View all " + totalContest + " Contests");
                            if (getActivity() != null && getActivity() instanceof UpComingContestActivity) {
                                ((UpComingContestActivity) getActivity()).setTabTitle(teamCount, joinedContestCount);
                                mBinding.tvAllContestCount.setText("All Contests (" + totalContest + ")");
                            }
                            setTeamCreateButtonName();
                        } else {
                            /// AppUtils.showErrorr((HomeActivity) getActivity(), "Not found any record");
                        }
                    } else {
                        // AppUtils.showErrorr(getActivity(), categoryByContestResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void setTeamCreateButtonName() {
       /* if (teamCount > 0) {
            UpComingContestActivity.fab_create_team.setVisibility(View.GONE);
        }*/

        mBinding.btnCreateTeam.setText("Create Team " + (teamCount + 1));
        if (teamCount > 11)
            mBinding.btnCreateTeam.setVisibility(View.GONE);
        else
            mBinding.btnCreateTeam.setVisibility(View.VISIBLE);
    }

/*
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisible()){
            if(isVisibleToUser){
                getContestByCategory();
            }
        }
    }*/

}