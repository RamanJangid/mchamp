package com.rg.mchampgold.app.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.GetWinnerScoreCardResponse;
import com.rg.mchampgold.app.dataModel.LiveFinishedContestData;
import com.rg.mchampgold.app.dataModel.RefreshScoreResponse;
import com.rg.mchampgold.app.dataModel.WinnerScoreCardItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.LiveFinishedContestItemAdapter;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.app.viewModel.ContestViewModel;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityLiveFinishedContestBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class LiveFinishedContestActivity extends AppCompatActivity implements OnContestItemClickListener {
    ActivityLiveFinishedContestBinding mBinding;
    LiveFinishedContestItemAdapter mAdapter;
    private ContestViewModel contestViewModel;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    ArrayList<LiveFinishedContestData> list = new ArrayList<>();
    String headerText;
    Context context;
    Menu menuTemp;
    boolean isFromFinished = false;
    String sportKey="";
    @Inject
    OAuthRestService oAuthRestService;
    private int seriesId;
    private int isLeaderboard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        contestViewModel = ContestViewModel.create(LiveFinishedContestActivity.this);
        MyApplication.getAppComponent().inject(contestViewModel);
        MyApplication.getAppComponent().inject(LiveFinishedContestActivity.this);
        context = LiveFinishedContestActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_live_finished_contest);
        mBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            getData();
        });
        mBinding.matchHeaderInfo.fps.setVisibility(View.VISIBLE);
        mBinding.matchHeaderInfo.fps.setOnClickListener(v -> AppUtils.openWebViewActivity(getString(R.string.fantasy_point_system), MyApplication.fantasy_point_url));
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.contest));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            headerText  = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
            isFromFinished = getIntent().getBooleanExtra("is_from_finished", false);
            seriesId = getIntent().getExtras().getInt(Constants.KEY_SERIES_ID,0);
            isLeaderboard = getIntent().getExtras().getInt(Constants.KEY_IS_LEADERBOARD, 0);
        }

//        if (isFromFinished) mBinding.cvUserWinnings.setVisibility(View.VISIBLE);

        if(headerText.equalsIgnoreCase("Winner Declared")) {
            mBinding.matchHeaderInfo.tvMatchTimer.setText("Winner Declared");
        }
        else if(headerText.equalsIgnoreCase("In Progress")) {
            mBinding.matchHeaderInfo.tvMatchTimer.setText("In Progress");
        }
        else {
            mBinding.matchHeaderInfo.tvMatchTimer.setText(headerText);
        }

        String teams[] = teamVsName.split(" ");
        mBinding.matchHeaderInfo.tvTeam1.setText(teams[0]);
        mBinding.matchHeaderInfo.tvTeam2.setText(teams[2]);

        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam1,teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam2,teamSecondUrl);


        setupRecyclerView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (menuTemp!=null){
            AppUtils.setWalletBalance(menuTemp,getResources().getColor(R.color.colorYellow), false, isLeaderboard, seriesId);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wallet,menu);
        menuTemp=menu;
        AppUtils.setWalletBalance(menu,getResources().getColor(R.color.colorYellow), false, isLeaderboard, seriesId);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;

           /* case R.id.navigation_wallet:
                openWalletActivity();
                return true;*/
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openNotificationActivity() {
        startActivity(new Intent(LiveFinishedContestActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(LiveFinishedContestActivity.this,MyWalletActivity.class));

    }




    private void setupRecyclerView() {
        mAdapter = new LiveFinishedContestItemAdapter(context,list,this, isFromFinished);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
        getData();

        mBinding.topLayout.setOnClickListener(view -> {
            Intent intent = new Intent(LiveFinishedContestActivity.this, PlayerPointsActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY,matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS,teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL,teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL,teamSecondUrl);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT,headerText);
			intent.putExtra(Constants.SPORT_KEY,sportKey);
            startActivity(intent);
        });
    }


    private void getData() {
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        contestViewModel.loadRefreshScore(request);
        contestViewModel.getRefreshScore().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    mBinding.swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    mBinding.swipeRefreshLayout.setRefreshing(false);
                   RefreshScoreResponse scoreResponse =  arrayListResource.getData();
                    if (scoreResponse.getStatus() == 1) {
                        if(scoreResponse.getRefreshScoreItem().getContest().size()>0) {
                            list = scoreResponse.getRefreshScoreItem().getContest();
                            mAdapter.updateData(list);
                            mBinding.noChallengeJoined.setVisibility(View.GONE);
                        }
                        else {
                            mBinding.noChallengeJoined.setVisibility(View.VISIBLE);
                        }

                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }


    @Override
    public void onContestClick(Contest contest, boolean isForDetail) {
        if(isForDetail) {
            MyApplication.isFromLiveFinished=true;
            Intent intent = new Intent(LiveFinishedContestActivity.this, UpComingContestDetailActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra( Constants.KEY_STATUS_HEADER_TEXT,headerText);
            intent.putExtra( Constants.KEY_STATUS_IS_TIMER_HEADER,false);
            intent.putExtra( Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS,false);
            intent.putExtra(Constants.SPORT_KEY,sportKey);
            intent.putExtra(Constants.KEY_SERIES_ID, seriesId);
            intent.putExtra(Constants.KEY_IS_LEADERBOARD, isLeaderboard);
            startActivity(intent);
        }
        else {
            Intent intent = new Intent(LiveFinishedContestActivity.this,MyTeamsActivity.class);
            intent.putExtra( Constants.KEY_MATCH_KEY,matchKey);
            intent.putExtra( Constants.KEY_TEAM_VS,teamVsName);
            intent.putExtra( Constants.KEY_TEAM_FIRST_URL,teamFirstUrl);
            intent.putExtra( Constants.KEY_TEAM_SECOND_URL,teamSecondUrl);
            intent.putExtra( Constants.KEY_IS_FOR_JOIN_CONTEST,true);
            intent.putExtra( Constants.KEY_CONTEST_DATA,contest);
            intent.putExtra( Constants.KEY_STATUS_HEADER_TEXT,headerText);
            intent.putExtra( Constants.KEY_STATUS_IS_TIMER_HEADER,true);
            intent.putExtra(Constants.SPORT_KEY,sportKey);
            startActivity(intent);
        }
    }

    public void getWinnerPriceCard(String contestId,String amount) {
        mBinding.setRefreshing(true);
        ContestRequest contestRequest = new ContestRequest();
        contestRequest.setMatchKey(matchKey);
        contestRequest.setLeagueId(contestId);
        contestRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<GetWinnerScoreCardResponse> bankDetailResponseCustomCall = oAuthRestService.getWinnersPriceCard(contestRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<GetWinnerScoreCardResponse>() {
            @Override
            public void success(Response<GetWinnerScoreCardResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    GetWinnerScoreCardResponse  getWinnerScoreCardResponse = response.body();
                    if(getWinnerScoreCardResponse.getStatus() ==1 && getWinnerScoreCardResponse.getResult().size()>0) {
                        ArrayList<WinnerScoreCardItem> priceList = getWinnerScoreCardResponse.getResult();
                        if (priceList.size() > 0) {
                          //  AppUtils.showWinningPopup(LiveFinishedContestActivity.this,priceList,""+amount);
                        }
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }
}
