package com.rg.mchampgold.app.view.basketball;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.dataModel.Limit;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.BasketBallSelectedPlayer;
import com.rg.mchampgold.app.view.activity.ChooseCandVCActivity;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.app.view.activity.PlayerInfoActivity;
import com.rg.mchampgold.app.view.adapter.SelectedUnSelectedPlayerAdapter;
import com.rg.mchampgold.app.view.fragment.CreateTeamPlayerFragment;
import com.rg.mchampgold.app.view.fragment.PlayingStatusSheetFragment;
import com.rg.mchampgold.app.viewModel.GetPlayerDataViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityCreateTeamBinding;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BasketBallCreateTeamActivity extends AppCompatActivity {
    ActivityCreateTeamBinding mBinding;
    SelectedUnSelectedPlayerAdapter mSelectedUnSelectedPlayerAdapter;
    private GetPlayerDataViewModel createTeamViewModel;
    String teamName;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;

    ArrayList<Player> pgList = new ArrayList<>();
    ArrayList<Player> sgList = new ArrayList<>();
    ArrayList<Player> sfList = new ArrayList<>();
    ArrayList<Player> pfList = new ArrayList<>();
    ArrayList<Player> cList = new ArrayList<>();

    private static int PG = 1;
    private static int SG = 2;
    private static int SF = 3;
    private static int PF = 4;
    private static int C = 5;

    ArrayList<Player> allPlayerList = new ArrayList<>();
    BasketBallSelectedPlayer selectedPlayer;
    boolean exeedCredit = false;
    int selectedType = PG;

    public static Activity createTeamAc;
    ArrayList<Player> selectedList = new ArrayList<>();
    int teamId;
    Context context;
    boolean isFromEditOrClone;
    String headerText;
    boolean isShowTimer;

    int fantasyType;
    int totalPlayerCount;
    int maxTeamPlayerCount;
    double totalCredit;
    Limit limit;
    String sport_key="BASKETBALL";
    int counterValue=0;


    public boolean isPointSorted = true, isCreditSorted = true, isPLayerSorted = true;
    public String playerStatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        createTeamViewModel = GetPlayerDataViewModel.create(BasketBallCreateTeamActivity.this);
        MyApplication.getAppComponent().inject(createTeamViewModel);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_team);
        context = BasketBallCreateTeamActivity.this;
        initialize();
        createTeamAc = this;
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setTitle(getString(R.string.create_team));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if(getIntent()!=null && getIntent().getExtras()!=null) {
            if(getIntent().getExtras().getBoolean("isFromEditOrClone")) {
                isFromEditOrClone = getIntent().getExtras().getBoolean("isFromEditOrClone");
                selectedList = (ArrayList<Player>) getIntent().getSerializableExtra("selectedList");
                teamId = getIntent().getExtras().getInt(Constants.KEY_TEAM_ID);
            }
            teamName = getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            matchKey=  getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName=  getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl=  getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl=  getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            fantasyType=  getIntent().getExtras().getInt(Constants.KEY_FANTASY_TYPE_STATUS);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT,"");
            sport_key = getIntent().getExtras().getString(Constants.SPORT_KEY,"BASKETBALL");
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER,false);
        }

        setTeamNames();

        String teams[] = teamVsName.split(" ");
        mBinding.tvTeam1.setText(teams[0]);
        mBinding.tvTeam2.setText(teams[2]);
        AppUtils.loadImageMatch(mBinding.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.ivTeam2, teamSecondUrl);
//        mBinding.matchHeaderInfo.tvTeamVs.setText(teamVsName);
//        mBinding.ivTeamFirst.setImageURI(teamFirstUrl);
//        mBinding.ivTeamSecond.setImageURI(teamSecondUrl);

        if(isShowTimer) {
            showTimer();
        }
        else {
            if(headerText.equalsIgnoreCase("Winner Declared")) {
//                mBinding.matchHeaderInfo.tvTimeTimer.setText("Winner Declared");
//                mBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#f70073"));
            }
            else if(headerText.equalsIgnoreCase("In Progress")) {
//                mBinding.matchHeaderInfo.tvTimeTimer.setText("In Progress");
//                mBinding.matchHeaderInfo.tvTimeTimer.setTextColor(Color.parseColor("#16ae28"));
            }
        }

        mBinding.tvPlayerCountPick.setText("Pick 1-4 Point-Guard");


        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                FragmentManager fm = getSupportFragmentManager();
                switch (position) {
                    case 0:
                        selectedType = PG;
                        mBinding.tabLayout.getTabAt(0).setText(Constants.PG + (selectedPlayer.getPg_selected() == 0?"":"(" + selectedPlayer.getPf_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 1-4 Point-Guard");
                        if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                            ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(pgList, PG);
                        break;
                    case 1:
                        selectedType = SG;
                        mBinding.tabLayout.getTabAt(1).setText(Constants.SG + (selectedPlayer.getSg_selected() == 0?"":"(" + selectedPlayer.getSg_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 1-4 Shooting-Guard");
                        if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                            ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(sgList, SG);
                        break;
                    case 2:
                        selectedType = SF;
                        mBinding.tabLayout.getTabAt(2).setText(Constants.SF  + (selectedPlayer.getSf_selected() == 0?"":"(" + selectedPlayer.getSf_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 1-4 Small-Forward");
                        if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                            ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(sfList, SF);
                        break;
                    case 3:
                        selectedType = PF;
                        mBinding.tabLayout.getTabAt(3).setText(Constants.PF + (selectedPlayer.getPf_selected() == 0?"":"(" + selectedPlayer.getPf_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 1-4 Power-Forward");
                        if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                            ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(pfList, PF);
                        break;
                    case 4:
                        selectedType = C;
                        mBinding.tabLayout.getTabAt(4).setText(Constants.C + (selectedPlayer.getC_selected() == 0?"":"(" + selectedPlayer.getC_selected() + ")"));
                        mBinding.tvPlayerCountPick.setText("Pick 1-4 Centre");
                        if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                            ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(cList, C);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mSelectedUnSelectedPlayerAdapter = new SelectedUnSelectedPlayerAdapter(0,totalPlayerCount,getApplicationContext());
        createTeamData();
        mBinding.rvSelected.setAdapter(mSelectedUnSelectedPlayerAdapter);
        setupRecyclerView();


        mBinding.btnCreateTeam.setOnClickListener(v -> {

            if(selectedPlayer.getSelectedPlayer() == totalPlayerCount){
                ArrayList<Player> sellectedList = new ArrayList<>();

                for (Player player : pgList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }

                for (Player player : sgList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }

                for (Player player : sfList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }

                for (Player player : pfList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }

                for (Player player : cList) {
                    if (player.isIsSelected())
                        sellectedList.add(player);
                }





                Intent intent = new Intent(BasketBallCreateTeamActivity.this, ChooseCandVCActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                intent.putExtra("playerList",sellectedList);
                intent.putExtra(Constants.KEY_TEAM_ID,teamId);
                intent.putExtra( Constants.KEY_STATUS_HEADER_TEXT,headerText);
                intent.putExtra( Constants.KEY_STATUS_IS_TIMER_HEADER,isShowTimer);
                intent.putExtra(Constants.SPORT_KEY,sport_key);
				intent.putExtra("localTeamCount", String.valueOf(selectedPlayer.getLocalTeamPlayerCount()));
                intent.putExtra("visitorTeamCount", String.valueOf(selectedPlayer.getVisitorTeamPlayerCount()));

                if(isFromEditOrClone)
                    intent.putExtra("isFromEditOrClone",true);
                else
                    intent.putExtra("isFromEditOrClone",false);

                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);

                startActivityForResult(intent,101);

                //startActivity(intent);
            }
            else {
                showToast("Please select "+totalPlayerCount+" players");
            }

        });

        mBinding.ivTeamPreview.setOnClickListener(view -> {
            Intent intent = new Intent(BasketBallCreateTeamActivity.this, BasketBallTeamPreviewActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS,  teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_TEAM_NAME, teamName);

            ArrayList<Player> selectedPGList = new ArrayList<>();
            ArrayList<Player> selectedSGLiSt = new ArrayList<>();
            ArrayList<Player> selectedSFList = new ArrayList<>();
            ArrayList<Player> selectedPFlList = new ArrayList<>();
            ArrayList<Player> selectedCList = new ArrayList<>();
            for (Player player : pgList) {
                if (player.isIsSelected())
                    selectedPGList.add(player);
            }

            for (Player player : sgList) {
                if (player.isIsSelected())
                    selectedSGLiSt.add(player);
            }
            for (Player player : sfList) {
                if (player.isIsSelected())
                    selectedSFList.add(player);
            }

            for (Player player : pfList) {
                if (player.isIsSelected())
                    selectedPFlList.add(player);
            }

            for (Player player : cList) {
                if (player.isIsSelected())
                    selectedCList.add(player);
            }

            intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedPGList);
            intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedSGLiSt);
            intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedSFList);
            intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedPFlList);
            intent.putExtra(Constants.KEY_TEAM_LIST_C, selectedCList);

            startActivity(intent);
        });

        mBinding.ivInfoSelection.setOnClickListener(view -> {
            showSelectionPopupDialog();
        });

        mBinding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (mBinding.tabLayout.getSelectedTabPosition() == 0) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-4 Point-Guard");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 1) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-4 Shooting-Guard");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 2) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-4 Small-Forward");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 3) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-4 Power-Forward");
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 4) {
                    mBinding.tvPlayerCountPick.setText("Pick 1-4 Centre");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mBinding.tvPoints.setOnClickListener(view -> {

            FragmentManager fm = getSupportFragmentManager();
            if (fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment) {
                BasketballCreateTeamPlayerFragment basketballCreateTeamPlayerFragment =
                        ((BasketballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()));

                if (isPointSorted) {
                    sortWithPoints(false);
                    basketballCreateTeamPlayerFragment.sortWithPoints(false);
                    isPointSorted = false;
                    mBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPointSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    sortWithPoints(true);
                    basketballCreateTeamPlayerFragment.sortWithPoints(true);
                    isPointSorted = true;
                    mBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPointSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                isCreditSorted = true;
                isPLayerSorted = true;
            }
        });

        mBinding.tvCredits.setOnClickListener(view -> {

            FragmentManager fm = getSupportFragmentManager();

            if (fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment) {

                BasketballCreateTeamPlayerFragment basketballCreateTeamPlayerFragment =
                        ((BasketballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()));

                if (isCreditSorted) {
                    sortWithCredit(false);
                    basketballCreateTeamPlayerFragment.sortWithCredit(false);
                    isCreditSorted = false;
                    mBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivCreditSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    isCreditSorted = true;
                    sortWithCredit(true);
                    basketballCreateTeamPlayerFragment.sortWithCredit(true);
                    mBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivCreditSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                isPointSorted = true;
                isPLayerSorted = true;
            }
        });

        mBinding.llPlayer.setOnClickListener(view -> {

            FragmentManager fm = getSupportFragmentManager();

            if (fm.getFragments().get(0) instanceof CreateTeamPlayerFragment) {

                BasketballCreateTeamPlayerFragment basketballCreateTeamPlayerFragment =
                        ((BasketballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()));
                if (isPLayerSorted) {
                    sortByPlayer(false);
                    basketballCreateTeamPlayerFragment.sortByPlayer(false);
                    isPLayerSorted = false;
                    mBinding.ivPlayerSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPlayerSortImage.setImageResource(R.drawable.ic_down_sort);
                } else {
                    isPLayerSorted = true;
                    sortByPlayer(true);
                    basketballCreateTeamPlayerFragment.sortByPlayer(true);
                    mBinding.ivPlayerSortImage.setVisibility(View.VISIBLE);
                    mBinding.ivPlayerSortImage.setImageResource(R.drawable.ic_up_sort);
                }
                mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                isPointSorted = true;
                isCreditSorted = true;
            }
        });

        mBinding.llPlayingStatus.setOnClickListener(view -> {
            showPlayingStatusDialog();
        });

    }

    private void sortWithPoints(boolean flag) {
        if (flag) {
            Collections.sort(pgList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(sgList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(sfList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(pfList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
            Collections.sort(cList, (contest, t1) -> Double.compare(contest.getSeriesPoints(), t1.getSeriesPoints()));
        } else {
            Collections.sort(pgList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(sgList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(sfList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(pfList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
            Collections.sort(cList, (contest, t1) -> Double.compare(t1.getSeriesPoints(), contest.getSeriesPoints()));
        }
    }


    private void sortWithCredit(boolean flag) {
        if (flag) {
            Collections.sort(pgList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(sgList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(sfList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(pfList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
            Collections.sort(cList, (contest, t1) -> Double.compare(contest.getCredit(), t1.getCredit()));
        } else {
            Collections.sort(pgList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(sgList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(sfList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(pfList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
            Collections.sort(cList, (contest, t1) -> Double.compare(t1.getCredit(), contest.getCredit()));
        }
    }

    private void sortByPlayer(boolean flag) {
        if (flag) {
            Collections.sort(pgList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(sgList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(sfList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(pfList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
            Collections.sort(cList, (contest, t1) -> contest.getName().compareTo(t1.getName()));
        } else {
            Collections.sort(pgList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(sgList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(sfList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(pfList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
            Collections.sort(cList, (contest, t1) -> t1.getName().compareTo(contest.getName()));
        }
    }

    private void showPlayingStatusDialog() {
        PlayingStatusSheetFragment playingStatusSheetFragment = new PlayingStatusSheetFragment(context);
        playingStatusSheetFragment.show(getSupportFragmentManager(), playingStatusSheetFragment.getTag());
    }

    public void changePlayerStatus(String playNotPlayText) {
        playerStatus = playNotPlayText;
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition()) instanceof BasketballCreateTeamPlayerFragment)
            ((BasketballCreateTeamPlayerFragment) fm.getFragments().get(mBinding.tabLayout.getSelectedTabPosition())).changePlayingStatus();
    }

    private void showSelectionPopupDialog() {
        Dialog dialog = new Dialog(BasketBallCreateTeamActivity.this);
        dialog.setContentView(R.layout.basketball_selection_rules_popup);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.findViewById(R.id.tv_got_it).setOnClickListener(view1 -> dialog.dismiss());
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_team,menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        MenuItem clearTeam = menu.findItem(R.id.clear_team);
        clearTeam.setVisible(true);
        LinearLayout rootView = (LinearLayout) clearTeam.getActionView();
        rootView.setOnClickListener(view -> {
            if (selectedPlayer.getSelectedPlayer() > 0) {

                Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.layout_clear_popup);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

                final TextView tvCancelTeam = dialog.findViewById(R.id.tv_cancel_team);
                final TextView tvClearTeam = dialog.findViewById(R.id.tv_clear_team);

                tvCancelTeam.setOnClickListener(view1 -> dialog.dismiss());

                tvClearTeam.setOnClickListener(view12 -> {

                    createTeamData();

                    for (int i = 0; i < allPlayerList.size(); i++) {
                        allPlayerList.get(i).setSelected(false);
                    }

                    mBinding.tvLocalTeam.setText("0");
                    mBinding.tvVisitorTeam.setText("0");
                    mBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
                    mBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
                    mBinding.ivPlayerSortImage.setVisibility(View.INVISIBLE);
                    isCreditSorted = true;
                    isPLayerSorted = true;
                    isPointSorted = true;
                    setupViewPager(mBinding.viewPager);
                    dialog.dismiss();
                });

                dialog.show();
            } else {
                AppUtils.showErrorr(this, "No player selected to clear.");
            }

        });


        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();/*   case R.id.navigation_notification:
                openNotificationActivity();
                return true;*//*case R.id.navigation_wallet:
                openWalletActivity();
                return true;
*/
        if (itemId == R.id.how_to_play) {
            AppUtils.openWebViewActivity(getString(R.string.how_to_play), MyApplication.how_to_play_url);
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openNotificationActivity() {
        startActivity(new Intent(BasketBallCreateTeamActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(BasketBallCreateTeamActivity.this, MyWalletActivity.class));

    }

    private void setupRecyclerView() {
        getData();
    }


    private void getData() {
        MyTeamRequest request = new MyTeamRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sport_key);
        createTeamViewModel.loadPlayerListRequest(request);
        createTeamViewModel.getPlayerList().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext,arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if(arrayListResource.getData().getStatus()==1 && arrayListResource.getData().getResult().size()>0) {
                        allPlayerList = arrayListResource.getData().getResult();
                        limit = arrayListResource.getData().getLimit();

                        totalCredit = limit.getTotalCredits();
                        totalPlayerCount = limit.getTotalPlayers();
                        mBinding.totalPlayers.setText("/"+totalPlayerCount);
                        maxTeamPlayerCount = limit.getTeamMaxPlayer();
                        mBinding.tvMsgError.setText("Max "+maxTeamPlayerCount+" Players from a team");
                        setData();
                        for (Player player : allPlayerList) {
                            if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PG))
                                pgList.add(player);
                            else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SG))
                                sgList.add(player);
                            else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_SF))
                                sfList.add(player);
                            else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PF))
                                pfList.add(player);
                            else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_C))
                                cList.add(player);
                        }

                        if(selectedList.size()>0) {
                            for (int i = 0; i < allPlayerList.size(); i++) {
                                for (Player player : selectedList) {
                                    if (player.getId()==allPlayerList.get(i).getId()) {
                                        allPlayerList.get(i).setSelected(true);
                                        if(player.getCaptain() ==1)
                                            allPlayerList.get(i).setCaptain(true);
                                        if(player.getVicecaptain() ==1)
                                            allPlayerList.get(i).setVcCaptain(true);
                                    }
                                }
                            }
                            setSelectedCountForEditOrClone();
                        }

                        setupViewPager(mBinding.viewPager);
                        sortWithCredit(false);
                        isCreditSorted = false;
                        isPointSorted = true;
                        isPLayerSorted = true;
//
//                        if (!MyApplication.tinyDB.getBoolean(Constants.SKIP_CREATETEAM_INSTRUCTION, false)) {
//
//                            callIntroductionScreen(
//                                    R.id.tabLayout,
//                                    "Player Category",
//                                    "Select a balanced team to help you win",
//                                    ShowcaseView.BELOW_SHOWCASE
//                            );
//                            MyApplication.tinyDB.putBoolean(Constants.SKIP_CREATETEAM_INSTRUCTION, true);
//                        }


                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }

    private void setSelectedCountForEditOrClone() {
        int countPG = 0;
        int countSG = 0;
        int countSF = 0;
        int countPF = 0;
        int countC = 0;
        int totalCount =0;
        int team1Count = 0;
        int team2Count = 0;
        double usedCredit =0;

        for (Player player : allPlayerList) {
            if (player.isIsSelected()) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_PG)) {
                    countPG++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_SG)) {
                    countSG++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_SF)) {
                    countSF++;
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_PF)) {
                    countPF++;
                }

                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_C)) {
                    countC++;
                }

                if(player.getTeam().equalsIgnoreCase("team1")) {
                    team1Count++;
                }

                if(player.getTeam().equalsIgnoreCase("team2")) {
                    team2Count++;
                }

                totalCount++;
                usedCredit += player.getCredit();
            }
        }


        selectedPlayer.setPg_selected(countPG);
        selectedPlayer.setSg_selected(countSG);
        selectedPlayer.setPf_selected(countPF);
        selectedPlayer.setSf_selected(countSF);
        selectedPlayer.setC_selected(countC);
        selectedPlayer.setSelectedPlayer(totalPlayerCount);
        selectedPlayer.setLocalTeamPlayerCount(team1Count);
        selectedPlayer.setVisitorTeamPlayerCount(team2Count);
        selectedPlayer.setTotal_credit(usedCredit);

        updateTeamData(
                0,
                selectedPlayer.getPg_selected(),
                selectedPlayer.getSg_selected(),
                selectedPlayer.getSf_selected(),
                selectedPlayer.getPf_selected(),
                selectedPlayer.getC_selected(),
                selectedPlayer.getSelectedPlayer(),
                selectedPlayer.getLocalTeamPlayerCount(),
                selectedPlayer.getVisitorTeamPlayerCount(),
                selectedPlayer.getTotal_credit());


    }

    public void setTeamNames() {
        if(teamVsName!=null) {
            String teams[] = teamVsName.split("Vs");
            mBinding.tvTeamNameFirst.setText(teams[0]);
            mBinding.tvTeamNameSecond.setText(teams[1]);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        BasketBallCreateTeamActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("This team will not be saved!");
        builder.setMessage("Are you sure you want to go back?").setPositiveButton("CONTINUE", dialogClickListener)
                .setNegativeButton("CANCEL", dialogClickListener).show();

    }


    private void showTimer()
    {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                   /* if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d " + twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    } else {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    }*/
                    mBinding.tvTimeTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");

                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.tvTimeTimer.setText("00h 00m 00s");
                }
            };
            countDownTimer.start();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void openPlayerInfoActivity(String playerId, String playerName, String team, String image, boolean is_added, int pos, int type) {
        Intent intent = new Intent(this, PlayerInfoActivity.class);
        intent.putExtra("matchKey", matchKey);
        intent.putExtra("playerId", playerId);
        intent.putExtra("playerName", playerName);
        intent.putExtra("team", team);
        intent.putExtra("image", image);
        intent.putExtra("flag", "0");
        intent.putExtra("is_added", is_added);
        intent.putExtra("pos", pos);
        intent.putExtra("type", type);
        intent.putExtra(Constants.SPORT_KEY, sport_key);
        startActivity(intent);
    }




    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();


        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment) {
            mFragmentList.add(fragment);
            //mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return Constants.PG+" " + (selectedPlayer.getPg_selected() == 0?"":"(" + selectedPlayer.getPg_selected() + ")");
                case 1:
                    return Constants.SG+" " + (selectedPlayer.getSg_selected() == 0?"":"(" + selectedPlayer.getSg_selected() + ")");
                case 2:
                    return Constants.SF+" " + (selectedPlayer.getSf_selected() == 0?"":"(" + selectedPlayer.getSf_selected() + ")");
                case 3:
                    return Constants.PF+" " + (selectedPlayer.getPf_selected() == 0?"":"(" + selectedPlayer.getPf_selected() + ")");
                case 4:
                    return Constants.C+" " + (selectedPlayer.getC_selected() == 0?"":"(" + selectedPlayer.getC_selected() + ")");
            }
            return "";
        }

        @Override
        public int getItemPosition(Object object) {
            // POSITION_NONE makes it possible to reload the PagerAdapter
            return POSITION_NONE;
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new BasketballCreateTeamPlayerFragment(allPlayerList, pgList, PG));
        adapter.addFrag(new BasketballCreateTeamPlayerFragment(allPlayerList, sgList, SG));
        adapter.addFrag(new BasketballCreateTeamPlayerFragment(allPlayerList, sfList, SF));
        adapter.addFrag(new BasketballCreateTeamPlayerFragment(allPlayerList, pfList, PF));
        adapter.addFrag(new BasketballCreateTeamPlayerFragment(allPlayerList, cList, C));
        viewPager.setAdapter(adapter);
        mBinding.tabLayout.setupWithViewPager(viewPager);

    }


    public void onPlayerClick(boolean isSelect, int position, int type) {
        if (type == PG) {
            double player_credit = 0.0;
            if (isSelect) {
                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount) {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players.");
                    return;
                }
                if(selectedPlayer.getPg_selected() >= 4) {
                    showTeamValidation("You can select only 4 Point-Guard.");
                    return;
                }

                if (pgList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }

                if (selectedPlayer.getPg_selected() < selectedPlayer.getPg_max_count()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getPg_selected() < selectedPlayer.getPg_min_count() || selectedPlayer.getExtra_player() > 0) {
                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getPg_selected() >= selectedPlayer.getPg_min_count()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = pgList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (pgList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            pgList.get(position).setSelected(isSelect);


                            updateTeamData(
                                    extra,
                                    selectedPlayer.getPg_selected()+1,
                                    selectedPlayer.getSg_selected(),
                                    selectedPlayer.getSf_selected(),
                                    selectedPlayer.getPf_selected(),
                                    selectedPlayer.getC_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );

                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getPg_selected() > 0) {
                    //   showTeamValidation("Pick 1 Point-Guard.");
                    player_credit = pgList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;

                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getPg_selected() > selectedPlayer.getPg_min_count()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (pgList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    pgList.get(position).setSelected(isSelect);

                    updateTeamData(
                            extra,
                            selectedPlayer.getPg_selected()-1,
                            selectedPlayer.getSg_selected(),
                            selectedPlayer.getSf_selected(),
                            selectedPlayer.getPf_selected(),
                            selectedPlayer.getC_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

        else if (type == SG) {
            double player_credit = 0.0;

            if (isSelect) {

                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount) {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players");
                    return;
                }

                if(selectedPlayer.getSg_selected() >=4) {
                    showTeamValidation("You can select only 4 Shooting-Guard.");
                    return;
                }


                if (sgList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }


                if (selectedPlayer.getSg_selected() < selectedPlayer.getSg_max_count()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getSg_selected() < selectedPlayer.getSg_min_count() || selectedPlayer.getExtra_player() > 0) {

                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getSg_selected() >= selectedPlayer.getSg_min_count()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = sgList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (sgList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() +1;

                            sgList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getPg_selected(),
                                    selectedPlayer.getSg_selected()+1,
                                    selectedPlayer.getSf_selected(),
                                    selectedPlayer.getPf_selected(),
                                    selectedPlayer.getC_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getSg_selected() > 0) {
                    //  showTeamValidation("Pick 3-5 Defender");
                    player_credit = sgList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;

                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getSg_selected() > selectedPlayer.getSg_min_count()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (sgList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() -1;

                    sgList.get(position).setSelected(isSelect);
                    updateTeamData(
                            extra,
                            selectedPlayer.getPg_selected(),
                            selectedPlayer.getSg_selected()-1,
                            selectedPlayer.getSf_selected(),
                            selectedPlayer.getPf_selected(),
                            selectedPlayer.getC_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

        else if (type == SF) {
            double player_credit = 0.0;
            if (isSelect) {
                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount) {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players.");
                    return;
                }
                if(selectedPlayer.getSf_selected() >= 4) {
                    showTeamValidation("You can select only 4 Small-Forward");
                    return;
                }

                if (sfList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }



                if (selectedPlayer.getSf_selected() < selectedPlayer.getSf_max_count()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getSf_selected() < selectedPlayer.getSf_min_count() || selectedPlayer.getExtra_player() > 0) {

                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getSf_selected() >= selectedPlayer.getSf_min_count()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = sfList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (sfList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() + 1;

                            sfList.get(position).setSelected(isSelect);
                            updateTeamData(
                                    extra,
                                    selectedPlayer.getPg_selected(),
                                    selectedPlayer.getSg_selected(),
                                    selectedPlayer.getSf_selected()+1,
                                    selectedPlayer.getPf_selected(),
                                    selectedPlayer.getC_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {
                if (selectedPlayer.getSf_selected() > 0) {
//                    showTeamValidation("Pick 3-5 Midfielder");
                    player_credit = sfList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;



                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getSf_selected() > selectedPlayer.getSf_min_count()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (sfList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    sfList.get(position).setSelected(isSelect);

                    updateTeamData(
                            extra,
                            selectedPlayer.getPg_selected(),
                            selectedPlayer.getSg_selected(),
                            selectedPlayer.getSf_selected()-1,
                            selectedPlayer.getPf_selected(),
                            selectedPlayer.getC_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

        else if (type == PF) {
            double player_credit = 0.0;

            if (isSelect) {

                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount)
                {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players.");
                    return;
                }

                if(selectedPlayer.getPf_selected() >= 4) {
                    showTeamValidation("You can select only 4 Power-Forward");
                    return;
                }


                if (pfList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }



                if (selectedPlayer.getPf_selected() < selectedPlayer.getPf_max_count()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getPf_selected() < selectedPlayer.getPf_min_count() || selectedPlayer.getExtra_player() > 0) {

                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getPf_selected() >= selectedPlayer.getPf_min_count()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = pfList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (pfList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() +1;

                            pfList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getPg_selected(),
                                    selectedPlayer.getSg_selected(),
                                    selectedPlayer.getSf_selected(),
                                    selectedPlayer.getPf_selected()+1,
                                    selectedPlayer.getC_selected(),
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {

                if (selectedPlayer.getPf_selected() > 0) {
                    //     showTeamValidation("Pick 1-3 Forward");
                    player_credit = pfList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;


                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getPf_selected() > selectedPlayer.getPf_min_count()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (pfList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    pfList.get(position).setSelected(isSelect);
                    updateTeamData(
                            extra,
                            selectedPlayer.getPg_selected(),
                            selectedPlayer.getSg_selected(),
                            selectedPlayer.getSf_selected(),
                            selectedPlayer.getPf_selected()-1,
                            selectedPlayer.getC_selected(),
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

        else if (type == C) {
            double player_credit = 0.0;

            if (isSelect) {

                if(selectedPlayer.getSelectedPlayer()>=totalPlayerCount)
                {
                    showTeamValidation("You can choose maximum "+totalPlayerCount+" players.");
                    return;
                }

                if(selectedPlayer.getC_selected() >= 4) {
                    showTeamValidation("You can select only 4 Centre");
                    return;
                }


                if (cList.get(position).getTeam().equals("team1")) {
                    if (selectedPlayer.getLocalTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                } else {
                    if (selectedPlayer.getVisitorTeamPlayerCount() >= maxTeamPlayerCount) {
                        showTeamValidation("You can select only "+maxTeamPlayerCount+" from each team.");
                        return;
                    }
                }



                if (selectedPlayer.getC_selected() < selectedPlayer.getC_max_count()) {
                    if (selectedPlayer.getSelectedPlayer() < totalPlayerCount) {
                        if (selectedPlayer.getC_selected() < selectedPlayer.getC_min_count() || selectedPlayer.getExtra_player() > 0) {

                            int  extra = selectedPlayer.getExtra_player();
                            if (selectedPlayer.getC_selected() >= selectedPlayer.getC_min_count()) {
                                extra = selectedPlayer.getExtra_player() - 1;
                            }

                            player_credit = cList.get(position).getCredit();

                            double total_credit = selectedPlayer.getTotal_credit() + player_credit;
                            if (total_credit > totalCredit) {
                                exeedCredit = true;
                                showTeamValidation("Not enough credits to select this player.");
                                return;
                            }
                            int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                            int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                            if (cList.get(position).getTeam().equals("team1"))
                                localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() + 1;
                            else
                                visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() +1;

                            cList.get(position).setSelected(isSelect);

                            updateTeamData(
                                    extra,
                                    selectedPlayer.getPg_selected(),
                                    selectedPlayer.getSg_selected(),
                                    selectedPlayer.getSf_selected(),
                                    selectedPlayer.getPf_selected(),
                                    selectedPlayer.getC_selected()+1,
                                    selectedPlayer.getSelectedPlayer() + 1,
                                    localTeamplayerCount,
                                    visitorTeamPlayerCount,
                                    total_credit
                            );
                        }
                        else {
                            minimumPlayerWarning();
                        }
                    }
                }
            } else {

                if (selectedPlayer.getC_selected() > 0) {
                    //     showTeamValidation("Pick 1-3 Forward");
                    player_credit = cList.get(position).getCredit();

                    double total_credit = selectedPlayer.getTotal_credit() - player_credit;


                    int extra = selectedPlayer.getExtra_player();
                    if (selectedPlayer.getC_selected() > selectedPlayer.getC_min_count()) {
                        extra = selectedPlayer.getExtra_player() + 1;
                    }
                    int localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount();
                    int visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount();

                    if (cList.get(position).getTeam().equals("team1"))
                        localTeamplayerCount = selectedPlayer.getLocalTeamPlayerCount() - 1;
                    else
                        visitorTeamPlayerCount = selectedPlayer.getVisitorTeamPlayerCount() - 1;

                    cList.get(position).setSelected(isSelect);
                    updateTeamData(
                            extra,
                            selectedPlayer.getPg_selected(),
                            selectedPlayer.getSg_selected(),
                            selectedPlayer.getSf_selected(),
                            selectedPlayer.getPf_selected(),
                            selectedPlayer.getC_selected()-1,
                            selectedPlayer.getSelectedPlayer() - 1,
                            localTeamplayerCount,
                            visitorTeamPlayerCount,
                            total_credit
                    );
                }
            }
        }

    }

    public void createTeamData() {
        selectedPlayer = new BasketBallSelectedPlayer();
        selectedPlayer.setExtra_player(3);

        selectedPlayer.setPg_min_count(1);
        selectedPlayer.setPg_max_count(4);
        selectedPlayer.setPg_selected(0);

        selectedPlayer.setSg_min_count(1);
        selectedPlayer.setSg_max_count(4);
        selectedPlayer.setSg_selected(0);

        selectedPlayer.setSf_min_count(1);
        selectedPlayer.setSf_max_count(4);
        selectedPlayer.setSf_selected(0);

        selectedPlayer.setPf_min_count(1);
        selectedPlayer.setPf_max_count(4);
        selectedPlayer.setPf_selected(0);

        selectedPlayer.setC_min_count(1);
        selectedPlayer.setC_max_count(4);
        selectedPlayer.setC_selected(0);

        selectedPlayer.setSelectedPlayer(0);

        selectedPlayer.setLocalTeamPlayerCount(0);
        selectedPlayer.setVisitorTeamPlayerCount(0);
        selectedPlayer.setTotal_credit(0.0);
        updateUi();
    }

    public void updateTeamData(
            int extra_player,
            int pg_selected,
            int sg_selected,
            int sf_selected,
            int pf_selected,
            int c_selected,
            int selectPlayer,
            int localTeamPlayerCount,
            int visitorTeamPlayerCount,
            double total_credit)
    {
        exeedCredit = false;
        selectedPlayer.setExtra_player(extra_player);
        selectedPlayer.setPg_selected(pg_selected);
        selectedPlayer.setSg_selected(sg_selected);
        selectedPlayer.setSf_selected(sf_selected);
        selectedPlayer.setPf_selected(pf_selected);
        selectedPlayer.setC_selected(c_selected);
        selectedPlayer.setSelectedPlayer(selectPlayer);
        selectedPlayer.setLocalTeamPlayerCount(localTeamPlayerCount);
        selectedPlayer.setVisitorTeamPlayerCount(visitorTeamPlayerCount);
        selectedPlayer.setTotal_credit(total_credit);
        mBinding.tvLocalTeam.setText("("+localTeamPlayerCount+")");
        mBinding.tvVisitorTeam.setText("("+visitorTeamPlayerCount+")");
        updateUi();
    }

    private void updateUi() {
        mBinding.tvSelectedPlayer.setText(String.valueOf(selectedPlayer.getSelectedPlayer()));

        if (selectedPlayer.getLocalTeamPlayerCount()+selectedPlayer.getVisitorTeamPlayerCount() == 8)
        {
            mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_dark_blue);
        }
        else
        {
            mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_grey);
        }
        if(selectedPlayer.getTotal_credit()<0)
            selectedPlayer.setTotal_credit(0);

        String creditLeft = String.valueOf(totalCredit-selectedPlayer.getTotal_credit());

        mBinding.tvUsedCredit.setText(creditLeft);

        mBinding.tvTeamCountFirst.setText("("+selectedPlayer.getLocalTeamPlayerCount()+")");
        mBinding.tvTeamCountSecond.setText("("+selectedPlayer.getVisitorTeamPlayerCount()+")");

        mSelectedUnSelectedPlayerAdapter.update(selectedPlayer.getSelectedPlayer());
        if (mBinding.tabLayout.getTabCount() > 0) {
            // Log.e("childCount",mBinding.tabLayout.getChildCount()+"");
            // Log.e("tabCount",mBinding.tabLayout.getTabCount()+"");
//            callFragmentRefresh();
            if (MyApplication.basketballplayerItemAdapter!=null) {
                if (mBinding.tabLayout.getSelectedTabPosition() == 0) {
                    mBinding.tabLayout.getTabAt(0).setText("PG " + (selectedPlayer.getPg_selected() == 0 ? "" : "(" + selectedPlayer.getPg_selected() + ")"));
                    MyApplication.basketballplayerItemAdapter.updateData(pgList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 1) {
                    mBinding.tabLayout.getTabAt(1).setText("SG " + (selectedPlayer.getSg_selected() == 0 ? "" : "(" + selectedPlayer.getSg_selected() + ")"));
                    MyApplication.basketballplayerItemAdapter.updateData(sgList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 2) {
                    mBinding.tabLayout.getTabAt(2).setText("SF " + (selectedPlayer.getSf_selected() == 0 ? "" : "(" + selectedPlayer.getSf_selected() + ")"));
                    MyApplication.basketballplayerItemAdapter.updateData(sfList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 3) {
                    mBinding.tabLayout.getTabAt(3).setText("PF " + (selectedPlayer.getPf_selected() == 0 ? "" : "(" + selectedPlayer.getPf_selected() + ")"));
                    MyApplication.basketballplayerItemAdapter.updateData(pfList, selectedType);
                } else if (mBinding.tabLayout.getSelectedTabPosition() == 4) {
                    mBinding.tabLayout.getTabAt(4).setText("C " + (selectedPlayer.getC_selected() == 0 ? "" : "(" + selectedPlayer.getC_selected() + ")"));
                    MyApplication.basketballplayerItemAdapter.updateData(cList, selectedType);
                }
            }
        }
    }

    public void callFragmentRefresh() {
        if(mBinding.viewPager.getAdapter()!=null)
            mBinding.viewPager.getAdapter().notifyDataSetChanged();

        FragmentManager fm = getSupportFragmentManager();
        switch (selectedType) {
            case 1:
                mBinding.tabLayout.getTabAt(0).setText(Constants.PG+" " + (selectedPlayer.getPg_selected() == 0?"":"(" + selectedPlayer.getPg_selected() + ")"));
                if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                    ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(pgList,selectedType);
                break;
            case 2:
                mBinding.tabLayout.getTabAt(1).setText(Constants.SG+" "+ (selectedPlayer.getSg_selected() == 0?"":"(" + selectedPlayer.getSg_selected() + ")"));
                if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                    ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(sgList,selectedType);
                break;
            case 3:
                mBinding.tabLayout.getTabAt(2).setText(Constants.SF+" " + (selectedPlayer.getSf_selected() == 0?"":"(" + selectedPlayer.getSf_selected() + ")"));
                if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                    ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(sfList,selectedType);
                break;
            case 4:
                mBinding.tabLayout.getTabAt(3).setText(Constants.PF+" " + (selectedPlayer.getPf_selected() == 0?"":"(" + selectedPlayer.getPf_selected() + ")"));
                if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                    ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(pfList,selectedType);
                break;
            case 5:
                mBinding.tabLayout.getTabAt(3).setText(Constants.C+" " + (selectedPlayer.getC_selected() == 0?"":"(" + selectedPlayer.getC_selected() + ")"));
                if(fm.getFragments().get(0) instanceof BasketballCreateTeamPlayerFragment)
                    ((BasketballCreateTeamPlayerFragment)fm.getFragments().get(0)).refresh(cList,selectedType);
                break;
        }

    }

    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public void minimumPlayerWarning() {
        if(selectedPlayer.getPg_selected()<1) {
            showTeamValidation("You must select at least 1 Point-Guard.");
        }
        else if(selectedPlayer.getSg_selected()<1) {
            showTeamValidation("You must select at least 1 Shooting-Guard.");
        }
        else  if(selectedPlayer.getSf_selected()<1) {
            showTeamValidation("You must select at least 1 Small-Forward");
        }
        else  if(selectedPlayer.getPf_selected()<1) {
            showTeamValidation("You must select at least 1 Power-Forward");
        }
        else  if(selectedPlayer.getC_selected()<1) {
            showTeamValidation("You must select at least 1 Centre");
        }
    }


    public void showTeamValidation(String mesg) {

        AppUtils.showErrorr(this, mesg);

      /*  final Flashbar flashbar = new Flashbar.Builder(this)
                .gravity(Flashbar.Gravity.TOP)
                .message(mesg)
                .backgroundDrawable(R.drawable.bg_gradient_create_team_warning)
                .showIcon()
                .icon(R.drawable.close)
                .iconAnimation(FlashAnim.with(this)
                        .animateIcon()
                        .pulse()
                        .alpha()
                        .duration(1000)
                        .accelerate())
                .build();

        flashbar.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flashbar.dismiss();
            }
        },2000);
    }

       */

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 101) {
            if(resultCode == Activity.RESULT_OK){
                Intent returnIntent = new Intent();
                returnIntent.putExtra("isTeamCreated",data.getBooleanExtra("isTeamCreated",false));
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }

        }
    }

    private void setData() {
//        mBinding.tvTotalCredit.setText(" /"+ String.valueOf(totalCredit));
//        mBinding.tvTotalPlayer.setText(" /"+ String.valueOf(totalPlayerCount));
        //  mBinding.tvMaxPlayerWarning.setText("Max "+maxTeamPlayerCount+" Players from a one team");
        mSelectedUnSelectedPlayerAdapter.updateTotalPlayerCount(totalPlayerCount);
    }


//    void callIntroductionScreen(int target, String title, String description, int abovE_SHOWCASE) {
//
//
//        ShowcaseView showcaseView = new ShowcaseView.Builder(this).withNewStyleShowcase()
//                .setTarget(new ViewTarget(target, this))
//                .setContentTitle(title)
//                .setContentText(description)
//                .setStyle(counterValue == 0 ? R.style.CustomShowcaseTheme : R.style.CustomShowcaseTheme)
//                .hideOnTouchOutside().setShowcaseEventListener(this)
//                .build();
//
//        showcaseView.forceTextPosition(abovE_SHOWCASE);
//        counterValue = counterValue + 1;
//
//        new Handler().postDelayed(() -> showcaseView.hideButton(), 2500);
//
//    }
//
//
//    @Override
//    public void onShowcaseViewHide(ShowcaseView showcaseView) {
//
//        switch (counterValue) {
//            case 1: {
//                callIntroductionScreen(
//                        R.id.ll_credit,
//                        "Credit Counter",
//                        "Use 100 credits to pick your players", ShowcaseView.BELOW_SHOWCASE
//                );
//                break;
//            }
//            case 2: {
//                callIntroductionScreen(
//                        R.id.ll_players,
//                        "Player Counter",
//                        "Pick 11 players to create your team"
//                        ,
//                        ShowcaseView.BELOW_SHOWCASE
//                );
//                break;
//            }
//
//        }
//
//    }
//
//    @Override
//    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
//
//    }
//
//    @Override
//    public void onShowcaseViewShow(ShowcaseView showcaseView) {
//
//    }
//
//    @Override
//    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {
//
//    }
}
