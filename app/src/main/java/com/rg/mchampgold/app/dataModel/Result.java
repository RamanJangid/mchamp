package com.rg.mchampgold.app.dataModel;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Result{

	@SerializedName("contest")
	private List<ContestItem> contest;

	@SerializedName("user_teams")
	private int userTeams;

	@SerializedName("joinedleauges")
	private int joinedleauges;

	public void setContest(List<ContestItem> contest){
		this.contest = contest;
	}

	public List<ContestItem> getContest(){
		return contest;
	}

	public void setUserTeams(int userTeams){
		this.userTeams = userTeams;
	}

	public int getUserTeams(){
		return userTeams;
	}

	public void setJoinedleauges(int joinedleauges){
		this.joinedleauges = joinedleauges;
	}

	public int getJoinedleauges(){
		return joinedleauges;
	}

	@Override
	public String toString(){
		return
				"Result{" +
						"contest = '" + contest + '\'' +
						",user_teams = '" + userTeams + '\'' +
						",joinedleauges = '" + joinedleauges + '\'' +
						"}";
	}
}