package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Limit {

	@SerializedName("maxplayers")
	private int totalPlayers;

	@SerializedName("team_max_player")
	private int teamMaxPlayer;

	@SerializedName("total_credits")
	private double totalCredits;

	public int getTotalPlayers() {
		return totalPlayers;
	}

	public void setTotalPlayers(int totalPlayers) {
		this.totalPlayers = totalPlayers;
	}

	public int getTeamMaxPlayer() {
		return teamMaxPlayer;
	}

	public void setTeamMaxPlayer(int teamMaxPlayer) {
		this.teamMaxPlayer = teamMaxPlayer;
	}

	public double getTotalCredits() {
		return totalCredits;
	}

	public void setTotalCredits(double totalCredits) {
		this.totalCredits = totalCredits;
	}
}