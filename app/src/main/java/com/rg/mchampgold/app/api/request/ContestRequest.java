package com.rg.mchampgold.app.api.request;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ContestRequest {

	@SerializedName("matchkey")
	private String matchKey;


	public String getMatchKey() {
		return matchKey;
	}

	public void setMatchKey(String matchKey) {
		this.matchKey = matchKey;
	}
	@SerializedName("user_id")
	private String user_id;

	@SerializedName("challenge_id")
	private String leagueId;

	@SerializedName("page")
	private String page;

	@SerializedName("category_id")
	private String categoryId;

	@SerializedName("entryfee")
	private String entryfee;

	@SerializedName("winning")
	private String winning;

	@SerializedName("contest_type")
	private String contestType;

	@SerializedName("contest_size")
	private String contestSize;

	@SerializedName("sport_key")
	private String sport_key;


	boolean showLeaderBoard;


	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getLeagueId() {
		return leagueId;
	}

	public void setLeagueId(String leagueId) {
		this.leagueId = leagueId;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public boolean isShowLeaderBoard() {
		return showLeaderBoard;
	}

	public void setShowLeaderBoard(boolean showLeaderBoard) {
		this.showLeaderBoard = showLeaderBoard;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getEntryfee() {
		return entryfee;
	}

	public void setEntryfee(String entryfee) {
		this.entryfee = entryfee;
	}

	public String getWinning() {
		return winning;
	}

	public void setWinning(String winning) {
		this.winning = winning;
	}

	public String getContestType() {
		return contestType;
	}

	public void setContestType(String contestType) {
		this.contestType = contestType;
	}

	public String getContestSize() {
		return contestSize;
	}

	public void setContestSize(String contestSize) {
		this.contestSize = contestSize;
	}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}
}