package com.rg.mchampgold.app.di.module;

import android.content.Context;
import android.content.SharedPreferences;


import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.MyRestHelper;
import com.rg.mchampgold.common.api.RestHelper;
import com.rg.mchampgold.common.utils.TinyDB;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    final private MyApplication application;

    public AppModule(MyApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public MyApplication provideMyApplication() {
        return application;
    }

    @Provides
    public Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    public TinyDB provideTinyDB() {
        SharedPreferences spPrivate = this.application.getSharedPreferences("private", Context.MODE_PRIVATE);
        return new TinyDB(spPrivate);
    }

    @Provides
    public AppModule provideAppModule() {
        return this;
    }

    @Provides
    public RestHelper provideMyRestHelper(AppModule appModule) {
        return new MyRestHelper(appModule);
    }

}



