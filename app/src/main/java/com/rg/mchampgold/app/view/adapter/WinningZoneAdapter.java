package com.rg.mchampgold.app.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.databinding.RecyclerItemWinningZoneBinding;

public class WinningZoneAdapter extends RecyclerView.Adapter<WinningZoneAdapter.WinningHolder> {
    @NonNull
    @Override
    public WinningHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerItemWinningZoneBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.recycler_item_winning_zone,
                parent, false);
        return new WinningHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull WinningHolder holder, int position) {

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class WinningHolder extends RecyclerView.ViewHolder {

        RecyclerItemWinningZoneBinding binding;

        public WinningHolder(RecyclerItemWinningZoneBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
