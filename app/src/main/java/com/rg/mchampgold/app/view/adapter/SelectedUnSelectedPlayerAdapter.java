package com.rg.mchampgold.app.view.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.databinding.LayoutPlayerSelectedBinding;


public class SelectedUnSelectedPlayerAdapter extends RecyclerView.Adapter<SelectedUnSelectedPlayerAdapter.ViewHolder> {

    private int  selectedPlayerCount;
    private int totalPlayerCount;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        final LayoutPlayerSelectedBinding binding;

        public ViewHolder(LayoutPlayerSelectedBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public SelectedUnSelectedPlayerAdapter(int selectedPlayerCount, int totalPlayerCount, Context context) {
        this.selectedPlayerCount = selectedPlayerCount;
        this.totalPlayerCount = totalPlayerCount;
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutPlayerSelectedBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_player_selected,
                        parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if(position<selectedPlayerCount) {
            holder.binding.ivSelectedUnselected.setImageResource(R.drawable.ic_team_selected_progress);
           // holder.binding.ivSelectedUnselected.setColorFilter(context.getResources().getColor(R.color.selection_indicator));
        }else {
            holder.binding.ivSelectedUnselected.setImageResource(R.drawable.ic_team_unselected_progress);
        }
        holder.binding.tvPos.setText((position+1)+"");
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return totalPlayerCount;
    }

    public void update(int selectedPlayerCount) {
        this.selectedPlayerCount = selectedPlayerCount;
        notifyDataSetChanged();
    }

    public void updateTotalPlayerCount(int totalPlayerCount) {
        this.totalPlayerCount = totalPlayerCount;
        notifyDataSetChanged();
    }




}