package com.rg.mchampgold.app.api.response;

import java.util.ArrayList;

public class UserOAuthResponse {
    public String access_token;
    public String token_type;
    public String refresh_token;
    public int expires_in;
    public String scope;
    public String user_id;
    public ArrayList<String> roles;

    public ArrayList<String> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }

    public boolean isAdmin() {
        for(String string:roles){
            if(string.equalsIgnoreCase("admin"))
                return true;
        }
        return false;
    }

    public boolean isRetailer() {
        for(String string:roles){
            if(string.equalsIgnoreCase("retailer"))
                return true;
        }
        return false;
    }
}
