package com.rg.mchampgold.app.api.request;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class BaseRequest {

	@SerializedName("user_id")
	private String user_id;

	@SerializedName("mobile")
	private String mobile;


	@SerializedName("type")
	private String type;

	@SerializedName("file")
	private String file;

	@SerializedName("amount")
	private String amount;

	@SerializedName("email")
	private String email;

	@SerializedName("matchkey")
	private String matchkey;

	@SerializedName("promo")
	private String promo;

	@SerializedName("payment_type")
	private String paymentType;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@SerializedName("challenge_id")
	private int challenge_id;

	@SerializedName("sport_key")
	private String sport_key;

	@SerializedName("team1_id")
	private String team1_id;

	@SerializedName("team2_id")
	private String team2_id;

	@SerializedName("player_id")
	private String player_id;

	@SerializedName("series_id")
	private String series_id;


	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getMatchkey() {
		return matchkey;
	}

	public void setMatchkey(String matchkey) {
		this.matchkey = matchkey;
	}

	public String getPromo() {
		return promo;
	}

	public void setPromo(String promo) {
		this.promo = promo;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public int getChallenge_id() {
		return challenge_id;
	}

	public void setChallenge_id(int challenge_id) {
		this.challenge_id = challenge_id;
	}

	public String getSport_key() {
		return sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}

	public String getTeam1_id() {
		return team1_id;
	}

	public void setTeam1_id(String team1_id) {
		this.team1_id = team1_id;
	}

	public String getTeam2_id() {
		return team2_id;
	}

	public void setTeam2_id(String team2_id) {
		this.team2_id = team2_id;
	}

	public String getPlayer_id() {
		return player_id;
	}

	public void setPlayer_id(String player_id) {
		this.player_id = player_id;
	}

	public String getSeries_id() {
		return series_id;
	}

	public void setSeries_id(String series_id) {
		this.series_id = series_id;
	}
}