package com.rg.mchampgold.app.view.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.ContestResponse;
import com.rg.mchampgold.app.dataModel.GetWinnerScoreCardResponse;
import com.rg.mchampgold.app.dataModel.WinnerScoreCardItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.TeamCreatedListener;
import com.rg.mchampgold.app.view.adapter.ContestItemAdapter;
import com.rg.mchampgold.app.view.basketball.BasketBallCreateTeamActivity;
import com.rg.mchampgold.app.view.football.FootballCreateTeamActivity;
import com.rg.mchampgold.app.view.fragment.MyJoinedContestFragment;
import com.rg.mchampgold.app.view.fragment.MyTeamFragment;
import com.rg.mchampgold.app.view.fragment.UpComingContestFragment;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.app.viewModel.ContestViewModel;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityAllContestBinding;
import com.rg.mchampgold.databinding.ActivityFilltersBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class AllContestActivity extends AppCompatActivity implements OnContestItemClickListener, TeamCreatedListener {

    ActivityAllContestBinding mBinding;
    ContestItemAdapter mAdapter;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    int categoryId;
    String date;
    String userReferCode = "";
    Context context;
    int teamCount;
    public static TeamCreatedListener listener;
    String sportKey="";
    boolean isTeamCreated = false;
    private ContestViewModel contestViewModel;
    private Boolean isWinnings = false, isTeam = false, isEntry = false, isWinner = false;
    ArrayList<Contest> list = new ArrayList<>();
    private ArrayList<String> entryFee = new ArrayList<>();
    private ArrayList<String> winning = new ArrayList<>();
    private ArrayList<String> contest_type = new ArrayList<>();
    private ArrayList<String> contest_size = new ArrayList<>();
    boolean isForContestDetails;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getAppComponent().inject(AllContestActivity.this);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        context = AllContestActivity.this;
        userReferCode = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_REFER_CODE);
        contestViewModel = ContestViewModel.create(AllContestActivity.this);
        MyApplication.getAppComponent().inject(contestViewModel);
        MyApplication.getAppComponent().inject(AllContestActivity.this);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_all_contest);
        initialize();
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.contest));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            teamCount = getIntent().getExtras().getInt(Constants.KEY_TEAM_COUNT);
            date = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT);
            isForContestDetails = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS);
            categoryId = getIntent().getIntExtra(Constants.KEY_ALL_CONTEST, 0);
            sportKey  = getIntent().getExtras().getString(Constants.SPORT_KEY);
        }

        String teams[] = teamVsName.split(" ");
        mBinding.matchHeaderInfo.tvTeam1.setText(teams[0]);
        mBinding.matchHeaderInfo.tvTeam2.setText(teams[2]);

        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam2, teamSecondUrl);

        MyTeamFragment myTeamFragment = new MyTeamFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.KEY_MATCH_KEY, matchKey);
        myTeamFragment.setArguments(bundle);

        UpComingContestFragment upCommingContestFragment = new UpComingContestFragment();
        upCommingContestFragment.setArguments(bundle);

        MyJoinedContestFragment myJoinedContestFragment = new MyJoinedContestFragment();
        myJoinedContestFragment.setArguments(bundle);


        mBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            if (categoryId == 111) {
                getAllContest();
            } else {
                getAllContestByCategoryId(categoryId);
            }
        });


        setupRecyclerView();
        if (categoryId == 111) {
            getAllContest();
        } else {
            getAllContestByCategoryId(categoryId);
        }

        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(date), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                    mBinding.matchHeaderInfo.tvMatchTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.matchHeaderInfo.tvMatchTimer.setText("00h 00m 00s");
                }
            };
            countDownTimer.start();
        } catch (Exception e) {
        }

        mBinding.tvPrizePoolSort.setOnClickListener(view -> {
            if (mAdapter != null) {
                if (isWinnings) {
                    isWinnings = false;
                    mAdapter.sortWithPrizePool(true);
                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_sort, 0);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                } else {
                    isWinnings = true;
                    mAdapter.sortWithPrizePool(false);
                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_sort, 0);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


                }


            }
        });

        mBinding.tvSpotSort.setOnClickListener(view -> {
            if (mAdapter != null) {
                if (isTeam) {
                    isTeam = false;
                    mAdapter.sortWithSpot(true);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_sort, 0);
                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


                } else {
                    isTeam = true;
                    mAdapter.sortWithSpot(false);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_sort, 0);
                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                }
            }
        });

        mBinding.tvEntrySort.setOnClickListener(view -> {
            if (mAdapter != null) {
                if (isEntry) {
                    isEntry = false;
                    mAdapter.sortWithEntry(true);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_sort, 0);

                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                } else {
                    isEntry = true;
                    mAdapter.sortWithEntry(false);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_sort, 0);
                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        });

        mBinding.tvWinnersSort.setOnClickListener(view -> {
            if (mAdapter != null) {
                if (isWinner) {
                    isWinner = false;
                    mAdapter.sortWithWinners(true);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_sort, 0);

                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


                } else {
                    isWinner = true;
                    mAdapter.sortWithWinners(false);
                    mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_sort, 0);

                    mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }
            }
        });
        showFilterDialog();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;

           /* case R.id.navigation_wallet:
                openWalletActivity();
                return true;*/
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        } else if (itemId == R.id.navigation_filters) {
            bottomSheetDialog.show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Dialog bottomSheetDialog;

    private void showFilterDialog() {
        ActivityFilltersBinding activityFilltersBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.activity_fillters, null, false);
        //  setContentView(activityFilltersBinding.getRoot());


        bottomSheetDialog = new Dialog(AllContestActivity.this);
        bottomSheetDialog.setContentView(activityFilltersBinding.getRoot());
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bottomSheetDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        activityFilltersBinding.closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        contest_size = new ArrayList<>();
        entryFee = new ArrayList<>();
        winning = new ArrayList<>();
        contest_type = new ArrayList<>();

        activityFilltersBinding.clearAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityFilltersBinding.checkbox1100entry.setChecked(false);
                activityFilltersBinding.checkbox1001000entry.setChecked(false);
                activityFilltersBinding.checkbox10005000entry.setChecked(false);
                activityFilltersBinding.checkbox5000Moreentry.setChecked(false);
                activityFilltersBinding.checkbox11000win.setChecked(false);
                activityFilltersBinding.checkbox100050000win.setChecked(false);
                activityFilltersBinding.checkbox5000010000win.setChecked(false);
                activityFilltersBinding.checkbox25LMorewin.setChecked(false);
                activityFilltersBinding.checkboxMultientry.setChecked(false);
                activityFilltersBinding.checkboxbonus.setChecked(false);
                activityFilltersBinding.checkboxconfirmed.setChecked(false);
                activityFilltersBinding.checkbox2members.setChecked(false);
                activityFilltersBinding.checkbox310member.setChecked(false);
                activityFilltersBinding.checkbox1120members.setChecked(false);
                activityFilltersBinding.checkbox21100members.setChecked(false);
                activityFilltersBinding.checkbox100110000members.setChecked(false);
                activityFilltersBinding.checkbox1011000members.setChecked(false);

                entryFee.clear();
                winning.clear();
                contest_size.clear();
                contest_type.clear();
                bottomSheetDialog.dismiss();
                if (categoryId == 111) {
                    getAllContest();
                } else {
                    getAllContestByCategoryId(categoryId);
                }
            }
        });

        activityFilltersBinding.applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                if (categoryId == 111) {
                    getAllContest();
                } else {
                    getAllContestByCategoryId(categoryId);
                }
            }
        });

        activityFilltersBinding.checkbox1100entry.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    entryFee.add("1");
                } else {
                    entryFee.remove("1");
                }
            }
        });

        activityFilltersBinding.checkbox1001000entry.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                entryFee.add("2");
            } else {
                entryFee.remove("2");
            }
        });

        activityFilltersBinding.checkbox10005000entry.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                entryFee.add("3");
            } else {
                entryFee.remove("3");
            }
        });

        activityFilltersBinding.checkbox5000Moreentry.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                entryFee.add("4");
            } else {
                entryFee.remove("4");
            }
        });

        activityFilltersBinding.checkbox11000win.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                winning.add("1");
            } else {
                winning.remove("1");
            }
        });

        activityFilltersBinding.checkbox100050000win.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                winning.add("2");
            } else {
                winning.remove("2");
            }
        });

        activityFilltersBinding.checkbox5000010000win.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                winning.add("3");
            } else {
                winning.remove("3");
            }
        });

        activityFilltersBinding.checkbox25LMorewin.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                winning.add("4");
            } else {
                winning.remove("4");
            }
        });


        activityFilltersBinding.checkboxMultientry.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_type.add("1");
            } else {
                contest_type.remove("1");
            }
        });
        activityFilltersBinding.checkboxbonus.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_type.add("3");
            } else {
                contest_type.remove("3");
            }
        });
        activityFilltersBinding.checkboxconfirmed.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_type.add("2");
            } else {
                contest_type.remove("2");
            }
        });


        activityFilltersBinding.checkbox2members.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_size.add("1");
            } else {
                contest_size.remove("1");
            }
        });


        activityFilltersBinding.checkbox310member.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_size.add("2");
            } else {
                contest_size.remove("2");
            }
        });

        activityFilltersBinding.checkbox1120members.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_size.add("3");
            } else {
                contest_size.remove("3");
            }
        });

        activityFilltersBinding.checkbox21100members.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_size.add("4");
            } else {
                contest_size.remove("4");
            }
        });

        activityFilltersBinding.checkbox1011000members.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_size.add("5");
            } else {
                contest_size.remove("5");
            }
        });

        activityFilltersBinding.checkbox100110000members.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                contest_size.add("6");
            } else {
                contest_size.remove("6");
            }
        });
    }


    private void openNotificationActivity() {
        startActivity(new Intent(AllContestActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(AllContestActivity.this, MyWalletActivity.class));

    }


    private void setupRecyclerView() {
        mAdapter = new ContestItemAdapter(context, list, this, true);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onContestClick(Contest contest, boolean isForDetail) {
        if (isForDetail) {
            Intent intent = new Intent(AllContestActivity.this, UpComingContestDetailActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
            intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
            intent.putExtra(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS, isForContestDetails);
            intent.putExtra(Constants.SPORT_KEY,sportKey);
            startActivity(intent);
        } else {
            if (teamCount > 0 || isTeamCreated) {
                Intent intent = new Intent(AllContestActivity.this, MyTeamsActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
                intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
                intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
                intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
                intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
                intent.putExtra(Constants.SPORT_KEY,sportKey);
                startActivity(intent);
            } else {
                creteTeam(contest);
            }
        }
    }


    public void creteTeam(Contest contest) {

        listener = this;
        Intent intent;
        if(sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(AllContestActivity.this, FootballCreateTeamActivity.class);
        }
        else if(sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(AllContestActivity.this, BasketBallCreateTeamActivity.class);
        }
        else {
            intent = new Intent(AllContestActivity.this,CreateTeamActivity.class);
        }
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, date);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY,sportKey);
        intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
        intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
        startActivity(intent);
    }

    private void getAllContest() {
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        request.setEntryfee(TextUtils.join(",", entryFee));
        request.setWinning(TextUtils.join(",", winning));
        request.setContestType(TextUtils.join(",", contest_type));
        request.setContestSize(TextUtils.join(",", contest_size));
        contestViewModel.loadContestRequest(request);
        contestViewModel.getContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    if (mBinding.swipeRefreshLayout != null)
                        mBinding.swipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (mBinding.swipeRefreshLayout != null)
                        mBinding.swipeRefreshLayout.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1) {
                        list = arrayListResource.getData().getResult().getContestArrayList();
                        mAdapter.updateData(list);
                        refreshListWithFilter();
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    break;
                }
            }

        });
    }

    private void refreshListWithFilter() {
        mBinding.tvPrizePoolSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        mBinding.tvEntrySort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        mBinding.tvSpotSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        mBinding.tvWinnersSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    private void getAllContestByCategoryId(int categoryId) {
        mBinding.setRefreshing(true);
        ContestRequest request = new ContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setCategoryId(categoryId + "");
        request.setEntryfee(TextUtils.join(",", entryFee));
        request.setWinning(TextUtils.join(",", winning));
        request.setContestType(TextUtils.join(",", contest_type));
        request.setContestSize(TextUtils.join(",", contest_size));

        CustomCallAdapter.CustomCall<ContestResponse> myBalanceResponseCustomCall = oAuthRestService.getContestByCategoryCode(request);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<ContestResponse>() {
            @Override
            public void success(Response<ContestResponse> response) {
                mBinding.setRefreshing(false);
                if (mBinding.swipeRefreshLayout != null)
                    mBinding.swipeRefreshLayout.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    ContestResponse categoryByContestResponse = response.body();
                    if (categoryByContestResponse.getStatus() == 1 && categoryByContestResponse.getResult() != null) {
                        if (categoryByContestResponse.getResult().getContestArrayList().size() > 0) {
                            list = categoryByContestResponse.getResult().getContestArrayList();
                            mAdapter.updateData(list);
                            refreshListWithFilter();
                        } else {
                            /// AppUtils.showErrorr((HomeActivity) getActivity(), "Not found any record");
                        }
                    } else {
                        // AppUtils.showErrorr(getActivity(), categoryByContestResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                if (mBinding.swipeRefreshLayout != null)
                    mBinding.swipeRefreshLayout.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


    public void getWinnerPriceCard(int contestId, String amount) {

        ContestRequest contestRequest = new ContestRequest();
        contestRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        contestRequest.setMatchKey(matchKey);
        contestRequest.setLeagueId(contestId + "");
        CustomCallAdapter.CustomCall<GetWinnerScoreCardResponse> bankDetailResponseCustomCall = oAuthRestService.getWinnersPriceCard(contestRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<GetWinnerScoreCardResponse>() {
            @Override
            public void success(Response<GetWinnerScoreCardResponse> response) {

                if (response.isSuccessful() && response.body() != null) {
                    GetWinnerScoreCardResponse getWinnerScoreCardResponse = response.body();
                    if (getWinnerScoreCardResponse.getStatus() == 1 && getWinnerScoreCardResponse.getResult().size() > 0) {
                        ArrayList<WinnerScoreCardItem> priceList = getWinnerScoreCardResponse.getResult();
                        if (priceList.size() > 0) {
                            AppUtils.showWinningPopup(AllContestActivity.this, priceList, "" + amount);
                        }
                    }
                }
            }

            @Override
            public void failure(ApiException e) {

                e.printStackTrace();
            }
        });
    }


    @Override
    public void getTeamCreated(boolean team_created) {

        isTeamCreated = team_created;

    }
}
