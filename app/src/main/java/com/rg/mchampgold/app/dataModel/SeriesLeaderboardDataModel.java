package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class SeriesLeaderboardDataModel {

    @SerializedName("id")
    String id;
    @SerializedName("user_id")
    String user_id;
    @SerializedName("series_id")
    String series_id;
    @SerializedName("points")
    String points;
    @SerializedName("rank")
    String rank;
    @SerializedName("status")
    String status;
    @SerializedName("team")
    String team;
    @SerializedName("image")
    String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getSeries_id() {
        return series_id;
    }

    public void setSeries_id(String series_id) {
        this.series_id = series_id;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRanks(){

        return "#"+getRank();
    }
    public String getUserPoints(){

        return getPoints()+" pts";
    }
}
