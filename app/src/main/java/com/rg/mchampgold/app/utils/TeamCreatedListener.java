package com.rg.mchampgold.app.utils;

public interface TeamCreatedListener {
    void getTeamCreated(boolean team_created);
}
