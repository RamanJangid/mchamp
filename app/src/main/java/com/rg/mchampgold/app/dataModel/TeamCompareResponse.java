package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class TeamCompareResponse {

    @SerializedName("result")
    private TeamCompareModel result;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private int status;

    public TeamCompareModel getResult() {
        return result;
    }

    public void setResult(TeamCompareModel result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
