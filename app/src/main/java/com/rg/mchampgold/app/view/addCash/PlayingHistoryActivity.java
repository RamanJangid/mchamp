package com.rg.mchampgold.app.view.addCash;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.PlayingHistoryItem;
import com.rg.mchampgold.app.dataModel.PlayingHistoryResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityPlayingHistoryBinding;

import javax.inject.Inject;

import retrofit2.Response;


public class PlayingHistoryActivity extends AppCompatActivity {

    private ActivityPlayingHistoryBinding fragmentPlayingHistoryBinding;
    @Inject
    OAuthRestService oAuthRestService;
    PlayingHistoryItem playingHistoryItem;


    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(this);
        super.onCreate(savedInstanceState);
        fragmentPlayingHistoryBinding = DataBindingUtil.setContentView(this, R.layout.activity_playing_history);
        getPlayingHistory();
        setSupportActionBar(fragmentPlayingHistoryBinding.mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void getPlayingHistory() {
        fragmentPlayingHistoryBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<PlayingHistoryResponse> myBalanceResponseCustomCall = oAuthRestService.getMyPlayingHistory(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<PlayingHistoryResponse>() {
            @Override
            public void success(Response<PlayingHistoryResponse> response) {
                fragmentPlayingHistoryBinding.setRefreshing(false);

                if (response.isSuccessful() && response.body() != null) {
                    PlayingHistoryResponse playingHistoryResponse = response.body();
                    if (playingHistoryResponse.getStatus() == 1 && playingHistoryResponse.getResult() != null) {
                        playingHistoryItem = playingHistoryResponse.getResult();
                        setPlayingHistory();
                    } else {
                        AppUtils.showErrorr(PlayingHistoryActivity.this, playingHistoryResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentPlayingHistoryBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void setPlayingHistory() {
        fragmentPlayingHistoryBinding.totalMatchTxt.setText(playingHistoryItem.getTotalMatchPlay() + "");
        fragmentPlayingHistoryBinding.totalContastTxt.setText(playingHistoryItem.getTotalLeaguePlay() + "");
        fragmentPlayingHistoryBinding.totalContestWinTxt.setText(playingHistoryItem.getTotalContestWin() + "");
        fragmentPlayingHistoryBinding.totalWinningsTxt.setText("₹" + playingHistoryItem.getTotalWinning());
    }


}