package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MatchListItem {

    @SerializedName("status")
    int status;
    @SerializedName("matchkey")
    String matchkey;
    @SerializedName("title")
    String title;
    @SerializedName("short_name")
    String short_name;
    @SerializedName("format")
    String format;
    @SerializedName("start_date")
    String start_date;
    @SerializedName("user_teams")
    int user_teams;
    @SerializedName("estimated_win")
    int estimated_win;
    @SerializedName("teams")
    private ArrayList<TeamsListItem> teams;
    @SerializedName("top_players")
    private ArrayList<TopPlayersItem> top_players;
    @SerializedName("top_scorer")
    private ArrayList<TopScoreItems> top_scorer;

    @SerializedName("sport_key")
    private String sport_key;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMatchkey() {
        return matchkey;
    }

    public void setMatchkey(String matchkey) {
        this.matchkey = matchkey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public ArrayList<TeamsListItem> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<TeamsListItem> teams) {
        this.teams = teams;
    }

    public ArrayList<TopPlayersItem> getTop_players() {
        return top_players;
    }

    public void setTop_players(ArrayList<TopPlayersItem> top_players) {
        this.top_players = top_players;
    }

    public ArrayList<TopScoreItems> getTop_scorer() {
        return top_scorer;
    }

    public void setTop_scorer(ArrayList<TopScoreItems> top_scorer) {
        this.top_scorer = top_scorer;
    }

    public int getUser_teams() {
        return user_teams;
    }

    public void setUser_teams(int user_teams) {
        this.user_teams = user_teams;
    }

    public int getEstimated_win() {
        return estimated_win;
    }

    public void setEstimated_win(int estimated_win) {
        this.estimated_win = estimated_win;
    }

    public String getSport_key() {
        return sport_key;
    }

    public void setSport_key(String sport_key) {
        this.sport_key = sport_key;
    }
}
