package com.rg.mchampgold.app.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.JoinedContestTeam;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.TeamCreatedListener;
import com.rg.mchampgold.app.view.adapter.ContestJoinTeamItemAdapter;
import com.rg.mchampgold.app.view.basketball.BasketBallCreateTeamActivity;
import com.rg.mchampgold.app.view.basketball.BasketBallTeamPreviewPointActivity;
import com.rg.mchampgold.app.view.football.FootballCreateTeamActivity;
import com.rg.mchampgold.app.view.football.FootballTeamPreviewPointActivity;
import com.rg.mchampgold.app.view.fragment.LeaderBoardFragment;
import com.rg.mchampgold.app.view.fragment.WinningBreakUpFragment;
import com.rg.mchampgold.app.viewModel.ContestDetailsViewModel;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityUpcommingContestDetailsBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class UpComingContestDetailActivity extends AppCompatActivity implements TeamCreatedListener {
    private static final int CREATE_TEAM_CONTEST_DETAIL = 101;
    ActivityUpcommingContestDetailsBinding mBinding;
    ContestJoinTeamItemAdapter mAdapter;
    ContestDetailsViewModel contestDetailsViewModel;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    TabAdapter mTabAdapter;
    int teamCount;
    ArrayList<JoinedContestTeam> list = new ArrayList<>();
    String headerText;
    boolean isShowTimer;
    String contestReferCode = "";
    String sportKey = "";
    @Inject
    OAuthRestService oAuthRestService;
    Contest contest;
    Context context;
    boolean isForContestDetails;
    Menu menuTemp;
    boolean isFromLive = false;
    public static TeamCreatedListener listener;
    int left=0;
    private int seriesId;
    private int isLeaderboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        contestDetailsViewModel = ContestDetailsViewModel.create(UpComingContestDetailActivity.this);
        MyApplication.getAppComponent().inject(contestDetailsViewModel);
        MyApplication.getAppComponent().inject(UpComingContestDetailActivity.this);
        context = UpComingContestDetailActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_upcomming_contest_details);
        mBinding.matchHeaderInfo.fps.setVisibility(View.VISIBLE);
        mBinding.matchHeaderInfo.fps.setOnClickListener(v -> AppUtils.openWebViewActivity(getString(R.string.fantasy_point_system), MyApplication.fantasy_point_url));
        MyApplication.teamJoined=false;
        initialize();
    }

    void initialize() {

        setSupportActionBar(mBinding.mytoolbar);

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            teamCount = getIntent().getExtras().getInt(Constants.KEY_TEAM_COUNT);
            contest = (Contest) getIntent().getSerializableExtra(Constants.KEY_CONTEST_DATA);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT, "");
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER, false);
            isForContestDetails = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_FOR_CONTEST_DETAILS, false);
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
            seriesId = getIntent().getExtras().getInt(Constants.KEY_SERIES_ID,0);
            isLeaderboard = getIntent().getExtras().getInt(Constants.KEY_IS_LEADERBOARD, 0);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.contest));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mBinding.setContestData(contest);
        mBinding.progressBar.setProgress(Integer.parseInt(contest.getGetjoinedpercentage()));
        String teams[] = teamVsName.split(" ");
        mBinding.matchHeaderInfo.tvTeam1.setText(teams[0]);
        mBinding.matchHeaderInfo.tvTeam2.setText(teams[2]);
        mBinding.tagMText.setText(String.valueOf(contest.getMaxTeamLimit()));

        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.matchHeaderInfo.ivTeam2, teamSecondUrl);


        contestReferCode = contest.getRefercode();

        if (isShowTimer) {
            showTimer();
        } else {
            if (headerText.equalsIgnoreCase("Winner Declared")) {
                mBinding.matchHeaderInfo.tvMatchTimer.setText("Winner Declared");
                mBinding.matchHeaderInfo.tvMatchTimer.setTextColor(getResources().getColor(R.color.pink));
            } else if (headerText.equalsIgnoreCase("In Progress")) {
                mBinding.matchHeaderInfo.tvMatchTimer.setText("In Progress");
                mBinding.matchHeaderInfo.tvMatchTimer.setTextColor(getResources().getColor(R.color.pink));
            } else {
                mBinding.matchHeaderInfo.tvMatchTimer.setText(headerText);
                mBinding.matchHeaderInfo.tvMatchTimer.setTextColor(getResources().getColor(R.color.pink));

            }

        }

        if (contest.getIs_free() == 1) {
            mBinding.tvIsFree.setVisibility(View.VISIBLE);
            mBinding.tvIsFree.setPaintFlags(mBinding.tvIsFree.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mBinding.tvIsFree.setText("₹" + contest.getDis_price());
            mBinding.btnJoin.setText("FREE");
//            mBinding.btnJoin.setText(contest.showJoinAmount());
        } else {
            mBinding.tvIsFree.setVisibility(View.GONE);
            mBinding.btnJoin.setText("₹" + contest.getEntryfee());
        }


        left = (contest.getMaximumUser()) - (contest.getJoinedusers());
        if (contest.getChallenge_type().equals("percentage")) {
            mBinding.txtStartValue.setText(contest.getJoinedusers() + " teams already entered");
            mBinding.txtEndValue.setText("");
            mBinding.progressBar.setMax(16);
            mBinding.progressBar.setProgress(8);

            if (contest.getWinning_percentage() != null) {
                if (!contest.getWinning_percentage().equals("") && !contest.getWinning_percentage().equals("0")) {
                    mBinding.tvTotalWinners.setText(contest.getWinning_percentage() + "% Win");
                }
            }


        } else {

            mBinding.progressBar.setMax(contest.getMaximumUser());
            mBinding.progressBar.setProgress(contest.getJoinedusers());

            if (left != 0)
                mBinding.txtStartValue.setText("" + left + " Spots left");
            else
                mBinding.txtStartValue.setText("Contest Closed");

            mBinding.txtEndValue.setText(contest.getMaximumUser() + " Spots");

            mBinding.tvTotalWinners.setText(contest.getTotalwinners() + " Team Win");

        }

        if (contest.isIsjoined()) {
            if (contest.getMultiEntry() == 1){
                if (contest.getUser_joined_count()<contest.getMaxTeamLimit()){
                    mBinding.btnJoinContest.setText("JOIN WITH MORE TEAMS");
                }else {
                    mBinding.btnJoinContest.setText("INVITE FRIENDS TO CONTEST");
                }
            } else
                mBinding.btnJoinContest.setText("INVITE FRIENDS TO CONTEST");

        } else {
            mBinding.btnJoinContest.setText("JOIN CONTEST NOW");
        }

        if (headerText.equalsIgnoreCase("In Progress") || headerText.equalsIgnoreCase("Winner Declared")||left==0) {
            mBinding.btnJoinContest.setVisibility(View.GONE);
        } else {
            mBinding.btnJoinContest.setVisibility(View.VISIBLE);
        }

        if (MyApplication.isFromLiveFinished) {
            isFromLive = true;
            mBinding.btnJoinContest.setVisibility(View.GONE);
//            mBinding.cardContent.setVisibility(View.GONE);
            MyApplication.isFromLiveFinished = false;
        }
        mBinding.btnJoinContest.setOnClickListener(view -> {
            clickContestJoin();
        });

        mBinding.btnJoin.setOnClickListener(view -> {
            if (mBinding.btnJoinContest.getVisibility() == View.VISIBLE) {
                clickContestJoin();
            }
        });

        mTabAdapter = new TabAdapter(getSupportFragmentManager());
        mTabAdapter.addFragment(WinningBreakUpFragment.newInstance(matchKey, contest.getId() + "", contest.getWinAmount() + "", contest.getWinning_percentage(), sportKey), "Winning Breakup");
        mTabAdapter.addFragment(LeaderBoardFragment.newInstance(isFromLive, matchKey, contest.getId() + "", isShowTimer, isForContestDetails, contest.getPdf(), sportKey), "Leaderboard");
//        if (isFromLive) {
//            mTabAdapter.addFragment(ContestStatsFragment.newInstance(matchKey, contest.getId() + "",sportKey), "Contest Stats");
//        }
        mBinding.viewPager.setAdapter(mTabAdapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager);
        if (isFromLive) mBinding.viewPager.setCurrentItem(1);

        /*View root = mBinding.tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(Color.parseColor("#fffda6"));
            drawable.setSize(2, root.getHeight());
            //  ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }*/
        mBinding.tagC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,contest.getMaxTeamLimit(),contest.isShowCTag(),contest.isShowMTag(),contest.isShowWDTag(), contest.isShowBTag(), contest.getBonusPercent());
            }
        });
        mBinding.tagM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,contest.getMaxTeamLimit(),contest.isShowCTag(),contest.isShowMTag(),contest.isShowWDTag(), contest.isShowBTag(), contest.getBonusPercent());
            }
        });
        mBinding.tagWd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,contest.getMaxTeamLimit(),contest.isShowCTag(),contest.isShowMTag(),contest.isShowWDTag(), contest.isShowBTag(), contest.getBonusPercent());
            }
        });

        mBinding.tagB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.showContestInformation(context,contest.getMaxTeamLimit(),contest.isShowCTag(),contest.isShowMTag(),contest.isShowWDTag(), contest.isShowBTag(), contest.getBonusPercent());
            }
        });
    }

    void clickContestJoin() {
        if (mBinding.btnJoinContest.getText().toString().trim().equalsIgnoreCase("JOIN CONTEST NOW") || mBinding.btnJoinContest.getText().toString().trim().equalsIgnoreCase("JOIN WITH MORE TEAMS")) {
            if (teamCount > 0) {
                Intent intent = new Intent(UpComingContestDetailActivity.this, MyTeamsActivity.class);
                intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
                intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
                intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
                intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
                intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
                intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
                intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
                intent.putExtra("isBonous", contest.isShowBTag());
                intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
                intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, isShowTimer);
                intent.putExtra(Constants.SPORT_KEY, sportKey);
                startActivity(intent);
            } else {
                creteTeam();
            }
        } else {
            openShareIntent();
        }
    }

    public void creteTeam() {

        listener = this;
        Intent intent;
        if (sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL)) {
            intent = new Intent(UpComingContestDetailActivity.this, FootballCreateTeamActivity.class);
        } else if (sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(UpComingContestDetailActivity.this, BasketBallCreateTeamActivity.class);
        } else {
            intent = new Intent(UpComingContestDetailActivity.this, CreateTeamActivity.class);
        }
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, true);
        intent.putExtra(Constants.SPORT_KEY, sportKey);
        intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount);
        intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
        startActivityForResult(intent, CREATE_TEAM_CONTEST_DETAIL);
    }


    public void openShareIntent() {
        String shareBody =
                "Hi, Inviting you to join " + Constants.APP_NAME + " and play fantasy sports to win cash daily.\n" +
                        "Use Contest code - " + contestReferCode +
                        "\nTo join this Exclusive invite-only contest on " + Constants.APP_NAME + " for the " + teamVsName + " match, Download App from~ " + MyApplication.apk_url;
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wallet, menu);
        menuTemp = menu;
        AppUtils.setWalletBalance(menu, getResources().getColor(R.color.pink), false, isLeaderboard, seriesId);
        //  AppUtils.loadImage((ImageView) menu.findItem(R.id.iv_profile).getActionView(), MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_PIC));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;
        } else if (itemId == R.id.navigation_wallet) {
            openWalletActivity();
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openNotificationActivity() {
        startActivity(new Intent(UpComingContestDetailActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(UpComingContestDetailActivity.this, MyWalletActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (TextUtils.isEmpty(contestReferCode) && !TextUtils.isEmpty(MyApplication.referCode)) {
            contestReferCode = MyApplication.referCode;
        }

        if (MyApplication.teamJoined){
            contest.setIsjoined(true);
            contest.setUser_joined_count(MyApplication.teamJoinedCount);
            if (!contest.getChallenge_type().equals("percentage")) {
                if (left != 0) {
                    left = left - MyApplication.teamJoinedCount;
                    mBinding.txtStartValue.setText("" + left + " Spots left");
                    mBinding.progressBar.setProgress(contest.getMaximumUser() - left);
                } else {
                    mBinding.txtStartValue.setText("Contest Closed");
                    mBinding.progressBar.setProgress(contest.getMaximumUser());
                }
            }
            MyApplication.teamJoined = false;
        }
        if (contest.isIsjoined()) {
            if (contest.getMultiEntry() == 1){
                if (contest.getUser_joined_count()<contest.getMaxTeamLimit()){
                    mBinding.btnJoinContest.setText("JOIN WITH MORE TEAMS");
                }else {
                    mBinding.btnJoinContest.setText("INVITE FRIENDS TO CONTEST");
                }
            } else
                mBinding.btnJoinContest.setText("INVITE FRIENDS TO CONTEST");

        } else {
            mBinding.btnJoinContest.setText("JOIN CONTEST NOW");
        }
        if (menuTemp != null) {
            AppUtils.setWalletBalance(menuTemp, getResources().getColor(R.color.pink), false, isLeaderboard, seriesId);
        }
    }

    private void showTimer() {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                    mBinding.matchHeaderInfo.tvMatchTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.matchHeaderInfo.tvMatchTimer.setText("00h 00m 00s");
                    startActivity(new Intent(UpComingContestDetailActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).putExtra("matchFinish",true));                }

            };
            countDownTimer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openPlayerPointActivityForLeatherBoard(boolean isForLeaderBoard, int teamId, int challengeId, String teamName, String points, String sportKey) {
        Intent intent;
        if (sportKey.equalsIgnoreCase(Constants.TAG_FOOTBALL))
            intent = new Intent(context, FootballTeamPreviewPointActivity.class);
        else if (sportKey.equalsIgnoreCase(Constants.TAG_BASKETBALL)) {
            intent = new Intent(context, BasketBallTeamPreviewPointActivity.class);
        } else {
            intent = new Intent(context, TeamPreviewPointActivity.class);
        }
        intent.putExtra("teamId", teamId);
        intent.putExtra("challengeId", challengeId);
        intent.putExtra("isForLeaderBoard", isForLeaderBoard);
        intent.putExtra(Constants.KEY_TEAM_NAME, teamName);
        intent.putExtra("tPoints", points);
        intent.putExtra(Constants.SPORT_KEY, sportKey);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        startActivity(intent);
    }

    @Override
    public void getTeamCreated(boolean team_created) {
        if (team_created) {
            teamCount++;
          /*  Intent intent = new Intent(UpComingContestDetailActivity.this, MyTeamsActivity.class);
            intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
            intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
            intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
            intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
            intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
            intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
            intent.putExtra(Constants.KEY_TEAM_COUNT, teamCount++);
            intent.putExtra("isBonous", contest.isShowBTag());
            intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
            intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, isShowTimer);
            startActivity(intent);*/
        }
    }

    public class TabAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        TabAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }

    public void switchTeam(String joinedSwitchTeamId, int teamid) {
        Intent intent = new Intent(UpComingContestDetailActivity.this, MyTeamsActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        intent.putExtra(Constants.KEY_IS_FOR_JOIN_CONTEST, true);
        intent.putExtra(Constants.KEY_CONTEST_DATA, contest);
        intent.putExtra(Constants.KEY_STATUS_HEADER_TEXT, headerText);
        intent.putExtra(Constants.KEY_STATUS_IS_TIMER_HEADER, isShowTimer);
        intent.putExtra("isForJoinedId", joinedSwitchTeamId);
        intent.putExtra("team_id", teamid+"");
        intent.putExtra(Constants.KEY_IS_FOR_SWITCH_TEAM, true);
        intent.putExtra(Constants.SPORT_KEY, sportKey);
        startActivity(intent);
    }


}
