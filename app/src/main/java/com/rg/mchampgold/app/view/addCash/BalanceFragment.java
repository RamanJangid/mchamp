package com.rg.mchampgold.app.view.addCash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.MyBalanceResponse;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.AddBalanceActivity;
import com.rg.mchampgold.app.view.activity.InvestmentActivity;
import com.rg.mchampgold.app.view.activity.InviteFriendActivity;
import com.rg.mchampgold.app.view.activity.WithdrawCashActivity;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentBalanceBinding;

import javax.inject.Inject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Response;


public class BalanceFragment extends Fragment {

    private FragmentBalanceBinding fragmentBalanceBinding;
    private MyBalanceResultItem myBalanceResultItem;

    @Inject
    OAuthRestService oAuthRestService;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBalanceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_balance, container, false);
        fragmentBalanceBinding.addCashBtn.setOnClickListener(view -> {

//            if(isNotVerified()) {
//                showNotVerifiedAlert();
//            } else {
//            }
            startActivity(new Intent(getActivity(), AddBalanceActivity.class));
        });

        fragmentBalanceBinding.withdrawBtn.setOnClickListener(view -> {
            if (isNotVerified()) {
                showNotVerifiedAlert();
            } else {
                startActivity(new Intent(getActivity(), WithdrawCashActivity.class));
            }
        });

        fragmentBalanceBinding.llInvestment.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), InvestmentActivity.class));
        });

        fragmentBalanceBinding.llRefer.setOnClickListener(v -> startActivity(new Intent(MyApplication.appContext, InviteFriendActivity.class)));

        return fragmentBalanceBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.getAppComponent().inject(this);
        setHasOptionsMenu(true);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        getUserBalance();
    }

    private boolean isNotVerified() {
        /*if((MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS,0)==1)
                &&(MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS,0)==1)
                &&(MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,0)==1)
                &&(MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS,0)==1))
            return false;
        else
            return true;*/

        if ((MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0) == 1) ||
                (MyApplication.tinyDB.getInt(Constants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 0) == 1))
            return false;
        else
            return true;
    }


    private void getUserBalance() {
        fragmentBalanceBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<MyBalanceResponse> myBalanceResponseCustomCall = oAuthRestService.getUserBalance(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<MyBalanceResponse>() {
            @Override
            public void success(Response<MyBalanceResponse> response) {
                fragmentBalanceBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    MyBalanceResponse myBalanceResponse = response.body();
                    if (myBalanceResponse.getStatus() == 1 && myBalanceResponse.getResult().size() > 0) {
                        myBalanceResultItem = myBalanceResponse.getResult().get(0);
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, myBalanceResultItem.getBalance() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_WINING_AMOUNT, myBalanceResultItem.getWinning() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BONUS_BALANCE, myBalanceResultItem.getBonus() + "");
                        setUserBalance();
                    } else {
                        AppUtils.showErrorr((AppCompatActivity) getActivity(), myBalanceResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentBalanceBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

    private void setUserBalance() {
        fragmentBalanceBinding.unutilizedTxt.setText("₹" + myBalanceResultItem.getBalance());
        fragmentBalanceBinding.winningsTxt.setText("₹" + myBalanceResultItem.getWinning());
        fragmentBalanceBinding.cashBonusTxt.setText("₹" + myBalanceResultItem.getBonus());
        fragmentBalanceBinding.totalBalanceTxt.setText("₹" + myBalanceResultItem.getTotal());
    }

    private void showNotVerifiedAlert() {
        new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Oops...")
                .setContentText("First Verify your account")
                .show();
    }
}