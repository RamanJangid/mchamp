package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Team {
	@SerializedName("players")
	private ArrayList<Player> players;

	@SerializedName("teamid")
	private int teamid;

	@SerializedName("matchkey")
	private String matchkey;


	@SerializedName("image")
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@SerializedName("user_id")
	private int userid;

	@SerializedName("teamnumber")
	private int teamnumber;

	@SerializedName("is_joined")
	private int isJoined;

	private boolean isSelectedTrue;

	@SerializedName("sport_key")
	private String sport_key ="";


	boolean isSelected;
	public void setPlayers(ArrayList<Player> players){
		this.players = players;
	}

	public ArrayList<Player> getPlayers(){
		return players;
	}

	public void setTeamid(int teamid){
		this.teamid = teamid;
	}

	public int getTeamid(){
		return teamid;
	}

	public void setMatchkey(String matchkey){
		this.matchkey = matchkey;
	}

	public String getMatchkey(){
		return matchkey;
	}

	public void setUserid(int userid){
		this.userid = userid;
	}

	public int getUserid(){
		return userid;
	}

	public void setTeamnumber(int teamnumber){
		this.teamnumber = teamnumber;
	}

	public int getTeamnumber(){
		return teamnumber;
	}

	@Override
	public String toString(){
		return
				"Player{" +
						"players = '" + players + '\'' +
						",teamid = '" + teamid + '\'' +
						",matchkey = '" + matchkey + '\'' +
						",userid = '" + userid + '\'' +
						",teamnumber = '" + teamnumber + '\'' +
						"}";
	}

	public String showTeamName() {
		return "Team "+teamnumber;
	}

	public String captainName() {
		String captionName ="";
		for (Player player:players) {
			if(player.getCaptain() ==1) {
                captionName = player.getName();
				break;
			}

		}
		return captionName;
	}

	public String vcCaptainName() {
		String vcCaptionName ="";
		for (Player player:players) {
			if(player.getVicecaptain() ==1) {
				vcCaptionName = player.getName();
				break;
			}

		}
		return vcCaptionName;
	}




	public String captainImage() {
		String captionImage ="";
		for (Player player:players) {
			if(player.getCaptain() ==1) {
				captionImage = player.getImage();
				break;
			}

		}
		return captionImage;
	}

	public String vcCaptainImage() {
		String vcCaptionImage ="";
		for (Player player:players) {
			if(player.getVicecaptain() ==1) {
				vcCaptionImage = player.getImage();
				break;
			}

		}
		return vcCaptionImage;
	}


	public String getSport_key() {
		return sport_key ==null?"":sport_key;
	}

	public void setSport_key(String sport_key) {
		this.sport_key = sport_key;
	}

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	public int getIsJoined() {
		return isJoined;
	}

	public void setIsJoined(int isJoined) {
		this.isJoined = isJoined;
	}

	public boolean isSelectedTrue() {
		return isSelectedTrue;
	}

	public void setSelectedTrue(boolean selectedTrue) {
		isSelectedTrue = selectedTrue;
	}
}