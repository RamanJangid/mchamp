package com.rg.mchampgold.app.api.service;



import com.rg.mchampgold.app.api.request.LoginRequest;
import com.rg.mchampgold.app.api.response.LoginResponse;
import com.rg.mchampgold.common.api.CustomCallAdapter;

import retrofit2.http.Body;
import retrofit2.http.POST;


public interface ClientRestService {

    @POST("api/auth/login")
    CustomCallAdapter.CustomCall<LoginResponse> userLogin(@Body LoginRequest loginRequest);
}
