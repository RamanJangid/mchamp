package com.rg.mchampgold.app.api;


import com.rg.mchampgold.app.di.module.AppModule;
import com.rg.mchampgold.common.api.RestHelper;

public class MyRestHelper extends RestHelper {

    public MyRestHelper(AppModule appModule) {
        super(appModule);
    }

    @Override
    public void logoutUser() {


    }

    @Override
    public void onErrorWhileRefreshingUserToken() {

    }
}
