package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import com.google.gson.annotations.Expose;

public class AddCashBannerListResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("result")
    @Expose
    private Result result;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Result getResult() {
        return result;
    }


    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("add_cash_banner_url")
        @Expose
        private String addCashBannerUrl;
        @SerializedName("0")
        @Expose
        private _0 _0;

        public String getAddCashBannerUrl() {
            return addCashBannerUrl;
        }

        public void setAddCashBannerUrl(String addCashBannerUrl) {
            this.addCashBannerUrl = addCashBannerUrl;
        }

        public _0 get0() {
            return _0;
        }

        public void set0(_0 _0) {
            this._0 = _0;
        }

    }

    public class _0 {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("link")
        @Expose
        private String link;
        @SerializedName("image")
        @Expose
        private String image;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

    }


}