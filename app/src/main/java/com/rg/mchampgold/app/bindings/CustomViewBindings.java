package com.rg.mchampgold.app.bindings;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;

import kotlin.jvm.JvmStatic;

public class CustomViewBindings {

    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("buttonBackground")
    public static void setCheckboxBackground(CheckBox checkBox, boolean isChecked) {
        if (isChecked) {
            Drawable top = MyApplication.appContext.getResources().getDrawable(R.drawable.ic_remove_player);
            checkBox.setCompoundDrawablesWithIntrinsicBounds(top, null, null, null);
            //checkBox.setButtonDrawable(R.drawable.checked);
        } else {
            Drawable top = MyApplication.appContext.getResources().getDrawable(R.drawable.ic_add_player);
            checkBox.setCompoundDrawablesWithIntrinsicBounds(top, null, null, null);
        }
    }

    @BindingAdapter("strikeThrough")
    @JvmStatic
    public static void strikeThrough(TextView view, Boolean show) {
        if (show) {
            view.setPaintFlags(view.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }
}
