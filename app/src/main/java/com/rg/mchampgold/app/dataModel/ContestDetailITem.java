package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ContestDetailITem {

	@SerializedName("contest")
	private ArrayList<JoinedContestTeam> value;

	public void setValue(ArrayList<JoinedContestTeam> value){
		this.value = value;
	}

	public ArrayList<JoinedContestTeam> getValue(){
		return value == null?new ArrayList<JoinedContestTeam>():value;
	}

	@Override
	public String toString(){
		return
				"ContestDetailITem{" +
						"value = '" + value + '\'' +
						"}";
	}
}