package com.rg.mchampgold.app.viewModel;


import androidx.arch.core.util.Function;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.dataModel.BalanceResponse;
import com.rg.mchampgold.app.dataModel.JoinContestResponse;
import com.rg.mchampgold.app.dataModel.MyTeamResponse;
import com.rg.mchampgold.app.repository.MatchRepository;
import com.rg.mchampgold.common.api.Resource;

import javax.inject.Inject;


public class TeamViewModel extends ViewModel {

    private MatchRepository mRepository;
    
    private final MutableLiveData<MyTeamRequest> teamRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<MyTeamResponse>> contestLiveData = Transformations.switchMap(teamRequestMutableLiveData, new Function<MyTeamRequest, LiveData<Resource<MyTeamResponse>>>() {
        @Override
        public LiveData<Resource<MyTeamResponse>> apply(final MyTeamRequest input) {
            LiveData<Resource<MyTeamResponse>> resourceLiveData = mRepository.getTeams(input);
            final MediatorLiveData<Resource<MyTeamResponse>> mediator = new MediatorLiveData<Resource<MyTeamResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<MyTeamResponse>>() {
                @Override
                public void onChanged(Resource<MyTeamResponse> arrayListResource) {
                    MyTeamResponse resp = arrayListResource.getData();
                    Resource<MyTeamResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    private final MutableLiveData<JoinContestRequest> getBalanceRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<BalanceResponse>> balanceLiveData = Transformations.switchMap(getBalanceRequestMutableLiveData, new Function<JoinContestRequest, LiveData<Resource<BalanceResponse>>>() {
        @Override
        public LiveData<Resource<BalanceResponse>> apply(final JoinContestRequest input) {
            LiveData<Resource<BalanceResponse>> resourceLiveData = mRepository.getUsableBalance(input);
            final MediatorLiveData<Resource<BalanceResponse>> mediator = new MediatorLiveData<Resource<BalanceResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<BalanceResponse>>() {
                @Override
                public void onChanged(Resource<BalanceResponse> arrayListResource) {
                    BalanceResponse resp = arrayListResource.getData();
                    Resource<BalanceResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    private final MutableLiveData<JoinContestRequest> getJoinContestRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<JoinContestResponse>> joinContestResponseLiveData = Transformations.switchMap(getJoinContestRequestMutableLiveData, new Function<JoinContestRequest, LiveData<Resource<JoinContestResponse>>>() {
        @Override
        public LiveData<Resource<JoinContestResponse>> apply(final JoinContestRequest input) {
            LiveData<Resource<JoinContestResponse>> resourceLiveData = mRepository.joinContest(input);
            final MediatorLiveData<Resource<JoinContestResponse>> mediator = new MediatorLiveData<Resource<JoinContestResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<JoinContestResponse>>() {
                @Override
                public void onChanged(Resource<JoinContestResponse> arrayListResource) {
                    JoinContestResponse resp = arrayListResource.getData();
                    Resource<JoinContestResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    /*
    Get the view model instance.
     */
    public static TeamViewModel create(FragmentActivity activity) {
        TeamViewModel viewModel = ViewModelProviders.of(activity).get(TeamViewModel.class);
        return viewModel;
    }

    public static TeamViewModel create(Fragment fragment) {
        TeamViewModel viewModel = ViewModelProviders.of(fragment).get(TeamViewModel.class);
        return viewModel;
    }

    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<MyTeamResponse>> getContestData() {
        return contestLiveData;
    }

    public void loadBalanceRequest(JoinContestRequest joinContestRequest) {
        getBalanceRequestMutableLiveData.setValue(joinContestRequest);
    }

    public LiveData<Resource<BalanceResponse>> getBalanceData() {
        return balanceLiveData;
    }

    public LiveData<Resource<JoinContestResponse>> joinContest() {
        return joinContestResponseLiveData;
    }

    public void loadJoinContestRequest(JoinContestRequest joinContestRequest) {
        getJoinContestRequestMutableLiveData.setValue(joinContestRequest);
    }

    public void loadMyTeamRequest(MyTeamRequest contestRequest) {
        teamRequestMutableLiveData.setValue(contestRequest);
    }



    @Inject
    public void setRepository(MatchRepository repository) {
        this.mRepository = repository;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}