package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class JoinedContestResponse{

	@SerializedName("result")
	private	JoinedContestItem joinedContestItem;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;



	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"JoinedContestResponse{" + 
			",message = '" + message + '\'' +
			",status = '" + status + '\'' + 
			"}";
		}

	public JoinedContestItem getJoinedContestItem() {
		return joinedContestItem == null?new JoinedContestItem():joinedContestItem;
	}

	public void setJoinedContestItem(JoinedContestItem joinedContestItem) {
		this.joinedContestItem = joinedContestItem;
	}
}