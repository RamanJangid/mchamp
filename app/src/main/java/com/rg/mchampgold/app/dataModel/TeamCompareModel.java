package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TeamCompareModel {

    @SerializedName("team1_name")
    String team1_name;
    @SerializedName("team2_name")
    String team2_name;
    @SerializedName("team1_image")
    String team1_image;
    @SerializedName("team2_image")
    String team2_image;
    @SerializedName("team1_points")
    String team1_points;
    @SerializedName("team2_points")
    String team2_points;
    @SerializedName("team1_rank")
    String team1_rank;
    @SerializedName("team2_rank")
    String team2_rank;
    @SerializedName("diff_points")
    String diff_points;
    @SerializedName("diff_text")
    String diff_text;
    @SerializedName("cvc_diff_text")
    String cvc_diff_text;
    @SerializedName("cvc_diff_points")
    String cvc_diff_points;
    @SerializedName("others_diff_text")
    String others_diff_text;
    @SerializedName("others_diff_points")
    String others_diff_points;
    @SerializedName("common_diff_text")
    String common_diff_text;
    @SerializedName("common_diff_points")
    String common_diff_points;

    @SerializedName("compare_c_vc")
    TeamCompareCVCModel compare_c_vc;

    @SerializedName("common_players")
    ArrayList<CommonPlayerListModel> common_players;

    @SerializedName("other_players")
    ArrayList<ArrayList<OtherPlayerListModel>> other_players;

    public String getTeam1_name() {
        return team1_name;
    }

    public void setTeam1_name(String team1_name) {
        this.team1_name = team1_name;
    }

    public String getTeam2_name() {
        return team2_name;
    }

    public void setTeam2_name(String team2_name) {
        this.team2_name = team2_name;
    }

    public String getTeam1_image() {
        return team1_image;
    }

    public void setTeam1_image(String team1_image) {
        this.team1_image = team1_image;
    }

    public String getTeam2_image() {
        return team2_image;
    }

    public void setTeam2_image(String team2_image) {
        this.team2_image = team2_image;
    }

    public String getTeam1_points() {
        return team1_points.split("\\.")[0];
    }

    public void setTeam1_points(String team1_points) {
        this.team1_points = team1_points;
    }

    public String getTeam2_points() {
        return team2_points.split("\\.")[0];
    }

    public void setTeam2_points(String team2_points) {
        this.team2_points = team2_points;
    }

    public String getDiff_points() {
        return diff_points;
    }

    public void setDiff_points(String diff_points) {
        this.diff_points = diff_points;
    }

    public String getDiff_text() {
        return diff_text;
    }

    public void setDiff_text(String diff_text) {
        this.diff_text = diff_text;
    }

    public TeamCompareCVCModel getCompare_c_vc() {
        return compare_c_vc;
    }

    public void setCompare_c_vc(TeamCompareCVCModel compare_c_vc) {
        this.compare_c_vc = compare_c_vc;
    }

    public ArrayList<CommonPlayerListModel> getCommon_players() {
        return common_players;
    }

    public void setCommon_players(ArrayList<CommonPlayerListModel> common_players) {
        this.common_players = common_players;
    }

    public ArrayList<ArrayList<OtherPlayerListModel>> getOther_players() {
        return other_players;
    }

    public void setOther_players(ArrayList<ArrayList<OtherPlayerListModel>> other_players) {
        this.other_players = other_players;
    }

    public String getTeam1_rank() {
        return team1_rank;
    }

    public void setTeam1_rank(String team1_rank) {
        this.team1_rank = team1_rank;
    }

    public String getTeam2_rank() {
        return team2_rank;
    }

    public void setTeam2_rank(String team2_rank) {
        this.team2_rank = team2_rank;
    }

    public String getCvc_diff_text() {
        return cvc_diff_text;
    }

    public void setCvc_diff_text(String cvc_diff_text) {
        this.cvc_diff_text = cvc_diff_text;
    }

    public String getCvc_diff_points() {
        return cvc_diff_points;
    }

    public void setCvc_diff_points(String cvc_diff_points) {
        this.cvc_diff_points = cvc_diff_points;
    }

    public String getOthers_diff_text() {
        return others_diff_text;
    }

    public void setOthers_diff_text(String others_diff_text) {
        this.others_diff_text = others_diff_text;
    }

    public String getOthers_diff_points() {
        return others_diff_points;
    }

    public void setOthers_diff_points(String others_diff_points) {
        this.others_diff_points = others_diff_points;
    }

    public String getCommon_diff_text() {
        return common_diff_text;
    }

    public void setCommon_diff_text(String common_diff_text) {
        this.common_diff_text = common_diff_text;
    }

    public String getCommon_diff_points() {
        return common_diff_points;
    }

    public void setCommon_diff_points(String common_diff_points) {
        this.common_diff_points = common_diff_points;
    }
}
