package com.rg.mchampgold.app.viewModel;


import androidx.arch.core.util.Function;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.dataModel.PlayerListResponse;
import com.rg.mchampgold.app.repository.MatchRepository;
import com.rg.mchampgold.common.api.Resource;

import javax.inject.Inject;


public class GetPlayerDataViewModel extends ViewModel {

    private MatchRepository mRepository;
    
    private final MutableLiveData<MyTeamRequest> teamRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<PlayerListResponse>> contestLiveData = Transformations.switchMap(teamRequestMutableLiveData, new Function<MyTeamRequest, LiveData<Resource<PlayerListResponse>>>() {
        @Override
        public LiveData<Resource<PlayerListResponse>> apply(final MyTeamRequest input) {
            LiveData<Resource<PlayerListResponse>> resourceLiveData = mRepository.getPlayerList(input);
            final MediatorLiveData<Resource<PlayerListResponse>> mediator = new MediatorLiveData<Resource<PlayerListResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<PlayerListResponse>>() {
                @Override
                public void onChanged(Resource<PlayerListResponse> arrayListResource) {
                    PlayerListResponse resp = arrayListResource.getData();
                    Resource<PlayerListResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    /*
    Get the view model instance.
     */
    public static GetPlayerDataViewModel create(FragmentActivity activity) {
        GetPlayerDataViewModel viewModel = ViewModelProviders.of(activity).get(GetPlayerDataViewModel.class);
        return viewModel;
    }


    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<PlayerListResponse>> getPlayerList() {
        return contestLiveData;
    }

    public void loadPlayerListRequest(MyTeamRequest contestRequest) {
        teamRequestMutableLiveData.setValue(contestRequest);
    }


    @Inject
    public void setRepository(MatchRepository repository) {
        this.mRepository = repository;
    }

    

    @Override
    protected void onCleared() {
        super.onCleared();
    }
}