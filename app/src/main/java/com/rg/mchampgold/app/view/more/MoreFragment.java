package com.rg.mchampgold.app.view.more;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.MoreInfoData;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.ContactusActivity;
import com.rg.mchampgold.app.view.activity.InviteFriendActivity;
import com.rg.mchampgold.app.view.activity.InvitedFriendActivity;
import com.rg.mchampgold.app.view.activity.LeaderboardActivity;
import com.rg.mchampgold.app.view.activity.LoginActivity;
import com.rg.mchampgold.app.view.activity.PersonalDetailsActivity;
import com.rg.mchampgold.app.view.activity.VerifyAccountActivity;
import com.rg.mchampgold.app.view.adapter.MoreItemAdapter;
import com.rg.mchampgold.app.view.interfaces.OnMoreItemClickListener;
import com.rg.mchampgold.databinding.FragmentMoreBinding;

import java.util.ArrayList;
import java.util.List;


public class MoreFragment extends Fragment implements OnMoreItemClickListener {

    private FragmentMoreBinding fragmentMoreBinding;
    private MoreItemAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMoreBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false);
        return fragmentMoreBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        menu.findItem(R.id.navigation_notification).setVisible(false);
        menu.findItem(R.id.navigation_wallet).setVisible(false);
        menu.findItem(R.id.navigation_award).setVisible(false);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        String[] stringTitleArray = getResources().getStringArray(R.array.more_menu_title_array);
        //  int[] imgs =    getResources().obtainTypedArray(R.array.more_menu_icon_array);
        int[] imgs = new int[]{
//                R.drawable.ic_user_avt,
//                R.drawable.ic_more_refer_earn,
                R.drawable.verified_protection,
//                R.drawable.ic_leaderboard_more,
                //  R.drawable.surface_1,
                R.drawable.ic_more_fantasy_point,
//                R.drawable.ic_more_refer_list,
                R.drawable.ic_privacy_policy,
                R.drawable.docs,
//                R.drawable.bonus,
//                R.drawable.information,
                R.drawable.find,
//                R.drawable.docs,
//                R.drawable.call,
//                R.drawable.ic_promoters,
//                R.drawable.logout,
//                R.drawable.logout,
        };


        List<MoreInfoData> moreInfoDataList = new ArrayList<>();
        for (int i = 0; i < stringTitleArray.length; i++) {
            MoreInfoData moreInfoData = new MoreInfoData();
            moreInfoData.setName(stringTitleArray[i]);
            moreInfoData.setResourceId(imgs[i]);
            moreInfoDataList.add(moreInfoData);
        }


        mAdapter = new MoreItemAdapter(moreInfoDataList, this, false);
        fragmentMoreBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        fragmentMoreBinding.recyclerView.setLayoutManager(mLayoutManager);

        // adding custom divider line with padding 3dp
//        fragmentMoreBinding.recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL, 1));
        fragmentMoreBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());


        fragmentMoreBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onMoreItemClick(int position, String title) {
//        switch (position) {
//            case 0:
//                startActivity(new Intent(MyApplication.appContext, PersonalDetailsActivity.class));
//                break;
//
//            case 1:
//                startActivity(new Intent(MyApplication.appContext, InviteFriendActivity.class));
//                break;
//
//            case 2:
//                startActivity(new Intent(MyApplication.appContext, VerifyAccountActivity.class));
//                break;
//
//          /*  case 3:
//                startActivity(new Intent(MyApplication.appContext, ScratchCardHistoryActivity.class));
//                break;*/
//
//            case 3:
//                AppUtils.openWebViewActivity(title, MyApplication.fantasy_point_url);
//                break;
//            case 4:
//                Intent intent = new Intent(getActivity(), InvitedFriendActivity.class);
//                startActivity(intent);
//                break;
//            case 5:
//                AppUtils.openWebViewActivity(title, MyApplication.privacy_url);
//                break;
//
//            case 6:
//                AppUtils.openWebViewActivity(title, MyApplication.terms_url);
//                break;
//
//            case 7:
//                AppUtils.openWebViewActivity(title, MyApplication.about_us_url);
//                break;
//
//            case 8:
//                AppUtils.openWebViewActivity(title, MyApplication.how_to_play_url);
//                break;
//           /* case 9:
//                AppUtils.openWebViewActivity(title, MyApplication.legality_url);
//                break;*/
//            case 9:
//                startActivity(new Intent(MyApplication.appContext, ContactusActivity.class));
//            break;
//
//           /* case 11:
//                startActivity(new Intent(MyApplication.appContext, PromotersActivity.class));
//            break;*/
//
//            case 10: {
//                showLogoutDialog();
//            }
//            break;
//
//            default:
//                break;
//
//        }
        if (title.equalsIgnoreCase(getString(R.string.profile))) {
            startActivity(new Intent(MyApplication.appContext, PersonalDetailsActivity.class));
        } else if (title.equalsIgnoreCase(getString(R.string.refer_and_earn_title))) {
            startActivity(new Intent(MyApplication.appContext, InviteFriendActivity.class));
        } else if (title.equalsIgnoreCase(getString(R.string.verify_account))) {
            startActivity(new Intent(MyApplication.appContext, VerifyAccountActivity.class));
        } else if (title.equalsIgnoreCase(getString(R.string.fantasy_point_system))) {
            AppUtils.openWebViewActivity(title, MyApplication.fantasy_point_url);
        } else if (title.equalsIgnoreCase(getString(R.string.refer_list))) {
            Intent intent = new Intent(getActivity(), InvitedFriendActivity.class);
            startActivity(intent);
        } else if (title.equalsIgnoreCase(getString(R.string.privacy_policy))) {
            AppUtils.openWebViewActivity(title, MyApplication.privacy_url);
        } else if (title.equalsIgnoreCase(getString(R.string.terms_conditions))) {
            AppUtils.openWebViewActivity(title, MyApplication.terms_url);
        } else if (title.equalsIgnoreCase(getString(R.string.bonus_terms_conditions))) {
            AppUtils.openWebViewActivity("", MyApplication.bonus_terms);
        } else if (title.equalsIgnoreCase(getString(R.string.about_us))) {
            AppUtils.openWebViewActivity(title, MyApplication.about_us_url);
        } else if (title.equalsIgnoreCase(getString(R.string.how_to_play))) {
            AppUtils.openWebViewActivity(title, MyApplication.how_to_play_url);
        } else if (title.equalsIgnoreCase(getString(R.string.contact_us))) {
            startActivity(new Intent(MyApplication.appContext, ContactusActivity.class));
        } else if (title.equalsIgnoreCase(getString(R.string.logout))) {
            showLogoutDialog();
        } else if (title.equals(getString(R.string.leaderboard))) {
            startActivity(new Intent(MyApplication.appContext, LeaderboardActivity.class));
        }
    }

    private void showLogoutDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.RoundedCornersDialog);

        // set title
        alertDialogBuilder.setTitle("Logout");
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you want to logout?")
                .setCancelable(false)
                .setPositiveButton("YES", (dialog, id) -> logout())
                .setNegativeButton("NO", (dialog, id) -> dialog.cancel());

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    private void logout() {
        MyApplication.tinyDB.clear();
        Intent i = new Intent(getActivity(), LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        getActivity().finish();

    }
}