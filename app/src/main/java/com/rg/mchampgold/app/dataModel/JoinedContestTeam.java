package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.common.utils.Constants;

@Generated("com.robohorse.robopojogenerator")
public class JoinedContestTeam {

  /*@SerializedName("winingamount")
	private String winingamount;

	@SerializedName("user_id")
	private int userid;

	@SerializedName("joined_count")
	private int joinedCount;

	@SerializedName("challenge_id")
	private int challengeId;

	@SerializedName("team_id")
	private int teamid;

	@SerializedName("userrank")
	private String rank;

	@SerializedName("join_id")
	private int joinId;



	@SerializedName("points")
	private String points;

	@SerializedName("teamname")
	private String teamname;

	@SerializedName("teamnumber")
	private int teamnumber;

	public void setWiningamount(String winingamount){
		this.winingamount = winingamount;
	}

	public String getWiningamount(){
		return winingamount;
	}

	public String getWiningamountShow(){
		return "Winning"+"₹ "+winingamount;
	}

	public void setJoinedCount(int joinedCount){
		this.joinedCount = joinedCount;
	}

	public int getJoinedCount(){
		return joinedCount;
	}

	public void setChallengeId(int challengeId){
		this.challengeId = challengeId;
	}

	public int getChallengeId(){
		return challengeId;
	}

	public void setTeamid(int teamid){
		this.teamid = teamid;
	}

	public int getTeamid(){
		return teamid;
	}

	public void setRank(String rank){
		this.rank = rank;
	}

	public String getRank(){
		return rank;
	}

	public void setJoinId(int joinId){
		this.joinId = joinId;
	}

	public int getJoinId(){
		return joinId;
	}

	public void setUserid(int userid){
		this.userid = userid;
	}

	public int getUserid(){
		return userid;
	}

	public void setPoints(String points){
		this.points = points;
	}

	public String getPoints(){
		return points;
	}

	public void setTeamname(String teamname){
		this.teamname = teamname;
	}

	public String getTeamname(){
		return teamname;
	}

	public void setTeamnumber(int teamnumber){
		this.teamnumber = teamnumber;
	}

	public int getTeamnumber(){
		return teamnumber;
	}
*/


    @SerializedName("userrank")
    private String rank;

    @SerializedName("totalwinners")
    private int totalwinners;

    @SerializedName("entryfee")
    private int entryfee;

    @SerializedName("team1display")
    private String team1display;

    @SerializedName("getjoinedpercentage")
    private int getjoinedpercentage;

    @SerializedName("joinedusers")
    private int joinedusers;

    @SerializedName("isjoined")
    private boolean isjoined;

    @SerializedName("isselected")
    private boolean isselected;

    @SerializedName("matchkey")
    private String matchkey;

    @SerializedName("multi_entry")
    private String multiEntry;

    @SerializedName("isselectedid")
    private String isselectedid;

    @SerializedName("points")
    private String points;

    @SerializedName("is_running")
    private int isRunning;

    @SerializedName("join_with")
    private int joinWith;

    @SerializedName("confirmed_challenge")
    private int confirmedChallenge;

    @SerializedName("is_bonus")
    private int isBonus;

    @SerializedName("refercode")
    private String refercode;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    @SerializedName("win_amount")
    private int winAmount;

    @SerializedName("team2display")
    private String team2display;

    @SerializedName("maximum_user")
    private int maximumUser;

/*	@SerializedName("win_amount")
	private String winingamount;*/

    @SerializedName("user_id")
    private int userid;


    @SerializedName("user_image")
    private String user_image = "";

    @SerializedName("teamname")
    private String teamname;


    @SerializedName("challenge_id")
    private int challengeId;

    @SerializedName("team_id")
    private int teamid;


    @SerializedName("join_id")
    private int joinId;


    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public int getTotalwinners() {
        return totalwinners;
    }

    public void setTotalwinners(int totalwinners) {
        this.totalwinners = totalwinners;
    }

    public int getEntryfee() {
        return entryfee;
    }

    public void setEntryfee(int entryfee) {
        this.entryfee = entryfee;
    }

    public String getTeam1display() {
        return team1display;
    }

    public void setTeam1display(String team1display) {
        this.team1display = team1display;
    }

    public int getGetjoinedpercentage() {
        return getjoinedpercentage;
    }

    public void setGetjoinedpercentage(int getjoinedpercentage) {
        this.getjoinedpercentage = getjoinedpercentage;
    }

    public int getJoinedusers() {
        return joinedusers;
    }

    public void setJoinedusers(int joinedusers) {
        this.joinedusers = joinedusers;
    }

    public boolean isIsjoined() {
        return isjoined;
    }

    public void setIsjoined(boolean isjoined) {
        this.isjoined = isjoined;
    }

    public boolean isIsselected() {
        return isselected;
    }

    public void setIsselected(boolean isselected) {
        this.isselected = isselected;
    }

    public String getMatchkey() {
        return matchkey;
    }

    public void setMatchkey(String matchkey) {
        this.matchkey = matchkey;
    }

    public String getMultiEntry() {
        return multiEntry;
    }

    public void setMultiEntry(String multiEntry) {
        this.multiEntry = multiEntry;
    }

    public String getIsselectedid() {
        return isselectedid;
    }

    public void setIsselectedid(String isselectedid) {
        this.isselectedid = isselectedid;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public int getIsRunning() {
        return isRunning;
    }

    public void setIsRunning(int isRunning) {
        this.isRunning = isRunning;
    }

    public int getJoinWith() {
        return joinWith;
    }

    public void setJoinWith(int joinWith) {
        this.joinWith = joinWith;
    }

    public int getConfirmedChallenge() {
        return confirmedChallenge;
    }

    public void setConfirmedChallenge(int confirmedChallenge) {
        this.confirmedChallenge = confirmedChallenge;
    }

    public int getIsBonus() {
        return isBonus;
    }

    public void setIsBonus(int isBonus) {
        this.isBonus = isBonus;
    }

    public String getRefercode() {
        return refercode;
    }

    public void setRefercode(String refercode) {
        this.refercode = refercode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWinAmount() {
        return winAmount;
    }

    public void setWinAmount(int winAmount) {
        this.winAmount = winAmount;
    }

    public String getTeam2display() {
        return team2display;
    }

    public void setTeam2display(String team2display) {
        this.team2display = team2display;
    }

    public int getMaximumUser() {
        return maximumUser;
    }

    public void setMaximumUser(int maximumUser) {
        this.maximumUser = maximumUser;
    }

    public boolean isCurrentTeam() {
        if (MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID).equalsIgnoreCase(userid + ""))
            return true;
        else
            return false;
    }

    public String showRank() {
        return "#" + rank;
    }

    public boolean showWiningAmount() {
        if (winAmount != 0)
            return true;
        else
            return false;
    }


    public String getWiningamountShow() {
        return "₹" + winAmount;
    }


    public String getTeamname() {
        return teamname;
    }

    public void setTeamname(String teamname) {
        this.teamname = teamname;
    }


    public void setChallengeId(int challengeId) {
        this.challengeId = challengeId;
    }

    public int getChallengeId() {
        return challengeId;
    }

    public void setTeamid(int teamid) {
        this.teamid = teamid;
    }

    public int getTeamid() {
        return teamid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getJoinId() {
        return joinId;
    }

    public void setJoinId(int joinId) {
        this.joinId = joinId;
    }
}