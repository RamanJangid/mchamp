package com.rg.mchampgold.app.view.adapter;

import com.google.gson.annotations.SerializedName;

public class AddScratchRequest {


    @SerializedName("user_id")
    private String user_id;

    @SerializedName("list_id")
    private Integer list_id;

    @SerializedName("amount")
    private double amount;


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Integer getList_id() {
        return list_id;
    }

    public void setList_id(Integer list_id) {
        this.list_id = list_id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
