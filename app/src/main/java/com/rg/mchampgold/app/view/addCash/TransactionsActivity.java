package com.rg.mchampgold.app.view.addCash;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.dataModel.MyTransactionHistoryResponse;
import com.rg.mchampgold.app.dataModel.TransactionItem;
import com.rg.mchampgold.app.dataModel.TransactionRequest;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.MyDividerItemDecoration;
import com.rg.mchampgold.app.view.adapter.TransactionItemAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityTransactionsBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;


public class TransactionsActivity extends AppCompatActivity {

    private ActivityTransactionsBinding fragmentTransactionsBinding;
    private MyBalanceResultItem myBalanceResultItem;
    TransactionItemAdapter mAdapter;
    ArrayList<TransactionItem> transactionItems = new ArrayList<>();

    @Inject
    OAuthRestService oAuthRestService;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(this);
        fragmentTransactionsBinding = DataBindingUtil.setContentView(this, R.layout.activity_transactions);
        setupRecyclerView();
        setSupportActionBar(fragmentTransactionsBinding.mytoolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView() {
        getTransaction();
        mAdapter = new TransactionItemAdapter(transactionItems, this);
        fragmentTransactionsBinding.rvTransaction.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        fragmentTransactionsBinding.rvTransaction.setLayoutManager(mLayoutManager);
        // adding custom divider line with padding 3dp
        fragmentTransactionsBinding.rvTransaction.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 1));
        fragmentTransactionsBinding.rvTransaction.setItemAnimator(new DefaultItemAnimator());
        fragmentTransactionsBinding.rvTransaction.setAdapter(mAdapter);
    }


    private void getTransaction() {
        fragmentTransactionsBinding.setRefreshing(true);
        TransactionRequest baseRequest = new TransactionRequest();
        baseRequest.setPage(1);
        baseRequest.setUserId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<MyTransactionHistoryResponse> myBalanceResponseCustomCall = oAuthRestService.getMyTransaction(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<MyTransactionHistoryResponse>() {
            @Override
            public void success(Response<MyTransactionHistoryResponse> response) {
                fragmentTransactionsBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    MyTransactionHistoryResponse playingHistoryResponse = response.body();
                    if (playingHistoryResponse.getStatus() == 1 && playingHistoryResponse.getTransactionHistoryItem() != null) {
                        if (playingHistoryResponse.getTransactionHistoryItem().getData().size() > 0) {
                            transactionItems = playingHistoryResponse.getTransactionHistoryItem().getData();
                            fragmentTransactionsBinding.tvNoTransaction.setVisibility(View.GONE);
                            mAdapter.updateData(transactionItems);
                        } else {
                            fragmentTransactionsBinding.tvNoTransaction.setVisibility(View.VISIBLE);
                        }
                    } else {
                        fragmentTransactionsBinding.tvNoTransaction.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                fragmentTransactionsBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }


}