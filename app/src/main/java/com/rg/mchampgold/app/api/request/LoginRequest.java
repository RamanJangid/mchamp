package com.rg.mchampgold.app.api.request;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class LoginRequest{

	@SerializedName("password")
	private String password;

	@SerializedName("email")
	private String email;

    @SerializedName("deviceId")
    private String deviceId;

    @SerializedName("fcmToken")
    private String fcmToken;

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"CheckVersionCodeResponse{" +
			"password = '" + password + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getFcmToken() {
		return fcmToken;
	}

	public void setFcmToken(String fcmToken) {
		this.fcmToken = fcmToken;
	}
}