package com.rg.mchampgold.app.dataModel;

import java.util.ArrayList;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CategoriesItem{

	@SerializedName("contest_sub_text")
	private String contestSubText;

	@SerializedName("contest_type_image")
	private String contestTypeImage;

	@SerializedName("leagues")
	private ArrayList<Contest> leagues;

	@SerializedName("contest_image_url")
	private String contestImageUrl;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("total_category_leagues")
	private int totalCategoryLeagues;

	@SerializedName("sort_order")
	private int sortOrder;

	@SerializedName("status")
	private int status;

	public void setContestSubText(String contestSubText){
		this.contestSubText = contestSubText;
	}

	public String getContestSubText(){
		return contestSubText;
	}

	public void setContestTypeImage(String contestTypeImage){
		this.contestTypeImage = contestTypeImage;
	}

	public String getContestTypeImage(){
		return contestTypeImage;
	}


	public void setContestImageUrl(String contestImageUrl){
		this.contestImageUrl = contestImageUrl;
	}

	public String getContestImageUrl(){
		return contestImageUrl;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTotalCategoryLeagues(int totalCategoryLeagues){
		this.totalCategoryLeagues = totalCategoryLeagues;
	}

	public int getTotalCategoryLeagues(){
		return totalCategoryLeagues;
	}

	public void setSortOrder(int sortOrder){
		this.sortOrder = sortOrder;
	}

	public int getSortOrder(){
		return sortOrder;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"CategoriesItem{" + 
			"contest_sub_text = '" + contestSubText + '\'' + 
			",contest_type_image = '" + contestTypeImage + '\'' + 
			",leagues = '" + leagues + '\'' + 
			",contest_image_url = '" + contestImageUrl + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",total_category_leagues = '" + totalCategoryLeagues + '\'' + 
			",sort_order = '" + sortOrder + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	public ArrayList<Contest> getLeagues() {
		return leagues;
	}

	public void setLeagues(ArrayList<Contest> leagues) {
		this.leagues = leagues;
	}
}