package com.rg.mchampgold.app.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.view.activity.CreateTeamActivity;
import com.rg.mchampgold.app.view.adapter.PlayerItemAdapter;
import com.rg.mchampgold.app.view.basketball.BasketBallCreateTeamActivity;
import com.rg.mchampgold.app.view.football.FootballCreateTeamActivity;
import com.rg.mchampgold.app.view.interfaces.PlayerItemClickListener;
import com.rg.mchampgold.databinding.FragmentPlayerBinding;

import java.util.ArrayList;


public class CreateTeamPlayerFragment extends Fragment implements PlayerItemClickListener {

    private FragmentPlayerBinding fragmentPlayerBinding;
    Context context;
  public  PlayerItemAdapter playerItemAdapter;
    ArrayList<Player> mainPlayerList = new ArrayList<>();
    public ArrayList<Player> playerTypeList = new ArrayList<>();
    public int type;
    private CreateTeamActivity createTeamActivity;

    private Boolean isPointsSorting = false;
    private Boolean isCredits = false;
    private Boolean isSelectedSorting = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentPlayerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_player, container, false);
        return fragmentPlayerBinding.getRoot();
    }

    public CreateTeamPlayerFragment(ArrayList<Player> mainPlayerList, ArrayList<Player> playerTypeList, int type) {
//        CreateTeamPlayerFragment myFragment = new CreateTeamPlayerFragment();
        this.mainPlayerList=mainPlayerList;
        this.playerTypeList=playerTypeList;
        this.type=type;
//        Bundle args = new Bundle();
//        args.putInt("type", type);
//        args.putSerializable("mainList", mainPlayerList);
//        args.putSerializable("typeList", playerTypeList);
//        myFragment.setArguments(args);
//        return myFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mainPlayerList = (ArrayList<Player>) getArguments().getSerializable("mainList");
//            playerTypeList = (ArrayList<Player>) getArguments().getSerializable("typeList");
//            type = getArguments().getInt("type");
//        }
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && playerItemAdapter!=null){
            playerItemAdapter.notifyDataSetChanged();
        }
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isAdded() && getActivity() != null) {
            createTeamActivity = (CreateTeamActivity) getActivity();
        }
        setupRecyclerView();
    }

   /* @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecyclerView();
    }*/





    private void setupRecyclerView() {
        playerItemAdapter = new PlayerItemAdapter(getContext(), mainPlayerList, playerTypeList, this, type);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        fragmentPlayerBinding.recyclerView.setLayoutManager(mLayoutManager);
        fragmentPlayerBinding.recyclerView.setHasFixedSize(true);
        fragmentPlayerBinding.recyclerView.setAdapter(playerItemAdapter);

        /*fragmentPlayerBinding.tvPoints.setOnClickListener(view -> {
            if (playerItemAdapter != null) {
                if (isPointsSorting) {
                    isPointsSorting = false;
                    playerItemAdapter.sortWithPoints(true);
                    createTeamActivity.isPointSorted = true;
                    fragmentPlayerBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    fragmentPlayerBinding.ivPointSortImage.setImageResource(R.drawable.ic_down_sort);
                    //  fragmentPlayerBinding.tvPoints.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_sort, 0);
                } else {
                    isPointsSorting = true;
                    playerItemAdapter.sortWithPoints(false);
                    createTeamActivity.isPointSorted = false;
                    fragmentPlayerBinding.ivPointSortImage.setVisibility(View.VISIBLE);
                    fragmentPlayerBinding.ivPointSortImage.setImageResource(R.drawable.ic_up_sort);
                    //  fragmentPlayerBinding.tvPoints.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_sort, 0);
                }
                fragmentPlayerBinding.ivCreditSortImage.setVisibility(View.INVISIBLE);
            }
        });

        fragmentPlayerBinding.tvCredits.setOnClickListener(view -> {
            if (playerItemAdapter != null) {
                if (isCredits) {
                    isCredits = false;
                    playerItemAdapter.sortWithCredit(true);
                    createTeamActivity.isCreditSorted = true;
                    fragmentPlayerBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    fragmentPlayerBinding.ivCreditSortImage.setImageResource(R.drawable.ic_down_sort);
                    // fragmentPlayerBinding.tvCredits.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_sort, 0);
                } else {
                    isCredits = true;
                    createTeamActivity.isCreditSorted = false;
                    playerItemAdapter.sortWithCredit(false);
                    fragmentPlayerBinding.ivCreditSortImage.setVisibility(View.VISIBLE);
                    fragmentPlayerBinding.ivCreditSortImage.setImageResource(R.drawable.ic_up_sort);
                    // fragmentPlayerBinding.tvCredits.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_sort, 0);
                }
                fragmentPlayerBinding.ivPointSortImage.setVisibility(View.INVISIBLE);
            }
        });*/





       /* fragmentPlayerBinding.tvSelectedPercent.setOnClickListener(view -> {
            if (playerItemAdapter != null) {
                if (isSelectedSorting) {
                    isSelectedSorting = false;
                    playerItemAdapter.sortWithSelectedBy(true);
                    fragmentPlayerBinding.ivSelectedSortImage.setImageResource(R.drawable.ic_down_sort);
                    // fragmentPlayerBinding.tvCredits.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_down_sort, 0);
                } else {
                    isSelectedSorting = true;
                    playerItemAdapter.sortWithSelectedBy(false);
                    fragmentPlayerBinding.ivSelectedSortImage.setImageResource(R.drawable.ic_up_sort);
                    // fragmentPlayerBinding.tvCredits.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_up_sort, 0);
                }
            }
        });*/

    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onPlayerClick(boolean isSelect, int position, int type) {
        if(getActivity()!=null)
            if(getActivity() instanceof FootballCreateTeamActivity) {
                ((FootballCreateTeamActivity)getActivity()).onPlayerClick(isSelect,position,type);
            }
            else if(getActivity() instanceof BasketBallCreateTeamActivity) {
                ((BasketBallCreateTeamActivity)getActivity()).onPlayerClick(isSelect,position,type);
            }
            else {
                ((CreateTeamActivity)getActivity()).onPlayerClick(isSelect,position,type);
            }
    }


    public void refresh(ArrayList<Player> selectedList, int type) {

        playerItemAdapter.updateData(selectedList, type);



        //  playerItemAdapter.notifyDataSetChanged();
     /*   playerItemAdapter = new PlayerItemAdapter(getContext(),mainPlayerList,selectedList, this,type);
        fragmentPlayerBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        fragmentPlayerBinding.recyclerView.setLayoutManager(mLayoutManager);
        fragmentPlayerBinding.recyclerView.setAdapter(playerItemAdapter);*/
    }

    public void changePlayingStatus() {
        playerItemAdapter.notifyDataSetChanged();
    }

    public void sortWithPoints(boolean flag) {
        playerItemAdapter.sortWithPoints(flag);
    }


    public void sortWithCredit(boolean flag) {
        playerItemAdapter.sortWithCredit(flag);
    }

    public void sortByPlayer(boolean flag) {
        playerItemAdapter.sortByPlayer(flag);
    }
}