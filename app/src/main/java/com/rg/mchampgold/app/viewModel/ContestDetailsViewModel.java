package com.rg.mchampgold.app.viewModel;


import androidx.arch.core.util.Function;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.dataModel.ContestDetailResponse;
import com.rg.mchampgold.app.dataModel.JoinedContestResponse;
import com.rg.mchampgold.app.repository.MatchRepository;
import com.rg.mchampgold.common.api.Resource;

import javax.inject.Inject;


public class ContestDetailsViewModel extends ViewModel {

    private MatchRepository mRepository;

    private final MutableLiveData<String> networkInfoObservable = new MutableLiveData<String>();


    private final MutableLiveData<ContestRequest> contestRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<ContestDetailResponse>> contestLiveData = Transformations.switchMap(contestRequestMutableLiveData, new Function<ContestRequest, LiveData<Resource<ContestDetailResponse>>>() {
        @Override
        public LiveData<Resource<ContestDetailResponse>> apply(final ContestRequest input) {
            LiveData<Resource<ContestDetailResponse>> resourceLiveData = mRepository.getContestDetails(input);
            final MediatorLiveData<Resource<ContestDetailResponse>> mediator = new MediatorLiveData<Resource<ContestDetailResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<ContestDetailResponse>>() {
                @Override
                public void onChanged(Resource<ContestDetailResponse> arrayListResource) {
                    ContestDetailResponse resp = arrayListResource.getData();
                    Resource<ContestDetailResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    private final MutableLiveData<JoinContestRequest> joinedContestRequestMutableLiveData = new MutableLiveData<>();
    private LiveData<Resource<JoinedContestResponse>> joinedContestLiveData = Transformations.switchMap(joinedContestRequestMutableLiveData, new Function<JoinContestRequest, LiveData<Resource<JoinedContestResponse>>>() {
        @Override
        public LiveData<Resource<JoinedContestResponse>> apply(final JoinContestRequest input) {
            LiveData<Resource<JoinedContestResponse>> resourceLiveData = mRepository.joinedContestList(input);
            final MediatorLiveData<Resource<JoinedContestResponse>> mediator = new MediatorLiveData<Resource<JoinedContestResponse>>();
            mediator.addSource(resourceLiveData, new Observer<Resource<JoinedContestResponse>>() {
                @Override
                public void onChanged(Resource<JoinedContestResponse> arrayListResource) {
                    JoinedContestResponse resp = arrayListResource.getData();
                    Resource<JoinedContestResponse> response = null;
                    switch (arrayListResource.getStatus()) {
                        case LOADING:
                            response = Resource.loading(null);
                            break;
                        case SUCCESS:
                            response = Resource.success(resp);
                            break;
                        case ERROR:
                            response = Resource.error(arrayListResource.getException(), null);
                            break;
                    }
                    mediator.setValue(response);
                }
            });
            return mediator;
        }
    });


    /*
    Get the view model instance.
     */
    public static ContestDetailsViewModel create(FragmentActivity activity) {
        ContestDetailsViewModel viewModel = ViewModelProviders.of(activity).get(ContestDetailsViewModel.class);
        return viewModel;
    }

    public static ContestDetailsViewModel create(Fragment fragment) {
        ContestDetailsViewModel viewModel = ViewModelProviders.of(fragment).get(ContestDetailsViewModel.class);
        return viewModel;
    }

    /**
     * Expose the LiveData So that UI can observe it.
     */
    public LiveData<Resource<ContestDetailResponse>> getContestData() {
        return contestLiveData;
    }

    public LiveData<Resource<JoinedContestResponse>> getJoinedContestData() {
        return joinedContestLiveData;
    }


    public void loadContestRequest(ContestRequest contestRequest) {
        contestRequestMutableLiveData.setValue(contestRequest);
    }

    public void loadJoinedContestRequest(JoinContestRequest joinContestRequest) {
        joinedContestRequestMutableLiveData.setValue(joinContestRequest);
    }


    @Inject
    public void setRepository(MatchRepository repository) {
        this.mRepository = repository;
    }



    @Override
    protected void onCleared() {
        super.onCleared();
    }
}