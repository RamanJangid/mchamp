package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ContestData {

    @SerializedName("contest")
    private ArrayList<Contest> contestArrayList;

    @SerializedName("user_teams")
    private int userTeamCount;

    @SerializedName("joined_leagues")
    private int joinedContestCount;

    public ArrayList<Contest> getContestArrayList() {
        return contestArrayList;
    }

    public void setContestArrayList(ArrayList<Contest> contestArrayList) {
        this.contestArrayList = contestArrayList;
    }

    public int getUserTeamCount() {
        return userTeamCount;
    }

    public void setUserTeamCount(int userTeamCount) {
        this.userTeamCount = userTeamCount;
    }

    public int getJoinedContestCount() {
        return joinedContestCount;
    }

    public void setJoinedContestCount(int joinedContestCount) {
        this.joinedContestCount = joinedContestCount;
    }
}
