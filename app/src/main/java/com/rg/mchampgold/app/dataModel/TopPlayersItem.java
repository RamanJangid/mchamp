package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

public class TopPlayersItem {
    @SerializedName("id")
    int id;
    @SerializedName("name")
    String name;
    @SerializedName("points")
    int points;
    @SerializedName("image")
    String image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
