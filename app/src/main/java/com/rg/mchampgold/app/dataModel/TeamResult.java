package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class TeamResult{

	@SerializedName("marathonstatus")
	private int marathonstatus;

	@SerializedName("teamid")
	private int teamid;

	@SerializedName("status")
	private int status;

	public void setMarathonstatus(int marathonstatus){
		this.marathonstatus = marathonstatus;
	}

	public int getMarathonstatus(){
		return marathonstatus;
	}

	public void setTeamid(int teamid){
		this.teamid = teamid;
	}

	public int getTeamid(){
		return teamid;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ContestDetailITem{" +
			"marathonstatus = '" + marathonstatus + '\'' + 
			",teamid = '" + teamid + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}