package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PlayingHistoryItem {

	@SerializedName("total_match_play")
	private int totalMatchPlay;

	@SerializedName("total_league_play")
	private int totalLeaguePlay;

	@SerializedName("total_winning")
	private String totalWinning;

	@SerializedName("total_contest_win")
	private int totalContestWin;

	public void setTotalMatchPlay(int totalMatchPlay){
		this.totalMatchPlay = totalMatchPlay;
	}

	public int getTotalMatchPlay(){
		return totalMatchPlay;
	}

	public void setTotalLeaguePlay(int totalLeaguePlay){
		this.totalLeaguePlay = totalLeaguePlay;
	}

	public int getTotalLeaguePlay(){
		return totalLeaguePlay;
	}

	public void setTotalWinning(String totalWinning){
		this.totalWinning = totalWinning;
	}

	public String getTotalWinning(){
		return totalWinning ==null?"":totalWinning;
	}

	public void setTotalContestWin(int totalContestWin){
		this.totalContestWin = totalContestWin;
	}

	public int getTotalContestWin(){
		return totalContestWin;
	}

	@Override
 	public String toString(){
		return 
			"PlayingHistoryItem{" +
			"total_match_play = '" + totalMatchPlay + '\'' + 
			",total_league_play = '" + totalLeaguePlay + '\'' + 
			",total_winning = '" + totalWinning + '\'' + 
			",total_contest_win = '" + totalContestWin + '\'' + 
			"}";
		}
}