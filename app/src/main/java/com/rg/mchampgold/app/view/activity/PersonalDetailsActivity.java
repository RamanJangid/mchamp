package com.rg.mchampgold.app.view.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.GetUserFullDetailsResponse;
import com.rg.mchampgold.app.dataModel.UpdateProfileRequest;
import com.rg.mchampgold.app.dataModel.UpdateProfileResponse;
import com.rg.mchampgold.app.dataModel.UserDetailValue;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.adapter.SpinnerAdapter;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityPersonalDetailsBinding;

import java.util.Calendar;

import javax.inject.Inject;

import retrofit2.Response;


public class PersonalDetailsActivity extends AppCompatActivity {

    ActivityPersonalDetailsBinding mBinding;
    String[] stateAr;
    String TAG = "Profile";
    String name1, email1, gender1, mobile1, state1, address1, city1, pincode1, country1;

    @Inject
    OAuthRestService oAuthRestService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_personal_details);
        MyApplication.getAppComponent().inject(PersonalDetailsActivity.this);
        initialize();

        stateAr = new String[getResources().getStringArray(R.array.india_states).length];
        stateAr = getResources().getStringArray(R.array.india_states);

        mBinding.stateSpinner.setAdapter(new SpinnerAdapter(this, stateAr));

        mBinding.etDob.setOnClickListener(view -> pickDate(mBinding.etDob));

        mBinding.stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    //   mBinding.stateSpinner.setEnabled(false);
                    state1 = stateAr[i];
                }
                ((TextView) mBinding.stateSpinner.getSelectedView()).setTextColor(getResources().getColor(R.color.colorBlack));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mBinding.tvChangePassword.setOnClickListener(v -> startActivity(new Intent(PersonalDetailsActivity.this, ChangePasswordActivity.class)));
        mBinding.btnSave.setOnClickListener(view -> {

            name1 = mBinding.name.getText().toString();
            email1 = mBinding.email.getText().toString();
            mobile1 = mBinding.mobile.getText().toString();
            address1 = mBinding.address.getText().toString();
            city1 = mBinding.city.getText().toString();
            pincode1 = mBinding.pincode.getText().toString();

            if (mBinding.maleRb.isChecked())
                gender1 = "male";
            else
                gender1 = "female";

            if (name1.length() < 4)
                AppUtils.showErrorr(PersonalDetailsActivity.this, "Please enter valid name");
            else if (email1.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email1).matches())
                AppUtils.showErrorr(PersonalDetailsActivity.this, "Please enter valid email address");
            else if (mobile1.length() != 10)
                AppUtils.showErrorr(PersonalDetailsActivity.this, "Please enter valid mobile number");
            else if (state1 == null || state1.equals("Select State")) {
                AppUtils.showErrorr(PersonalDetailsActivity.this, "Please select your state");
            } else if (pincode1.length() > 0 && pincode1.length() < 6) {
                AppUtils.showErrorr(PersonalDetailsActivity.this, "Please enter valid pin code");
            } else {
                mBinding.setRefreshing(true);
                updateUserProfile();
            }
        });

        mBinding.setRefreshing(true);
        getFullUserDetails();
    }


    void initialize() {
        AppUtils.disableCopyPaste(mBinding.name);
        AppUtils.disableCopyPaste(mBinding.email);
        AppUtils.disableCopyPaste(mBinding.mobile);
        AppUtils.disableCopyPaste(mBinding.etPass);
        AppUtils.disableCopyPaste(mBinding.etDob);
        AppUtils.disableCopyPaste(mBinding.city);
        AppUtils.disableCopyPaste(mBinding.pincode);

        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.str_edit_profile));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void pickDate(final TextView dialog) {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(PersonalDetailsActivity.this, (datepicker, selectedyear, selectedmonth, selectedday) -> {
            String dayOfMonth = String.valueOf(selectedday).length() == 1 ? "0" + selectedday : String.valueOf(selectedday);
            String month = String.valueOf(selectedmonth + 1).length() == 1 ? "0" + (selectedmonth + 1) : String.valueOf((selectedmonth + 1));
            String d = dayOfMonth + "/" + month + "/" + selectedyear;
//            String d = (selectedmonth + 1) + "/" + selectedday + "/" + selectedyear;
            dialog.setText(d);
        }, mYear, mMonth, mDay);

        mDatePicker.getDatePicker().setMaxDate((long) (System.currentTimeMillis() - (5.681e+11)));
        mDatePicker.setTitle("Select Birth Date");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mDatePicker.getDatePicker().setFirstDayOfWeek(Calendar.MONDAY);
        }
        mDatePicker.show();
    }


    private void getFullUserDetails() {
        mBinding.setRefreshing(true);
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));

        CustomCallAdapter.CustomCall<GetUserFullDetailsResponse> userFullDetailsResponseCustomCall = oAuthRestService.getUserFullDetails(baseRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<GetUserFullDetailsResponse>() {
            @Override
            public void success(Response<GetUserFullDetailsResponse> response) {
                mBinding.setRefreshing(false);

                GetUserFullDetailsResponse getUserFullDetailsResponse = response.body();
                if (getUserFullDetailsResponse.getStatus() == 1) {
                    UserDetailValue userDetailValue = getUserFullDetailsResponse.getResult().getValue();
                    mBinding.name.setText(userDetailValue.getUsername());
                    mBinding.email.setText(userDetailValue.getEmail());
                    mBinding.mobile.setText(userDetailValue.getMobile());

                    if (userDetailValue.getGender().equals("female")) {
                        mBinding.femaleRb.setChecked(true);
                        mBinding.maleRb.setChecked(false);
                    } else if (userDetailValue.getGender().equals("male")) {
                        mBinding.maleRb.setChecked(true);
                        mBinding.femaleRb.setChecked(false);
                    } else {
                        mBinding.maleRb.setChecked(false);
                        mBinding.femaleRb.setChecked(false);
                    }
                    mBinding.etDob.setText(userDetailValue.getDob());
                    //session.setImage(userDetailValue.getImage());
                    mBinding.address.setText(userDetailValue.getAddress());
                    mBinding.city.setText(userDetailValue.getCity());
                    mBinding.pincode.setText(userDetailValue.getPincode());
                    state1 = userDetailValue.getState();
                    //session.setAmountWon(String.valueOf(userDetailValue.getTotalwon()));
                    // session.setChallengesPlayed(String.valueOf(userDetailValue.getTotalchallenges()));

                    for (int i = 0; i < stateAr.length; i++) {
                        if (stateAr[i].equalsIgnoreCase(userDetailValue.getState()))
                            mBinding.stateSpinner.setSelection(i);
                    }

                    if (userDetailValue.getDobfreeze() == 1) {
                        mBinding.etDob.setEnabled(false);
                    }

                    if (userDetailValue.getMobilefreeze() == 1) {
                        mBinding.mobile.setEnabled(false);
                    }

                    if (userDetailValue.getEmailfreeze() == 1) {
                        mBinding.email.setEnabled(false);
                    }

                    /*
                    if (session.getPANVerified().equals("1")) {
                        name.setEnabled(false);
                    }*/


                    if (userDetailValue.getStatefreeze() == 1)
                        mBinding.stateSpinner.setEnabled(false);


                } else {
                    AppUtils.showErrorr(PersonalDetailsActivity.this, getUserFullDetailsResponse.getMessage());
                }


            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });


    }


    private void updateUserProfile() {
        mBinding.setRefreshing(true);
        UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
        updateProfileRequest.setId(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        updateProfileRequest.setUsername(name1);
        updateProfileRequest.setDob(mBinding.etDob.getText().toString().trim());
        updateProfileRequest.setGender(gender1);
        updateProfileRequest.setAddress(address1);
        updateProfileRequest.setCity(city1);
        updateProfileRequest.setState(state1);
        updateProfileRequest.setCountry(country1);
        updateProfileRequest.setPincode(pincode1);


        if (mBinding.mobile.isEnabled()) {
            updateProfileRequest.setMobile(mBinding.mobile.getText().toString().trim());
        }

        CustomCallAdapter.CustomCall<UpdateProfileResponse> userFullDetailsResponseCustomCall = oAuthRestService.updateProfile(updateProfileRequest);
        userFullDetailsResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<UpdateProfileResponse>() {
            @Override
            public void success(Response<UpdateProfileResponse> response) {
                mBinding.setRefreshing(false);
                UpdateProfileResponse updateProfileResponse = response.body();
                if (updateProfileResponse.getStatus() == 1) {
                    AppUtils.showSuccess(PersonalDetailsActivity.this, "Profile Updated Successfully.");
                    MyApplication.tinyDB.putString(Constants.SHARED_PREFERENCE_USER_NAME, name1);
                } else {
                    AppUtils.showErrorr(PersonalDetailsActivity.this, updateProfileResponse.getMessage());
                }
            }

            @Override
            public void failure(ApiException e) {
                mBinding.setRefreshing(false);

            }
        });

    }
}
