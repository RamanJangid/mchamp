package com.rg.mchampgold.app.repository;


import androidx.lifecycle.MutableLiveData;

import com.rg.mchampgold.app.api.response.LoginResponse;
import com.rg.mchampgold.app.api.service.UserRestService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserRepository {
    final private UserRestService userRestService;
    private final MutableLiveData<LoginResponse> loginMutableLiveDataResponse = new MutableLiveData<>();


    @Inject
    public UserRepository(UserRestService api) {
        this.userRestService = api;
    }


 /*   public LiveData<Resource<LoginResponse>> loginUser(LoginRequest loginRequest) {
        LiveData<Resource<LoginResponse>> liveData = new NetworkBoundResource<LoginResponse, LoginResponse>() {

            @Override
            protected void saveCallResult(@NonNull LoginResponse loginResponse) {
                loginMutableLiveDataResponse.postValue(loginResponse);
            }

            @Override
            protected boolean shouldFetch(@Nullable LoginResponse data) {
                return true;
            }

            @NonNull
            @Override
            protected LiveData<LoginResponse> loadFromDb() {
                loginMutableLiveDataResponse.setValue(loginMutableLiveDataResponse.getValue());
                return loginMutableLiveDataResponse;
            }

            @NonNull
            @Override
            protected LiveData<ApiResponse<LoginResponse>> createCall() {
                return userRestService.userLogin(loginRequest);
            }
        }.getAsLiveData();

        return liveData;
    }*/


}





