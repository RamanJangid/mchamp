package com.rg.mchampgold.app.view.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.CreateTeamRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.JoinItem;
import com.rg.mchampgold.app.dataModel.MyBalanceResponse;
import com.rg.mchampgold.app.dataModel.MyBalanceResultItem;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.UsableBalanceItem;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.utils.TeamCreatedListener;
import com.rg.mchampgold.app.utils.TextViewLinkHandler;
import com.rg.mchampgold.app.view.adapter.ChooseCaptainVCPlayerItemAdapter;
import com.rg.mchampgold.app.view.basketball.BasketBallCreateTeamActivity;
import com.rg.mchampgold.app.view.basketball.BasketBallTeamPreviewActivity;
import com.rg.mchampgold.app.view.football.FootballCreateTeamActivity;
import com.rg.mchampgold.app.view.football.FootballTeamPreviewActivity;
import com.rg.mchampgold.app.view.interfaces.PlayerItemClickListener;
import com.rg.mchampgold.app.viewModel.CreateTeamViewModel;
import com.rg.mchampgold.app.viewModel.TeamViewModel;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityChooseCaptionVcBinding;

import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Response;

public class ChooseCandVCActivity extends AppCompatActivity implements PlayerItemClickListener/*, OnShowcaseEventListener */ {
    public ActivityChooseCaptionVcBinding mBinding;
    ChooseCaptainVCPlayerItemAdapter mAdapter;
    private CreateTeamViewModel createTeamViewModel;
    String matchKey;
    String teamVsName;
    String teamFirstUrl;
    String teamSecondUrl;
    String localTeamCount = "0";
    String visitorTeamCount = "0";
    int teamId = 0;
    ArrayList<Player> list = new ArrayList<>();
    Context context;
    String selectedCaptain = "";
    String selectedVCaptain = "";
    boolean isFromEditClone;
    boolean isShowTimer;
    String headerText;
    int counterValue = 0;
    Menu menuTemp;
    TeamCreatedListener listener;
    String sportKey = "";
    private boolean isPointSorted = false, isCaptainSorted = false, isViceCaptainSorted = false;
    private int teamCount = 1;
    private TeamViewModel teamViewModel;
    double availableB;
    double usableB;
    Dialog dialog;
    private Contest contest;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        teamViewModel = TeamViewModel.create(ChooseCandVCActivity.this);
        createTeamViewModel = CreateTeamViewModel.create(ChooseCandVCActivity.this);
        MyApplication.getAppComponent().inject(teamViewModel);
        MyApplication.getAppComponent().inject(createTeamViewModel);
        MyApplication.getAppComponent().inject(ChooseCandVCActivity.this);
        context = ChooseCandVCActivity.this;
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_choose_caption_vc);
        initialize();
    }


    void initialize() {
        setSupportActionBar(mBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.choose_c_vc_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (getIntent() != null && getIntent().getExtras() != null) {
            matchKey = getIntent().getExtras().getString(Constants.KEY_MATCH_KEY);
            teamVsName = getIntent().getExtras().getString(Constants.KEY_TEAM_VS);
            teamFirstUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_FIRST_URL);
            teamSecondUrl = getIntent().getExtras().getString(Constants.KEY_TEAM_SECOND_URL);
            list = (ArrayList<Player>) getIntent().getSerializableExtra("playerList");
            teamId = getIntent().getExtras().getInt(Constants.KEY_TEAM_ID);
            isFromEditClone = getIntent().getExtras().getBoolean("isFromEditOrClone", false);
            isShowTimer = getIntent().getExtras().getBoolean(Constants.KEY_STATUS_IS_TIMER_HEADER, false);
            headerText = getIntent().getExtras().getString(Constants.KEY_STATUS_HEADER_TEXT, "");
            sportKey = getIntent().getExtras().getString(Constants.SPORT_KEY);
            localTeamCount = getIntent().getExtras().getString("localTeamCount", "");
            visitorTeamCount = getIntent().getExtras().getString("visitorTeamCount", "");
            teamCount = getIntent().getExtras().getInt(Constants.KEY_TEAM_COUNT, 1);
            contest = (Contest) getIntent().getSerializableExtra(Constants.KEY_CONTEST_DATA);
        }

        String teams[] = teamVsName.split(" ");
        mBinding.tvTeam1.setText(teams[0]);
        mBinding.tvTeam2.setText(teams[2]);
        mBinding.tvLocalTeam.setText(localTeamCount);
        mBinding.tvVisitorTeam.setText(visitorTeamCount);
        AppUtils.loadImageMatch(mBinding.ivTeam1, teamFirstUrl);
        AppUtils.loadImageMatch(mBinding.ivTeam2, teamSecondUrl);


        if (isFromEditClone) {
            for (Player player : list) {
                if (player.isCaptain())
                    selectedCaptain = player.getName();
                if (player.isVcCaptain())
                    selectedVCaptain = player.getName();
            }
            //   mBinding.btnCreateTeam.setBackgroundResource(R.drawable.red_card_bg);
  /*          mBinding.tvCaptain.setText("(C) "+selectedCaptain);
            mBinding.tvViceCaptain.setText("(VC) "+selectedVCaptain);*/

        }
        if (sportKey.equalsIgnoreCase("BASKETBALL")) {
            mBinding.llK.setVisibility(View.INVISIBLE);
        }
        if (isShowTimer) {
            showTimer();
        } else {
            if (headerText.equalsIgnoreCase("Winner Declared")) {
                mBinding.tvTimeTimer.setText("Winner Declared");
                mBinding.tvTimeTimer.setTextColor(getResources().getColor(R.color.color_match_declared));
            } else if (headerText.equalsIgnoreCase("In Progress")) {
                mBinding.tvTimeTimer.setText("In Progress");
                mBinding.tvTimeTimer.setTextColor(getResources().getColor(R.color.color_match_progress));
            } else {
                mBinding.tvTimeTimer.setText(headerText);
                mBinding.tvTimeTimer.setTextColor(getResources().getColor(R.color.color_match_progress));
            }
        }

        setupRecyclerView();

        mBinding.btnCreateTeam.setOnClickListener(view -> {
            if (sportKey.equalsIgnoreCase("CRICKET") || sportKey.equalsIgnoreCase("FOOTBALL")) {
                if (selectedCaptain.equals("") || selectedVCaptain.equals("")) {
                    AppUtils.showErrorr(ChooseCandVCActivity.this, "Please select Captain & V. Captain");
                    return;
                }
            } else {
                if (selectedCaptain.equals("")) {
                    AppUtils.showErrorr(ChooseCandVCActivity.this, "Please select Captain & V. Captain");
                    return;
                }
            }
            createTeam();
        });

        mBinding.ivTeamPreview.setOnClickListener(view -> {
            if (sportKey.equalsIgnoreCase("CRICKET")) {
                mBinding.kCard.setVisibility(View.VISIBLE);
                navigateTeamPreview();
            } else if (sportKey.equalsIgnoreCase("FOOTBALL")) {
                mBinding.kCard.setVisibility(View.VISIBLE);
                navigateTeamPreview2();
            } else {
                mBinding.kCard.setVisibility(View.GONE);
                navigateTeamPreview3();
            }
        });
        if (sportKey.equalsIgnoreCase("CRICKET")) {
            mBinding.kCard.setVisibility(View.VISIBLE);
        } else if (sportKey.equalsIgnoreCase("FOOTBALL")) {
            mBinding.kCard.setVisibility(View.VISIBLE);
        } else {
            mBinding.kCard.setVisibility(View.GONE);
        }

        mBinding.llPoints.setOnClickListener(view -> {
            if (isPointSorted) {
                mAdapter.sortWithPoints(false);
                isPointSorted = false;
                mBinding.ivPointSort.setVisibility(View.VISIBLE);
                mBinding.ivPointSort.setImageResource(R.drawable.ic_down_sort);
            } else {
                mAdapter.sortWithPoints(true);
                isPointSorted = true;
                mBinding.ivPointSort.setVisibility(View.VISIBLE);
                mBinding.ivPointSort.setImageResource(R.drawable.ic_up_sort);
            }
            mBinding.ivSkSort.setVisibility(View.INVISIBLE);
            mBinding.ivKSort.setVisibility(View.INVISIBLE);
            isCaptainSorted = true;
            isViceCaptainSorted = true;

        });

        mBinding.llSk.setOnClickListener(view -> {
            if (isCaptainSorted) {
                mAdapter.sortWithCaptain(false);
                isCaptainSorted = false;
                mBinding.ivSkSort.setVisibility(View.VISIBLE);
                mBinding.ivSkSort.setImageResource(R.drawable.ic_down_sort);
            } else {
                mAdapter.sortWithCaptain(true);
                isCaptainSorted = true;
                mBinding.ivSkSort.setVisibility(View.VISIBLE);
                mBinding.ivSkSort.setImageResource(R.drawable.ic_up_sort);
            }
            mBinding.ivPointSort.setVisibility(View.INVISIBLE);
            mBinding.ivKSort.setVisibility(View.INVISIBLE);
            isPointSorted = true;
            isViceCaptainSorted = true;

        });

        mBinding.llK.setOnClickListener(view -> {
            if (isViceCaptainSorted) {
                mAdapter.sortWithViceCaptain(false);
                isViceCaptainSorted = false;
                mBinding.ivKSort.setVisibility(View.VISIBLE);
                mBinding.ivKSort.setImageResource(R.drawable.ic_down_sort);
            } else {
                mAdapter.sortWithViceCaptain(true);
                isViceCaptainSorted = true;
                mBinding.ivKSort.setVisibility(View.VISIBLE);
                mBinding.ivKSort.setImageResource(R.drawable.ic_up_sort);
            }
            mBinding.ivPointSort.setVisibility(View.INVISIBLE);
            mBinding.ivSkSort.setVisibility(View.INVISIBLE);
            isPointSorted = true;
            isCaptainSorted = true;

        });
    }

    private void navigateTeamPreview() {
        Intent intent = new Intent(ChooseCandVCActivity.this, TeamPreviewActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);
        ArrayList<Player> selectedWkList = new ArrayList<>();
        ArrayList<Player> selectedBatLiSt = new ArrayList<>();
        ArrayList<Player> selectedArList = new ArrayList<>();
        ArrayList<Player> selectedBowlList = new ArrayList<>();

        for (Player player : this.list) {
            if (player.isIsSelected()) {
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_KEEP)) {
                    selectedWkList.add(player);
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_BAT)) {
                    selectedBatLiSt.add(player);
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_ALL_R)) {
                    selectedArList.add(player);
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_BOL)) {
                    selectedBowlList.add(player);
                }
            }
        }

        intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedWkList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedBatLiSt);
        intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedArList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedBowlList);

        startActivity(intent);
    }

    private void navigateTeamPreview2() {
        Intent intent = new Intent(ChooseCandVCActivity.this, FootballTeamPreviewActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);

        ArrayList<Player> selectedWkList = new ArrayList<>();
        ArrayList<Player> selectedBatLiSt = new ArrayList<>();
        ArrayList<Player> selectedArList = new ArrayList<>();
        ArrayList<Player> selectedBowlList = new ArrayList<>();
        for (Player player : this.list) {
            if (player.isIsSelected()) {
                if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_GK))
                    selectedWkList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_DEF))
                    selectedBatLiSt.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_MID))
                    selectedArList.add(player);
                else if (player.getRole().equalsIgnoreCase(Constants.KEY_PLAYER_ROLE_ST))
                    selectedBowlList.add(player);
            }
        }

        intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedWkList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedBatLiSt);
        intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedArList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedBowlList);

        startActivity(intent);
    }

    private void navigateTeamPreview3() {
        Intent intent = new Intent(ChooseCandVCActivity.this, BasketBallTeamPreviewActivity.class);
        intent.putExtra(Constants.KEY_MATCH_KEY, matchKey);
        intent.putExtra(Constants.KEY_TEAM_VS, teamVsName);
        intent.putExtra(Constants.KEY_TEAM_FIRST_URL, teamFirstUrl);
        intent.putExtra(Constants.KEY_TEAM_SECOND_URL, teamSecondUrl);

        ArrayList<Player> selectedPGList = new ArrayList<>();
        ArrayList<Player> selectedSGLiSt = new ArrayList<>();
        ArrayList<Player> selectedSFList = new ArrayList<>();
        ArrayList<Player> selectedPFlList = new ArrayList<>();
        ArrayList<Player> selectedCList = new ArrayList<>();
        for (Player player : this.list) {
            if (player.isIsSelected()) {
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_PG)) {
                    selectedPGList.add(player);
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_SG)) {
                    selectedSGLiSt.add(player);
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_SF)) {
                    selectedSFList.add(player);
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_PF)) {
                    selectedPFlList.add(player);
                }
                if (player.getRole().equals(Constants.KEY_PLAYER_ROLE_C)) {
                    selectedCList.add(player);
                }
            }
        }

        intent.putExtra(Constants.KEY_TEAM_LIST_WK, selectedPGList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BAT, selectedSGLiSt);
        intent.putExtra(Constants.KEY_TEAM_LIST_AR, selectedSFList);
        intent.putExtra(Constants.KEY_TEAM_LIST_BOWL, selectedPFlList);
        intent.putExtra(Constants.KEY_TEAM_LIST_C, selectedCList);

        startActivity(intent);
    }


    private void createTeam() {
        CreateTeamRequest request = new CreateTeamRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(matchKey);
        request.setSport_key(sportKey);
        StringBuilder sb = new StringBuilder();
        String captain = "";
        String vcCaptain = "";

        for (Player player : list) {
            if (!player.isHeader())
                sb.append(player.getId() + "").append(",");

            if (player.isCaptain())
                captain = player.getId() + "";

            if (player.isVcCaptain())
                vcCaptain = player.getId() + "";
        }

        request.setPlayers(sb.toString());
        request.setCaptain(captain);
        if (teamId != 0)
            request.setTeamId(teamId);
        request.setViceCaptain(vcCaptain);
        createTeamViewModel.loadCreateTeamRequest(request);
        createTeamViewModel.createTeam().observe(this, arrayListResource -> {
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    if (listener != null) {
                        listener.getTeamCreated(false);
                    }
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getResult() != null) {

                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();

                        if (teamCount > 0) {
                            removeActivity();
                        } else {
                            checkBalance(arrayListResource.getData().getResult().getTeamid());
                        }

                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                        if (listener != null) {
                            listener.getTeamCreated(false);
                        }
                    }
                    break;
                }
            }

        });
    }

    private void checkBalance(int teamid) {
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setLeagueId(contest.getId() + "");
        teamViewModel.loadBalanceRequest(request);
        teamViewModel.getBalanceData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    mBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getResult().size() > 0) {
                        UsableBalanceItem balanceItem = arrayListResource.getData().getResult().get(0);
                        availableB = balanceItem.getUsertotalbalance();
                        usableB = balanceItem.getUsablebalance();
                        if (dialog != null) {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }
                        createDialog(teamid);
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    break;
                }
            }

        });
    }

    private void removeActivity() {

        if (CreateTeamActivity.createTeamAc != null) {
            CreateTeamActivity.createTeamAc.finish();
        }
        if (FootballCreateTeamActivity.createTeamAc != null) {
            FootballCreateTeamActivity.createTeamAc.finish();
        }
        if (BasketBallCreateTeamActivity.createTeamAc != null) {
            BasketBallCreateTeamActivity.createTeamAc.finish();
        }

        if (UpComingContestDetailActivity.listener != null) {
            listener = UpComingContestDetailActivity.listener;
            listener.getTeamCreated(true);
        }

        if (AllContestActivity.listener != null) {
            listener = AllContestActivity.listener;
            listener.getTeamCreated(true);
        }

        if (PrivateContestActivity.listener != null) {
            listener = PrivateContestActivity.listener;
            listener.getTeamCreated(true);
        }
        MyApplication.teamJoined = true;
        finish();
    }

    public void createDialog(int teamid) {


        dialog = new Dialog(ChooseCandVCActivity.this);
        dialog.setContentView(R.layout.joined_team_confirm_dialog);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        LinearLayout okBtn = dialog.findViewById(R.id.ok_btn);
        TextView currentBalTxt = dialog.findViewById(R.id.currentBalTxt);
        TextView joinedBaltxt = dialog.findViewById(R.id.joinedBaltxt);
        TextView remaingBaltxt = dialog.findViewById(R.id.remaingBaltxt);
        TextView tv_tc_join_dialog = dialog.findViewById(R.id.tv_tc_join_dialog);
        RelativeLayout cancelButton = dialog.findViewById(R.id.cancel_button);
        TextView tPay = dialog.findViewById(R.id.toPay);
        RelativeLayout switchTeamBtn = dialog.findViewById(R.id.switch_team_Btn);

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        currentBalTxt.setText("₹ " + decimalFormat.format(availableB));
        joinedBaltxt.setText("₹ " + decimalFormat.format(Double.parseDouble(contest.getEntryfee())));
        tv_tc_join_dialog.setText(Html.fromHtml("By joining this contest, you accept <font color='" + getResources().getColor(R.color.pink) + "'>" + getString(R.string.app_names) + "</font> <a href='" + MyApplication.terms_url + "'>T&amp;C</a> and <a href='" + MyApplication.privacy_url + "'>Privacy Policy</a> and confirm that you are not a resident of Assam, Odisha, Sikkim, Andhra Pradesh, Tamil Nadu, Nagaland or Telangana."));
        tv_tc_join_dialog.setMovementMethod(LinkMovementMethod.getInstance());
        tv_tc_join_dialog.setMovementMethod(new TextViewLinkHandler() {
            @Override
            public void onLinkClick(String url) {
                if (url.equalsIgnoreCase(MyApplication.terms_url)) {
                    AppUtils.openWebViewActivity(getString(R.string.terms_conditions), MyApplication.terms_url);
                } else {
                    AppUtils.openWebViewActivity(getString(R.string.privacy_policy), MyApplication.privacy_url);
                }
//                Toast.makeText(mainBinding.tvTc.getContext(), url, Toast.LENGTH_SHORT).show();
            }
        });
        double remainBal = usableB;
        tPay.setText("₹ " + (Double.parseDouble(contest.getEntryfee()) - remainBal));

        if (remainBal > 0) {
            remaingBaltxt.setText("₹ " + decimalFormat.format(remainBal));
        } else {
            remaingBaltxt.setText("₹ 0.0");
        }


        dialog.show();

        okBtn.setOnClickListener(view13 -> dialog.dismiss());

        cancelButton.setOnClickListener(view1 -> dialog.dismiss());

        switchTeamBtn.setOnClickListener(view12 -> {
            dialog.dismiss();
            joinChallenge(teamid);
//            if (contest.getIsBonus() == 1) {
//                if ((usableB + availableB) < Double.parseDouble(contest.getEntryfee())) {
//                    double requiredBalance = Double.parseDouble(contest.getEntryfee()) - (usableB + availableB);
//                    startActivity(new Intent(ChooseCandVCActivity.this, AddBalanceActivity.class).putExtra("balance_required", requiredBalance));
//                } else {
//                    joinChallenge(teamid);
//                }
//
//            } else {
//                if (availableB < Double.parseDouble(contest.getEntryfee())) {
//                    double requiredBalance = Double.parseDouble(contest.getEntryfee()) - (availableB);
//                    startActivity(new Intent(ChooseCandVCActivity.this, AddBalanceActivity.class).putExtra("balance_required", requiredBalance));
//                } else {
//                    joinChallenge(teamid);
//
//                }
//            }
        });

    }

    private void joinChallenge(int teamid) {
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setLeagueId(contest.getId() + "");
        request.setMatchKey(contest.getMatchkey());
        request.setTeamId(teamid + "");
        request.setSport_key(sportKey);
        teamViewModel.loadJoinContestRequest(request);
        teamViewModel.joinContest().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    mBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    mBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    dialog.dismiss();
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getResult().size() > 0) {
                        ArrayList<JoinItem> joinItems = arrayListResource.getData().getResult();
                        if (joinItems.get(0).isStatus()) {
                            MyApplication.referCode = joinItems.get(0).getReferCode();
                            getUserBalance();
                        } else {
                            AppUtils.showErrorr(ChooseCandVCActivity.this, joinItems.get(0).getMessage());
                        }
                    } else {
                        Toast.makeText(MyApplication.appContext, arrayListResource.getData().getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    break;
                }
            }

        });
    }

    private void getUserBalance() {
        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        CustomCallAdapter.CustomCall<MyBalanceResponse> myBalanceResponseCustomCall = oAuthRestService.getUserBalance(baseRequest);
        myBalanceResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<MyBalanceResponse>() {
            @Override
            public void success(Response<MyBalanceResponse> response) {
                mBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    MyBalanceResponse myBalanceResponse = response.body();
                    if (myBalanceResponse.getStatus() == 1 && myBalanceResponse.getResult().size() > 0) {
                        MyBalanceResultItem myBalanceResultItem = myBalanceResponse.getResult().get(0);

                        MyApplication.tinyDB.putString(Constants.KEY_USER_BALANCE, myBalanceResultItem.getBalance() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_WINING_AMOUNT, myBalanceResultItem.getWinning() + "");
                        MyApplication.tinyDB.putString(Constants.KEY_USER_BONUS_BALANCE, myBalanceResultItem.getBonus() + "");
                        Toast.makeText(ChooseCandVCActivity.this, "You have Successfully join this contest", Toast.LENGTH_SHORT).show();
                        MyApplication.teamJoinedCount = 1;
                        removeActivity();
                    } else {
                        AppUtils.showErrorr(ChooseCandVCActivity.this, myBalanceResponse.getMessage());
                    }

                }
            }

            @Override
            public void failure(ApiException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;
        } else if (itemId == R.id.navigation_wallet) {
            startActivity(new Intent(ChooseCandVCActivity.this, MyWalletActivity.class));
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void openNotificationActivity() {
        startActivity(new Intent(ChooseCandVCActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(ChooseCandVCActivity.this, MyWalletActivity.class));
    }

    private void setupRecyclerView() {
        mAdapter = new ChooseCaptainVCPlayerItemAdapter(selectedCaptain, selectedVCaptain, context, list, this, sportKey);
        mBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setAdapter(mAdapter);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isCaptain()) {
                mAdapter.isCap = true;
            }
            if (list.get(i).isVcCaptain()) {
                mAdapter.isVice = true;
            }
        }
        /*if (!MyApplication.tinyDB.getBoolean(Constants.SKIP_CREATECVC_INSTRUCTION, false)) {
         *//*  callIntroductionScreen(R.id.linear_captain,
                    "Choose a Captain",
                    "Your Captain score gives you 2x points",
                    ShowcaseView.ABOVE_SHOWCASE);*//*

            MyApplication.tinyDB.putBoolean(Constants.SKIP_CREATECVC_INSTRUCTION, true);
        }
*/
    }


    public void setCaptainVcCaptionName(String caption, String VCaption, ArrayList<Player> list) {
        selectedCaptain = caption;
        selectedVCaptain = VCaption;
        this.list = list;
        if (sportKey.equalsIgnoreCase("BASKETBALL")) {
            if (!selectedCaptain.equals("")) {
                mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_color_primary);
                mBinding.btnCreateTeam.setTextColor(getResources().getColor(R.color.white));
            } else {
                mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_grey);
            }

        } else {
            if (!selectedCaptain.equals("") && !selectedVCaptain.equals("")) {
                mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_color_primary);
                mBinding.btnCreateTeam.setTextColor(getResources().getColor(R.color.white));
            } else {
                mBinding.btnCreateTeam.setBackgroundResource(R.drawable.rounded_corner_filled_grey);
            }

        }

        //  mBinding.tvCaptain.setText(AppUtils.getShortName(caption));
        //   mBinding.tvViceCaptain.setText(AppUtils.getShortName(VCaption));
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (menuTemp!=null){
//            AppUtils.setWalletBalance(menuTemp,getResources().getColor(R.color.pink));
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.menu_wallet, menu);
        // menuTemp=menu;
        // AppUtils.setWalletBalance(menu,getResources().getColor(R.color.golden_text_color));
        // AppUtils.loadImageCircle((ImageView) menu.findItem(R.id.iv_profile).getActionView(), MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_PIC));
        return true;
    }


    public void openPlayerInfoActivity(String playerId, String playerName, String team, String image) {
        Intent intent = new Intent(this, PlayerInfoActivity.class);
        intent.putExtra("matchKey", matchKey);
        intent.putExtra("playerId", playerId);
        intent.putExtra("playerName", playerName);
        intent.putExtra("team", team);
        intent.putExtra("image", image);
        intent.putExtra("flag", "1");
        startActivity(intent);
    }

    private void showTimer() {
        try {
            CountDownTimer countDownTimer = new CountDownTimer(AppUtils.EventDateMilisecond(headerText), 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    long time = millisUntilFinished;
                    long seconds = time / 1000 % 60;
                    long minutes = (time / (1000 * 60)) % 60;
                    long diffHours = (time / (60 * 60 * 1000));
                   /* if (TimeUnit.MILLISECONDS.toDays(millisUntilFinished) > 0) {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(TimeUnit.MILLISECONDS.toDays(millisUntilFinished) + "d " + twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    } else {
                        mBinding.matchHeaderInfo.tvTimeTimer.setText(twoDigitString(diffHours) + "h " + twoDigitString(minutes) + "m " + twoDigitString(seconds) + "s ");
                    }*/
                    mBinding.tvTimeTimer.setText(twoDigitString(diffHours) + "h : " + twoDigitString(minutes) + "m : " + twoDigitString(seconds) + "s ");
                }

                private String twoDigitString(long number) {
                    if (number == 0) {
                        return "00";
                    } else if (number / 10 == 0) {
                        return "0" + number;
                    }
                    return String.valueOf(number);
                }

                @Override
                public void onFinish() {
                    mBinding.tvTimeTimer.setText("00h 00m 00s");
                    startActivity(new Intent(ChooseCandVCActivity.this, HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP).putExtra("matchFinish", true));
                }
            };
            countDownTimer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPlayerClick(boolean isSelect, int position, int type) {

    }


    /*void callIntroductionScreen(int target, String title, String description, int abovE_SHOWCASE) {
        ShowcaseView showcaseView = new ShowcaseView.Builder(this).withMaterialShowcase()
                .setTarget(new ViewTarget(target, this))
                .setContentTitle(title)
                .setContentText(description)
                .setStyle(R.style.CustomShowcaseTheme)
                .hideOnTouchOutside().setShowcaseEventListener(this)
                .build();

        showcaseView.forceTextPosition(abovE_SHOWCASE);
        counterValue = counterValue + 1;

        new Handler().postDelayed(() -> showcaseView.hideButton(), 2500);

    }


    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {

        switch (counterValue) {
            case 1: {
                callIntroductionScreen(R.id.linear_vc,
                        "Choose a Vice Captain",
                        "Your Vice Captain score gives you 1.5x points", ShowcaseView.ABOVE_SHOWCASE);
            }
        }

    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }

    @Override
    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {
    }
*/

}
