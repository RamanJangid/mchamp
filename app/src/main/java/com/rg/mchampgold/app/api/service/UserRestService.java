package com.rg.mchampgold.app.api.service;


import androidx.lifecycle.LiveData;

import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.ContestRequest;
import com.rg.mchampgold.app.api.request.CreateTeamRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.request.MyTeamRequest;
import com.rg.mchampgold.app.dataModel.BalanceResponse;
import com.rg.mchampgold.app.dataModel.ContestDetailResponse;
import com.rg.mchampgold.app.dataModel.ContestResponse;
import com.rg.mchampgold.app.dataModel.CreateTeamResponse;
import com.rg.mchampgold.app.dataModel.JoinContestResponse;
import com.rg.mchampgold.app.dataModel.JoinedContestResponse;
import com.rg.mchampgold.app.dataModel.MatchListResponse;
import com.rg.mchampgold.app.dataModel.MyTeamResponse;
import com.rg.mchampgold.app.dataModel.PlayerListResponse;
import com.rg.mchampgold.app.dataModel.PlayerPointsResponse;
import com.rg.mchampgold.app.dataModel.RefreshScoreResponse;
import com.rg.mchampgold.common.api.ApiResponse;

import retrofit2.http.Body;
import retrofit2.http.POST;


public interface UserRestService {

    @POST("api/auth/getmatchlist")
    LiveData<ApiResponse<MatchListResponse>> getMatchList(@Body BaseRequest baseRequest);

    @POST("api/auth/myjoinedmatches")
    LiveData<ApiResponse<MatchListResponse>> getMyMatchList(@Body BaseRequest baseRequest);

    @POST("api/auth/get-challenges-new")
    LiveData<ApiResponse<ContestResponse>> getContest(@Body ContestRequest contestRequest);

    @POST("api/auth/leaguedetails")
    LiveData<ApiResponse<ContestDetailResponse>> getContestDetails(@Body ContestRequest contestRequest);

    @POST("api/auth/leaderboard")
    LiveData<ApiResponse<ContestDetailResponse>> getLeaderBoard(@Body ContestRequest contestRequest);

    @POST("api/auth/myjointeam")
    LiveData<ApiResponse<MyTeamResponse>> getMyTeams(@Body MyTeamRequest myTeamRequest);

    @POST("api/auth/getplayerlist")
    LiveData<ApiResponse<PlayerListResponse>> getPlayerList(@Body MyTeamRequest myTeamRequest);

    @POST("api/auth/create-team")
    LiveData<ApiResponse<CreateTeamResponse>> createTeam(@Body CreateTeamRequest createTeamRequest);


    @POST("api/auth/myusablebalance")
    LiveData<ApiResponse<BalanceResponse>> getUsableBalance(@Body JoinContestRequest joinContestRequest);

    @POST("api/auth/joinleague")
    LiveData<ApiResponse<JoinContestResponse>> joinContest(@Body JoinContestRequest joinContestRequest);

    @POST("api/auth/myjoinedleauges")
    LiveData<ApiResponse<JoinedContestResponse>> joinedContestList(@Body JoinContestRequest joinContestRequest);

    //use this api on live and fished match result
    @POST("api/auth/refresh-scores-new")
    LiveData<ApiResponse<RefreshScoreResponse>> refreshScore(@Body ContestRequest contestRequest);

    //use this api to get player points
    @POST("api/auth/matchplayerspoints")
    LiveData<ApiResponse<PlayerPointsResponse>> getPlayerPoints(@Body ContestRequest contestRequest);

/*
    @POST("api/auth/get-challenges-new")
    LiveData<ApiResponse<ContestResponse>> getContest(@Body ContestRequest contestRequest);*/


}

