package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ContestItem{

	@SerializedName("totalwinners")
	private int totalwinners;

	@SerializedName("entryfee")
	private int entryfee;

	@SerializedName("team1display")
	private String team1display;

	@SerializedName("getjoinedpercentage")
	private int getjoinedpercentage;

	@SerializedName("joinedusers")
	private int joinedusers;

	@SerializedName("isjoined")
	private boolean isjoined;

	@SerializedName("isselected")
	private boolean isselected;

	@SerializedName("matchkey")
	private String matchkey;

	@SerializedName("multi_entry")
	private String multiEntry;

	@SerializedName("isselectedid")
	private String isselectedid;

	@SerializedName("points")
	private String points;

	@SerializedName("is_running")
	private int isRunning;

	@SerializedName("join_with")
	private int joinWith;


	@SerializedName("is_free_key")
	private int is_free_key;

	@SerializedName("dis_price")
	private String dis_price="0";

	public String getDis_price() {
		return dis_price;
	}

	public void setDis_price(String dis_price) {
		this.dis_price = dis_price;
	}


	@SerializedName("confirmed_challenge")
	private int confirmedChallenge;

	@SerializedName("is_bonus")
	private int isBonus;

	@SerializedName("refercode")
	private String refercode;

	@SerializedName("name")
	private String name;

	public int getIs_free_key() {
		return is_free_key;
	}

	public void setIs_free_key(int is_free_key) {
		this.is_free_key = is_free_key;
	}

	@SerializedName("id")
	private int id;

	@SerializedName("win_amount")
	private int winAmount;

	@SerializedName("team2display")
	private String team2display;

	@SerializedName("maximum_user")
	private int maximumUser;

	public void setTotalwinners(int totalwinners){
		this.totalwinners = totalwinners;
	}

	public int getTotalwinners(){
		return totalwinners;
	}

	public void setEntryfee(int entryfee){
		this.entryfee = entryfee;
	}

	public int getEntryfee(){
		return entryfee;
	}

	public void setTeam1display(String team1display){
		this.team1display = team1display;
	}

	public String getTeam1display(){
		return team1display;
	}

	public void setGetjoinedpercentage(int getjoinedpercentage){
		this.getjoinedpercentage = getjoinedpercentage;
	}

	public int getGetjoinedpercentage(){
		return getjoinedpercentage;
	}

	public void setJoinedusers(int joinedusers){
		this.joinedusers = joinedusers;
	}

	public int getJoinedusers(){
		return joinedusers;
	}

	public void setIsjoined(boolean isjoined){
		this.isjoined = isjoined;
	}

	public boolean isIsjoined(){
		return isjoined;
	}

	public void setIsselected(boolean isselected){
		this.isselected = isselected;
	}

	public boolean isIsselected(){
		return isselected;
	}

	public void setMatchkey(String matchkey){
		this.matchkey = matchkey;
	}

	public String getMatchkey(){
		return matchkey;
	}

	public void setMultiEntry(String multiEntry){
		this.multiEntry = multiEntry;
	}

	public String getMultiEntry(){
		return multiEntry;
	}

	public void setIsselectedid(String isselectedid){
		this.isselectedid = isselectedid;
	}

	public String getIsselectedid(){
		return isselectedid;
	}

	public void setPoints(String points){
		this.points = points;
	}

	public String getPoints(){
		return points;
	}

	public void setIsRunning(int isRunning){
		this.isRunning = isRunning;
	}

	public int getIsRunning(){
		return isRunning;
	}

	public void setJoinWith(int joinWith){
		this.joinWith = joinWith;
	}

	public int getJoinWith(){
		return joinWith;
	}

	public void setConfirmedChallenge(int confirmedChallenge){
		this.confirmedChallenge = confirmedChallenge;
	}

	public int getConfirmedChallenge(){
		return confirmedChallenge;
	}

	public void setIsBonus(int isBonus){
		this.isBonus = isBonus;
	}

	public int getIsBonus(){
		return isBonus;
	}

	public void setRefercode(String refercode){
		this.refercode = refercode;
	}

	public String getRefercode(){
		return refercode;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setWinAmount(int winAmount){
		this.winAmount = winAmount;
	}

	public int getWinAmount(){
		return winAmount;
	}

	public void setTeam2display(String team2display){
		this.team2display = team2display;
	}

	public String getTeam2display(){
		return team2display;
	}

	public void setMaximumUser(int maximumUser){
		this.maximumUser = maximumUser;
	}

	public int getMaximumUser(){
		return maximumUser;
	}

	@Override
 	public String toString(){
		return 
			"ContestItem{" + 
			"totalwinners = '" + totalwinners + '\'' + 
			",entryfee = '" + entryfee + '\'' + 
			",team1display = '" + team1display + '\'' + 
			",getjoinedpercentage = '" + getjoinedpercentage + '\'' + 
			",joinedusers = '" + joinedusers + '\'' + 
			",isjoined = '" + isjoined + '\'' + 
			",isselected = '" + isselected + '\'' + 
			",matchkey = '" + matchkey + '\'' + 
			",multi_entry = '" + multiEntry + '\'' + 
			",isselectedid = '" + isselectedid + '\'' + 
			",points = '" + points + '\'' + 
			",is_running = '" + isRunning + '\'' + 
			",join_with = '" + joinWith + '\'' + 
			",confirmed_challenge = '" + confirmedChallenge + '\'' + 
			",is_bonus = '" + isBonus + '\'' + 
			",refercode = '" + refercode + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",win_amount = '" + winAmount + '\'' + 
			",team2display = '" + team2display + '\'' + 
			",maximum_user = '" + maximumUser + '\'' + 
			"}";
		}
}