package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RefreshScoreItem {
    @SerializedName("contest")
    private ArrayList<LiveFinishedContestData> contest;

    public ArrayList<LiveFinishedContestData> getContest() {
        return contest == null?new ArrayList<>():contest;
    }

    public void setContest(ArrayList<LiveFinishedContestData> contest) {
        this.contest = contest;
    }
}
