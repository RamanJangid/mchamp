package com.rg.mchampgold.app.view.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.databinding.ActivityFilltersBinding;

import javax.inject.Inject;

public class FilterActivity extends AppCompatActivity{

    ActivityFilltersBinding mBinding;

    @Inject
    OAuthRestService oAuthRestService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_fillters);
        initialize();
    }

    void initialize() {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wallet,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {// openNotificationActivity();
            return true;

          /*  case R.id.navigation_wallet:
               // openWalletActivity();
                return true;*/
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }






}
