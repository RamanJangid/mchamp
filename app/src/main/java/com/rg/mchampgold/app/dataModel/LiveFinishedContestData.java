package com.rg.mchampgold.app.dataModel;

import java.util.List;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;
import com.rg.mchampgold.R;

@Generated("com.robohorse.robopojogenerator")
public class LiveFinishedContestData {

    @SerializedName("winingamount")
    private int winingamount;

    @SerializedName("is_private")
    private int isPrivate;

    @SerializedName("totalwinners")
    private int totalwinners;

    @SerializedName("refercode")
    private String refercode = "";

    @SerializedName("id")
    private int challengeId;

    public String getFirst_rank_prize() {
        return first_rank_prize;
    }

    public void setFirst_rank_prize(String first_rank_prize) {
        this.first_rank_prize = first_rank_prize;
    }

    @SerializedName("first_rank_prize")
    private String first_rank_prize = "0";


    public String getRefercode() {
        return refercode;
    }

    public void setRefercode(String refercode) {
        this.refercode = refercode;
    }

    @SerializedName("minimum_user")
    private int minimumUser;

    @SerializedName("recycler_item_price_card")
    private List<PriceCardItem> priceCard;

    @SerializedName("teams")
    private List<ContestTeamModel> teams;

    @SerializedName("multi_entry")
    private int multiEntry;

    @SerializedName("confirmed")
    private int confirmed;

    @SerializedName("points")
    private String points;

    @SerializedName("matchstatus")
    private String matchstatus;

    @SerializedName("teamid")
    private int teamid;

    @SerializedName("userrank")
    private int userrank;

    @SerializedName("maximum_user")
    private int maximumUser;

    @SerializedName("can_invite")
    private int canInvite;

    @SerializedName("entryfee")
    private int entryfee;

    @SerializedName("challenge_type")
    private String challengeType;

    @SerializedName("joinedusers")
    private int joinedusers;

    @SerializedName("joinid")
    private int joinid;

    @SerializedName("grand")
    private int grand;

    @SerializedName("winning_percentage")
    private Object winningPercentage;

    @SerializedName("pdf")
    private String pdf;

    @SerializedName("join_with")
    private String joinWith;

    @SerializedName("name")
    private String name;

    @SerializedName("team_number_get")
    private int teamNumberGet;

    @SerializedName("win_amount")
    private int winAmount;

    @SerializedName("winning_amount")
    private String totalwinningAmount;

    @SerializedName("pdf_created")
    private int pdfCreated;

    @SerializedName("status")
    private int status;

    @SerializedName("getjoinedpercentage")
    private String getjoinedpercentage;

    @SerializedName("confirmed_challenge")
    private int confirmed_challenge;

    @SerializedName("is_bonus")
    private int isBonus;

    @SerializedName("bonus_percent")
    private String bonusPercent;


    @SerializedName("category")
    private String category;

    @SerializedName("max_team_limit")
    private int maxTeamLimit;

    @SerializedName("wd")
    private int wd;

    @SerializedName("user_joined_count")
    private int user_joined_count;

    @SerializedName("flexible_league")
    private int flexibleLeague;

    @SerializedName("flexible_win_amount")
    private String flexibleWinAmount;

    @SerializedName("flexible_first_rank_prize")
    private String flexibleFirstRankPrize;

    public String showFirstRankPrize() {
        return "₹" + first_rank_prize;
    }

    public int getPrizeColorCode() {
        return flexibleLeague == 1 ? R.color.unselected_tab : R.color.match_title_color;
    }

    public boolean isFlexibleLeague() {
        return flexibleLeague == 1;
    }

    public String getFlexibleWinAmount() {
        return "₹" + flexibleWinAmount;
    }

    public void setFlexibleWinAmount(String flexibleWinAmount) {
        this.flexibleWinAmount = flexibleWinAmount;
    }

    public String getFlexibleFirstRankPrize() {
        return "₹" + flexibleFirstRankPrize;
    }

    public void setFlexibleFirstRankPrize(String flexibleFirstRankPrize) {
        this.flexibleFirstRankPrize = flexibleFirstRankPrize;
    }

    public int getFlexibleLeague() {
        return flexibleLeague;
    }

    public void setFlexibleLeague(int flexibleLeague) {
        this.flexibleLeague = flexibleLeague;
    }

    public int getMaxTeamLimit() {
        return maxTeamLimit;
    }

    public void setMaxTeamLimit(int maxTeamLimit) {
        this.maxTeamLimit = maxTeamLimit;
    }

    public void setWiningamount(int winingamount) {
        this.winingamount = winingamount;
    }

    public int getWiningamount() {
        return winingamount;
    }

    public void setIsPrivate(int isPrivate) {
        this.isPrivate = isPrivate;
    }

    public int getIsPrivate() {
        return isPrivate;
    }

    public void setTotalwinners(int totalwinners) {
        this.totalwinners = totalwinners;
    }

    public int getTotalwinners() {
        return totalwinners;
    }

    public void setChallengeId(int challengeId) {
        this.challengeId = challengeId;
    }

    public int getChallengeId() {
        return challengeId;
    }

    public void setMinimumUser(int minimumUser) {
        this.minimumUser = minimumUser;
    }

    public int getMinimumUser() {
        return minimumUser;
    }

    public void setPriceCard(List<PriceCardItem> priceCard) {
        this.priceCard = priceCard;
    }

    public List<PriceCardItem> getPriceCard() {
        return priceCard;
    }

    public void setMultiEntry(int multiEntry) {
        this.multiEntry = multiEntry;
    }

    public int getMultiEntry() {
        return multiEntry;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPoints() {
        return points;
    }

    public void setMatchstatus(String matchstatus) {
        this.matchstatus = matchstatus;
    }

    public String getMatchstatus() {
        return matchstatus;
    }

    public void setTeamid(int teamid) {
        this.teamid = teamid;
    }

    public int getTeamid() {
        return teamid;
    }

    public void setUserrank(int userrank) {
        this.userrank = userrank;
    }

    public int getUserrank() {
        return userrank;
    }

    public void setMaximumUser(int maximumUser) {
        this.maximumUser = maximumUser;
    }

    public int getMaximumUser() {
        return maximumUser;
    }

    public void setCanInvite(int canInvite) {
        this.canInvite = canInvite;
    }

    public int getCanInvite() {
        return canInvite;
    }

    public void setEntryfee(int entryfee) {
        this.entryfee = entryfee;
    }

    public int getEntryfee() {
        return entryfee;
    }

    public void setChallengeType(String challengeType) {
        this.challengeType = challengeType;
    }

    public String getChallengeType() {
        return challengeType;
    }

    public void setJoinedusers(int joinedusers) {
        this.joinedusers = joinedusers;
    }

    public int getJoinedusers() {
        return joinedusers;
    }

    public void setJoinid(int joinid) {
        this.joinid = joinid;
    }

    public int getJoinid() {
        return joinid;
    }

    public void setGrand(int grand) {
        this.grand = grand;
    }

    public int getGrand() {
        return grand;
    }

    public void setWinningPercentage(Object winningPercentage) {
        this.winningPercentage = winningPercentage;
    }

    public Object getWinningPercentage() {
        return winningPercentage;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getPdf() {
        return pdf;
    }

    public void setJoinWith(String joinWith) {
        this.joinWith = joinWith;
    }

    public String getJoinWith() {
        return joinWith;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setTeamNumberGet(int teamNumberGet) {
        this.teamNumberGet = teamNumberGet;
    }

    public int getTeamNumberGet() {
        return teamNumberGet;
    }

    public void setWinAmount(int winAmount) {
        this.winAmount = winAmount;
    }

    public int getWinAmount() {
        return winAmount;
    }

    public void setPdfCreated(int pdfCreated) {
        this.pdfCreated = pdfCreated;
    }

    public int getPdfCreated() {
        return pdfCreated;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String showWinningAmout() {
        return "₹" + winAmount;
    }


    public String totalWinnersSt() {
        return totalwinners + "";
    }

    public String getSpots() {
//		int left = (getMaximumUser()) - (getJoinedusers());
        return getJoinedusers() + "/" + getMaximumUser();
    }

    public String showJoinAmount() {
        return "₹" + entryfee;
    }


    public String getTotalwinningAmount() {
        return totalwinningAmount;
    }

    public void setTotalwinningAmount(String totalwinningAmount) {
        this.totalwinningAmount = totalwinningAmount;
    }

    public String showUserRank() {
        return "#" + userrank;
    }


    public String getGetjoinedpercentage() {
        return getjoinedpercentage;
    }

    public void setGetjoinedpercentage(String getjoinedpercentage) {
        this.getjoinedpercentage = getjoinedpercentage;
    }

    //for bonous

    public boolean isShowBTag() {
        return isBonus == 1 ? true : false;
    }
    //for confirm

    public boolean isShowCTag() {
        return confirmed_challenge == 1 ? true : false;
    }
    //for multi user

    public boolean isShowMTag() {
        return multiEntry == 1 ? true : false;
    }

    //for multi user
    public boolean isShowWDTag() {
        if (wd == 1)
            return true;
        else
            return false;
    }

    public String getBonusPercent() {
        return bonusPercent;
    }

    public void setBonusPercent(String bonusPercent) {
        this.bonusPercent = bonusPercent;
    }

    public int getConfirmed_challenge() {
        return confirmed_challenge;
    }

    public void setConfirmed_challenge(int confirmed_challenge) {
        this.confirmed_challenge = confirmed_challenge;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getWd() {
        return wd;
    }

    public void setWd(int wd) {
        this.wd = wd;
    }

    public List<ContestTeamModel> getTeams() {
        return teams;
    }

    public void setTeams(List<ContestTeamModel> teams) {
        this.teams = teams;
    }

    public int getIsBonus() {
        return isBonus;
    }

    public void setIsBonus(int isBonus) {
        this.isBonus = isBonus;
    }

    public int getUser_joined_count() {
        return user_joined_count;
    }

    public void setUser_joined_count(int user_joined_count) {
        this.user_joined_count = user_joined_count;
    }

    @Override
    public String toString() {
        return "LiveFinishedContestData{" +
                "winingamount=" + winingamount +
                ", isPrivate=" + isPrivate +
                ", totalwinners=" + totalwinners +
                ", refercode='" + refercode + '\'' +
                ", challengeId=" + challengeId +
                ", first_rank_prize='" + first_rank_prize + '\'' +
                ", minimumUser=" + minimumUser +
                ", priceCard=" + priceCard +
                ", teams=" + teams +
                ", multiEntry=" + multiEntry +
                ", confirmed=" + confirmed +
                ", points='" + points + '\'' +
                ", matchstatus='" + matchstatus + '\'' +
                ", teamid=" + teamid +
                ", userrank=" + userrank +
                ", maximumUser=" + maximumUser +
                ", canInvite=" + canInvite +
                ", entryfee=" + entryfee +
                ", challengeType='" + challengeType + '\'' +
                ", joinedusers=" + joinedusers +
                ", joinid=" + joinid +
                ", grand=" + grand +
                ", winningPercentage=" + winningPercentage +
                ", pdf='" + pdf + '\'' +
                ", joinWith='" + joinWith + '\'' +
                ", name='" + name + '\'' +
                ", teamNumberGet=" + teamNumberGet +
                ", winAmount=" + winAmount +
                ", totalwinningAmount='" + totalwinningAmount + '\'' +
                ", pdfCreated=" + pdfCreated +
                ", status=" + status +
                ", getjoinedpercentage='" + getjoinedpercentage + '\'' +
                ", confirmed_challenge=" + confirmed_challenge +
                ", isBonus=" + isBonus +
                ", bonusPercent='" + bonusPercent + '\'' +
                ", category='" + category + '\'' +
                ", maxTeamLimit=" + maxTeamLimit +
                ", wd=" + wd +
                ", user_joined_count=" + user_joined_count +
                ", flexibleLeague=" + flexibleLeague +
                ", flexibleWinAmount='" + flexibleWinAmount + '\'' +
                ", flexibleFirstRankPrize='" + flexibleFirstRankPrize + '\'' +
                '}';
    }
}