package com.rg.mchampgold.app.view.activity;

import android.graphics.Bitmap;
import android.os.Bundle;

import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.databinding.ActivityWebviewBinding;


public class WebActivity extends AppCompatActivity {
    ActivityWebviewBinding mBinding;

    String title ="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_webview);
        mBinding.webView.loadUrl(getIntent().getStringExtra("type"));
        title =  getIntent().getExtras().getString("title");
        mBinding.webView.getSettings().setJavaScriptEnabled(true);
        initialize();
        mBinding.webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mBinding.setRefreshing(true);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mBinding.setRefreshing(false);
            }
        });
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void initialize() {
        setSupportActionBar(mBinding.linearToolBar.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


    }

}
