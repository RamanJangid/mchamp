package com.rg.mchampgold.app.view.myMatches;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.dataModel.SportType;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.myMatches.finished.FinishedMatchFragment;
import com.rg.mchampgold.app.view.myMatches.live.LiveMatchFragment;
import com.rg.mchampgold.app.view.myMatches.upComing.UpcomingMatchFragment;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.FragmentMyMatchesBinding;

import java.util.ArrayList;
import java.util.List;


public class MyMatchesFragment extends Fragment {

    private FragmentMyMatchesBinding fragmentMyMatchesBinding;
    String sportKey="CRICKET";
    private TextView deSelectTab;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentMyMatchesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_matches, container, false);
       /* if(container!=null)
        container.removeAllViews();*/
        View root = fragmentMyMatchesBinding.tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(Color.parseColor("#dbdfea"));
            drawable.setSize(2, root.getHeight());
            //  ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
        /*ArrayList<SportType> list=new ArrayList<>();
        SportType sportType=new SportType();
        sportType.setSportName("CRICKET");
        sportType.setSport_key("CRICKET");
        list.add(sportType);
        SportType sportType2=new SportType();
        sportType2.setSportName("BASKETBALL");
        sportType2.setSport_key("BASKETBALL");
        list.add(sportType2);
        SportType sportType1=new SportType();
        sportType1.setSportName("FOOTBALL");
        sportType1.setSport_key("FOOTBALL");
        list.add(sportType1);
        setSportsCategory(list);*/
        return fragmentMyMatchesBinding.getRoot();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        MyApplication.tinyDB.putString(Constants.SPORT_KEY,sportKey);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        setupViewPager(fragmentMyMatchesBinding.viewPager);
    }

    private void setSportsCategory(ArrayList<SportType> list) {
        String jsonList = new Gson().toJson(list);
        MyApplication.tinyDB.putString(Constants.SHARED_SPORTS_LIST,jsonList);
        if(list.size()>1) {
            fragmentMyMatchesBinding.sportTab.setVisibility(View.VISIBLE);
        }
        else {
            fragmentMyMatchesBinding.sportTab.setVisibility(View.GONE);
        }
        for(int i=0; i<list.size(); i++) {
            TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
            tabOne.setText(list.get(i).getSportName());

            fragmentMyMatchesBinding.sportTab.addTab(fragmentMyMatchesBinding.sportTab.newTab().setText(list.get(i).getSportName()));
            if (list.get(i).getSportName().equalsIgnoreCase("CRICKET")) {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.cricket_tab_selector, 0, 0);
                tabOne.setTextColor(getResources().getColor(R.color.pink));
                deSelectTab = tabOne;
            }else if (list.get(i).getSportName().equalsIgnoreCase("FOOTBALL")){
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.football_tab_selector, 0, 0);
            }else {
                tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.basketball_tab_selector, 0, 0);
            }
            fragmentMyMatchesBinding.sportTab.getTabAt(i).setCustomView(tabOne);
        }
        fragmentMyMatchesBinding.sportTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                TextView textView = view.findViewById(R.id.tab);
                textView.setTextColor(getResources().getColor(R.color.pink));
                deSelectTab.setTextColor(getResources().getColor(R.color.unselected_tab));
                deSelectTab = textView;

                sportKey = list.get(tab.getPosition()).getSport_key();
                MyApplication.tinyDB.putString(Constants.SPORT_KEY,sportKey);

//                if (sportKey.equals(Constants.TAG_FOOTBALL)) {
//                    fragmentMyMatchesBinding.llComingSoon.setVisibility(View.VISIBLE);
//                } else {
//                    fragmentMyMatchesBinding.llComingSoon.setVisibility(View.GONE);
//                    setupRecyclerView();
//                }
                setupRecyclerView();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }


    @Override
    @SuppressWarnings("deprecation")
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());


        adapter.addFrag(new UpcomingMatchFragment(), getString(R.string.upcoming));
        adapter.addFrag(new LiveMatchFragment(), getString(R.string.live));
        adapter.addFrag(new FinishedMatchFragment(), getString(R.string.finished));
        viewPager.setAdapter(adapter);

        fragmentMyMatchesBinding.tabLayout.setSelectedTabIndicatorColor(Color.TRANSPARENT);
        fragmentMyMatchesBinding.tabLayout.setupWithViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.navigation_wallet) {
            startActivity(new Intent(getActivity(), MyWalletActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_wallet_theme, menu);
        menu.findItem(R.id.navigation_notification).setVisible(false);
        AppUtils.setWalletBalance(menu, getResources().getColor(R.color.colorYellow), false, 0, 0);
//        menu.findItem(R.id.navigation_wallet).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }
}