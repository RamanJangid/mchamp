package com.rg.mchampgold.app.dataModel;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PriceCardItem{

	@SerializedName("start_position")
	private int startPosition;

	@SerializedName("price")
	private int price;

	@SerializedName("id")
	private int id;

	public void setStartPosition(int startPosition){
		this.startPosition = startPosition;
	}

	public int getStartPosition(){
		return startPosition;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"PriceCardItem{" + 
			"start_position = '" + startPosition + '\'' + 
			",price = '" + price + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}



}