package com.rg.mchampgold.app.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.rg.mchampgold.R;

import com.rg.mchampgold.app.dataModel.LiveMatchesScoreInningListItem;
import com.rg.mchampgold.databinding.FullScoreCardInningsLayoutBinding;

import java.util.ArrayList;
import java.util.List;

public class FullScoreCardInningsTabFragment extends Fragment {
    FullScoreCardInningsLayoutBinding mBinding;
    ArrayList<LiveMatchesScoreInningListItem> inningListItems=new ArrayList<>();
    int position;
    public FullScoreCardInningsTabFragment(ArrayList<LiveMatchesScoreInningListItem> inningListItems,int position) {
        this.inningListItems=inningListItems;
        this.position=position;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.full_score_card_innings_layout, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        for (int i=0; i<inningListItems.size(); i++){
            adapter.addFrag(new PlayerFullScoreCardFragment(inningListItems.get(i)), i==0?(i+1)+"st Inning":i==1?(i+1)+"nd Inning":i==2?(i+1)+"rd Inning":(i+1)+"th Inning");
        }

        mBinding.viewPager.setAdapter(adapter);
        mBinding.tabLayout.setupWithViewPager(mBinding.viewPager);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}