package com.rg.mchampgold.app.view.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.BaseRequest;
import com.rg.mchampgold.app.api.request.JoinContestRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Contest;
import com.rg.mchampgold.app.dataModel.JoinedContesttItem;
import com.rg.mchampgold.app.dataModel.LiveMatchesResponse;
import com.rg.mchampgold.app.dataModel.MatchListItem;
import com.rg.mchampgold.app.dataModel.TeamsListItem;
import com.rg.mchampgold.app.dataModel.TopPlayersItem;
import com.rg.mchampgold.app.dataModel.TopScoreItems;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.FullScoreCardActivity;
import com.rg.mchampgold.app.view.adapter.JoinedContestItemAdapter;
import com.rg.mchampgold.app.view.interfaces.OnContestItemClickListener;
import com.rg.mchampgold.app.viewModel.ContestDetailsViewModel;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.LiveMatchesFragmentBinding;

import java.util.ArrayList;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

public class LiveMatchesFragment extends Fragment implements OnContestItemClickListener, View.OnClickListener {
    @Inject
    OAuthRestService oAuthRestService;
    ContestDetailsViewModel contestDetailsViewModel;
    ArrayList<JoinedContesttItem> list = new ArrayList<>();
    private JoinedContestItemAdapter mAdapter;
    LiveMatchesFragmentBinding liveMatchesFragmentBinding;
    ArrayList<MatchListItem> matchListItems = new ArrayList<>();
    ArrayList<TeamsListItem> teamsListItems = new ArrayList<>();
    ArrayList<TopPlayersItem> topPlayersItems = new ArrayList<>();
    ArrayList<TopScoreItems> topScoreItems = new ArrayList<>();
    String match_key;
    int position;
    public LiveMatchesFragment(String match_key,int position){
        this.match_key=match_key;
        this.position=position;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        liveMatchesFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.live_matches_fragment, container, false);
        liveMatchesFragmentBinding.fullScoreCard.setOnClickListener(this);
        setupRecyclerView();
        return liveMatchesFragmentBinding.getRoot();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        MyApplication.getAppComponent().inject(this);
        contestDetailsViewModel = ContestDetailsViewModel.create(this);
        MyApplication.getAppComponent().inject(contestDetailsViewModel);
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private void setupRecyclerView() {
        mAdapter = new JoinedContestItemAdapter(getActivity(), list, this,match_key);
        liveMatchesFragmentBinding.recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        liveMatchesFragmentBinding.recyclerView.setLayoutManager(mLayoutManager);
        liveMatchesFragmentBinding.recyclerView.setAdapter(mAdapter);
        //getData();
    }
    @Override
    public void onContestClick(Contest contest, boolean isForDetail) {

    }

    @Override
    public void onResume() {
        super.onResume();
        LiveMatcheslist();
        getData();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.fullScoreCard) {
            Intent intent = new Intent(getActivity(), FullScoreCardActivity.class);
            intent.putExtra("match_key", matchListItems.get(position).getMatchkey());
            intent.putExtra("teamvsteam", matchListItems.get(position).getShort_name());
            intent.putExtra("teamName1", teamsListItems.get(0).getShort_name());
            intent.putExtra("teamName1Image", teamsListItems.get(0).getImage());
            intent.putExtra("scoreFull1", teamsListItems.get(0).getScores_full());
            intent.putExtra("teamName2", teamsListItems.get(1).getShort_name());
            intent.putExtra("teamName2Image", teamsListItems.get(1).getImage());
            intent.putExtra("scoreFull2", teamsListItems.get(1).getScores_full());
            getActivity().startActivity(intent);
        }
    }
    public void LiveMatcheslist() {

        BaseRequest baseRequest = new BaseRequest();
        baseRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        baseRequest.setMatchkey(match_key);
        CustomCallAdapter.CustomCall<LiveMatchesResponse> orderIdResponse = oAuthRestService.liveMatches(baseRequest);
        orderIdResponse.enqueue(new CustomCallAdapter.CustomCallback<LiveMatchesResponse>() {
            @Override
            public void success(Response<LiveMatchesResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    LiveMatchesResponse bannerListResponse = response.body();
                    if (bannerListResponse.getStatus() == 1 && bannerListResponse.getResult().size() > 0) {
                        matchListItems=bannerListResponse.getResult();
                        liveMatchesFragmentBinding.myteamcount.setText("My Teams ("+bannerListResponse.getResult().get(position).getUser_teams()+")");
                        liveMatchesFragmentBinding.estimateWinning.setText("₹"+bannerListResponse.getResult().get(position).getEstimated_win());
                        if (bannerListResponse.getResult().get(position).getTeams().size()>0) {
                            teamsListItems = bannerListResponse.getResult().get(position).getTeams();
                            setupTeamData();
                        }
                        if (bannerListResponse.getResult().get(position).getTop_players().size()>0) {
                            topPlayersItems = bannerListResponse.getResult().get(position).getTop_players();
                            setupPlayerList();
                        }
                        if (bannerListResponse.getResult().get(position).getTop_scorer().size()>0) {
                            topScoreItems = bannerListResponse.getResult().get(position).getTop_scorer();
                            setupTopScorePlayer();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(com.rg.mchampgold.common.api.ApiException e) {
                e.printStackTrace();
            }
        });
    }

    private void setupTopScorePlayer() {
        liveMatchesFragmentBinding.topPlayer1.setText(topScoreItems.get(0).getName());
        liveMatchesFragmentBinding.topPlayer1Run.setText(topScoreItems.get(0).getRun()+"("+topScoreItems.get(0).getBall()+")");

        liveMatchesFragmentBinding.topPlayer2.setText(topScoreItems.get(1).getName());
        liveMatchesFragmentBinding.topPlayer2Run.setText(topScoreItems.get(1).getRun()+"("+topScoreItems.get(1).getBall()+")");
    }

    public void setupTeamData(){
        if (teamsListItems.get(0).getScores_inning_1().equalsIgnoreCase("")){
            liveMatchesFragmentBinding.llIninngT1.setVisibility(View.GONE);
            liveMatchesFragmentBinding.llIninngT2.setVisibility(View.GONE);
        }
        liveMatchesFragmentBinding.teamName1.setText(teamsListItems.get(0).getShort_name());
        liveMatchesFragmentBinding.scoresFull1.setText(teamsListItems.get(0).getScores_full());
        liveMatchesFragmentBinding.overs1.setText("("+teamsListItems.get(0).getOvers()+")");
        liveMatchesFragmentBinding.scoresFull3.setText(teamsListItems.get(0).getScores_inning_1());
        liveMatchesFragmentBinding.overs3.setText("("+teamsListItems.get(0).getOvers_inning_1()+")");
        AppUtils.loadImageBanner(liveMatchesFragmentBinding.teamImage1, teamsListItems.get(0).getImage());

        liveMatchesFragmentBinding.teamName2.setText(teamsListItems.get(1).getShort_name());
        liveMatchesFragmentBinding.scoresFull2.setText(teamsListItems.get(1).getScores_full());
        liveMatchesFragmentBinding.overs2.setText("("+teamsListItems.get(1).getOvers()+")");
        liveMatchesFragmentBinding.scoresFull4.setText(teamsListItems.get(1).getScores_inning_1());
        liveMatchesFragmentBinding.overs4.setText("("+teamsListItems.get(1).getOvers_inning_1()+")");
        AppUtils.loadImageBanner(liveMatchesFragmentBinding.teamImage2, teamsListItems.get(1).getImage());
    }
    public void setupPlayerList(){
        for(int i=0; i< topPlayersItems.size(); i++) {
            CircleImageView imgView = new CircleImageView(getActivity());
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(90, 90);
            lp.setMarginStart(5);
            imgView.setLayoutParams(lp);
            AppUtils.loadImageBanner(imgView, topPlayersItems.get(i).getImage());
            liveMatchesFragmentBinding.llPlayer.addView(imgView);
        }
    }

    @SuppressLint("RestrictedApi")
    public void getData() {
        JoinContestRequest request = new JoinContestRequest();
        request.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        request.setMatchKey(match_key);
        contestDetailsViewModel.loadJoinedContestRequest(request);
        contestDetailsViewModel.getJoinedContestData().observe(this, arrayListResource -> {
            Log.d("Status ", "" + arrayListResource.getStatus());
            switch (arrayListResource.getStatus()) {
                case LOADING: {
                    liveMatchesFragmentBinding.setRefreshing(true);
                    break;
                }
                case ERROR:
                    liveMatchesFragmentBinding.setRefreshing(false);
                    Toast.makeText(MyApplication.appContext, arrayListResource.getException().getErrorModel().errorMessage, Toast.LENGTH_SHORT).show();
                    break;
                case SUCCESS: {
                    liveMatchesFragmentBinding.setRefreshing(false);
                    if (arrayListResource.getData().getStatus() == 1 && arrayListResource.getData().getJoinedContestItem().getContest().size() > 0) {
                        liveMatchesFragmentBinding.rlNoTeam.setVisibility(View.GONE);
                        liveMatchesFragmentBinding.rlMainLayout.setVisibility(View.VISIBLE);
                        list = arrayListResource.getData().getJoinedContestItem().getContest();
                        mAdapter.updateData(list);
                    } else {
                        liveMatchesFragmentBinding.rlNoTeam.setVisibility(View.VISIBLE);
                        liveMatchesFragmentBinding.rlMainLayout.setVisibility(View.GONE);
                    }
                    break;
                }
            }

        });
    }
}
