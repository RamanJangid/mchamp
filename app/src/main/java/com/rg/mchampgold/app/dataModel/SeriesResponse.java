package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SeriesResponse {

    @SerializedName("status")
    int status;

    @SerializedName("result")
    ArrayList<SeriesDataModel> result;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<SeriesDataModel> getResult() {
        return result;
    }

    public void setResult(ArrayList<SeriesDataModel> result) {
        this.result = result;
    }
}
