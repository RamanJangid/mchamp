package com.rg.mchampgold.app.dataModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class MultiSportsPlayerPointItem implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("player_name")
    private String player_name;

    @SerializedName("team")
    private String team;

    @SerializedName("selected_by")
    private String selected_by;

    @SerializedName("captain_by")
    private String captainBy;

    @SerializedName("points")
    private String points;

    @SerializedName("image")
    private String image;

    @SerializedName("isSelected")
    private int isSelected;

    @SerializedName("is_topplayer")
    private int isTopplayer;

    @SerializedName("breakup_points")
    private ArrayList<MultiSportsPlayerBreakPointItem> breakup_points;


    public ArrayList<MultiSportsPlayerBreakPointItem> getBreakup_points() {
        return breakup_points;
    }

    public void setBreakup_points(ArrayList<MultiSportsPlayerBreakPointItem> breakup_points) {
        this.breakup_points = breakup_points;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayer_name() {
        return player_name;
    }

    public void setPlayer_name(String player_name) {
        this.player_name = player_name;
    }

    public String getSelected_by() {
        return selected_by;
    }

    public void setSelected_by(String selected_by) {
        this.selected_by = selected_by;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String showSelectedBy() {
        return selected_by+"%";
    }

    public String showCaptainBy() {
        return captainBy +"%";
    }

    public int getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(int isSelected) {
        this.isSelected = isSelected;
    }

    public void setIsTopplayer(int isTopplayer){
        this.isTopplayer = isTopplayer;
    }

    public int getIsTopplayer(){
        return isTopplayer;
    }

    public String getCaptainBy() {
        return captainBy;
    }

    public void setCaptainBy(String captainBy) {
        this.captainBy = captainBy;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }
}
