package com.rg.mchampgold.app.view.football;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.api.request.TeamPreviewPointRequest;
import com.rg.mchampgold.app.api.service.OAuthRestService;
import com.rg.mchampgold.app.dataModel.Player;
import com.rg.mchampgold.app.dataModel.TeamPointPreviewResponse;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.app.view.activity.MyWalletActivity;
import com.rg.mchampgold.app.view.activity.NotificationActivity;
import com.rg.mchampgold.common.api.ApiException;
import com.rg.mchampgold.common.api.CustomCallAdapter;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.ActivityFootballTeamPointPreviewBinding;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;


public class FootballTeamPreviewPointActivity extends AppCompatActivity {

    ActivityFootballTeamPointPreviewBinding activityTeamPointPreviewBinding;

    @Inject
    OAuthRestService oAuthRestService;

    List<Player> listGk = new ArrayList<>();
    List<Player> listDif = new ArrayList<>();
    List<Player> listMid = new ArrayList<>();
    List<Player> listForward = new ArrayList<>();


    String teamId;
    String challengeId;
    boolean isForLeaderBoard;
    String teamName="";
    String tPoints="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        MyApplication.getAppComponent().inject(FootballTeamPreviewPointActivity.this);
        AppUtils.setStatusBar(this,getResources().getColor(R.color.match_pitch_bg));
        activityTeamPointPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_football_team_point_preview);
        initialize();

        LinearLayoutManager horizontalLayoutManagaerr = new LinearLayoutManager(FootballTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.wickRecyclerView.setLayoutManager(horizontalLayoutManagaerr);

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(FootballTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.bolRecyclerView.setLayoutManager(horizontalLayoutManagaer);

        LinearLayoutManager horizontalLayoutManagaer1 = new LinearLayoutManager(FootballTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.allRecyclerView.setLayoutManager(horizontalLayoutManagaer1);

        LinearLayoutManager horizontalLayoutManagaer2 = new LinearLayoutManager(FootballTeamPreviewPointActivity.this, LinearLayoutManager.HORIZONTAL, false);
        activityTeamPointPreviewBinding.batRecyclerView.setLayoutManager(horizontalLayoutManagaer2);

        activityTeamPointPreviewBinding.icClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.navigation_notification) {
            openNotificationActivity();
            return true;
        } else if (itemId == R.id.navigation_wallet) {
            openWalletActivity();
            return true;
        } else if (itemId == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void initialize() {
    /*    setSupportActionBar(activityTeamPointPreviewBinding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.team_preview));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
*/


        if(getIntent()!=null && getIntent().getExtras()!=null) {
            teamId=  getIntent().getExtras().getInt("teamId")+"";
            challengeId=  getIntent().getExtras().getInt("challengeId")+"";
            isForLeaderBoard=  getIntent().getExtras().getBoolean("isForLeaderBoard");
            teamName=  getIntent().getExtras().getString(Constants.KEY_TEAM_NAME);
            tPoints=  getIntent().getExtras().getString("tPoints");
        }

        activityTeamPointPreviewBinding.icClose.setOnClickListener(view -> finish());
        activityTeamPointPreviewBinding.teamName.setText(teamName);
        // activityTeamPointPreviewBinding.totalPoints.setText("Total Points "+tPoints);

        getPlayerInfo();

    }


    private void openNotificationActivity() {
        startActivity(new Intent(FootballTeamPreviewPointActivity.this, NotificationActivity.class));
    }

    private void openWalletActivity() {
        startActivity(new Intent(FootballTeamPreviewPointActivity.this, MyWalletActivity.class));
    }

    public void getPlayerInfo() {
        activityTeamPointPreviewBinding.setRefreshing(true);
        TeamPreviewPointRequest teamPreviewPointRequest = new TeamPreviewPointRequest();
        teamPreviewPointRequest.setUser_id(MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_ID));
        teamPreviewPointRequest.setChallenge(challengeId);
        teamPreviewPointRequest.setTeamid(teamId);
        teamPreviewPointRequest.setSport_key(Constants.TAG_FOOTBALL);
        CustomCallAdapter.CustomCall<TeamPointPreviewResponse> bankDetailResponseCustomCall = oAuthRestService.getPreviewPoints(teamPreviewPointRequest);
        bankDetailResponseCustomCall.enqueue(new CustomCallAdapter.CustomCallback<TeamPointPreviewResponse>() {
            @Override
            public void success(Response<TeamPointPreviewResponse> response) {
                activityTeamPointPreviewBinding.setRefreshing(false);
                if (response.isSuccessful() && response.body() != null) {
                    TeamPointPreviewResponse teamPointPreviewResponse = response.body();
                    if(teamPointPreviewResponse.getStatus() ==1) {

                        listGk = teamPointPreviewResponse.getResult().getGoalKeeperList();
                        listDif = teamPointPreviewResponse.getResult().getDefenderList();
                        listMid = teamPointPreviewResponse.getResult().getMidfielderList();
                        listForward = teamPointPreviewResponse.getResult().getForwardList();

                        activityTeamPointPreviewBinding.wickRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.wickRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listGk,matchKey));

                        activityTeamPointPreviewBinding.batRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.batRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listDif,matchKey));

                        activityTeamPointPreviewBinding.bolRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.bolRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listForward,matchKey));

                        activityTeamPointPreviewBinding.allRecyclerView.setOnTouchListener((view, motionEvent) -> true);
//                        activityTeamPointPreviewBinding.allRecyclerView.setAdapter(new PreviewPlayerItemAdapter(isForLeaderBoard, listMid,matchKey));

                    }
                    else {
                        AppUtils.showErrorr(FootballTeamPreviewPointActivity.this,teamPointPreviewResponse.getMessage());
                    }
                }
            }

            @Override
            public void failure(ApiException e) {
                activityTeamPointPreviewBinding.setRefreshing(false);
                e.printStackTrace();
            }
        });
    }

}
