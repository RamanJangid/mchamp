package com.rg.mchampgold.app.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.rg.mchampgold.R;
import com.rg.mchampgold.app.MyApplication;
import com.rg.mchampgold.app.utils.AppUtils;
import com.rg.mchampgold.common.utils.Constants;
import com.rg.mchampgold.databinding.NewReferEarnBinding;

public class InviteFriendActivity extends AppCompatActivity {

    NewReferEarnBinding binding;
    String userReferCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUtils.setStatusBar(this, getResources().getColor(R.color.accent));
        binding = DataBindingUtil.setContentView(this, R.layout.new_refer_earn);
        initialize();

        userReferCode = MyApplication.tinyDB.getString(Constants.SHARED_PREFERENCE_USER_REFER_CODE);
        binding.code.setText(userReferCode);
        binding.btnInvite.setOnClickListener(view -> {
            String shareBody =
                    "Here's " + getString(R.string.rupee) + " " + MyApplication.invite_bonus + " to play fantasy cricket with me On " + getString(R.string.app_name) +
                            " Click " + MyApplication.apk_url + " to download " + getString(R.string.app_name) + " app & use My code "
                            + userReferCode + " To Register";

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");

            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        });

        binding.tvBonus.setOnClickListener(v -> AppUtils.openWebViewActivity("", MyApplication.bonus_terms));


        binding.btnShareText.setOnClickListener(view -> {
            String shareBody =
                    "Here's " + getString(R.string.rupee) + " " + MyApplication.invite_bonus + " to play fantasy cricket with me On " + getString(R.string.app_name) +
                            "Click " + MyApplication.apk_url + " to download " + getString(R.string.app_name) + " app & use My code "
                            + userReferCode + " To Register";

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");

            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        });


        binding.copyImage.setOnClickListener(view -> setClipboard(userReferCode));

//        binding.tvTc.setOnClickListener(view -> startActivity(new Intent(MyApplication.appContext, WebActivity.class).putExtra("title", "Terms & Conditions").putExtra("type", MyApplication.terms_url)));

//        binding.tvFaq.setOnClickListener(view -> startActivity(new Intent(MyApplication.appContext, WebActivity.class).putExtra("title", "Terms & Conditions").putExtra("type", MyApplication.terms_url)));
    }

    private void setClipboard(String text) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
        clipboard.setPrimaryClip(clip);
        AppUtils.showSuccess(InviteFriendActivity.this, "Invite code copy.");
    }

    void initialize() {
        setSupportActionBar(binding.mytoolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.refer_and_earn_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
